@isTest
public class PublicKnowledgeSearchControllerTest {

    static List<Tutorial__kav> testArticles {get;set;}
    static PublicKnowledgeSearchController pageCon {get;set;}
    
    static testMethod void PublicKnowledgeSearchController() {
        
        init();
        
        PageReference pageRef = Page.PublicKnowledgeSearch;
        pageRef.getParameters().put('search','Test');
        Test.setCurrentPage(pageRef);
        
        pageCon = new PublicKnowledgeSearchController();
        PublicKnowledgeServices.setPageCookie();
        pageCon.init();
        
        system.assertEquals(3,pageCon.searchResults.size());
    }
    
    static testMethod void search() {
		
        init();
        
        pageCon = new PublicKnowledgeSearchController();
        
        pageCon.searchString = 'Test';
        pageCon.search();
        
        system.assertEquals(3,pageCon.searchResults.size());
    }
    
    static void init() { //IN PROGRESS
        testArticles = new List<Tutorial__kav>();
        
        //Create new articles
		testArticles.add(new Tutorial__kav(
			Title = 'Article 1',
	        Summary = 'Summary',
	        URLName = 'TestTitle',
	        Content__c = 'Test Content'
		));
		testArticles.add(new Tutorial__kav(
			Title = 'Article 2',
	        Summary = 'Summary',
	        URLName = 'TestTitle2',
	        Content__c = 'Test Test Content'
		));
		testArticles.add(new Tutorial__kav(
			Title = 'Test Article',
	        Summary = 'Summary',
	        URLName = 'TestTitle3',
	        Content__c = 'Test Content'
		));
		insert testArticles;
        
        //Publish articles
        Tutorial__kav testArticle = new Tutorial__kav();
        List<Id> fixedSearchResults = new List<Id>();
        for (Integer i = 0;  i < testArticles.size(); i++) {
			testArticle = [SELECT KnowledgeArticleId FROM Tutorial__kav WHERE Id = :testArticles[i].Id];
			KbManagement.PublishingService.publishArticle(testArticle.KnowledgeArticleId, true);
			fixedSearchResults.add(testArticle.Id);
        }
		
		Test.setFixedSearchResults(fixedSearchResults);
    }
}