global without sharing class ProviderAssignmentScheduled_CS implements Schedulable {

	//Currently the reassignment process takes ~5 soql statements per record
	public static final Integer MAX_SOQL = 90;
	
	public static final String jobNameOnHour = 'Order Expiration Process (Hour)';	
	public static final String jobNameOnHalfHour = 'Order Expiration Process (Half-Hour)';	
	
	// Schedule for every 30 minutes
	public static final String scheduleOnHour = '0 0 * * * ?';
	public static final String scheduleOnHalfHour = '0 30 * * * ?';

	public static void schedule(){

		String jobName1 = (Test.isRunningTest() ? jobNameOnHour + ' Test' : jobNameOnHour);
		String jobName2 = (Test.isRunningTest() ? jobNameOnHalfHour + ' Test' : jobNameOnHalfHour);
		
		System.debug('Job Name 1: ' + jobName1);
		System.debug('Job Name 2: ' + jobName2);
		
		String jobId1 = System.schedule(jobName1, 
		        	scheduleOnHour, 
		        	new ProviderAssignmentScheduled_CS());
		
		String jobId2 = System.schedule(jobName2, 
		        	scheduleOnHalfHour, 
		        	new ProviderAssignmentScheduled_CS());
		
		System.debug('jobIds: ' + jobId1 + ', ' + jobId2);
	}

	global void execute(SchedulableContext SC){
		System.debug('In scheduled process ProviderAssignmentScheduled_CS...');
		
		DateTime currentTime = DateTime.now();
		String currentTimeFormat = currentTime.formatGmt('yyyy-MM-dd') + 'T' + currentTime.formatGMT('HH:mm:ss') + 'Z';
		
		System.debug('Finding orders expiring before ' + currentTimeFormat);
		
		List<Order__c> expiredOrders = [
			SELECT
				Id,
				Name,
				Provider__c,
				Accepted_By__c,
				Accepted_Date__c,
				Assigned_Date__c,
				Expiration_Time__c,
				Status__c,
				Plan_Patient__c,
				Plan_Patient__r.Patient__c,
				Assignment_History__c,
				Geolocation__Latitude__s,
				Geolocation__Longitude__s,
				CreatedDate,
				(
					SELECT
						Active__c,
						Unassignment__c,
						Unassigned_At__c
					FROM Order_Assignments__r
					ORDER BY CreatedDate DESC
				)
			FROM Order__c
			WHERE Expiration_Time__c < :currentTime
		];
		
		System.debug('expired orders: ' + expiredOrders);
		
		System.debug('# of expired orders: ' + expiredOrders.size());
		
		//Deactivate all current provider assignments
		List<Order_Assignment__c> assignmentsToUpdate = new List<Order_Assignment__c>();
		
		for(Order__c o : expiredOrders){
			if(o.Order_Assignments__r.size() > 0){
				Order_Assignment__c mostRecentAssignment = o.Order_Assignments__r[0];
				
				mostRecentAssignment.Active__c = false;
				mostRecentAssignment.Unassignment__c = 'Expired';
				mostRecentAssignment.Unassigned_At__c = DateTime.now();
				
				assignmentsToUpdate.add(mostRecentAssignment);
			}
		}
		
		update assignmentsToUpdate;
		
		//Clear all provider information and send back to the assignment process
		System.debug('Queries issued before assignment: ' + Limits.getQueries());
		for(Order__c o : expiredOrders){

			//If we exceed the limit, set up a batch process and bail out
			if(Limits.getQueries() >= MAX_SOQL){						        
		        System.debug('Too many queries, setting up to spawn batch.');		        
		        Database.executeBatch(new ProviderAssignmentBatch_CS());		        
		        break;
			}

			System.debug('Expired order: ' + o.Name);
			
			//Reset assignment-related values
			o.Provider__c = null;
			o.Accepted_By__c = null;
			o.Accepted_Date__c = null;
			o.Expiration_Time__c = null;
			o.Status__c = OrderModel_CS.STATUS_NEW;

            ProviderAssignmentServices_CS p = new ProviderAssignmentServices_CS(o);
			p.doAssignmentProcess();
			
			System.debug('New expiration time: ' + o.Expiration_Time__c);
		}
		
		System.debug('Queries issued after assignment: ' + Limits.getQueries());
		
		update expiredOrders;
	}

}