public without sharing class AccountTriggerDispatcher_CS {    
    public AccountTriggerDispatcher_CS(Map<Id,SObject> oldMap, Map<Id,SObject> newMap, List<SObject> triggerOld, List<SObject> triggerNew, Boolean isInsert, Boolean isUpdate, Boolean isDelete, Boolean isUndelete, Boolean isBefore, Boolean isAfter){
        if(isBefore) {
            if(isInsert) { doBeforeInsert(triggerNew); }
            else if(isUpdate) { doBeforeUpdate(oldMap, newMap, triggerOld, triggerNew); }
            else if(isDelete) { doBeforeDelete(oldMap, triggerOld); }           
        }else if(isAfter) {
            if(isInsert) { doAfterInsert(newMap, triggerNew); }
            else if(isUpdate) {doAfterUpdate(oldMap, newMap, triggerOld, triggerNew); }
            else if(isDelete) { doAfterDelete(oldMap, triggerold); }
            else if(isUndelete){ doAfterUnDelete(newMap, triggerNew); }        
        }
    }
    
    public void doBeforeInsert(List<SObject> triggerNew) {
        
    }
    public void doBeforeUpdate(Map<Id,SObject> oldMap, Map<Id,SObject> newMap, List<SObject> triggerOld, List<SObject> triggerNew) {
        
    }
    public void doBeforeDelete(Map<Id,SObject> oldMap, List<SObject> triggerOld) {
    
    }    
    public void doAfterInsert(Map<Id,SObject> newMap, List<SObject> triggerNew) {
        ////system.debug('DOING AFTER INSERT');
        Set<Id> providerIds = getproviderIds(triggerNew);
        ////system.debug('Provider Id size : '+providerIds.size());
        if(providerIds.size() > 0) {
            populateLatLng(providerIds);
        }
    }    
    public void doAfterUpdate(Map<Id,SObject> oldMap, Map<Id,SObject> newMap, List<SObject> triggerOld, List<SObject> triggerNew) {
        ////system.debug('DOING AFTER UPDATE');
        if(triggerNew.size() > 0) {
            ////system.debug('new address : '+((Account)triggerNew[0]).BillingStreet+', '+((Account)triggerNew[0]).BillingCity + ', '+((Account)triggerNew[0]).BillingState+' '+((Account)triggerNew[0]).BillingPostalCode);
        }
        //accountProviderLocationService(newMap);
        
        //Filter out providers whose addresses did not change to limit API calls
        Set<Id> addressChangeAccountIds = getAddressChangeAccountIds(oldMap, triggerNew);
        ////system.debug('address change account ids size : '+addressChangeAccountIds.size());
        if(addressChangeAccountIds.size() > 0) {
            populateLatLng(addressChangeAccountIds);
        }
    }    
    public void doAfterDelete(Map<Id,SObject> oldMap, List<SObject> triggerOld) {
    
    }    
    public void doAfterUndelete(Map<Id,SObject> newMap, List<SObject> triggerNew) {
    
    }
    public Set<Id> getproviderIds(List<SObject> triggerNew) {
        ////system.debug('GETPROVIDERMAP BEGIN');
        Set<Id> providerIds = new Set<Id>();
        for(SObject so : triggerNew) {
            Account acct = (Account)so;     
            if(acct.Type__c == 'Provider') {
                providerIds.add(acct.Id);
            }
        }
        return providerIds;
    }  
    public Set<Id> getAddressChangeAccountIds(Map<Id,SObject> oldMap, List<SObject> triggerNew) {
        Set<Id> addressChangeAccountIds = new Set<Id>();
        
        for(SObject so : triggerNew) {
            Account newP = (Account)so;
            Account oldP = (Account)oldMap.get(newP.Id);
            String oldAddress = (oldP.ShippingStreet != null ? oldP.ShippingStreet : '') + (oldP.ShippingCity != null ? oldP.ShippingCity : '') + (oldP.ShippingState != null ? oldP.ShippingState : '') + (oldP.ShippingPostalCode != null ? oldP.ShippingPostalCode : '') + (oldP.ShippingCountry != null ? oldP.ShippingCountry : '');
            String newAddress = (newP.ShippingStreet != null ? newP.ShippingStreet : '') + (newP.ShippingCity != null ? newP.ShippingCity : '') + (newP.ShippingState != null ? newP.ShippingState : '') + (newP.ShippingPostalCode != null ? newP.ShippingPostalCode : '') + (newP.ShippingCountry != null ? newP.ShippingCountry : '');
            
            if(newP.Type__c == 'Provider' && oldAddress != newAddress) {
                addressChangeAccountIds.add(newP.Id);
            }
        }
        return addressChangeAccountIds;
    }
    
    @future(callout=true)
    public static void populateLatLng(Set<Id> providerIds) {
        ////system.debug('POPULATE LAT LNG : '+providerIds);
        Map<Id, Account> providerMap = new Map<Id, Account>([
            SELECT
                Id,
                ShippingStreet,
                ShippingCity,
                ShippingState,
                ShippingPostalCode,
                ShippingCountry,
                Corporate_Geolocation__Latitude__s,
                Corporate_Geolocation__Longitude__s
            FROM Account
            WHERE Id IN : providerIds
        ]);
        List<GeocodingModel_CS> geoModels = new List<GeocodingModel_CS>();
        for(String key : providerMap.keySet()) {
            Account acct = providerMap.get(key);
            if(acct.ShippingStreet != null) {
                GeocodingModel_CS geoModel = new GeocodingModel_CS();
                geoModel.recordId = key;
                geoModel.street = acct.ShippingStreet;
                geoModel.city = acct.ShippingCity;
                geoModel.state = acct.ShippingState;
                geoModel.postalCode = acct.ShippingPostalCode;
                geoModel.country = acct.ShippingCountry;
                geoModels.add(geoModel);
            }
        }
        ////system.debug('Account GeoModels : '+geoModels);
        if(geoModels.size() > 0) {
            
            GeocodingUtils_CS.GeocodeAddresses(geoModels);

            for(GeocodingModel_CS geoModel : geoModels) {
                ////system.debug('GeoModel Key '+geoModel.RecordId);
                ////system.debug('lat, lng : '+geomodel.lat+', '+geomodel.lng);
                if(providerMap.containsKey(geoModel.recordId)) {
                    Account a = providerMap.get(geoModel.recordId); 
                    if(geoModel.lat != null) { a.Corporate_Geolocation__Latitude__s = geoModel.lat; }
                    if(geoModel.lng != null) { a.Corporate_Geolocation__Longitude__s = geoModel.lng; }
                    ////system.debug('Lat and Lng Set : '+a);
                }
            }
            ////system.debug('DOING UPDATE');
            update providerMap.values();
        }
    }

	/*
	//TODO: Delete
    public void accountProviderLocationService(Map<Id,SObject> newMap) {
        list<Provider_Location__c> lst_prv_loc = new list<Provider_Location__c>();
    
        for(Provider_Location__c pl : [Select  Account__c,
                                                Pediatric_Orthotic_Prosthetic_Services__c, 
                                                Languages__c, 
                                                Durable_Medical_Equipment_Services__c, 
                                                Adult_Orthotic_and_Prosthetic_Services__c 
                                                From Provider_Location__c 
                                                Where Account__c IN : newMap.keySet() ])
        {
            Account updatedAccount = (Account)newMap.get( pl.Account__c );
            pl.Pediatric_Orthotic_Prosthetic_Services__c = updatedAccount.Pediatric_Orthotic_Prosthetic_Services__c;
            pl.Durable_Medical_Equipment_Services__c = updatedAccount.Durable_Medical_Equipment_Services__c;
            pl.Adult_Orthotic_and_Prosthetic_Services__c = updatedAccount.Adult_Orthotic_and_Prosthetic_Services__c;
            pl.Languages__c = (updatedAccount.Languages_Spoken__c != null ? updatedAccount.Languages_Spoken__c  + ';': '') + (updatedAccount.Other__c != null ? updatedAccount.Other__c : '');
            
            lst_prv_loc.add( pl );
        }
        
        update lst_prv_loc;
    }
	*/

}