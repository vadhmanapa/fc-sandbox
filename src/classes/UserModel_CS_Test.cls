@isTest
private class UserModel_CS_Test {

	/******* Provider *******/
	static private List<Account> providers;
	static private List<Contact> providerUsers;

	/******* Test Objects *******/
	static private UserModel_CS user;
	
	/******* Test Methods *******/
	
	static testMethod void UserModel_Test_UsernameNotUnique(){
		init();
		
		Test.startTest();
		
		userModel_CS identicalUser = new UserModel_CS(providerUsers[0].clone(false,true,false,false));
		
		//"Health_Plan_User_Id" is set as an external ID
		//TODO: Figure out how to handle this
		identicalUser.instance.Health_Plan_User_Id__c = '';
		
		insert identicalUser.instance;
		
		Boolean unique = user.checkUniqueUsername(providerUsers[0].Username__c);
		
		System.assertEquals(false,unique);
		
		Test.stopTest();
		
	}

	static testMethod void UserModel_Test_UsernameUnique(){
		init();
		
		Test.startTest();
		
		Boolean unique = user.checkUniqueUsername(providerUsers[0].Username__c);
		System.assertEquals(true,unique);
		
		Test.stopTest();
	}
	
	static testMethod void UserModel_CS_ConstructFromId(){
		init();
		
		Test.startTest();
		
		user = new UserModel_CS(providerUsers[0]);
	
		Test.stopTest();
	}

	static testMethod void UserModel_CS_TestRole(){
		init();

		//TODO: TestDataFactory does not create Role's as of 12/13
		//Create role
		Role__c newRole = new Role__c();
		insert newRole;
		
		user.instance.Content_Role__c = newRole.Id;
		update user.instance;
		
		Test.startTest();
		
		Role__c userRole = user.role;
		
		System.assert(newRole.Id == userRole.Id, 'Roles are not correctly obtained.');
		
		Test.stopTest();
	}
	
	static testMethod void UserModel_Test_UpdateRecordSuccessful(){
		init();
		
		Test.StartTest();
		
		try {
			user.instance.Email = 'email@email.com';
			update user.instance;
		}catch (DMLException e) {
			System.assert(false);
		}
		
		Test.StopTest();
	}

	static testMethod void UserModel_Test_UpdateRecordUnsuccessful(){
		init();
		
		Test.StartTest();
		
		user.instance.AssistantName = 'This string is too long apsdfn;najkedgnasdhgasdjfasdfjasdjgkahsdjfkahsldfasdjflbkahsdfhlakasdfkl';
		
		try {
			update user.instance;
			System.assert(false);
		}catch (DMLException e) {}
	}
	
	static testMethod void UserModel_Test_UpdateRecordMissingRequiredField(){
		init();
		
		Test.StartTest();
		
		user.instance.Id = null;
		
		//Exception Caught
		try {
			update user.instance;
			System.assert(false);
		}catch (DMLException e) {}
		
		Test.StopTest();
	}

	static testMethod void UserModel_CS_ChangePassword(){
		init();
		
		Test.startTest();
		
		String newPassword = '!321CBAcba';
		
		System.assert(user.changePassword(newPassword), 'Password change failed.');
		
		Test.stopTest();
	}

	static testMethod void UserModel_CS_ResetPassword(){
		init();
		
		Test.startTest();
		
		System.assert(user.resetPassword(), 'Password reset failed.');
	
		Test.stopTest();
	}
	
	static testMethod void UserModel_CS_IsCurrentPassword(){
		init();
		
		Test.startTest();
		
		//Check against the default password
		System.assert(user.isCurrentPassword('abcABC123!'), 'User does not have default password.');
	
		Test.stopTest();
	}
	
	static testMethod void UserModel_CS_SendUsernameTo(){
		init();
		
		Test.startTest();
		
		System.assert(user.sendUsernameTo(),'Sending username email failed');
		
		Test.stopTest();
	}
	
	static testMethod void UserModel_CS_SendPasswordTo(){
		init();
		
		Test.startTest();
		
		String newPassword = '!321CBAcba';
		
		System.assert(user.changePassword(newPassword),'Change password before send failed.');
		System.assert(user.sendPasswordTo(newPassword),'Password email failed to send.');
	
		Test.stopTest();
	}
	
	static testMethod void UserModel_CS_SendEmailTo(){
		init();		
	
		Test.startTest();
	
		System.assert(user.sendEmailTo('TestSubject','TestBody'),'Sending email failed.');
	
		Test.stopTest();
	}
	
	static testMethod void UserModel_CS_DeactivateUser(){
		init();
		
		Test.startTest();
		
		user.deactivate();
		
		System.assert(user.instance.Active__c == false, 'User not deactivated.');
	
		Test.stopTest();
	}

	static testMethod void UserModel_CS_Misc(){		
		init();

		Test.startTest();

		System.assert(user.AccountName != '', 'Invalid AccountName returned.');
		System.assert(user.AccountType != '', 'Invalid AccountName returned.');
	
		Test.stopTest();
	}

    static void init(){
    	
    	///////////////////////////////////
		// Data
		///////////////////////////////////
    	
    	//Providers
		providers = TestDataFactory_CS.generateProviders('TestProvider', 1);
		insert providers;
		
		//Provider users, set user roles/profiles
		providerUsers = TestDataFactory_CS.generateProviderContacts('ProviderUser', providers, 1);
		insert providerUsers;

		user = new UserModel_CS(providerUsers[0]);
    }
}