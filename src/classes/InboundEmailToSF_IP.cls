/* The Inbound Email class is created by IP Developers to track down all emails received by Integra. The e-mails will be forwarded to Integra's Salesforce E-mail account, which
be interpreted by this Inbound Email class and stored in Email Log object.*/
/**
* Email services are automated processes that use Apex classes
* to process the contents, headers, and attachments of inbound
* email.
*/
global class InboundEmailToSF_IP implements Messaging.InboundEmailHandler {
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        String emailBody= '';   
        // String emailSender = email.fromname.substring(0,email.fromname.indexOf(' '));
        // Integer stringResult = emailSender.length();
        String fName;
        String lName;
        emailBody = email.plainTextBody;  
        List<Email_Log__c> newMail = new List<Email_Log__c>();
        List<Contact> excon = new List<Contact>();
        String fromAddress = email.fromAddress;
        String[] toAddresses = email.toAddresses;
        System.debug('Size of toAddress' + toAddresses.size());
        String allToAdd = '';
        String ccAdd = '';
        if (toAddresses.size() > 0){
            System.debug('Entering split for to address');
            UtilityClassForIP toSplit = new UtilityClassForIP();
            allToAdd = toSplit.StringSplitter(toAddresses);
            System.debug('allToADD = '+allToAdd);
        }
        if (email.ccAddresses != null){
            String[] ccAddresses = email.ccAddresses;
            System.debug('Size of cc' +ccAddresses.size());
            if (ccAddresses.size() > 0){
                System.debug('Entering split for cc address');
                UtilityClassForIP splitter = new UtilityClassForIP();
                ccAdd = splitter.StringSplitter (ccAddresses);
            }  
        }
        
        String reference = email.inReplyTo;
        String newMailId = '';
        String newEmailId = '';
        Boolean isSuccess = false;
        String accountId;
        String contactId;
        // Yahia added
        String status;
        String toEmailAddress;
        // Yahia - have to split handling claims emails vs everything else
        //List <Contact>  con = [Select Id, Email, AccountId from Contact where Email =: fromAddress];
        
        boolean claims = false;
        boolean clear = false;
        for(String i : toAddresses){
            if(i.containsIgnoreCase('claims')){
                claims = true;
            }
            if(i.containsIgnoreCase('clear')){
                clear = true;
            }
        }
        // Yahia - If email is to claims - then do the non-credentialed provider filtering. Else, the old code
        if(claims == True && clear == false){
            toEmailAddress = 'claims@accessintegra.com';
            List <Contact>  con = [Select Id, Email, AccountId, RecordType.Name from Contact where Email =: fromAddress AND Status__c = 'Active' AND 
                                   (RecordType.Name = 'Provider' OR RecordType.Name = 'Integra Roster') ORDER BY RecordType.Name DESC];
            System.debug ('The number of contacts quried is'+con.size());
            if(con.size() > 0){
                if (con[0].RecordType.Name != 'Integra Roster'){
                	accountId = con[0].AccountId;
                	contactId = con[0].Id;
                	// Yahia added
                } else {
                    accountId = Null;
                    contactId = Null;
                }
                status = 'Open';
            } else{
                // Yahia added
                status = 'Non-Credentialed';
                accountId = NULL;
                contactId = NULL;
                // Yahia - No need to create email contact... just dump into a new bucket called Non-Credentialed
            }
        } else {
            toEmailAddress = 'clearhelp@accessintegra.com';
            List <Contact>  con = [Select Id, Email, AccountId from Contact where Email =: fromAddress];
            System.debug ('The number of contacts quried is'+con.size());
            if(con.size() > 0){
                accountId = con[0].AccountId;
                contactId = con[0].Id;
                // Yahia added
                status = 'Open';
            } else {
                // Yahia added
                status = 'Open';
                List<Account> a = [Select Id, Name from Account where Name LIKE 'Integra Partners%' LIMIT 1];
                if (a.size() > 0) {
                    Contact newContact = new Contact();
                    newContact.FirstName = 'Email';
                    newContact.LastName = 'Contact';
                    newContact.AccountId = a[0].Id;
                    newContact.Email = fromAddress;
                    insert newContact;
                    accountId = newContact.AccountId;
                    contactId = newContact.Id;
                }
            }
        }
            // Yahia - added status
            /*
Email_Log__c addMail = new Email_Log__c(From_Email_Address__c = fromAddress, Email_Subject__c = email.Subject, Email_Content__c = emailBody, Message_Id__c = email.messageId, 
Email_Account__c = accountId, Email_Contact__c = contactId, To_Email_Address__c = toAddresses[0], All_To_Addresses__c = allToAdd,
Email_Reference_Id__c = reference, CC_Addresses__c = ccAdd);*/
            
            Email_Log__c addMail = new Email_Log__c(From_Email_Address__c = fromAddress, Email_Subject__c = email.Subject, Email_Content__c = emailBody, Message_Id__c = email.messageId, 
                                                    Email_Account__c = accountId, Email_Contact__c = contactId, To_Email_Address__c = toEmailAddress, All_To_Addresses__c = allToAdd,
                                                    Email_Reference_Id__c = reference, CC_Addresses__c = ccAdd, Status__c = status);
            newMail.add(addMail);
            
            
            
            if (newMail.size() > 0){
                System.debug('Entering mail insert');
                try{
                    Database.SaveResult[] srList = Database.insert(newMail, false);
                    for (Database.SaveResult dsr : srList) {
                        if (dsr.isSuccess()) {
                            // Operation was successful, so get the ID of the record that was processed
                            System.debug('Successfully inserted email. Email ID: ' + dsr.getId());
                            isSuccess = true;
                            newMailId = dsr.getId();
                        } else {
                            // Operation failed, so get all errors                
                            for(Database.Error err : dsr.getErrors()) {
                                System.debug('The following error has occurred.');                    
                                System.debug(err.getStatusCode() + ': ' + err.getMessage());
                                System.debug('Email Log fields that affected this error: ' + err.getFields());
                            }
                            //insert newMail;   
                            // newMailId = newMail[0].Id;
                            //  System.debug('Email Inserted. Job successful ' + newMail);   
                        }
                    } 
                }
                catch (System.Dmlexception e) 
                {
                    System.debug('Email not inserted. Job unsuccessful ' + e.getMessage());
                }      
                
                if (isSuccess == true && email.binaryAttachments != null && email.binaryAttachments.size() > 0) {
                    for (integer i = 0 ; i < email.binaryAttachments.size() ; i++) {
                        Attachment attachment = new Attachment();
                        // attach to the newly created email record
                        attachment.ParentId = newMailId;
                        attachment.Name = email.binaryAttachments[i].filename;
                        attachment.Body = email.binaryAttachments[i].body;
                        insert attachment;
                    }
                }
                
            } 
            
            result.success = true;
            return result;
        }
    }