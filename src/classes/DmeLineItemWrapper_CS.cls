public class DmeLineItemWrapper_CS {
		
		// IMPORTANT: Do not change the names of these variables. They match column names in the Heroku database.
		private String sfid;
		private String orderC;
		private String dmeC;
		private Double quantityC;
		private String productCodeC;

		public DmeLineItemWrapper_CS(Id newOrder, DME_Line_Item__c dmeLineItem) {
			this.sfid = dmeLineItem.Id;
			this.orderC = newOrder;
			this.dmeC = dmeLineItem.DME__c;
			this.quantityC = dmeLineItem.Quantity__c;
			this.productCodeC = dmeLineItem.Product_Code__c;
		}

		public DmeLineItemWrapper_CS(DME_Line_Item__c dmeLineItem) {
			this.sfid = dmeLineItem.Id;
			this.orderC = dmeLineItem.Order__c;
			this.dmeC = dmeLineItem.DME__c;
			this.quantityC = dmeLineItem.Quantity__c;
			this.productCodeC = dmeLineItem.Product_Code__c;
		}
	}