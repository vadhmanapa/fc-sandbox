@isTest
private class PayerUsersSummary_CS_Test {

	/******* Test Parameters *******/
	static private final Integer N_USERS = 15;

	/******* Provider *******/
	static private List<Account> payers;
	static private List<Contact> payerUsers;
	static private List<Patient__c> patients;

	/******* Test Objects *******/
	static private PayerUsersSummary_CS usersSummary;

	/******* Test Methods *******/

	static testMethod void PayerUsersSummary_Test_PageLanding() {
		init();
		
		Test.startTest();
		
		//Check initial state of page components
		System.assert(usersSummary.userListSetCon.getPageSize() == 10, 'userListSetCon pageSize ' + 
			usersSummary.userListSetCon.getPageSize() + ' != 10');
		
		System.assert(usersSummary.userListSetCon.getPageNumber() == 1, 'userListSetCon pageNumber ' + 
			usersSummary.userListSetCon.getPageSize() + ' != 1');
			
		System.assert(usersSummary.userListSetCon.getResultSize() == N_USERS - 1, 'userListSetCon resultSize ' + 
			usersSummary.userListSetCon.getResultSize() + ' != ' + (N_USERS - 1));
		
		System.assert(usersSummary.userListRecords.size() == usersSummary.pageSize, 'userList size ' + 
			usersSummary.userListRecords.size() + ' != ' + usersSummary.pageSize);		
			
		Test.stopTest();	
	}

    static testMethod void PayerUsersSummary_Test_Pagination() {
        init();
        
       	Test.startTest();

       	usersSummary.userListSetCon.next();
       	
       	System.assert(usersSummary.userListSetCon.getPageNumber() == 2, 'userListSetCon pageNumber ' + 
			usersSummary.userListSetCon.getPageSize() + ' != 2');
       	
		usersSummary.pageSize = 20;
		
		System.assert(usersSummary.userListSetCon.getPageSize() == 20, 'userListSetCon pageSize ' + 
			usersSummary.userListSetCon.getPageSize() + ' != 20');
       	
       	Test.stopTest();
    }
    
    static testMethod void PayerUsersSummary_Test_SearchUsers(){
    	init();
    	
    	Test.startTest();
    	
    	usersSummary.searchString = 'Test1';
    	usersSummary.searchUsers();
    	
    	for (Contact u : usersSummary.userListRecords){
    		System.Assert(u.Name.containsIgnoreCase('Test1') || u.Username__c.containsIgnoreCase('Test1') || u.Email.containsIgnoreCase('Test1'), 'Not all records contain \'Test1\'');
    	}
    	
    	usersSummary.searchString = 'NonsenseString13297ry8q0oephrg938qad';
    	usersSummary.searchUsers();
    	
    	System.assert(usersSummary.UserListRecords.size() == 0, 'Search let in bad records');
    	
    	Test.stopTest();
    	
    }
    
    static testMethod void PayerUsersSummary_Test_SendUsername(){
    	init();
    	
    	Test.startTest();
    	
    	usersSummary.selectedUserId = payerUsers[1].Id;
    	usersSummary.selectedUser = new UserModel_CS(payerUsers[1]);
    	System.assert(usersSummary.selectedUser != null, 'No selected user context.');
    	
    	usersSummary.sendUsername();
    	
    	Test.stopTest();
    }
    
    static testMethod void ProviderUsersSummary_Test_ResetPassword(){
    	init();
    	
    	Test.startTest();
    	
    	usersSummary.selectedUserId = payerUsers[1].Id;
    	usersSummary.selectedUser = new UserModel_CS(payerUsers[1]);
    	System.assert(usersSummary.selectedUser != null, 'No selected user context.');
    	
    	String oldPassword = usersSummary.selectedUser.instance.Password__c;
    	
    	usersSummary.resetPassword();
    	
      	//Re-query the user
    	List<Contact> deactivatedUsers = [select Id,Password__c from Contact where Id = :usersSummary.selectedUserId limit 1];  	
    	
    	System.assert(deactivatedUsers.size() == 1, 'More than one user returned');
    	System.assert(deactivatedUsers[0].Password__c != oldPassword, 'Password not reset.');
    	
    	Test.stopTest();
    }
    
    static testMethod void ProviderUsersSummary_Test_DeactivateUser(){
    	init();
    	
    	Test.startTest();
    	
    	usersSummary.selectedUserId = payerUsers[1].Id;
    	usersSummary.selectedUser = new UserModel_CS(payerUsers[1]);
    	
    	System.assert(usersSummary.selectedUser != null, 'No selected user context.');
    	
    	usersSummary.deactivateUser();
    	
    	//Re-query the user
    	List<Contact> deactivatedUsers = [select Id,Active__c from Contact where Id = :usersSummary.selectedUserId limit 1];
    	
    	System.assert(deactivatedUsers.size() == 1, 'More than one user returned');
    	System.assert(deactivatedUsers[0].Active__c == false, 'Selected user is still active.');
    	
    	Test.stopTest();
    }
    
	static void init(){
    	
    	///////////////////////////////////
		// Data
		///////////////////////////////////
    	
    	//Patients
		patients = TestDataFactory_CS.generatePatients('TestPatient', 1);
		insert patients;
    	
    	//Payers
		payers = TestDataFactory_CS.generatePlans('TestPayer', 1);
		insert payers;
		
		//Provider users, set first user to Admin
		payerUsers = TestDataFactory_CS.createPlanContacts('PayerUser', payers, N_USERS);
		payerUsers[0].Profile__c = 'Admin';
		update payerUsers;

		///////////////////////////////////
		// Page Landing
		///////////////////////////////////

		//Set the page and any parameters
		Test.setCurrentPage(Page.PayerUsersSummary_CS.setRedirect(true));
		
		//Log the desired user in
		TestServices_CS.login(payerUsers[0]);
    	
    	//Land on the page
    	usersSummary = new PayerUsersSummary_CS();
    	usersSummary.init();
    }
}