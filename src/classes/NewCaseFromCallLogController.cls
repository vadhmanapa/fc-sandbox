public class NewCaseFromCallLogController {

    //Because it's easy
    Map<String,String> pageParams = ApexPages.currentPage().getParameters();
    public Boolean inError {get;set;}
    
    public Case newCase {get;set;}
    public Contact caseContact {get;set;}
    
    public Call_Log__c callLog {get;set;}
    public Call_Log_Case__c newCallLogCase{get;set;}
    
    public String newCaseType {
		get{
            if(RecordTypes.recordTypesById.containsKey(newCase.RecordTypeId)) {
				return RecordTypes.recordTypesById.get(newCase.RecordTypeId).Name;
            }
            return '';
		}
	}
    
    public NewCaseFromCallLogController() {
        
        //Reset page state vars
        inError = false;
        
        //New Case
        newCase = new Case(
        	Origin = 'Phone',
            OwnerId = UserInfo.getUserId(),
			RecordTypeId = RecordTypes.defaultCaseId,
			Priority = 'Standard'
        );
        
        //New connection between call log and case
        newCallLogCase = new Call_Log_Case__c();
        
        //Check for Log Id
        if(pageParams.containsKey('logId') && String.isNotBlank(pageParams.get('logId'))) {
            
            Id callLogId = pageParams.get('logId');
            
            callLog = [
                SELECT 
                	Id, 
                	Name,
                    Caller_Contact__c ,
                    Call_Reason__c, 
                    Call_Reason_Detail__c,
                    Call_Result_Description__c, 
                    Patient__c, 
                    Patient_Payor__c
                FROM Call_Log__c
                WHERE Id = :callLogId
            ];
            
            //Update connector
            newCallLogCase.Call_Log__c = callLogId;
            
            //Copy info
            CaseUtils.CopyFromCallLog(callLog, newCase);
        } else {
            PageUtils.addError('Missing Call Log Id in URL!');
            inError = true;
        }
    }
    
    public void updateCaseContact() {
		
		caseContact = [
			SELECT AccountId,Phone,MobilePhone,Email
			FROM Contact
			WHERE Id = :newCase.ContactId
		];
		
		CaseUtils.CopyFromContact(caseContact, newCase);
		
		if (String.isBlank(newCase.SuppliedPhone) || 
			String.isBlank(newCase.SuppliedEmail))
		{
			PageUtils.addWarning('The Contact record is missing information (Phone, Email) that' +
				' is vital for recontact and notifications. Please enter the missing information manually.');
		}
	}

    public PageReference cancel() {
        PageReference ref = new PageReference('/' + callLog.Id);        
        return ref;
    }
    
	public PageReference save() {
		
		if(newCase.ContactId == null) {
			newCase.ContactId.addError('Please enter a Contact');
			return null;
		}
		
		if(String.isBlank(newCase.Subject)) {
			newCase.Subject.addError('Please enter a Subject');
			return null;
		}
		
		Database.DMLOptions dmo = new Database.DMLOptions();
		dmo.emailHeader.triggerUserEmail = true;
		dmo.assignmentRuleHeader.useDefaultRule = true;
		
		if(String.isNotBlank(newCase.SuppliedEmail)) {
			dmo.emailHeader.triggerAutoResponseEmail = true;
			dmo.emailHeader.triggerOtherEmail = true;
		}
			
		newCase.setOptions(dmo);
		
		try {
			insert newCase;
            
            newCallLogCase.Case__c = newCase.Id;
            insert newCallLogCase;
            
		} catch(Exception e) {
			return null;
		}
		
		return new PageReference('/' + newCase.Id);
	}
}