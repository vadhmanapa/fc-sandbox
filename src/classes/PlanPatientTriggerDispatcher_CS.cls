/**
 *  @Description Dispatcher class for the trigger on Plan Patient
 *  @Author Cloud Software LLC
 *  Revision History: 
 *		12/14/2015 - karnold - ISSCI-167 - Added process to check for duplicate plan patients. And activated trigger.
 *		02/16/2016 - karnold - Fromatted code.
 *
 */
 public without sharing class PlanPatientTriggerDispatcher_CS {
	public PlanPatientTriggerDispatcher_CS(Map<Id, SObject> oldMap, Map<Id, SObject> newMap, List<SObject> triggerOld, List<SObject> triggerNew, Boolean isInsert, Boolean isUpdate, Boolean isDelete, Boolean isUndelete, Boolean isBefore, Boolean isAfter) {
		if (isBefore) {
			System.debug('Is Before');
			if (isInsert) { doBeforeInsert(triggerNew); }
			else if (isUpdate) { doBeforeUpdate(oldMap, newMap, triggerOld, triggerNew); }
			else if (isDelete) { doBeforeDelete(oldMap, triggerOld); }
		} else if (isAfter) {
			System.debug('Is After');
			if (isInsert) { doAfterInsert(newMap, triggerNew); }
			else if (isUpdate) { doAfterUpdate(oldMap, newMap, triggerOld, triggerNew); }
			else if (isDelete) { doAfterDelete(oldMap, triggerold); }
			else if (isUndelete) { doAfterUnDelete(newMap, triggerNew); }
		}
	}

	public void doBeforeInsert(List<SObject> triggerNew) {
		populateName(triggerNew);

		//Map<String, Plan_Patient__c> planPatientMap = populatePPMap(triggerNew);
		//populateLatLng(planPatientMap);
		blockDuplicatePlanPatients(triggerNew);
	}
	public void doBeforeUpdate(Map<Id, SObject> oldMap, Map<Id, SObject> newMap, List<SObject> triggerOld, List<SObject> triggerNew) {
		populateName(triggerNew);

		Map<String, Plan_Patient__c> addressChangePlanPatients = getAddressChangePPs(oldMap, triggerNew);
		//populateLatLng(addressChangePlanPatients);
	}
	public void doBeforeDelete(Map<Id, SObject> oldMap, List<SObject> triggerOld) {

	}
	public void doAfterInsert(Map<Id, SObject> newMap, List<SObject> triggerNew) {

	}
	public void doAfterUpdate(Map<Id, SObject> oldMap, Map<Id, SObject> newMap, List<SObject> triggerOld, List<SObject> triggerNew) {

	}
	public void doAfterDelete(Map<Id, SObject> oldMap, List<SObject> triggerOld) {

	}
	public void doAfterUndelete(Map<Id, SObject> newMap, List<SObject> triggerNew) {

	}

	// Helper Methods //

	public void populateName(List<SObject> triggerNew) {
		for (SObject so : triggernew) {
			Plan_Patient__c p = (Plan_Patient__c) so;
			if (p.Last_Name__c != null) {
				if (p.First_Name__c != null) {
					p.Name = p.First_Name__c + ' ' + p.Last_Name__c;
				} else {
					p.Name = p.Last_Name__c;
				}
			}
		}
	}

	// Prevent Duplication Methods //

	/**
	 * @description Populates a list with any Plan Patients that match either the id number or name and date of birth.
	 */
	private static List<Plan_Patient__c> populatePotentialPatientDuplicates(List<SObject> triggerNew) {
		Set<String> idNumberSet = new Set<String> ();
		Set<String> nameSet = new Set<String> ();
		Set<Date> dobSet = new Set<Date> ();

		for (Plan_Patient__c pp : (List<Plan_Patient__c>) triggerNew) {
			idNumberSet.add(pp.ID_Number__c);
			nameSet.add(pp.Name);
			dobSet.add(pp.Date_of_Birth__c);
		}

		return [
			SELECT
				Name,
				Date_of_Birth__c,
				ID_Number__c,
				Health_Plan__c
			FROM Plan_Patient__c
			WHERE ID_Number__c IN : idNumberSet
			OR(Name IN : NameSet AND Date_of_Birth__c IN : dobSet)
		];
	}

	/**
	 * @description Adds an error to any records that match a record already in the database.
	 *	Duplicates are defined as:
	 *		1) in the same health plan network AND both
	 *		2) Having the same ID number that is NOT blank OR
	 *		3) Having the same name and date of birth.
	 */
	private static void blockDuplicatePlanPatients(List<SObject> triggerNew) {
		List<Plan_Patient__c> duplicatesList = populatePotentialPatientDuplicates(triggerNew);

		Map<String, Id> idNumberToHealthPlanMap = new Map<String, Id> ();
		Map<String, Id> nameDobToHealthPlanMap = new Map<String, Id> ();
		for (Plan_Patient__c pp : duplicatesList) {
			idNumberToHealthPlanMap.put(pp.ID_Number__c, pp.Health_Plan__c);
			nameDobToHealthPlanMap.put(pp.Name + String.valueOf(pp.Date_of_Birth__c), pp.Health_Plan__c);
			System.debug(pp);
		}

		for (Plan_Patient__c pp : (List<Plan_Patient__c>) triggerNew) {
			if (String.isNotBlank(pp.ID_Number__c) &&
					idNumberToHealthPlanMap.containsKey(pp.ID_Number__c) &&
					idNumberToHealthPlanMap.get(pp.ID_Number__c) == pp.Health_Plan__c) {

				pp.addError('ID#: A patient already exists with this ID number.\n' +
						'Please confirm the ID you entered is correct then search again to locate the existing patient in the system');
			}
			if (nameDobToHealthPlanMap.containsKey(pp.Name + String.valueOf(pp.Date_of_Birth__c)) &&
					nameDobToHealthPlanMap.get(pp.Name + String.valueOf(pp.Date_of_Birth__c)) == pp.Health_Plan__c) {

				pp.addError('Patient Name and Date Of Birth: A patient already exists with this name and date of birth.\n' +
						'Please confirm the name and DOB you entered are correct then search again to locate the existing patient in the system.');
			}
		}
	}

	// GeoLocation Service Methods //

	public Map<String, Plan_Patient__c> populatePPMap(List<SObject> triggerNew) {
		Map<String, Plan_Patient__c> ppMap = new Map<String, Plan_Patient__c> ();
		integer i = 0;
		for (SObject so : triggerNew) {
			Plan_Patient__c pp = (Plan_Patient__c) so;
			if (pp.Id != null) {
				ppMap.put(pp.Id, pp);
			} else {
				ppMap.put(String.valueOf(i), pp);
				i++;
			}
		}
		return ppMap;
	}

	public Map<String, Plan_Patient__c> getAddressChangePPs(Map<Id, SObject> oldMap, List<SObject> triggerNew) {
		Map<String, Plan_Patient__c> addressChangePPs = populatePPMap(triggerNew);
		for (Plan_Patient__c newPP : addressChangePPs.values()) {
			Plan_Patient__c oldPP = (Plan_Patient__c) oldMap.get(newPP.Id);
			String oldAddress = (oldPP.Street__c != null ? oldPP.Street__c : '') + (oldPP.City__c != null ? oldPP.City__c : '') + (oldPP.State__c != null ? oldPP.State__c : '') + (oldPP.Zip_Code__c != null ? oldPP.Zip_Code__c : '');
			String newAddress = (newPP.Street__c != null ? newPP.Street__c : '') + (newPP.City__c != null ? newPP.City__c : '') + (newPP.State__c != null ? newPP.State__c : '') + (newPP.Zip_Code__c != null ? newPP.Zip_Code__c : '');
			if (oldAddress == newAddress) {
				addressChangePPs.remove(newPP.Id);
			}
		}
		return addressChangePPs;
	}

	public void populateLatLng(Map<String, SObject> planPatientMap) {
		/* Error: Geocoding currently not in place for Plan Patient
		  List<GeocodingModel_CS> geoModels = new List<GeocodingModel_CS>();
		  for(String key : planPatientMap.keySet()) {
		  Plan_Patient__c pp = (Plan_Patient__c)planPatientMap.get(key);
		  GeocodingModel_CS geoModel = new GeocodingModel_CS();
		  geoModel.recordId = key;
		  geoModel.street = pp.Street__c;
		  geoModel.city = pp.City__c;
		  geoModel.state = pp.State__c;
		  geoModel.postalCode = pp.Zip_Code__c;
		  geoModels.add(geoModel);
		  }
		  if(geoModels.size() > 0) {
		 
		  GeocodingUtils_CS.GeocodeAddresses(geoModels);
		 
		  for(String key : planPatientMap.keySet()) {
		  for(GeocodingModel_CS geoModel : geoModels) {
		  if(key == geoModel.recordId) {
		  Plan_Patient__c pp = (Plan_Patient__c)planPatientMap.get(key); 
		  pp.Geolocation__Latitude__s = geoModel.lat;
		  pp.Geolocation__Longitude__s = geoModel.lng;
		  break;
		  }
		  }
		  }
		  }
		 */
	}
}