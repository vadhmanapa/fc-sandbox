@isTest
global with sharing class PublicKnowledgeCalloutMock implements HttpCalloutMock {

	global static Boolean success = false;

	global HTTPResponse respond(HTTPRequest req){
		
		HttpResponse res = new HttpResponse();
		res.setBody('foo<LoginKey>www.test.com</LoginKey>bar');
		if (success){
			res.setStatusCode(200);
		} else {
			res.setStatusCode(400);
		}
		return res;
	}

}