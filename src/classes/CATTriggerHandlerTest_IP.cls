@isTest
private class CATTriggerHandlerTest_IP {
	
	@isTest static void testCATToAccount() {
		
		Profile p = [Select id from Profile where Name ='Credentialing Team'];
		User u = new User(LastName='User', 
						  Alias='use',
						  Email='testuser@test.com',
						  Username='testingtriggeruser@testingcenter.com',
						  CommunityNickname='testing',
						  EmailEncodingKey='UTF-8',
						  LanguageLocaleKey='en_US',
						  LocaleSidKey='en_US',
						  ProfileId = p.Id,
						  TimeZoneSidKey='America/New_York');
		insert u;

		Round_Robin__c rr = new Round_Robin__c(User__c = u.Id,
									Function__c = 'Credentialing CATs',
									Active__c = True);
		insert rr;
		
		Cred_Trigger_CS__c CTCS = new Cred_Trigger_CS__c();
		CTCS.Active__c = True;
		insert CTCS;

		System.runAs(u){

			// create a test account
			Map<String, Id> rTypeMap = new Map<String, Id>();

			Account testAcc = new Account(Name='Test Account', 
						        Type_Of_Provider__c = 'DME');
			insert testAcc;
            
            Contact testCon = new Contact(AccountId = testAcc.Id, FirstName = 'Test', LastName = 'Audit', Email = 'test@test.com');
            insert testCon;

			for(RecordType rt: [Select Id, Name from RecordType where sObjectType = 'CAT__c']) {
				rTypeMap.put(rt.Name, rt.Id);
			}
            
            System.debug('Number od Record types'+rTypeMap.size());

			// Case 1: Initial Credentialing

			CAT__c iniCred = new CAT__c(Account__c = testAcc.Id, Application_Completed_Date__c = System.today(), New_Application_Received__c = System.today(),
										Credentialing_Status__c = 'Ready for triage', RecordTypeId = rTypeMap.get('Credentialing Application'), Application_Type__c = 'Initial Credentialing',
										Missing_Documentation__c = 'Accreditation');
			insert iniCred;

			Account iniCredTest = [Select Id, Name, Status__c, Credentialing_Status__c, Application_Completed_Date__c, Application_Received__c from Account where Id =:testAcc.Id];

			System.assertEquals('Pending Credentialing', iniCredTest.Status__c, 'Status was updated');
			System.assertEquals('Ready for triage', iniCredTest.Credentialing_Status__c, 'Cred Status was updated');
			System.assertNotEquals(null, iniCredTest.Application_Completed_Date__c, 'Date was filled');

			iniCred.Credentialing_Status__c = 'In Triage';
			update iniCred;
		

		  	Attachment attachment = new Attachment();
	  		attachment.Body = Blob.valueOf('aaaa');
			attachment.Name = String.valueOf('test.txt');
		    attachment.ParentId = iniCred.Id; 
			insert attachment;
			
			iniCred.Credentialing_Status__c = '1st document submission form sent';
			iniCred.Provider_Contact__c = testCon.Id;
			update iniCred;

			Case iniCase = [Select Id, ParentId, Reason from Case where CAT__c =: iniCred.Id];
			//Insert incoming email message
			EmailMessage emOutgoing = new EmailMessage(
			ParentId = iniCase.Id,
			Incoming = True 
		);
		insert emOutgoing;

			//CAT__c iniCred2 = [Select Id, Credentialing_Status__c,Missing_Documentation__c,Provider_Contact__c, Case_Created__c from CAT__c where Account__c  =: testAcc.Id LIMIT 1];
			//iniCred2.Credentialing_Status__c = 'Document Submission form sent';
			//iniCred2.Missing_Documentation__c = 'Billing Contact';
   //         iniCred2.Provider_Contact__c = testCon.Id;
   //         iniCred2.Application_Status_Comments__c = 'Testing IniCred2';
			//update iniCred2;

			//CAT__c iniCredCaseCreated = [Select Id, Credentialing_Status__c,Missing_Documentation__c,Provider_Contact__c, Case_Created__c from CAT__c where Id =: iniCred2.Id LIMIT 1];
            
   //         Case testCaseForCred = [Select Id, CaseNumber, CAT__c, Missing_Documentation__c from Case where CAT__c =: iniCred2.Id LIMIT 1];
			//System.assertNotEquals(null, testCaseForCred, 'Case was created');
   //         System.assertEquals(true, iniCredCaseCreated.Case_Created__c, 'Case was created');

            //iniCred.Credentialing_Status__c = 'Ready for Data Entry';
            //update iniCred;

            //CAT__c iniCredTestDataEntry = [Select Id, Name, (SELECT id from Data_Entry__r) from CAT__c where Id=:iniCred.Id];
            //System.assertNotEquals(0, iniCredTestDataEntry.Data_Entry__r.size(),'New Data Entry created');

			//iniCred.Completed_Date__c = System.today();
   //         iniCred.Application_Status_Comments__c = 'Applcaiton Completed';
			//update iniCred;

			//Account iniCredTestUpdate = [Select Id, Name, Status__c, Credentialing_Status__c,Current_Credential_Date__c, Application_Completed_Date__c, Application_Received__c from Account where Id =:testAcc.Id];
			//System.assertNotEquals(null, iniCredTestUpdate.Current_Credential_Date__c, 'The date was updated');

			// Case 2: Recredentialing

			//CAT__c reCred = new CAT__c(Account__c = testAcc.Id, Recred_Application_Received__c = System.now(),Credentialing_Status__c = 'Application Received', 
			//						   RecordTypeId = rTypeMap.get('Recredentialing'), Completed_Date__c = System.today()+1 );
			//insert reCred;

			//Account reCredTest = [Select Id, Name, Status__c, Credentialing_Status__c,Current_Credential_Date__c, Application_Completed_Date__c, Application_Received__c from Account where Id =:testAcc.Id];
			//System.assertEquals(System.today()+1, reCredTest.Current_Credential_Date__c, 'The date was updated');

			//reCred.Recredentialing_Status__c = 'Ready for Data Entry';
			//update reCred;

			//CAT__c reCredTestDataEntry = [Select Id, Name, (SELECT id from Data_Entry__r) from CAT__c where Id=: reCred.Id];
			//System.assertNotEquals(0, reCredTestDataEntry.Data_Entry__r.size(),'New Records created');

			// Case 3: Close Account

			CAT__c clAcc = new CAT__c(Account__c = testAcc.Id, RecordTypeId = rTypeMap.get('Close Account'), Closed_Status_Reason__c = 'Economic reason / IP Cost (Provider)',
									  Closed_Term_Date__c = System.today(), Closed_Status__c = 'Closed/Integra Reason');
			insert clAcc;

			Account clAccTest = [Select Id, Name, Status__c, Close_Status_Reason__c, Status_Reason__c, Close_Term_Date__c from Account where Id =:testAcc.Id];
			System.assertEquals(clAcc.Closed_Status_Reason__c, clAccTest.Close_Status_Reason__c, 'The picklist was updated');
			System.assertEquals(clAcc.Closed_Term_Date__c, clAccTest.Close_Term_Date__c, 'The date was updated');
			System.assertEquals(clAcc.Closed_Status__c, clAccTest.Status__c, 'The picklist was updated');

			// Case 4: Reopen Account

			//CAT__c opAcc = new CAT__c(Account__c = testAcc.Id, RecordTypeId = rTypeMap.get('Reopen Account'), Credentialing_Status__c = 'Application Received');
			//insert opAcc;

			//Account opAccTest = [Select Id, Name, Status__c, Credentialing_Status__c from Account where Id =:testAcc.Id];
			//System.assertEquals('Pending Credentialing', opAccTest.Status__c, 'The status was updated');
			//System.assertEquals('Ready for triage', opAccTest.Credentialing_Status__c, 'The status was updated');

			//Case 5: Roster Update

			CAT__c rosUp = new CAT__c(Account__c = testAcc.Id, RecordTypeId = rTypeMap.get('Demographic Update'), Missing_Info_Contact_Date__c = System.today(), Update_Category__c = 'Fax Number');
			insert rosUp;

			Account rosUpTest = [Select Id, Name, Status__c, Missing_Info_Contact_Date__c from Account where Id =:testAcc.Id];
			System.assertEquals(rosUp.Missing_Info_Contact_Date__c, rosUpTest.Missing_Info_Contact_Date__c, 'The date was updated');
            
   //         CAT__c notMeeting = new CAT__c(Account__c = testAcc.Id, RecordTypeId = rTypeMap.get('Not Meeting Requirements'), Not_Meeting_Cred_Requirement_Reason__c = 'No Accreditation');
			//insert notMeeting;

			//Account notMeetingTest = [Select Id, Name, Status__c, Missing_Info_Contact_Date__c from Account where Id =:testAcc.Id];
			//System.assertEquals('Not Meet Credentialing Requirements', notMeetingTest.Status__c , 'The status was updated');
			
		}
	}
	
	
}