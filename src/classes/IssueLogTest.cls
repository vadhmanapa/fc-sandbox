@isTest
private class IssueLogTest {
    static testMethod void IssueLog(){
        
        // create test user
        string randomName = string.valueof(Datetime.now()).replace('-','').replace(':','').replace(' ','');
        String userName = randomName + 'testadfhcheck@test.com';
        Profile p = [Select id from Profile where Name ='Claims w Service Cloud'];
        User testUser = new User();
        testUser.FirstName = 'tesdfst';
        testUser.LastName = 'cheafgck';
        testUser.Username = userName;
        testUser.Email = randomName+userName;
        testUser.Alias = 'tefadhst';
        testUser.Team__c = 'Fadoo';
        testUser.CommunityNickname = 'testforemail';
        testUser.TimeZoneSidKey = 'America/Los_Angeles';
        testUser.EmailEncodingKey = 'ISO-8859-1';
        testUser.ProfileId = p.Id;
        testUser.LanguageLocaleKey = 'en_US';
        testUser.LocaleSidKey = 'en_US';
        testUser.Assign_Email__c = true;
        testUser.IsActive = true;
        insert testUser;
        String testUserId = testUser.Id;
        
        // create a test account - can be the provider or payor
        Account testAccount = new Account();
        testAccount.Name = 'Affinity';
        testAccount.OwnerId = testUser.ID;
        insert testAccount;
        
		Issue_Log__c il = new Issue_Log__c(Account__c = testAccount.ID);
        insert il;
        
        Issue_Log__c testil = [SELECT ID, touch_counter__c FROM Issue_Log__c WHERE id=: il.Id];
        system.assertEquals(1, testil.touch_counter__c, 'Should be 1');
        
        update il;
        
        testil = [SELECT ID, touch_counter__c FROM Issue_Log__c WHERE id=: il.Id];
        system.assertEquals(2, testil.touch_counter__c, 'Should be 1');
        
        il.Touch_counter__c = NULL;
        update il;
        
        testil = [SELECT ID, touch_counter__c FROM Issue_Log__c WHERE id=: il.Id];
        system.assertEquals(1, testil.touch_counter__c, 'Should be 1');
        
    }
}