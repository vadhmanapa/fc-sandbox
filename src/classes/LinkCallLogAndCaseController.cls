public class LinkCallLogAndCaseController {

    public String linkMode {get;set;}
    
    public String retURL {get;set;}
    
    public Call_Log_Case__c caseLogLink {get;set;}
    
    public void displayMessage(ApexPages.Severity severity, String message) {
		ApexPages.addMessage(new ApexPages.Message(severity, message));
    }
    
    public LinkCallLogAndCaseController() {
        
        Map<String,String> pageParams = ApexPages.currentPage().getParameters();
        
        caseLogLink = new Call_Log_Case__c();
        
        if(pageParams.containsKey('caseId') && String.isNotBlank(pageParams.get('caseId'))) {
            caseLogLink.Case__c = pageParams.get('caseId');
            retURL = '/' + pageParams.get('caseId');
        }
        
        if(pageParams.containsKey('logId') && String.isNotBlank(pageParams.get('logId'))) {
			caseLogLink.Call_Log__c = pageParams.get('logId');
			retURL = '/' + pageParams.get('logId');
        }
    }
    
    public PageReference link() {
        
        try {
			insert caseLogLink;
        } catch(Exception e) {
        	PageUtils.addError('Failed to link!');
            return null;
        }
        
        return new PageReference(retURL);
    }
    
    public PageReference cancel() {
        return new PageReference(retURL);
    }
}