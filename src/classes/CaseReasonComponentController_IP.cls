public with sharing class CaseReasonComponentController_IP extends ComponentControllerBase_IP {
    public String recordTypeId {get;set;}
    public String selectedCallerType {get;set;}
    public CaseReasonDetail__c crDetail {get;set;}
    public Case thisCase {get;set{
        if(value != null){
            thisCase = new Case();
            thisCase = value;
            if(!init){
                setAutoFillValues();
                init = true;
            }
        }
    }}
    public Boolean init {get{if (init == null) init = false; return init;}set;}

    public String reasonSelected {get
        {
            if(reasonSelected == null) {
                reasonSelected = '';
            }
            return reasonSelected;
        } set;
    }

    public List<SelectOption> getReasons(){
        List<SelectOption> options = new List<SelectOption>();
        options.clear();
        if(String.isNotBlank(recordTypeId)){
            String recordTypeName = (String)RecordTypes.recordTypesById.get(recordTypeId).Name;
            options.addAll(CaseReasonRelationshipHelper_IP.caseReasonPicklist(selectedCallerType , recordTypeName));
        }
        
        return options;
    }

    public String reasonDetailSelected {get
        {
            if(reasonDetailSelected == null) {
                reasonDetailSelected = '';
            }
            return reasonDetailSelected;
        } set;
    }

    public List<SelectOption> getReasonDetails(){
        List<SelectOption> options = new List<SelectOption>();
        options.clear();
        if(String.isNotBlank(recordTypeId)){
            String recordTypeName = (String)RecordTypes.recordTypesById.get(recordTypeId).Name;
            options.addAll(CaseReasonRelationshipHelper_IP.caseReasonDetailsPicklist(reasonSelected));
        }
        return options;
    }

    public void setAutoFillValues(){
        if(thisCase != null){
            if(String.isNotBlank(thisCase.CaseReasonId__c)){
                // apply the id value to reasonSelected
                reasonSelected = (Id) thisCase.CaseReasonId__c;
            }

            if(String.isNotBlank(thisCase.CaseReasonDetailId__c)){
                reasonDetailSelected = (Id) thisCase.CaseReasonDetailId__c;
                crDetail = [SELECT Id, CaseReasonDetail__c, Script__c, IsAdditionalDetailsRequired__c FROM CaseReasonDetail__c WHERE Id=: (Id) thisCase.CaseReasonDetailId__c LIMIT 1];
            }
        }
    }

    public void caseReasonProperties(){
        crDetail = new CaseReasonDetail__c();
        crDetail = CaseReasonRelationshipHelper_IP.reasonDetailList.containsKey(reasonDetailSelected) ? CaseReasonRelationshipHelper_IP.reasonDetailList.get(reasonDetailSelected) : null;
    }

    // Reset validation on Reason Detail when Case Reason is changed
    public void resetReasonDetailValidation(){
        reasonDetailSelected = '';
        crDetail = null;
        crDetail = new CaseReasonDetail__c();
    }

    public void setParentControllerValues(){
        if(String.isNotBlank(this.reasonSelected)){
            pageController.caseLog.caseDetails.CaseReason__c = CaseReasonRelationshipHelper_IP.getCaseReasonValue((Id)this.reasonSelected).Reason__c;
            pageController.caseLog.caseDetails.CaseReasonId__c = CaseReasonRelationshipHelper_IP.getCaseReasonValue((Id)this.reasonSelected).Id;
        }

        if(String.isNotBlank(this.reasonDetailSelected)){
            pageController.caseLog.caseDetails.CaseReasonDetail__c = CaseReasonRelationshipHelper_IP.getCaseReasonDetailValue((Id)this.reasonDetailSelected).CaseReasonDetail__c;
            pageController.caseLog.caseDetails.CaseReasonDetailId__c = CaseReasonRelationshipHelper_IP.getCaseReasonDetailValue((Id)this.reasonDetailSelected).Id;
        }
    }

    public Boolean validation(){
        if(String.isBlank(this.reasonSelected)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Case Reason is required to proceed'));
            return false;
        }

        if(String.isBlank(this.reasonDetailSelected)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Case Reason Details is required to proceed'));
            return false;
        }

        return true;
    }
}