@isTest
private class CopyProductToParentTest_IP {
	
	@isTest static void TestCopy() {
		// Implement test code
		Profile p = [Select id from Profile where Name ='Integra Standard User'];
		User u = new User(LastName='User', 
						  Alias='use',
						  Email='test@test.com',
						  Username='testingtrigger@testingcenter.com',
						  CommunityNickname='nick',
						  EmailEncodingKey='UTF-8',
						  LanguageLocaleKey='en_US',
						  LocaleSidKey='en_US',
						  ProfileId = p.Id,
						  TimeZoneSidKey='America/New_York');
		insert u;
		
		System.runAs(u){
			//create test lead
			Lead testLead = new Lead(LastName='Test last Name', 
			                     Company='Test Company',
			                     Status = 'Open', 
			                     Type__c = 'Provider',
			                     Type_of_Provider__c= 'DME');
		insert testLead;

		Account a = new Account(Name='Test Account', 
						        Type_Of_Provider__c = 'DME');
		insert a;
		//create test opportunity
		Opportunity testOpp = new Opportunity(Name = 'Test Lead Opp', 
											  AccountId = a.Id, 
											  CloseDate = System.today(), 
											  StageName='Open');
		insert testOpp;
		//create two product items
		
		Provider_Product_Identifier__c testProd1 = new Provider_Product_Identifier__c(Product_Name__c='Oxygen');
		insert testProd1;
		
		Provider_Product_Identifier__c testProd2 = new Provider_Product_Identifier__c(Product_Name__c='Diabetic');
		insert testProd2;

		// product items

		Provider_Product_Item__c testProvProd1 = new Provider_Product_Item__c(Lead__c = testLead.Id, Product_Identifier__c= testProd1.Id);
		insert testProvProd1;

		String prodAdd = testProd1.Product_Name__c;

		Lead checkCopyLead = [Select Id, Products_Serviced__c from Lead where Id=: testLead.Id];
		// System.assertEquals(checkCopyLead.Products_Serviced__c, prodAdd, 'The result is oxygen and was copied to Lead');

		// for opportunity

		testProvProd1.Opportunity__c = testOpp.Id;
		update testProvProd1;

		Opportunity checkCopyOpp = [Select Id, Products_Serviced__c from Opportunity where Id=: testOpp.Id];
		System.assertEquals(checkCopyOpp.Products_Serviced__c, prodAdd, 'The result was oxygen and was copied to Opportunity with update');

		Provider_Product_Item__c testProvProd2 = new Provider_Product_Item__c(Lead__c = testLead.Id, Product_Identifier__c= testProd2.Id);
		insert testProvProd2;

		prodAdd = testProd1.Product_Name__c + ',' +' '+ testProd2.Product_Name__c;

		Lead checkCopyLead2 = [Select Id, Products_Serviced__c from Lead where Id=: testLead.Id];
		//System.assertEquals(checkCopyLead2.Products_Serviced__c, prodAdd, 'Second item was added and updated');

		testProvProd2.Opportunity__c = testOpp.Id;
		update testProvProd2;

		Opportunity checkCopyOpp2 = [Select Id, Products_Serviced__c from Opportunity where Id=: testOpp.Id];
		System.assertEquals(checkCopyOpp2.Products_Serviced__c, prodAdd, 'The second item was added and updated');
		}
		
	}
	
	
	
}