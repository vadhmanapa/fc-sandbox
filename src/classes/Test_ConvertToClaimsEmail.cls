/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_ConvertToClaimsEmail {

    static testMethod void testCaseToEmail() {
      // create a case
      Account testProvider = new Account(Name = 'Test Claim Provider', Type_Of_Provider__c = 'DME');
        insert testProvider;
        String providerId = testProvider.Id;
        
       Contact testContact = new Contact(FirstName = 'Test',LastName = 'Contact', AccountId = providerId, Email = 'test@abc.com');
       insert testContact;
       String conId = testContact.Id;
       
       Case newCase = new Case(ContactId = conId, AccountId = providerId, Origin = 'Email', Subject = 'Test Email', Description = 'This is a test for Cases to Claims');
       insert newCase;
        String caseId = newCase.Id;
       
       newCase.Convert_to_Claims_Email__c = true;
       update newCase;
        
        List<Email_Log__c> testEmail = [Select Id, Case_Email_Id__c from Email_Log__c where Case_Email_Id__c =:caseId];
        for (Email_Log__c em : testEmail){
            System.assertEquals(1, testEmail.size(), 'A new Email was created');
        }
        
    }
}