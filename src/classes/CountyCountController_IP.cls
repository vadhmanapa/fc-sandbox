public with sharing class CountyCountController_IP {
	public static List<Opportunity> allOpps = new List<Opportunity>();
	public static Integer ilTotal = 0;

	public CountyCountController_IP() {

		allOpps = [Select Id, Name, CampaignId, AccountId, Corp_County__c from Opportunity where StageName = 'Received "Completed" App'];
	}

	public List<PieDataIllinois> getIllinoisData(){
		List<PieDataIllinois> data = new List<PieDataIllinois>();
		List<US_State_County__c> ilCounty = new List<US_State_County__c>();
		Set<String> countyNames = new Set<String>();
		Campaign c = [Select Id from Campaign where Name = 'Illinois Wellcare'];
		ilCounty = [Select Id, Name from US_State_County__c where State__c = 'Illinois'];
		Set<Id> allIlOpps = new Set<id>();
		for(Opportunity o : [Select Id from Opportunity where StageName = 'Received "Completed" App' AND CampaignId =: c.Id]){
			allIlOpps.add(o.Id);
		}
		List<County_Item__c> ilCountyCoverage = [Select Id, US_State_County__r.Name from County_Item__c where Opportunity__r.Id =:allIlOpps];


		if(c != null && ilCountyCoverage.size()>0) {
		   
		   for(County_Item__c ci : ilCountyCoverage){
	
		   		countyNames.add(ci.US_State_County__r.Name);
		   	
		   }
		   
		   if(countyNames.size() > 0){
		   
		   		data.add(new PieDataIllinois('Counties Covered', Integer.valueOf(countyNames.size()), Integer.valueOf(ilTotal)));
		   	  	
		   }
		   
		}
		
		return data;

	}

	public class PieDataIllinois{

		public Integer numCovered {get;set;}
		public Integer total {get; set;}
		public String name{get;set; }

		public PieDataIllinois(String name, Integer numCovered, Integer total){

			this.name = name;
			this.numCovered = numCovered;
			this.total = total;

		}
	}


	/****************************** California***********************************************/

	public List<PieDataCal> getCalData(){
		List<PieDataCal> data = new List<PieDataCal>();
		List<US_State_County__c> calCounty = new List<US_State_County__c>();
		Set<String> caCountyNames = new Set<String>();
		Campaign c = [Select Id from Campaign where Name = 'California Wellcare'];
		calCounty = [Select Id, Name from US_State_County__c where State__c = 'California'];
		Set<Id> allIlOpps = new Set<id>();
		for(Opportunity o : [Select Id from Opportunity where StageName = 'Received "Completed" App' AND CampaignId =: c.Id]){
			allIlOpps.add(o.Id);
		}
		List<County_Item__c> caCountyCoverage = [Select Id, US_State_County__r.Name from County_Item__c where Opportunity__r.Id =:allIlOpps];


		if(c != null && caCountyCoverage.size()>0) {
		   
		   for(County_Item__c ci : caCountyCoverage){
	
		   		caCountyNames.add(ci.US_State_County__r.Name);
		   	
		   }
		   
		   if(caCountyNames.size() > 0){
		   
		   		data.add(new PieDataCal('Counties Covered', Integer.valueOf(caCountyNames.size())));
		   	  	
		   }
		   
		}
		
		return data;

	}

	public class PieDataCal{

		public Integer numCovered {get;set;}
		public Integer total {get; set;}
		public String name{get;set; }

		public PieDataCal(String name, Integer numCovered){

			this.name = name;
			this.numCovered = numCovered;

		}
	}

	/******************************************Texas******************************************************/

	public List<PieDataTex> getTexData(){
		List<PieDataTex> data = new List<PieDataTex>();
		List<US_State_County__c> txsCounty = new List<US_State_County__c>();
		Set<String> txCountyNames = new Set<String>();
		Campaign c = [Select Id from Campaign where Name = 'Texas Wellcare'];
		txsCounty = [Select Id, Name from US_State_County__c where State__c = 'Texas'];
		Set<Id> allIlOpps = new Set<id>();
		for(Opportunity o : [Select Id from Opportunity where StageName = 'Received "Completed" App' AND CampaignId =: c.Id]){
			allIlOpps.add(o.Id);
		}
		List<County_Item__c> txCountyCoverage = [Select Id, US_State_County__r.Name from County_Item__c where Opportunity__r.Id =:allIlOpps];


		if(c != null && txCountyCoverage.size()>0) {
		   
		   for(County_Item__c ci : txCountyCoverage){
	
		   		txCountyNames.add(ci.US_State_County__r.Name);
		   	
		   }
		   
		   if(txCountyNames.size() > 0){
		   
		   		data.add(new PieDataTex('Counties Covered', Integer.valueOf(txCountyNames.size())));
		   	  	
		   }
		   
		}
		
		return data;

	}

	public class PieDataTex{

		public Integer numCovered {get;set;}
		public Integer total {get; set;}
		public String name{get;set; }

		public PieDataTex(String name, Integer numCovered){

			this.name = name;
			this.numCovered = numCovered;

		}
	}

	/************************************ Hawaii*********************************************/

	public List<PieDataHaw> getHawData(){
		List<PieDataHaw> data = new List<PieDataHaw>();
		List<US_State_County__c> hwCounty = new List<US_State_County__c>();
		Set<String> hwCountyNames = new Set<String>();
		Campaign c = [Select Id from Campaign where Name = 'California Wellcare'];
		hwCounty = [Select Id, Name from US_State_County__c where State__c = 'California'];
		Set<Id> allIlOpps = new Set<id>();
		for(Opportunity o : [Select Id from Opportunity where StageName = 'Received "Completed" App' AND CampaignId =: c.Id]){
			allIlOpps.add(o.Id);
		}
		List<County_Item__c> hwCountyCoverage = [Select Id, US_State_County__r.Name from County_Item__c where Opportunity__r.Id =:allIlOpps];


		if(c != null && hwCountyCoverage.size()>0) {
		   
		   for(County_Item__c ci : hwCountyCoverage){
	
		   		hwCountyNames.add(ci.US_State_County__r.Name);
		   	
		   }
		   
		   if(hwCountyNames.size() > 0){
		   
		   		data.add(new PieDataHaw('Counties Covered', Integer.valueOf(hwCountyNames.size())));
		   	  	
		   }
		   
		}
		
		return data;

	}

	public class PieDataHaw{

		public Integer numCovered {get;set;}
		public Integer total {get; set;}
		public String name{get;set; }

		public PieDataHaw(String name, Integer numCovered){

			this.name = name;
			this.numCovered = numCovered;

		}
	}

	/*************************************************************************************************************************

	Things to do:
	1. The value starts from Docusign - Try to figure out the access to Docusign
	2. SHould have a way to differentiate other acounts from Wellcare Lead Accounts ---> either mark it in Docusign or get the 
	accounts from D.Sign and query accounts and opportunity. To make easier, update CopyLeadDetails to update account Opportunity
	Campaign.
	3. Query - Select Id, Name, Opportunity Campaign (Select Id, Name, StageName, Campaign Name from Opportunity) from Account
	where Id=: docusign account
	4. Opportunity stage should be 'Receive Completed App'. If it passes all this steps, then include in the list. Based on the account state,
	get the county from US State County
	5. Compare the County in account to county in US State County ---> if matched, then include the county and get the number of county too.
	*************************************************************************************************************************/
}