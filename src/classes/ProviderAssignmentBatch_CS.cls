global without sharing class ProviderAssignmentBatch_CS implements Database.Batchable<SObject>, Database.Stateful {
	
	//Currently the reassignment process takes ~5 soql statements per record
	public static final Integer MAX_SOQL = 190;
	
	public static final String jobName = 'Order Expiration Batch';
	
	//Flag to indicate another batch process is required to process all records
	public Boolean spawnNewBatch = false;
	
	global Database.Querylocator start(Database.BatchableContext bc){
		System.debug('Begin start for batch.');
	
		//Assignment query
		SoqlBuilder assignmentQuery = new SoqlBuilder()
			.selectx('Id')
			.selectx('Active__c')
			.selectx('Order__c')
			.selectx('Provider__c')
			.selectx('Unassigned_At__c')
			.selectx('Unassignment__c')
			.fromx('Order_Assignments__r')
			.orderByx(new OrderBy('CreatedDate').descending());

		NestableCondition conditions = new AndCondition()
			.add(new FieldCondition('Expiration_Time__c').lessThan(DateTime.now()));
			
		SoqlBuilder query = new SoqlBuilder()
			.selectx('Id')
			.selectx('Name')
			.selectx('Provider__c')
			.selectx('Accepted_By__c')
			.selectx('Assigned_Date__c')
			.selectx('Accepted_Date__c')
			.selectx('Expiration_Time__c')
			.selectx('Status__c')
			.selectx('Plan_Patient__c')
			.selectx('Plan_Patient__r.Patient__c')
			.selectx('Assignment_History__c')
			.selectx('Geolocation__Latitude__s')
			.selectx('Geolocation__Longitude__s')
			.selectx('CreatedDate')
			.selectx(assignmentQuery)
			.fromx('Order__c')
			.wherex(conditions);
		
		String queryString = query.toSoql();
		
		System.debug('SB Query: ' + queryString);
		
		return Database.getQueryLocator(queryString);
	}
	
	global void execute(Database.BatchableContext bc, List<Order__c> expiredOrders){
		
		System.debug('Begin execute for batch, expiredOrders size: ' + expiredOrders.size());		

		//Deactivate all current provider assignments
		List<Order_Assignment__c> assignmentsToUpdate = new List<Order_Assignment__c>();
		
		for(Order__c o : expiredOrders){
			if(o.Order_Assignments__r.size() > 0){
				Order_Assignment__c mostRecentAssignment = o.Order_Assignments__r[0];
				
				System.debug('Deactivating assignment: ' + mostRecentAssignment.Id);
				
				mostRecentAssignment.Active__c = false;
				mostRecentAssignment.Unassignment__c = 'Expired';
				mostRecentAssignment.Unassigned_At__c = DateTime.now();
				
				assignmentsToUpdate.add(mostRecentAssignment);
			}
		}
		
		update assignmentsToUpdate;
		
		for(Order__c o : expiredOrders){

			//If we exceed the limit, set up a batch process and bail out
			if(Limits.getQueries() >= MAX_SOQL){						        
		        System.debug('Too many queries, setting up to spawn batch.');		        
		        //Database.executeBatch(new ProviderAssignmentBatch_CS());		        
		        spawnNewBatch = true;
		        break;
			}

			System.debug('Expired order: ' + o.Name);
			
			//Reset assignment-related values
			o.Provider__c = null;
			o.Accepted_By__c = null;
			o.Accepted_Date__c = null;
			o.Expiration_Time__c = null;
			o.Status__c = OrderModel_CS.STATUS_NEW;

            ProviderAssignmentServices_CS p = new ProviderAssignmentServices_CS(o);
			p.doAssignmentProcess();
		}
		
		update expiredOrders;
	}
	
	global void finish(Database.BatchableContext bc){
		System.debug('Begin finish for batch.');
		
		if(spawnNewBatch){
			System.debug('Spawning new batch.');
			Database.executeBatch(new ProviderAssignmentBatch_CS());
		}
	}
	
}