public without sharing class ProviderCreateNewUser_CS extends ProviderPortalServices_CS {

	private final String pageAccountType = 'Provider';
	
	/******* Error Messages *******/
	private final String nonUniqueUsernameError = 'Username already exists';
	private final String missingUsernameError = 'Please enter a unique username';
	public String usernameErrorMsg {get;set;}

	/******* Page Interface Params *******/
	public Contact newUser {get;set;}	
	public String username {get;set;}
	
	/******* Constructor/Init *******/
	public ProviderCreateNewUser_CS(){}
	
	// Default page action, add additional page action functionality here
	public PageReference init(){

		///////////////////////////////////
		// Authentication and Permission
		///////////////////////////////////
		
		if(!isSecure && !Test.isRunningTest()){
			return DoLogout();
		}
				
		if(!authenticated){
			return DoLogout();
		}

		if(portalUser.AccountType != pageAccountType){
			return homepage;
		}
		
		System.debug('Create user permish: ' + portalUser.role.Create_Users__c);
		
		if(!portalUser.role.Create_Users__c){
			return homepage;
		}
		
		///////////////////////////////////
		// Additional init
		///////////////////////////////////

		newUser = new Contact();

		return null;
	}//End Init
	
	/******* Page Buttons/Actions *******/
	public PageReference createNewUser(){
		
		//Check for username
		if(String.isBlank(username)){
			usernameErrorMsg = missingUsernameError;
			System.debug('createNewUser: Error, username not entered');
			return null;
		}
		
		//Check for unique username
		if(UserServices_CS.checkUniqueUsername(username)){			
			newUser.Username__c = username;
			System.debug('createNewUser: Username ' + username + ' is unique.');
		}
		else{
			usernameErrorMsg = nonUniqueUsernameError;
			System.debug('createNewUser: Error, username ' + username + ' is not unique.');
			return null;
		}
		
		///////////////////////////////////
		// Add required user details here
		///////////////////////////////////
		
		// Assign the new user to this account
		newUser.Entity__c = portalUser.instance.Entity__c;
		
		// Give the new user the same role as the current user
		newUser.Content_Role__c = portalUser.instance.Content_Role__c;

		// Make the user active 
		// TODO: Should this be the case?
		newUser.Active__c = true;
		
		// TODO: LEGACY DATA, YUCK
		newUser.AccountId = portalUser.instance.AccountId;
		
		System.debug('New User Details: ' + newUser);

		// Attempt to insert
		Database.Saveresult newUserResult;
		
		try{			
			newUserResult = Database.insert(newUser);
			
			System.debug('newUser SF Id: ' + newUser.Id);
			
			if(newUserResult.isSuccess()){

				// Give the new user a password and salt
				UserServices_CS.resetUserPassword(newUser);
				
				update newUser;

				//TODO: Message for success
				System.debug('Successfully created new user ' + newUser.Username__c);
				
				return Page.ProviderUsersSummary_CS.setRedirect(true);
			}
			else{
				//TODO: Message for failure
				System.debug('Failed to create new user ' + newUser.Username__c);
			}
		}
		catch(Exception e){
			System.debug('Exception: Failed to create new user ' + newUser.Username__c + ' : ' + e.getMessage());
		}
				
		return null;
	}
}