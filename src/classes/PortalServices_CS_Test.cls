@isTest
private class PortalServices_CS_Test {
	
	/*
	/	TODO: Needed to test:
	/		
	/		GetResourceURL()
	/		*other url related functions*
	/		IE compatibility
	*/

	/******* Test Parameters *******/
	static final Integer N_ORDERS = 5;

	/******* Provider *******/
	static private List<Account> providers;
	static private List<Contact> providerUsers;
	
	/******* Test Objects *******/
	static private PortalServices_CS pServices;

	/******* Test Methods *******/
	
    static testMethod void PortalServices_Test_PageLanding(){
    	init();
    }
    
    static testMethod void PortalServices_Test_Logout(){
    	init();
    	
    	Test.setCurrentPage(pServices.DoLogout());
    	
    	System.assert(ApexPages.currentPage().getUrl().toLowerCase().contains('providerportallogin'), 
    		'Redirect failed, current page: ' + ApexPages.currentPage().getUrl());
    }
    
    static testMethod void PayerPortalNoLogin(){
    	init();
    	
    	//Attempt to access PayerUsersSummary page without logging in - chcek in test coverage that it
    	//redirects to PayerPortalLogin
    	Test.setCurrentPage(Page.PayerUsersSummary_CS.setRedirect(true));
    	PayerUsersSummary_CS usersSummary = new PayerUsersSummary_CS();
    	usersSummary.init();
    	System.debug('URL: ' + ApexPages.currentPage().getUrl());
    	
    }
    
    static testMethod void ProviderPortalNoLogin(){
    	init();
    	
    	//Attempt to access ProviderCreateNewUser page without logging in - chcek in test coverage that it
    	//redirects to ProviderPortalLogin
    	Test.setCurrentPage(Page.ProviderCreateNewUser_CS.setRedirect(true));
    	ProviderCreateNewUser_CS createNewUser = new ProviderCreateNewUser_CS();
    	createNewUser.init();
    }
    
    // This test method does nothing other than provide code coverage
    //	for statically-typed html
    static testMethod void PortalServices_Test_Misc(){
    	init();
    	
    	String misc = '';
    	
    	misc = pServices.loginnav;
    	misc = PortalServices_CS.guestUserID;    	
    	misc = PortalServices_CS.sessionId;
    	
    	misc = pServices.portalUser.instance.Id;    	
    	System.assert(misc == pServices.portalUser.instance.Id);
    }
   
	static void init(){
    	
    	///////////////////////////////////
		// Data
		///////////////////////////////////
    	
    	//Providers
		providers = TestDataFactory_CS.generateProviders('TestProvider', 1);
		insert providers;
		
		//Provider users
		providerUsers = TestDataFactory_CS.createProviderContacts('ProviderUser', providers, 1);

		///////////////////////////////////
		// Page Landing
		///////////////////////////////////

		//Set the page and authentication cookie
		Test.setCurrentPage(Page.ProviderHomeDashboard_CS.setRedirect(true));
		
		//Log the desired user in
		TestServices_CS.login(providerUsers[0]);
    	
    	//Make provider portal services
    	pServices = new PortalServices_CS();
    }
}