/**
 Test class to compile and cover Complaint Email Inbound Service
 */
@isTest
private class TestComplaintEmail_IP {

    static testMethod void testMail() {
 // create a new email and envelope object
  Messaging.InboundEmail email = new Messaging.InboundEmail() ;
  Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
  
  // create sample accout
  
  Account acc =  new Account();
  acc.Name = 'Test Use';
  acc.Type_Of_Provider__c = 'DME';
  insert acc;
  String accId = acc.Id;
  
// sample contact info

Contact con = new Contact();
con.FirstName = 'Test';
con.LastName = 'Check';
con.AccountId = accId;
con.Email = 'someaddress@email.com';
insert con;
 
  // setup the data for the email
  List<String> toAdd = new List<String>();
  toAdd.add('test@salesforce.com');
  toAdd.add('test@test.com');
   email.subject = 'Test Email';
  email.fromname = 'Test Sender';
  email.messageId = 'test id 4';
  email.fromAddress = 'someaddress@email.com';
  email.toAddresses = toAdd;
 
  // add an attachment
  Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
  attachment.body = blob.valueOf('my attachment');
  attachment.fileName = 'testtextfile.txt';
  attachment.mimeTypeSubType = 'text/plain';
 
  email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
 
  // call the email service class and test it with the data in the testMethod
  CompaintEmailService_IP emailProcess = new CompaintEmailService_IP();
  emailProcess.handleInboundEmail(email, env);
 
  // query for the email the email service created
  Issue_Log__c testLog = [select Id, Email_Subject__c, From_Email_Address__c, Message_Id__c from Issue_Log__c where From_Email_Address__c = :email.fromAddress and Message_Id__c = :email.messageId];
 
  System.assertEquals(testLog.Message_Id__c,'test id 4');
  System.assertEquals(testLog.From_Email_Address__c,'someaddress@email.com');
 
  // find the attachment
  Attachment a = [select name from attachment where parentId = :testLog.id];
 
  System.assertEquals(a.name,'testtextfile.txt');
   }
    static testMethod void testMail2(){
    	// Account Sample 2
  
  Account acc1 = new Account();
  acc1.Name = 'Integra Partners';
  acc1.Type_Of_Provider__c = 'DME';
  insert acc1;
  
   // create a new email and envelope object
  Messaging.InboundEmail em = new Messaging.InboundEmail() ;
  Messaging.InboundEnvelope en = new Messaging.InboundEnvelope();
   // setup the data for the email
   List<String> toAdd = new List<String>();
  toAdd.add('test@salesforce.com');
  toAdd.add('test@test.com');
em.subject = 'Test Subject';
em.fromname = 'Test Email Sender';
em.messageId = 'test 123';
em.fromAddress = 'test@email.com';
em.toAddresses = toAdd;
  
  CompaintEmailService_IP emailProcess = new CompaintEmailService_IP();
  emailProcess.handleInboundEmail(em, en);
  
  Issue_Log__c testLog = [select Id, Email_Subject__c, From_Email_Address__c, Message_Id__c from Issue_Log__c where From_Email_Address__c = :em.fromAddress and Message_Id__c = :em.messageId];
 
  System.assertEquals(testLog.Message_Id__c,'test 123', 'Message Id checked');
  System.assertEquals(testLog.From_Email_Address__c,'test@email.com', 'Email Address checked');
  
    }
  
}