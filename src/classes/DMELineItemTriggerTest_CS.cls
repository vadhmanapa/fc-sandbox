/**
 *  @Description Test for DMELineItemDispatcher_CS
 *  @Author Cloud Software LLC
 *  Revision History: 
 *		01/12/2016 - karnold - Added header. Fixed test to remove comment from order for failure test.
 *
 */
@isTest
private class DMELineItemTriggerTest_CS { 

	static List<DME__c> dmeList;
	static List<Order__c> orderList;
	static List<DME_Line_Item__c> lineItemList1;
	static List<DME_Line_Item__c> lineItemList2;
	
	static testMethod void beforeInsert_success() {

		init();
		System.debug(orderList[0]);
		
		//Insert DME Line Items
		insert lineItemList1;

		Map<Id, DME_Line_Item__c> dmeLiMap = new Map<Id, DME_Line_Item__c>(lineItemList1);
		Map<Id, DME_Line_Item__c> queriedDmeLiMap = new Map<Id, DME_Line_Item__c>([
			SELECT Deliver_By__c
			FROM DME_Line_Item__c
		]);

		Datetime nowRounded = Datetime.now();
		nowRounded = nowRounded.addSeconds(60 - nowRounded.second()).addMinutes(59 - nowRounded.minute());
		
		System.assertEquals(nowRounded.addHours(24), queriedDmeLiMap.get(lineItemList1[0].Id).Deliver_By__c);
		System.assertEquals(nowRounded.addHours(48), queriedDmeLiMap.get(lineItemList1[1].Id).Deliver_By__c);
	}

	static testMethod void beforeInsert_failureNoComments() {
		init();

		//Insert DME Line Items
		try {
			insert lineItemList2;
			System.assert(false, 'An excpetion was NOT thrown.');
		} catch(DMLException e) {
			System.assert(e.getMessage().contains('Orders with miscellaneous DMEs must have comments'), 'Error was not correct');
		}
	}

	static void init() {

		Account prov = TestDataFactory_CS.generateProviders('Test Provider', 1)[0];

		insert TestDataFactory_CS.generateDMESettings();
		
		dmeList = TestDataFactory_CS.generateDMEs(8);
		dmeList[0].Max_Delivery_Time__c = 24;
		dmeList[1].Max_Delivery_Time__c = 48;
		dmeList[2].Miscellaneous__c = true;
		dmeList[4].Max_Delivery_Time__c = 24;
		dmeList[5].Max_Delivery_Time__c = 48;
		dmeList[6].Miscellaneous__c = true;
		insert dmeList;

		orderList = TestDataFactory_CS.generateOrders(null, prov.Id, 2);
		orderList[0].Comments__c = 'Comment';
		orderList[1].Comments__c = '';
		insert orderList;

		Map<Id, Map<Id, Decimal>> orderToDMEToQuantity1 = new Map<Id, Map<Id, Decimal>>{
			orderList[0].Id => new Map<Id, Decimal>{
				dmeList[0].Id => 1,
				dmeList[1].Id => 2,
				dmeList[2].Id => 3,
				dmeList[3].Id => 4
			}
		};

		Map<Id, Map<Id, Decimal>> orderToDMEToQuantity2 = new Map<Id, Map<Id, Decimal>>{
			orderList[1].Id => new Map<Id, Decimal>{
				dmeList[4].Id => 1,
				dmeList[5].Id => 2,
				dmeList[6].Id => 3,
				dmeList[7].Id => 4
			}
		};

		lineItemList1 = TestDataFactory_CS.generateDMELineItems(orderToDMEToQuantity1);
		lineItemList2 = TestDataFactory_CS.generateDMELineItems(orderToDMEToQuantity2);
		System.debug(lineItemList1);
	}
}