public without sharing abstract class AbstractModel_CS {

	public Integer tempID {get; private set;}
	public Boolean selected {get; set;}
	
	public AbstractModel_CS(){
		resetTempID();
		
		this.selected = false;
	}
	
	public void resetTempID(){
		this.tempID = Math.abs(Crypto.getRandomInteger());
	}
}