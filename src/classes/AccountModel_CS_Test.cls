@isTest
private class AccountModel_CS_Test {
	
	/****** Test Objects ******/
	private static List<Account> providerList;
	private static Account provider;
	private static AccountModel_CS model;
	
	/****** Test Methods ******/
	static testMethod void instanceTest(){
		init();
		model = new AccountModel_CS(provider);
		System.Assert(provider.Id == model.instance.Id);
	}
	
	static testMethod void getModelsTest(){
		init();
		
		List<AccountModel_CS> models = AccountModel_CS.getModels(providerList);
		System.AssertEquals(5,models.size());
		
		for(AccountModel_CS am : models)
		System.AssertEquals(false,am.selected);
	} 
	
	static testMethod void getAddressTest(){
		init();
		
		model = new AccountModel_CS(provider);
		
		model.instance.ShippingState = 'California';
		model.instance.ShippingCity = 'Los Angeles';
		model.instance.ShippingPostalCode = '90001';
		
		String addressLine = model.getAddressCityStateZip();
		System.debug('Address Line: ' + addressLine);
	}
	
	static testMethod void getAddressTest_NoCity(){
		init();
		
		model = new AccountModel_CS(provider);
		
		model.instance.ShippingState = 'California';
		model.instance.ShippingPostalCode = '90001';
		
		String addressLine = model.getAddressCityStateZip();
		System.debug('Address Line: ' + addressLine);
	}
	
	static testMethod void getAddressTest_NoState(){
		init();
		
		model = new AccountModel_CS(provider);
		
		model.instance.ShippingCity = 'Los Angeles';
		model.instance.ShippingPostalCode = '90001';
		
		String addressLine = model.getAddressCityStateZip();
		System.debug('Address Line: ' + addressLine);
	}
	
	static void init(){
		providerList = TestDataFactory_CS.generateProviders('Test Provider', 5);
		provider = providerList[0];
    	insert providerList;
	}
}