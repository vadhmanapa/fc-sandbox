/**
 *  @Description Services to transfer from the old provider location object to the new provider location object.
 *  @Author Cloud Software LLC
 *  Revision History: 
 *      12/02/2015 - karnold - ICA-168 - Added copyFieldsToNewProvider to copy fields to new provider location object. 
 *		12/02/2015 - karnold - ICA-168 - Changed setNewOpenAndCloseTimes to modify object directly, not return a copy and renamed to copyOpenAndCloseTimes.
 *		12/22/2015 - karnold - Created updateObjectProviderLocation to replace the 3 other object specific methods.
 *		03/02/2016 - Ksharp - ICA-531 added code to set the "Is_Open__c" Field on the new object.
 */
public class ProviderLocationTransferServices {
	private static Set<String> providerLocationsFieldNames = 
			new Set<String> { 'Monday__c', 'Tuesday__c', 'Wednesday__c', 'Thursday__c', 'Friday__c', 'Saturday__c', 'Sunday__c' };
	public static Map<String, Map<String, String>> oldFieldNameToNewFieldNamesMap;

	public static void insertNewProviderLocationRecords(List<Provider_Location__c> newProviderLocationList) {
		insert newProviderLocationList;
	}

	public static List<Provider_Location__c> transferDataToNewProviderLocationObject(List<Provider_Locations__c> providerLocationsList) {
		List<Provider_Location__c> newProviderLocationList = new List<Provider_Location__c> ();

		for (Provider_Locations__c oldProviderLocation : providerLocationsList) {

			// Create new record
			Provider_Location__c newProviderLocation = new Provider_Location__c();

			// Populate record with standard (one to one copy) fields
			copyFieldsToNewProvider(oldProviderLocation, newProviderLocation);

			// Set all open and close time fields on the new record. Flag the new record if data issues are found
			copyOpenAndCloseTimes(oldProviderLocation, newProviderLocation);

			// Add new record to list of records to be inserted
			newProviderLocationList.add(newProviderLocation);
		}

		// Insert new records
		insertNewProviderLocationRecords(newProviderLocationList);

		// Return list of records to be inserted
		return newProviderLocationList;
	}

	/**
	* @description Takes a list of objects with null Provider_Location_New__c fields and 
	*	populates that based on the lookups from the new provider location object to the old object.
	*/
	public static List<sObject> updateObjectProviderLocation(List<sObject> obj) {
		
		// Create and populate map from each provider location ID to its county items
		Map<Id, List<sObject>> oldLocationToObjListMap = new Map<Id, List<sObject>>();
		for (sObject o : obj) {
			if (!oldLocationToObjListMap.containsKey((Id) o.get('Provider_Location__c'))) {
				oldLocationToObjListMap.put((Id) o.get('Provider_Location__c'), new List<sObject>());
			}
			oldLocationToObjListMap.get((Id) o.get('Provider_Location__c')).add(o);
		}

		// Query for all the related new provider location objects
		List<Provider_Location__c> newProviderList = [
			SELECT
				Provider_Location_Old__c
			FROM Provider_Location__c
			WHERE Provider_Location_Old__c IN :oldLocationToObjListMap.keySet()
		];

		// Update the items with the new lookup
		List<sObject> updatedObjList = new List<sObject>();
		for (Provider_Location__c providerLoc : newProviderList) {
			for (sObject o : oldLocationToObjListMap.get(providerLoc.Provider_Location_Old__c)) {
				o.put('Provider_Location_New__c', providerLoc.Id);
				updatedObjList.add(o);
			}
		}

		return updatedObjList;
	}

	/**
	* @description Performs the same process as the generic version but references the specific field names for orders
	*/
	public static List<Order__c> updateOrderProviderLocation(List<Order__c> os) {
		Map<Id, List<Order__c>> oldProvLocIdToOrderListMap = new Map<Id, List<Order__c>> ();
		for (Order__c o : os) {
			if (!oldProvLocIdToOrderListMap.containsKey(o.Provider_Locations__c)) {
				oldProvLocIdToOrderListMap.put(o.Provider_Locations__c, new List<Order__c>());
			}
			oldProvLocIdToOrderListMap.get(o.Provider_Locations__c).add(o);
		}

		List<Provider_Location__c> provLocList = [
			SELECT
				Id,
				Provider_Location_Old__c
			FROM Provider_Location__c
			WHERE Provider_Location_Old__c IN :oldProvLocIdToOrderListMap.keySet()
		];

		List<Order__c> updatedOrderList = new List<Order__c>();
		for (Provider_Location__c provLoc : provLocList) {
			for (Order__c ord : oldProvLocIdToOrderListMap.get(provLoc.Provider_Location_Old__c)) {
				ord.Provider_Location__c = provLoc.Id;
				updatedOrderList.add(ord);
			}
		}

		return updatedOrderList;
	}

	/* Copy data field methods */

	/* Sets values for: Account__c, ADA_Form_Received__c, All_States_and_Counties__c, MACleanCity__c,
	  MACleanCountry__c, MACleanCounty__c, MACleanDistrict__c, MACleanPostalCode__c, MACleanState__c, MACleanStreet__c, 
	  Does_NOT_Deliver__c, Handicap_Accessible__c, MALatitude__c, Local_Counties__c, Location_City__c, Location_Contact__c,
	  Location_Contact_2__c, Location_Fax__c, Location_Medicaid__c, 
	  Location_Medicaid_State__c, Location_NPI__c, Location_Phone__c, Location_State__c, Location_Street__c, Location_Wellcare_ID__c,
	  Location_Zip_Code__c, MALongitude__c, Provider_Location_Old__c, Send_Wellcare_Docs__c, Send_Wellcare_Docs_To__c, 
	  Store_Closure_Date__c, Store_County__c, Name_DBA__c, Store_Status__c, Wellcare_Sent_on__c
	 */
	private static void copyFieldsToNewProvider(Provider_Locations__c oldProviderLocation, Provider_Location__c newProviderLocation) {
		newProviderLocation.Account__c = oldProviderLocation.Account__c;
		newProviderLocation.ADA_Form_Received__c = oldProviderLocation.ADA_Form_Received__c;
		newProviderLocation.All_States_and_Counties__c = oldProviderLocation.All_States_and_Counties__c;
		newProviderLocation.MACleanCity__c = oldProviderLocation.MACleanCity__c;
		newProviderLocation.MACleanCountry__c = oldProviderLocation.MACleanCountry__c;
		newProviderLocation.MACleanCounty__c = oldProviderLocation.MACleanCounty__c;
		newProviderLocation.MACleanDistrict__c = oldProviderLocation.MACleanDistrict__c;
		newProviderLocation.MACleanPostalCode__c = oldProviderLocation.MACleanPostalCode__c;
		newProviderLocation.MACleanState__c = oldProviderLocation.MACleanState__c;
		newProviderLocation.MACleanStreet__c = oldProviderLocation.MACleanStreet__c;
		newProviderLocation.Does_NOT_Deliver__c = oldProviderLocation.Does_NOT_Deliver__c;
		newProviderLocation.Handicap_Accessible__c = oldProviderLocation.Handicap_Accessible__c;
		newProviderLocation.MALatitude__c = oldProviderLocation.MALatitude__c;
		newProviderLocation.Local_Counties__c = oldProviderLocation.Local_Counties__c;
		newProviderLocation.Location_City__c = oldProviderLocation.Location_City__c;
		newProviderLocation.Location_Contact__c = oldProviderLocation.Location_Contact__c;
		newProviderLocation.Location_Contact_2__c = oldProviderLocation.Location_Contact_2__c;
		newProviderLocation.Location_Fax__c = oldProviderLocation.Location_Fax__c;
		newProviderLocation.Location_Medicaid__c = oldProviderLocation.Location_Medicaid__c;
		newProviderLocation.Location_Medicaid_State__c = oldProviderLocation.Location_Medicaid_State__c;
		newProviderLocation.Location_NPI__c = oldProviderLocation.Location_NPI__c;
		newProviderLocation.Location_Phone__c = oldProviderLocation.Location_Phone__c;
		newProviderLocation.Location_State__c = oldProviderLocation.Location_State__c;
		newProviderLocation.Location_Street__c = oldProviderLocation.Location_Street__c;
		newProviderLocation.Location_Wellcare_ID__c = oldProviderLocation.Location_Wellcare_ID__c;
		newProviderLocation.Location_Zip_Code__c = oldProviderLocation.Location_Zip_Code__c;
		newProviderLocation.MALongitude__c = oldProviderLocation.MALongitude__c;
		newProviderLocation.Provider_Location_Old__c = oldProviderLocation.Id;
		newProviderLocation.Send_Wellcare_Docs__c = oldProviderLocation.Send_Wellcare_Docs__c;
		newProviderLocation.Send_Wellcare_Docs_To__c = oldProviderLocation.Send_Wellcare_Docs_To__c;
		newProviderLocation.Store_Closure_Date__c = oldProviderLocation.Store_Closure_Date__c;
		newProviderLocation.Store_County__c = oldProviderLocation.County__c;
		newProviderLocation.Name_DBA__c = oldProviderLocation.Name_DBA__c;
		newProviderLocation.Store_Status__c = oldProviderLocation.Store_Status__c;
		newProviderLocation.Wellcare_Sent_on__c = oldProviderLocation.Wellcare_Sent_on__c;
	}

	// Sets values for: Data_Issue_Found__c, Description_of_Data_Issue__c, *_Closing_Time__c, *_Is_24_Hours__c, *_Is_Open__c, *_Opening_Time__c
	public static void copyOpenAndCloseTimes(Provider_Locations__c oldProviderLocation, Provider_Location__c newProviderLocation) {
		oldFieldNameToNewFieldNamesMap = getOldFieldNameToNewFieldNamesMap();

		// Go through each day field on the old record
		for (String name : providerLocationsFieldNames) {
			if (String.isBlank(String.valueOf(oldProviderLocation.get(name)))) {
				if (newProviderLocation.Data_Issue_Found__c && String.valueOf(newProviderLocation.Description_of_Data_Issue__c).length() < 130772) {
					newProviderLocation.Description_of_Data_Issue__c += ' Issue found on the fields ' + oldFieldNameToNewFieldNamesMap.get(name).get('OpenTime') + ' and ' + oldFieldNameToNewFieldNamesMap.get(name).get('CloseTime') + '. A null value was found on the ' + name + ' field on the original record.';
				} else {
					newProviderLocation.Data_Issue_Found__c = true;
					newProviderLocation.Description_of_Data_Issue__c = '(Original record: ' + oldProviderLocation.Id + '). Issue found on the fields ' + oldFieldNameToNewFieldNamesMap.get(name).get('OpenTime') + ' and ' + oldFieldNameToNewFieldNamesMap.get(name).get('CloseTime') + '. A null value was found on the ' + name + ' field on the original record.';
				}
				continue;
			}

			// Get the value of the field on the old record with all spaces removed
			String oldValue = String.valueOf(oldProviderLocation.get(name)).trim().replaceAll('(\\s+)', '');

			// If the value is 'Closed', we leave the two fields blank and continue on without flagging the record
			if (oldValue.containsIgnoreCase('closed')) {
				continue;

				// Ensure that the value on the old record is not null and is in an accepted format, else flag and continue
			} else if (String.isBlank(oldValue) || oldValue.substring(0, 1).isAlpha() || oldValue.containsNone('-')) {
				if (newProviderLocation.Data_Issue_Found__c && String.valueOf(newProviderLocation.Description_of_Data_Issue__c).length() < 130772) {
					newProviderLocation.Description_of_Data_Issue__c += ' Issue found on the fields ' + oldFieldNameToNewFieldNamesMap.get(name).get('OpenTime') + ' and ' + oldFieldNameToNewFieldNamesMap.get(name).get('CloseTime') + '.';
				} else {
					newProviderLocation.Data_Issue_Found__c = true;
					newProviderLocation.Description_of_Data_Issue__c = '(Original record: ' + oldProviderLocation.Id + '). Issue found on the fields ' + oldFieldNameToNewFieldNamesMap.get(name).get('OpenTime') + ' and ' + oldFieldNameToNewFieldNamesMap.get(name).get('CloseTime') + '.';
				}
				continue;

				// Scenario 1 - the start time is in the AM and the end time is in the PM
			} else if (oldValue.containsIgnoreCase('am') && oldValue.substringAfter('-').containsIgnoreCase('pm')) {
				
				// Flag this day as open.
				newProviderLocation.put(name.replaceAll('__c','') + '_is_Open__c', true); //added KS 3/2/2016

				// Set the start time
				if (oldValue.toLowerCase().substringBefore('am').contains(':')) {
					newProviderLocation.put(
					(oldFieldNameToNewFieldNamesMap.get(name).get('OpenTime')),
					                        Integer.valueOf(oldValue.substringBefore(':')) + (Double.valueOf(oldValue.toLowerCase().substringBetween(':', 'am')) / 60)
					);
				} else {
					newProviderLocation.put(
					(oldFieldNameToNewFieldNamesMap.get(name).get('OpenTime')),
					                        Integer.valueOf(oldValue.toLowerCase().substringBefore('am'))
					);
				}

				// Set the end time
				if (oldValue.toLowerCase().substringAfter('-').contains(':')) {
					if (Integer.valueOf(oldValue.substringBetween('-', ':')) == 12) {
						newProviderLocation.put(
						(oldFieldNameToNewFieldNamesMap.get(name).get('CloseTime')),
						                        Integer.valueOf(oldValue.substringBetween('-', ':')) + (Double.valueOf(oldValue.toLowerCase().substringAfter('-').substringBetween(':', 'pm')) / 60)
						);
					} else {
						newProviderLocation.put(
						(oldFieldNameToNewFieldNamesMap.get(name).get('CloseTime')),
						                        Integer.valueOf(oldValue.substringBetween('-', ':')) + 12 + (Double.valueOf(oldValue.toLowerCase().substringAfter('-').substringBetween(':', 'pm')) / 60)
						);
					}
				} else {
					if (Integer.valueOf(oldValue.toLowerCase().substringBetween('-', 'pm')) == 12) {
						newProviderLocation.put(
						(oldFieldNameToNewFieldNamesMap.get(name).get('CloseTime')),
						                        Integer.valueOf(oldValue.toLowerCase().substringBetween('-', 'pm'))
						);
					} else {
						newProviderLocation.put(
						(oldFieldNameToNewFieldNamesMap.get(name).get('CloseTime')),
						                        Integer.valueOf(oldValue.toLowerCase().substringBetween('-', 'pm')) + 12
						);
					}
				}

				// Scenario 2 - AM and PM are not specified
			} else if (!oldValue.containsIgnoreCase('am') && !oldValue.containsIgnoreCase('pm')) {
				if (oldValue.substringBefore('-').contains(':') && oldValue.substringAfter('-').contains(':')) {
					if (!oldValue.substringBefore(':').isNumeric()
					    || !oldValue.substringBetween('-', ':').isNumeric()
					    || Integer.valueOf(oldValue.substringBefore(':')) == 12
					    || Integer.valueOf(oldValue.substringBetween('-', ':')) == 12
					    || (Integer.valueOf(oldValue.substringBefore(':')) < Integer.valueOf(oldValue.substringBetween('-', ':')))
					) {
						addErrorCouldNotDetermineAMOrPM(newProviderLocation, name, oldProviderLocation.Id);
						continue;
					}

					newProviderLocation.put(
					(oldFieldNameToNewFieldNamesMap.get(name).get('OpenTime')),
					                        Integer.valueOf(oldValue.substringBefore(':')) + (Double.valueOf(oldValue.substringBetween(':', '-')) / 60)
					);
					newProviderLocation.put(
					(oldFieldNameToNewFieldNamesMap.get(name).get('CloseTime')),
					                        Integer.valueOf(oldValue.substringBetween('-', ':')) + 12 + (Double.valueOf(oldValue.toLowerCase().substringAfterLast(':')) / 60)
					);

					// Flag this day as open.
					newProviderLocation.put(name.replaceAll('__c','') + '_is_Open__c', true); //added KS 3/2/2016

				} else if (!oldValue.contains(':')) {
					if (!oldValue.substringBefore('-').isNumeric()
					    || !oldValue.substringAfter('-').isNumeric()
					    || Integer.valueOf(oldValue.substringBefore('-')) == 12
					    || Integer.valueOf(oldValue.substringAfter('-')) == 12
					    || (Integer.valueOf(oldValue.substringBefore('-')) < Integer.valueOf(oldValue.substringAfter('-')))
					) {
						addErrorCouldNotDetermineAMOrPM(newProviderLocation, name, oldProviderLocation.Id);
						continue;
					}

					newProviderLocation.put(
					(oldFieldNameToNewFieldNamesMap.get(name).get('OpenTime')),
					                        Integer.valueOf(oldValue.substringBefore('-'))
					);
					newProviderLocation.put(
					(oldFieldNameToNewFieldNamesMap.get(name).get('CloseTime')),
					                        Integer.valueOf(oldValue.substringAfter('-')) + 12
					);
					
					// Flag this day as open.
					newProviderLocation.put(name.replaceAll('__c','') + '_is_Open__c', true); //added KS 3/2/2016

				} else {
					if (oldValue.substringBefore('-').contains(':') && !oldValue.substringAfter('-').contains(':')) {
						if (!oldValue.substringBefore(':').isNumeric()
						    || !oldValue.substringAfter('-').isNumeric()
						    || Integer.valueOf(oldValue.substringBefore(':')) == 12
						    || Integer.valueOf(oldValue.substringAfter('-')) == 12
						    || (Integer.valueOf(oldValue.substringBefore(':')) < Integer.valueOf(oldValue.substringAfter('-')))
						) {
							addErrorCouldNotDetermineAMOrPM(newProviderLocation, name, oldProviderLocation.Id);
							continue;
						}

						newProviderLocation.put(
						(oldFieldNameToNewFieldNamesMap.get(name).get('OpenTime')),
						                        Integer.valueOf(oldValue.substringBefore(':')) + (Double.valueOf(oldValue.substringBetween(':', '-')) / 60)
						);
						newProviderLocation.put(
						(oldFieldNameToNewFieldNamesMap.get(name).get('CloseTime')),
						                        Integer.valueOf(oldValue.substringAfter('-')) + 12
						);

						// Flag this day as open.
						newProviderLocation.put(name.replaceAll('__c','') + '_is_Open__c', true); //added KS 3/2/2016

					} else {
						if (!oldValue.substringBefore('-').isNumeric()
						    || !oldValue.substringBetween('-', ':').isNumeric()
						    || Integer.valueOf(oldValue.substringBefore('-')) == 12
						    || Integer.valueOf(oldValue.substringBetween('-', ':')) == 12
						    || (Integer.valueOf(oldValue.substringBefore('-')) < Integer.valueOf(oldValue.substringBetween('-', ':')))
						) {
							addErrorCouldNotDetermineAMOrPM(newProviderLocation, name, oldProviderLocation.Id);
							continue;
						}

						newProviderLocation.put(
						(oldFieldNameToNewFieldNamesMap.get(name).get('OpenTime')),
						                        Integer.valueOf(oldValue.substringBefore('-'))
						);
						newProviderLocation.put(
						(oldFieldNameToNewFieldNamesMap.get(name).get('CloseTime')),
						                        Integer.valueOf(oldValue.substringBetween('-', ':')) + 12 + (Double.valueOf(oldValue.toLowerCase().substringAfterLast(':')) / 60)
						);

						// Flag this day as open.
						newProviderLocation.put(name.replaceAll('__c','') + '_is_Open__c', true); //added KS 3/2/2016

					}
				}
				// Scenario 3 - either AM or PM were not specified. This case was not found in existing data and is being added purely as a precaution.
			} else {
				if (!newProviderLocation.Data_Issue_Found__c) {
					newProviderLocation.Data_Issue_Found__c = true;
					newProviderLocation.Description_of_Data_Issue__c = '(Original record: ' + oldProviderLocation.Id + '). Issue found on the fields ' + oldFieldNameToNewFieldNamesMap.get(name).get('OpenTime') + ' and ' + oldFieldNameToNewFieldNamesMap.get(name).get('CloseTime') + '. Unexpected error occurred.';
				} else {
					newProviderLocation.Description_of_Data_Issue__c += ' Issue found on the fields ' + oldFieldNameToNewFieldNamesMap.get(name).get('OpenTime') + ' and ' + oldFieldNameToNewFieldNamesMap.get(name).get('CloseTime') + '. Unexpected error occurred.';

				}
			}
		}
	}

	private static void addErrorCouldNotDetermineAMOrPM(Provider_Location__c newProviderLocation, String name, Id oldProviderLocationId) {
		if (newProviderLocation.Data_Issue_Found__c && String.valueOf(newProviderLocation.Description_of_Data_Issue__c).length() < 130772) {
			newProviderLocation.Description_of_Data_Issue__c += ' Issue found on the fields ' + oldFieldNameToNewFieldNamesMap.get(name).get('OpenTime') + ' and ' + oldFieldNameToNewFieldNamesMap.get(name).get('CloseTime') + '. Could not determine whether the opening and closing times are AM or PM.';
		} else {
			newProviderLocation.Data_Issue_Found__c = true;
			newProviderLocation.Description_of_Data_Issue__c = '(Original record: ' + oldProviderLocationId + '). Issue found on the fields ' + oldFieldNameToNewFieldNamesMap.get(name).get('OpenTime') + ' and ' + oldFieldNameToNewFieldNamesMap.get(name).get('CloseTime') + '. Could not determine whether the opening and closing times are AM or PM.';
		}
	}

	private static Map<String, Map<String, String>> getOldFieldNameToNewFieldNamesMap() {
		Map<String, Map<String, String>> oldFieldLabelToNewFieldLabelsMap = new Map<String, Map<String, String>> ();
		for (String providerLocationsFieldName : providerLocationsFieldNames) {
			oldFieldLabelToNewFieldLabelsMap.put(providerLocationsFieldName, new Map<String, String> { 'OpenTime' => providerLocationsFieldName.substringBefore('_') + '_Opening_Time__c', 'CloseTime' => providerLocationsFieldName.substringBefore('_') + '_Closing_Time__c' });
		}
		return oldFieldLabelToNewFieldLabelsMap;
	}
}