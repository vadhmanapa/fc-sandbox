@isTest
private class ProviderOrdersSummary_CS_Test {
	public static Account plan;
	public static Account provider;
	public static Contact user;
	public static List<Order__c> orderList;
    public static Role__c adminRole;
	
    static testMethod void ProviderOrdersSummary_CS_PageLanding() {
        init();
		
		//Page Landing
		PageReference pr = Page.ProviderOrdersSummary_CS;
		Test.setCurrentPage(pr);
		TestServices_CS.login(user);
		
		//Page Init
        ProviderOrdersSummary_CS cont = new ProviderOrdersSummary_CS();		
		cont.init();
    }
    
    static testMethod void ProviderOrdersSummary_CS_PageLandingNoAuth(){
    	init();
    	
    	//Page Landing, no login
    	PageReference pr = Page.ProviderOrdersSummary_CS;
		Test.setCurrentPage(pr);
    	
    	//Page Init
    	ProviderOrdersSummary_CS cont = new ProviderOrdersSummary_CS();    	
    	cont.init();
    }
    
    static testMethod void ProviderOrdersSummary_CS_initWithFilter(){
    	init();
    	
    	//Page Landing with filter param
    	PageReference pr = Page.ProviderOrdersSummary_CS;
    	pr.getParameters().put('filter', 'Pending Acceptance');
		Test.setCurrentPage(pr);
		TestServices_CS.login(user);
		
		//Page Init		
    	ProviderOrdersSummary_CS cont = new ProviderOrdersSummary_CS();		
		cont.init();
    }
    
    static testMethod void ProviderOrdersSummary_CS_FilterByUser(){
    	init();

        user.Content_Role__c = adminRole.Id;
        update user;
    	
    	//Page Landing with filter param
    	PageReference pr = Page.ProviderOrdersSummary_CS;
		Test.setCurrentPage(pr);
		TestServices_CS.login(user);
		
		//Page Init		
    	ProviderOrdersSummary_CS cont = new ProviderOrdersSummary_CS();		
		cont.init();

        Boolean isFilterByUserPermission = cont.filterByUserPermission;
		//Filter will be set up for User by default, select a user
		cont.filterByUser = cont.filterByUserOptions[0].getValue();
		System.debug('Selected User: ' + cont.filterByUserOptions[0].getValue());
		
		//Repopulate orderList
		cont.populateOrderList();
        cont.pageSize = 10;
        
        System.assertEquals(orderList.size(), cont.orderList.size());
    }
    
    static testMethod void ProviderOrdersSummary_CS_FilterByAccount(){
    	init();
    	
    	//Page Landing with filter param
    	PageReference pr = Page.ProviderOrdersSummary_CS;
		Test.setCurrentPage(pr);
		TestServices_CS.login(user);
		
		//Page Init		
    	ProviderOrdersSummary_CS cont = new ProviderOrdersSummary_CS();		
		cont.init();
		
		//Filter will be set up for User by default, change to account
        List<SelectOption> entityTypeOptions = cont.filterByEntityTypeOptions;
		cont.filterByEntityType = 'Provider';
		
		//Select an account
		cont.filterByPayer = cont.filterByPayerOptions[0].getValue();
		System.debug('Selected Provider: ' + cont.filterByUserOptions[0].getValue());
		
		//Repopulate orderList
		cont.populateOrderList();
    }

    static testMethod void ProviderOrdersSummary_CS_FilterByPayer(){
        init();

        for (Order__c o : orderList) {
            o.Accepted_By__c = user.Id;
        }
        update orderList;

        //Page Landing with filter param
        PageReference pr = Page.ProviderOrdersSummary_CS;
        Test.setCurrentPage(pr);
        TestServices_CS.login(user);

        //Page Init
        ProviderOrdersSummary_CS cont = new ProviderOrdersSummary_CS();
        cont.init();

        //Filter will be set up for User by default, change to account
        cont.filterByEntityType = 'User';

        //Select an account
        cont.filterByPayer = cont.filterByPayerOptions[0].getValue();
        System.debug('Selected Provider: ' + cont.filterByUserOptions[0].getValue());

        //Repopulate orderList
        cont.populateOrderList();
    }
    
    public static void init() {
    	List<Account> providerList = TestDataFactory_CS.generateProviders('Test Provider', 1);
    	provider = providerList[0];
    	insert providerList;
    	
    	List<Account> payerList = TestDataFactory_CS.generatePlans('Test Payer', 1);
    	plan = payerList[0];
    	insert payerList;
    	
    	List<Plan_Patient__c> patientList = TestDataFactory_CS.generatePlanPatientsPlanOnly(plan.Id, 1);
    	insert patientList;  	
    	
    	user = TestDataFactory_CS.createProviderContacts('Admin', providerList, 1)[0];
        adminRole = TestDataFactory_CS.generateAdminRole();
        insert adminRole;
    	
    	orderList = TestDataFactory_CS.generateOrders(patientList[0].Id, provider.Id, 3);
    	for(Order__c o : orderList){
    		o.Entered_By__c = user.Id;
    		o.Status__c = 'New';
    	}
    	insert orderList;
	}
}