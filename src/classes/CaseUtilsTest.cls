@isTest
private class CaseUtilsTest {

	static Account payor {get;set;}
	static Contact patient {get;set;}
	static Call_Log__c callLog {get;set;}
	static Case testCase {get;set;}

    static testMethod void CopyFromCallLog() {
       
       init();
       
       CaseUtils.CopyFromCallLog(callLog, testCase);
       
       system.assertEquals(callLog.Call_Reason__c,testCase.Type);
       system.assertEquals(CaseUtils.CASE_REASON_MULTI,testCase.Reason);
       system.assertEquals(callLog.Call_Result_Description__c,testCase.Description);
       system.assertEquals(callLog.Patient__c,testCase.Patient__c);
       system.assertEquals(callLog.Patient_Payor__c,testCase.Patient_Payor__c);
       system.assertEquals(patient.Id,testCase.ContactId);
       system.assertEquals(patient.AccountId,testCase.AccountId);
       system.assertEquals(patient.Phone,testCase.SuppliedPhone);
       system.assertEquals(patient.Email,testCase.SuppliedEmail);
    }
    
    static testMethod void testMultiselect() {
    	
    	String input = '[Tom, Dick, Harry]';
    	List<String> split = CaseUtils.parseMultiselect(input);
    	System.assertEquals(3, split.size());
    	Set<String> expectedSet = new Set<String>{'Tom','Dick','Harry'};
    	for (String s : split){
    		System.debug(s);
    		System.assert(expectedSet.contains(s));
    	}
    	String output = CaseUtils.formatMultiselect(split);
    	System.assertEquals('Tom;Dick;Harry', output);
    	System.assertEquals(output, CaseUtils.saveMultiselect(input));
    }
    
    static testMethod void coverage() {
    	
    	CaseUtils.debugOnCase(new Case(), 'Test');
    }
    
    static void init(){
    	
    	payor = new Account(
    		Name = 'Test Account',
    		RecordTypeId = RecordTypes.payorAccountId
    	);
    	insert payor;
    	
    	patient = new Contact(
    		FirstName = 'TestFirst',
    		LastName = 'TestLast',
    		AccountId = payor.Id,
    		Account = payor,
    		Phone = '1111111111',
    		MobilePhone = '2222222222',
    		Email = 'test@test.com',
    		RecordTypeId = RecordTypes.patientId
    	);
    	insert patient;
    	
    	callLog = new Call_Log__c(
    		Caller_Account__c = payor.Id,
    		Caller_Contact__c = patient.Id,
    		Call_Reason__c = 'Test',
    		Call_Reason_Detail__c = 'Test',
    		Call_Result_Description__c = 'Test',
    		Call_Reason_Detail_Multi__c = '[Test, Test2]',
    		Patient__c = patient.Id,
    		Patient_Payor__c = patient.AccountId
    	);
    	insert callLog;
    	
    	testCase = new Case(
    		Subject = 'Test Case',
    		Priority = 'Standard',
    		RecordTypeId = RecordTypes.standardCaseId,
    		Type = CaseUtils.CASE_TYPE_QUE_SUPPORT
    	);
    }
    
}