/**
 *  @Author Cloud Software LLC
 *  Revision History:
 *      10/07/2016 - vadhmanapa - Added additional Provider Rejection reasons to select options method.
 *          Based on requirements to accomodate additional reasons.
 *
 */

public without sharing class ProviderOrderSummary_CS extends ProviderPortalServices_CS{

    private ProviderOrderServices_CS providerOrderServices;
    private PatientServices_CS patientServices;
    public static String guestUserId {  get{ return UserInfo.getUserId(); } }

    /******* Page Notifictations *******/
    public String errorMessage {get;set;}
    public Boolean errorFlag {get;set;}

    //Error message for messages
    public String noteErrorMsg {get;set;}

    /******* Page Permissions *******/
    public Boolean acceptOrderPermission {get{ return portalUser.role.Accept_Orders__c; }}
    public Boolean rejectOrderPermission {get{ return portalUser.role.Reject_Orders__c; }}
    public Boolean deliveryOrderPermission {get{ return portalUser.role.Mark_Orders_Delivered__c; }}
    public Boolean shippingOrderPermission {get{ return portalUser.role.Mark_Orders_Shipped__c; }}
    public Boolean markFutureDosOrderPermission {
        get {
            return portalUser.role.Mark_Orders_Future_DOS__c;
        }
    }

    /******* Parameters Needed for Modal *******/
    //Order accepted info
    public String estimatedDate {get;set;}
    public String changeEstimatedDateInfo {get;set;} // Sprint 2/13 - an input textbox to hold reason to change the estimated delivery date from current date

    //Order delivered info
    public String deliveredDate {get;set;}
    public String estimatedDelDate {get;set;}
    public String changeDeliveryReason { get; set; }

    //Order shipping info
    public String deliveryMethod {get;set;}
    public List<SelectOption> getDeliveryMethods(){
        return new List<SelectOption>{
            new SelectOption('','Please Select...'),
            new SelectOption('Store Personnel','Store Personnel'),
            new SelectOption('USPS','USPS'),
            new SelectOption('FedEx','FedEx'),
            new SelectOption('UPS','UPS'),
            new SelectOption('Other Carrier','Other Carrier')
        };
    }
    public String trackingNumber {get;set;}

    //Order rejection
    public String rejectionReason {get;set;}
    public List<SelectOption> getRejectionReasons(){
        List<SelectOption> options =  new List<SelectOption>{
            new SelectOption(OrderModel_CS.STATUS_PROVIDER_REJECTED_OUT_OF_STOCK,'Out of stock'),
            new SelectOption(OrderModel_CS.STATUS_PROVIDER_REJECTED_DONT_CARRY,'Do not carry this product'),
            new SelectOption(OrderModel_CS.STATUS_PROVIDER_REJECTED_DELIVERY_DEADLINE,'Delivery deadline can not be met'),
            new SelectOption(OrderModel_CS.STATUS_PROVIDER_REJECTED_DIFFERENT_PROVIDER,'Different provider listed on authorization'),
            new SelectOption(OrderModel_CS.STATUS_PROVIDER_REJECTED_REIMBURSEMENT_BELOW_COST,'Reimbursement is below cost'),
            new SelectOption(OrderModel_CS.STATUS_PROVIDER_REJECTED_DO_NOT_REPAIR,'Repairs are not performed for items not sold by provider'),
            new SelectOption(OrderModel_CS.STATUS_PROVIDER_REJECTED_DO_NOT_SERVICE,'Location is not serviced by provider'),
            new SelectOption(OrderModel_CS.STATUS_PROVIDER_REJECTED_OUTSTANDING_BALANCE,'Patient balance outstanding/financial issue'),
            new SelectOption(OrderModel_CS.STATUS_PROVIDER_REJECTED_MEDICARE_BID_WINNER,'Medicare Primary - Provider not a bid winner for ordered item(s)'),// Added by VK on 10/07/2016
            new SelectOption(OrderModel_CS.STATUS_PROVIDER_REJECTED_MEDICARE_DECLINED_SERVICE,'Medicare Primary - Bid winner, but declined to service')// Added by VK on 10/07/2016
                /***************** NOTE: DEACTIVATED BY VK ON 03/31/2017 *******************/
            // new SelectOption(OrderModel_CS.STATUS_PROVIDER_REJECTED_PATIENT_READMITTED,'Patient Readmitted and no longer needs services at home'),// Added by VK on 10/07/2016
            // new SelectOption(OrderModel_CS.STATUS_PROVIDER_REJECTED_PATIENT_REFUSED,'Patient Refused delivery'),// Added by VK on 10/07/2016
            // new SelectOption(OrderModel_CS.STATUS_PROVIDER_REJECTED_DUPLICATE,'Duplicate order'),
            // new SelectOption(OrderModel_CS.STATUS_PROVIDER_REJECTED_MEDICARE_NO_DOC,'Medicare Primary -Unable to obtain documentation'),// Added by VK on 10/07/2016
            // new SelectOption(OrderModel_CS.STATUS_PROVIDER_REJECTED_PAYOR_NOT_PARTICIPATING,'Primary payor not participating with Integra'),// Added by VK on 10/07/2016
            // new SelectOption(OrderModel_CS.STATUS_PROVIDER_REJECTED_PATIENT_NO_BENIFITS,'Patient does not have benefits for this/these service(s)')// Added by VK on 10/07/2016
        };

        options.sort();
        options.add(0,new SelectOption('','Please select a reason...'));
        options.add(new SelectOption(OrderModel_CS.STATUS_PROVIDER_REJECTED_OTHER,'Other (Please explain in the notes section below)')); // Added by VK on 10/07/2016

        return options;
    }
    public String otherRejectionReason {get;set;}

    /******* Page Interface *******/
    public OrderModel_CS orderModel {get;set;}
    public Plan_Patient__c thisPatient {get;set;}

    //Order notes
    public String noteBody {get;set;}

    //Order Future DOS
    public Boolean isMarkedAsFutureDos { get; set; }
    public String futureDosNote { get; set; }
    public Integer numberOfDaysBetweenExpectedDeliveryDateAndCurrentDate{
        get {
            return (this.orderModel.instance.Estimated_Delivery_Time__c == null)
                ? 0 : Date.today().daysBetween(this.orderModel.instance.Estimated_Delivery_Time__c.date());
        }
    }
    public Boolean isOrderFutureDos {
        get {
            return (this.orderModel.instance.Status__c == OrderModel_CS.STATUS_FUTURE_DOS);
        }
    }


    //Order note needs attention
    public Boolean flagAttention {get;set;}
    public Boolean unflagAttention {get;set;}
    public String attentionReason {get;set;}
    public List<SelectOption> getAttentionReasons(){
        /*List<SelectOption> options = new List<SelectOption>{
            new SelectOption(OrderModel_CS.STATUS_NEEDS_ATTENTION_AUTH_EXPIRED,'Authorization expired'),
            new SelectOption(OrderModel_CS.STATUS_NEEDS_ATTENTION_AUTH_EXTENSION,'Authorization extension needed'),
            new SelectOption(OrderModel_CS.STATUS_NEEDS_ATTENTION_INCOMPLETE_DELIVERY_INFO,'Delivery information incomplete'),
//          new SelectOption(OrderModel_CS.STATUS_NEEDS_ATTENTION_FUTURE_DOS,'Future DOS'), TODO: Remove this value (deprecated)
            new SelectOption(OrderModel_CS.STATUS_NEEDS_ATTENTION_PATIENT_ALREADY_SERVICED,'Patient already serviced by another provider'),
            new SelectOption(OrderModel_CS.STATUS_NEEDS_ATTENTION_UNABLE_TO_CONTACT_PATIENT,'Patient unable to be contacted'),
            // new SelectOption(OrderModel_CS.STATUS_NEEDS_ATTENTION_OTHER,'Other (please describe in detail)'),
            new SelectOption(OrderModel_CS.STATUS_NEEDS_ATTENTION_PATIENT_REFUSED,'Patient refused the order')
        };*/

        /*
         ** NOTE: SPRINT 12/12 - BY VK
        */
        
        List<SelectOption> options = new List<SelectOption>();
        Map<String, String> reasonVals = new Map<String, String>();
        reasonVals.putAll(OrderStatusMetadataWrapper_IP.getProviderNAQ());

        if(reasonVals.size() > 0) {
            for(String s: reasonVals.keySet()) {
                if(s != OrderStatusMetadataWrapper_IP.providerOtherReason){
                    options.add(new SelectOption(reasonVals.get(s), s));
                }
                
            }   
        }

        options.sort();
        options.add(new SelectOption(reasonVals.get(OrderStatusMetadataWrapper_IP.providerOtherReason),OrderStatusMetadataWrapper_IP.providerOtherReason));
        options.add(0,new SelectOption('','Please Select a reason...'));

        return options;
    }

    // TODO: Set controller or not? List for now.
    public ApexPages.Standardsetcontroller lineItemSetCon {get;set;}

    /*
    public List<DME_Line_Item__c> lineItemList
    {
        get
        {
            if(lineItemSetCon != null && lineItemSetCon.getRecords().size() > 0)
            {
                return (List<DME_Line_Item__c>) lineItemSetCon.getRecords();
            }
            return new List<DME_Line_Item__c>();
        }
        set;
    }
    */

    public List<DME_Line_Item__c> lineItemList
    {
        get{
            return ProductServices_CS.retrieveDLIsForOrder(orderModel.instance);
        }
        private set;
    }

    public String pendingOrderCount
    {
        get{
            return (providerOrderServices != null) ? String.valueOf(providerOrderServices.returnAggregateOrdersByStage('Pending Acceptance')) : '';
        }
        set;
    }

    public List<OrderMessage_CS> orderMessages{
        get
        {
            return (orderModel.instance.Message_History_JSON__c != null) ? OrderJSONFactory_CS.returnOrderMessages(orderModel.instance.Message_History_JSON__c) : new List<OrderMessage_CS>();
            //Q: Will returning null bust the front end?
        }
        private set;
    }//End orderMessages get;set;

    public List<OrderHistory_CS> orderHistory{
        get{
            return (orderModel.instance.Order_History_JSON__c != null) ? orderModel.returnHistory() : new List<OrderHistory_CS>();
        }
    }

    /*
    public List<OrderHistory_CS> orderHistory{
        get{
            if(orderModel.instance.Order_History_JSON__c != null){
                //return orderModel.returnHistory();
                List<OrderHistory_CS> retHistory = orderModel.returnHistory();
                List<OrderHistory_CS> ordHist = new List<OrderHistory_CS>();
                for(OrderHistory_CS oh: retHistory){
                    if( oh.orderStatus == OrderModel_CS.STATUS_PROVIDER_REJECTED_OUT_OF_STOCK ||
                        oh.orderStatus == OrderModel_CS.STATUS_PROVIDER_REJECTED_DONT_CARRY ||
                        oh.orderStatus == OrderModel_CS.STATUS_PROVIDER_REJECTED_DELIVERY_DEADLINE ||
                        oh.orderStatus == OrderModel_CS.STATUS_PROVIDER_REJECTED_OTHER )
                    {
                        return ordHist;
                    }else {
                        ordHist.add(oh);
                    }
                }
            }
            return new List<OrderHistory_CS>();
        }
    }
    */

    /******* File Attachments *******/
    public String attachmentUploadMessage {get;set;}
    public final String attachmentSuccess = 'Attachment uploaded successfully';
    public final String attachmentFailure = 'Error uploading attachment';

    public String selectedAttachmentId {get;set;}

    public Attachment attachment {
        get{
            // return (attachment == null) ? new Attachment() : attachment;
            if(attachment == null){
                attachment = new Attachment();
            }
            return attachment;
        }
        set;
    }
    public List<Attachment> currentAttachments {
        get{
            return [
                select
                    Name,
                    BodyLength,
                    OwnerId,
                    CreatedDate
                from Attachment
                where ParentId = :orderModel.instance.Id
                order by CreatedDate desc
            ];
        }
        set;
    }

    /********* Constructor/Init *********/
    public ProviderOrderSummary_CS()
    {

    }

    public PageReference init()
    {

        ///////////////////////////////////
        // Authentication and Permission
        ///////////////////////////////////

        if(!isSecure && !Test.isRunningTest()){return DoLogout();} 

        if(!authenticated){return DoLogout();} 

        if(portalUser.AccountType != pageAccountType) {return homepage;}

        ///////////////////////////////////
        // Additional init
        ///////////////////////////////////
        
        System.debug('\n\n portalUser => ' + portalUser + '\n');

        providerOrderServices = new ProviderOrderServices_CS(portalUser.instance.Id);
        patientServices = new PatientServices_CS(portalUser.instance.Id);


        // Get order Id from page
        if(ApexPages.currentPage().getParameters().containsKey('oid'))
        {
            String orderId = ApexPages.currentPage().getParameters().get('oid');
            System.debug('OrderSummary: OrderId: ' + orderId);

            // Query to order
            Order__c o = providerOrderServices.getOrderById(orderId);

            // If no order found, bail out
            if(o == null){
                return Page.ProviderOrdersSummary_CS;
            }

            System.debug(o);

            // Find the order based on order id
            orderModel = new OrderModel_CS(o);

            System.debug(orderModel.instance);

            // Find patient based on order
            system.debug('Plan Patient ID from order model: ' + orderModel.instance.Plan_Patient__c);
            thisPatient = patientServices.getPlanPatientById(orderModel.instance.Plan_Patient__c);
            system.debug('### thisPatient: ' + thisPatient);

            // Find the order line items
            lineItemSetCon = providerOrderServices.getOrderLineItems(orderModel.instance);

            // Set up flags for "Needs Attention"
            flagAttention = false;
            unflagAttention = false;
        }
        else
        {
            // TODO: Display error for order not found, return to OrdersSummary for now.
            PageReference pr = Page.ProviderOrdersSummary_CS;
            pr.setRedirect(true);
            return pr;
        }

        return null;
    }

    /********* Page Buttons/Actions *********/

    /*** Accept Order to function as for both new pending acceptance order and pending re-acceptance order **/
    public String displayExpectedDate {
        get{
            if(orderModel.instance.Estimated_Delivery_Time__c != null){
                // DST_Dates__c daylightAdjustmentDates = DST_Dates__c.getOrdDefaults();
                Datetime newDate = orderModel.instance.Estimated_Delivery_Time__c;
                return newDate.format('MM/dd/yyyy h:mm a','America/New_York');
            }
            return null;
        }set;
    }

    public Boolean futureDOSHeadsUp = true;

    public PageReference acceptOrder(){

       System.debug('acceptOrder: Attempting to accept with time: ' + estimatedDate);

        DateTime parsedTime;
        // Integer clickCounter = 1;

        //Reset errors
        errorFlag = false;
        errorMessage = '';

        //Check if the time is entered
        if(estimatedDate == null){
            errorFlag = true;
            errorMessage = 'Please enter a time.';
            System.debug('Please enter a time.');
            return null;
        }

        //Validate the time
        try{
            parsedTime = DateTime.parse(estimatedDate);
        }
        catch(Exception e){
            errorFlag = true;
            errorMessage = 'The time is invalid, please check the format.';
            System.debug('The time is invalid, please check the format.');
            return null;
        }

        system.debug('acceptOrder: estimatedDate: ' + parsedTime + ' now: ' + System.now());

        //Validate parsed time
        if(parsedTime < System.now()){
            errorFlag = true;
            errorMessage = 'Estimated delivery times must be in the future.';
            System.debug('acceptOrder: Time is null or before now. Estimated delivery times must be in the future.');
            return null;
        }

        // Check 2 : If order is in Pending Re-acceptance, the user should fill in the reason whenever he tries to change previous date

        if(orderModel.instance.Status__c == 'Pending Re-acceptance' && orderModel.instance.Estimated_Delivery_Time__c != parsedTime && String.isBlank(this.changeEstimatedDateInfo)){

            errorFlag = true;
            errorMessage = Label.ExpectedDateWasChanged;
            System.debug(LoggingLevel.ERROR, 'Validation for entered reason during changing Expected Delibery Date');
//            futureDOSHeadsUp = false;
            return null;
        }

        // Update order

        orderModel.instance.Estimated_Delivery_Time__c = parsedTime;
        orderModel.instance.Expiration_Time__c = null;

        if(orderModel.instance.Status__c == 'Pending Re-acceptance') {

            orderModel.instance.ReacceptanceDate__c = Datetime.now();

            if(String.isNotBlank(this.changeEstimatedDateInfo)) {

               addNewJSONMessageHistory(this.changeEstimatedDateInfo);   
            }
        }else {

            System.debug('acceptOrder: Successfully set estimate delivery time to ' + parsedTime);

            orderModel.instance.Accepted_By__c = portalUser.instance.Id;
            orderModel.instance.Accepted_Date__c = DateTime.now();
        }

        orderModel.changeStatus(portalUser.instance,OrderModel_CS.STATUS_PROVIDER_ACCEPTED);
        OrderTracking__c orderTracking;

        if(orderModel.instance.Estimated_Delivery_Time__c != parsedTime) {

            String orderStatus = OrderModel_CS.STATUS_PROVIDER_ACCEPTED;
            orderTracking = initOrderTracking(this.OrderModel.instance);
            orderTracking.Change_Note__c = this.changeEstimatedDateInfo;
            orderTracking.Current_Status__c = orderStatus;
            orderTracking.ExpectedDateFrom__c = this.orderModel.instance.Estimated_Delivery_Time__c;
            orderTracking.ExpectedDateTo__c = parsedTime;
            if(orderModel.instance.Status__c == 'Pending Re-acceptance'){
                orderTracking.Activity_Type__c = 'Order Re-accepted';
                orderTracking.Previous_Accepted_By__c = this.orderModel.instance.Accepted_By__c;
                orderTracking.Re_Accepted_By__c = this.portalUser.instance.Id;
            }else{
                orderTracking.Activity_Type__c = 'Order Accepted';
            }
        }

        update this.orderModel.instance;

        if(orderTracking != null){

            insert orderTracking;
        }

        return null;    
    }

    public PageReference rejectOrder(){

        //Reset errors
        errorFlag = false;
        errorMessage = '';

        //Check for rejection details
        if(String.isBlank(rejectionReason)){
            errorFlag = true;
            errorMessage = 'Please select a reason for rejection.';

            System.debug('rejectOrder Error: Please select a reason for rejection.');
            return null;
        }

        System.debug('rejectOrder: Rejected the order: ' + rejectionReason + ' / ' + otherRejectionReason);

        //Status change
        orderModel.changeStatus(portalUser.instance, rejectionReason);

        //New information
        orderModel.instance.Reason_for_Rejection__c = otherRejectionReason;

        //Find the most recent provider assignment and unassign it
        List<Order_Assignment__c> assignments = [
            SELECT
                Active__c,
                Unassignment__c,
                Unassigned_At__c
            FROM Order_Assignment__c
            WHERE Order__c = :orderModel.instance.Id
            ORDER BY CreatedDate DESC
        ];

        if(assignments.size() > 0){
            Order_Assignment__c mostRecentAssignment = assignments[0];

            mostRecentAssignment.Active__c = false;
            mostRecentAssignment.Unassignment__c = 'Rejected';
            mostRecentAssignment.Unassigned_At__c = DateTime.now();

            update mostRecentAssignment;
        }

        update orderModel.instance;

        return Page.ProviderOrdersSummary_CS;
    }

    public PageReference markDelivered(){

        //Reset errors
        errorMessage = '';
        errorFlag = false;

        System.debug('markDelivered: Entered delivery date: ' + deliveredDate);

        DateTime parsedTime;

        if(String.isBlank(deliveredDate)){
            errorFlag = true;
            errorMessage = 'Please enter a delivery date.';

            System.debug('markDelivered: Please enter a delivery date.');
            return null;
        }

        //Parse the entered date/time into the order field
        try{
            parsedTime = datetime.parse(deliveredDate);
        }catch(Exception e){
            errorFlag = true;
            errorMessage = 'The time is invalid, please check the format.';

            System.debug('The time is invalid, please check the format.');
            return null;
        }

        //Error if the entered time is in the future
        if(parsedTime > system.now()){

            errorFlag = true;
            errorMessage = 'The entered time for delivery must take place before the current time.';

            System.debug('markDelivered: The entered time for delivery must take place before the current time.');
            return null;
        }


        System.debug('markDelivered: Successfully delivered on: ' + parsedTime);

        orderModel.instance.Delivery_Date_Time__c = parsedTime;
        orderModel.instance.Delivery_Method__c = 'Store Personnel';

        orderModel.changeStatus(portalUser.instance,OrderModel_CS.STATUS_DELIVERED);

        update orderModel.instance;

        return null;
    }

    // added by VK on 12/19
    
    public PageReference changeEstimatedDeliveryDate(){
        //Reset errors
        this.errorMessage = '';
        this.errorFlag = false;

        System.debug('changeEstimatedDeliveryDate: Entered delivery date: ' + this.estimatedDelDate);
        System.debug('changeEstimatedDeliveryDate: Entered delivery date note: ' + this.changeDeliveryReason);

        DateTime parsedTime;

        if (String.isBlank(this.estimatedDelDate)) {
            this.errorFlag = true;
            this.errorMessage = Label.NoExpectedDeliveryDate;
            System.debug('changeEstimatedDeliveryDate: ' + this.errorMessage);
            return null;
        }

        //Parse the entered date/time into the order field
        try {
            parsedTime = datetime.parse(this.estimatedDelDate);
        } catch(Exception e) {
            this.errorFlag = true;
            this.errorMessage = Label.BadDatetimeFormat;
            System.debug(this.errorMessage);
            return null;
        }

        //Error if the entered time is in the future
        if (parsedTime < System.now()) {
            this.errorFlag = true;
            this.errorMessage = Label.DeliveryDateMustBeInTheFuture;
            System.debug('acceptOrder: Time is null or before now. ' + this.errorMessage);
            return null;
        }

        if(this.orderModel.instance.Stage__c == 'Future DOS' && parsedTime < Datetime.now().addDays(7)) {
            this.errorFlag = true;
            this.errorMessage = 'This order\'s expected delivery date is within 7 days. To change expected delivery, please click \'Unmark as future DOS\'.';
            System.debug('acceptOrder: Time is null or before now. ' + this.errorMessage);
            return null;
        }

        /****Currently ND Team does not need the reason to be required - VK ****/
        
        /*if (String.isEmpty(this.changeDeliveryReason)) {
            this.errorFlag = true;
            this.errorMessage = Label.ChangeDeliveryDateNoteEmpty;
            System.debug('changeEstimatedDeliveryDate: ' + this.errorMessage);
            return null;
        }*/

        //Create order tracking record
        String orderStatus = OrderModel_CS.STATUS_PROVIDER_ACCEPTED;
        OrderTracking__c orderTracking = initOrderTracking(this.OrderModel.instance);
        orderTracking.Change_Note__c = this.estimatedDelDate;
        orderTracking.Current_Status__c = orderTracking.Previous_Status__c;
        orderTracking.ExpectedDateFrom__c = this.orderModel.instance.Estimated_Delivery_Time__c;
        orderTracking.ExpectedDateTo__c = parsedTime;
        orderTracking.Activity_Type__c = 'Change in Estimated Delivery Date';

        this.orderModel.instance.Estimated_Delivery_Time__c = parsedTime;
        addNewJSONMessageHistory(this.changeDeliveryReason);
        update this.orderModel.instance;
        //Insert order tracking record
        insert orderTracking;
        return null;
    }

    public PageReference markShipped(){

        //Reset errors
        errorMessage = '';
        errorFlag = false;

        if(String.isBlank(deliveryMethod)){
            errorFlag = true;
            errorMessage = 'Please select a delivery method.';

            System.debug('markShipped: Please select a delivery method.');
            return null;
        }

        if(String.isBlank(trackingNumber)){
            errorFlag = true;
            errorMessage = 'Please enter a tracking number.';

            System.debug('markShipped: Please enter a tracking number.');
            return null;
        }

        System.debug('markShipped: Valid shipping info: ' + deliveryMethod + ' / ' + trackingNumber);

        orderModel.instance.Delivery_Method__c = deliveryMethod;
        orderModel.instance.Tracking_Number__c = trackingNumber;
        orderModel.instance.Delivery_Date_Time__c = DateTime.now();

        orderModel.changeStatus(portalUser.instance,OrderModel_CS.STATUS_SHIPPED);

        update orderModel.instance;

        return null;
    }

    // Sprint 2/13 - Added by Dima

    public void changeFutureDos() {
        System.debug('\n\n makedAsFutureDos start \n');
        System.debug('\n\n this.futureDosNote => ' + this.futureDosNote + '\n');
        System.debug('\n\n this.isMarkedAsFutureDos => ' + this.isMarkedAsFutureDos + '\n');

        resetErrors();

        if (this.isMarkedAsFutureDos == null) return;

        if (String.isEmpty(this.futureDosNote) && this.isMarkedAsFutureDos) {
            this.errorFlag = true;
            this.errorMessage = Label.NoFutureDosNoteEntered;
            return;
        }

        if (String.isNotEmpty(this.futureDosNote)) {
            addNewJSONMessageHistory(this.futureDosNote);
        }

        String orderStatus;
        OrderTracking__c orderTracking = initOrderTracking(this.OrderModel.instance);
        if (this.isMarkedAsFutureDos == true) {
            orderStatus = OrderModel_CS.STATUS_FUTURE_DOS;
            orderTracking.Activity_Type__c = 'Order marked as Future DOS';
        }
        else {
            orderStatus = OrderModel_CS.STATUS_PENDING_REACCEPTANCE;
            orderTracking.Activity_Type__c = 'Order unmarked from Future DOS';
        }
        this.orderModel.changeStatus(this.portalUser.instance, orderStatus);

        update orderModel.instance;

        orderTracking.Change_Note__c = this.futureDosNote;
        orderTracking.Current_Status__c = orderStatus;
        insert orderTracking;
    }

    private void resetErrors() {
        this.errorMessage = '';
        this.errorFlag = false;
    }

    private OrderTracking__c initOrderTracking(Order__c order) {
        OrderTracking__c orderActivityTracking = new OrderTracking__c(
            Order__c = order.Id
            , Provider__c = order.Provider__c
            , Changed_Order_By__c = this.portalUser.instance.Id
            , Date_Order_Changed__c = Datetime.now()
            , Previous_Status__c = order.Status__c
        );
        return orderActivityTracking;
    }

    /********* Notes Section *********/
    private void addNewJSONMessageHistory(String newNoteString) {
        this.orderModel.instance.Message_Body__c = newNoteString;
        OrderJSONFactory_CS.processOrderMessage(this.portalUser.instance, this.orderModel.instance);
        this.orderModel.instance.Message_Body__c = '';
        this.orderModel.instance.LastModifiedDate_Notes__c = DateTime.now();
    }

    public void createNewMessage(){

        System.debug('Creating new message.\nMessage: ' + noteBody + '\nFlagAttention : ' + flagAttention +
            '\nAttention reason: ' + attentionReason + '\nUnFlagAttention: ' + unflagAttention);

        //Reset error messages
        noteErrorMsg = '';

        //Do not allow blank notes unless flagging or unflagging.
        if(String.isBlank(noteBody) && !flagAttention && !unflagAttention){
            System.debug('ERROR: No note body.');

            //Set page error
            noteErrorMsg = 'Please enter contents for the note.';

            return;
        }

        //If flagging/unflagging attention
        if(flagAttention){

            //Verify status is selected
            if(String.isBlank(attentionReason)){
                System.debug(LoggingLevel.ERROR,'ERROR: No reason selected for flagging attention.');

                //Set page error message
                noteErrorMsg = 'Please select a reason for attention.';

                return;
            }

            orderModel.changeStatus(portalUser.instance,attentionReason);
            /*
            if(attentionReason == OrderModel_CS.STATUS_NEEDS_ATTENTION_INCOMPLETE_DELIVERY_INFO){
                orderModel.changeStatus(portalUser.instance, OrderModel_CS.STATUS_NEEDS_ATTENTION_INCOMPLETE_DELIVERY_INFO);
            }
            else if(attentionReason == OrderModel_CS.STATUS_NEEDS_ATTENTION_PATIENT_REFUSED_OTHER){
                orderModel.changeStatus(portalUser.instance, OrderModel_CS.STATUS_NEEDS_ATTENTION_PATIENT_REFUSED_OTHER);
            }
            else if(attentionReason == OrderModel_CS.STATUS_NEEDS_ATTENTION_OTHER){
                orderModel.changeStatus(portalUser.instance, OrderModel_CS.STATUS_NEEDS_ATTENTION_OTHER);
            }
            */

        } else if(unflagAttention){

            String currentStatus = orderModel.instance.Status__c;

            if(orderHistory.size() < 2){
                System.debug(LoggingLevel.ERROR,'ERROR: Order has no status before current.');
                return;
            }


            String lastStatus = orderHistory[1].orderStatus;

            Boolean changeResult = orderModel.changeStatus(portalUser.instance, lastStatus);

            if(!changeResult){
                System.debug(LoggingLevel.ERROR,'ERROR: Order status change failed from ' + currentStatus + ' to ' + lastStatus);
                return;
            }
        }

        //Valid Note, be sure to quite before getting here if not
        addNewJSONMessageHistory(this.noteBody);

        update orderModel.instance;

        //Reset the note on the page
        noteBody = '';
        attentionReason = '';
        flagAttention = false;
        unflagAttention = false;

        //Update the order model
        Id orderId = orderModel.instance.Id;
        orderModel = new OrderModel_CS(providerOrderServices.getOrderById(orderId));



        /*
        System.debug('Creating new message.\nMessage: ' + noteBody + '\nFlagAttention : ' + flagAttention +
            '\nAttention reason: ' + attentionReason + '\nUnFlagAttention: ' + unflagAttention);

        if(noteBody != null && noteBody != ''){

            //Check against the current state
            //  If attention is needed and the order is not in needs attention, transition
            //  If no attention is needed and the order is in needs attention, transition
            if(flagAttention && orderModel.instance.Stage__c != OrderModel_CS.STAGE_NEEDS_ATTENTION){

                if(attentionReason == OrderModel_CS.STATUS_NEEDS_ATTENTION_INCOMPLETE_DELIVERY_INFO){
                    System.debug('New message changing status to ' + OrderModel_CS.STATUS_NEEDS_ATTENTION_INCOMPLETE_DELIVERY_INFO);
                    orderModel.changeStatus(portalUser.instance, OrderModel_CS.STATUS_NEEDS_ATTENTION_INCOMPLETE_DELIVERY_INFO);
                }
                else if(attentionReason == OrderModel_CS.STATUS_NEEDS_ATTENTION_PATIENT_REFUSED_WRONG_PRODUCT){
                    System.debug('New message changing status to ' + OrderModel_CS.STATUS_NEEDS_ATTENTION_PATIENT_REFUSED_WRONG_PRODUCT);
                    orderModel.changeStatus(portalUser.instance, OrderModel_CS.STATUS_NEEDS_ATTENTION_PATIENT_REFUSED_WRONG_PRODUCT);
                }
                else if(attentionReason == OrderModel_CS.STATUS_NEEDS_ATTENTION_PATIENT_REFUSED_OTHER){
                    System.debug('New message changing status to ' + OrderModel_CS.STATUS_NEEDS_ATTENTION_PATIENT_REFUSED_OTHER);
                    orderModel.changeStatus(portalUser.instance, OrderModel_CS.STATUS_NEEDS_ATTENTION_PATIENT_REFUSED_OTHER);
                }
                else{
                    System.debug('createNewMessage Error: Please select a reason for needs attention.');

                    //Quit early
                    return null;
                }
            }
            else if(unflagAttention && orderModel.instance.Stage__c == OrderModel_CS.STAGE_NEEDS_ATTENTION){

                String currentStatus = orderModel.instance.Status__c;
                String lastStatus = orderHistory[1].orderStatus;

                if(orderModel.changeStatus(portalUser.instance, lastStatus)){
                    System.debug('createNewMessage Error: Moved from ' + currentStatus +
                    ' to ' + lastStatus);
                }
                else{
                    System.debug('createNewMessage Error: Failed to move from' + currentStatus +
                    ' to ' + lastStatus);

                    //Quit early
                    return null;
                }
            }
        }
        else{
            System.debug('createNewMessage Error: No note entered.');

            //Quit early
            return null;
        }

        //Valid Note, be sure to quite before getting here if not
        orderModel.instance.Message_Body__c = noteBody;
        OrderJSONFactory_CS.processOrderMessage(portalUser.instance, orderModel.instance);
        orderModel.instance.Message_Body__c = '';

        orderModel.instance.LastModifiedDate_Notes__c = DateTime.now();

        noteBody = '';
        //attention = (orderModel.instance.Stage__c == OrderModel_CS.STAGE_NEEDS_ATTENTION ? true : false);

        update orderModel.instance;

        return null;
        */
    }//End createNewMessage

    /******* Misc Functions *******/
    public PageReference print(){
        PageReference pr = Page.ProviderOrderSummaryPDF_CS.setRedirect(true);
        pr.getParameters().put('oid',orderModel.instance.Id);
        return pr;
    }

    public void uploadAttachment(){

        attachment.OwnerId = UserInfo.getUserId();
        attachment.ParentId = this.orderModel.instance.Id;
        attachment.IsPrivate = false;

        try{
            System.debug('Parent Id'+attachment.ParentId);
            System.debug('OwnerId'+attachment.OwnerId);
            insert attachment;
        }
        catch(DMLException e){
            attachmentUploadMessage = 'Error uploading attachment';
            return;
        }

        attachmentUploadMessage = 'Attachment uploaded successfully';

        attachment = new Attachment();
    }

    public void deleteAttachment(){

        System.debug('selectedAttachment: ' + selectedAttachmentId);

        if(String.isNotBlank(selectedAttachmentId)){
            for(Attachment a : currentAttachments){
                System.debug('file id: ' + a.Id);

                if(String.valueOf(a.Id) == selectedAttachmentId){
                    delete a;
                }
            }
        }
    }
}