@isTest
public class ProviderLocationGeocodeBatchTest_CS {
public static Set<Id> providerLocationIdSet;
public static List< Provider_Location__c > providerLocationList;
public static List<Account> providerList;

public static void init(){

	providerList = TestDataFactory_CS.generateProviders('test', 1);
	insert providerList;

	providerLocationList = TestDataFactory_CS.generateProviderLocations(providerList.get(0).Id, 2);

	providerLocationList.get(0).Location_Street__c = '111 S Test Street';
	providerLocationList.get(0).Location_City__c = 'Test';
	providerLocationList.get(0).Location_State__c = 'TN';
	providerLocationList.get(0).Location_Zip_Code__c = '22222';

	providerLocationList.get(1).Location_Street__c = '789 N Abc Rd';
	providerLocationList.get(1).Location_State__c = 'MN';
	insert providerLocationList;

	providerLocationList.get(0).MALatitude__c = 0;
	providerLocationList.get(0).MALongitude__c = 0;

	providerLocationList.get(1).MALatitude__c = 0;
	providerLocationList.get(1).MALongitude__c = 0;
	update providerLocationList;

}

//public class MyMock implements HttpCalloutMock {
	//Boolean isMockSuccess;
	//public MyMock(Boolean isMockSuccess) {
		//this.isMockSuccess = isMockSuccess;
	//}
	//public HTTPResponse respond(HTTPRequest req) {
		//System.debug('HttpCalloutMock ran!');
		//HttpResponse res = new HttpResponse();
		//res.setHeader('Content-Type', 'application/json');
		//res.setBody('{"info":{"statuscode":0,"copyright":{"text":"\u00A9 2015 MapQuest, Inc.","imageUrl":"http://api.mqcdn.com/res/mqlogo.gif","imageAltText":"\u00A9 2015 MapQuest, Inc."},"messages":[]},"options":{"maxResults":-1,"thumbMaps":false,"ignoreLatLngInput":false},"results":[{"providedLocation":{"location":"40 W Baseline Rd, Tempe, AZ 85283, United States, "},"locations":[{"street":"40 W Baseline Rd","adminArea6":"","adminArea6Type":"Neighborhood","adminArea5":"Tempe","adminArea5Type":"City","adminArea4":"Maricopa","adminArea4Type":"County","adminArea3":"AZ","adminArea3Type":"State","adminArea1":"US","adminArea1Type":"Country","postalCode":"85283","geocodeQualityCode":"P1AAA","geocodeQuality":"POINT","dragPoint":false,"sideOfStreet":"R","linkId":"48400000516037","unknownInput":"","type":"s","latLng":{"lat":33.378221,"lng":-111.939722},"displayLatLng":{"lat":33.378221,"lng":-111.939722}}]}]}');
		//res.setStatusCode(this.isMockSuccess ? 200 : 400);
		//return res;
	//}
//}

static testMethod void providerLocationGeocodeBatchTest(){
	init();

	List< Provider_Location__c > geocodedProviderLocationList;

	//Test.setMock(HttpCalloutMock.class, new myMock(true));
	ProviderLocationGeocodeBatch_CS c = new ProviderLocationGeocodeBatch_CS();

	System.Test.startTest();
	Database.executeBatch(c,2);
	System.Test.stopTest();

	Set<Id> resultIds = (new Map<Id,SObject>(providerLocationList)).keySet();

	geocodedProviderLocationList = [SELECT Id, MALatitude__c, MALongitude__c FROM Provider_Location__c WHERE Id IN :resultIds ORDER BY Location_Street__c ASC ];

	System.assertEquals( 1, geocodedProviderLocationList.get(0).MALatitude__c);
	System.assertEquals( -1, geocodedProviderLocationList.get(0).MALongitude__c);

	System.assertEquals( 40, geocodedProviderLocationList.get(1).MALatitude__c);
	System.assertEquals( -40, geocodedProviderLocationList.get(1).MALongitude__c);
}
}