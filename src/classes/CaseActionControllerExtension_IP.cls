public with sharing class CaseActionControllerExtension_IP {

    /*public CaseReasonComponentController_IP reasonCompController {set;
        get{
            if(getComponentControllerMap() != null) {
                CaseReasonComponentController_IP crc;
                crc = (CaseReasonComponentController_IP)getComponentControllerMap().get('reasonComponent');
                if(crc != null) {
                    return crc;
                }
            }
            return new CaseReasonComponentController_IP();
        }
    }

    public CaseActionandDetailsController_IP actionandDetails{set;
        get{
            if(getComponentControllerMap() != null) {
                CaseActionandDetailsController_IP actions;
                actions = (CaseActionandDetailsController_IP) getComponentControllerMap().get('actionComponent');
                if(actions != null) {
                    return actions;
                }
            }

            return new CaseActionandDetailsController_IP();
        }
    }

    public CaseActionControllerExtension_IP(ApexPages.StandardController stdController) {
        caseLog.userAction = (Case_Action__c)stdController.getRecord();
        String cId = ApexPages.currentPage().getParameters().get('cId') != null ? ApexPages.currentPage().getParameters().get('cId') : ApexPages.currentPage().getParameters().get('retURL').replace('%2F','').replace('/','');
        String splitId = cId.substringBefore('?');
        System.debug('Id'+splitId);
        caseLog.caseDetails = [SELECT Id, Account.Name, Contact.Name, RecordTypeId, CaseNumber, Status, Subject, Description, CaseReason__c, CaseReasonDetail__c, Last_Case_Comment__c, CallerType__c,
                      CaseReasonId__c, CaseReasonDetailId__c, CaseActionId__c, CreatedDate
                      FROM Case 
                      WHERE Id=: (Id)splitId LIMIT 1];

        caseLog.userAction.Case__c = caseLog.caseDetails.Id;
    }

    public PageReference save(){
        // If case reason and reason details are not filled in, fill them
        if(String.isBlank(caseLog.caseDetails.CaseReason__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Case Reason needed to proceed'));
            return null;
        }

        if(String.isBlank(caseLog.caseDetails.CaseReasonDetail__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Case Reason Detail needed to proceed'));
            return null;
        }

        if(String.isBlank(this.actionandDetails.actionSelected)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Action is needed to proceed'));
            return null;
        }

        if(String.isBlank(this.actionandDetails.actionDetailSelected)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Action Detail needed to proceed'));
            return null;
        }

        if(String.isBlank(caseLog.caseDetails.CaseReason__c) && String.isNotBlank(reasonCompController.reasonSelected)){
            caseLog.caseDetails.CaseReason__c = CaseReasonRelationshipHelper_IP.getCaseReasonValue((Id)this.reasonCompController.reasonSelected).Reason__c;
            caseLog.caseDetails.CaseReasonId__c = CaseReasonRelationshipHelper_IP.getCaseReasonValue((Id)this.reasonCompController.reasonSelected).Id;
        }

        if(String.isBlank(caseLog.caseDetails.CaseReasonDetail__c) && String.isNotBlank(reasonCompController.reasonDetailSelected)){
            caseLog.caseDetails.CaseReasonDetail__c = CaseReasonRelationshipHelper_IP.getCaseReasonDetailValue((Id)this.reasonCompController.reasonDetailSelected).CaseReasonDetail__c;
            caseLog.caseDetails.CaseReasonDetailId__c = CaseReasonRelationshipHelper_IP.getCaseReasonDetailValue((Id)this.reasonCompController.reasonDetailSelected).Id;

            update caseLog.caseDetails;
        }

        if(String.isNotBlank(actionandDetails.actionDetailSelected)){
            ActionDetail__c actToUpdate = CaseReasonRelationshipHelper_IP.getActionValues((Id)this.actionandDetails.actionDetailSelected);

            caseLog.userAction.CaseAction__c = actToUpdate.Action__c;
            caseLog.userAction.CaseActionDetail__c = actToUpdate.ActionDetail__c;
            caseLog.userAction.Case_Comment__c = this.actionandDetails.userComments;

            insert caseLog.userAction;
        }


        return new PageReference('/'+caseLog.caseDetails.Id);
    }

    public PageReference cancel(){
        return new PageReference('/'+caseLog.caseDetails.Id);
    }

    /*public List<SelectOption> getActions(){
        List<SelectOption> options = new List<SelectOption>();
        try {

            if((CaseReasonRelationshipHelper_IP.actionAndDetails((String)parentCase.CaseReasonDetail__c)).size() > 0) {
            for(String s: CaseReasonRelationshipHelper_IP.actionAndDetails((String)parentCase.CaseReasonDetail__c).keySet()) {
                options.add(new SelectOption(s,s));
            }
            options.sort();
        }else{
            options.add(new SelectOption('','---None---'));
        }
        } catch(Exception e) {
            System.debug(e.getMessage());
        } 
        
        return options;
    }

    public List<SelectOption> getActionDetails(){
        List<SelectOption> options = new List<SelectOption>();
        if(!(CaseReasonRelationshipHelper_IP.actionDetailsBasedOnAction((String)parentCase.CaseReasonDetail__c, actionSelected)).isEmpty()) {
            for(String s: CaseReasonRelationshipHelper_IP.actionDetailsBasedOnAction((String)parentCase.CaseReasonDetail__c, actionSelected)) {
                options.add(new SelectOption(s,s));
            }
            options.sort();
        }else{
            options.add(new SelectOption('','---None---'));
        }
        
        return options;
    }

    public PageReference save(){
        resetErrorFlag();

        if(String.isEmpty(actionSelected)) {
            raiseFlag = true;
            errorList.add('ACTION IS REQUIRED TO PROCEED');
            logAction.CaseAction__c.addError('PLEASE REVIEW THE ERRORS');
        }
        if(String.isEmpty(actionDetailSelected)) {
            raiseFlag = true;
            errorList.add('ACTION DETAIL IS REQUIRED TO PROCEED');
            logAction.CaseActionDetail__c.addError('PLEASE REVIEW THE ERRORS');
        }
        if(parentCase.RecordTypeId == RecordTypes.caseTypes.get('Claims Email').getRecordTypeId() ||
           parentCase.RecordTypeId == RecordTypes.caseTypes.get('ClearHelp Email').getRecordTypeId() ||
           parentCase.RecordTypeId == RecordTypes.caseTypes.get('Email').getRecordTypeId())
        {
            if(String.isNotBlank(logAction.Case_Comment__c) && logAction.Case_Comment__c.length() > 1000){
                raiseFlag = true;
                characterLimitErrorMsg = 'The comments cannot be longer than 1000 characters';
            }
        }else{
            if(String.isNotBlank(logAction.Case_Comment__c) && logAction.Case_Comment__c.length() > 500){
                raiseFlag = true;
                characterLimitErrorMsg = 'The comments cannot be longer than 500 characters';
            }
        }

        if(!raiseFlag) {
            logAction.CaseAction__c = actionSelected;
            logAction.CaseActionDetail__c = actionDetailSelected;

            Database.SaveResult sr = Database.insert(logAction, true);
            if(sr.isSuccess()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'A new case action has been created'));
                return new PageReference('/'+parentCase.Id);
            } else {
              for(Database.Error e: sr.getErrors()) {
                String error = '';
                System.debug(e.getStatusCode() + ': ' + e.getMessage());
                System.debug('Fields that affected this error: ' + e.getFields());
                error = e.getStatusCode()+':'+ ' '+e.getMessage();
                error+= ' '+ 'Fields that affected:'+' '+e.getFields();

                raiseFlag = true;
                errorList.add(error);
            }
            return null;
          }
        }
        return null;
    }

    public void resetErrorFlag(){
        raiseFlag = false;
        errorList = new String[]{};
    }*/
}