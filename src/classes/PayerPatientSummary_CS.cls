public without sharing class PayerPatientSummary_CS extends PayerPortalServices_CS {

	private final String pageAccountType = 'Payer';
	
	private PatientServices_CS patientService;
	private PayerOrderServices_CS orderService;
	
	/******* Interface *******/
	public Plan_Patient__c thisPatient {get;set;}
	
	public ApexPages.Standardsetcontroller orderHistorySetCon {get;set;}
	public List<Order__c> orderHistory {
		get
		{
            if(orderHistorySetCon != null && orderHistorySetCon.getRecords().size() > 0)
            {
                return (List<Order__c>) orderHistorySetCon.getRecords();
            }
            return new List<Order__c>();
        }
        set;
	}
	
	public String selectedOrderId {get;set;}
	
	/******* Constructor/Init *******/
	public PayerPatientSummary_CS(){}
	
	public PageReference init(){
		
		///////////////////////////////////
		// Authentication and Permission
		///////////////////////////////////
			
		if(!isSecure && !Test.isRunningTest()){
			return DoLogout();
		}
			
		if(!authenticated){
			return DoLogout();
		}

		if(portalUser.AccountType != pageAccountType){
			return homepage;
		}
		
		///////////////////////////////////
		// Additional init
		///////////////////////////////////
		
		patientService = new PatientServices_CS(portalUser.instance.Id);
		orderService = new PayerOrderServices_CS(portalUser.instance.Id);
		
		if(ApexPages.currentPage().getParameters().containsKey('pid')){
			String pid = ApexPages.currentPage().getParameters().get('pid');
			
			if(pid != null && pid != ''){
				
				//Id is valid
				thisPatient = patientService.getPlanPatientById(pid);
				
				if(thisPatient == null){
					System.debug('PatientSummary Error: Patient does not exist.');
					return Page.PayerPatientsSummary_CS;
				}
				
				//Patient is valid
				orderHistorySetCon = orderService.getOrdersByPatientId(pid);
				orderHistorySetCon.setPageSize(10);
			}
			else{
				System.debug('PatientSummary Error: Patient Id invalid.');
				return Page.PayerPatientsSummary_CS;
			}
		}
		else{
			System.debug('PatientSummary Error: No patient Id found.');
			return Page.PayerPatientsSummary_CS;
		}
		
		return null;
	}

	public void savePatientInfo(){
		Database.Saveresult result = Database.update(thisPatient);
		
		if(result.isSuccess()){
			System.debug('savePatientInfo: Patient ' + thisPatient.name + ' successfully updated!');
		}
		else{
			System.debug('savePatientInfo: Patient ' + thisPatient.name + ' failed to update.');
		}
	}
}