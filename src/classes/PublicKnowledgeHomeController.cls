public with sharing class PublicKnowledgeHomeController {
	
    public transient List<DataCategory> categories {get;set;}
    
    //
    //	Wrapper Classes
    //
    
    public class DataCategory {
    	
    	public Schema.DataCategory record {get; set;}
    	
    	public String label { get { return record.getLabel(); } }
    	public String name { get { return record.getName(); } }
    	public Integer tempId { get;set; }
    	
    	public List<DataCategory> children {get;set;}
    	public List<Article> articles {get;set;}
    	
    	public DataCategory(Schema.DataCategory record) {
    		this.record = record;
    		this.tempId = Math.abs(Crypto.getRandomInteger());
    		this.children = new List<DataCategory>();
    		this.articles = new List<Article>();
    	}
    }
    
    public class Article {
    	
    	public Tutorial__kav record {get; set;}
    	
    	public String title { 
    		get { 
    			List<String> titleSplit = record.Title.split(': ', 2);
	    		if (titleSplit.size() == 2) {
	    			return titleSplit[1];
	    		}else {
					return record.Title;
	    		}
			} 
		}
    	public String summary { get { return record.Summary; } }
    	public String content { get { return record.Content__c; } }
    	public String articleNumber { 
    		get { 
	    		String shortNumber = record.ArticleNumber; //the article number w/o leading zeroes
    			while (shortNumber.subString(0,1) == '0') {
	    			shortNumber = shortNumber.subString(1);
    			}
    			return shortNumber; 
    		}
    	}
    	
    	public Article(Tutorial__kav record) {
    		this.record = record;
    	}
    }
    
    //
    //	Constructor and Methods
    //
    
    public PublicKnowledgeHomeController() {
    	initDataCategories();
    }
    
    public PageReference init() {

    	return PublicKnowledgeServices.checkForReferrer();
    }
    
    public void initDataCategories() {
    	
    	categories = new List<DataCategory>();
    	
    	Map <String, DataCategory> categoryMap = new Map <String, DataCategory>();
    	List <Schema.Datacategory> topCategories = DataCategoryUtils.getDataCategoriesFromGroup('KnowledgeArticleVersion', 'Que');

    	for(Schema.Datacategory cat : topCategories) {
    		
    		DataCategory wrapper = new DataCategory(cat);
    		
    		//Add this category
    		categories.add(wrapper);
    		categoryMap.put(cat.getName(), wrapper);
    		
    		//Add child categories
    		for(Schema.Datacategory child : cat.getChildCategories()) {
    			
    			DataCategory childWrapper = new DataCategory(child);
    			
    			//Add child category
    			wrapper.children.add(childWrapper);
    			categoryMap.put(child.getName(), childWrapper);
    			
    		}
    	}  	
	            
	    List <Tutorial__kav> tutorialArticles = [
	    		SELECT Title,Summary,Content__c,KnowledgeArticleId,ArticleNumber,
	    		(
	    			SELECT 
	    				Id, 
	    				ParentId, 
	    				DataCategoryGroupName, 
	    				DataCategoryName
    				FROM DataCategorySelections
	    		)
	        	FROM Tutorial__kav
	        	WHERE PublishStatus = 'Online'
	        	AND Language = 'en_US'
	        	WITH DATA CATEGORY que__c BELOW all__c 
	    ];
        
        for(Tutorial__kav article : tutorialArticles) {
        	
        	for (Tutorial__DataCategorySelection cat : article.DataCategorySelections) {
        		
        		//Locate the DataCategory and add this article
        		if(!categoryMap.containsKey(cat.DataCategoryName)){
        			system.debug(LoggingLevel.INFO,'Could not find data category ' + cat.DataCategoryName);
        			continue;
        		}
        		
        		categoryMap.get(cat.DataCategoryName).articles.add(new Article(article));		
        	}
        }
    }
}