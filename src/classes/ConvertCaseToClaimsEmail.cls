public class ConvertCaseToClaimsEmail {

    public static void convertCaseToClaim(Case toConvert) {
        convertCasesToClaims(new List<Case>{toConvert});
    }
    
    public static void convertCasesToClaims(List<Case> toConvert) {
        
        List<Email_Log__c> newEmailLogs  = new List <Email_Log__c>();
	
    	for (Case c : toConvert){
            
            String email = c.Copy_Email_Address__c;
            
            System.debug('The email Id is:' + email);
            
            newEmailLogs.add(new Email_Log__c(
                Email_Account__c = c.Account.Id, 
                Email_Contact__c = c.Contact.Id, 
                From_Email_Address__c = c.Copy_Email_Address__c,
                To_Email_Address__c = 'claims@accessintegra.com',
                Department_Responsible__c = 'Claims', 
                Email_Subject__c = c.Subject,
                Email_Content__c = c.Description, 
                Case_Email_Id__c = c.Id));
        }
        
        System.debug('Number of Email Logs to be created ' + newEmailLogs.size());
    
        try{
            
            insert newEmailLogs;
            
            System.debug('Cases successfully converted to Claims Email Logs');
            
            for (Case ca : toConvert){
                ca.Case_Created__c = true;
            }
            
        } catch(System.Dmlexception e){
            System.debug ('Failed to create Email Logs: ' + e);
        }
    }
}