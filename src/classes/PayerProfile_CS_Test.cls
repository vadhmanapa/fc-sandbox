@isTest
private class PayerProfile_CS_Test {

    /******* Test Parameters *******/
	static private final Integer N_USERS = 2;

	/******* Payer *******/
	static private List<Account> payers;
	static private List<Contact> payerUsers;

	/******* Test Objects *******/
	static private PayerProfile_CS cont;
	public static Contact user;
	public static Account plan;
	public static Account provider;
	public static List<Plan_Patient__c> patientList;

	/******* Valid Phones *******/
	public static String validPhone1;
	public static String validPhone2;
	public static String validPhone3;
	public static String validPhone4;
	public static String validPhone5;
	public static String validPhone6;
	
	/******* Invalid Phones *******/
	public static String invalidPhone1;
	public static String invalidPhone2;
	public static String invalidPhone3;
	
	/******* Valid Emails *******/
	public static String validEmail1;
	public static String validEmail2;
	
	/******* Invalid Emails *******/
	public static String invalidEmail1;
	public static String invalidEmail2;
	
	/******* Test Methods *******/
	
	/*
	//Page Landing without authorization
	static testMethod void PayerProfile_CS_PageLandingNoAuth(){
		
		//init without login
		List<Account> payerList = TestDataFactory_CS.generatePlans('Test Payer', 1);
    	plan = payerList[0];
    	insert payerList;
		user = TestDataFactory_CS.generatePlanContacts('Admin', payerList, 1)[0];
    	insert user;
    	
    	//Page Landing
		PageReference pr = Page.PayerProfile_CS;
		Test.setCurrentPage(pr);
	}

	//Test regex for phone validation (valid numbers)
	static testMethod void PayerProfile_CS_ValidPhone(){
		init();
		createTempInfo();
		
		cont.phone = validPhone1;
		System.assert(cont.updatePhone(), 'vPhone1 invalid');
		
		cont.phone = validPhone2;
		System.assert(cont.updatePhone(), 'vPhone2 invalid');
		
		cont.phone = validPhone3;
		System.assert(cont.updatePhone(), 'vPhone3 invalid');
		
		cont.phone = validPhone4;
		System.assert(cont.updatePhone(), 'vPhone4 invalid');
		
		cont.phone = validPhone5;
		System.assert(cont.updatePhone(), 'vPhone5 invalid');
		
		cont.phone = validPhone6;
		System.assert(cont.updatePhone(), 'vPhone6 invalid');	
	}
	
	//Test regex for phone validation (invalid numbers)
	static testMethod void PayerProfile_CS_invalidPhone(){
		init();
		createTempInfo();
		
		cont.phone = invalidPhone1;
		System.assert(!cont.updatePhone(), 'ivPhone1 valid');
		
		cont.phone = invalidPhone2;
		System.assert(!cont.updatePhone(), 'ivPhone2 valid');
		
		cont.phone = invalidPhone3;
		System.assert(!cont.updatePhone(), 'ivPhone3 valid');
	}
	
	//Test regex for email validation (valid emails)
	static testMethod void PayerProfile_CS_validEmail(){
		init();
		createTempInfo();
		
		cont.email = validEmail1;
		System.assert(cont.updateEmail(), 'vEmail 1 invalid');
		
		cont.email = validEmail2;
		System.assert(cont.updateEmail(), 'vEmail 2 invalid');
	}
	
	//Test regex for email validation (invalid emails)
	static testMethod void PayerProfile_CS_invalidEmail(){
		init();
		createTempInfo();
		
		cont.email = invalidEmail1;
		System.assert(!cont.updateEmail(), 'ivEmail 1 valid');
		
		cont.email = invalidEmail2;
		System.assert(!cont.updateEmail(), 'ivEmail 2 valid');
	}
	
	//Temp information properly updates user
	static testMethod void PayerProfile_CS_updateAllInfo(){
		init();
		createTempInfo();
		
		cont.phone = validPhone1;
		cont.email = validEmail1;
		System.assert(cont.updateAllInfo(), 'Profile not updated from temporary information');
		
		cont.email = invalidEmail1;
		System.assert(!cont.updateAllInfo(), 'Information should not have updated');
		
		cont.phone = invalidPhone1;
		System.assert(!cont.updateAllInfo(), 'Information should not have updated');
		
	}
	*/
	
	//Profile properly saves (or does not)
	static testMethod void PayerProfile_CS_SaveProfile(){
		init();

		cont.saveProfile();
		System.assert(cont.usernameErrorMsg == '', 'Profile not saved');	
	}
	
	//Profile properly saves (or does not)
	static testMethod void PayerProfile_CS_SaveProfileBlankUsername(){
		init();

		cont.username = '';

		cont.saveProfile();
		System.assert(cont.usernameErrorMsg != '', 'Profile not saved');	
	}

	//Profile properly saves (or does not)
	static testMethod void PayerProfile_CS_SaveProfileDuplicateUsername(){
		init();

		cont.username = payerUsers[1].Username__c;

		cont.saveProfile();
		System.assert(cont.usernameErrorMsg != '', 'Profile not saved');	
	}
	
	//All cases of change password method
	static testMethod void PayerProfile_CS_ChangePassword(){
		init();
		String password1 = 'aEwr658#F';
		String password2 = 'Tye37$8Sf';
		String password3 = '6fh38fDB^';
		String password4 = 'bagel';
		
		cont.portalUser.changePassword(password1);
		
		cont.oldPassword = password1;
		cont.newPassword = password2;
		cont.newPasswordConfirm = password3;
		cont.changePassword();
		System.assertEquals(cont.mismatchedPasswords,cont.passwordMessage);
		
		cont.newPassword = password4;
		cont.newPasswordConfirm = password4;
		cont.changePassword();
		System.assertEquals(cont.invalidPassword,cont.passwordMessage);
		
		cont.oldPassword = password2;
		cont.newPassword = password3;
		cont.changePassword();
		System.assertEquals(cont.incorrectOldPassword,cont.passwordMessage);
		
		cont.oldPassword = password1;
		cont.newPassword = password2;
		cont.newPasswordConfirm = password2;
		cont.changePassword();
		System.assertEquals(cont.validPassword,cont.passwordMessage);
		
	}
	
	//Test phone Numbers and emails
	public static void createTempInfo(){
		
		validPhone1 = '8005555555';
		validPhone2 = '(800)5555555';
		validPhone3 = '(800)-555-5555';
		validPhone4 = '(800) - 555 - 5555';
		validPhone5 = '800-555-5555';
		validPhone6 = '800 555 5555';
		
		invalidPhone1 = '800555555';
		invalidPhone2 = '800d555d5555';
		invalidPhone3 = '8005 555 555';
		
		validEmail1 = 'test@email.com';
		validEmail2 = 'test%&W(fs)@test.example';
		
		invalidEmail1 = 'test@emailcom';
		invalidEmail2 = 'testemail.com';
	}
	
	public static void init() {
    	List<Account> payerList = TestDataFactory_CS.generatePlans('Test Payer', 1);
    	plan = payerList[0];
    	insert payerList;
    	
    	payerUsers = TestDataFactory_CS.createPlanContacts('Admin', payerList, N_USERS);
    	user = payerUsers[0];
    	
    	//Page Landing
		PageReference pr = Page.PayerProfile_CS;
		Test.setCurrentPage(pr);
		TestServices_CS.login(user);
		
		//Page init
		cont = new PayerProfile_CS();
		cont.init();
	}
    
}