/**
 *  @Description Batch to transfer Provider Location objects from the old object to the new Provider_Location__c object.
 *		To be run after Provider_Location__c deploy.
 *  @Author Cloud Software LLC
 *  Revision History: 
 *		12-22-2015 - karnold - Added comments, changed call to generic
 *
 */
public class BusinessLicenseBatch_CS implements Database.Batchable<SObject>{ 

	String queryString;

	public BusinessLicenseBatch_CS(String query) {
		this.queryString = query;
	}

	public BusinessLicenseBatch_CS() {
		
		SoqlBuilder query = new SoqlBuilder()
			.selectx('Id')
			.selectx('Provider_Location__c')
			.fromx('Contracts_Licenses__c')
			.wherex(new FieldCondition('Provider_Location_New__c').equals(null));
			
		queryString = query.toSoql();

	}

	public Database.QueryLocator start(Database.BatchableContext context) {
		
		return Database.getQueryLocator(queryString);
	}

	public void execute(Database.BatchableContext context, List<Contracts_Licenses__c> scope) {
		System.debug(scope);
		List<Contracts_Licenses__c> conLicList = (List<Contracts_Licenses__c>) ProviderLocationTransferServices.updateObjectProviderLocation(scope);
		update conLicList;
	}

	public void finish(Database.BatchableContext context) {

	}
}