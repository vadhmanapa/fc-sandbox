/* FINAL ASSIGNMENT PROCESS FLOW */
//Get list of possible providers:
    //IF item in the order is in a Medicare Primary Bid category:
        //1. If an item in the order is in a Medicare Primary Bid category, find the nearest Integra Medicare Primary Winner
        //2. If an item in the order is in a Medicare Primary Bid category and there is no Integra Medicare Primary Winner nearby, find nearest non-Integra medicare primary winner (Manually Assign queue?)
    //IF it is shippable (add field) providerDME
    //ELSE
        //1. GET CLARIFICATION: Look for old orders that have a provider assigned; grab that provider                           
        //2. Find the preffered providers within largest radius of order
        //3. Find the secondary providers within largest radius of order
        //4. Find the base providers within largets radius of order
        //5. Add to Manually Assign Queue
    
//Query for all of the products on the order.
    //If none of the possible providers have all of the products on the order, go to the next provider tier
    
//Assign by driving time
    //Using the driving time api, find the nearest provider and assign

//140211 UPDATE: Limit the functionality for first rendition. If an order has been fulfilled in the past, assign to most recent provider.
//If not, assign to the nearest primary provider (IMM) regardless of products or medicare primary products.

public class ProviderAssignmentServices_CS {
	
	private static final Time DEFAULT_EXPIRATION_WINDOW_BEGIN = Time.newInstance(9,0,0,0);	// 9AM Local time
	private static final Time DEFAULT_EXPIRATION_WINDOW_END = Time.newInstance(17,0,0,0);	// 5PM Local time
	private static final Integer DEFAULT_EXPIRATION_HOURS = 2;
	
	private static final Integer MAX_PREFERRED_SEARCH = 100;
	
    private Contact orderCreator;
    private Order__c orderToAssign;
    private Map<Id,DME__c> orderDMEs;
    private Set<Id> medicarePrimaryDMECatIds;
    private List<Id> previouslyAssignedProviderIds;
    private Id planPatientId;
    private Id patientId;
    private String language;
    private Decimal oLat;
    private Decimal oLng;
    public Decimal maxDistance;
     
    public ProviderAssignmentServices_CS(Id orderId) {
        orderToAssign = [
            SELECT 
                Id,
                Name,
                Provider__c,
                Street__c,
                City__c,
                State__c,
                Zip__c,
                Geolocation__Latitude__s,
                Geolocation__Longitude__s,
                Assignment_History__c,
                Plan_Patient__c,
                Plan_Patient__r.Native_Language__c,
                Plan_Patient__r.Patient__c,
                Plan_Patient__r.Patient__r.Native_Language__c,
                CreatedDate,
                (
                    SELECT DME__c 
                    FROM DMEs__r
                )
            FROM Order__c
            WHERE Id = :orderId
        ];
               
        /*140211
        if(orderToAssign.Plan_Patient__r.Native_Language__c != null) {
            language = orderToAssign.Plan_Patient__r.Native_Language__c;
        }else if(orderToAssign.Plan_Patient__r.Patient__r.Native_Language__c != null) {
            language = orderToAssign.Plan_Patient__r.Patient__r.Native_Language__c;
        }
        */
        
        if(orderToAssign.Assignment_History__c != null) {
            previouslyAssignedProviderIds = orderToAssign.Assignment_History__c.split(',');
        }
        
        planPatientId = orderToAssign.Plan_Patient__c;
        patientId = orderToAssign.Plan_Patient__r.Patient__c;
        //140211 medicarePrimaryDMECatIds = new Set<Id>();
        
        oLat = orderToAssign.Geolocation__Latitude__s;
        oLng = orderToAssign.Geolocation__Longitude__s;
        //140211 getOrderDMEs(orderToAssign.DMEs__r);
        
        doAssignmentProcess();
    }
    
    // This constructor is a shortcut for the interim assignment process
    public ProviderAssignmentServices_CS(Order__c order) {
        
        //system.debug('IN PROVIDER ASSIGNMENT SERVICES');
        
        orderToAssign = order;
        
        system.debug('Order To Assign: ' + orderToAssign);
        
        //Get previously assigned providers
        List<Order_Assignment__c> assignments = [
        	SELECT
        		(
        			SELECT
        				Id,
        				Provider__c
        			FROM Order_Assignments__r
        			ORDER BY CreatedDate DESC
        		)
        	FROM Order__c
        	WHERE Id = :orderToAssign.Id
        ].Order_Assignments__r;
        
        if(assignments != null){
        	System.debug('Found ' + assignments.size() + ' assignments.');
        }
        
        previouslyAssignedProviderIds = new List<Id>();
        
        for(Order_Assignment__c a : assignments){
        	System.debug('Previous Provider: ' + a.Provider__c);
        	previouslyAssignedProviderIds.add(a.Provider__c);
        }
        
        /*
        if(orderToAssign.Assignment_History__c != null) {
            previouslyAssignedProviderIds = orderToAssign.Assignment_History__c.split(',');
        }
        
        if(String.isNotBlank(orderToAssign.Provider__c)){
       		previouslyAssignedProviderIds.add(orderToAssign.Provider__c);
        }
        */
        
        planPatientId = orderToAssign.Plan_Patient__c;

        if(orderToAssign.Plan_Patient__c != null){
        	patientId = orderToAssign.Plan_Patient__r.Patient__c;
        }
        
        oLat = orderToAssign.Geolocation__Latitude__s;        
        oLng = orderToAssign.Geolocation__Longitude__s;
        
        /*
        System.debug('Plan Patient Id: ' + planPatientId);
        System.debug('Patient Id: ' + patientId);
        System.debug('Lat: ' + oLat);
        System.debug('Lng: ' + oLng);
        */
        //doAssignmentProcess();
    }
     
    public Order__c doAssignmentProcess() {

        Id providerToAssign;
        
        /*140211 Integer primaryCatCnt = medicarePrimaryDMECatIds.size();
        //TODO: Check for Dropship
        if(primaryCatCnt == 0) {
            providerToAssign = getRecentOrderProvider();             
        }else if(primaryCatCnt == 1) {
            //providerToAssign = getMedicarePrimaryIntegra();
        } 
        */

        //If patient lookups exist then find their recent providers else get preferred providers                         
        if(patientId != null || planPatientId != null) {
        	System.debug('Finding recent provider for: ' + orderToAssign.Name);

	        //Found recent provider
	        if(String.isNotBlank(providerToAssign = getRecentOrderProvider())){
	            System.debug('Found recent provider: ' + providerToAssign);
	            
	        	orderToAssign.Provider__c = providerToAssign;
	        	orderToAssign.Status__c = OrderModel_CS.STATUS_PROVIDER_ASSIGNED_AUTO;
	        	
	        	//Create the provider assignment
				Order_Assignment__c newAssignment = new Order_Assignment__c(
					Active__c = true,
					Assigned_At__c = DateTime.now(),
					Assignment__c = 'Auto',
					Order__c = orderToAssign.Id,
					Provider__c = providerToAssign
				);
				
				insert newAssignment;
				
				return orderToAssign;
	        }
        }

		System.debug('Finding preferred provider for: ' + orderToAssign.Name);
		if(String.isNotBlank(providerToAssign = getPreferredProviders())){
			System.debug('Found preferred provider: ' + providerToAssign);

			//Expiration
			/*REMOVED TEMPORARILY
			String providerLocale;
			if(String.isNotBlank(providerLocale) = [select Locale__c from Account where Id = :providerToAssign].Locale__c)){
				DateTime currentTime = DateTime.now();
				
				System.debug('Finding expiration with current gmt time: ' + currentTime.formatGmt('MM/dd/yyyy HH:mm') + ' in ' + providerLocale);
	        	orderToAssign.Expiration_Time__c = getExpirationTime(currentTime,providerLocale);
			}*/
			
            orderToAssign.Provider__c = providerToAssign;
            orderToAssign.Status__c = OrderModel_CS.STATUS_PROVIDER_ASSIGNED_AUTO;
        	
			//Create the provider assignment
			Order_Assignment__c newAssignment = new Order_Assignment__c(
				Active__c = true,
				Assigned_At__c = DateTime.now(),
				Assignment__c = 'Auto',
				Order__c = orderToAssign.Id,
				Provider__c = providerToAssign
			);
	
			insert newAssignment;

			return orderToAssign;
		}

		System.debug('Manually assigning: ' + orderToAssign);
	    addToManuallyAssignQueue(orderToAssign);
        
        return orderToAssign;
    }
    
    public Id getRecentOrderProvider() {
        
		//Get the orders that are NOT THE CURRENT ORDER and have THE SAME PLAN PATIENT or related patient with PROVIDERS
        
        NestableCondition conditions = new AndCondition()
        	.add(new FieldCondition('Id').notEquals(orderToAssign.Id))
        	.add(new FieldCondition('Provider__c').notEquals(null))
        	.add(new FieldCondition('Provider__r.Type__c').equals('Provider'))
        	.add(new FieldCondition('Provider__r.Clear_Enabled__c').equals(true)) 
        	//LEGACY 'Active' flag
        	.add(new FieldCondition('Provider__r.Status__c').equals('Active'));
        	
        NestableCondition patientConditions = new OrCondition();
		
		if(String.isNotBlank(planPatientId)){
			patientConditions.add(new FieldCondition('Plan_Patient__c').equals(planPatientId));
		}
		
		if(String.isNotblank(patientId)){
			patientConditions.add(new FieldCondition('Plan_Patient__r.Patient__c').equals(patientId));
		}
		
		if(previouslyAssignedProviderIds != null && previouslyAssignedProviderIds.size() > 0){
        	conditions.add(new SetCondition('Provider__c',Operator.NOT_IN,previouslyAssignedProviderIds));
        }
		
		conditions.add(patientConditions);
		
		SoqlBuilder query = new SoqlBuilder()
			.selectx('CreatedDate')
			.selectx('Provider__c')
			.selectx('Provider__r.Corporate_Geolocation__Latitude__s')
			.selectx('Provider__r.Corporate_Geolocation__Longitude__s')
			.fromx('Order__c')
			.wherex(conditions)
			.orderbyx(new OrderBy('CreatedDate').descending())
			.limitx(1);
        
        Order__c[] recentOrders = database.query(query.toSoql());
        
        System.debug('Found recent orders: ' + recentOrders);
        
        if(recentOrders.size() > 0){
            return recentOrders[0].Provider__c;
        }
        
       	return null;       
    }
    
    public Id getPreferredProviders() {
        //140211 Map<Id,Account> preferredProviders = new Map<Id,Account>(providerQuery('Preferred'));
       	
        if(orderToAssign.Geolocation__Latitude__s != null && orderToAssign.Geolocation__Longitude__s != null) {
            List<Account> preferredProviders = providerQuery('Preferred');
            
            if(preferredProviders != null && preferredProviders.size() > 0) {
                /*140211
                Map<Id, Account> possibleProviders = getProvidersWithAllProducts(preferredProviders);
                if(possibleProviders.size() > 0) {
                    return getNearestProviderId(possibleProviders);
                }
                */
                return preferredProviders[0].Id;
            } 
            //140211 return getSecondaryProviders();
        }
        
        return null;
    }
    
    public static DateTime getExpirationTime(DateTime currentTime, String providerLocale){

		Time expirationWindowBegin;
		Time expirationWindowEnd;
		Integer expirationHours;

        //Expiration Settings
        Expiration_Settings__c expirationSettings = Expiration_Settings__c.getInstance('Default');
		
		System.debug('Expiration Settings: ' + expirationSettings);
		
		if(expirationSettings != null){
			expirationWindowBegin = Time.newInstance(Integer.valueOf(expirationSettings.Window_Begin__c),0,0,0);
			expirationWindowEnd = Time.newInstance(Integer.valueOf(expirationSettings.Window_End__c),0,0,0);
			expirationHours = Integer.valueOf(expirationSettings.Hours__c);
		}
		else{
			expirationWindowBegin = DEFAULT_EXPIRATION_WINDOW_BEGIN;
			expirationWindowEnd = DEFAULT_EXPIRATION_WINDOW_END;
			expirationHours = DEFAULT_EXPIRATION_HOURS;
		}
    	
    	DateTimeModel_CS currentTimeLocal = new DateTimeModel_CS(currentTime,providerLocale);
    	DateTimeModel_CS expiresAt = new DateTimeModel_CS(currentTime,providerLocale);
    	
    	Date localDate = currentTimeLocal.localDate();
    	Time localTime = currentTimeLocal.localTime();

     	DateTimeModel_CS windowStart = DateTimeModel_CS.newInstanceFromLocal(localDate, expirationWindowBegin, providerLocale);
     	DateTimeModel_CS windowEnd = DateTimeModel_CS.newInstanceFromLocal(localDate, expirationWindowEnd, providerLocale);
     	
     	//Find the difference between the window end and expiring hours
     	DateTimeModel_CS windowEarlyEnd = DateTimeModel_CS.newInstanceFromLocal(localDate, expirationWindowEnd.addHours(-1*expirationHours), providerLocale);
     	
     	//System.debug('Order placed at: ' + currentTimeLocal.format('EEE MM/dd/yyyy HH:mm aa zzzz'));	
     	
     	//Detect if order placed outside of window, if so advance to 9AM Monday
    	if(currentTimeLocal.format('EEE').equals('Sat')){
    		expiresAt = DateTimeModel_CS.newInstanceFromLocal(currentTime.date().addDays(2), expirationWindowBegin, providerLocale);
    	}
    	else if(currentTimeLocal.format('EEE').equals('Sun')){
    		expiresAt = DateTimeModel_CS.newInstanceFromLocal(currentTime.date().addDays(1), expirationWindowBegin, providerLocale);
    	}
		
    	//Case 1: Within window 9AM - 3PM
    	if(expiresAt.dt >= windowStart.dt && expiresAt.dt <= windowEarlyEnd.dt){
    		//System.debug('Inside window.');
    		
    		//Add 2 hours
    		expiresAt = currentTimeLocal.addHours(2);
    	}
    	//Case 2: Exceeds local window
    	else if(expiresAt.dt > windowEarlyEnd.dt && expiresAt.dt <= windowEnd.dt){
    		//System.debug('Will exceed window.');
    		
    		//Add 2 hours then 16 hours
    		expiresAt = currentTimeLocal.addHours(18);
    	}
    	//Case 3: Outside local window, after 5PM
    	else if(expiresAt.dt > windowEnd.dt){
			//System.debug('After window.');
			
    		//Set to 9AM the following day then add 2 hours
    		expiresAt = DateTimeModel_CS.newInstanceFromLocal(currentTimeLocal.localDate().addDays(1), expirationWindowBegin, providerLocale).addHours(2);
    	}
    	//Case 4: Outside local window, before 9AM
    	else if(expiresAt.dt < windowStart.dt){
    		//System.debug('Before window.');
    		
    		//Set to 9AM then add 2 hours
    		expiresAt = DateTimeModel_CS.newInstanceFromLocal(currentTimeLocal.localDate(), expirationWindowBegin, providerLocale).addHours(2);
    	}
    	
		//If the order expires on Saturday or Sunday move it to Monday
		if(expiresAt.format('EEE').equals('Sat')){
			//System.debug('On a Saturday.');
			
			expiresAt = expiresAt.addDays(2);
		}
		else if(expiresAt.format('EEE').equals('Sun')){
			//System.debug('On a Sunday.');
			
			expiresAt = expiresAt.addDays(1);
		}
    	
    	//System.debug('Set expiration to: ' + expiresAt.format('EEE MM/dd/yyyy HH:mm aa zzzz'));
    	
    	return expiresAt.dt;
    }
    
    /*1403213 Only using Preferred Providers for first iteration
    public Id getSecondaryProviders() {
        Map<Id,Account> secondaryProviders = new Map<Id,Account>(providerQuery('Secondary'));
        if(secondaryProviders.size() > 0) {
            Map<Id, Account> possibleProviders = getProvidersWithAllProducts(secondaryProviders);
            if(possibleProviders.size() > 0) {
                return getNearestProviderId(possibleProviders);
            }
        } 
        return getBaseProviders();
    }
    public Id getBaseProviders() {
        Map<Id,Account> baseProviders = new Map<Id,Account>(providerQuery('Base'));
        if(baseProviders.size() > 0) {
            Map<Id, Account> possibleProviders = getProvidersWithAllProducts(baseProviders);
            if(possibleProviders.size() > 0) {
                return getNearestProviderId(possibleProviders);
            }
        } 
        return null;
    }*/
    /*140213 Not concerned with Medicare Primary for first iteration
    public Id getMedicarePrimaryIntegra() {
        Decimal oLat = orderToAssign.Geolocation__Latitude__s;
        Decimal oLng = orderToAssign.Geolocation__Longitude__s;
        String soql;
        soql =  'SELECT Provider__c, Provider__r.Corporate_Geolocation__c, Provider__r.Corporate_Geolocation__Latitude__s, Provider__r.Corporate_Geolocation__Longitude__s ';
        soql += 'FROM Provider_DME_Category__c ';
        soql += 'WHERE Category__c IN '+medicarePrimaryDMECatIds+' AND Medicare_Primary_Winner__c = true ';
        soql += 'AND '+getDistanceFunction('Provider__r.Corporate_Geolocation__c')+' < '+maxDistance+' ';
        soql += 'ORDER BY '+getDistanceFunction('Provider__r.Corporate_Geolocation__c');
        
        Map<Id,Account> medicarePrimaryProviders = new Map<Id,Account>(); 
        for(Provider_DME_Category__c pDMECat : database.query(soql)) {
            medicarePrimaryProviders.put(pDMECat.Provider__c, pDMECat.Provider__r);
        }
        if(medicarePrimaryProviders.size() > 0) {
            Map<Id, Account> possibleProviders = getProvidersWithAllProducts(medicarePrimaryProviders);
            if(possibleProviders.size() > 0) {
                return getNearestProviderId(possibleProviders);
            }
        }
        return null;
        
    }
    */
    /*140217
    public void getOrderDMEs(List<DME_Line_Item__c> lineItems) {
        //Get a list of products (DMEs) to compare with Provider-supplied products
        Set<Id> dmeSet = new Set<Id>();
        for(DME_Line_Item__c li : lineItems) {
            dmeSet.add(li.DME__c);
        }
        orderDMEs = new Map<Id,DME__c>([SELECT Id, DME_Category__c, DME_Category__r.Medicare_Primary_Bid__c FROM DME__c WHERE ID IN :dmeSet]);
        for(DME__c dme : orderDMEs.values()) {
            if(dme.DME_Category__r.Medicare_Primary_Bid__c) {
                medicarePrimaryDMECatIds.add(dme.DME_Category__c);
            }
        }
    }
    public Map<Id, Account> getProvidersWithAllProducts(Map<Id,Account> providerMap) {
        Map<Id, Account> providersWithAllProducts = new Map<Id, Account>();
        Set<Id> dmeIds;
        for(Account p : [
            SELECT 
                Id,             
                (
                    SELECT DME__c 
                    FROM Provider_DMEs__r 
                    WHERE DME__c IN : orderDMEs.keySet()
                )
            FROM Account
            WHERE Id IN :providerMap.keySet()
        ]) {
            dmeIds = new Set<Id>();
            for(Provider_DME__c pDME : p.Provider_DMEs__r) {
                dmeIds.add(pDME.DME__c);
            }
            if(orderDMEs.size() == dmeIds.size()) {
                providersWithAllProducts.put(p.Id, p);
            }
        }
        return providersWithAllProducts;
    }
    */
    /*140213 Using only DISTANCE to find nearest provider
    public Id getNearestProviderId(Map<Id,Account> providerMap) {
        if(providerMap.size() == 1) {
            for(String pId : providerMap.keySet()) {
                return pId;
            }
        }else if(providerMap.size() > 1) {
            GeocodingModel_CS orderGeoModel = new GeocodingModel_CS();
            orderGeoModel.recordId = orderToAssign.Id;
            orderGeoModel.lat = oLat;
            orderGeoModel.lng = oLng;
            
            List<GeocodingModel_CS> providerGeoModels = new List<GeocodingModel_CS>();
            for(Account p : providerMap.values()) {
                GeocodingModel_CS pGeoModel = new GeocodingModel_CS();
                pGeoModel.recordId = p.Id;
                pGeoModel.lat = p.Corporate_Geolocation__Latitude__s;
                pGeoModel.lng = p.Corporate_Geolocation__Longitude__s;
                providerGeoModels.add(pGeoModel);
            }
            GeocodingModel_CS nearestProvider = GeocodingUtils_CS.getNearestProvider(orderGeoModel, providerGeoModels);
            return nearestProvider.recordId;
        }
        return null;
    }
    */
    
    public void addToManuallyAssignQueue(Order__c orderToAssign) {
        orderToAssign.Status__c = OrderModel_CS.STATUS_MANUALLY_ASSIGN_PROVIDER ;
        //140217 update orderToAssign;
    }
    
    public List<Account> providerQuery(string providerRank) {
        
        NestableCondition conditions = new AndCondition()
        	.add(new FieldCondition('Type__c').equals('Provider'))
        	.add(new FieldCondition('Provider_Rank__c').equals(providerRank))
        	.add(new FieldCondition('Corporate_Geolocation__Latitude__s').notEquals(null))
        	.add(new FieldCondition('Corporate_Geolocation__Longitude__s').notEquals(null))
        	.add(new FieldCondition('Clear_Enabled__c').equals(true))        	
        	//LEGACY 'Active' flag
        	.add(new FieldCondition('Status__c').equals('Active'));
        	
        if(previouslyAssignedProviderIds != null && previouslyAssignedProviderIds.size() > 0){
        	conditions.add(new SetCondition('Id',Operator.NOT_IN,previouslyAssignedProviderIds));
        }
        
        SoqlBuilder query = new SoqlBuilder()
        	.selectx('Id')
        	.selectx('Corporate_Geolocation__Latitude__s')
        	.selectx('Corporate_Geolocation__Longitude__s')
        	.selectx('Clear_Enabled__c')
        	.fromx('Account')
        	.wherex(conditions)
        	.orderByx(new OrderBy(getDistanceFunction('Corporate_Geolocation__c')).ascending().nullsLast());
        	
		System.debug('Provider Query: ' + query.toSoql());
		
		List<Account> preferredProviders = Database.query(query.toSoql());
		
        return preferredProviders;
    }
    
    public String getDistanceFunction(String geoField) {
        return 'DISTANCE('+geoField+', GEOLOCATION('+oLat+', '+oLng+'), \'mi\')';
    }
}