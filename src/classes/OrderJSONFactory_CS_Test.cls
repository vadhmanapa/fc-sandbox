/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class OrderJSONFactory_CS_Test {

	/******* Test Parameters *******/
	static final Integer N_ORDERS = 5;

	/******* Provider *******/
	static private List<Account> providers;
	
	/******* Payer *******/
	static private List<Account> payers;
	static private List<Contact> payerUsers;
	
	/******* Patients *******/
	static private List<Patient__c> patients;
	static private List<Plan_Patient__c> planPatients;
	
	/******* Orders *******/
	static private List<Order__c> orders;

	static testMethod void OrderJSONFactory_CS_Test_OrderMessages(){
		init();
		
		OrderJSONFactory_CS.processOrderMessage(payerUsers[0], orders[0]);	
		
		System.assert(orders[0].Message_History_JSON__c != null, 'No order messages');
		
		List<OrderMessage_CS> messages = OrderJSONFactory_CS.returnOrderMessages(orders[0].Message_History_JSON__c);
		
		System.assert(messages.size() == 1, 'No order messages.');
	}
	
	public static void init() {		
    	
    	///////////////////////////////////
		// Data
		///////////////////////////////////
    	
    	//Providers
		providers = TestDataFactory_CS.generateProviders('TestProvider', 1);
		insert providers;
		
		//Payers
		payers = TestDataFactory_CS.generatePlans('TestPayer', 1);
		insert payers;
		
		//Payer users
		payerUsers = TestDataFactory_CS.generatePlanContacts('PayerUser', payers, 1);
		insert payerUsers;		
		
		//Patients
		patients = TestDataFactory_CS.generatePatients('TestPatient', 1);
		insert patients;
		
		//Plan patients
		planPatients = TestDataFactory_CS.generatePlanPatients(
			new Map<Id,List<Patient__c>>{
				payers[0].Id => new List<Patient__c>{patients[0]}
			}
		);
		insert planPatients;
		
		//Orders, set status to 'New' yielding stage 'Pending Acceptance'
		orders = TestDataFactory_CS.generateOrders(planPatients[0].Id, providers[0].Id, N_ORDERS);
		for(Order__c o : orders){
			o.Status__c = OrderModel_CS.STATUS_NEW;
		}
		
		Database.Saveresult[] results = Database.insert(orders);
		
		for(Database.Saveresult r : results){
			System.debug('Insert result: ' + r.isSuccess());
			
			for(Database.Error e : r.getErrors()){
				System.debug(e.getMessage());
			}
		}
	}

}