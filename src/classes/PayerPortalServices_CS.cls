public virtual without sharing class PayerPortalServices_CS extends PortalServices_CS {

	public final String pageAccountType = 'Payer';

	public Boolean portalMember = false;

	public PayerPortalServices_CS(){
		super();
		
		if(portalUser != null && portalUser.AccountType == pageAccountType){
			portalMember = true;
		}
	}
}