public with sharing class PublicKnowledgeServices {
	
	//EDITED 4/17/2015, 4/20/2015 ISSCI-80
	
	public static final String REDIRECT_URL = 'http://www.accessintegra.com';
	public static final Id HELP_PROFILE = [SELECT Id FROM Profile WHERE Name='Que Help Center Profile'].Id;
	
	//Accepted list of refering domains
	public static List<String> referers{
		get{
			if (referers == null){
				referers = new List<String>();
				Map<String, Referral_URL__c> settingMap = Referral_URL__c.getAll();
				for (Referral_URL__c url : settingMap.values()){
					referers.add(url.Domain__c);
				}
				System.debug(referers);
			}
			return referers;
		} private set;
	}
	
	public static Cookie getPageCookie(){
		Map<String, System.Cookie> cookies = ApexPages.currentPage().getCookies();
		System.debug('Cookie map: ' + cookies);
		if (cookies.containsKey('valid')){
			System.debug('Returning cookie: ' + cookies.get('valid'));
			return cookies.get('valid');
		} else {
			System.debug('No cookie found');
			return null;
		}
	}
	
	public static void setPageCookie(){
		if (getPageCookie() == null){
			Cookie pageCookie = new Cookie(
				'valid', //Name
				'true', //Value
				null, //Path
				-1, //Age: Session cookie
				true //Secure
			);
			ApexPages.currentPage().setCookies(new List<Cookie>{pageCookie});
			System.debug('Setting cookie');
			System.debug(getPageCookie());
		}
	}

	public static Boolean pageCookieValid(){
		Cookie pageCookie = getPageCookie();
		if (pageCookie == null){
			return false;
		}
		return pageCookie.getValue() == 'true';
	}
	
	public static PageReference checkForReferrer(){
		
		//REDIRECT FOR INVALID REFERER
    	//Check for incoming domain, we MUST have this
    	String referer;
    	
    	if( ApexPages.currentPage().getHeaders().containsKey('Referer') ){
    		referer = ApexPages.currentPage().getHeaders().get('Referer');
    		
    		//If the referer is approved, set cookie and do not redirect
    		if( verifyReferer(referer) ){
    			setPageCookie();
    			return null;
    		}
    	}
    	else{
    		System.debug(LoggingLevel.INFO,'No referer found in header.');
    	}
    	
    	//Check for cookie
    	if(pageCookieValid()){
    		System.debug(LoggingLevel.INFO,'Cookie found.');
    		return null;
    	}
    	System.debug(LoggingLevel.INFO,'No cookie found.');
    	return new PageReference(REDIRECT_URL);
	}
	
	public static Boolean verifyReferer(String refererAddr) {
		
		for( String referer : referers ){
			//System.debug(referer);
			if( refererAddr.contains(referer) ){
				System.debug(LoggingLevel.INFO,'Verified referer: ' + refererAddr);
				return true;
			}
		}
		
		System.debug(LoggingLevel.INFO,'Failed to verify referer: ' + refererAddr);
		return false;
	}

}