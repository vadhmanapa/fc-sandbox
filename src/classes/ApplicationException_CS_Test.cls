@isTest
private class ApplicationException_CS_Test {
	
	static testMethod void sendEmail() {
		try{
			al__Foo__c foo = new al__Foo__c();
			foo.al__NumberInteger__c = 10; //Invalid field value
			insert foo;
		}catch(DMLException e){
			try{
				throw new applicationException('Error Message', e, true);
			}
			catch(ApplicationException ae){
				System.assert(true);
			}
		}
		
	}
	
	static testMethod void doNotSendEmail() {
		try{
			al__Foo__c foo = new al__Foo__c();
			foo.al__NumberInteger__c = 10; //Invalid field value
			insert foo;
		}catch(DMLException e){
			try{
				throw new applicationException('Error Message', e, false);
			}
			catch(ApplicationException ae){
				System.assert(true);
			}
		}
	}
}