/**
 *  @Description Test class for DeleteOldSimOrdersBatch_CS
 *  @Author Cloud Software LLC, Keith Arnold
 *  Revision History: 
 *      01/15/2016 - karnold - Created.
 *
 */
@isTest
private class DeleteOldSimOrdersBatch_CS_Test {
    
    private static testMethod void batchDelete_Test() {
        // GIVEN
        TestDataFactory_CS data = new TestDataFactory_CS();
        List<Order__c> oldOrderList = data.getOrderList();
        List<Order__c> todaysOrders = new List<Order__c>();

        for (Order__c o : oldOrderList) {
            o.RecordTypeId = OrderModel_CS.simulationRecordTypeId;
            todaysOrders.add(o.clone(false, true, true, false));
            Test.SetCreatedDate(o.Id, DateTime.now().addDays(-2));
        }

        update oldOrderList;
        insert todaysOrders;

        DeleteOldSimOrdersBatch_CS batch = new DeleteOldSimOrdersBatch_CS();

        // WHEN
        Test.startTest();
            Database.executeBatch(batch);
        Test.stopTest();

        // THEN
        Map<Id, Order__c> ordersAfterDelete = new Map<Id, Order__c>([
            SELECT Id
            FROM Order__c
        ]);

        // Assert that we have orders left in the database and only the orders from today remain.
        System.assert(!ordersAfterDelete.isEmpty());
        System.assert(!oldOrderList.isEmpty());
        System.assert(!todaysOrders.isEmpty());

        for (Order__c o : todaysOrders) {
            System.assert(ordersAfterDelete.containsKey(o.Id));
        }

        for (Order__c o : oldOrderList) {
            System.assert(!ordersAfterDelete.containsKey(o.Id));
        }
    }

    private static testMethod void schedulableDelete_Test() {
        // GIVEN
        TestDataFactory_CS data = new TestDataFactory_CS();
        List<Order__c> oldOrderList = data.getOrderList();
        List<Order__c> todaysOrders = new List<Order__c>();

        for (Order__c o : oldOrderList) {
            o.RecordTypeId = OrderModel_CS.simulationRecordTypeId;
            todaysOrders.add(o.clone(false, true, true, false));
            Test.SetCreatedDate(o.Id, DateTime.now().addDays(-2));
        }

        update oldOrderList;
        insert todaysOrders;

        String cronTime = '0 0 0 1 1 ? 2022'; // Set the time to run the job.

        // WHEN
        Test.startTest();
        String jobId = System.schedule('DeleteOrdersScheduledBatchTest', cronTime, new DeleteOldSimOrdersBatch_CS());
        Test.stopTest();

        // THEN
        CronTrigger ct = [
            SELECT Id, CronExpression, TimesTriggered, NextFireTime
            FROM CronTrigger 
            WHERE Id = :jobId
        ];

        System.assertEquals(cronTime, ct.CronExpression); // Check the cron times are the same.
        System.assertEquals(0, ct.TimesTriggered);  // Assert that it has not run.
        System.assertEquals(DateTime.newInstance(2022, 1, 1), ct.NextFireTime); // Assert when the job will fire next.
    }
}