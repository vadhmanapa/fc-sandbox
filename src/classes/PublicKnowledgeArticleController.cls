public with sharing class PublicKnowledgeArticleController {

	public static Map<String,String> pageParams = ApexPages.currentPage().getParameters();
	
	public Tutorial__kav article {get;set;}

	public PublicKnowledgeArticleController(){
		
	}
	
	public PageReference init(){
		
		PageReference redirect = PublicKnowledgeServices.checkForReferrer();
		if (redirect != null) {
			return redirect;
		}
		
		if( pageParams.containsKey('aId') &&
			String.isNotBlank(pageParams.get('aId'))) 
		{
			Id articleId = pageParams.get('aId');
			//TODO look up MasterVersionID
			List<KnowledgeArticleVersion> articles = [
	            SELECT ArticleType
	            FROM KnowledgeArticleVersion
	            WHERE PublishStatus = 'Online'
	            AND IsLatestVersion = true
	            AND Language = 'en_US'
	            AND KnowledgeArticleId = :articleId
	            LIMIT 1
	        ];
	        
	        if(articles.size() == 0) {
	        	system.debug(LoggingLevel.INFO,'No article id:' + articleId + ' found!');
	        	return null;
	        }
	        
	        article = [
	        	SELECT 
	        		KnowledgeArticleId,
	        		Title,
	        		Summary,
	        		Content__c
	        	FROM Tutorial__kav
	        	WHERE PublishStatus = 'Online' 
	        	AND KnowledgeArticleId = :articleId
	        ];
	        
	        //Published Articles Cannot Be Modified!!!
	        incrementViewCount();
	        
	        return null;
		} else {
        	system.debug(LoggingLevel.INFO,'No id found!');
        	return null;
        }
	}
	
	private void incrementViewCount() {

		List<KnowledgeArticle_Stats__c> articleStats = [
			SELECT 
				External_Views__c,
				Internal_Views__c
			FROM KnowledgeArticle_Stats__c
			WHERE Tutorial__c = :article.KnowledgeArticleId
		];
		
		if (articleStats.size() == 0) {
			//Create a stats object for the article
			KnowledgeArticle_Stats__c stats = new KnowledgeArticle_Stats__c(
				Tutorial__c = article.KnowledgeArticleId,
				Title__c = article.Title
			);
			if (UserInfo.getProfileId() != PublicKnowledgeServices.HELP_PROFILE) {
				//If logged in user is a guest, increment the external view count
				stats.Internal_Views__c = 1;
				stats.External_Views__c = 0;
			} else {
				//Otherwise increment the internal view count	
				stats.Internal_Views__c = 0;
				stats.External_Views__c = 1;
			}
			insert stats;
			return;
		}		

		//Do we want to handle more than 1?

		if (UserInfo.getProfileId() != PublicKnowledgeServices.HELP_PROFILE) {
			//If logged in user is a guest, increment the external view count
			articleStats[0].Internal_Views__c++;
		} else {
			//Otherwise increment the internal view count	
			articleStats[0].External_Views__c++;
		}
		articleStats[0].Title__c = article.Title;
		update articleStats[0];
	}
}