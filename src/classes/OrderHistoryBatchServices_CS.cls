public class OrderHistoryBatchServices_CS { 

	//Accepts a list of Orders and sets the "Accepted At" field on those Orders' child Assignment Histories.
	//Returns a map of Order Id -> List<Errors> when OrderHistoryJSONModel_CS objects on that order could not find an Assignment History to set.
	public static Map<Id, List<String>> setAssignedAtOnOrderHistory(Map<Id, Order__c> orderMap) {

		//Error map the function returns
		Map<Id, List<String>> orderIdToErrorsMap = new Map<Id, List<String>>();

		//Query Assignment History objects and map Order Id -> List<Assignment History>
		Map<Id, List<Order_Assignment__c>> orderIdToHistoryList = new Map<Id, List<Order_Assignment__c>>();
		for (Order_Assignment__c oa : [
			SELECT Accepted_At__c, Assigned_At__c, Order__c, Provider__c, Unassigned_At__c, Provider__r.Name
			FROM Order_Assignment__c
			WHERE Order__c IN :orderMap.keySet()
		]) {
			if (!orderIdToHistoryList.containsKey(oa.Order__c)) {
				orderIdToHistoryList.put(oa.Order__c, new List<Order_Assignment__c>());
			}
			orderIdToHistoryList.get(oa.Order__c).add(oa);
		}
		System.debug('OrderHistoryBatchServices_CS: Order Id to History List: ' + orderIdToHistoryList);

		//Create a list of OrderHistoryJSONModel_CS objects from each order, map Order Id -> List<OrderHistoryJSONModel_CS>, and map Contact Id -> List<OrderHistoryJSONModel_CS>
		Map<Id, List<OrderHistoryJSONModel_CS>> orderIdToHistoryModelMap = new Map<Id, List<OrderHistoryJSONModel_CS>>();
		Map<Id, List<OrderHistoryJSONModel_CS>> contactIdToHistoryModelMap = new Map<Id, List<OrderHistoryJSONModel_CS>>();
		for (Id orderId : orderMap.keySet()) {
			if (orderMap.get(orderId).Order_History_JSON__c != null) {
				System.debug('Order: ' + orderMap.get(orderId));
                try {
                    for (OrderHistoryJSONModel_CS m : OrderHistoryJSONModel_CS.parseList(orderMap.get(orderId).Order_History_JSON__c)) {
                        //Only include OrderHistoryJSONModel_CS whose Order Status is Provider Accepted
                        if (m.orderStatus == 'Provider Accepted') {
                            if (!orderIdToHistoryModelMap.containsKey(orderId)) {
                                orderIdToHistoryModelMap.put(orderId, new List<OrderHistoryJSONModel_CS>());
                            }
                            orderIdToHistoryModelMap.get(orderId).add(m);
    
                            if (!orderIdToHistoryModelMap.containsKey(m.contactId)) {
                                contactIdToHistoryModelMap.put(m.contactId, new List<OrderHistoryJSONModel_CS>());
                            } 
                            contactIdToHistoryModelMap.get(m.contactId).add(m);
                        }
                    }
                 }catch(JSONException e) {
					if (!orderIdToErrorsMap.containsKey(orderId)) {
						orderIdToErrorsMap.put(orderId, new List<String>());
					}
					orderIdToErrorsMap.get(orderId).add('Order History JSON malformed');
				}   
			}
		}
		System.debug('OrderHistoryBatchServices_CS: Order Id to History Model Map: ' + orderIdToHistoryModelMap);
		System.debug('OrderHistoryBatchServices_CS: Contact Id to History Model Map: ' + contactIdToHistoryModelMap);

		//Query for Accounts whose Contact is on an OrderHistoryJSONModel_CS and map Contact Id -> Account
		Map<Id, Account> contactIdToAccountMap = new Map<Id, Account>();
		for (Contact c : [
			SELECT AccountId, Account.RecordType.DeveloperName
			FROM Contact
			WHERE Id IN :contactIdToHistoryModelMap.keySet()
		]) {
			contactIdToAccountMap.put(c.Id, c.Account);
		}
		System.debug('OrderHistoryBatchServices_CS: Contact Id to Account Map: ' + contactIdToAccountMap);

		//Check if each OrderHistoryJSONModel_CS falls within the window of an Assignment History on that Order with the correct Account
		Map<Id, Order_Assignment__c> assignmentsToUpdate = new Map<Id, Order_Assignment__c>();
		for (Id orderId : orderIdToHistoryList.keySet()) {
			System.debug('OrderHistoryBatchServices_CS: Current order: ' + orderMap.get(orderId));
			for (Order_Assignment__c oa : orderIdToHistoryList.get(orderId)) {
				System.debug('OrderHistoryBatchServices_CS: Current assignment: ' + oa);
				//Integer orderAssignmentSetCount = 0; //How many models is this order assignment getting its values from
				if(oa.Unassigned_At__c == null) {
					System.debug('OrderHistoryBatchServices_CS: Unassigned at is null');
					oa.Accepted_At__c = orderMap.get(orderId).Accepted_Date__c;
					System.debug('OrderHistoryBatchServices_CS: Updated assignment: ' + oa);
					assignmentsToUpdate.put(oa.Id, oa);
					continue;
				}
				if (orderIdToHistoryModelMap.containsKey(orderId)) {
					for (OrderHistoryJSONModel_CS m : orderIdToHistoryModelMap.get(orderId)) {
						System.debug('OrderHistoryBatchServices_CS: Current model: ' + m);
						if (m != null && (contactIdToAccountMap.get(m.ContactId) == null || contactIdToAccountMap.get(m.ContactId).RecordType.DeveloperName != 'Payer_Relations')) {
							//If the OrderHistoryJSONModel_CS is m.timestamped between Assigned/Unassigned time or is after Assigned time and Unassigned time is null
							if (m.timestamp > oa.Assigned_At__c && m.timestamp <= oa.Unassigned_At__c ) {
								//If the account on the Order Assignment matches the account on the OrderHistoryJSONModel_CS, set the Accepted At time on Order Assignment 
								if (m.accountName == oa.Provider__r.Name || (contactIdToAccountMap.get(m.ContactId) != null && oa.Provider__c == contactIdToAccountMap.get(m.ContactId).Id)) {
									System.debug('OrderHistoryBatchServices_CS: Setting accepted time on Order Assignment: ' + oa);
									oa.Accepted_At__c = m.timestamp;
									if (!assignmentsToUpdate.containsKey(oa.Id) || assignmentsToUpdate.get(oa.Id).Accepted_At__c > oa.Accepted_At__c) {
										assignmentsToUpdate.put(oa.Id, oa);
									}
									//orderAssignmentSetCount++;
								//If the accounts do not match, add an error message
								}else {
									if (!orderIdToErrorsMap.containsKey(orderId)) {
										orderIdToErrorsMap.put(orderId, new List<String>());
									}
									orderIdToErrorsMap.get(orderId).add('Account conflict for model with timestamp: ' + m.timestamp);
								}
							}else {
								System.debug('OrderHistoryBatchServices_CS: Timestamp ' + m.timestamp + ' outside timeframe ' + oa.Assigned_At__c + ' - ' + oa.Unassigned_At__c);
							}
						}
					}
				}
				//If it falls within multiple windows, add an error message
				/*if (orderAssignmentSetCount > 1) {
					if (!orderIdToErrorsMap.containsKey(orderId)) {
						orderIdToErrorsMap.put(orderId, new List<String>());
					}
					orderIdToErrorsMap.get(orderId).add('More than one model for assignment history: ' + oa.Id);
				}*/
			}
		}
		System.debug('OrderHistoryBatchServices_CS: Order Id to Errors Map: ' + orderIdToErrorsMap);

		update assignmentsToUpdate.values();
		return orderIdToErrorsMap;
	}
}