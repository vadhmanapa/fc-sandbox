@isTest
private class ProviderSearchControllerTest_IP {
	
	@isTest static void testProviderSearch() {
		// Implement test code

		Profile p = [Select id from Profile where Name ='Integra Standard Platform User'];
		User u = new User(LastName='User1', 
						  Alias='use',
						  Email='test@test.com',
						  Username='testingtrigger@testingcenter.com',
						  CommunityNickname='nick',
						  EmailEncodingKey='UTF-8',
						  LanguageLocaleKey='en_US',
						  LocaleSidKey='en_US',
						  ProfileId = p.Id,
						  TimeZoneSidKey='America/New_York');
		insert u;

		System.runAs(u){

			Account a = new Account(Name='Test Account', 
						        Type_Of_Provider__c = 'DME',
						        Status__c = 'Active');
		insert a;

		Map<Id, Provider_Locations__c> testProvMap = new Map<Id, Provider_Locations__c>();

		Provider_Locations__c pl = new Provider_Locations__c(Account__c = a.Id,
															 Name_DBA__c = 'Test Account Store', 
															 Adult_Orthotic_and_Prosthetic_Services__c = 'Test Product; Test1; Test 2',
															 Durable_Medical_Equipment_Services__c = 'Test Product',
															 Pediatric_Orthotic_Prosthetic_Services__c = 'Test Product',
															 Location_State__c = 'NY');
		insert pl;

		Provider_Locations__c pl2 = new Provider_Locations__c(Account__c = a.Id,
															 Name_DBA__c = 'Test Account Store2',
															 Adult_Orthotic_and_Prosthetic_Services__c = 'Test Product; Test1; Test 2',
															 Durable_Medical_Equipment_Services__c = 'Test Product',
															 Pediatric_Orthotic_Prosthetic_Services__c = 'Test Product',
															 Location_State__c = 'AL');
		insert pl2;

		Provider_Locations__c pl3 = new Provider_Locations__c(Account__c = a.Id,
															 Name_DBA__c = 'Test Account Store3',
															 Location_State__c = 'NY');
		insert pl3;

		for(Provider_Locations__c plc: [Select Id, Name_DBA__c, Account__c from Provider_Locations__c where Account__c =: a.Id]) {
			testProvMap.put(plc.Id, plc);
		}
		//create sample states

		//Set<String> states = new Set<String>{'AL', 'AK', 'NY', 'NJ'};
		Map<String, String> stateMap = new Map<String, String>{'Alabama' => 'Alabama', 'Alaska' => 'Alaska', 'New York' => 'New York', 'New Jersey' => 'New Jersey'};
		List<US_State_County__c> allIns = new List<US_State_County__c>();
		for(String s : stateMap.keySet()){
			for(Integer i=0; i<3; i++){

				String na = 'Test'+' '+i;
				US_State_County__c newCounty = new US_State_County__c(Name=na, State__c = stateMap.get(s), State_Code__c = s);
				allIns.add(newCounty);
			}
		}

		allIns.add(new US_State_County__c(Name= 'Alabama', State__c = 'Alabama', State_Code__c = 'AL'));
		insert allIns;

		List<State_Identifier__c> toInsert = new List<State_Identifier__c>();

		US_State_County__c us = [Select Id, Name, State__c from US_State_County__c where State__c = 'Alabama' AND Name = 'Test 0'];
		//toInsert.add(new State_Identifier__c(Provider_Location__c = pl.Id, Medicaid__c = '234555', State__c = us.State__c));
		//toInsert.add(new State_Identifier__c(Provider_Location__c = pl2.Id, Medicaid__c = '134987', State__c = us.State__c));

		US_State_County__c us2 = [Select Id, Name, State__c from US_State_County__c where State__c = 'Alaska' AND Name = 'Test 0'];
		//toInsert.add(new State_Identifier__c(Provider_Location__c = pl.Id, Medicaid__c = '499474', State__c = us2.State__c));
		//toInsert.add(new State_Identifier__c(Provider_Location__c = pl2.Id, Medicaid__c = '849042', State__c = us2.State__c));

		insert toInsert;

		US_State_County__c alabamaCon = [Select Id, Name, State__c from US_State_County__c where Name='Alabama' AND State__c = 'Alabama'];

		List<County_Item__c> countyItems = new List<County_Item__c>();

		//countyItems.add(new County_Item__c(Provider_Location__c = pl.Id, US_State_County__c = alabamaCon.Id, County_Name__c = 'Alabama', State__c = 'Alabama'));
		//countyItems.add(new County_Item__c(Provider_Location__c = pl2.Id, US_State_County__c = alabamaCon.Id, County_Name__c = 'Alabama', State__c = 'Alabama'));

		insert countyItems;
		

		PageReference pageRef = Page.ProviderSearchPage;
		Test.setCurrentPage(pageRef);

		ProviderSearchController_IP testController = new ProviderSearchController_IP();
		testController.availableProviders();

		List<SelectOption> testStates = new List<SelectOption>();
		testStates = testController.getStateNames();

		System.assertNotEquals(0, testStates.size(), 'The states were queried');

		//testController.runCountyServedSearch('New York', '');
		Integer listSize = testController.countResult;
		System.assertNotEquals(0, listSize, 'Records for New York found');

		// set to a different state and test

		System.debug('Changing state to Alabama');

		testController.stateSelected = 'Alabama';
		testController.runCountyServedSearch(testController.stateSelected, '');
		testController.availableProviders();

		listSize = testController.countResult;
		System.debug('Size'+listSize);
		System.assertNotEquals(0, listSize, 'Records were quried for Alabama');

		// **************** Setting fixed Result Search for SOSL *******************

		Id[] fixedSearchResults = new Id[1];
		fixedSearchResults[0] = pl.Id;
		Test.setFixedSearchResults(fixedSearchResults);

		System.debug('Searching for product Shoes');

		testController.searchProduct = 'test';
		List<List<Provider_Locations__c>> searchList = [FIND 'test' IN ALL FIELDS RETURNING Provider_Locations__c(Id, Adult_Orthotic_and_Prosthetic_Services__c,Pediatric_Orthotic_Prosthetic_Services__c, Durable_Medical_Equipment_Services__c) LIMIT 1];
		testController.productSearch(testController.searchProduct, testProvMap);
		//testController.availableProviders();

		listSize = testController.countResult;
		System.assertNotEquals(0, searchList.size(), 'Records were queried for State of Alabama and Product Shoes');
		//Boolean checkProductIsThere = testController.checkProductIsThere(testController.searchProduct, pl); // searching mechanism
		//System.assertEquals(true, checkProductIsThere, 'The products was searched');
		
		System.debug('Searching county for Alabama');

		testController.searchProduct = null;
		testController.searchString = null;
		testController.availableProviders();

		listSize = testController.countResult;
		System.assertNotEquals(0, listSize, 'Records were queried for State of Alabama and no County');

		testController.searchProduct = null;
		testController.searchString = 'Alabama';
		testController.runCountyServedSearch(testController.stateSelected, testController.searchString);
		testController.availableProviders();

		listSize = testController.countResult;
		System.assertNotEquals(0, listSize, 'Records were queried for State of Alabama and County Alabama');


		// **************** Setting fixed Result Search for SOSL *******************

		Id[] fixedSearchResults2 = new Id[1];
		fixedSearchResults2[0] = pl.Id;
		Test.setFixedSearchResults(fixedSearchResults2);

		System.debug('Searching for product Shoes');

		testController.searchProduct = 'test';
		searchList = [FIND 'test' IN ALL FIELDS RETURNING Provider_Locations__c(Id, Adult_Orthotic_and_Prosthetic_Services__c,Pediatric_Orthotic_Prosthetic_Services__c, Durable_Medical_Equipment_Services__c) LIMIT 1];
		testController.productSearch(testController.searchProduct, testProvMap);
		// testController.availableProviders();

		System.assertNotEquals(0, searchList.size(), 'Records were queried for State of Alabama and Product Shoes');

		testController.availableProviders();
		listSize = testController.countResult;
		System.assertNotEquals(0, listSize);

		System.debug('Searching for Medicaid only');

		testController.searchProduct = null;
		testController.includeMed = true;
		testController.availableProviders();

		listSize = testController.countResult;
		System.assertNotEquals(0, listSize, 'The records were queried for Medicaid');

		System.debug('Searching for Medicaid only with no counties');

		testController.includeMed = true;
		testController.searchString = '';
		testController.searchProduct = '';
		testController.medicaidSearch(testController.stateSelected, testProvMap);
		testController.availableProviders();

		listSize = testController.countResult;
		System.assertNotEquals(0, listSize, 'The records were queried for Medicaid');

		} // end of system.runAs

	} // end of test method
	
	
}