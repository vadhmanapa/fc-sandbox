@isTest
private class PayToForProviderLocationTest
{
	private static PayToForProviderLocation scc;
	//private static Pay_To__c newPay;
	private static Integer payToSize;
	static Account acc;
	static Provider_Location__c pLoc;
	static User testUser;

	static{
		Profile p = [Select id from Profile where Name = 'Integra Standard User'];
		Decimal d = Math.random();
		testUser = new User(LastName = 'User1'+d,
		             Alias = 'use',
		             Email = 'test'+d+'@test.com',
		             Username = 'testingtrigger'+d+'@testingcenter.com',
		             CommunityNickname = 'nick',
		             EmailEncodingKey = 'UTF-8',
		             LanguageLocaleKey = 'en_US',
		             LocaleSidKey = 'en_US',
		             ProfileId = p.Id,
		             TimeZoneSidKey = 'America/New_York'
		);
		insert testUser;
	}

	private static void init(){
		// Given

		System.runAs(testUser) {

			acc = new Account(Name = 'Test Account', Type_Of_Provider__c = 'DME', Legal_Name__c = 'Test Legal Name', Tax_ID__c = '10254');
			insert acc;

			pLoc = new Provider_Location__c(Account__c = acc.Id,Name_DBA__c ='Test Store 1');
			insert pLoc;

			//load the page

			PageReference pageRef = Page.PayToForProviderLocation;
			pageRef.getParameters().put('pId', pLoc.Id);
			Test.setCurrentPageReference(pageRef);

			//load the extension
			//Provider_Location__c testPL = [Select id from Provider_Location__c where id = :pl1.id];
			Pay_To__c newPay = new Pay_To__c();
			ApexPages.StandardController sc = new ApexPages.StandardController(newPay);
			scc = new PayToForProviderLocation(sc);
		}
	}

	private static testMethod void testWithNoPayTo(){
		// GIVEN
		init();
		System.assert(scc.payList == null);
		// WHEN

		Test.startTest();
		scc.cancel();
		Test.stopTest();

		//THEN
	}

	private static testMethod void testWithNewAddPayTo(){
		// GIVEN
		init();

		// WHEN
		Test.startTest();
		scc.AddNewPay();
		scc.newPayTo.Mailing_Address__c = '5166 Test Address 1';
		scc.newPayTo.Mailing_Address_2__c = 'Address 2';
		scc.newPayTo.City__c = 'Test City';
		scc.newPayTo.State__c = 'Alaska';
		scc.newPayTo.Zip__c = '10005-014';
		scc.save();
		Test.stopTest();
		
		// THEN
		Provider_Location__c checkLoc = [Select id, Pay_To__c from Provider_Location__c where id = :scc.pLoc.id];
		Pay_To__c newPay = [Select id from Pay_To__c where State__c = 'Alaska' LIMIT 1];
		System.assert(checkLoc.Pay_To__c == newPay.Id);
	}

	private static testMethod void testWithNewAddPayToChange(){
		// GIVEN
		init();

		// WHEN
		Test.startTest();
		scc.AddNewPay();
		scc.newPayTo.Mailing_Address__c = '5166 Test Address 1';
		scc.newPayTo.Mailing_Address_2__c = 'Address 2';
		scc.newPayTo.City__c = 'Test City';
		scc.newPayTo.State__c = 'Alaska';
		scc.newPayTo.Zip__c = '10005-014';
		scc.save();
		Test.stopTest();
		
		// THEN
		Provider_Location__c checkLoc = [Select id, Pay_To__c from Provider_Location__c where id = :scc.pLoc.id];
		Pay_To__c newPay = [Select id from Pay_To__c where State__c = 'Alaska' LIMIT 1];
		System.assert(checkLoc.Pay_To__c == newPay.Id);
	}

	private static void initSecondProvider(){
		// Given
		//testWithNewAddPayTo();

		System.runAs(testUser) {

			// create new PL
			acc = new Account(Name = 'Test Testing Account', Type_Of_Provider__c = 'DME', Legal_Name__c = 'Test 123 Legal Name', Tax_ID__c = '102545456');
			insert acc;

			Pay_To__c nPay = new Pay_To__c(Taxpayer_Name__c= 'Test 123 Legal Name', Tax_ID__c = '102545456', Mailing_Address__c = '5166 Test Address 1', Mailing_Address_2__c = 'Address 2', City__c = 'Test City',
											State__c = 'Hawaii', Zip__c = '10005-014');
			insert nPay;

			Provider_Location__c pl = new Provider_Location__c(Account__c = acc.Id,Name_DBA__c ='Test ABC Store 1', Pay_To__c = nPay.Id);
			insert pl;

			pLoc = new Provider_Location__c(Account__c = acc.Id,Name_DBA__c ='Test ABC Store 2');
			insert pLoc;

			//load the page
			PageReference pageRef = Page.PayToForProviderLocation;
			pageRef.getParameters().put('pId', pLoc.Id);
			Test.setCurrentPageReference(pageRef);

			//load the extension
			Pay_To__c newPay = new Pay_To__c();
			ApexPages.StandardController sc = new ApexPages.StandardController(newPay);
			scc = new PayToForProviderLocation(sc);
		}
	}

	private static testMethod void testForPayListSize(){
		initSecondProvider();
		payToSize = scc.payList.size();
		System.assert(payToSize > 0);
		Test.startTest();
		scc.payList[0].isChecked = true;
		Pay_To__c addPay = scc.payList[0].payDetails;
		scc.save();
		Test.stopTest();
		Provider_Location__c checkLoc = [Select id, Pay_To__c from Provider_Location__c where id = :scc.pLoc.id];
		System.assert(checkLoc.Pay_To__c == addPay.Id);
	}

	private static testMethod void testAddNewPayWithPayList(){
		initSecondProvider();

		scc.payList[0].isChecked = true;

		Test.startTest();
		scc.AddNewPay();
		scc.newPayTo.Mailing_Address__c = '5166 Test Address 1';
		scc.newPayTo.Mailing_Address_2__c = 'Address 2';
		scc.newPayTo.City__c = 'Test City';
		scc.newPayTo.State__c = 'Texas';
		scc.newPayTo.Zip__c = '10005-014';
		scc.newPayTo.Use_Store_Name__c = true;
		scc.save();
		Test.stopTest();

		Provider_Location__c checkLoc = [Select id, Pay_To__c from Provider_Location__c where id = :scc.pLoc.id];
		Pay_To__c newPay = [Select id from Pay_To__c where State__c = 'Texas' LIMIT 1];
		System.assert(checkLoc.Pay_To__c == newPay.Id);
	}
}