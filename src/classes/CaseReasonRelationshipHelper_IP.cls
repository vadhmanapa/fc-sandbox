/***
	@Author: Venkat Adhmanapa on behalf of Integra Partners
	@Description:
	This helper class establishes the caller type and record type of the case object and based on the user selection, queries and passes back the relavent reasons,
	reason details, actions and action details.
		# The  object is architected so that each and every caller type and record type selection spawns its own tree of reasons, reason details, actions and action details.
		# In order to query a reason, caller type and case record type is required. This takes care of seleting a reason relation tree on run time.
		# Reason Details are dependent on Reasons, Actions are dependent on Reason Details and Action Details are dependent on selected action.
	This helper class should be accessed when the reasons and actions are entered on Case entry page and case detail page.
***/
public with sharing class CaseReasonRelationshipHelper_IP {

	public static Map<Id, CaseReason__c> reasonList {get;set;}
	public static Map<Id, CaseReasonDetail__c> reasonDetailList {get;set;}
	public static Map<Id, CRR__c> actionDetails {get;set;}

	public static List<SelectOption> caseReasonPicklist(String callerType, String recordType){

		List<SelectOption> caseReasons = new List<SelectOption>();
		List<CallerType__c> callerTypeCaseReasons = new List<CallerType__c>();
		reasonList = new Map<Id, CaseReason__c> ();

		System.debug(LoggingLevel.DEBUG, 'Caller Type selected is: '+callerType);
		System.debug(LoggingLevel.DEBUG, 'Record Type selected is: '+recordType);

		if((String.isBlank(callerType) || String.isBlank(recordType)) && callerType != 'None') {
			System.debug(LoggingLevel.ERROR, 'Cannot proceed without the required variables');
		}else {
			
			callerTypeCaseReasons = [SELECT Id, CallerType__c, RecordType__c, (SELECT Id, Name, Reason__c FROM Case_Reasons__r)
													FROM CallerType__c
													WHERE CallerType__c =:callerType AND RecordType__c =:recordType
													LIMIT 1];
		}

		if(callerTypeCaseReasons != null && !callerTypeCaseReasons.isEmpty()) {
			for(CallerType__c c: callerTypeCaseReasons) {
				if(c.Case_Reasons__r.size() > 0) {
					// the caller type and record type returned some reasons
					for(CaseReason__c cr: c.Case_Reasons__r) {
						caseReasons.add(new SelectOption(cr.Id, cr.Reason__c));
						reasonList.put(cr.Id, cr);
					}
					caseReasons.sort();
					caseReasons.add(0, new SelectOption('','----None----'));
				}else {
					caseReasons.add(new SelectOption('','----None----'));
				}
			}
		}else {
			caseReasons.add(new SelectOption('','----None----'));
		}

		return caseReasons;
	}

	public static List<SelectOption> caseReasonDetailsPicklist(String selectedReason){

		List<SelectOption> caseReasonDetails = new List<SelectOption>();
		List<CaseReason__c> reasonDetailForReasons = new List<CaseReason__c>();
		reasonDetailList = new Map<Id, CaseReasonDetail__c>();

		System.debug(LoggingLevel.DEBUG, 'Selected Case Reason is: '+selectedReason);

		if(String.isBlank(selectedReason)) {
			System.debug(LoggingLevel.ERROR, 'Cannot proceed without Case Reason');
		}else {
			reasonDetailForReasons = [SELECT Id, Reason__c, (SELECT Id, CaseReasonDetail__c, Script__c FROM Case_Reason_Details__r)
														FROM CaseReason__c
														WHERE Id=: (Id)selectedReason
														LIMIT 1];
		}

		// reasonDetailForReasons.addAll(caseReasonDetail(reasonId));

		if(reasonDetailForReasons != null && !reasonDetailForReasons.isEmpty()) {
			for(CaseReason__c cr: reasonDetailForReasons) {
				if(cr.Case_Reason_Details__r.size() > 0) {
					for(CaseReasonDetail__c crd: cr.Case_Reason_Details__r) {
						caseReasonDetails.add(new SelectOption(crd.Id, crd.CaseReasonDetail__c));
						reasonDetailList.put(crd.Id, crd);
					}

					caseReasonDetails.sort();
					caseReasonDetails.add(0, new SelectOption('','----None----'));
				}else {
					caseReasonDetails.add(new SelectOption('','----None----'));
				}
			}
		}else {
			caseReasonDetails.add(new SelectOption('','----None----'));
		}

		return caseReasonDetails;
	}

	public static List<CRR__c> actionAndDetails(String reasonDetail, String selectedAction, String userAccess){ 

		System.debug(LoggingLevel.DEBUG, 'Selected Case Reason Detail is: '+reasonDetail);

		if(String.isBlank(reasonDetail)) {
			System.debug(LoggingLevel.ERROR, 'Cannot proceed without Case Reason Detail');
			return new List<CRR__c>();
		}else {

			AndCondition conditions = new AndCondition();
			conditions.add(new FieldCondition('CaseReasonDetail__c').equals((Id)reasonDetail));
			if(String.isNotBlank(selectedAction)) {
				conditions.add(new FieldCondition('ActionDetail__r.Action__c').equals(selectedAction));
			}
            if(String.isNotBlank(userAccess)){
                OrCondition accessCondition = new OrCondition();
                if(userAccess == 'Tier 1'){
                    conditions.add(new FieldCondition('ActionDetail__r.Tier__c').equals(userAccess));
                }else if(userAccess == 'Tier 2'){
                    accessCondition.add(new FieldCondition('ActionDetail__r.Tier__c').equals('Tier 1'));
                    accessCondition.add(new FieldCondition('ActionDetail__r.Tier__c').equals('Tier 2'));
                    conditions.add(accessCondition);
                }else{
                    accessCondition.add(new FieldCondition('ActionDetail__r.Tier__c').equals('Tier 1'));
                    accessCondition.add(new FieldCondition('ActionDetail__r.Tier__c').equals('Tier 2'));
                    accessCondition.add(new FieldCondition('ActionDetail__r.Tier__c').equals('Tier 3'));
                    conditions.add(accessCondition);
                }
                
            }

			String soql = new SoqlBuilder()
							.selectx(new Set<Object>{'Id', 'ActionDetail__r.Id', 'ActionDetail__r.Action__c', 'ActionDetail__r.ActionDetail__c', 
											'ActionDetail__r.Tier__c', 'ActionDetail__r.CaseStatustoUpdate__c'})
							.fromx('CRR__c')
							.wherex(conditions)
							.toSoql();

			List<CRR__c> actionDetails = Database.query(soql);

			//[SELECT Id, ActionDetail__r.Id, ActionDetail__r.Action__c, ActionDetail__r.ActionDetail__c, ActionDetail__r.Tier__c FROM CRR__c WHERE Id=: (Id)reasonDetail];

			if(!actionDetails.isEmpty() && actionDetails != null) {
				return actionDetails;
			}else {
				return new List<CRR__c>();
			}	
		}

		return null;
	}

	public static List<SelectOption> actionDetailsPicklist(String selectedReasonDetail, String userTier){

		List<SelectOption> actionsList = new List<SelectOption>();
		List<CRR__c> reasonRelationship = new List<CRR__c>();

		reasonRelationship.addAll(actionAndDetails(selectedReasonDetail, '', userTier));

		if(reasonRelationship!= null && !reasonRelationship.isEmpty()) {
			Set<String> actionValues = new Set<String>();
			for(CRR__c crr: reasonRelationship) {
				actionValues.add(crr.ActionDetail__r.Action__c);
				
			}

			for(String s: actionValues) {
				actionsList.add(new SelectOption(s, s));
			}

			actionsList.sort();
			actionsList.add(0, new SelectOption('', '----None----'));
		}else {
			actionsList.add(new SelectOption('', '----None----'));
		}

		return actionsList;
	}

	public static List<SelectOption> actionDetailsPicklist(String selectedDetail, String selectedAction, String userTier){
		
		List<SelectOption> actionDetailsList = new List<SelectOption>();
		List<CRR__c> reasonRelationship = new List<CRR__c>();
		actionDetails = new Map<Id, CRR__c>();

		reasonRelationship.addAll(actionAndDetails(selectedDetail, selectedAction, userTier));

		if(!reasonRelationship.isEmpty()) {
			for(CRR__c crr: reasonRelationship) {
				actionDetailsList.add(new SelectOption(crr.ActionDetail__r.Id,crr.ActionDetail__r.ActionDetail__c));
				actionDetails.put(crr.ActionDetail__c, crr);
			}
			actionDetailsList.sort();
			actionDetailsList.add(0, new SelectOption('', '----None----'));
		}else {
			actionDetailsList.add(new SelectOption('', '----None----'));
		}

		return actionDetailsList;
	}

	public static CaseReason__c getCaseReasonValue(Id reasonId){
		if(String.isNotBlank(reasonId)){
			return [SELECT Id, Name, Reason__c FROM CaseReason__c WHERE Id=: reasonId LIMIT 1];
		}

		return null;
	}

	public static CaseReasonDetail__c getCaseReasonDetailValue (Id reasonDetId){
		if(String.isNotBlank(reasonDetId)){
			return [SELECT Id, CaseReasonDetail__c, Script__c FROM CaseReasonDetail__c WHERE Id=: reasonDetId LIMIT 1];
		}

		return null;
	}

	public static ActionDetail__c getActionValues(Id actId){
		if(String.isNotBlank(actId)){
			return [SELECT Id, Action__c, ActionDetail__c, Tier__c, CaseStatustoUpdate__c FROM ActionDetail__c WHERE Id=: actId LIMIT 1];
		}

		return null;
	}

	/*public static Map<String,String> actionAndDetails(String caseReasonDetail){

		System.debug(LoggingLevel.INFO, 'CASE REASON DETAIL FOR CURRENT CASE IS: '+caseReasonDetail);
		Map<String, String> assocaitedActions = new Map<String, String>();

		if(String.isNotEmpty(caseReasonDetail)) {
			String queryString = 'SELECT Action__c, ActionDetail__c FROM Case_Reasons_Relationship__c WHERE CaseReasonDetail__c=: caseReasonDetail';
			List<Case_Reasons_Relationship__c> allRelatedActions = Database.query(queryString);
			if(allRelatedActions.size() > 0) {
				for(Case_Reasons_Relationship__c cr: allRelatedActions) {
					if(cr != null) {
						assocaitedActions.put((String)cr.Action__c, (String)cr.ActionDetail__c);
					}
				}
			}else {
				System.debug(LoggingLevel.ERROR, 'NO ACTION AND ACTION DETAILS WERE FOUND FOR THIS CASE REASON DETAIL: '+caseReasonDetail);
			}
		}

		return assocaitedActions;
	}

	public static Set<String> actionDetailsBasedOnAction(String caseReasonDetail, String action){

		System.debug(LoggingLevel.INFO, 'ACTION SEELECTED FOR CASE ACTION IS: '+action);

		Set<String> actionDetails = new Set<String>();

		if(String.isNotEmpty(caseReasonDetail)) {

			String queryString = 'Select ActionDetail__c from Case_Reasons_Relationship__c WHERE CaseReasonDetail__c=: caseReasonDetail ';
			if(String.isNotEmpty(action)) {
				queryString += 'AND Action__c =: action';
			}
			
			List<Case_Reasons_Relationship__c> detailsForAction = Database.query(queryString);

			if(detailsForAction.size() > 0) {
				for(Case_Reasons_Relationship__c c: detailsForAction) {
					if(c != null) {
						actionDetails.add((String) c.ActionDetail__c);
					}	
				}
			}else {
				System.debug(LoggingLevel.ERROR, 'NO ACTION DETAILS FOUND FOR THE SELECTED ACTION: '+action);
			}
		}
		return actionDetails;
	}*/
}