public with sharing class NewOrderReassignmentController {

	public Order_Reassignment__c newOrderReassignment {get;set;}
	public Call_Log__c existingCallLog {get;set;}
	public Order__c existingOrder {get;set;}
	
	public void displayMessage(ApexPages.Severity severity, String message) {
		ApexPages.addMessage(new ApexPages.Message(severity, message));
    }
	
	public PageReference getReturnURL() {
		
		if(existingCallLog != null) {
			return new PageReference('/' + existingCallLog.Id);
		} else if(existingOrder != null) {
			return new PageReference('/' + existingOrder.Id);
		} else {
			return new PageReference('/');
		}
	}
	
	public PageReference getCurrentPage() {
		return ApexPages.currentPage();
	}
	
	public NewOrderReassignmentController(ApexPages.StandardController stdCon) {
		
		Map<String,String> pageParams = ApexPages.currentPage().getParameters();
        
        newOrderReassignment = new Order_Reassignment__c(
        	Order_Type__c = 'Clear'
        );
        
        if(pageParams.containsKey('logId') && String.isNotBlank(pageParams.get('logId'))) {
            newOrderReassignment.Call_Log__c = pageParams.get('logId');
            refreshCallLogInfo();
        }
        
        if(pageParams.containsKey('oId') && String.isNotBlank(pageParams.get('oId'))) {
            newOrderReassignment.Order__c = pageParams.get('oId');
            refreshOrderInfo();
        }
	}

	public void refreshCallLogInfo() {
		
		if(newOrderReassignment.Call_Log__c != null) {
			
			existingCallLog = [
				SELECT 
					Id,
					Name,
					Caller_First_Name__c,
					Caller_Last_Name__c,
					Caller_Type__c,
					Call_Reason__c,
					Call_Result__c
				FROM Call_Log__c
				WHERE Id = :newOrderReassignment.Call_Log__c
			];
		}		
	}

	public void refreshOrderInfo() {
		
		if(newOrderReassignment.Order__c != null) {
			
			existingOrder = [
				SELECT 
					Id,
					Name,
					Provider__c,
					Status__c, // Added by VK on 10/07/2016 - Autofill Reassignment reason from Order Status
					Stage__c, // Added by VK on 10/07/2016
					Reason_for_Rejection__c,
					Order_History_JSON__c,
					Integra_Comments__c
				FROM Order__c
				WHERE Id = :newOrderReassignment.Order__c
			];

			newOrderReassignment.Old_Provider__c = existingOrder.Provider__c;
			if(existingOrder.Stage__c == 'Rejected') {
				// Split the reason into two parts
				String[] splicedStatus = existingOrder.Status__c.trim().split('Provider Rejected - ');
				System.debug(LoggingLevel.DEBUG, 'Status after splitting:'+splicedStatus);
				newOrderReassignment.Reassignment_Reason__c = 'Provider Rejected';
				newOrderReassignment.Reassignment_Reason_Detail__c = splicedStatus[1];			
			}

			if(!String.isBlank(existingOrder.Reason_for_Rejection__c)) {
				newOrderReassignment.Details__c = existingOrder.Reason_for_Rejection__c;
			}
		}
	}

	public PageReference save() {
		
		Map<String,String> pageParams = ApexPages.currentPage().getParameters();
		
		if(String.isBlank(newOrderReassignment.Reassignment_Reason__c)) {
			newOrderReassignment.Reassignment_Reason__c.addError('Please select a reassignment reason.');
			return getCurrentPage();
		}
		
		if(newOrderReassignment.Order__c == null) {
			newOrderReassignment.Order__c.addError('Please select an order.');
			return null;
		}
		
		if(String.isBlank(newOrderReassignment.Order_Type__c)) {
			newOrderReassignment.Order__c.addError('Please select an order type.');
			return null;
		}
		
		if(newOrderReassignment.New_Provider__c == null) {
			newOrderReassignment.addError('Please select a new provider.');
			return null;
		}
		
		try {
			
			OrderModel_CS.reassignOrder(existingOrder,newOrderReassignment.New_Provider__c);
			
			update existingOrder;
			
			insert newOrderReassignment;
			
		} catch(Exception e) {
			displayMessage(ApexPages.Severity.ERROR,e.getMessage());
			return null;
		}
		
		return getReturnURL();
	}

	public PageReference cancel() {
		return getReturnURL();
	}
}