@isTest
private class OrderServices_CS_Test {
	public static List<Plan_Patient__c> planPatientList;
	public static List<Account> providerList;
	public static List<Order__c> orderList;
	public static List<Account> planList;
	public static List<Contact> planAdminList;
	
	public static TestDataFactory_CS platform;
	
	static testMethod void OrderServices_CS_Test(){
		init();
		
		OrderServices_CS cont = new OrderServices_CS(planAdminList[0].Id);
		SoqlBuilder alsb = cont.getBaseQuery();
		
		Order__c contOrder = cont.getOrderById(orderList[0].Id);
		system.assertEquals(orderList[0].Id, contOrder.Id);
		
		Apexpages.Standardsetcontroller orderSetCon = cont.getOrders();
		system.assertEquals(5, orderSetCon.getRecords().size());
		
		ApexPages.StandardSetController recOrderSetCon = cont.getRecurringOrders(orderList[4].Id);
		List<Order__c> recOrders = recOrderSetCon.getRecords();
		system.assertEquals(orderList[0].Id, recOrders[0].Id);
		
		Apexpages.Standardsetcontroller newOrderSetCon = cont.getNewestOrders(5);
	}
	
	static testMethod void OrderServices_CS_getBaseQueryProvider(){
		init();
		List<Contact> providerAdminList = platform.providerContactsToReturn;
		
		OrderServices_CS cont = new OrderServices_CS(providerAdminList[0].Id);
		SoqlBuilder alsb = cont.getBaseQuery();
	}
	
	static testMethod void OrderServices_CS_attachToBaseQueryProvider(){
		init();
		List<Contact> providerAdminList = platform.providerContactsToReturn;
		
		OrderServices_CS cont = new OrderServices_CS(providerAdminList[0].Id);
		SoqlBuilder sb = cont.attachToBaseQuery(new AndCondition()
													.add(new FieldCondition('Provider__c', providerList[0].Id)));
	}
	
	static testMethod void OrderServices_CS_futureGoLiveDate() {
		init();
		List<Contact> providerAdminList = platform.providerContactsToReturn;
		
		OrderServices_CS cont = new OrderServices_CS(providerAdminList[0].Id);
		cont.cAccount.Go_Live_Date__c = Date.today().addDays(7);
		
		SoqlBuilder alsb = cont.getBaseQuery();
		Apexpages.Standardsetcontroller setCon1 = new Apexpages.Standardsetcontroller(Database.getQueryLocator(alsb.toSoql()));
		
		SoqlBuilder sb = cont.attachToBaseQuery(new AndCondition()
													.add(new FieldCondition('Provider__c', providerList[0].Id)));
		Apexpages.Standardsetcontroller setCon2 = new Apexpages.Standardsetcontroller(Database.getQueryLocator(sb.toSoql()));
		
		System.assertEquals(0, setCon1.getResultSize());
		System.assertEquals(0, setCon2.getResultSize());
	}
	
	/*
	static testMethod void OrderServices_CS_Test_searchOrders(){
		init();
		List<Contact> providerAdminList = platform.providerContactsToReturn;
		
		OrderServices_CS cont = new OrderServices_CS(providerAdminList[0].Id);
		ApexPages.Standardsetcontroller setCon = cont.searchOrders('testFirst');
		system.assertEquals(1, setCon.getResultSize());
	}
	*/	
	static testMethod void OrderServices_CS_Test_getOrdersByEstimatedDeliveryDate(){
		init();
		orderList[0].Estimated_Delivery_Time__c = system.now().addDays(3);
		update orderList[0];
		System.debug('OrderList[0]: ' + orderList[0]);
		
		OrderServices_CS cont = new OrderServices_CS(planAdminList[0].Id);
		
		Apexpages.Standardsetcontroller orderSetCon = cont.getOrdersByEstimatedDeliveryDate(system.now(), system.now().addDays(7));
		System.debug('Order Set Controller Records: ' + orderSetCon.getRecords());
		List<Order__c> estOrders = orderSetCon.getRecords();
		system.assertEquals(orderList[0].Id, estOrders[0].Id);
	}
	
	/*
	//If getOrdersByDeliveryDate is used, Uncomment
	static testMethod void OrderServices_CS_Test_getOrdersByDeliveryDate(){
		init();
		
		
		orderList[0].Delivery_Date_Time__c = system.today().addDays(2);
		update orderList[0];
		system.assertEquals(system.today().addDays(2), [SELECT Delivery_Date_Time__c FROM Order__c WHERE Id = :orderList[0].Id].Delivery_Date_Time__c);
		
		OrderServices_CS cont = new OrderServices_CS(planAdminList[0].Id);
		
		Apexpages.Standardsetcontroller orderSetCon = cont.getOrdersByDeliveryDate(system.today(), system.today().addDays(7));
		system.debug('orderSetCon: ' + orderSetCon);
		List<Order__c> delOrders = orderSetCon.getRecords();
		system.assertEquals(orderList[0].Id, delOrders[0].Id);
	}
	*/
	
	static testMethod void OrderServices_CS_Test_getOrdersByStatus(){
		init();
		orderList[0].Status__c = 'Provider Accepted';
		update orderList[0];
		
		OrderServices_CS cont = new OrderServices_CS(planAdminList[0].Id);
		Apexpages.Standardsetcontroller setCon = cont.getOrdersByStatus('Accepted');
		List<Order__c> statusOrders = setCon.getRecords();
		system.assertEquals(orderList[0].Id, statusOrders[0].Id);
	}
	
	static testMethod void OrderServices_CS_Test_getOrdersByZip(){
		init();
		orderList[0].Zip__c = '87765';
		update orderList[0];
		
		OrderServices_CS cont = new OrderServices_CS(planAdminList[0].Id);
		Apexpages.Standardsetcontroller setCon = cont.getOrdersByZip('87765');
		List<Order__c> zipOrders = setCon.getRecords();
		system.assertEquals(orderList[0].Id, zipOrders[0].Id);
	}
	
	static testMethod void OrderServices_CS_Test_getOrdersByStateCity(){
		init();
		orderList[0].State__c = 'MA';
		orderList[0].City__c = 'Boston';
		update orderList[0];
		
		OrderServices_CS cont = new OrderServices_CS(planAdminList[0].Id);
		Apexpages.Standardsetcontroller setCon = cont.getOrdersByStateCity('MA', 'Boston');
		List<Order__c> stateCityOrders = setCon.getRecords();
		system.assertEquals(orderList[0].Id, stateCityOrders[0].Id);
	}
	
	static testMethod void OrderServices_CS_Test_getOrdersByLocation(){
		init();
		orderList[0].State__c = 'MA';
		orderList[0].City__c = 'Boston';
		orderList[0].Zip__c = '87765';
		orderList[0].Street__c = '111 S 1st St';
		orderList[0].Apartment_Number__c = '111';
		update orderList[0];
		
		OrderServices_CS cont = new OrderServices_CS(planAdminList[0].Id);
		Apexpages.Standardsetcontroller setCon = cont.getOrdersByLocation('87765', 'MA', 'Boston', '111 S 1st St', '111');
		List<Order__c> locOrders = setCon.getRecords();
		system.assertEquals(orderList[0].Id, locOrders[0].Id);
	}
	
	static testMethod void OrderServices_CS_SearchOrders() {
		init();
		
		OrderServices_CS cont = new OrderServices_CS(planAdminList[0].Id);
		Apexpages.Standardsetcontroller setCon = cont.searchOrders('testLast');
		System.Assert(setCon.getResultSize() > 0, 'No records returned.');
		setCon = cont.searchOrders('nonsenseString328up9hEgaV');
		System.Assert(setCon.getResultSize() == 0, 'Records returned: ' + setCon.getRecords());
	}
	
	static testMethod void OrderServices_CS_Test_returnAggregateOrdersByStatus(){
		init();
		orderList[0].Status__c = 'Accepted';
		orderList[1].Status__c = 'Accepted';
		orderList[2].Status__c = 'Approval Pending';
		orderList[3].Status__c = 'Approval Pending';
		orderList[4].Status__c = 'Delivered';
		update orderList;
		
		OrderServices_CS cont = new OrderServices_CS(planAdminList[0].Id);
		Integer numReturned = cont.returnAggregateOrdersByStatus('Accepted');
		system.assertEquals(2, numReturned);
	}
	
	static testMethod void OrderServices_CS_getOrdersByStage(){
		init();
		orderList[0].Status__c = 'Accepted';
		orderList[1].Status__c = 'Accepted';
		orderList[2].Status__c = 'Approval Pending';
		orderList[3].Status__c = 'Approval Pending';
		orderList[4].Status__c = 'Delivered';
		update orderList;
		
		OrderServices_CS cont = new OrderServices_CS(planAdminList[0].Id);
		ApexPages.Standardsetcontroller setcon = cont.getOrdersByStage('Recurring');
		system.assertEquals(2, setcon.getRecords().size());
	}
	
	static testMethod void OrderServices_CS_getOrderByStageNullStage(){
		init();
		
		OrderServices_CS cont = new OrderServices_CS(planAdminList[0].Id);
		try{
			ApexPages.Standardsetcontroller setcon = cont.getOrdersByStage(null);
			system.assert(false);
		}catch(Exception e){
			system.assert(true);
		}
	}
	
	static testMethod void OrderServices_CS_returnAggregateOrdersByStage(){
		init();
		orderList[0].Status__c = 'Accepted';
		orderList[1].Status__c = 'Accepted';
		orderList[2].Status__c = 'Approval Pending';
		orderList[3].Status__c = 'Approval Pending';
		orderList[4].Status__c = 'Delivered';
		update orderList;
		
		OrderServices_CS cont = new OrderServices_CS(planAdminList[0].Id);
		Integer numReturned = cont.returnAggregateOrdersByStage('Recurring');
		system.assertEquals(2, numReturned);
	}
	
	static testMethod void OrderServices_CS_Test_returnAggregateOrdersGroupedByStatus(){
		init();
		orderList[0].Status__c = 'Accepted';
		orderList[1].Status__c = 'Accepted';
		orderList[2].Status__c = 'Approval Pending';
		orderList[3].Status__c = 'Approval Pending';
		orderList[4].Status__c = 'Delivered';
		update orderList;
		
		OrderServices_CS cont = new OrderServices_CS(planAdminList[0].Id);
		List<AggregateResult> arOrders = cont.returnAggregateOrdersGroupedByStatus(system.today().addDays(-1), system.today().addDays(1));
	}
	
	static testMethod void OrderServices_CS_returnAggregateOrdersGroupedByStatusNoDate(){
		init();
		orderList[0].Status__c = 'Accepted';
		orderList[1].Status__c = 'Accepted';
		orderList[2].Status__c = 'Approval Pending';
		orderList[3].Status__c = 'Approval Pending';
		orderList[4].Status__c = 'Delivered';
		update orderList[0];
		
		OrderServices_CS cont = new OrderServices_CS(planAdminList[0].Id);
		List<AggregateResult> arOrders = cont.returnAggregateOrdersGroupedByStatus();
	}
	
	static testMethod void OrderServices_CS_getOrdersByFilters(){
		init();
		Map<String, String> filterMap = new Map<String, String>();
		filterMap.put('Provider__c', providerList[0].Id);
		
		OrderServices_CS cont = new OrderServices_CS(planAdminList[0].Id);
		cont.getOrdersByFilters(filterMap);
	}
	
	static testMethod void OrderServices_CS_getOrdersByFiltersEmpty(){
		init();
		Map<String, String> filterMap = new Map<String, String>();
		
		OrderServices_CS cont = new OrderServices_CS(planAdminList[0].Id);
		cont.getOrdersByFilters(filterMap);
	}
	
	static testMethod void OrderServices_CS_getNewestOrders(){
		init();
		Map<String, String> filterMap = new Map<String, String>();
		filterMap.put('Provider__c', providerList[0].Id);
		
		OrderServices_CS  cont = new OrderServices_CS(planAdminList[0].Id);
		cont.getNewestOrders(4, filterMap);
	}
	
	static testMethod void OrderServices_CS_getNewestOrdersNoFilters(){
		init();
		Map<String, String> filterMap = new Map<String, String>();
		
		OrderServices_CS  cont = new OrderServices_CS(planAdminList[0].Id);
		cont.getNewestOrders(4, filterMap);
	}
	
	static testMethod void OrderServices_CS_getOrderLineItems(){
		init();
		Order__c testOrder = platform.ordersToReturn[0];
		
		OrderServices_CS cont = new OrderServices_CS(planAdminList[0].Id);
		ApexPages.Standardsetcontroller setcon = cont.getOrderLineItems(testOrder);
		system.assertEquals(3, setcon.getRecords().size());
	}
	
	static testMethod void OrderServices_CS_getOrderLineItemsNullOrder(){
		init();
		Order__c testOrder = new Order__c();
		
		OrderServices_CS cont = new OrderServices_CS(planAdminList[0].Id);
		ApexPages.Standardsetcontroller setcon = cont.getOrderLineItems(testOrder);
		system.assertEquals(null, setcon);
	}
	
	static void init(){
		platform = new TestDataFactory_CS();
		
		planPatientList = platform.getPlanPatientList();
		providerList = platform.getProviderList();

		orderList = TestDataFactory_CS.generateOrders(planPatientList[0].Id, providerList[0].Id, 4);
		orderList.add(platform.getOrderList()[0]);
		orderList[0].Original_Order__c = orderList[4].Id;
		upsert orderList;
		
		
		planList = platform.getPlanList();
		planAdminList = TestDataFactory_CS.generatePlanContacts('Admin', planList, 1);
		insert planAdminList;
	}
}