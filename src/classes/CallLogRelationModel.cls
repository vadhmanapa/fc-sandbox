public class CallLogRelationModel extends AbstractSObjectModel_CS {
    
    public Call_Log_Relation__c instance {
        get{ return (Call_Log_Relation__c)this.record; }
    }
     
	public static List<CallLogRelationModel> getModels(List<Call_Log_Relation__c> records){
		List<CallLogRelationModel> models = new List<CallLogRelationModel>();
		
		for(Call_Log_Relation__c r : records){
			models.add(new CallLogRelationModel(r));
		}
		
		return models;
	}
        
    public SObject relation {get;set;}
    public String relationName {get;set;}    
    
    public CallLogRelationModel(Call_Log_Relation__c record) {
    	super(record);
    }
    
    public CallLogRelationModel(Call_Log_Relation__c record, String relationName) {
    	super(record);
    	
    	this.relationName = relationName;
    }
    
    public static Boolean isPopulated(Call_Log_Relation__c input) {
        return (
            input.Case__c != null ||
            input.Claims_Inquiry__c != null |
            input.Contact__c != null ||
            input.Other_Call_Log__c != null ||
            input.Order__c != null ||
            input.Patient__c != null
        );
    }
    
    public Boolean isPopulated {
        get {
            return (
                instance.Case__c != null ||
                instance.Claims_Inquiry__c != null |
                instance.Contact__c != null ||
                instance.Other_Call_Log__c != null ||
                instance.Order__c != null ||
            	instance.Patient__c != null
            );
        }
    }
        
    public Schema.SObjectType relationType {
        get {
            if(instance == null){
                return null;
            }
            
            if(instance.Case__c != null) {
                return Case.getSObjectType();
            } else if(instance.Claims_Inquiry__c != null) {
                return Claims_Inquiry__c.getSObjectType();
            } else if(instance.Contact__c != null) {
                return Contact.getSObjectType();
            } else if(instance.Other_Call_Log__c != null) {
                return Call_Log__c.getSObjectType();
            } else if(instance.Order__c != null) {
                return Order__c.getSObjectType();
            } else if(instance.Patient__c != null) {
                return Patient__c.getSObjectType();
            }
            
            return null;
        }
    }
        
    public String relationTypeName {
        get {
            if(relationType == null) {
                return 'Error';
            }
            
            return relationType.getDescribe().getName();
        }
    }
}