/**
 *  @Description A controller class for PayerOrdersSummary_CS page
 *  @Author Cloud Software LLC
 *  Revision History:
 *		03/31/2017 - dgarkusha - Sprint 3/20 - Added default sorting values for order summary page.
 */

public without sharing class PayerOrdersSummary_CS extends PayerPortalServices_CS {

	private final String pageAccountType = 'Payer';
    
    private PayerOrderServices_CS payerOrderServices;
	private UserServices_CS userServices;
    
    public Apexpages.Standardsetcontroller orderSetCon {get;set;}
    public List<Order__c> orderList 
    {
        get{
			List<Order__c> retOrderList = new List<Order__c>();
            if(orderSetCon.getRecords() != null){
                retOrderList = (List<Order__c>) orderSetCon.getRecords();
            }
            return retOrderList;
        }
        set;
    }
    
    /******* Filtering *******/
    // public String filterByStage {get{ return (filterByStage == null) ? '' : filterByStage; }set;}
     public String filterByStage {
     	/*
		** CP-80: Default Order Summary Table Sorting - SPRINT 3/20/2017 - ADDED BY DGARKUSHA
	 	*/
		get {
			return (filterByStage == null) ? '' : filterByStage;
		}
		set {
			this.filterByStage = value;
			this.sortColumn = null;
			this.sortOrder = null;
		}
	}
    public String filterBySearch {get{ return (filterBySearch == null) ? '' : filterBySearch; }set;}
    public String filterByTime {get{ return (filterByTime == null) ? '' : filterByTime; }set;}
    
    /**
     ** NOTE: STAGING FOR FUTURE RELEASE - SPRINT 12/12 - BY VK **/
    
     public String filterBySubStage {get{return (filterBySubStage == null) ? '' : filterBySubStage;}set;} // added by VK on 01-05-2015 for Sprint 12/12
    
    /******* For Filtering by User/Account *******/
    
    public List<SelectOption> filterByEntityTypeOptions {
    	get{
    		return new List<SelectOption>{
    			new SelectOption('User','User'),
    			new SelectOption('Provider','Provider')
    		};
    	}
    	set;
    }
    public String filterByEntityType {
    	get{ 
    		return (filterByEntityType == null) ? 'User' : filterByEntityType; 
    	}
    	set;
    }
    
    public List<SelectOption> filterByUserOptions {get;set;}
    public String filterByUser {get{ return (filterByUser == null) ? '' : filterByUser; }set;}
    
    public List<SelectOption> filterByProviderOptions {get;set;}
    public String filterByProvider {get{ return (filterByProvider == null) ? '' : filterByProvider; }set;}
    
    public Map<Id,String> userMap {get;set;}
	public Map<Id,String> providerMap {get;set;}
        
    /************ Interface *************/
    public Integer pageSize
    {
    	get{
    		if (pageSize == null){ pageSize = 10; }
    		return pageSize;
    	} 
		set{
			pageSize = value;
			if(orderSetCon != null){ orderSetCon.setPageSize(value); }
		} 	
    }

    public Integer totalPages {
    	get{
    		return (Integer)Math.ceil((Decimal)orderSetCon.getResultSize() / orderSetCon.getPageSize());
    	}
    	set;
	}

    public String userFilter {get;set;}
    public String selectedOrderId {get;set;}
    
    //public Integer numberReturnedRecords {get;set;}


    /******* Constructor *******/   
    public PayerOrdersSummary_CS(){}
    
	// Default page action, add additional page action functionality here
	public PageReference init(){
		
		///////////////////////////////////
		// Authentication and Permission
		///////////////////////////////////
		
		if(!isSecure && !Test.isRunningTest()) return DoLogout();
				
		if(!authenticated) return DoLogout();

		if(portalUser.AccountType != pageAccountType) return homepage;

		///////////////////////////////////
		// Additional init
		///////////////////////////////////
		
		payerOrderServices = new PayerOrderServices_CS(portalUser.instance.Id);
		userServices = new UserServices_CS(portalUser.instance.Id);
		
	    //Default behavior if passed a filter that isn't included
	    if(ApexPages.currentPage().getParameters().containsKey('filter')){
	    	filterByStage = ApexPages.currentPage().getParameters().get('filter');
	    }
	    else{
	    	filterByStage = '';
	    }
	    
	    //Default to current user for Filter - Commented out until internal procedures are concluded
	    //filterByUser = String.valueOf(portalUser.instance.Id);
	    filterByTime = 'thisWeek';
	    
        /**********************************************************************************
         
		 ******** NOTE: TO BE DEPLOYED IN LATER SPRINTS ***********
		 
        if (this.STAGES_WITH_ORDERS_FOR_ALL_TIME.contains(this.filterByStage)){
	    	filterByTime = 'allTime';
            pageSize = 100;
	    }else {
	    	filterByTime = 'thisWeek';
            pageSize = 10;
	    }

		***********************************************************************************/
	    
	    //Retrieve the unfiltered list of orders to display
	    populateOrderList();
		
		//Populate the filter boxes
		populateFilterBoxes();
		
		return null;
	}//End Init
    
    public void populateOrderList(){

    	/*if (this.STAGES_WITH_ORDERS_FOR_ALL_TIME.contains(this.filterByStage)){ */
    	/*
		** CP-80: Default Order Summary Table Sorting - SPRINT 3/20/2017 - ADDED BY DGARKUSHA
	 	*/
			/*this.filterByTime = 'allTime';
			this.pageSize = 100;
		}else {
            this.filterByTime = 'lastMonth';
            this.pageSize = 10;
        }*/

    	//Mutually-exclusive searching for users/providers
    	if(filterByEntityType == 'User'){
    		filterByProvider = '';
    	}
    	else{
    		filterByUser = '';
    	}

		System.debug(
			'\nFilter terms: ' +
			'\nstage: ' + filterByStage +
			'\nuserId: ' + filterByUser +
			'\nsearchTerm: ' + filterBySearch +
			'\ntimeFrame: ' + filterByTime
		);
    	
    	/**
	     ** NOTE: STAGING FOR FUTURE RELEASE - SPRINT 12/12 - BY VK - TO BE REPLACED WITH CURRENT orderSetCon
	     ** orderSetCon = payerOrderServices.getOrdersForSummary(filterByStage, filterBySubStage, filterByUser,filterByProvider,filterBySearch,filterByTime);
	    **/

		if (String.isNotEmpty(this.sortColumn)) this.sortColumn.trim();

		System.debug('\n\n sortColumn => ' + this.sortColumn + '\n');
		System.debug('\n\n sortOrder => ' + this.sortOrder + '\n');
        
        orderSetCon = payerOrderServices.getOrdersForSummaryWithSort(filterByStage,filterBySubStage,filterByUser,filterByProvider,filterBySearch,filterByTime, new Map<String, String> {this.sortColumn => this.sortOrder});
    	orderSetCon.setPageSize(pageSize);
    	
		System.debug('orderSetCon pageSize: ' + orderSetCon.getPageSize());
		System.debug('orderSetCon resultSize: ' + orderSetCon.getResultSize());
		System.debug('orderSetCon # of pages: ' + totalPages);
    }

	// 4/23/15 Changed population method for filterByUserOptions
	public void populateFilterBoxes(){
		
		//Generate filter content based on order results 
	    filterByUserOptions = new List<SelectOption>{new SelectOption('','All')};
	    filterByProviderOptions = new List<SelectOption>{new SelectOption('','All')};
	    
	    //NOTE: We need to use the entire order list, not the subset, to grab info	    
	    List<Order__c> completeList = payerOrderServices.getOrderListForSummary(filterByStage,filterBySubStage,'',filterByProvider,filterBySearch,filterByTime);
	    
	    userMap = new Map<Id,String>();
	    providerMap = new Map<Id,String>();
	    
	    //userMap.put(portalUser.instance.Id, portalUser.instance.Name);
	    
	    for(Order__c o : completeList){
	    	/* 4/23/15 Changed population method for userMap
	    	if(o.Entered_By__c != null){
	    		userMap.put(o.Entered_By__c,o.Entered_By__r.Name);
	    	}*/
	    	if(o.Provider__c != null){
    			providerMap.put(o.Provider__c,o.Provider__r.Name);
    		}
	    }
	    
	    //New method for populating filterByUserOptions
	   	for (Contact c : [SELECT Name
						  FROM Contact 
						  WHERE Entity__c = :portalUser.instance.Entity__c
						  AND Active__c = true
						  ORDER BY Name asc]) {
			userMap.put(c.Id, c.Name);
			filterByUserOptions.add(new SelectOption(String.valueOf(c.Id),c.Name));
		}
	    
	    /*for(Id i : userMap.keySet()){
	    	filterByUserOptions.add(new SelectOption(String.valueOf(i),userMap.get(i)));
	    }*/
	    
	    for(Id i : providerMap.keySet()){
	    	filterByProviderOptions.add(new SelectOption(String.valueOf(i),providerMap.get(i)));
	    }
	}
}