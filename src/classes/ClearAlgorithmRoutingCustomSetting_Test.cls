/*===============================================================================================
 * Cloud Software LLC
 * Name: ClearAlgorithmRoutingCustomSetting_Test
 * Description: Test class for ClearAlgorithmRoutingCustomSetting
 * Created Date: 9/21/2015
 * Created By: Andrew Nash (Cloud Software)
 * 
 * 	Date Modified			Modified By			Description of the update
 *	SEP 21, 2015			Andrew Nash			Created
 *
 *	01/07/2015 - karnold - Deprecated. Class will be deleted on release.
 ==============================================================================================*/
@isTest
public without sharing class ClearAlgorithmRoutingCustomSetting_Test {
	/*
	public static Clear_Algorithm_Routing__c testCustomSetting;

	//===========================================================================================
	//===================== USE CLEAR ALGORITHM ROUTING TRUE/FALSE TEST =========================
	//===========================================================================================
	static testmethod void UseClearAlgorithmRouting_False_Test() {
		
		// ASSUME
		ClearAlgorithmRoutingCustomSetting.test_useNewAlgorithm = true;
		testCustomSetting = TestDataFactory_CS.clearAlgorithmCustomSetting(false);

		// WHEN
		Test.startTest();
		Boolean testSetting = ClearAlgorithmRoutingCustomSetting.UseClearAlgorithmRouting();
		Boolean customSetting = ClearAlgorithmRoutingCustomSetting.useNewAlgorithm;
		Test.stopTest();

		// THEN
		System.assertEquals(true, testSetting, 'TEST ERROR: testSetting variable returned false');
		System.assertEquals(false, customSetting, 'TEST ERROR: customSetting variable returned true');

	}
	//===========================================================================================
	//=================== USE CLEAR ALGORITHM ROUTING FASLE/TRUE TEST ===========================
	//===========================================================================================
	static testmethod void UseClearAlgorithmRouting_True_Test() {
		
		// ASSUME
		ClearAlgorithmRoutingCustomSetting.test_useNewAlgorithm = false;
		testCustomSetting = TestDataFactory_CS.clearAlgorithmCustomSetting(true);

		// WHEN
		Test.startTest();
		Boolean testSetting = ClearAlgorithmRoutingCustomSetting.UseClearAlgorithmRouting();
		Boolean customSetting = ClearAlgorithmRoutingCustomSetting.useNewAlgorithm;
		Test.stopTest();

		// THEN
		System.assertEquals(false, testSetting, 'TEST ERROR: testSetting variable returned true');
		System.assertEquals(true, customSetting, 'TEST ERROR: customSetting variable returned false');

	}
	//===========================================================================================
	//========================= USE CLEAR ALGORITHM ROUTING NULL TEST ===========================
	//===========================================================================================
	static testmethod void UseClearAlgorithmRouting_Null_Test() {
		
		// ASSUME
		// Nothing to declare since this is testing a null value.

		// WHEN
		Test.startTest();
		Boolean testSetting = ClearAlgorithmRoutingCustomSetting.UseClearAlgorithmRouting();
		Boolean customSetting = ClearAlgorithmRoutingCustomSetting.useNewAlgorithm;
		Test.stopTest();

		// THEN
		System.assertEquals(false, testSetting, 'TEST ERROR: testSetting variable returned false');
		System.assertEquals(false, customSetting, 'TEST ERROR: customSetting variable returned false');

	}
	*/
}