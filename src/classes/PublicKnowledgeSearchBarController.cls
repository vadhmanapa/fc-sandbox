public with sharing class PublicKnowledgeSearchBarController {

	public String searchString {get;set;}

	public PageReference search() {
        
        System.debug(LoggingLevel.INFO,'Searching for: ' + searchString);
        
        if(String.isBlank(searchString)) {
            //Some validation message
            System.debug(LoggingLevel.WARN,'SearchString is blank.');
            return null;
        }
        
        PageReference searchPage = Page.PublicKnowledgeSearch;
        searchPage.getParameters().put('search',searchString);
        searchPage.setRedirect(true);
        
        System.debug('Redirecting: ' + searchPage.getUrl());
        
        return searchPage;
    }
}