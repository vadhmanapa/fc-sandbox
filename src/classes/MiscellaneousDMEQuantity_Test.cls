/**
 *  @Description Test class for MiscellaneousDMEQuantity Batch
 *  @Author Cloud Software LLC, Kelly Sharp
 *  Revision History: 
 *		12/09/2015 - created
 *		12/11/2015 - karnold - Reformatted header, added DMEs to test setup
 */
@isTest
public class MiscellaneousDMEQuantity_Test { 

	static testmethod void batch_Test() {

		// ASSUME
		List<DME__c> dmes = TestDataFactory_CS.generateDMEs(3);
		dmes[0].Max_Delivery_Time__c = 100;
		dmes[1].Max_Delivery_Time__c = 100;
		dmes[2].Max_Delivery_Time__c = 100;
		dmes[2].Miscellaneous__c = true;
		insert dmes;

		Account plan = TestDataFactory_CS.generatePlans('testPlan', 1)[0];
		insert plan;
		Plan_Patient__c planPatient = TestDataFactory_CS.generatePlanPatientsPlanOnly(plan.Id, 1)[0];
		insert planPatient;
		Account prov = TestDataFactory_CS.generateProviders('Test Provider', 1)[0];
		insert prov;
		Order__c ord = TestDataFactory_CS.generateOrders(planPatient.Id, prov.Id, 1)[0];
		insert ord;

		Map<Id, Map<Id, Decimal>> orderToDMEToQuantity = new Map<Id, Map<Id, Decimal>>();
		Map<Id, Decimal> dmeToQuant = new Map<Id, Decimal>();
		dmeToQuant.put(dmes[0].Id, 10);
		dmeToQuant.put(dmes[1].Id, 11);
		dmeToQuant.put(dmes[2].Id, 12);
		orderToDMEToQuantity.put(ord.Id, dmeToQuant);
		List<DME_Line_Item__c> dmeLineItems = TestDataFactory_CS.generateDMELineItems(orderToDMEToQuantity);
		insert dmeLineItems;
		
		MiscellaneousDMEQuantityDelete batch = new MiscellaneousDMEQuantityDelete();

		// WHEN
		Test.startTest();
			Database.executeBatch(batch);
		Test.stopTest();

		// THEN
		DME_Line_Item__c dmeMisc = [SELECT Id, Quantity__c FROM DME_Line_Item__c WHERE DME__r.Miscellaneous__c = true];
		System.assertEquals(null, dmeMisc.Quantity__c);

		List<DME_Line_Item__c> dmeList = [SELECT Id, Quantity__c FROM DME_Line_Item__c WHERE DME__r.Miscellaneous__c = false];
		System.assert(!dmeList.isEmpty());
		for (DME_Line_Item__c dmeLi : dmeList) {
			System.assertNotEquals(null, dmeLi.Quantity__c);
		}
	}
}