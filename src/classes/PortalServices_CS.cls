/**
 *  @Description A service class for provider and payer portal services
 *  @Author Cloud Software LLC
 *  Revision History:
 *		11/22/2016 - vadhmanapa - Sprint 11/21 - Added time zone offset to counter issue with times getting offseted due to daylight savings time.
 *		03/31/2017 - dgarkusha - Sprint 3/20 - Added default sorting values for order summary page.
 */

public virtual without sharing class PortalServices_CS{

	public Boolean authenticated = false;	
	public UserModel_CS portalUser {get;set;}
	
	/********* Page Interface *********/

	/*
	** CP-80: Default Order Summary Table Sorting - SPRINT 3/20/2017
	** SET DEFAULT VALUES FOR ORDER SUMMARY SORTING
	** ADDED BY DGARKUSHA
	 */
	public String sortColumn {
		get {
			return String.isNotEmpty(this.sortColumn) ? sortColumn : OrderServices_CS.defaultTableSortColumn;
		}
		set;
	}
	public String sortOrder {
		get {
			return String.isNotEmpty(this.sortOrder) ? sortOrder : OrderServices_CS.defaultTableSortOrder;
		}
		set;
	}

	public final Set<String> STAGES_WITH_ORDERS_FOR_ALL_TIME = new Set<String>{
		'Pending Acceptance',
		//'Needs My Attention',
		//'Needs Payor Attention',
		//'All Needs Attention',
		'Needs Attention',
		'Future DOS'
	};

	// Get sessionId, this should always be unique.
	// If it appears these are not unique on browser close->open then
	// look into methods using Visualforce

	public static String guestUserId {	get{ return UserInfo.getUserId(); } }

	public static String sessionId{	get{ return Userinfo.getSessionId(); } }

	public static Boolean isSecure{	get{ return Apexpages.currentPage().getHeaders().containsKey('CipherSuite'); } }

	public String urlPath
	{
		get
		{
			if(urlPath == null){ urlPath = GetResourceURL('PortalSupportFiles'); }
			return(urlPath);
		}
		set;
	}

	/******* Constructor *******/	

	public PortalServices_CS(){
	    //IE9 Visualforce hack
	    String browserType = Apexpages.currentPage().getHeaders().get('USER-AGENT'); //gets the browser name 
	    if(browserType != null && browserType.contains('MSIE')){
	        Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=8');
	    }
        
	    checkAuthentication();	    	    
	}
    
    /*** daylight saving calculation -- BY VK ON 11/22/2016---***/
    
    /*public static Double timeZoneOffSet(){
        
        Timezone userTZ = Timezone.getTimeZone('America/New_York'); // set for New York timezone for now. Later can have portal user based timezone
        
        DateTime newdt = DateTime.newInstance(System.now().date(), System.now().timeGmt());
	    return (Double)(userTZ.getOffset(newdt))/(1000*60*60*24);
    }*/
	
	public Boolean checkAuthentication(){
		
		//Look for cookie information
		if(ApexPages.currentPage().getCookies().containsKey('userid')){
			if(ApexPages.currentPage().getCookies().containsKey('sessionid')){
				
				Cookie userIdCookie = ApexPages.currentPage().getCookies().get('userid');
				Cookie sessionIdCookie = ApexPages.currentPage().getCookies().get('sessionid');
				
				String encryptedUserId = userIdCookie.getValue();
				String encryptedSessionId = sessionIdCookie.getValue(); 			
				
				//Get the contact information				
				try{
					//Contact c = [select Id,Name,Salt__c from Contact where Id_Key__c = :encryptedUserId];
					
					Contact portalContact = getPortalUser(encryptedUserId);
					
					portalUser = new UserModel_CS(portalContact);
					
					//Match the session Id
					if(encryptedSessionId == PasswordServices_CS.hash(sessionId,portalContact.Salt__c))
					{
						authenticated = true;
						
						updateAuthentication();
						
						System.debug('checkAuthentication: Authenticated ' + portalUser.instance.Username__c + ' successfully!');
						return true;
					}
					else{
						System.debug('checkAuthentication Error: Cookie sessionId ' + encryptedSessionId + ' doesn\'t match ' + PasswordServices_CS.hash(sessionId,portalContact.Salt__c));
					}
				}
				catch(Exception e){
					System.debug('checkAuthentication Exception: Could not find portalUser with Id Key: ' + encryptedUserId);				
					throw ((e instanceOf ApplicationException) ? e : new ApplicationException('Authentication has expired.',e));
				}
			}
			else{
				System.debug('checkAuthentication: Cookie sessionId not found.');
			}
		}
		else{
			System.debug('checkAuthentication: Cookie userId not found.');
		}
		
		return false;
	}
	/*
	public Boolean DoAuthenticate(){

		if(ApexPages.currentPage().getCookies().containsKey('userid')){
			if(ApexPages.currentPage().getCookies().containsKey('sessionid')){
				
				Cookie userIdCookie = ApexPages.currentPage().getCookies().get('userid');
				Cookie sessionIdCookie = ApexPages.currentPage().getCookies().get('sessionid');
				
				String cookieUserId = userIdCookie.getValue();
				String cookieSessionId = sessionIdCookie.getValue(); 
								
				try{
					Contact c = [select Id,Name,Salt__c from Contact where Id_Key__c = :cookieUserId];
					
					portalUser = new UserModel_CS(cookieUserId);
					
					if(	cookieSessionId != null && cookieSessionId != '' && 
						cookieSessionId == PasswordServices_CS.hash(sessionId,c.Salt__c))
					{
						authenticated = true;
						
						updateCookies();				
						
						System.debug('DoAuthenticate: Authenticated ' + portalUser.instance.instance.Username__c + ' successfully!');				
						return true;
					}
					else{
						System.debug('DoAuthenticate Error: Cookie sessionId ' + cookieSessionId + ' doesn\'t match ' + PasswordServices_CS.hash(sessionId,c.Salt__c));
					}
				}
				catch(Exception e){
					System.debug('DoAuthenticate() Exception: Cookie sessionId ' + cookieSessionId + ' doesn\'t match ' + Userinfo.getSessionId());				
					throw ((e instanceOf ApplicationException) ? e : new ApplicationException('Authentication has expired.',e));
				}
			}
			else{
				System.debug('DoAuthenticate(): Cookie sessionId not found.');
			}
		}
		else{
			System.debug('DoAuthenticate(): Cookie userId not found.');
		}
		
		return false;
	}
	*/
	
	public PageReference loginPage{
		get{
			if(ApexPages.currentPage().getUrl().toLowerCase().contains('provider')){
				return Page.ProviderPortalLogin_CS.setRedirect(true);
			}
			else{
				return Page.PayerPortalLogin_CS.setRedirect(true);
			}
		}
	}
	
	public PageReference homePage{
		get{
			if(portalUser != null){
				if(portalUser.instance.Entity__r.Type__c == 'Provider'){
					return Page.ProviderHomeDashboard_CS;
				}
				else if(portalUser.instance.Entity__r.Type__c == 'Payer'){
					return Page.PayerHomeDashboard_CS;
				}
			}
			else{
				System.debug('PortalServices Homepage Error: No user context.');
			}
			
			//Extreme fallback
			if(ApexPages.currentPage().getUrl().toLowerCase().contains('provider')){
				return Page.ProviderHomeDashboard_CS.setRedirect(true);
			}
			else{
				return Page.PayerHomeDashboard_CS.setRedirect(true);
			}
			/*
			if(ApexPages.currentPage().getUrl().toLowerCase().contains('providerportal')){
				return Page.ProviderHomeDashboard_CS.setRedirect(true);
			}
			else if(ApexPages.currentPage().getUrl().toLowerCase().contains('payerportal')){
				return Page.PayerHomeDashboard_CS.setRedirect(true);
			}
			
			*/
		}
	}
	
	public PageReference DoLogout(){
	
		// Invalidate all forms of authentication
		authenticated = false;
		portalUser = null;
			
		Cookie userIdCookie = new Cookie('userid',null,null,0,false);
		Cookie sessionCookie = new Cookie('sessionid', null, null, 0, false);
		
		//Redirect to login page
		System.debug('Redirecting to Login page');
		
		ApexPages.currentPage().setCookies(new Cookie[]{userIdCookie,sessionCookie});
		ApexPages.currentPage().setRedirect(true);		
		
		return loginPage;
	}
		
	public void addAuthentication(String userId){
		
		String encryptedUserId = PasswordServices_CS.hash(userId,portalUser.instance.Salt__c);
		String encryptedSessionId = PasswordServices_CS.hash(sessionId, portalUser.instance.Salt__c);
		
		Cookie userIdCookie = new Cookie('userid', encryptedUserId, null, 3600, isSecure);
		Cookie sessionIdCookie = new Cookie('sessionid', encryptedSessionId, null, 3600, isSecure);
		
		ApexPages.currentPage().setCookies(new Cookie[]{userIdCookie,sessionIdCookie});
		
		System.debug('addAuthentication: Successfully added authentication.');
	}
	
	public void removeAuthentication(){
	
		// Invalidate all forms of authentication
		authenticated = false;
		portalUser = null;
			
		Cookie userIdCookie = new Cookie('userid',null,null,0,false);
		Cookie sessionCookie = new Cookie('sessionid', null, null, 0, false);
		
		//Redirect to login page
		System.debug('Redirecting to Login page');
		
		ApexPages.currentPage().setCookies(new Cookie[]{userIdCookie,sessionCookie});
		ApexPages.currentPage().setRedirect(true);
		
		System.debug('removeAuthentication: Successfully removed authentication.');
	}
	
	public Boolean updateAuthentication(){
		if(portalUser != null){
			
			String encryptedUserId = PasswordServices_CS.hash(portalUser.instance.Id,portalUser.instance.Salt__c);
			String encryptedSessionId = PasswordServices_CS.hash(sessionId, portalUser.instance.Salt__c);
			
			Cookie userIdCookie = new Cookie('userid', encryptedUserId, null, 3600, isSecure);
			Cookie sessionIdCookie = new Cookie('sessionid', encryptedSessionId, null, 3600, isSecure);
			
			ApexPages.currentPage().setCookies(new Cookie[]{userIdCookie,sessionIdCookie});
			
			System.debug('updateAuthentication: Successfully updated userid and sessionid.');
			
			return true;
		}
		else{
			System.debug('updateAuthentication Error: No user context.');
		}
		return false;
	}
	
	public Contact getPortalUser(String encryptedUserId){
	
		SoqlBuilder query = new SoqlBuilder()
			.selectx(UserModel_CS.objectFields)
			.fromx('Contact')
			.wherex(new FieldCondition('Id_Key__c').equals(encryptedUserId))
			.limitx(1);
		
		return Database.query(query.toSoql());
	}
	
	/*
	*	Page Content
	*/

	/*
	public String loginHeader 
	{
		get
		{
			if(loginHeader == null)
			{
				loginHeader = '<head>';
				loginHeader += '<meta charset="utf-8">';
				loginHeader += '<meta name="viewport" content="initial-scale = 1, user-scalable = no">';
				loginHeader += '<title>Integra - Health Plans</title>';
				loginHeader += '';
				loginHeader += '<!--Load Theme CSS -->';
				loginHeader += '<link rel="stylesheet" href="' + urlPath + '/css/foundation.css">';
				loginHeader += '<link rel="stylesheet" href="' + urlPath + '/css/datepicker.css")}">';
				loginHeader += '<link rel="stylesheet" href="' + urlPath + '/css/header.css")}">';
				loginHeader += '<link rel="stylesheet" href="' + urlPath + '/css/dashboard.css")}">';
				loginHeader += '<link rel="stylesheet" href="' + urlPath + '/css/pathway.css")}">';
				loginHeader += '<link rel="stylesheet" href="' + urlPath + '/css/secondary.css")}">';
				loginHeader += '<link rel="stylesheet" href="' + urlPath + '/css/summary-orders.css")}">';
				loginHeader += '<link rel="stylesheet" href="' + urlPath + '/css/order.css")}">';
				loginHeader += '<link rel="stylesheet" href="' + urlPath + '/css/ie.css")}">';
				loginHeader += '<link rel="stylesheet" href="' + urlPath + '/css/general_foundicons.css")}">';
				loginHeader += '';
				loginHeader += '<!--Load JS Files -->';
				loginHeader += '<script src="' + urlPath + '/js/vendor/modernizr.js")}"></script>';
				loginHeader += '<script src="' + urlPath + '/js/chart.min.js")}"></script>';		
				loginHeader += '';
				loginHeader += '<!--IE -->';
				loginHeader += '<!--[if lt IE 8]>';
				loginHeader += ' <link rel="stylesheet" href="' + urlPath + '/stylesheets/general_foundicons_ie7.css">';
				loginHeader += ' <script src="' + urlPath + '/js/excanvas.js"></script>';
				loginHeader += '  <![endif]-->';
				loginHeader += '</head>';
				loginHeader += '';
				loginHeader += '';
				loginHeader += '<body>';
				loginHeader += '<!--[if lt IE 8]>';
				loginHeader += ' <div id="ie">Hello, we see you\'re using an <strong>outdated version of Internet Explorer</strong>. This site will remain readable for you but you will be missing out on some more advanced features and layout that only more recent, standards-compliant, browsers like <a href="http://www.firefox.com/" title="Firefox web browser | Faster, more secure, &amp; customizable">Firefox</a>, <a href="http://www.apple.com/safari/download/" title="Apple - Safari - Download Safari - Download the world&#8217;s fastest and most innovative browser for Mac and PC">Safari</a>, <a href="http://www.google.com/chrome" title="Google Chrome - Een nieuwe browser downloaden">Google Chrome</a> or <a href="http://www.browserforthebetter.com/download.html" title="Internet Explorer 8 - Browser for the Better">Internet Explorer 9</a> support. You might also see the odd glitch here and there. If you can upgrade, we encourage you to do so, you\'ll like it.</div>';
				loginHeader += '<![endif]-->';
			}
			return(loginHeader);
		}
		
		set;
	}		
	*/
	
	public String loginNav
	{
		get
		{
			if(loginnav == null)
			{
				loginnav ='<!--header-->';
				loginnav +='<div class="header">';
				loginnav +='<div class="row">';
				loginnav +='  <div class="large-4 small-12 columns">';
				loginnav +='  <img src="' + urlPath + '/images/health-plan-logo.png")}" alt="Integra for Health Plan">';
				loginnav +='  </div>';
				loginnav +='  <!--right navigation-->';
				loginnav +='  <nav class="large-8 small-12 columns" role="navigation">';
				loginnav +='  <ul class="menu">';
				//loginnav +='   <li><a class="ico-3" href="OrdersSummary_CS">My Orders</a></li>';
				//loginnav +='   <li><a class="ico-2" href="MyPatients_CS">Patients</a></li>';
				//loginnav +='   <li><a class="ico-1" href="MyProfile_CS">My Profile</a></li>';
				//loginnav +='   <li><a class="ico-4" href="#">Settings</a></li>';
				//loginnav +='   <li><a class="ico-5" href="LogoutRedirect_CS">Logout</a></li>';
				loginnav +='  </ul>';
				loginnav +='  </nav>';
				loginnav +=' </div>';
				loginnav +='</div><!--end header-->';			
			}
			return(loginnav);
		}
		set;
	}

	// Pass the resource name
    public static String GetResourceURL(String resourceName)
    {

    	// Fetching the resource
        List<StaticResource> resourceList= [SELECT Name, NamespacePrefix, SystemModStamp FROM StaticResource WHERE Name = :resourceName];

		System.debug('# of resources: ' + resourceList.size());

        // Checking if the result is returned or not
        if(resourceList.size() == 1)
        {
        	// Getting namespace
            String namespace = resourceList[0].NamespacePrefix;
            // Resource URL
            return '/resource/' + resourceList[0].SystemModStamp.getTime() + '/' + (namespace != null && namespace != '' ? namespace + '__' : '') + resourceName; 
        }
        else return '';
   	}
   
	public static Boolean isRunningInSandbox() {
    	String s  =  System.URL.getSalesforceBaseUrl().getHost();
	    return (Pattern.matches('(.*\\.)?cs[0-9]*(-api)?\\..*force.com',s));
	} 

}