@isTest
private class PatientServices_CS_Test {
	public static List<Account> planList;
	public static List<Contact> planAdminList;
	public static List<Plan_Patient__c> planPatientList;
	
    static testMethod void PatientServices_CS_Test() {
    	init();
    	
    	PatientServices_CS cont = new PatientServices_CS(planAdminList[0].Id);
    	//Boolean insertSucceeded = cont.insertPatient(patientList[0]);
    	//system.assert(insertSucceeded);
    	upsert planPatientList;
    	Id patientId = planPatientList[0].Id;
    	SoqlBuilder testSOQL = cont.attachToBaseQuery(new AndCondition()
											.add(new FieldCondition('Id').equals(patientId)));
    	
    	Plan_Patient__c patientOne = cont.getPlanPatientById(String.valueOf(planPatientList[0].Id));
    	system.assertNotEquals(null, patientOne);
    	
    	Apexpages.Standardsetcontroller setCon = cont.getPatients();
    	system.assertEquals(5, setCon.getRecords().size());
    	
    	//Boolean deleteSucceeded = cont.deletePatient(planPatientList[0]);
    	//system.assertEquals(deleteSucceeded);
    }

	static testMethod void PatientServices_CS_getBaseQuery() {
        init();

        PatientServices_CS cont = new PatientServices_CS(planAdminList[0].Id);
        SoqlBuilder soql = cont.getBaseQuery();

        String soqlString = soql.toSoql();
        System.debug('\n\n soqlString => ' + soqlString + '\n');

        System.assert(String.isNotEmpty(soqlString));
    }

    static testMethod void PatientServices_CS_getPatientsWithSort_SearchByDateWithAscSortingByFirstName() {
        init();

        PatientServices_CS cont = new PatientServices_CS(planAdminList[0].Id);

        Apexpages.Standardsetcontroller setCon = cont.getPatientsWithSort(Date.today().format(), new Map<String, String>{'First Name' => 'ASC'});
        System.assertEquals(5, setCon.getResultSize());
    }

    static testMethod void PatientServices_CS_getPatientsWithSort_SearchByFullNameWithDescSortingByFirtsName() {
        init();

        PatientServices_CS cont = new PatientServices_CS(planAdminList[0].Id);

        Apexpages.Standardsetcontroller setCon = cont.getPatientsWithSort('testFirst testSecond', new Map<String, String>{'First Name' => 'DESC'});
        System.assertEquals(0, setCon.getResultSize());
    }
    
    static void init(){
    	List<Patient__c> patientList = TestDataFactory_CS.generatePatients('testLast', 5);
    	insert patientList;
    	system.debug(patientList);
    	
    	planList = TestDataFactory_CS.generatePlans('testPlan', 1);
    	insert planList;
    	system.debug(planList);
    	
    	Map<Id, List<Patient__c>> planToPatients = new Map<Id, List<Patient__c>>();
    	planToPatients.put(planList[0].Id, patientList);
    	planPatientList = TestDataFactory_CS.generatePlanPatients(planToPatients);
    	insert planPatientList;
    	system.debug(planPatientList);
    	
    	planAdminList = TestDataFactory_CS.generatePlanContacts('Admin', planList, 1);
    	insert planAdminList;
    }
}