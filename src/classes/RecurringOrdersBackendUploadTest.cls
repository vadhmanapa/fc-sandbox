@isTest
private class RecurringOrdersBackendUploadTest {
    private static User testUsr;
    private static Order__c testParentOrder;
    
    @isTest static void initz()
    {
        
        //Create test User
        Integer db = (Integer)Math.random()*10000;
        String name = 'testAccessIntegra'+db+'@testAccessIntegra'+db+'.com';
        Profile p = [Select id from Profile where Name ='Integra Standard User'];
        testUsr = new User(LastName='User'+db,
                          FirstName='Testing', 
                          Alias='TUser',
                          Email= name,
                          Username=name,
                          CommunityNickname='testing',
                          EmailEncodingKey='UTF-8',
                          LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US',
                          ProfileId = p.Id,
                          TimeZoneSidKey='America/New_York');
        insert testUsr;
        
        System.runAs(testUsr)
        {
            //create a payer account to add it to Plan patient
            
            Account testAcc = new Account();
            testAcc.Name = 'Test Payer Account';
            testAcc.RecordTypeId =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payer Relations').getRecordTypeId();
            testAcc.Type__c = 'Payer';
            testAcc.Status__c = 'Active';
            insert testAcc;
            
            DME__c testItem = new DME__c();
            testItem.Name = 'L143';
            testItem.Active__c = true;
            testItem.Description__c = 'Test Product';
            testItem.External_Code__c = 'L143';
            testItem.Max_Delivery_Time__c = 48;
            insert testItem;
            
            
            //cretae a test Plan patient needed to create order
            
            Plan_Patient__c tstPat = new Plan_Patient__c();
            tstPat.First_Name__c = 'Superman';
            tstPat.Last_Name__c = 'Batman';
            tstPat.Health_Plan__c = testAcc.Id;
            tstPat.ID_Number__c = '123456';
            tstPat.Street__c = 'Baker Street';
            tstPat.Apartment_Number__c = '221 B';
            tstPat.City__c = 'New York';
            tstPat.State__c = 'NY';
            tstPat.Date_of_Birth__c = Date.today().addYears(-50);
            tstPat.Phone_Number__c = '900-TES-TCALL';
            insert tstPat;
            
            // create order for this patient
            
            testParentOrder = new Order__c();
            testParentOrder.Authorization_Number__c = '666666';
            testParentOrder.Payer__c = testAcc.Id;
            testParentOrder.Plan_Patient__c = tstPat.Id;
            testParentOrder.Authorization_Start_Date__c = Date.newInstance(2016, 09, 01);
            testParentOrder.Authorization_End_Date__c = Date.newInstance(2016, 12, 31);
            testParentOrder.Delivery_Window_Begin__c = Datetime.newInstance(2016, 09, 01, 09, 00, 00);
            testParentOrder.Delivery_Window_End__c = Datetime.newInstance(2016, 09, 04, 09, 00, 00);
            testParentOrder.Recurring__c = true;
            testParentOrder.Recurring_Frequency__c = 'Monthly';
            insert testParentOrder;
            
            DME_Line_Item__c testLine = new DME_Line_Item__c();
            testLine.Order__c = testParentOrder.Id;
            testLine.DME__c = testItem.Id;
            testLine.Recurring_Frequency__c = 'Monthly';
            testLine.Quantity__c = 2;
            insert testLine;
            
        }
    }
    
    @isTest static void RecurringOrdersTestMonthly(){
         initz();
         Order__c testOrder = [SELECT Id, Recurring_Frequency__c, Backend_Uploaded__c FROM Order__c WHERE Id=: testParentOrder.Id LIMIT 1];
         testOrder.Backend_Uploaded__c = true;
        
         Test.startTest();
         update testOrder;
         Test.stopTest();
        
        Order__c parentWithRecurringOrders = [SELECT ID, (SELECT ID, Name FROM Orders__r)FROM Order__c WHERE Id=: testParentOrder.Id];
        
        System.assertNotEquals(null, parentWithRecurringOrders.Orders__r.size());
        System.assertEquals(3, parentWithRecurringOrders.Orders__r.size());
    }
    
     /*@isTest static void RecurringOrdersTestWeekly(){
         initz();
         Order__c testOrder = [SELECT Id, Recurring_Frequency__c, Backend_Uploaded__c FROM Order__c WHERE Id=: testParentOrder.Id LIMIT 1];
         testOrder.Recurring_Frequency__c = 'Weekly';
         testOrder.Backend_Uploaded__c = true;
        
         Test.startTest();
         update testOrder;
         Test.stopTest();
        
        Order__c parentWithRecurringOrders = [SELECT ID, (SELECT ID, Name FROM Orders__r)FROM Order__c WHERE Id=: testParentOrder.Id];
        
        System.assertNotEquals(null, parentWithRecurringOrders.Orders__r.size());
        //System.assertEquals(3, parentWithRecurringOrders.Orders__r.size());
    }*/
    
     /*@isTest static void RecurringOrdersTestDaily(){
         initz();
         Order__c testOrder = [SELECT Id, Recurring_Frequency__c, Backend_Uploaded__c FROM Order__c WHERE Id=: testParentOrder.Id LIMIT 1];
         testOrder.Recurring_Frequency__c = 'Daily';
         testOrder.Backend_Uploaded__c = true;
        
         Test.startTest();
         update testOrder;
         Test.stopTest();
        
        Order__c parentWithRecurringOrders = [SELECT ID, (SELECT ID, Name FROM Orders__r)FROM Order__c WHERE Id=: testParentOrder.Id];
        
        System.assertNotEquals(null, parentWithRecurringOrders.Orders__r.size());
        //System.assertEquals(3, parentWithRecurringOrders.Orders__r.size());
    }*/
}