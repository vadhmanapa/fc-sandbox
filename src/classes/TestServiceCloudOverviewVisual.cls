@isTest
public class TestServiceCloudOverviewVisual {
    static testMethod void testServiceCloudOverview(){
        PageReference pageRef = Page.ServiceCloudOverviewVisual;
        Test.setCurrentPageReference(pageRef);
        
        Integer i;
        List<Call_Log__c> testCalls = new List<Call_Log__c>();
    for (i=0; i<10; i++){
        Call_Log__c cTest = new Call_Log__c();
        cTest.Call_Reason__c = 'Clear';
        cTest.Caller_Type__c = 'Provider';
        cTest.Call_Reason_Detail__c = 'Password change';
        testCalls.add(cTest);
    }
    insert testCalls;
        
        ServiceCloudOverviewCharts testCaseData = new ServiceCloudOverviewCharts();
        String testName = 'test';
        Integer testSize = 1000;
        ServiceCloudOverviewCharts.BarSeriesData cases = new ServiceCloudOverviewCharts.BarSeriesData(testName, testSize);
        List<ServiceCloudOverviewCharts.BarSeriesData> testCaseList = testCaseData.getBarData();
        
        ServiceCloudOverviewCharts testCallsData = new ServiceCloudOverviewCharts();
        String testCallName = 'test';
        Integer testCallSize = 1000;
        ServiceCloudOverviewCharts.PieWedgeData calls = new ServiceCloudOverviewCharts.PieWedgeData(testCallName, testCallSize);
        List<ServiceCloudOverviewCharts.PieWedgeData> testCallsList = testCaseData.getPieData();
        
        ServiceCloudOverviewCharts testEmailsData = new ServiceCloudOverviewCharts();
        String testEmailName = 'test';
        Integer testEmailSize = 1000;
        ServiceCloudOverviewCharts.OpenEmailBarChart emails = new ServiceCloudOverviewCharts.OpenEmailBarChart(testEmailName, testEmailSize);
        List<ServiceCloudOverviewCharts.OpenEmailBarChart> testEmailList = testEmailsData.getCaseData();
    }

}