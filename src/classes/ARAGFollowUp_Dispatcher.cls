public class ARAGFollowUp_Dispatcher {

    public static void doAfterInsert(List<ARAG_Follow_Up__c> nList, Map<Id,ARAG_Follow_Up__c> oldMap){
        updateARAGAfterInsert(nList,oldMap);     
    }
	
	public static void doAfterDelete(List<ARAG_Follow_Up__c> mList, Map<Id,ARAG_Follow_Up__c> oldMap2){
        updateARAGAfterDelete(mList,oldMap2);
    }

    public static void updateARAGAfterInsert(List<ARAG_Follow_Up__c> nList, Map<Id,ARAG_Follow_Up__c> oldMap){
        Set<Id> aragIds = new Set<Id>();

		for(ARAG_Follow_Up__c afu : nList){
			if(afu.Related_CWI__c != null){                   
				aragIds.add(afu.Related_CWI__c);
			}
		}
  
		system.debug('ARAG Id list size:' + aragIds.size());

		Map<Id, ARAG__c> aragFUMap = new Map<Id, ARAG__c> ([
			SELECT 
				Id, 
				Pending_Action__c, 
				Resolution_Action__c,
				CRS_Correction_Type__c, 
				Collector_s_Notes__c, 
				Completed__c,
				Completed_Date__c,
                Corrective_Action_Notes__c,
				Worked_By__c,
				Denial_Reason__c,
				Denial_Notes__c,
				PADR_Action__c,
				Correction_Type__c,
				Denial_Worked_by__c,
				Date_Denial_Worked__c,
				Scrub_Type__c,
				Billing_Issue__c,
				Procedure_Code_s__c,
				Corrective_Action__c,
				Scrubbed_by__c,
				Scrubbed_Date__c
			FROM ARAG__c 
			WHERE Id IN :aragIds
		]);
    
		List<ARAG__c> updatedARAG = new List<ARAG__c>();
		Map<Id, RecordType> rMap = new Map<Id, RecordType>([Select Id, Name from RecordType where sObjectType = 'ARAG_Follow_Up__c']);

		System.debug('Size of the Record Type map'+rMap.size());                               

		for (ARAG_Follow_Up__c afu : nList){

			ARAG__c a = aragFUMap.get(afu.Related_CWI__c);
        
        // modified by Venkat ----> adding record types to the object. For each record type, the values should be updated in the related section.

			if (a != null) {

				// Case a : CRS Follw-Up

				if(rMap.size() > 0){

					if (rMap.get(afu.RecordTypeId).Name == 'CRS Follow-Up'){

				a.Pending_Action__c = afu.Pending_Action__c;                           
				a.Resolution_Action__c = afu.Resolution_Action__c;
				a.CRS_Correction_Type__c = afu.CRS_Correction_Type__c;
				a.Collector_s_Notes__c = afu.Collector_s_Notes__c;
				a.Completed__c = afu.Completed__c;
				a.Completed_Date__c = afu.Completed_Date__c;
				a.Worked_By__c = afu.Worked_By__c;
            
				system.debug('Fields are being updated for: ' + a.Id);
				updatedARAG.add(a);                                                         //add item to the new list
				system.debug('UpdatedARAG List first size:' + updatedARAG.size());

				}

				// Case b: Denial

				if(rMap.get(afu.RecordTypeId).Name == 'Denial'){

					a.Denial_Reason__c = afu.Denial_Reason__c;
					a.Denial_Notes__c = afu.Denial_Notes__c;
					a.PADR_Action__c = afu.PADR_Action__c;
					a.Correction_Type__c = afu.PADR_Correction_Type__c;
					a.Denial_Worked_by__c = afu.Denial_Worked_by__c;
					a.Date_Denial_Worked__c = afu.Denial_Worked_Date_and_Time__c;

				system.debug('Fields are being updated for: ' + a.Id);
				updatedARAG.add(a);                                                         //add item to the new list
				system.debug('UpdatedARAG List first size:' + updatedARAG.size());
				}
				
				// Case c: Scrubbing

				if(rMap.get(afu.RecordTypeId).Name == 'Scrubbing'){

					a.Scrub_Type__c = afu.Scrub_Type__c;
					a.Billing_Issue__c = afu.Billing_Issue1__c;
					a.Procedure_Code_s__c = afu.Procedure_Codes__c;
					a.Corrective_Action__c = afu.Corrective_Action__c;
                    a.Corrective_Action_Notes__c = afu.Corrective_Action_Notes__c;
					a.Scrubbed_by__c = afu.Scrubbed_by__c;
					a.Scrubbed_Date__c = afu.Scrubbed_Date_Time__c;

				system.debug('Fields are being updated for: ' + a.Id);
				updatedARAG.add(a);                                                         //add item to the new list
				system.debug('UpdatedARAG List first size:' + updatedARAG.size());

				}

				}
				
			}
		}
        
		update updatedARAG;
		system.debug('UpdatedARAG List second size:' + updatedARAG.size());
	}
	/************************************* delete trigger*****************************************************/	
	
	public static void updateARAGAfterDelete(List<ARAG_Follow_Up__c> mList, Map<Id,ARAG_Follow_Up__c> oldMap2){
		Set<Id> aragIdsForDeleted = new Set<Id>();
		Map<Id, RecordType> rMap = new Map<Id, RecordType>([Select Id, Name from RecordType where sObjectType = 'ARAG_Follow_Up__c']);

        for (ARAG_Follow_Up__c afu: mList) {
            if (afu.Related_CWI__c != null) {
                aragIdsForDeleted.add(afu.Related_CWI__c);
            }                                                               
        }
    
		system.debug('Size of aragIdsForDeleted: ' + aragIdsForDeleted.size());
	
		Map<Id, ARAG__c> aragsForDeleted = new Map<Id, ARAG__c> ([
			SELECT 
				Id, Name,
				Pending_Action__c, 
				Resolution_Action__c,
				CRS_Correction_Type__c,
                Corrective_Action_Notes__c,
				Collector_s_Notes__c, 
				Completed__c,
				Completed_Date__c,
				Worked_By__c,
				Denial_Reason__c,
				Denial_Notes__c,
				PADR_Action__c,
				Correction_Type__c,
				Denial_Worked_by__c,
				Date_Denial_Worked__c,
				Scrub_Type__c,
				Billing_Issue__c,
				Procedure_Code_s__c,
				Corrective_Action__c,
				Scrubbed_by__c,
				Scrubbed_Date__c,
				(
					SELECT
						Pending_Action__c, 
						Resolution_Action__c,
						CRS_Correction_Type__c, 
						Collector_s_Notes__c,
                        Corrective_Action_Notes__c,
						Completed__c,
						Completed_Date__c,
						Worked_By__c,
						Denial_Reason__c,
						Denial_Notes__c,
						PADR_Action__c,
						PADR_Correction_Type__c,
						Denial_Worked_by__c,
						Date_Denial_Worked__c,
						Scrub_Type__c,
						Billing_Issue1__c,
						Procedure_Codes__c,
						Corrective_Action__c,
						Scrubbed_by__c,
						Scrubbed_Date__c,
						RecordTypeId
					FROM ARAG_Follow_Ups__r
					ORDER BY CreatedDate DESC
				)
			FROM ARAG__c
			WHERE Id IN :aragIdsForDeleted
		]);
	
		List<ARAG__c> ARAGsToUpdate = new List<ARAG__c>();
		// List<ARAG_Follow_Up__c> recordTypeCount = new <ARAG_Follow_Up__c>();
		List<String> recordTypeName = new List<String>();
        Integer count; // a monitor to check number of times the loop is running. Just to prevent re-updating the values
        // Boolean values are used to check whether a old value was found.
        Boolean matchCRS;
        Boolean matchDenial;
        Boolean matchScrub;
    
		for (ARAG_Follow_Up__c afu : mList){
			
			
			ARAG__c arg = aragsForDeleted.get(afu.Related_CWI__c);
			count = 0;
            
			if (arg == null) { 
				system.debug(LoggingLevel.WARN,'The ARAG is null');
				continue;
			}
		
		/************************ deprecated in this version. Implemented in the else part of the code. ****************************/
            /*	if(arg.ARAG_Follow_Ups__r.size() == 0){
				
				system.debug(LoggingLevel.INFO,'ARAG ' + arg.Name + ' has no children');
				
				arg.Pending_Action__c = null;       				                
				arg.Resolution_Action__c = null;    
				arg.CRS_Correction_Type__c = null; 
				arg.Collector_s_Notes__c = null; 
				arg.Completed__c = false; 
				arg.Completed_Date__c = null; 
				arg.Worked_By__c = null; 
			
			} */
         //----------------------------------------------------------------------------------------------------------------------------
           
            if(arg.ARAG_Follow_Ups__r.size() > 0){
			
				// ARAG_Follow_Up__c recentAFU = arg.ARAG_Follow_Ups__r[0];
                matchCRS = false;
                matchDenial = false;
                matchScrub = false;
                Integer matchingRType = 0;
                
                for(ARAG_Follow_Up__c af : arg.ARAG_Follow_Ups__r){

                	if(af.RecordTypeId != null && rMap.get(afu.RecordTypeId).Name == rMap.get(af.RecordTypeId).Name){

                		matchingRType++;
                	}
                	System.debug('Number of matching Record types found'+matchingRType);
                }
                
                if(matchingRType > 0){
                    
                    for(ARAG_Follow_Up__c af : arg.ARAG_Follow_Ups__r){
                    
                    if(af.RecordTypeId != null && rMap.get(afu.RecordTypeId).Name == 'CRS Follow-Up' && rMap.get(afu.RecordTypeId).Name == rMap.get(af.RecordTypeId).Name && count < 1 ){
                        // another record of same record type found. since the order is descending, the records are already ordered
                        
                        arg.Pending_Action__c = af.Pending_Action__c;       				                
                        arg.Resolution_Action__c = af.Resolution_Action__c;              
                        arg.CRS_Correction_Type__c = af.CRS_Correction_Type__c;
                        arg.Collector_s_Notes__c = af.Collector_s_Notes__c;
                        arg.Completed__c = af.Completed__c;
                        arg.Completed_Date__c = af.Completed_Date__c;
                        arg.Worked_By__c = af.Worked_By__c;
                        
                        count++;
                        matchCRS = true;
                        System.debug('Count of CRS updateCRS'+count);
                        System.debug('Found a previous CRS value. Updating the previous value');
                        break;
                    }
                    
                    // Case b : RecordType ==> Denials
                    
                    if(af.RecordTypeId != null && rMap.get(afu.RecordTypeId).Name == 'Denial' && rMap.get(afu.RecordTypeId).Name == rMap.get(af.RecordTypeId).Name && count < 1 ){
                        // another record of same record type found. since the order is descending, the records are already ordered
                        
                        arg.Denial_Reason__c = af.Denial_Reason__c;
                        arg.Denial_Notes__c = af.Denial_Notes__c;
                        arg.PADR_Action__c = af.PADR_Action__c;
                        arg.Correction_Type__c = af.PADR_Correction_Type__c;
                        arg.Denial_Worked_by__c = af.Denial_Worked_by__c;
                        arg.Date_Denial_Worked__c = af.Date_Denial_Worked__c;
                        
                        count++;
                        matchDenial = true;
                        System.debug('Count of CRS updateCRS'+count);
                        System.debug('Found a previous Denial value. Updating the previous value');
                        break;
                    }
                    
                    // Case c : RecordType ==> Scrubbing
                    
                     if(af.RecordTypeId != null && rMap.get(afu.RecordTypeId).Name == 'Scrubbing' && rMap.get(afu.RecordTypeId).Name == rMap.get(af.RecordTypeId).Name && count < 1 ){
                        // another record of same record type found. since the order is descending, the records are already ordered
                        
                        arg.Scrub_Type__c = af.Scrub_Type__c;
                        arg.Billing_Issue__c = af.Billing_Issue1__c;
                        arg.Procedure_Code_s__c = af.Procedure_Codes__c;
                        arg.Corrective_Action__c = af.Corrective_Action__c;
                        arg.Corrective_Action_Notes__c = af.Corrective_Action_Notes__c;
                        arg.Scrubbed_by__c = af.Scrubbed_by__c;
                        arg.Scrubbed_Date__c = af.Scrubbed_Date__c;
                        
                        count++;
                        matchScrub = true;
                        System.debug('Count of CRS updateCRS'+count);
                        System.debug('Found a previous Scrubbing value. Updating the previous value');
                        break;
                    }

                } 
                    
                }else {
                    
                    if(rMap.get(afu.RecordTypeId).Name == 'CRS Follow-Up' ){
                        // another record of same record type found. since the order is descending, the records are already ordered
                        
                        arg.Pending_Action__c = '';       				                
                        arg.Resolution_Action__c = '';              
                        arg.CRS_Correction_Type__c = '';
                        arg.Collector_s_Notes__c = '';
                        arg.Completed__c = false;
                        arg.Completed_Date__c = null;
                        arg.Worked_By__c = null;
                     System.debug('No previous CRS value found. Updating to null');
                    }
                
                if(rMap.get(afu.RecordTypeId).Name == 'Denial' ){
                        // another record of same record type found. since the order is descending, the records are already ordered
                        
                        arg.Denial_Reason__c = '';
                        arg.Denial_Notes__c = '';
                        arg.PADR_Action__c = '';
                        arg.Correction_Type__c = '';
                        arg.Denial_Worked_by__c = null;
                        arg.Date_Denial_Worked__c = null;
                        
                        System.debug('No previous Denial value found. Updating to null');
                    }
                
                if(rMap.get(afu.RecordTypeId).Name == 'Scrubbing' ){
                        // another record of same record type found. since the order is descending, the records are already ordered
                        
                        arg.Scrub_Type__c = '';
                        arg.Billing_Issue__c = '';
                        arg.Procedure_Code_s__c = '';
                        arg.Corrective_Action__c = '';
                        arg.Corrective_Action_Notes__c = '';
                        arg.Scrubbed_by__c = null;
                        arg.Scrubbed_Date__c = null;
                        
                    System.debug('No previous Scrubbing value found. Updating to null');
                    }
                    
                }
				
            } else {
                
                // if no matching record types were found
                
                
                        // another record of same record type found. since the order is descending, the records are already ordered
                        
                        arg.Pending_Action__c = '';       				                
                        arg.Resolution_Action__c = '';              
                        arg.CRS_Correction_Type__c = '';
                        arg.Collector_s_Notes__c = '';
                        arg.Completed__c = false;
                        arg.Completed_Date__c = null;
                        arg.Worked_By__c = null;
                     System.debug('No previous CRS value found. Updating to null');
                   
                        // another record of same record type found. since the order is descending, the records are already ordered
                        
                        arg.Denial_Reason__c = '';
                        arg.Denial_Notes__c = '';
                        arg.PADR_Action__c = '';
                        arg.Correction_Type__c = '';
                        arg.Denial_Worked_by__c = null;
                        arg.Date_Denial_Worked__c = null;
                        
                     System.debug('No previous Denial value found. Updating to null');
                    
                        // another record of same record type found. since the order is descending, the records are already ordered
                        
                        arg.Scrub_Type__c = '';
                        arg.Billing_Issue__c = '';
                        arg.Procedure_Code_s__c = '';
                        arg.Corrective_Action__c = '';
                        arg.Corrective_Action_Notes__c = '';
                        arg.Scrubbed_by__c = null;
                        arg.Scrubbed_Date__c = null;
                        
                    System.debug('No previous Scrubbing value found. Updating to null');
                   
                
            }             
			
				//system.debug('Most recent follow-up: ' + recentAFU);
										
			    ARAGsToUpdate.add(arg);
		}
        
		update ARAGsToUpdate;
	}
}