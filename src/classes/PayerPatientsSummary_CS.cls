public without sharing class PayerPatientsSummary_CS extends PayerPortalServices_CS {

    private final String pageAccountType = 'Payer';
    
    private PatientServices_CS patientServices;

    public Apexpages.Standardsetcontroller patientSetCon {get;set;}
    public String searchStringMessage {get;set;}
    
    public String filterBySearch {get{ return (filterBySearch == null) ? '' : filterBySearch; }set;}
    public Boolean showSearchPanel {get;set;}
    public Boolean showCreatePatients {
		get {
			return portalUser.role.Create_Plan_Patients__c;
		} 
		private set;
	}
    //-------------Added by VK on 10/07/2016------------------------
    public Boolean viewPatientsPermission {
        get {
            return portalUser.role.View_Patients__c;
        } 
        private set;
    }

    public Boolean editUserPermission {
        get {
            return portalUser.role.Edit_Users__c;
        } 
        private set;
    }

    public Boolean editPatientsPermission {
        get {
            return portalUser.role.Edit_Plan_Patients__c;
        } 
        private set;
    }

//-------------------------------------------------------------------
    public List<Plan_Patient__c> patientList
    {
        get{
            return (patientSetCon.getRecords() != null) ? (List<Plan_Patient__c>) patientSetCon.getRecords() : new List<Plan_Patient__c>();
        }
        private set;
    }
    
    /******* Interface *******/
    public Integer pageSize {
        get{
            if(pageSize == null){ pageSize = 10; }
            return pageSize;
        }
        set{
            pageSize = value;           
            if(patientSetCon != null){  patientSetCon.setPageSize(value); }
        }
    }

    public Integer totalPages {
        get{
            return (Integer)Math.ceil((Decimal)patientSetCon.getResultSize() / patientSetCon.getPageSize());
        }
        set;
    }

    /******* Constructor/Init *******/
    public PayerPatientsSummary_CS(){}

    public PageReference init(){
        
        ///////////////////////////////////
        // Authentication and Permission
        ///////////////////////////////////
        
        if(!isSecure && !Test.isRunningTest()) return DoLogout();
                
        if(!authenticated) return DoLogout();

        if(portalUser.AccountType != pageAccountType) return homepage;
        
        ///////////////////////////////////
        // Additional init
        ///////////////////////////////////

        patientServices = new PatientServices_CS(portalUser.instance.Id);
        populatePatientList();
        showSearchPanel = true;
        
        return null;
    }
    
    /******* Search Patient *******/
    private void populatePatientList() {
        try {
            String sortColumn = System.currentPageReference().getParameters().get('sortCol');
            String sortOrder = System.currentPageReference().getParameters().get('sortOrd');
            if (String.isNotEmpty(sortColumn)) sortColumn.trim();

            System.debug('\n\n sortColumn => ' + sortColumn + '\n');
            System.debug('\n\n sortOrder => ' + sortOrder + '\n');

            patientSetCon = patientServices.getPatientsWithSort(filterBySearch, new Map<String, String>{sortColumn => sortOrder});
            if (patientSetCon != null) {
                patientSetCon.setPageSize(pageSize);
            }
        }
        catch (Exception e) {
            searchStringMessage += 'An error occured: ' + e.getMessage();
            System.debug('An error occured: ' + e.getMessage());
        }
    }


    public void searchPatient(){
        
        //Reset the search results message
        searchStringMessage = '';

        System.debug('\n\n filterBySearch => ' + filterBySearch + '\n');
        if(String.isNotBlank(filterBySearch)){
            populatePatientList();

            /*String searchWithWildCards = '%' + filterBySearch + '%';
            Id entity = portalUser.instance.Entity__c;
            
            try{

                populatePatientList();


                String queryString = 'SELECT First_Name__c, Last_Name__c, Phone_Number__c, ID_Number__c, Cell_Phone_Number__c FROM Plan_Patient__c ';
                queryString += 'WHERE (ID_Number__c LIKE :searchWithWildCards OR Last_Name__c LIKE :searchWithWildCards ';
                queryString += 'OR Medicare_Id__c LIKE :searchWithWildCards OR Medicaid_Id__c LIKE :searchWithWildCards) ';
                queryString += 'AND Health_Plan__c = :entity';
                System.debug('Search patient query: ' + queryString);
                patientSetCon = new Apexpages.Standardsetcontroller(Database.getQueryLocator(queryString));
                patientSetCon.setPageSize(pageSize);
                showSearchPanel = false;       
            }
            catch(Exception e){
                searchStringMessage += 'An error occured: ' + e.getMessage();
                System.debug('An error occured: ' + e.getMessage());
            }*/
            showSearchPanel = false;
        }else {
            searchStringMessage += 'Please enter search criteria';
        }
    }
}