public with sharing class CallLogUtils {

	public static void updateCallLogTimestamp(Call_Log__c log, Task logTask) {
		
		if (logTask.skyplatform__Call_Start_Date_Time__c != null) {
			system.debug(LoggingLevel.INFO,'Updating received time: ' + logTask.skyplatform__Call_Start_Date_Time__c);
			log.Call_Received__c = logTask.skyplatform__Call_Start_Date_Time__c;
		}
		
		if (logTask.skyplatform__Call_Start_Date_Time__c != null) {
			
			DateTime endTime = logTask.skyplatform__Call_Start_Date_Time__c.addSeconds(logTask.CallDurationInSeconds);
			
			system.debug(LoggingLevel.INFO,'Updating end time: ' + endTime);
			log.Call_Ended__c = endTime;
		}
	}
}