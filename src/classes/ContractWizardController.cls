public with sharing class ContractWizardController {

	Contract payorContract;
	public String accId {get; set;}

		public ContractWizardController() {
			
			this.payorContract = new Contract();
			if(ApexPages.currentPage().getParameters().get('accId') != null) {
				accountInfo();
			}
		}

		// Controller with 1 args

		public ContractWizardController(ApexPages.StandardController stdCon){

			this.payorContract = (Contract) stdCon.getRecord();
			if(ApexPages.currentPage().getParameters().get('accId')!= null) {
				accountInfo();
			}
		}

		//Controller for Test Method

		public ContractWizardController(Contract testPayorCont){

			this.payorContract = testPayorCont;
			if(ApexPages.currentPage().getParameters().get('accId') != null) {
				accountInfo();
			}
		}

		//This method returns a contract variable. It creates an empty record for the variable

		public Contract getContract(){

			if(payorContract == null) payorContract = new Contract();
			return payorContract;
		}

		public void accountInfo(){

			accId = ApexPages.currentPage().getParameters().get('accId');
            Map<String, Id> rTypeMap = new Map<String, Id>();
			for(RecordType rt: [Select Id, Name from RecordType where sObjectType = 'Contract']) {
				rTypeMap.put(rt.Name, rt.Id);
			}
			// query the health plan values
			List<Account> payorAcc = [Select Id, Name, BillingStreet, BillingCity,BillingState, BillingPostalCode, BillingCountry from Account where Id =:accId];

			//pre-populate the available account values to Contract

			payorContract.AccountId = payorAcc[0].Id;
			payorContract.BillingStreet = payorAcc[0].BillingStreet;
			payorContract.BillingCity = payorAcc[0].BillingCity;
			payorContract.BillingState = payorAcc[0].BillingState;
			payorContract.BillingPostalCode = payorAcc[0].BillingPostalCode;
			payorContract.BillingCountry = payorAcc[0].BillingCountry;
            payorContract.RecordTypeId = rTypeMap.get('Payer Relations');
		}

        public PageReference mainPage(){
            return Page.ContractIntroPage_IP;
        }
    	public PageReference amendments(){

			return Page.ContractStep1_IP;
		}
    	
        public PageReference requirements(){
            return Page.ContractStep2Requirements_IP;
        }
    	
        public PageReference claims(){
                return Page.ContractStep3Claims_IP;
        }
    	public PageReference termination(){
                return Page.ContractStep4Termination_IP;
        }
    	public PageReference miscellaneous(){
                return Page.ContractStep5Miscellaneous_IP;
        }
        public PageReference save(){
            if(payorContract != null){
                payorContract.Status = 'Draft';
                upsert payorContract;
            }
            if( payorContract != null){
                String lid = payorContract.Id;
                return new PageReference('/'+lid);
            } else{
                return new PageReference ('/'+accId);
            }
        }
         public PageReference cancel() {
            return new PageReference('/'+accId);
        }
}