/**
 * Email services are automated processes that use Apex classes
 * to process the contents, headers, and attachments of inbound
 * email.
 */
global class CompaintEmailService_IP implements Messaging.InboundEmailHandler {

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
             String emailBody= '';   
       // String emailSender = email.fromname.substring(0,email.fromname.indexOf(' '));
       // Integer stringResult = emailSender.length();
        String fName;
        String lName;
        emailBody = email.plainTextBody;  
        List<Issue_Log__c> newMail = new List<Issue_Log__c>();
        List<Contact> excon = new List<Contact>();
        String fromAddress = email.fromAddress;
        String[] toAddresses = email.toAddresses;
        String newMailId = '';
        Boolean isSuccess = false;
        /*if(email.fromname != null){
             // fName = email.fromname.substring(0,email.fromname.indexOf(' '));
             lName = email.fromname.substring(email.fromname.indexOf(' ')); 
        } else {
            fName = 'Email';
            lName = 'Contact';
        }*/
               
        String accountId;
        String contactId;
        List <Contact>  con = [Select Id, Email, AccountId from Contact where Email =: fromAddress];
        System.debug ('The number of contacts quried is'+con.size());
                if(con.size() > 0){
            accountId = con[0].AccountId;
            contactId = con[0].Id;
                } else{
        List<Account> a = [Select Id, Name from Account where Name LIKE 'Integra Partners%' LIMIT 1];
        if (a.size() > 0){
        Contact newContact = new Contact();
        newContact.FirstName = 'Email';
        newContact.LastName = 'Contact';
        newContact.AccountId = a[0].Id;
        newContact.Email = fromAddress;
        insert newContact;
        accountId = newContact.AccountId;
        contactId = newContact.Id;
        }
     }
     
            Issue_Log__c addMail = new Issue_Log__c (From_Email_Address__c = fromAddress, Email_Subject__c = email.Subject, Email_Content__c = emailBody, 
                                                           Account__c = accountId, Account_Contact__c = contactId, Message_Id__c = email.messageId, Email__c = true, Issue_Type__c = 'Complaint', Issue_Level__c = 'Normal', Method_of_Complaint__c = 'Email');
            newMail.add(addMail);
            try{
                Database.SaveResult[] srList = Database.insert(newMail, false);
                for (Database.SaveResult dsr : srList) {
                if (dsr.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed
                        System.debug('Successfully inserted email. Email ID: ' + dsr.getId());
                        isSuccess = true;
                        newMailId = dsr.getId();
                } else {
                            // Operation failed, so get all errors                
                            for(Database.Error err : dsr.getErrors()) {
                                    System.debug('The following error has occurred.');                    
                                System.debug(err.getStatusCode() + ': ' + err.getMessage());
                                System.debug('Email Log fields that affected this error: ' + err.getFields());
                            }
                                //insert newMail;   
                                // newMailId = newMail[0].Id;
                                //  System.debug('Email Inserted. Job successful ' + newMail);   
                        }
                } 
            }
        catch (System.Dmlexception e) 
        {
            System.debug('Email not inserted. Job unsuccessful ' + e.getMessage());
        }      
     
        if (isSuccess == true && email.binaryAttachments != null && email.binaryAttachments.size() > 0) {
      for (integer i = 0 ; i < email.binaryAttachments.size() ; i++) {
        Attachment attachment = new Attachment();
        // attach to the newly created email record
        attachment.ParentId = newMailId;
        attachment.Name = email.binaryAttachments[i].filename;
        attachment.Body = email.binaryAttachments[i].body;
        insert attachment;
            }
        }
        result.success = true;
        return result;
    }
}