@isTest
private class NewOrderReassignmentControllerTest {

	static PageReference pageRef;
	static NewOrderReassignmentController pageCon;

    static void init() {
    	
    	Call_Log__c existingCallLog = new Call_Log__c();
    	insert existingCallLog;
    	
    	Order__c existingOrder = new Order__c();
    	insert existingOrder;
    	
    	ApexPages.Standardcontroller stdCon = new ApexPages.Standardcontroller(existingOrder);
    	
		pageRef = Page.NewCallLogLink;
    	pageRef.getParameters().put('logId',existingCallLog.Id);
    	pageRef.getParameters().put('oId',existingOrder.Id);
    	
    	pageCon = new NewOrderReassignmentController(stdCon);
    }
}