@isTest
private class TestTriggerOnCheckDeposit {
	
	@isTest static void testTrigger() {
		// Insert a bunch of Clearinghouse records
		//test for duplicate values
		Clearinghouse__c claim1 = new Clearinghouse__c(Amount__c = 150.06, Payer__c = 'Empire');
		Insert claim1;

		Clearinghouse__c claim2 = new Clearinghouse__c(Amount__c = 150.06, Payer__c = 'Empire');
		insert claim2;

		Clearinghouse__c claim3 = new Clearinghouse__c(Amount__c = 200, Payer__c = 'Aetna');
		insert claim3;


		// test the code with new Check Deposit
		Check_Deposit__c check1 = new Check_Deposit__c(Paid_Amount__c = 150.06, Remitter_Name__c = 'Empire');
		insert check1;

		List<Check_Deposit__c> testCheck = [Select Id, Paid_Amount__c, Remitter_Name__c, Status__c from Check_Deposit__c
											where Paid_Amount__c = 150.06];
		for(Check_Deposit__c c : testCheck){

			System.assertEquals(c.Status__c,'More than one possible match','Status update Checked');
		}

		// test for unique Check Deposit
		Check_Deposit__c check2 = new Check_Deposit__c(Paid_Amount__c = 200, Remitter_Name__c = 'Aetna');
		insert check2;

		List<Check_Deposit__c> testCheck1 = [Select Id, Paid_Amount__c, Remitter_Name__c, Status__c from Check_Deposit__c
											where Paid_Amount__c = 200];
		for(Check_Deposit__c c : testCheck1){

			System.assertEquals(c.Status__c,'Claim File with matching deposit','Status update Checked');
		}

		

		//test for no match

		Check_Deposit__c check4 = new Check_Deposit__c(Paid_Amount__c = 300, Remitter_Name__c = 'Check');
		insert check4;

		List<Check_Deposit__c> testCheck3 = [Select Id, Paid_Amount__c, Remitter_Name__c, Status__c from Check_Deposit__c
											where Paid_Amount__c = 300];
		for(Check_Deposit__c c : testCheck3){

			System.assertEquals(c.Status__c,'Claim file no deposit','Status update Checked');
		}

	}
	

	
}