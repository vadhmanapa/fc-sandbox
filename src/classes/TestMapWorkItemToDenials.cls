/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestMapWorkItemToDenials {

    static testMethod void TestCopyDenials() {
        
        ARAG__c testBill = new ARAG__c(Bill__c = '25956', Provider__c = 'Johnny Depp', Payor_Family_text__c = 'Charles Babbage',
        								Total_Due__c = 10, Status__c = 'Unassigned', Billed_Date__c = System.today(), Visit_Date__c = System.today());
        insert testBill;
        String testId = testBill.Id;
        
        Denials__c testDenial = new Denials__c(Bill__c = '25956', Provider__c = 'Johnny');
        insert testDenial;
        String denyId = testDenial.Id;
        
        testDenial.Work_Denial__c = true;
        update testDenial;
        
        List<Denials__c> checkDeny = [Select Id, Payor__c from Denials__c where Id =: denyId];
        for (Denials__c de : checkDeny){
        System.assertEquals('Charles Babbage', de.Payor__c, 'The denial was mapped and values copied');
    
        }
    }
}