@isTest
private class ProviderCreateNewUser_CS_Test {
	public static Account provider;
	public static Contact user;
	
    static testMethod void ProviderCreateNewUser_CS_Test() {
    	init();
    	Test.setCurrentPage(Page.ProviderCreateNewUser_CS);
    	TestServices_CS.login(user);
    	
    	ProviderCreateNewUser_CS cont = new ProviderCreateNewUser_CS();
    	cont.init();
    	system.assertNotEquals(null, cont.newUser);
    	
    	cont.username = 'testUserName';
    	
    	cont.newUser.Health_Plan_User_Id__c = '008497';
    	cont.newUser.LastName = 'testLast';
    	
    	PageReference pr = cont.createNewUser();
    	
    	System.debug('returned url: ' + pr.getURL());
    	System.debug('expected url: ' + Page.ProviderUsersSummary_CS.getURL());
    	
    	system.assert(pr.getURL() == Page.ProviderUsersSummary_CS.getURL(), 'User not created.');
    	
    	Contact newUser = [
    		SELECT
    			Entity__c,
    			Password__c,
    			Salt__c,
    			Active__c
    		FROM Contact
    		WHERE Username__c = 'testUserName'
    	];
    	
    	system.assertEquals(provider.Id, newUser.Entity__c);
    	system.assertNotEquals(null, newUser.Password__c);
    	system.assertNotEquals(null, newUser.Salt__c);
    	system.assertEquals(true, newUser.Active__c);
    }
    
    static testMethod void ProviderCreateNewUser_CS_initNotAuth(){
    	init();
    	Test.setCurrentPage(Page.ProviderCreateNewUser_CS);
    	
    	ProviderCreateNewUser_CS cont = new ProviderCreateNewUser_CS();
    	cont.init();
    }
    
    static testMethod void ProviderCreateNewUser_CS_initMissmatch(){
    	init();
    	Account plan = TestDataFactory_CS.generatePlans('TestPlan', 1)[0];
    	insert plan;
    	user.Entity__c = plan.Id;
    	update user;
    	Test.setCurrentPage(Page.ProviderCreateNewUser_CS);
    	TestServices_CS.login(user);
    	
    	ProviderCreateNewUser_CS cont = new ProviderCreateNewUser_CS();
    	cont.init();
    }
    /*
    static testMethod void ProviderCreateNewUser_CS_createNewUserFailedInsert() {
    	//createNewUser
    	//needs: nothing
    	init();
    	Test.setCurrentPage(Page.ProviderCreateNewUser_CS);
    	TestServices_CS.login(user);
    	
    	ProviderCreateNewUser_CS cont = new ProviderCreateNewUser_CS();
    	cont.init();
    	cont.createNewUser();
    }
    */
    public static void init(){
    	List<Account> initAccts = TestDataFactory_CS.generateProviders('TestProv', 1);
    	provider = initAccts[0];
    	insert provider;
    	
    	Role__c userRole = new Role__c();
    	userRole.Create_Users__c = true;
    	insert userRole;
    	
    	user = TestDataFactory_CS.createProviderContacts('Admin', initAccts, 1)[0];
    	user.Content_Role__c = userRole.Id;
    	
    	update user;
    }
}