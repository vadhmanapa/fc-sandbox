@isTest
private class LegacyOrderLookupBatchTest_CS {

	static Account provider;
	static Account plan;
	static Plan_Patient__c planPatient;
	private static Provider_Location__c providerLocation;

	//Tests the error message creation auxiallary function LegacyOrderLookupBatchServices_CS.getErrorMessageForLegacyOrder()
	static testMethod void getErrorMessageForLegacyOrder_Test() {

		init();
		List<New_Orders__c> legacyOrders = TestDataFactory_CS.generateDuplicateNewOrders(providerLocation.Id, plan.Id, provider.Id, 'A0000, A0001', '1, 2', 'PatientName', '000', true, 3);
		List<Order__c> orders = TestDataFactory_CS.generateOrders(planPatient.Id, provider.Id, 3);
		String errorMessageExpected = 'Base message: ' + legacyOrders[1].Id + ', ' + legacyOrders[2].Id + ', Potential matching order(s): ' + orders[0].Id + ', ' + orders[1].Id + ', ' + orders[2].Id;
		String errorMessageActual = LegacyOrderLookupBatchServices_CS.getErrorMessageForLegacyOrder(legacyOrders, orders, 'Base message');
		System.assertEquals(errorMessageExpected, errorMessageActual);
	}

	//Tests the LegacyOrderLookupBatch_CS
	static testMethod void batchTest() {

		//Setup test data
		init();

		//Insert legacy orders -> system will insert orders by trigger
		List<New_Orders__c> allLegacyOrders = new List<New_Orders__c>();
		List<Order__c> orders = new List<Order__c>(); //All orders not created by trigger

		//Case 1: 1 Legacy Order -> 1 Order - Set lookup on that order
		List<New_Orders__c> legacyOrdersCase1 = TestDataFactory_CS.generateDuplicateNewOrders(providerLocation.Id, plan.Id, provider.Id, 'A0000, A0001', '1, 2', 'PatientName1', '001', true, 1);
		allLegacyOrders.addAll(legacyOrdersCase1);
		//List<Order__c> ordersCase1 = TestDataFactory_CS.generateOrders(planPatient.Id, provider.Id, 1);

		//Case 2: N Legacy Orders -> N Orders 
		//Case 2a: K = N-1 orders cancelled
		//Case 2a(i): HCPCs/units match - Set lookup on active order from Legacy Order w/ HCPC info
		List<New_Orders__c> legacyOrdersCase2ai = TestDataFactory_CS.generateDuplicateNewOrders(providerLocation.Id, plan.Id, provider.Id, 'A0000, A0001', '1, 2', 'PatientName2', '002', true, 3);
		allLegacyOrders.addAll(legacyOrdersCase2ai);

		//Case 2a(ii)_1: HCPCs do not match - Error "HCPCs or units do not match"
		List<New_Orders__c> legacyOrdersCase2aii_1 = TestDataFactory_CS.generateDuplicateNewOrders(providerLocation.Id, plan.Id, provider.Id, 'A0000, A0001', '1, 2', 'PatientName3', '003', true, 2);
		legacyOrdersCase2aii_1.addAll(TestDataFactory_CS.generateDuplicateNewOrders(providerLocation.Id, plan.Id, provider.Id, 'A0000, A0007', '1, 2', 'PatientName3', '003', true, 1));
		allLegacyOrders.addAll(legacyOrdersCase2aii_1);

		//Case 2a(ii)_2: Units do not match - Error "HCPCs or units do not match"
		List<New_Orders__c> legacyOrdersCase2aii_2 = TestDataFactory_CS.generateDuplicateNewOrders(providerLocation.Id, plan.Id, provider.Id, 'A0000, A0001', '1, 2', 'PatientName4', '004', true, 2);
		legacyOrdersCase2aii_2.addAll(TestDataFactory_CS.generateDuplicateNewOrders(providerLocation.Id, plan.Id, provider.Id, 'A0000, A0001', '1, 3', 'PatientName4', '004', true, 1));
		allLegacyOrders.addAll(legacyOrdersCase2aii_2);

		//Case 2b_1: K != N-1 orders cancelled (2 active) - Error "Multiple valid orders"
		List<New_Orders__c> legacyOrdersCase2b_1 = TestDataFactory_CS.generateDuplicateNewOrders(providerLocation.Id, plan.Id, provider.Id, 'A0000, A0001', '1, 2', 'PatientName5', '005', true, 3);
		allLegacyOrders.addAll(legacyOrdersCase2b_1);

		//Case 2b_2: K != N-1 orders cancelled (0 active) - Error "Multiple valid orders"
		List<New_Orders__c> legacyOrdersCase2b_2 = TestDataFactory_CS.generateDuplicateNewOrders(providerLocation.Id, plan.Id, provider.Id, 'A0000, A0001', '1, 2', 'PatientName6', '006', true, 3);
		allLegacyOrders.addAll(legacyOrdersCase2b_2);

		//Insert legacy orders and create map Id -> object
		insert allLegacyOrders;
		Map<Id, New_Orders__c> legacyOrdersMap = new Map<Id, New_Orders__c>(allLegacyOrders);

		System.debug(legacyOrdersCase1);
		orders.addAll(TestDataFactory_CS.generateOrdersWithLegacyOrders(planPatient.Id, provider.Id, legacyOrdersCase1));
		orders.addAll(TestDataFactory_CS.generateOrdersWithLegacyOrders(planPatient.Id, provider.Id, legacyOrdersCase2ai));
		orders.addAll(TestDataFactory_CS.generateOrdersWithLegacyOrders(planPatient.Id, provider.Id, legacyOrdersCase2aii_1));
		orders.addAll(TestDataFactory_CS.generateOrdersWithLegacyOrders(planPatient.Id, provider.Id, legacyOrdersCase2aii_2));
		orders.addAll(TestDataFactory_CS.generateOrdersWithLegacyOrders(planPatient.Id, provider.Id, legacyOrdersCase2b_1));
		orders.addAll(TestDataFactory_CS.generateOrdersWithLegacyOrders(planPatient.Id, provider.Id, legacyOrdersCase2b_2));
		System.debug('Orders: ' + orders);
		insert orders;

		//Query for orders
		List<Order__c> queriedOrdersPreBatch = [
				SELECT Status__c, Legacy_Orders__c, CreatedDate, Authorization_Number__c, Patient_Name__c
				FROM Order__c
		];
		System.debug('Queried orders pre-batch: ' + queriedOrdersPreBatch);

		//Map legacy Id -> list of orders, set status to cancelled on correct orders, delete the lookup, then update
		Map<Id, List<Order__c>> legacyIdToOrdersList = new Map<Id, List<Order__c>>();
		for (Order__c o : queriedOrdersPreBatch) {
			if (!legacyIdToOrdersList.containsKey(o.Legacy_Orders__c)) {
				legacyIdToOrdersList.put(o.Legacy_Orders__c, new List<Order__c>());
			}
			legacyIdToOrdersList.get(o.Legacy_Orders__c).add(o);
			Id noId = o.Legacy_Orders__c;
			if (noId == legacyOrdersCase2ai[1].Id || noId == legacyOrdersCase2ai[2].Id || noId == legacyOrdersCase2aii_1[1].Id || noId == legacyOrdersCase2aii_1[2].Id ||
					noId == legacyOrdersCase2aii_2[1].Id || noId == legacyOrdersCase2aii_2[2].Id || noId == legacyOrdersCase2b_1[2].Id || noId == legacyOrdersCase2b_2[0].Id ||
					noId == legacyOrdersCase2b_2[1].Id || noId == legacyOrdersCase2b_2[2].Id) {
				o.Status__c = 'Cancelled';
			}
			o.Patient_Name__c = legacyOrdersMap.get(o.Legacy_Orders__c).Patient_Name__c;
			o.Authorization_Number__c = legacyOrdersMap.get(o.Legacy_Orders__c).Plan_Order__c;
			o.Order_Image__c = 'https://shreddr.captricity.com';
			o.Legacy_Orders__c = null;
		}
		update queriedOrdersPreBatch;
		for (Order__c o : queriedOrdersPreBatch) {
			System.debug('Updated queried order pre-batch: ' + o);
		}

		//Run batch
		Test.startTest();
		LegacyOrderLookupBatch_CS batch = new LegacyOrderLookupBatch_CS();
		Database.executeBatch(batch);
		Test.stopTest();

		//Query orders and assert that the correct lookups were set
		Map<Id, Order__c> queriedOrdersPostBatch = new Map<Id, Order__c>([
				SELECT Legacy_Orders__c
				FROM Order__c
		]);
		System.debug(legacyOrdersCase1[0]);
		System.debug(legacyIdToOrdersList.get(legacyOrdersCase1[0].Id));
		System.debug(queriedOrdersPostBatch.get(legacyIdToOrdersList.get(legacyOrdersCase1[0].Id)[0].Id));
		System.assertEquals(legacyOrdersCase1[0].Id, queriedOrdersPostBatch.get(legacyIdToOrdersList.get(legacyOrdersCase1[0].Id)[0].Id).Legacy_Orders__c);
		System.assertEquals(legacyOrdersCase2ai[0].Id, queriedOrdersPostBatch.get(legacyIdToOrdersList.get(legacyOrdersCase2ai[0].Id)[0].Id).Legacy_Orders__c);
		System.assertEquals(null, queriedOrdersPostBatch.get(legacyIdToOrdersList.get(legacyOrdersCase2ai[1].Id)[0].Id).Legacy_Orders__c);
		System.assertEquals(null, queriedOrdersPostBatch.get(legacyIdToOrdersList.get(legacyOrdersCase2ai[2].Id)[0].Id).Legacy_Orders__c);
		System.assertEquals(null, queriedOrdersPostBatch.get(legacyIdToOrdersList.get(legacyOrdersCase2aii_1[0].Id)[0].Id).Legacy_Orders__c);
		System.assertEquals(null, queriedOrdersPostBatch.get(legacyIdToOrdersList.get(legacyOrdersCase2aii_1[1].Id)[0].Id).Legacy_Orders__c);
		System.assertEquals(null, queriedOrdersPostBatch.get(legacyIdToOrdersList.get(legacyOrdersCase2aii_1[2].Id)[0].Id).Legacy_Orders__c);
		System.assertEquals(null, queriedOrdersPostBatch.get(legacyIdToOrdersList.get(legacyOrdersCase2aii_2[0].Id)[0].Id).Legacy_Orders__c);
		System.assertEquals(null, queriedOrdersPostBatch.get(legacyIdToOrdersList.get(legacyOrdersCase2aii_2[1].Id)[0].Id).Legacy_Orders__c);
		System.assertEquals(null, queriedOrdersPostBatch.get(legacyIdToOrdersList.get(legacyOrdersCase2aii_2[2].Id)[0].Id).Legacy_Orders__c);
		System.assertEquals(null, queriedOrdersPostBatch.get(legacyIdToOrdersList.get(legacyOrdersCase2b_1[0].Id)[0].Id).Legacy_Orders__c);
		System.assertEquals(null, queriedOrdersPostBatch.get(legacyIdToOrdersList.get(legacyOrdersCase2b_1[1].Id)[0].Id).Legacy_Orders__c);
		System.assertEquals(null, queriedOrdersPostBatch.get(legacyIdToOrdersList.get(legacyOrdersCase2b_1[2].Id)[0].Id).Legacy_Orders__c);
		System.assertEquals(null, queriedOrdersPostBatch.get(legacyIdToOrdersList.get(legacyOrdersCase2b_2[0].Id)[0].Id).Legacy_Orders__c);
		System.assertEquals(null, queriedOrdersPostBatch.get(legacyIdToOrdersList.get(legacyOrdersCase2b_2[1].Id)[0].Id).Legacy_Orders__c);
		System.assertEquals(null, queriedOrdersPostBatch.get(legacyIdToOrdersList.get(legacyOrdersCase2b_2[2].Id)[0].Id).Legacy_Orders__c);

		//Query legacy orders and assert that error messages are correct
		Map<Id, New_Orders__c> queriedLegacyOrders = new Map<Id, New_Orders__c>([
				SELECT Lookup_Reset_Failed__c, Lookup_Reset_Failed_Reason__c
				FROM New_Orders__c
		]);
		System.assertEquals(false, queriedLegacyOrders.get(legacyOrdersCase1[0].Id).Lookup_Reset_Failed__c);
		System.assertEquals(false, queriedLegacyOrders.get(legacyOrdersCase2ai[0].Id).Lookup_Reset_Failed__c);

		System.assertEquals(true, queriedLegacyOrders.get(legacyOrdersCase2aii_1[0].Id).Lookup_Reset_Failed__c);
		String failureMessage = 'HCPCs or units do not match potential duplicate legacy orders with Ids: ' + legacyOrdersCase2aii_1[1].Id + ', ' + legacyOrdersCase2aii_1[2].Id + ', Potential matching order(s): ';
		failureMessage += queriedOrdersPostBatch.get(legacyIdToOrdersList.get(legacyOrdersCase2aii_1[0].Id)[0].Id).Id;
		System.assertEquals(failureMessage, queriedLegacyOrders.get(legacyOrdersCase2aii_1[0].Id).Lookup_Reset_Failed_Reason__c);

		System.assertEquals(true, queriedLegacyOrders.get(legacyOrdersCase2aii_2[0].Id).Lookup_Reset_Failed__c);
		failureMessage = 'HCPCs or units do not match potential duplicate legacy orders with Ids: ' + legacyOrdersCase2aii_2[1].Id + ', ' + legacyOrdersCase2aii_2[2].Id + ', Potential matching order(s): ';
		failureMessage += queriedOrdersPostBatch.get(legacyIdToOrdersList.get(legacyOrdersCase2aii_2[0].Id)[0].Id).Id;
		System.assertEquals(failureMessage, queriedLegacyOrders.get(legacyOrdersCase2aii_2[0].Id).Lookup_Reset_Failed_Reason__c);

		System.assertEquals(true, queriedLegacyOrders.get(legacyOrdersCase2b_1[0].Id).Lookup_Reset_Failed__c);
		failureMessage = 'Multiple valid orders for this legacy order and legacy orders with Ids: ' + legacyOrdersCase2b_1[1].Id + ', ' + legacyOrdersCase2b_1[2].Id + ', Potential matching order(s): ' ;
		failureMessage += queriedOrdersPostBatch.get(legacyIdToOrdersList.get(legacyOrdersCase2b_1[0].Id)[0].Id).Id + ', ' + queriedOrdersPostBatch.get(legacyIdToOrdersList.get(legacyOrdersCase2b_1[1].Id)[0].Id).Id;
		System.assertEquals(failureMessage, queriedLegacyOrders.get(legacyOrdersCase2b_1[0].Id).Lookup_Reset_Failed_Reason__c);

		System.assertEquals(true, queriedLegacyOrders.get(legacyOrdersCase2b_2[0].Id).Lookup_Reset_Failed__c);
		failureMessage = 'Multiple valid orders for this legacy order and legacy orders with Ids: ' + legacyOrdersCase2b_2[1].Id + ', ' + legacyOrdersCase2b_2[2].Id + ', Potential matching order(s): ';
		failureMessage += queriedOrdersPostBatch.get(legacyIdToOrdersList.get(legacyOrdersCase2b_2[0].Id)[0].Id).Id + ', ' + queriedOrdersPostBatch.get(legacyIdToOrdersList.get(legacyOrdersCase2b_2[1].Id)[0].Id).Id + ', ';
		failureMessage += queriedOrdersPostBatch.get(legacyIdToOrdersList.get(legacyOrdersCase2b_2[2].Id)[0].Id).Id;
		System.assertEquals(failureMessage, queriedLegacyOrders.get(legacyOrdersCase2b_2[0].Id).Lookup_Reset_Failed_Reason__c);
	}

	private static void init() {

		provider =  TestDataFactory_CS.generateProviders('TestProvider', 1)[0];
		insert provider;
		plan = TestDataFactory_CS.generatePlans('TestPlan', 1)[0];
		insert plan;

		Patient__c patient = TestDataFactory_CS.generatePatients('LastName', 1)[0];
		Map<Id, List<Patient__c>> planToPatient = new Map<Id, List<Patient__c>>{
				plan.Id => new List<Patient__c>{patient}
		};
		planPatient = TestDataFactory_CS.generatePlanPatients(planToPatient)[0];

		providerLocation = TestDataFactory_CS.generateProviderLocations(provider.Id, 1)[0];
		insert providerLocation;
	}
}