public with sharing class NewCallLogLinkController {

	public class ExistingRelation {
		public Call_Log_Relation__c relation {get;set;}
		
		public Boolean selected {get;set;}
		
		public existingRelation(Call_Log_Relation__c relation) {
			this.relation = relation;
		}
	}

	public class SearchResult {
		public SObject record {get;set;}
		
		public String name {get;set;}
		public String details {get;set;}
		
		public Boolean selected {get;set;}
		
		public SearchResult(SObject record) {
			
			this.record = record;
			
			//Check type
			if(record.Id.getSObjectType() == Account.SObjectType) {
				this.name = ((Account)record).Name;
			} else if(record.Id.getSObjectType() == Contact.SObjectType) {
				this.name = ((Contact)record).Name;
			} else if(record.Id.getSObjectType() == Claims_Inquiry__c.SObjectType) {
				this.name = ((Claims_Inquiry__c)record).Name;
			} else if(record.Id.getSObjectType() == Order__c.SObjectType) {
				this.name = ((Order__c)record).Name;
			} else {
				this.name = 'Unsupported object';
				this.details = 'Unsupported object';
			}
		}
		
		public SearchResult(Call_Log_Relation__c rel) {
			
		}
		
		public Call_Log_Relation__c getRelation() {
			Call_Log_Relation__c newRelation = new Call_Log_Relation__c();
			
			//Check type
			if(record.Id.getSObjectType() == Account.SObjectType) {
				newRelation.Account__c = record.Id;
			} else if(record.Id.getSObjectType() == Contact.SObjectType) {
				newRelation.Contact__c = record.Id;
			} else if(record.Id.getSObjectType() == Claims_Inquiry__c.SObjectType) {
				newRelation.Claims_Inquiry__c = record.Id;
			} else if(record.Id.getSObjectType() == Order__c.SObjectType) {
				newRelation.Order__c = record.Id;
			} else {
				return null;
			}
			
			return newRelation;
		}
		
	}

	public Map<String,String> pageParams = ApexPages.currentPage().getParameters();

	public Call_Log__c parentCallLog {get;set;}

	public List<ExistingRelation> existingRelations {get;set;}

	public String searchSObjectType {get;set;}
	public String searchText {get;set;}
	public List<SearchResult> searchResults {get;set;}

	public NewCallLogLinkController(ApexPages.StandardController stdCon) {
		
		//Note: The standard controller here is for a Call_Log_Relation and is not used
		
		reset();
	}
	
	public void reset() {
		
		searchResults = new List<SearchResult>();
		existingRelations = new List<ExistingRelation>();
		
		if(!pageParams.containsKey('callLogId')) {
			PageUtils.addError('No Call Log Id found in URL!');
			return;
		}
		
		try{

			parentCallLog = [
				SELECT Id,Name,
				(
					SELECT 
						Id,
						Name,
						SObject_Link__c,
						SObject_Type__c,
						Account__c,
						Contact__c,
						Claims_Inquiry__c,
						Order__c
					FROM Call_Log_Relations__r
				)
				FROM Call_Log__c
				WHERE Id = :pageParams.get('callLogId')
			];
			
			existingRelations = new List<ExistingRelation>();
			
			for(Call_Log_Relation__c rel : parentCallLog.Call_Log_Relations__r) {
				existingRelations.add(new ExistingRelation(rel));
			}
			
		} catch(Exception e) {
			PageUtils.addError(e.getMessage());
			return;
		}
	}
	
	public void search() {
		
		String query = '';
		
		query += 'SELECT Id,Name';
		query += ' FROM ' + searchSObjectType;
		
		if(String.isNotBlank(searchText)) {
			query += ' WHERE Name LIKE \'%' + searchText + '%\'';
		}
		
		system.debug('Query: ' + query);
		
		searchResults = new List<SearchResult>();
		
		for(SObject o : Database.query(query)) {
			searchResults.add(new SearchResult(o));
		}
		
		system.debug(searchResults);	
	}

	public void link() {
		
		List<Call_Log_Relation__c> newRelations = new List<Call_Log_Relation__c>();
		
		for(SearchResult sr : searchResults) {
			if(sr.selected) {
				Call_Log_Relation__c newRelation = sr.getRelation();
				
				newRelation.Call_Log__c = parentCallLog.Id;
				
				newRelations.add(newRelation);
			}
		}
		
		insert newRelations;
		
		reset();
	}
	
	public void unlink() {
		
		List<Call_Log_Relation__c> toDelete = new List<Call_Log_Relation__c>();
		
		for(ExistingRelation er : existingRelations) {
			if(er.selected) {
				toDelete.add(er.relation);
			}
		}
		
		delete toDelete;
		
		reset();
	}
	
	public PageReference returnToPage() {
		return new PageReference('/' + parentCallLog.Id);
	}
}