public with sharing class NewCallLogController {

	Call_Log__c newCallLog {get;set;}

	public NewCallLogController( ApexPages.StandardController stdCon ){
		newCallLog = (Call_Log__c)stdCon.getRecord();
	}
	
	public PageReference init(){
		
		try{
			insert newCallLog;
			
			PageReference ref = Page.EditCallLog;
			ref.getParameters().put('Id',newCallLog.Id);
			
			//Pass along any parameters
			Map<String, String> pageParams = ApexPages.currentPage().getParameters();
			for (String key : pageParams.keySet()){
				ref.getParameters().put(key, pageParams.get(key));
			}
			
			return ref;
		}
		catch( Exception err ){
			System.debug(LoggingLevel.ERROR,err.getMessage());
			PageUtils.addError(err.getMessage());
			System.debug('Exception: ' + err.getMessage());
		}
		
		return null;
	}
}