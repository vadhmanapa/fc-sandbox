/**
 *  @Description 
 *  @Author Cloud Software LLC
 *  Revision History: 
 *		12/23/2015 - karnold - Added header, removed references to DME_Category__c
 *
 */
@isTest
private class DMEWrapper_CS_Test {
	
	private static DME_Settings__c dmeSettings;
	private static List<DME__c> testDMEs;
	private static List<DMEWrapper_CS> wrappers;
	private static Integer counter = 1;
	
	static testMethod void DMEWrapper_Test(){
		init();
		for (DME__c d: testDMEs){
			System.debug(counter + ' times through loop');
			wrappers.add(new DMEWrapper_CS(d));
			counter++;
		}
		wrappers.add(new DMEWrapper_CS()); 
	}
	
	private static void init(){
		dmeSettings = TestDataFactory_CS.generateDMESettings();
		insert dmeSettings;
		
		testDMEs = TestDataFactory_CS.generateDMEs(3);
		insert testDMEs;
		
		wrappers = new List<DMEWrapper_CS>();
	}
}