public without sharing class OrderJSONFactory_CS {
	
	/******* Order Notes/Messages *******/
	public static List<OrderMessage_CS> returnOrderMessages (String JSONMessage){
		system.debug('### returnOrderMessages JSONMessage: ' + JSONMessage);

		return (List<OrderMessage_CS>) JSON.deserialize(JSONMessage, List<OrderMessage_CS>.class);
	}
	
	//Not batch compatible. Used to process single orders and their messages. 
	//Depends on extended order query that includes related contacts and contact names
	//TODO: REWRITE WITH JSON TOOLS!!!!! 
	public static void processOrderMessage(Contact fromContact, Order__c thisOrder){
		System.debug('### Case_Manager__r.Name: ' + thisOrder.Case_Manager__r.Name);
		
		Map<Id, String> contactIdToNameMap = new Map<Id, String>();
		
		//Select o.Entered_By__c, o.Case_Manager__c, o.Accepted_By__c From Order__c o
		
		if(thisOrder.Entered_By__c != null) contactIdToNameMap.put(thisOrder.Entered_By__c, thisOrder.Entered_By__r.Name);
		if(thisOrder.Case_Manager__c != null) contactIdToNameMap.put(thisOrder.Case_Manager__c, thisOrder.Case_Manager__r.Name);
		if(thisOrder.Accepted_By__c != null) contactIdToNameMap.put(thisOrder.Accepted_By__c, thisOrder.Accepted_By__r.Name);
		
		//Select contact that matches the passed in "from" contact and remove from map
		String fromName;
		if(contactIdToNameMap.get(fromContact.Id) != null){
			fromName = contactIdToNameMap.remove(fromContact.Id);
		}else{
			fromName = fromContact.Name;
		}
		System.debug('### contactIdToNameMap: ' + contactIdToNameMap);
		
		//Select remaining names as the "to" contacts
		List<String> toNames = contactIdToNameMap.values();
		System.debug('### toNames: ' + toNames);
		//List<String> toNames, String fromName, String message, Boolean urgent, String orderNumber, Id orderId
		OrderMessage_CS newMessage = new OrderMessage_CS(toNames, fromName, thisOrder.Message_Body__c, thisOrder.Message_is_Urgent__c, thisOrder.Name, thisOrder.Id);
		List<OrderMessage_CS> messageListWrapper = new List<OrderMessage_CS>();
		messageListWrapper.add(newMessage);
		String jsonString = JSON.serializePretty(messageListWrapper);
		
		//Prepend to existing messages
		if(thisOrder.Message_History_JSON__c == null ){
			thisOrder.Message_History_JSON__c = jsonString;
		}else{
			thisOrder.Message_History_JSON__c = jsonString.replace(']', ',') + thisOrder.Message_History_JSON__c.replace('[', '');
		}
		//Add logic to make drop oldest message if message history is too long
		while(thisOrder.Message_History_JSON__c.length() > 32768){
			thisOrder.Message_History_JSON__c = removeLastJSON(thisOrder.Message_History_JSON__c);
		}
		System.debug('### thisOrder.Message_History_JSON__c: ' + thisOrder.Message_History_JSON__c);
		thisOrder.Message_Pending__c = true;
		
	}
	
	private static String removeLastJSON(String JSONString){
		return JSONString.substringBeforeLast('{') + ']';
	}	
}