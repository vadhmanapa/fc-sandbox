/**
*  @Description    Test Class for ClearAlgorithmSettingSerivces
*  @Author Cloud Software LLC
*  Revision History: 
*      11/18/2015 - karnold - ICA-299 - overhauled class to refelct new changes in ClearAlgorithmSettingSerivces 
*
*/
@isTest
public class ClearAlgorithmSettingsTest {
	private static List<User> testUsers;
	private static List<Clear_Algorithm_Settings__c> testClearAlgorithmSettings;

	/* Clear Algorithm Settings Sharing Tests */

	private static void init() {
		testUsers = TestDataFactory_CS.generateUsers('ExecutiveManagement',1);
		testUsers.add(TestDataFactory_CS.generateUsers('Claims_User',1)[0]);
		insert testUsers;

		testClearAlgorithmSettings = TestDataFactory_CS.generateClearAlgorithmSettings(testUsers[0], 1);
		testClearAlgorithmSettings.add(TestDataFactory_CS.generateClearAlgorithmSettings(testUsers[1], 1)[0]);
		insert testClearAlgorithmSettings;
	}

	static testMethod void addReadPermissionsForApproved() {
		init();
		System.RunAs(testUsers[1]){
			testClearAlgorithmSettings[1].Status__c = ClearAlgorithmSettingsServices.STATUS_APPROVED;
			update testClearAlgorithmSettings[1];
		}

		System.Test.startTest();
		List<Clear_Algorithm_Settings__Share> casShares = [SELECT Id FROM Clear_Algorithm_Settings__Share WHERE UserOrGroupId=:testUsers[0].Id];
		System.Test.stopTest();

		System.assertEquals(1, casShares.size());
	}

	/* Clear Algorithm Settings Services Tests */

	/* Test Init Methods */

	private static Clear_Algorithm_Settings__c initServicesTestsOneRecord() {
		User testUser = TestDataFactory_CS.generateUsers('Claims_User',1)[0];
		insert testUser;

		return TestDataFactory_CS.generateClearAlgorithmSettings(false, 'Draft');
	}

	/* Tests */
	private static testMethod void blockActivationForNewRecord() {
		// GIVEN
		Clear_Algorithm_Settings__c cas = initServicesTestsOneRecord();
		cas.Active__c = true;

		// WHEN
		Test.startTest();
		try {
			insert cas;
			System.assert(false);
		} catch (DMLException e) {
			Test.stopTest();
			// THEN
			System.assert(e.getMessage().contains('Only approved records can be activated.'));
		}
	}

	private static testMethod void blockNonDraftStatusForNewRecord() {
		// GIVEN
		Clear_Algorithm_Settings__c cas = initServicesTestsOneRecord();
		cas.Status__c = 'Approved';

		// WHEN
		Test.startTest();
		try {
			insert cas;
			System.assert(false);
		} catch (DMLException e) {
			Test.stopTest();
			// THEN
			System.assert(e.getMessage().contains('New records can only have a status of "Draft".'));
		}
	}

	private static testMethod void blockDateFieldsForNewRecords_Activated() {
		// GIVEN
		Clear_Algorithm_Settings__c cas = initServicesTestsOneRecord();
		cas.Activated_On__c = Date.today();

		// WHEN
		Test.startTest();
		try {
			insert cas;
			System.assert(false);
		} catch (DMLException e) {
			Test.stopTest();
			// THEN
			System.assert(e.getMessage().contains('New records cannot have an activated date.'));
		}
	}

	private static testMethod void blockDateFieldsForNewRecords_Approved() {
		// GIVEN
		Clear_Algorithm_Settings__c cas = initServicesTestsOneRecord();
		cas.Approved_On__c = Date.today();

		// WHEN
		Test.startTest();
		try {
			insert cas;
			System.assert(false);
		} catch (DMLException e) {
			Test.stopTest();
			// THEN
			System.assert(e.getMessage().contains('New records cannot have an approved date.'));
		}
	}

	private static testMethod void blockDateFieldsForNewRecords_Deactivated() {
		// GIVEN
		Clear_Algorithm_Settings__c cas = initServicesTestsOneRecord();
		cas.Deactivated_On__c = Date.today();

		// WHEN
		Test.startTest();
		try {
			insert cas;
			System.assert(false);
		} catch (DMLException e) {
			Test.stopTest();
			// THEN
			System.assert(e.getMessage().contains('New records cannot have an deactivated date.'));
		}
	}

	private static testMethod void blockActivationIfNotApproved_Negative() {
		// GIVEN
		Clear_Algorithm_Settings__c cas = initServicesTestsOneRecord();
		insert cas;
		cas.Active__c = true;

		// WHEN
		Test.startTest();
		try {
			update cas;
			System.assert(false);
		} catch (DMLException e) {
			Test.stopTest();
			// THEN
			System.assert(e.getMessage().contains('Only approved records can be activated.'));
		}
	}

	private static testMethod void blockActivationIfNotApproved_Positive() {
		// GIVEN
		Clear_Algorithm_Settings__c cas = initServicesTestsOneRecord();
		insert cas;
		TestDataFactory_CS.runSubmitAlgorithmSettingsForApprovalProcess_Test(cas);
		cas = [
			SELECT
				Active__c,
				Status__c
			FROM Clear_Algorithm_Settings__c
			LIMIT 1
		];
		cas.Active__c = true;

		// WHEN
		Test.startTest();
			update cas;
		Test.stopTest();

		// THEN
		Clear_Algorithm_Settings__c newCas = [
			SELECT
				Active__c
			FROM Clear_Algorithm_Settings__c
			LIMIT 1
		];

		System.assert(newCas.Active__c);
	}

	private static testMethod void approvalProcess_SetApprovedDate() {
		// GIVEN
		Clear_Algorithm_Settings__c cas = initServicesTestsOneRecord();
		insert cas;

		// WHEN
		Test.startTest();
		TestDataFactory_CS.runSubmitAlgorithmSettingsForApprovalProcess_Test(cas);
		Test.stopTest();

		// THEN
		Clear_Algorithm_Settings__c newCas = [
			SELECT
				Approved_On__c,
				Status__c
			FROM Clear_Algorithm_Settings__c
			LIMIT 1
		];

		System.assertNotEquals(null, newCas.Approved_On__c.date());
		System.assertEquals(ClearAlgorithmSettingsServices.STATUS_APPROVED, newCas.Status__c);
	}

	private static testMethod void blockDateFieldUpdates_NegativeActivated() {
		// GIVEN
		Clear_Algorithm_Settings__c cas = initServicesTestsOneRecord();
		insert cas;
		cas.Activated_On__c = Date.today();

		// WHEN
		Test.startTest();
		try {
			update cas;
			System.assert(false);
		} catch (DMLException e) {
			Test.stopTest();
			// THEN
			System.assert(e.getMessage().contains('The activated on date cannot be changed.'));
		}
	}

	private static testMethod void blockDateFieldUpdates_NegativeDeactivated() {
		// GIVEN
		Clear_Algorithm_Settings__c cas = initServicesTestsOneRecord();
		insert cas;
		cas.Deactivated_On__c = Date.today();

		// WHEN
		Test.startTest();
		try {
			update cas;
			System.assert(false);
		} catch (DMLException e) {
			Test.stopTest();
			// THEN
			System.assert(e.getMessage().contains('The deactivated on date cannot be changed.'));
		}
	}

	private static testMethod void blockDateFieldUpdates_NegativeApproved() {
		// GIVEN
		Clear_Algorithm_Settings__c cas = initServicesTestsOneRecord();
		insert cas;
		cas.Approved_On__c = Date.today();

		// WHEN
		Test.startTest();
		try {
			update cas;
			System.assert(false);
		} catch (DMLException e) {
			Test.stopTest();
			// THEN
			System.assert(e.getMessage().contains('The approved on date cannot be changed.'));
		}
	}

	private static testMethod void blockEditForApproved() {
		// GIVEN
		Clear_Algorithm_Settings__c cas = initServicesTestsOneRecord();
		insert cas;
		TestDataFactory_CS.runSubmitAlgorithmSettingsForApprovalProcess_Test(cas);
		cas = [
			SELECT
				Active__c,
				Existing_Provider_Max__c
			FROM Clear_Algorithm_Settings__c
			LIMIT 1
		];

		cas.Active__c = true;
		cas.Existing_Provider_Max__c = cas.Existing_Provider_Max__c + 1;

		// WHEN
		Test.startTest();
		try {
			update cas;
			System.assert(false);
		} catch (DMLException e) {
			Test.stopTest();
			// THEN
			System.assert(e.getMessage().contains(
					'Cannot edit an approved record except to activate it.'), e.getMessage());
		}
		
	}

	private static testMethod void blockActivationForFormerlyActiveRecord() {
		// GIVEN
		Clear_Algorithm_Settings__c cas = initServicesTestsOneRecord();
		insert cas;
		System.debug('Approving CAS ' + cas.Id);
		TestDataFactory_CS.runSubmitAlgorithmSettingsForApprovalProcess_Test(cas);
		cas = [
			SELECT
				Active__c
			FROM Clear_Algorithm_Settings__c
			LIMIT 1
		];
		System.debug('Activating CAS ' + cas.Id);
		cas.Active__c = true;
		update cas;

		// Second CAS to deactivate the first record
		Clear_Algorithm_Settings__c newActiveCas = initServicesTestsOneRecord();
		System.debug('Inserting new Active CAS ' + newActiveCas.Id);
		insert newActiveCas;
		System.debug('Approving new Active CAS ' + newActiveCas.Id);
		TestDataFactory_CS.runSubmitAlgorithmSettingsForApprovalProcess_Test(newActiveCas);
		newActiveCas = [
			SELECT
				Active__c
			FROM Clear_Algorithm_Settings__c
			WHERE Id = :newActiveCas.Id
			LIMIT 1
		];
		System.debug('Activating new Active CAS ' + newActiveCas.Id);
		newActiveCas.Active__c = true;
		update newActiveCas;

		// Activating original record
		cas.Active__c = true;

		// WHEN
		Test.startTest();
		try {
			update cas;
			System.assert(false);
		} catch (DMLException e) {
			Test.stopTest();
			// THEN
			System.assert(e.getMessage().contains(
				'Records that used to be active must be cloned before they can be re-activated.'));
		}
	}

	private static testMethod void setActivationDateOnNewActiveRecord() {
		// GIVEN
		Clear_Algorithm_Settings__c cas = initServicesTestsOneRecord();
		insert cas;
		TestDataFactory_CS.runSubmitAlgorithmSettingsForApprovalProcess_Test(cas);
		cas = [
			SELECT
				Active__c
			FROM Clear_Algorithm_Settings__c
			LIMIT 1
		];
		cas.Active__c = true;

		// WHEN
		Test.startTest();
		update cas;
		Test.stopTest();

		// THEN
		Clear_Algorithm_Settings__c newCas = [
			SELECT
				Active__c,
				Activated_On__c
			FROM Clear_Algorithm_Settings__c
			LIMIT 1
		];

		System.assertNotEquals(null, newCas.Activated_On__c.date());
		System.assert(newCas.Active__c);
	}

	private static testMethod void setDeactivatedDateOnDeactivation() {
		// GIVEN
		Clear_Algorithm_Settings__c cas = initServicesTestsOneRecord();
		insert cas;
		TestDataFactory_CS.runSubmitAlgorithmSettingsForApprovalProcess_Test(cas);
		cas = [
			SELECT
				Active__c
			FROM Clear_Algorithm_Settings__c
			LIMIT 1
		];
		cas.Active__c = true;
		update cas;

		// insert second record to deactivate the original record
		Clear_Algorithm_Settings__c newActiveCas = initServicesTestsOneRecord();
		insert newActiveCas;
		TestDataFactory_CS.runSubmitAlgorithmSettingsForApprovalProcess_Test(newActiveCas);
		newActiveCas = [
			SELECT
				Active__c
			FROM Clear_Algorithm_Settings__c
			WHERE Id = :newActiveCas.Id
			LIMIT 1
		];
		newActiveCas.Active__c = true;

		// WHEN
		Test.startTest();
		update newActiveCas;	// Activating the new record (and deactivating original)
		Test.stopTest();

		// THEN
		Clear_Algorithm_Settings__c newCas = [
			SELECT
				Active__c,
				Deactivated_On__c
			FROM Clear_Algorithm_Settings__c
			WHERE Id = : cas.Id
			LIMIT 1
		];

		System.assertNotEquals(null, newCas.Deactivated_On__c.date());
		System.assert(!newCas.Active__c);
	}
	
}