global class OrdersUpdateToReacceptSchedule implements Schedulable {
	public static final String jobName = 'Update Future to Pending Schedule Process';
	public static final String jobSchedule = '0 0 3 ? * MON-FRI';

	public static void updateSchedule() {

		String name = (Test.isRunningTest() ? jobName + ' Test' : jobName);
		
		String jobId = System.schedule(
						name, 
		        		jobSchedule, 
		        		new OrdersUpdateToReacceptSchedule()
	        			);

		System.debug('jobId: ' + jobId);
	}

	global void execute(SchedulableContext SC) {
     OrdersUpdateToReacceptanceBatch ofp = new OrdersUpdateToReacceptanceBatch();
     Database.executeBatch(ofp, 200);
   }
}