public without sharing class PayerUserSummary_CS extends PayerPortalServices_CS {

	private final String pageAccountType = 'Payer';

	/******* Page Permissions *******/
	public Boolean resetPasswordPermission {get{ return portalUser.role.Reset_Password__c; }}

	/******* Page Interface *******/
	public UserModel_CS thisUser {get;set;}
	public Boolean editable {get;set;}
	
	public boolean displayPhoneError {get;set;}
	public boolean displayEmailError {get;set;}
	
	/******* Error Messages *******/
	public String usernameErrorMsg {get;set;}
	public final String nonUniqueUsernameError = 'Username already exists';
	public final String missingUsernameError = 'Please enter a unique username';
	
	/******* New User Info *******/
	public String firstName {get;set;}
	public String lastName {get;set;}
	public String phone {get;set;}
	public String email {get;set;}
	public String username {get;set;}
	
	/*
	//Populates the user data with the temp data
	public Boolean updateAllInfo(){
		
		Boolean success = true;
		displayPhoneError = false; displayEmailError = false;
		
		if (!updatePhone()){
			displayPhoneError = true;
			success = false;
		}
		
		if (!updateEmail()){
			displayEmailError = true;
			success = false;
		}
		
		if (success){
			thisUser.instance.FirstName = tempFirstName;
			thisUser.instance.LastName = tempLastName;
		}
		
		return success;
		
	}
	
	//Checks if the phone number entered is valid, and if so, updates the user's phone number
	public Boolean updatePhone() {
		
		Pattern phone = Pattern.compile('\\(?\\d{3}\\)?\\s?\\-?\\s?\\d{3}\\s?\\-?\\s?\\d{4}');
		System.debug('Pattern compiled');
		Pattern noPhone = Pattern.compile('');
		Matcher phoneMatcher = phone.matcher(tempPhone);
		Matcher noPhoneMatcher = noPhone.matcher(tempPhone);
		
		if (phoneMatcher.matches() || noPhoneMatcher.matches()){
			thisUser.instance.Phone = tempPhone;
			return true;
		}
		
		else{
			return false;
		}
	}
	
	//Checks if the email address entered is valid, and if so, updates the user's email address
	public Boolean updateEmail(){
		Pattern email = Pattern.compile('.*\\@.*\\..*');
		Pattern noEmail = Pattern.compile('');
		Matcher emailMatcher = email.Matcher(tempEmail);
		Matcher noEmailMatcher = noEmail.Matcher(tempEmail);
		
		if (emailMatcher.matches() || noEmailMatcher.matches()){
			thisUser.instance.Email = tempEmail;
			return true;
		}
		
		else{
			return false;
		}
	}
	*/
	
	/******* Constructor/Init *******/
	public PayerUserSummary_CS(){}
	
	public PageReference init(){
		
		///////////////////////////////////
		// Authentication and Permission
		///////////////////////////////////
		
		if(!isSecure && !Test.isRunningTest()){
			return DoLogout();
		}
				
		if(!authenticated){
			return DoLogout();
		}

		if(portalUser.AccountType != pageAccountType){
			return homepage;
		}
		
		///////////////////////////////////
		// Additional init
		///////////////////////////////////

		// If the user ID was passed, get it and create the usermodel
		if(ApexPages.currentPage().getParameters().containsKey('uId')){
			String uId = ApexPages.currentPage().getParameters().get('uId');
			
			UserServices_CS userServices = new UserServices_CS(portalUser.Id);
			
			Contact thisUserContact = userServices.getUserById(uId);			
			thisUser = new UserModel_CS(thisUserContact);
			
			firstName = thisUser.instance.FirstName;
			lastName = thisUser.instance.LastName;
			phone = thisUser.instance.Phone;
			email = thisUser.instance.Email;
			username = thisUser.instance.Username__c;		
		}
		else{
			System.debug('PayerUserSummary Error: No user id parameter found.');
			return Page.PayerUsersSummary_CS;
		}
		
		return null;
	}
		
	/********* Page Buttons/Actions *********/
		
	//Updates the system with the information the user enters
	public void saveUser(){
		
		//Clear the error message
		usernameErrorMsg = '';
		
		//Check for blank username
		if(String.isBlank(username)){
			usernameErrorMsg = missingUsernameError;
			System.debug('No username entered.');
			return;
		}
		
		//Check if username is unique
		if(!thisUser.checkUniqueUsername(username)){
			usernameErrorMsg = nonUniqueUsernameError;
			System.debug('Username: ' + username + ' is not unique.');
			return;
		}

		thisUser.instance.FirstName = firstName;		
		thisUser.instance.LastName = lastName;
		thisUser.instance.Phone = phone;
		thisUser.instance.Email = email;
		thisUser.instance.Username__c = username;
		
		System.debug('New user info: ' + thisUser.instance);
		
		update thisUser.instance;
	}
	
	public void sendUsername(){
		
		if(thisUser != null){
			if(thisUser.sendUsernameTo()){
				System.debug('Successfully sent username for ' + thisUser.instance.Name + 
					' to ' + thisUser.instance.Email);
			}
			else{
				System.debug('Error: Failed to send username for ' + thisUser.instance.Name + 
					' to ' + thisUser.instance.Email);
			}
		}
	}
	
	public void resetPassword(){
		
		if(thisUser != null){
			String newPassword = PasswordServices_CS.randomPassword(8);
			
			if(thisUser.changePassword(newPassword)){
				thisUser.sendPasswordTo(newPassword);
				System.debug('Successfully changed password for ' + thisUser.instance.Name);
			}
			else{
				//TODO: Error for failed change password
				System.debug('Error: Failed to change password for ' + thisUser.instance.Name);
			}
		}
	}
	
	public void deactivateUser(){
		
		if(thisUser != null){
			thisUser.deactivate();
		}		
	}
}