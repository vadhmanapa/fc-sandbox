/**
 *  @Description Batch to transfer Provider Location objects from the old object to the new Provider_Location__c object.
 *		To be run after Provider_Location__c deploy.
 *  @Author Cloud Software LLC
 *  Revision History: 
 *		12-22-2015 - karnold - Added comments, changed call to generic
 *
 */
public class RefundAndRecoupmentBatch_CS implements Database.Batchable<SObject> {
	
	String queryString;

	public RefundAndRecoupmentBatch_CS(String query) {
		this.queryString = query;
	}

	public RefundAndRecoupmentBatch_CS() {

		SoqlBuilder query = new SoqlBuilder()
			.selectx('Id')
			.selectx('Provider_Location__c')
			.fromx('Recoupment__c')
			.wherex(new FieldCondition('Provider_Location_New__c').equals(null));
			
		queryString = query.toSoql();

	}

	public Database.QueryLocator start(Database.BatchableContext context) {
		
		return Database.getQueryLocator(queryString);
	}

	public void execute(Database.BatchableContext context, List<Recoupment__c> scope) {
		System.debug(scope);
		List<Recoupment__c> recList = (List<Recoupment__c>) ProviderLocationTransferServices.updateObjectProviderLocation(scope);
		update recList;
	}

	public void finish(Database.BatchableContext context) {

	}
}