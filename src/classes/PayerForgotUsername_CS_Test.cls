/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PayerForgotUsername_CS_Test {
	
	/******* Test Parameters *******/
	static private final Integer N_USERS = 15;

	/******* Payer *******/
	static private List<Account> payers;
	static private List<Contact> payerUsers;

	/******* Test Objects *******/
	static private PayerForgotUsername_CS forgotUsername;
	
	/******* Test Methods *******/
	static testMethod void PayerForgotUsername_Test_PageLanding(){
		init();
	}
	
	static testMethod void PayerForgotUsername_Test_RequestUsername(){
		init();
		
		forgotUsername.userEmail = payerUsers[0].Email;
		
		//TODO: How to validate?
		forgotUsername.requestUsername();
	}
	
	static void init(){
    	
    	///////////////////////////////////
		// Data
		///////////////////////////////////
    	
    	//Payers
		payers = TestDataFactory_CS.generatePlans('TestPlan', 1);
		insert payers;
		
		//Payer users
		payerUsers = TestDataFactory_CS.createPlanContacts('PlanUser', payers, N_USERS);

		///////////////////////////////////
		// Page Landing
		///////////////////////////////////

		//Set the page and any parameters
		Test.setCurrentPage(Page.PayerForgotUsername_CS.setRedirect(true));
		
		//Log the desired user in
		TestServices_CS.login(payerUsers[0]);
    	
    	//Land on the page
    	forgotUsername = new PayerForgotUsername_CS();
    }
}