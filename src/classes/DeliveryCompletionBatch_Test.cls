/*===============================================================================================
 * Cloud Software LLC
 * Name: DeliveryCompletionBatch
 * Description: Test Class for DeliveryCompletionBatch
 * Created Date: 10/01/2015
 * Created By: Andrew Nash (Cloud Software)
 * 
 * 	Date Modified			Modified By			Description of the update
 *	OCT 01, 2015			Andrew Nash			Created
 ==============================================================================================*/
@isTest
public class DeliveryCompletionBatch_Test {
	
	static Account testProvider;
	static Account testPlan;
	static Plan_Patient__c testPlanPatient;
	static Patient__c testPatient;
	static List<Order__c> testOrderList;
	static List<Order_Assignment__c> testOrderAssignmentList;

	//===========================================================================================
	//================================= TEST METHOD FOR BATCH ===================================
	//===========================================================================================
	static testMethod void batchTest() {
		
		// ASSUME
		// Create the needed test data by calling the init method
		init();

		// WHEN
		Test.startTest();
		DeliveryCompletionBatch batch = new DeliveryCompletionBatch();
		Database.executeBatch(batch);
		Test.stopTest();

		Set<Id> testOrderAssignmentIds = new Set<Id>();
		for(Order_Assignment__c oa : testOrderAssignmentList) {
			testOrderAssignmentIds.add(oa.Id);
		}

		List<Order_Assignment__c> batchProcessedOAs = [
			SELECT 
				Delivery_Completion_Time__c 
			FROM Order_Assignment__c 
			WHERE Id IN :testOrderAssignmentIds
		];

		//THEN
		for(Order_Assignment__c oa : batchProcessedOAs) {
			System.assertNotEquals(null, oa.Delivery_Completion_Time__c, 'TEST ERROR: Failed to set Order Assignment Deliver Completion Time.');
		}
	}
	//===========================================================================================
	//============================== INITIALIZE NEEDED TEST DATA ================================
	//===========================================================================================
	private static void init() {
		
		// Create the test provider account data
		testProvider =  TestDataFactory_CS.generateProviders('TestProvider', 1)[0];
		insert testProvider;

		// Create the test plan data
		testPlan = TestDataFactory_CS.generatePlans('TestPlan', 1)[0];
		insert testPlan;

		// Create the patient data
		testPatient = TestDataFactory_CS.generatePatients('LastName', 1)[0];
		insert testPatient;

		// Create a map to store a list of plans to patients
		Map<Id, List<Patient__c>> planToPatient = new Map<Id, List<Patient__c>>{
			testPlan.Id => new List<Patient__c>{testPatient}
		};

		// Create the plan patient data
		testPlanPatient = TestDataFactory_CS.generatePlanPatients(planToPatient)[0];
		insert testPlanPatient;

		// Create the order data
		
		testOrderList = TestDataFactory_CS.generateOrders(testPlanPatient.Id, testProvider.Id, 4);
		for(Order__c o : testOrderList) {
			o.Accepted_Date__c = Datetime.now();
			o.Delivery_Date_Time__c = o.Accepted_Date__c.addDays(3);
		}
		insert testOrderList;

		// Create the order assignment data
		testOrderAssignmentList = new List<Order_Assignment__c>();
		for(Order__c o : testOrderList) {
			testOrderAssignmentList.add(TestDataFactory_CS.generateOrderAssignments(o.Id, testProvider.Id, o.Accepted_Date__c.addHours(-1), null));
		}
		insert testOrderAssignmentList;
	}
	
}