@isTest
private class CaseTriggerDispatcherTest {

    private static Case testCase;
    private static Account testPay;
    private static Contact testProvider;
    private static User u;

    private static TestCaseDataFactory_IP testData;

    @isTest static void testCaseFromClearHelp(){
        //generateTestData();
        testData = new TestCaseDataFactory_IP();
        testData.initTestData();
        Case testCase = new Case();
        System.runAs(TestCaseDataFactory_IP.csUser){
            testCase.Origin = 'ClearHelp Email';
            testCase.SuppliedEmail = 'test@test.com';
            testCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('ClearHelp Email').getRecordTypeId();
            testCase.Subject = 'Test Email';
            testCase.Description = 'Test Body';
            Test.startTest();
            insert testCase;
            Test.stopTest();
            
        }
        
        Case testResult = [SELECT Id, AccountId, ContactId FROM Case WHERE Id=: testCase.Id LIMIT 1];
        System.assertNotEquals(null, testResult.AccountId);
        System.assertNotEquals(null, testResult.ContactId);
    }
    
    @isTest static void testCaseFromClaimsEmail(){
        //generateTestData();
        testData = new TestCaseDataFactory_IP();
        testData.initTestData();
        Case testCase = new Case();
        System.runAs(TestCaseDataFactory_IP.csUser){
            testCase.Origin = 'Claims Email';
            testCase.SuppliedEmail = 'test@test.com';
            testCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Claims Email').getRecordTypeId();
            testCase.Subject = 'Test Email';
            testCase.Description = 'Test Body';
            Test.startTest();
            insert testCase;
            Test.stopTest();
            
        }
        
        Case testResult = [SELECT Id, AccountId, ContactId, Intake__c FROM Case WHERE Id=: testCase.Id LIMIT 1];
        System.assertNotEquals(null, testResult.AccountId);
        System.assertNotEquals(null, testResult.ContactId);
        System.assertNotEquals(null, testResult.Intake__c);
    }
    
    @isTest static void testCaseFromRollUpAfterInsert(){
        //generateTestData();
        testData = new TestCaseDataFactory_IP();
        testData.initTestData();
        Case testCase = new Case();
        testCase.Origin = 'Claims Email';
        testCase.SuppliedEmail = 'test@test.com';
        testCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Claims Email').getRecordTypeId();
        testCase.Subject = 'Test Email';
        testCase.Description = 'Test Body';
        insert testCase;
        
        Case childCase = new Case();
        
        System.runAs(TestCaseDataFactory_IP.csUser){
            childCase.Origin = 'Claims Email';
            childCase.SuppliedEmail = 'test@test.com';
            childCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Claims Email').getRecordTypeId();
            childCase.Subject = 'Test Email';
            childCase.Description = 'Test Body';
            childCase.ParentId = testCase.Id;
            Test.startTest();
            insert childCase;
            Test.stopTest();
            
        }
        
        Case testResult = [SELECT Id, AccountId, ContactId, Intake__c, NumberofClaims__c FROM Case WHERE Id=: testCase.Id LIMIT 1];
        System.assertEquals(1, testResult.NumberofClaims__c);
    }
    
    @isTest static void testCaseFromRollUpAfterDelete(){
        //generateTestData();
        testData = new TestCaseDataFactory_IP();
        testData.initTestData();
        Case testCase = new Case();
        testCase.Origin = 'Claims Email';
        testCase.SuppliedEmail = 'test@test.com';
        testCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Claims Email').getRecordTypeId();
        testCase.Subject = 'Test Email';
        testCase.Description = 'Test Body';
        insert testCase;
        
        Case childCase = new Case();
        childCase.Origin = 'Claims Email';
        childCase.SuppliedEmail = 'test@test.com';
        childCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Claims Email').getRecordTypeId();
        childCase.Subject = 'Test Email';
        childCase.Description = 'Test Body';
        childCase.ParentId = testCase.Id;
        insert childCase;
        
        System.runAs(TestCaseDataFactory_IP.csUser){
            
            Test.startTest();
            delete childCase;
            Test.stopTest();
            
        }
        
        Case testResult = [SELECT Id, AccountId, ContactId, Intake__c, NumberofClaims__c FROM Case WHERE Id=: testCase.Id LIMIT 1];
        System.assertEquals(0, testResult.NumberofClaims__c);
    }
    
    @isTest static void testCaseFromClaimsEmailNoContact(){
        //generateTestData();
        testData = new TestCaseDataFactory_IP();
        testData.initTestData();
        Case testCase = new Case();
        System.runAs(TestCaseDataFactory_IP.csUser){
            testCase.Origin = 'Claims Email';
            testCase.SuppliedEmail = 'buhaha@test.com';
            testCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Claims Email').getRecordTypeId();
            testCase.Subject = 'Test Email';
            testCase.Description = 'Test Body';
            Test.startTest();
            insert testCase;
            Test.stopTest();
            
        }
        
        Case testResult = [SELECT Id, AccountId, ContactId, Intake__c, NonCredContact__c FROM Case WHERE Id=: testCase.Id LIMIT 1];
        System.assertEquals(null, testResult.AccountId);
        System.assertEquals(null, testResult.ContactId);
        System.assertNotEquals(null, testResult.Intake__c);
        System.assertEquals(true, testResult.NonCredContact__c);
    }

    @isTest static void testCATToAccount() {
		
		Profile p = [Select id from Profile where Name ='Credentialing Team'];
		User u = new User(LastName='User', 
						  Alias='use',
						  Email='testuser@test.com',
						  Username='testingtriggeruser@testingcenter.com',
						  CommunityNickname='testing',
						  EmailEncodingKey='UTF-8',
						  LanguageLocaleKey='en_US',
						  LocaleSidKey='en_US',
						  ProfileId = p.Id,
						  TimeZoneSidKey='America/New_York');
		insert u;

		Round_Robin__c rr = new Round_Robin__c(User__c = u.Id,
									Function__c = 'Credentialing CATs',
									Active__c = True);
		insert rr;

		Cred_Trigger_CS__c CTCS = new Cred_Trigger_CS__c();
		CTCS.Active__c = True;
		insert CTCS;
		
		System.runAs(u){
			// create a test account
			Map<String, Id> rTypeMap = new Map<String, Id>();
			for(RecordType rt: [Select Id, Name from RecordType where sObjectType = 'CAT__c']) {
				rTypeMap.put(rt.Name, rt.Id);
			}


			Account testAcc = new Account(Name='Test Account', 
						        Type_Of_Provider__c = 'DME');
			insert testAcc;
            
            Contact testCon = new Contact(AccountId = testAcc.Id, FirstName = 'Test', LastName = 'Audit', Email = 'test@test.com');
            insert testCon;

            CAT__c iniCred = new CAT__c(Account__c = testAcc.Id, Application_Completed_Date__c = System.today(), New_Application_Received__c = System.today(),
										Credentialing_Status__c = 'In triage', RecordTypeId = rTypeMap.get('Credentialing Application'), Application_Type__c = 'Initial Credentialing',
										Missing_Documentation__c = 'Accreditation');
            insert iniCred;

		  	Attachment attachment = new Attachment();
	  		attachment.Body = Blob.valueOf('aaaa');
			attachment.Name = String.valueOf('test.txt');
		    attachment.ParentId = iniCred.Id; 
			insert attachment;

			iniCred.Credentialing_Status__c = '1st document submission form sent';
			iniCred.Provider_Contact__c = testCon.Id;
			update iniCred;

            Case c = [Select Id from Case where CAT__c =: iniCred.Id];

            System.assertNotEquals(null, c.id, 'Case not created');

		}
	}

	@isTest static void generateTestData(){

        Profile p = [Select id from Profile where Name ='Credentialing Team'];
        u = new User(LastName='User', 
                          Alias='use',
                          Email='testuser@test.com',
                          Username='testingtriggeruser@testingcenter.com',
                          CommunityNickname='testing',
                          EmailEncodingKey='UTF-8',
                          LanguageLocaleKey='en_US',
                          LocaleSidKey='en_US',
                          ProfileId = p.Id,
                          TimeZoneSidKey='America/New_York');
        insert u;

		testPay = new Account();
        testPay.Name = 'This is test payor';
        testPay.Type__c = 'Payer';
        testPay.Status__c = 'Active';
        insert testPay;
        
        testProvider = new Contact();
        testProvider.FirstName = 'Pro';
        testProvider.LastName = 'I am Provider';
        testProvider.RecordTypeId = RecordTypes.providerContactId;
        testProvider.AccountId = testPay.Id;
        testProvider.Birthdate = System.today();
        testProvider.Health_Plan_ID__c = '90920-9-029-9';
        testProvider.Email = 'test@test.com';
        testProvider.Active__c = true;
        testProvider.Status__c = 'Active';
        insert testProvider;

        Contact testProvider1 = new Contact();
        testProvider1.FirstName = 'Pro 89';
        testProvider1.LastName = 'I am Provider';
        testProvider1.RecordTypeId = RecordTypes.providerContactId;
        testProvider1.AccountId = testPay.Id;
        testProvider1.Birthdate = System.today();
        testProvider1.Health_Plan_ID__c = '90920-9-029-9';
        testProvider1.Email = 'test@test.com';
        testProvider1.Active__c = true;
        testProvider1.Status__c = 'Active';
        insert testProvider1;
	}
}