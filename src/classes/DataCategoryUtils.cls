public with sharing class DataCategoryUtils {

	public static List<Schema.DataCategory> getDataCategoriesFromGroup(String sobjectName, String groupName) {
		List<Schema.DataCategory> output = new List<Schema.DataCategory>();
		
		List <DataCategoryGroupSobjectTypePair> pairs = new List<DataCategoryGroupSobjectTypePair>();
		
		DataCategoryGroupSobjectTypePair pair1 = new DataCategoryGroupSobjectTypePair();
		pair1.setSobject(sobjectName);
		pair1.setDataCategoryGroupName(groupName);
		
		pairs.add(pair1);
		
		for(Schema.DescribeDataCategoryGroupStructureResult result : 
		    Schema.describeDataCategoryGroupStructures(pairs,false))
		{
		 	List<Schema.DataCategory> topCategories = result.getTopCategories();
		 	
		 	//There is a top category called "All" that we want to avoid
		 	if(topCategories.size() == 1 && topCategories[0].getName() == 'All') {
		 		output.addAll(topCategories[0].getChildCategories());
		 	} else {		 	
			    output.addAll(topCategories);
		 	}
		}
		
		return output;
	}

	public static Map<String,Schema.DataCategory> getAllDataCategoriesFromGroup(String sobjectName, String groupName) {
		Map<String,Schema.DataCategory> output = new Map<String,Schema.DataCategory>();
		
		List <DataCategoryGroupSobjectTypePair> pairs = new List<DataCategoryGroupSobjectTypePair>();
		
		DataCategoryGroupSobjectTypePair pair1 = new DataCategoryGroupSobjectTypePair();
		pair1.setSobject(sobjectName);
		pair1.setDataCategoryGroupName(groupName);
		
		pairs.add(pair1);
		
		for(Schema.DescribeDataCategoryGroupStructureResult result : 
		    Schema.describeDataCategoryGroupStructures(pairs,false))
		{
		    //Add to map
		    for(Schema.DataCategory cat : getAllSubCategories(result.getTopCategories())) {
		    	output.put(cat.getName(),cat);
		    }
		}
		
		return output;
	}

	//Recursive function to extract data categories
	public static List<Schema.DataCategory> getAllSubCategories(List<Schema.DataCategory> categories) {
		List<Schema.DataCategory> output = new List<Schema.DataCategory>(categories);
		
		for(Schema.DataCategory cat : categories) {
			//Recursive call
			output.addAll( getAllSubCategories( cat.getChildCategories() ) );
		}

		return output;
	} 
}