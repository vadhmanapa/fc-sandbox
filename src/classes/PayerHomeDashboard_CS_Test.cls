/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PayerHomeDashboard_CS_Test {
	
	/******* Test Parameters *******/
	static final Integer N_ORDERS = 6;

	/******* Provider *******/
	static private List<Account> providers;
	
	/******* Payer *******/
	static private List<Account> payers;
	static private List<Contact> payerUsers;
	
	/******* Patients *******/
	static private List<Patient__c> patients;
	static private List<Plan_Patient__c> planPatients;
	
	/******* Orders *******/
	static private List<Order__c> orders;
	
	/******* Test Objects *******/
	public static PayerHomeDashboard_CS dashboard;
	
	/******* Test Methods *******/	
	
    static testMethod void PayerHomeDashboard_Test_PageLanding() {	
		init();
    }
    
    static testMethod void PayerHomeDashboard_Test_LookbackTime_ThisWeek(){
    	init();
    	
    	test.startTest();
		
		dashboard.lookbackTime = 'thisweek';
		dashboard.updateOrderCounts();
		
		test.stopTest();
    }	
    
    static testMethod void PayerHomeDashboard_Test_LookbackTime_LastWeek(){
    	init();
    	
    	test.startTest();
		
		dashboard.lookbackTime = 'lastweek';
		dashboard.updateOrderCounts();
		
		test.stopTest();
    }
    
    static testMethod void PayerHomeDashboard_Test_LookbackTime_ThisMonth(){
    	init();
		
		test.startTest();
		
		dashboard.lookbackTime = 'thismonth';
		dashboard.updateOrderCounts();
		
		test.stopTest();
    }
    
    static testMethod void PayerHomeDashboard_Test_LookbackTime_LastMonth(){
    	init();
    	
    	test.StartTest();
		
		dashboard.lookbackTime = 'lastmonth';
		dashboard.updateOrderCounts();
		
		test.stopTest();
	}
	
	static testMethod void PayerHomeDashboard_Test_updateOrderCounts(){
		init();
		
		dashboard.updateOrderCounts();
		
		System.AssertEquals('1', dashboard.completedCountString);
		System.AssertEquals('1', dashboard.pendingAcceptCountString);
		System.AssertEquals('1', dashboard.inProgressCountString);
		System.AssertEquals('1', dashboard.needsAttentionCountString);
		System.AssertEquals('1', dashboard.cancelledCountString);
		System.AssertEquals('1', dashboard.recurringCountString);
	}
	
	static testMethod void PayerHomeDashboard_Test_GetLookbackTimes(){
		init();
		List<SelectOption> options = dashboard.lookbackTimes;
	}
	
	public static void init() {		
    	
    	///////////////////////////////////
		// Data
		///////////////////////////////////
    	
    	//Providers
		providers = TestDataFactory_CS.generateProviders('TestProvider', 1);
		insert providers;
		
		//Payers
		payers = TestDataFactory_CS.generatePlans('TestPayer', 1);
		insert payers;
		
		//Payer users
		payerUsers = TestDataFactory_CS.createPlanContacts('PayerUser', payers, 1);
		
		//Patients
		patients = TestDataFactory_CS.generatePatients('TestPatient', 1);
		insert patients;
		
		//Plan patients
		planPatients = TestDataFactory_CS.generatePlanPatients(
			new Map<Id,List<Patient__c>>{
				payers[0].Id => new List<Patient__c>{patients[0]}
			}
		);
		insert planPatients;
		
		//Orders, set status to 'New' yielding stage 'Pending Acceptance'
		orders = TestDataFactory_CS.generateOrders(planPatients[0].Id, providers[0].Id, N_ORDERS);
		orders[0].Status__c = OrderModel_CS.STATUS_DELIVERED;
		orders[1].Status__c = 'Needs Attention - Delivery Information Incomplete';
		orders[2].Status__c = OrderModel_CS.STATUS_PROVIDER_ACCEPTED;
		orders[3].Status__c = OrderModel_CS.STATUS_NEW;
		orders[4].Status__c = OrderModel_CS.STATUS_CANCELLED;
		orders[5].Status__c = OrderModel_CS.STATUS_RECURRING;
		
		Database.Saveresult[] results = Database.insert(orders);
		
		for(Database.Saveresult r : results){
			System.debug('Insert result: ' + r.isSuccess());
			
			for(Database.Error e : r.getErrors()){
				System.debug(e.getMessage());
			}
		}
		
		///////////////////////////////////
		// Page Landing
		///////////////////////////////////
		
		//Set the page and any parameters
		PageReference pr = Page.PayerHomeDashboard_CS;

		//Navigate to page
		Test.setCurrentPage(pr);
		
		//Log the desired user in
		TestServices_CS.login(payerUsers[0]);
		
		dashboard = new PayerHomeDashboard_CS();
		dashboard.init();
	}
}