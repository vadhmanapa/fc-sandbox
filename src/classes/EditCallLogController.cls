public with sharing class EditCallLogController {

	public static final Integer MAX_CALL_LOG_LIST_SIZE = 50;
	public static final Integer MAX_CASE_LIST_SIZE = 50;
	public static final Integer MAX_CALL_LOG_LINK_SIZE = 50;

	public static final String DEFAULT_LINK_OPTION = 'Case';

	public static final String CALLER_CONTACT_FIELD_ID = '00NQ0000001LJl2';
	public static final String DEFAULT_RETURL = '/a2f/o';

	public static final String STATUS_START = 'Unfinished';
	public static final String STATUS_FINISH = 'Finished';

	public static final String NO_SUBJECT = '-- No Subject --';

	public PageReference retPage;
	public Map<String,String> pageParams = ApexPages.currentPage().getParameters();
	
	public String CASE_QUE_RECORD_TYPE_ID {get; set;}

	public List<SelectOption> callReasonDetailOptions {
		get {
			List<SelectOption> options = new List<SelectOption>();

			for(Schema.PicklistEntry entry :
				Call_Log__c.Call_Reason_Detail_Multi__c.getDescribe().getPicklistValues())
			{
				if(!entry.isActive()) { continue; }
				options.add(new SelectOption(entry.getValue(),entry.getLabel()));
			}

			return options;
		}
	}

	public List<SelectOption> productCategoryOptions {
		get {
			List<SelectOption> options = new List<SelectOption>();

			for(Schema.PicklistEntry entry :
				Call_Log__c.Product_Category__c.getDescribe().getPicklistValues())
			{
				if(!entry.isActive()) { continue; }
				options.add(new SelectOption(entry.getValue(),entry.getLabel()));
			}

			return options;
		}
	}

	public Boolean enableRecordTypeSelection {
		get { return (RecordTypes.availableCallLogTypes.size() > 1); }
	}

	public String callLogType {
		get {
			if(RecordTypes.recordTypesById.containsKey(log.RecordTypeId)){
				return RecordTypes.recordTypesById.get(log.RecordTypeId).Name;
			}
			return '';
		}
	}

	public String newContactType {
		get{
			if(RecordTypes.recordTypesById.containsKey(newContact.RecordTypeId)) {
				return RecordTypes.recordTypesById.get(newContact.RecordTypeId).Name;
			}
			return '';
		}
	}

	public String newCaseType {
		get{
			if(RecordTypes.recordTypesById.containsKey(newCase.RecordTypeId)) {
				return RecordTypes.recordTypesById.get(newCase.RecordTypeId).Name;
			}
			return '';
		}
	}

	public List<SelectOption> queSupportDetailOptions {
		get {
			List<SelectOption> options = new List<SelectOption>();

			for(Schema.PicklistEntry entry :
				Case.Que_Support_Detail__c.getDescribe().getPicklistValues())
			{
				if(!entry.isActive()) { continue; }
				options.add(new SelectOption(entry.getValue(),entry.getLabel()));
			}

			return options;
		}
	}
	

	// Created: 8/13/2015 - karnold - ISSCI-145 - Changed case reasons to a select options list to be able to filter out options.
	public List<SelectOption> caseReasons {
		get {			
			return queSupportDetailOptions;
		}
	}
	
	//Case Sub-reason picklist
	public List<SelectOption> caseSubReasons {
		get {
			List<SelectOption> options = new List<SelectOption>();

			for(Schema.PicklistEntry entry :
				Case.Case_SubReason__c.getDescribe().getPicklistValues())
			{
				if(!entry.isActive()) { continue; }
				options.add(new SelectOption(entry.getValue(),entry.getLabel()));
			}

			return options;
		}
	}

	// Created: 7/2/2015 - karnold - ISSCI-64 - Options list for product details on the newCase modal.
	public List<SelectOption> productCategoryDetailOptions {
		get {
			List<SelectOption> options = new List<SelectOption>();

			for(Schema.PicklistEntry entry :
				Case.Product_Category__c.getDescribe().getPicklistValues())
			{
				if(!entry.isActive()) { continue; }
				options.add(new SelectOption(entry.getValue(),entry.getLabel()));
			}

			return options;
		}
	}

	// 8/12/2015 - karnold - ISSCI-145 - Added to conditionally render the Que Support Call Button 
	public Boolean renderQueSupportCallButton {
		get {
			if (!callerCaseList.isEmpty() && log.Call_Reason__c == 'Que Support') {
				return true;
			} else {
				return false;
			}
		}
	}

	public static final Map<Id,String> contactRecordTypeToCallerType = new Map<Id,String>{
		RecordTypes.providerContactId => 'Provider',
		RecordTypes.payorContactId => 'Payor',
		RecordTypes.patientId => 'Patient',
		RecordTypes.doctorId => 'Hospital/DR'
	};

	//Linking

	public String selectedLinkOption {
		get{ if(selectedLinkOption == null) { selectedLinkOption = DEFAULT_LINK_OPTION; } return selectedLinkOption; }
		set;
	}

	public static List<SelectOption> linkOptions {
		get {
			if(linkOptions == null) {
				linkOptions = new List<SelectOption>{
					new SelectOption('','--Please Select--'),
					new SelectOption('Case','Case'),
					new SelectOption('Claims_Inquiry__c','Claim Inquiry'),
					new SelectOption('Contact','Contact'),
					new SelectOption('Patient','Patient'),
					new SelectOption('Other_Call_Log__c','Call Log'),
					new SelectOption('Order__c','Order')
				};
			}
			return linkOptions;
		}
	}

	//Links to list pages
	public String callerLogListURL {
		get{ return '/a2f?rlid=' + CALLER_CONTACT_FIELD_ID + '&id=' + log.Caller_Contact__c; }
	}

	public String caseListURL {
		get{ return '/500?rlid=RelatedCaseList&id=' + log.Caller_Contact__c; }
	}

	public String callerInquiriesListURL {
		get { return ''; }
	}

	public void doNothing(){}

	//Records

	public Call_Log__c log {get;set;}

	public Contact caller {get;set;}
	public Contact patient {get;set;}

	public Contact newContact {get;set;}
	public Contact newPatient {get;set;}
	public Case newCase {get;set;}
	public Claims_Inquiry__c newInquiry {get;set;}
	public Call_Log_Relation__c newRelation {get;set;}

	public Boolean newContactSaveStatus {get;set;}
	public Boolean newPatientSaveStatus {get;set;}
	public Boolean newCaseSaveStatus {get;set;}
	public Boolean newInquirySaveStatus {get;set;}

	//Related Lists
	public List<CallLogWrapper> callerLogList {get;set;}
	public List<CaseWrapper> callerCaseList {get;set;}
	public List<InquiryWrapper> callerInquiryList {get;set;}
	public List<RelationWrapper> callLogRelations {get;set;}

	//Wrapper Classes
	public abstract class ListItemWrapper {
		public SObject baseRecord {get;set;}
		public Boolean selected {get;set;}
		public Boolean linked {get;set;}

		public ListItemWrapper() {
			this.selected = false;
			this.linked = false;
		}

		public ListItemWrapper(SObject baseRecord) {
			this();
			this.baseRecord = baseRecord;
		}
	}

	public class CallLogWrapper extends ListItemWrapper {

		public Call_Log__c record { get { return (Call_Log__c)baseRecord; } }

		public CallLogWrapper(Call_Log__c record) {
			super(record);
		}
	}

	public class CaseWrapper extends ListItemWrapper {

		public Case record { get { return (Case)baseRecord; } }

		public String subject { get { return (record.subject == null ? NO_SUBJECT : record.subject); } }

		public CaseWrapper(Case record) {
			super(record);
		}
	}

	public class RelationWrapper extends ListItemWrapper {

		public Call_Log_Relation__c record { get { return (Call_Log_Relation__c)baseRecord; } }

		public RelationWrapper(Call_Log_Relation__c record) {
			super(record);
		}
	}

	public class InquiryWrapper extends ListItemWrapper {

		public Claims_Inquiry__c record { get { return (Claims_Inquiry__c)baseRecord; } }

		public InquiryWrapper(Claims_Inquiry__c record) {
			super(record);
		}
	}

	//Controller and Methods
	public EditCallLogController(ApexPages.StandardController stdCon) {

		//Set Que Record Type Id for Case
		CASE_QUE_RECORD_TYPE_ID = '012300000019Su0AAE';
		
		List<String> fieldNames = new List<String>();
		for (String fieldName : Schema.sObjectType.Call_Log__c.fields.getMap().keySet()){
			fieldNames.add(fieldName);
		}
		if(!Test.isRunningTest()){
			stdCon.addFields(fieldNames);
		}

		log = (Call_Log__c)stdCon.getRecord();
		System.debug(LoggingLevel.INFO,log);

		init();
		/*
		if(!Test.isRunningTest()){
			stdCon.addFields(new List<String>{});
		}
		*/

	}

	public void init() {

		//Handle page parameters
		if( pageParams.containsKey('retURL') &&
			String.isNotBlank(pageParams.get('retURL')))
		{
			retPage = new PageReference('/' + pageParams.get('retURL'));
		} else {
			retPage = new PageReference(DEFAULT_RETURL);
		}

		if( pageParams.containsKey('callerId') &&
			String.isNotBlank(pageParams.get('callerId')))
		{
			log.Caller_Contact__c = pageParams.get('callerId');
			System.debug('Retrieving contact: ' + log.Caller_Contact__c);
		}

		populatePayorAccountList();
		resetCallerInfo();
		resetNewContact();
		resetNewPatient();
	}

	//***********************************************************************//
	//	Caller Information
	//***********************************************************************//

	//Added for Caller Type 'Payor' to replace Account lookup field for Caller_Account__c
	public List<SelectOption> payorAccounts {get;set;}

	public void populatePayorAccountList() {
		payorAccounts = new List<SelectOption>();

		List<Account> accs = [
			SELECT Name
			FROM Account
			WHERE Type__c = :'Payer'
			AND Status__c = :'Active'
			ORDER BY Name
		];

		for (Account acc :accs) {
			payorAccounts.add(new SelectOption(acc.Id, acc.Name));
		}
	}

	public void resetCallerInfo() {

		if(log.Caller_Contact__c != null) {

			log.Caller_First_Name__c = '';
			log.Caller_Last_Name__c = '';
			log.Caller_Phone__c = '';
			log.Caller_Email__c = '';
			log.Caller_Account__c = null;
			log.Caller_Type__c = '';
			log.Caller_Company__c = '';

			System.debug('Caller contact id: ' + log.Caller_Contact__c);
			Contact caller = [
				SELECT
					Id,
					Name,
					FirstName,
					LastName,
					Phone,
					Email,
					AccountId,
					Account.Name,
					RecordTypeId
				FROM Contact
				WHERE Id = :log.Caller_Contact__c
			];

			log.Caller_First_Name__c = caller.FirstName;
			log.Caller_Last_Name__c = caller.LastName;
			log.Caller_Phone__c = caller.Phone;
			log.Caller_Email__c = caller.Email;
			log.Caller_Account__c = caller.AccountId;

			/*if(caller.RecordTypeId != RecordTypes.patientId) {
				log.Caller_Company__c = caller.Account.Name;
			}*/

			if(contactRecordTypeToCallerType.containsKey(caller.RecordTypeId)) {
				log.Caller_Type__c = contactRecordTypeToCallerType.get(caller.RecordTypeId);
			} else {
				log.Caller_Type__c = 'Misc';
			}

			//Populate patient if caller is of "Patient" type
			if(caller.RecordTypeId == RecordTypes.patientId) {
				log.Patient__c = log.Caller_Contact__c;
				resetPatientInfo();
			}
		}

		//Update caller related information
		resetCallerLogList();

		resetCallerCaseList();
		resetNewCase();

		resetCallerInquiryList();
		resetNewInquiry();

		resetRelationList();
		resetNewRelation();
	}

	public void resetCallerLogList(){

		callerLogList = new List<CallLogWrapper>();

		if(log.Caller_Contact__c == null){
			return;
		}

		Set<Id> linkedCallLogs = new Set<Id>();

		for(Call_Log_Relation__c rel : [
			SELECT
				Call_Log__c,
				Other_Call_Log__c
			FROM Call_Log_Relation__c
			WHERE Call_Log__c = :log.Id
			AND Other_Call_Log__c != null
			LIMIT 50
		]){
			linkedCallLogs.add(rel.Other_Call_Log__c);
		}

		for(Call_Log__c log : [
			SELECT
				Name,
				Call_Reason__c,
				Call_Reason_Other__c,
				Resolved_On_First_Call__c,
				Caller_Recontact__c,
				Call_Result__c,
				Call_Result_Description__c,
				CreatedDate,
				(
					SELECT Id
					FROM Related_Logs__r
					WHERE Call_Log__c = :log.Id
					LIMIT 1
				)
			FROM Call_Log__c
			WHERE Caller_Contact__c = :log.Caller_Contact__c
			AND Id != :log.Id
			ORDER BY CreatedDate DESC
			LIMIT :MAX_CALL_LOG_LIST_SIZE
		]){
			CallLogWrapper newWrapper = new CallLogWrapper(log);

			if(log.Related_Logs__r.size() > 0) {
				newWrapper.linked = true;
			}

			callerLogList.add(newWrapper);
		}
	}

	public void addCallLogLink() {

		List<Call_Log_Relation__c> toInsert = new List<Call_Log_Relation__c>();

		for(CallLogWrapper cw : callerLogList) {
			if(cw.selected && !cw.linked) {
				toInsert.add(new Call_Log_Relation__c(
					Call_Log__c = log.Id,
					Other_Call_Log__c = cw.record.Id
				));
			}
		}

		insert toInsert;

		resetCallerLogList();
		resetRelationList();
	}

	public void removeCallLogLink() {

		Set<Id> logIds = new Set<Id>();

		for(CallLogWrapper cw : callerLogList) {
			if(cw.selected) {
				logIds.add(cw.record.Id);
			}
		}

		delete [
			SELECT Id
			FROM Call_Log_Relation__c
			WHERE Other_Call_Log__c IN :logIds
			AND Call_Log__c = :log.Id
		];

		resetCallerLogList();
		resetRelationList();
	}

	//***********************************************************************//
	//	New Contact
	//***********************************************************************//

	public void resetNewContact() {
		newContact = new Contact();
		newContact.RecordTypeId = RecordTypes.patientId;
	}

	public void saveNewContact() {

		Savepoint sp = Database.setSavepoint();

		newContactSaveStatus = true;

		if(newContact.RecordTypeId == RecordTypes.patientId || newContact.RecordTypeId == RecordTypes.payorContactId) {
			newContact.AccountId = newContact.Health_Plan__c;
		}

		try {
			insert newContact;
		} catch(Exception e) {
			System.debug(LoggingLevel.ERROR,'Error creating new contact: ' + e.getMessage());
			PageUtils.addError('Error creating new contact: ' + e.getMessage());

			newContactSaveStatus = false;
			Database.rollback(sp);

			return;
		}

		log.Caller_Contact__c = newContact.Id;

		resetNewContact();
		resetCallerInfo();
	}

	//***********************************************************************//
	//	Patient Information
	//***********************************************************************//

	public void resetPatientInfo() {

		log.Patient_Payor__c = null;
		log.Patient_First_Name__c = '';
		log.Patient_Last_Name__c = '';
		log.Patient_Phone_Number__c = '';
		log.Patient_Email__c = '';

		if(log.Patient__c != null) {

			Contact patient = [
				SELECT
					Id,
					AccountId,
					FirstName,
					LastName,
					Phone,
					Email
				FROM Contact
				WHERE Id = :log.Patient__c
			];

			log.Patient_Payor__c = patient.AccountId;
			log.Patient_First_Name__c = patient.FirstName;
			log.Patient_Last_Name__c = patient.LastName;
			log.Patient_Phone_Number__c = patient.Phone;
			log.Patient_Email__c = patient.Email;
		}
	}

	public void resetNewPatient() {
		newPatient = new Contact(
			recordTypeId = RecordTypes.patientId
		);
	}

	public void saveNewPatient() {

		Savepoint sp = Database.setSavepoint();

		newPatientSaveStatus = true;

		newPatient.AccountId = newPatient.Health_Plan__c;

		try {
			insert newPatient;
		} catch(Exception e) {
			PageUtils.addError('Error creating new patient: ' + e.getMessage());
			newPatientSaveStatus = false;

			Database.rollback(sp);

			return;
		}

		log.Patient__c = newPatient.Id;

		resetNewPatient();
		resetPatientInfo();
	}

	//***********************************************************************//
	//	Case Information
	//***********************************************************************//
	public void resetCallerCaseList(){

		callerCaseList = new List<CaseWrapper>();

		if(log.Caller_Contact__c == null) {
			for (Case c : [
				SELECT
					CaseNumber,
					Subject,
					Priority,
					CreatedDate,
					Status
				FROM Case
				WHERE Id IN (
					SELECT Case__c
					FROM Call_Log_Case__c
					WHERE Call_Log__c = : log.Id
				)
				ORDER BY CreatedDate DESC
				LIMIT :MAX_CASE_LIST_SIZE
			]){
				CaseWrapper newWrapper = new CaseWrapper(c);
				newWrapper.linked = true;
				callerCaseList.add(newWrapper);
			}


			return;
		}

		for (Case c : [
			SELECT
				CaseNumber,
				Subject,
				Priority,
				CreatedDate,
				Status,
				(
					SELECT Id
					FROM Call_Logs__r
					WHERE Call_Log__c = :log.Id
				)
			FROM Case
			WHERE ContactId = :log.Caller_Contact__c
			ORDER BY CreatedDate DESC
			LIMIT :MAX_CASE_LIST_SIZE
		]){
			CaseWrapper newWrapper = new CaseWrapper(c);

			if(c.Call_Logs__r.size() > 0) {
				newWrapper.linked = true;
			}

			callerCaseList.add(newWrapper);
		}
	}

	public void resetNewCase() {

		newCase = new Case(
			OwnerId = UserInfo.getUserId(),
			Origin = 'Inbound Phone',
			RecordTypeId = RecordTypes.defaultCaseId,
			Priority = 'Standard'
		);

		CaseUtils.CopyFromCallLog(log, newCase);
	}

	public void saveNewCase() {
		saveCase(false);		
	}

    /**
    *	Description:		New method to create and close a case in one call.
    *	Created Date/By:	8/12/2015 - karnold - ISSCI-144
    *	Last Modified:		
    */
    public void saveNewCaseAndClose() {
		saveCase(true);
	}
	
	public void saveCase(Boolean isClosed) {
		newCaseSaveStatus = true;

		//Check required info

		if(String.isBlank(newCase.Subject) && newCase.RecordTypeId != CASE_QUE_RECORD_TYPE_ID) {
			newCase.Subject.addError('Please enter a subject');
			newCaseSaveStatus = false;
			return;
		}

		if(String.isBlank(newCase.Priority)) {
			newCase.Priority.addError('Please select a priority');
			newCaseSaveStatus = false;
			return;
		}
		
		if(String.isBlank(newCase.AccountId) && newCase.RecordTypeId == CASE_QUE_RECORD_TYPE_ID) {
			newCase.AccountId.addError('Please select an Account');
			newCaseSaveStatus = false;
			return;
		}

		/*
		if(newCase.Patient__c == null) {
			newCase.Patient__c.addError('Please select a patient');
			newCaseSaveStatus = false;
			return;
		}
		*/

		/*
		if(String.isBlank(newCase.Escalation_Level__c)) {
			newCase.Escalation_Level__c.addError('Please select an escalation level. The default is "Tier 1".');
			newCaseSaveStatus = false;
			return;
		}
		*/

		//Get ready to save

		Savepoint sp = Database.setSavepoint();

		//Added 4/16/2015 ISSCI-33
		newCase.Que_Support_Detail__c = CaseUtils.saveMultiselect(newCase.Que_Support_Detail__c);
		// 7/2/2015 - karnold - ISSCI-64 - Added to save product category multiselect properly on case creation 
		newCase.Product_Category__c = CaseUtils.saveMultiselect(newCase.Product_Category__c);

		Database.DMLOptions dmo = new Database.DMLOptions();

		try {
			AssignmentRule caseAssignmentRule = [
				SELECT Id
				FROM AssignmentRule
				WHERE Name = 'Custom Assignment'
			];

			dmo.assignmentRuleHeader.useDefaultRule = false;
			dmo.assignmentRuleHeader.assignmentRuleId = caseAssignmentRule.Id;

		} catch (Exception e) {
			System.debug(LoggingLevel.INFO,e.getMessage());
			dmo.assignmentRuleHeader.useDefaultRule = true;
		}

		if (isClosed) {
			newCase.Status = 'Closed';
		}
		newCase.setOptions(dmo);

		try{
			upsert log;
			insert newCase;
		} catch (Exception e) {
			PageUtils.addError('Error inserting case: ' + e.getMessage());
			newCaseSaveStatus = false;

			Database.rollback(sp);

			return;
		}

		Call_Log_Case__c clc = new Call_Log_Case__c(
			Case__c = newCase.Id,
			Call_Log__c = log.Id
		);

		try{
			insert clc;
		} catch (Exception e) {
			PageUtils.addError('Error linking case: ' + e.getMessage());
			Database.rollback(sp);
			return;
		}

		resetNewCase();
		resetCallerCaseList();
	}

	public void updateCaseContact() {

		if(newCase.ContactId == null) {
			return;
		}

		Contact caseContact = [
			SELECT AccountId,Phone,MobilePhone,Email
			FROM Contact
			WHERE Id = :newCase.ContactId
		];

		CaseUtils.CopyFromContact(caseContact, newCase);

		if (String.isBlank(newCase.SuppliedPhone) ||
			String.isBlank(newCase.SuppliedEmail))
		{
			PageUtils.addWarning('The Contact record is missing information (Phone, Email) that' +
				' is vital for recontact and notifications. Please enter the missing information manually.');
		}
	}

	public void addCaseLink(){

		List<Call_Log_Case__c> toInsert = new List<Call_Log_Case__c>();

		SavePoint sp = Database.setSavepoint();
		
		try {
			upsert log;
			for(CaseWrapper cw : callerCaseList) {
				if(cw.selected && !cw.linked) {
					toInsert.add(new Call_Log_Case__c(
						Call_Log__c = log.Id,
						Case__c = cw.record.Id
					));
				}
			}
	
			insert toInsert;
	
			resetCallerCaseList();
			resetRelationList();
		}catch (Exception e){
			PageUtils.addError('Error linking case: ' + e.getMessage());
			Database.rollback(sp);
			return;
		}
		
	}

	public void removeCaseLink(){

		Set<Id> caseIds = new Set<Id>();

		for(CaseWrapper cw : callerCaseList) {
			if(cw.selected) {
				caseIds.add(cw.record.Id);
			}
		}

		delete [
			SELECT Id
			FROM Call_Log_Case__c
			WHERE Case__c IN :caseIds
			AND Call_Log__c = :log.Id
		];

		resetCallerCaseList();
		resetRelationList();
	}

	//***********************************************************************//
	//	Call Log Relation
	//***********************************************************************//

	public void resetRelationList() {

		System.debug(LoggingLevel.INFO,'Resetting relation list.');

		callLogRelations = new List<RelationWrapper>();

		if(log.Id == null) {
			return;
		}

		for(Call_Log_Relation__c rel : [
			SELECT SObject_Name__c,SObject_Type__c,SObject_Link__c
			FROM Call_Log_Relation__c
			WHERE Call_Log__c = :log.Id
			LIMIT 50
		]){
			RelationWrapper newWrapper = new RelationWrapper(rel);
			newWrapper.linked = true;

			callLogRelations.add(newWrapper);
		}
	}

	public void resetNewRelation() {

		System.debug(LoggingLevel.INFO,'Resetting relation.');

		newRelation = new Call_Log_Relation__c(
			Call_Log__c = log.Id
		);
	}

	public void addRelationLink() {

		System.debug(LoggingLevel.INFO,'Adding relation link.');

		if(log.Id == null) {
			System.debug(LoggingLevel.ERROR,'Can\'t find Call Log Id!');
			return;
		}

		if(CallLogRelationModel.isPopulated(newRelation)) {

			system.debug('Relation: ' + newRelation);

			try {
				insert newRelation;
				resetNewRelation();
			} catch(Exception e) {
				PageUtils.addError(e.getMessage());
			}
		}

		resetRelationList();
	}

	public void removeRelationLink() {

		List<Call_Log_Relation__c> toDelete = new List<Call_Log_Relation__c>();

		for(RelationWrapper lw : callLogRelations) {
			if(lw.selected) {
				toDelete.add(lw.record);
			}
		}

		delete toDelete;

		resetRelationList();
	}

	//***********************************************************************//
	//	Claim Inquiry
	//***********************************************************************//

	public void resetCallerInquiryList() {

		callerInquiryList = new List<InquiryWrapper>();

		//No inquiries to find if we don't have a Contact
		if(log.Caller_Contact__c == null) {
			return;
		}

		for(Claims_Inquiry__c c : [
			SELECT
				Name,
				(
					SELECT Id
					FROM Call_Log_Claims__r
					WHERE Call_Log__c = :log.Id
				)
			FROM Claims_Inquiry__c
			WHERE Contact__c = :log.Caller_Contact__c
			ORDER BY CreatedDate DESC
			LIMIT 50
		]){
			InquiryWrapper newWrapper = new InquiryWrapper(c);

			if(c.Call_Log_Claims__r.size() > 0) {
				newWrapper.linked = true;
			}

			callerInquiryList.add(newWrapper);
		}
	}

	public void resetNewInquiry() {

		newInquiry = new Claims_Inquiry__c(
			Status_of_Inquiry__c = 'Open',
			Inquiry_via__c = 'Call',
			Inquiry_Created_on__c = Date.today(),
			Assigned_To__c = UserInfo.getUserId()
		);

		//Copy Caller if they are a Provider
		if( log.Caller_Contact__c != null &&
			log.Caller_Type__c == 'Provider')
		{
			newInquiry.Contact__c = log.Caller_Contact__c;
			newInquiry.Provider__c = log.Caller_Account__c;
		}

		//Copy Patient Account
		if(log.Patient__c != null) {
			newInquiry.Health_Plan__c = log.Patient_Payor__c;
		}

		//Copy Call Reason and Call Reason Detail
		if( String.isNotBlank(log.Call_Reason__c) &&
			String.isNotBlank(log.Call_Reason_Detail__c))
		{
			newInquiry.Reason_for_Inquiry__c = log.Call_Reason_Detail__c;
		}

		//Copy Call Reason Other
		if(String.isNotBlank(log.Call_Reason_Other__c)) {
			newInquiry.Additional_Details__c = log.Call_Reason_Other__c;
		}

		//Copy Bill Number
		if(String.isNotBlank(log.Claim_Bill_Number__c)) {
			newInquiry.Bill_Number__c = log.Claim_Bill_Number__c;
		}
	}

	public void saveNewInquiry() {

		Savepoint sp = Database.setSavepoint();

		newInquirySaveStatus = true;

		if(getNewInquiryWorkItems().size() == 0) {
			newInquiry.Create_ARAG__c = true;
		}

		if(String.isBlank(newInquiry.Issue_Summary__c)) {
			newInquiry.Issue_Summary__c.addError('Please include the issue summary.');
			newInquirySaveStatus = false;
			return;
		}

		try {
			insert newInquiry;
			System.debug(LoggingLevel.INFO,'Successfully inserted inquiry: ' + newInquiry.Id);
		} catch(Exception e) {
			PageUtils.addError('Error inserting claim inquiry: ' + e.getMessage());
			Database.rollback(sp);

			newInquirySaveStatus = false;
			return;
		}

		Call_Log_Claim__c newCallLogInquiry = new Call_Log_Claim__c(
			Claims_Inquiry__c = newInquiry.Id,
			Call_Log__c = log.Id
		);

		try {
			insert newCallLogInquiry;
			System.debug(LoggingLevel.INFO,'Successfully linked inquiry: ' + newCallLogInquiry.Id);
		} catch(Exception e) {
			PageUtils.addError('Error linking claim inquiry: ' + e.getMessage());
			Database.rollback(sp);

			newInquirySaveStatus = false;
			return;
		}

		resetNewInquiry();
		resetCallerInquiryList();
	}

	public void addInquiryLink() {

		List<Call_Log_Claim__c> toInsert = new List<Call_Log_Claim__c>();

		for(InquiryWrapper cw : callerInquiryList) {
			if(cw.selected && !cw.linked) {
				toInsert.add(new Call_Log_Claim__c(
					Call_Log__c = log.Id,
					Claims_Inquiry__c = cw.record.Id
				));
			}
		}

		insert toInsert;
	}

	public void removeInquiryLink() {

		Set<Id> claimIds = new Set<Id>();

		for(InquiryWrapper cw : callerInquiryList) {
			if(cw.selected) {
				claimIds.add(cw.record.Id);
			}
		}

		delete [
			SELECT Id
			FROM Call_Log_Claim__c
			WHERE Claims_Inquiry__c IN :claimIds
			AND Call_Log__c = :log.Id
		];
	}

	public List<ARAG__c> getNewInquiryWorkItems() {

		List<ARAG__c> claimWorkItems = new List<ARAG__c>();

		system.debug(LoggingLevel.INFO,'Bill Number: ' + newInquiry.Bill_Number__c);

		if(String.isNotBlank(newInquiry.Bill_Number__c)) {

			for(ARAG__c workItem : [
				SELECT Id,Name
				FROM ARAG__c
				WHERE Bill__c = :newInquiry.Bill_Number__c
				LIMIT 50
			]){
				claimWorkItems.add(workItem);
			}
		}

		return claimWorkItems;
	}

	//***********************************************************************//
	//	Quick Save
	//***********************************************************************//

	public PageReference quickSave() {

		try{
			log.Call_Reason_Detail_Multi__c = CaseUtils.saveMultiselect(log.Call_Reason_Detail_Multi__c); //Added 4/16/2015 ISSCI-33
			log.Product_Category__c = CaseUtils.saveMultiselect(log.Product_Category__c);
			upsert log;

		} catch(Exception e) {
			PageUtils.addError('Error saving call log: ' + e.getMessage());
			return null;
		}
		return null;
	}


	//***********************************************************************//
	//	Close Call Log
	//***********************************************************************//

	public PageReference close() {

		Savepoint sp = Database.setSavepoint();

		if( !(	log.Call_Reason__c == 'Referral' &&
				log.Call_Reason_Detail__c == 'Submit new order' &&
				log.Transferred_To_Provider__c == null) &&
		   	(log.Call_Reason__c != null) &&
		   	(log.Call_Result__c != null) &&
		   	(log.Caller_Type__c != 'Provider' || log.Call_Reason__c != 'Que Support' || log.Caller_Account__c != null))
	  	{

			try{

				log.Status__c = 'Closed';
				log.Call_Ended__c = DateTime.now();
				log.Call_Reason_Detail_Multi__c = CaseUtils.saveMultiselect(log.Call_Reason_Detail_Multi__c); //Added 4/16/2015 ISSCI-33
				log.Product_Category__c = CaseUtils.saveMultiselect(log.Product_Category__c);


				upsert log;

				//Link patient if entered
				if(log.Patient__c != null) {

					Call_Log_Relation__c patientLink = new Call_Log_Relation__c(
						Call_Log__c = log.Id,
						Patient__c = log.Patient__c
					);

					insert patientLink;
				}

			} catch(Exception e) {
				PageUtils.addError('Error saving call log: ' + e.getMessage());
				Database.rollback(sp);
				return null;
			}

			return new PageReference('/' + log.Id);

		} else {
			if( (	log.Call_Reason__c == 'Referral' &&
					log.Call_Reason_Detail__c == 'Submit new order' &&
					log.Transferred_To_Provider__c == null))
			{
				PageUtils.addError('Transferred To Provider should be selected when Call Reason is "Referral" and Call Reason Detail is "Submit new order"');
			}
			else if (log.Call_Reason__c == null){
				PageUtils.addError('Call Reason is required to Close Call Log');
			}else if(log.Call_Result__c == null){
				PageUtils.addError('Call Result is required to Close Call Log');
			}
			else if (log.Caller_Type__c == 'Provider' && log.Call_Reason__c == 'Que Support' && log.Caller_Account__c == null) {
				PageUtils.addError('Caller Account is required for Que Support questions');
			}
			return null;

		}


	}
	
	//***********************************************************************//
	//	Transfer to Claims
	//***********************************************************************//
	
	public PageReference transferToClaims() {
		
		log.Call_Reason__c = 'Claims';
		log.Call_Result__c = 'Internal Transfer';
		log.Transferred_To_Department__c = 'Claims';
		return close();
	}
	
	//***********************************************************************//
	//	Transfer to Customer Service
	//***********************************************************************//
	
	public PageReference transferToCS() {
		
		log.Call_Reason__c = 'Customer Service';
		log.Call_Result__c = 'Internal Transfer';
		log.Transferred_To_Department__c = 'Customer Service';
		return close();
	}

    //***********************************************************************//
	//	Que Support Call    // 8/12/2015 - karnold - added to close a call log when a que case exists.
	//***********************************************************************//
	
	public PageReference queSupportCall() {
		log.Call_Reason__c = 'Que Support';
        log.Call_Result__c = 'Created Case';
		return close();
	}


	//***********************************************************************//
	//	Cancel
	//***********************************************************************//

	public PageReference cancel() {

		upsert log;

		return new PageReference('/' + log.Id);
	}


	//***********************************************************************//
	//	Contact Record Type Handling in New Contact Modal - Added 4/9/2015, Modified 4/18/2015 - ISSCI-46
	//***********************************************************************//

	//Only allow certain contact record types on the page
	public List<SelectOption> contactRecordTypesSelectOptions{
		get{
			if (contactRecordTypesSelectOptions == null){
				contactRecordTypesSelectOptions = new List<SelectOption>();
				contactRecordTypesSelectOptions.add(new SelectOption(RecordTypes.patientId, 'Patient'));
				contactRecordTypesSelectOptions.add(new SelectOption(RecordTypes.payorContactId, 'Payor'));
				contactRecordTypesSelectOptions.add(new SelectOption(RecordTypes.doctorId, 'Hospital/Dr.'));
				contactRecordTypesSelectOptions.add(new SelectOption(RecordTypes.otherId, 'Other'));
			}
			return contactRecordTypesSelectOptions;
		} private set;
	}

	public static final List<String> CONTACT_FIELDS = new List<String>{
		'FirstName',
		'Middle_Name__c',
		'LastName',
		'Phone',
		'Fax',
		'Email',
		'Birthdate',
		'Health_Plan__c',
		'Health_Plan_ID__c',
		'Medicare_ID__c',
		'Medicaid_ID__c',
		'MailingStreet',
		'MailingCity',
		'MailingState',
		'MailingPostalCode',
		'AccountId'
	};

	public static final List<String> CONTACT_RECORD_TYPES = new List<String>{
		RecordTypes.patientId, //"Patient"
		RecordTypes.payorContactId, //"Payor"
		RecordTypes.doctorId, //"Hospital/DR"
		//RecordTypes.providerContactId, //"Doctor"
		//RecordTypes.payerRelationsId, //"Care Coordinator"
		RecordTypes.otherId //"Other"
	};

	public static final Map<String, List<String>> REQUIRED_FIELDS = new Map<String, List<String>>{
		RecordTypes.patientId => new List<String>{
			'LastName',
			'Phone',
			'Birthdate',
			'Health_Plan__c'
			//'Health_Plan_ID__c'
		},
		RecordTypes.payorContactId => new List<String>{
			'LastName',
			'Phone',
			'Health_Plan__c'
		},
		RecordTypes.doctorId => new List<String>{
			'LastName',
			'Phone'
		}
		/*RecordTypes.providerContactId => new List<String>{
			'LastName',
			'Phone'
		},
		RecordTypes.payerRelationsId => new List<String>{
			'Email',
			'Health_Plan__c'
		}*/
	};

	public static final Map<String, List<String>> HIDDEN_FIELDS = new Map<String, List<String>>{
		RecordTypes.patientId => new List<String>{
			'Fax',
			'Email',
			'AccountId'
		},
		RecordTypes.payorContactId => new List<String>{
			'Middle_Name__c',
			'Birthdate',
			'Health_Plan_ID__c',
			'Medicare_ID__c',
			'Medicaid_ID__c',
			'AccountId'
		},
		RecordTypes.doctorId => new List<String>{
			'Middle_Name__c',
			'Birthdate',
			'Email',
			'Health_Plan__c',
			'Health_Plan_ID__c',
			'Medicare_ID__c',
			'Medicaid_ID__c'
		},
		/*RecordTypes.providerContactId => new List<String>{
			'Middle_Name__c',
			'Email',
			'Birthdate',
			'Health_Plan__c',
			'Health_Plan_ID__c',
			'Medicare_ID__c',
			'Medicaid_ID__c'
		},
		RecordTypes.payerRelationsId => new List<String>{
			'Fax',
			'Birthdate',
			'Health_Plan_ID__c',
			'Medicare_ID__c',
			'Medicaid_ID__c',
			'MailingStreet',
			'MailingCity',
			'MailingState',
			'MailingPostalCode'
		}*/
		RecordTypes.otherId => new List<String>{
			'Middle_Name__c',
			'BirthDate',
			'Health_Plan__c',
			'Health_Plan_ID__c',
			'Medicare_ID__c',
			'Medicaid_ID__c'
		}
	};

	public Map<String, Map<String, Boolean>> recordTypeIdToFieldToRequired {
		get{
			if (recordTypeIdToFieldToRequired == null){
				recordTypeIdToFieldToRequired = populateEmptyMap(false);
				for (String rt : REQUIRED_FIELDS.keySet()){
					for (String field : REQUIRED_FIELDS.get(rt)){
						recordTypeIdToFieldToRequired.get(rt).put(field, true);
					}
				}
				System.debug(recordTypeIdToFieldToRequired);
			}
			return recordTypeIdToFieldToRequired;
		} private set;
	}
	public Map<String, Map<String, Boolean>> recordTypeIdToFieldToRendered {
		get{
			if (recordTypeIdToFieldToRendered == null){
				recordTypeIdToFieldToRendered = populateEmptyMap(true);
				for (String rt : HIDDEN_FIELDS.keySet()){
					for (String field : HIDDEN_FIELDS.get(rt)){
						recordTypeIdToFieldToRendered.get(rt).put(field, false);
					}
				}
				System.debug(recordTypeIdToFieldToRendered);
			}
			return recordTypeIdToFieldToRendered;
		} private set;
	}

	public Map<String, Map<String, Boolean>> populateEmptyMap(Boolean defaultSetting){
		Map<String, Map<String, Boolean>> outerMap = new Map<String, Map<String, Boolean>>();
		for (String rt : CONTACT_RECORD_TYPES){
			Map<String, Boolean> innerMap = new Map<String, Boolean>();
			for (String field : CONTACT_FIELDS){
				innerMap.put(field, defaultSetting);
			}
			outerMap.put(rt, innerMap);
		}
		return outerMap;
	}
}