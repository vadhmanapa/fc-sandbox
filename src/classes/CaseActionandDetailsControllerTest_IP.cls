@isTest
private class CaseActionandDetailsControllerTest_IP
{
	public static TestCaseDataFactory_IP generateNewData;
	private static CaseActionandDetailsController_IP caseActionCon;
	private static User testUser;

	@isTest static void testActionPicklistOnInit()
	{
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		generateNewData.generateReasonsAndActions();
		testUser = TestCaseDataFactory_IP.csUser;
		List<SelectOption> actions = new List<SelectOption>();
		List<SelectOption> actionDetails = new List<SelectOption>();

		System.runAs(testUser){
			Test.startTest();
			caseActionCon = new CaseActionandDetailsController_IP();
			actions = caseActionCon.getActions();
			actionDetails = caseActionCon.getActionDetails();
			Test.stopTest();
		}
		
		System.assertEquals(1, actions.size(),'The list has only none option and is empty without required input parameters');
		System.assertEquals(1, actionDetails.size(), 'The list has only none option and is empty without input parameters');
	}

	@isTest static void testActionPicklistWithInput()
	{
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		generateNewData.generateReasonsAndActions();
		testUser = TestCaseDataFactory_IP.csUser;
		List<SelectOption> actions = new List<SelectOption>();

		System.runAs(testUser){
			caseActionCon = new CaseActionandDetailsController_IP();
			caseActionCon.reasonDetailSelected = TestCaseDataFactory_IP.reasonDetail[0].Id;

			Test.startTest();
			actions = caseActionCon.getActions();
			Test.stopTest();
		}

		System.assertNotEquals(1, actions.size(),'The list has only none option and is empty without required input parameters');
	}

	@isTest static void testActionandDetailsFromParentCase()
	{
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		generateNewData.generateReasonsAndActions();
		testUser = TestCaseDataFactory_IP.csUser;
		List<SelectOption> actions = new List<SelectOption>();
		
		System.runAs(testUser){
			Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Payor0' LIMIT 1];
			Account prAcc = [SELECT Id, Name FROM Account WHERE Name = 'Provider0' LIMIT 1];
			Contact c = [SELECT Id, Name, Birthdate,FirstName, LastName, Account.Name, AccountId, RecordTypeId, Account.Status__c, Phone, Email, Health_Plan_ID__c 
						FROM Contact WHERE Health_Plan_ID__c= '123450' AND AccountId =: acc.Id LIMIT 1];
			Case cas = new Case();
			cas.AccountId = acc.Id;
			cas.ContactId = c.Id;
			cas.CaseReasonDetailId__c = TestCaseDataFactory_IP.reasonDetail[0].Id;
			cas.CaseActionId__c = TestCaseDataFactory_IP.actionAndDetails[0].Id;
			insert cas;

			caseActionCon = new CaseActionandDetailsController_IP();

			Test.startTest();
			caseActionCon.preCase = cas;
			Test.stopTest();
		}

		System.assertNotEquals('', caseActionCon.reasonDetailSelected ,'The value was filled from parent case');
		System.assertNotEquals('', caseActionCon.actionSelected ,'The value was filled from parent case');
		System.assertNotEquals('', caseActionCon.actionDetailSelected ,'The value was filled from parent case');
	}

	@isTest static void testActionDetailsReset()
	{
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		generateNewData.generateReasonsAndActions();
		testUser = TestCaseDataFactory_IP.csUser;
		List<SelectOption> actions = new List<SelectOption>();
		
		System.runAs(testUser){
			Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Payor0' LIMIT 1];
			Account prAcc = [SELECT Id, Name FROM Account WHERE Name = 'Provider0' LIMIT 1];
			Contact c = [SELECT Id, Name, Birthdate,FirstName, LastName, Account.Name, AccountId, RecordTypeId, Account.Status__c, Phone, Email, Health_Plan_ID__c 
						FROM Contact WHERE Health_Plan_ID__c= '123450' AND AccountId =: acc.Id LIMIT 1];
			Case cas = new Case();
			cas.AccountId = acc.Id;
			cas.ContactId = c.Id;
			cas.CaseReasonDetailId__c = TestCaseDataFactory_IP.reasonDetail[0].Id;
			cas.CaseActionId__c = TestCaseDataFactory_IP.actionAndDetails[0].Id;
			insert cas;

			caseActionCon = new CaseActionandDetailsController_IP();

			Test.startTest();
			caseActionCon.preCase = cas;
			caseActionCon.resetActionDetails();
			Test.stopTest();
		}

		System.assertEquals('', caseActionCon.actionDetailSelected ,'The value was reset after filled from parent case');
	}

	@isTest static void testGetActionDetailsValue()
	{
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		generateNewData.generateReasonsAndActions();
		testUser = TestCaseDataFactory_IP.csUser;
		List<SelectOption> actions = new List<SelectOption>();
		CRR__c cr = new CRR__c();

		System.runAs(testUser){
			caseActionCon = new CaseActionandDetailsController_IP();
			caseActionCon.reasonDetailSelected = TestCaseDataFactory_IP.reasonDetail[0].Id;
			actions = caseActionCon.getActions();
			ActionDetail__c testAct= [SELECT Id, Name, Action__c FROM ActionDetail__c WHERE Id=: TestCaseDataFactory_IP.actionAndDetails[0].Id];
			caseActionCon.actionSelected = testAct.Action__c;
			actions = caseActionCon.getActionDetails();
			caseActionCon.actionDetailSelected = testAct.Id;

			Test.startTest();
			cr = caseActionCon.getActionRelatedValues();
			Test.stopTest();
		}

		System.assertNotEquals(null, cr,'The method returned the requested');
	}

	@isTest static void testResetController()
	{
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		generateNewData.generateReasonsAndActions();
		testUser = TestCaseDataFactory_IP.csUser;
		List<SelectOption> actions = new List<SelectOption>();
		CRR__c cr = new CRR__c();

		System.runAs(testUser){
			caseActionCon = new CaseActionandDetailsController_IP();
			caseActionCon.reasonDetailSelected = TestCaseDataFactory_IP.reasonDetail[0].Id;
			actions = caseActionCon.getActions();
			ActionDetail__c testAct= [SELECT Id, Name, Action__c FROM ActionDetail__c WHERE Id=: TestCaseDataFactory_IP.actionAndDetails[0].Id];
			caseActionCon.actionSelected = testAct.Action__c;
			actions = caseActionCon.getActionDetails();
			caseActionCon.actionDetailSelected = testAct.Id;

			Test.startTest();
			cr = caseActionCon.getActionRelatedValues();
			Test.stopTest();
		}

		System.assertNotEquals(null, cr,'The method returned the requested');
	}

	@isTest static void testSetValueToParent()
	{
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		generateNewData.generateReasonsAndActions();
		testUser = TestCaseDataFactory_IP.csUser;
		List<SelectOption> actions = new List<SelectOption>();
		CRR__c cr = new CRR__c();

		Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Payor0' LIMIT 1];
		Account prAcc = [SELECT Id, Name FROM Account WHERE Name = 'Provider0' LIMIT 1];
		Contact c = [SELECT Id, Name, Birthdate,FirstName, LastName, Account.Name, AccountId, RecordTypeId, Account.Status__c, Phone, Email, Health_Plan_ID__c 
					FROM Contact WHERE Health_Plan_ID__c= '123450' AND AccountId =: acc.Id LIMIT 1];

		Case cas = new Case();
		cas.AccountId = acc.Id;
		cas.ContactId = c.Id;
		cas.CaseReasonDetailId__c = TestCaseDataFactory_IP.reasonDetail[0].Id;
		cas.CaseActionId__c = TestCaseDataFactory_IP.actionAndDetails[0].Id;
		insert cas;

		Case testCase = [SELECT Id, CaseNumber FROM Case WHERE Id=: cas.Id LIMIT 1];
		
		System.runAs(testUser){
			caseActionCon = new CaseActionandDetailsController_IP();
			PageControllerBase_IP pgControllerTest = new PageControllerBase_IP();
			pgControllerTest.caseLog.caseDetails = testCase;
			caseActionCon.pageController = pgControllerTest;
			caseActionCon.reasonDetailSelected = TestCaseDataFactory_IP.reasonDetail[0].Id;
			actions = caseActionCon.getActions();
			ActionDetail__c testAct= [SELECT Id, Name, Action__c FROM ActionDetail__c WHERE Id=: TestCaseDataFactory_IP.actionAndDetails[0].Id];
			caseActionCon.actionSelected = testAct.Action__c;
			actions = caseActionCon.getActionDetails();
			caseActionCon.actionDetailSelected = testAct.Id;
			
			Test.startTest();
			caseActionCon.setParentControllerValues();
			Test.stopTest();
		}

		System.assertNotEquals(null, cr,'The method returned the requested');
	}
}