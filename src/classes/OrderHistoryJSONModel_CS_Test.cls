@isTest
private class OrderHistoryJSONModel_CS_Test { 

	static testMethod void OrderHistoryJSONModel_CS_Parse_Test() {
		String historyJSON = '[ {';
		historyJSON += '\"timestamp\" :  \"2015-10-06T23:44:24.566Z\",';
		historyJSON +=  '\"orderStatus\" : \"Status 0\",';
		historyJSON +=  '\"contactName\" : \"Test Name 0\",';
		historyJSON +=  '\"contactId\" : \"Id 0\",';
		historyJSON +=  '\"accountName\" : \"Test Account Name 0\"';
		historyJSON +=  '}, {';
		historyJSON += '\"timestamp\" :  \"2015-10-06T23:44:24.566Z\",';
		historyJSON +=  '  \"orderStatus\" : \"Status 1\",';
		historyJSON +=  '  \"contactName\" : \"Test Name 1\",';
		historyJSON +=  '  \"contactId\" : \"Id 1\",';
		historyJSON +=  '  \"accountName\" : \"Test Account Name 1\"';
		historyJSON +=  '}, {';
		historyJSON += '\"timestamp\" :  \"2015-10-06T23:44:24.566Z\",';
		historyJSON +=  '  \"orderStatus\" : \"Status 2\",';
		historyJSON +=  '  \"contactName\" : \"Test Name 2\",';
		historyJSON +=  '  \"contactId\" : \"Id 2\",';
		historyJSON +=  '  \"accountName\" : \"Test Account Name 2\"';
		historyJSON +=  '}, {';
		historyJSON += '\"timestamp\" :  \"2015-10-06T23:44:24.566Z\",';
		historyJSON +=  '  \"orderStatus\" : \"Status 3\",';
		historyJSON +=  '  \"contactName\" : \"Test Name 3\",';
		historyJSON +=  '  \"contactId\" : \"Id 3\",';
		historyJSON +=  '  \"accountName\" : \"Test Account Name 3\"';
		historyJSON +=  '}, {';
		historyJSON += '\"timestamp\" :  \"2015-10-06T23:44:24.566Z\",';
		historyJSON +=  '  \"orderStatus\" : \"Status 4\",';
		historyJSON +=  '  \"contactName\" : \"Test Name 4\",';
		historyJSON +=  '  \"contactId\" : \"Id 4\",';
		historyJSON +=  '  \"accountName\" : \"Test Account Name 4\"';
		historyJSON +=  '} ]';

		List<OrderHistoryJSONModel_CS> ohList = OrderHistoryJSONModel_CS.parseList(historyJSON);
		System.assertEquals(5, ohList.size());
		for (Integer i = 0; i < ohList.size(); i++) {
			System.assertNotEquals(null, ohList[i].timestamp);
			System.assertEquals('Status ' + i, ohList[i].orderStatus);
			System.assertEquals('Test Name ' + i, ohList[i].contactName);
			System.assertEquals('Id ' + i, ohList[i].contactId);
			System.assertEquals('Test Account Name ' + i, ohList[i].accountName);
		}
	}
}