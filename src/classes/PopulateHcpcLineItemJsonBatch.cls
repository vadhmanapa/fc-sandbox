/**
 *  @Description Batch will copy all DME_Line_Item__c object onto the Order__c.Hcpc_Line_Item_Json__c field as a JSON value.
 *  @Author Cloud Software LLC, Keith Arnold
 *  Revision History: 
 *		01/12/2016 - karnold - Created.
 *
 */
public class PopulateHcpcLineItemJsonBatch implements Database.Batchable<SObject> {
	private String query;

	/* Constructors */

	/**
	* @description Will construct a batch with the default query (All orders)
	*/
	public PopulateHcpcLineItemJsonBatch() {
		this.query = 
		'SELECT (SELECT Order__c, DME__c, Quantity__c, Product_Code__c FROM DMEs__r) ' +
		'FROM Order__c ';
	}

	/**
	* @description Will construct a batch with a specified query on Order__c
	* @param query A SOQL query on Order__c to run the batch with.
	*/
	public PopulateHcpcLineItemJsonBatch(String query) {
		this.query = query;
	}

	/* Batch methods */
	
	public Database.QueryLocator start(Database.BatchableContext context) {
		return Database.getQueryLocator(query);
	}

	public void execute(Database.BatchableContext context, List<Order__c> scope) {
		for (Order__c order : scope) {
			List<DmeLineItemWrapper_CS> wrapperList = new List<DmeLineItemWrapper_CS>();
			for (DME_Line_Item__c li : order.DMEs__r) {
				wrapperList.add(new DmeLineItemWrapper_CS(li));
			}
			order.HCPC_Line_Item_JSON__c = JSON.serialize(wrapperList);
		}

		update scope;
	}
	
	public void finish(Database.BatchableContext context) {}
}