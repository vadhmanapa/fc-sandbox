@isTest
private class TestEmailAssignment_IP {

    static testMethod void CreateTestData() {
        // TO DO: implement unit test
        // create test user
       string randomName = string.valueof(Datetime.now()).replace('-','').replace(':','').replace(' ','');
        String userName = randomName + 'testcheasdck@test.com';
        User testUser = new User();
        Profile p = [Select id from Profile where Name ='Claims w Service Cloud'];
      testUser.FirstName = 'teasdst';
      testUser.LastName = 'dididid';
      testUser.Username = userName;
      testUser.Email = randomName+userName;
      testUser.Alias = 'teasdst';
      testUser.CommunityNickname = 'testforemail';
      testUser.TimeZoneSidKey = 'America/Los_Angeles';
      testUser.EmailEncodingKey = 'ISO-8859-1';
      testUser.ProfileId = p.Id;
      testUser.LanguageLocaleKey = 'en_US';
      testUser.LocaleSidKey = 'en_US';
      testUser.Assign_Email__c = true;
      insert testUser;
      String testUserId = testUser.Id; 
         
      //create test email log
       
    /*  Email_Log__c testlo = new Email_Log__c(From_Email_Address__c = 'test@example.com', Email_Subject__c = 'test subject', Email_Content__c = 'test content');
      insert testlo;
        List<User> checkUser = [SELECT Id, Assign_Email__c, Counter__c FROM User WHERE Id = :assignedTo  LIMIT 1];
    for( User u : checkUser ){
        System.assertEquals(1, u.Counter__c, 'The counter was incremented ');
        System.assertEquals(true, u.Assign_Email__c, 'The email was assigned to this user');
    }  
       
      
      Email_Log__c testlog = new Email_Log__c(From_Email_Address__c = 'test@example.com', Email_Subject__c = 'test subject', Email_Content__c = 'test content', Assigned_To__c = testUserId, OwnerId = testUserId);
      insert testlog;
      String testId = testlog.Id;
      String assignedTo = testlog.Assigned_To__c;
      List<Email_Log__c> checkLog = [SELECT Id, Assigned_To__c FROM Email_Log__c WHERE Id = :testId  LIMIT 1];
    for( Email_Log__c log : checkLog ){
        System.assertEquals(1, checkLog.size(), 'The Email log was created ');
        System.assertNotEquals(null, log.Assigned_To__c, 'The email log was assigned');
    }
     List<User> checkUser = [SELECT Id, Assign_Email__c, Counter__c FROM User WHERE Id = :assignedTo  LIMIT 1];
    for( User u : checkUser ){
        System.assertEquals(1, u.Counter__c, 'The counter was incremented ');
        System.assertEquals(true, u.Assign_Email__c, 'The email was assigned to this user');
    }*/
        // insert a test account
        
        Account testAcc = new Account(Name = 'Test Account', Type__c = 'Provider');
        insert testAcc;
    
   // test case-3
   
    Email_Log__c testlog7 = new Email_Log__c(From_Email_Address__c = 'test@test.com',To_Email_Address__c = 'Clearhelp@accessintegra.com', All_To_Addresses__c = 'Clearhelp@accessintegra.com' , CC_Addresses__c = 'clearhelp@accessintegra.com', Email_Subject__c = 'test subject', Email_Content__c = 'test content');
      insert testlog7;
      String testId7 = testlog7.Id;
      List<Email_Log__c> checkLog7 = [SELECT Id, Assigned_To__c FROM Email_Log__c WHERE Id = :testId7  LIMIT 1];
    for( Email_Log__c log : checkLog7 ){
        System.assertEquals(1, checkLog7.size(), 'The Email log was created ');
        System.assertNotEquals(null , log.Assigned_To__c, 'The email log was assigned');
    }     
              

      Email_Log__c testlog10 = new Email_Log__c(From_Email_Address__c = 'test1@testing.com',To_Email_Address__c = 'claims@accessintegra.com',  All_To_Addresses__c = 'Claims@accessintegra.com' , CC_Addresses__c = 'claims@accessintegra.com',Email_Subject__c = 'test claims', Email_Content__c = 'test content10',
                                                Email_Account__c = testAcc.Id);
      insert testlog10;
      String testId10 = testlog10.Id;
      List<Email_Log__c> checkLog10 = [SELECT Id, Assigned_To__c, Assigned_To__r.Assign_Email__c FROM Email_Log__c WHERE Id = :testId10  LIMIT 1];
    for( Email_Log__c log : checkLog10 ){
        System.assertEquals(1, checkLog10.size(), 'The Email log was created ');
        System.assertEquals(true, log.Assigned_To__r.Assign_Email__c, 'The email log was assigned');
    }
	
    Email_Log__c searchEmail = [SELECT Id from Email_Log__c where Id=: testId10];
    searchEmail.Pushed_to_Inquiry__c = true;
    searchEmail.Status__c = 'Closed';
    searchEmail.Closed_By__c = testUserId;
    searchEmail.Closed_on__c = system.now();
    searchEmail.Email_Reason__c = 'Other';
        searchEmail.Email_Sub_Reason__c = 'No response necessary';
        searchEmail.Email_Resolution__c = 'No response necessary';
        searchEmail.Health_Plan__c = testAcc.Id;
    update searchEmail;
    List<Claims_Inquiry__c> claim = [SELECT Id, Email_Log__c from Claims_Inquiry__c where Email_Log__c =: testId10];
     for( Claims_Inquiry__c cl : claim ){
        System.assertEquals(1, claim.size(), 'The Claim Inquiry was created ');
    } 
        
        //test for Forwaded to Que Support
     
     testlog10.Forwarded_to_Que__c = true;
     update testlog10;
     
     List<Case> testQueCase = [Select Id,Que_Email_Log__c from Case where Que_Email_Log__c =:testlog10.Id];
        for(Case c : testQueCase){
            System.assertEquals(1, testQueCase.size(),'Matching Case found');
        }
    
    }
}