/**
 *  @Description Batch class that will delete all Simulation RecordType Order__c records over 1 day old.
 *  @Author Cloud Software LLC, Keith Arnold
 *  Revision History: 
 *      01/15/2016 - karnold - Created.
 *
 */
public class DeleteOldSimOrdersBatch_CS implements Database.Batchable<SObject>, Schedulable {
    private String query;
    private DateTime dt;

    /* Constructors */

    public DeleteOldSimOrdersBatch_CS() {
        dt = DateTime.now().addDays(-1);

        query = 
            'SELECT Id ' + 
            'FROM Order__c ' + 
            'WHERE RecordTypeId = \'' + OrderModel_CS.simulationRecordTypeId + '\' ' +
            'AND CreatedDate <: dt';
    }

    public DeleteOldSimOrdersBatch_CS(String query) {
        this.query = query;
    }

    public void setDateTime(DateTime newDt) {
        dt = newDt;
    }

    /* Batch Methods */

    public Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext context, List<Order__c> scope) {
        System.debug('Deleteing: ' + scope);
        delete scope;
    }
    
    public void finish(Database.BatchableContext context) {
        
    }

    /* Schedulable Methods */

    public void execute(SchedulableContext  ctx) {
        System.debug('Batch: ' + this);
        Database.executeBatch(this);
    }
}