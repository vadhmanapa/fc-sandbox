@isTest
private class PublicKnowledgeSearchBarControllerTest {

	static PublicKnowledgeSearchBarController pageCon {get;set;}

    static testMethod void search() {
        
        pageCon = new PublicKnowledgeSearchBarController();
        pageCon.searchString = 'Test';
        
        PageReference searchPage = pageCon.search();
        
        system.assert(searchPage.getParameters().containsKey('search'));
        system.assertEquals('Test',searchPage.getParameters().get('search'));        
    }
}