/*
@description Sweeper class (schedulable and batchable) that checks that no Order are in Future DOS stage
when their expected delivery date has been passed. If there are still some Orders -
update them with 'Pending Re-Acceptance' status
Use SF Schedule Apex interface
@author - Dima
@date - 2/28
 */
global class OrderFutureToPendingSweeperBatch implements Schedulable, Database.Batchable<sObject> {

    private String queryString;

    public OrderFutureToPendingSweeperBatch() {
        this.queryString = createSoqlString();
    }

    public OrderFutureToPendingSweeperBatch(String queryString) {
        this.queryString = queryString;
    }

    global void execute(SchedulableContext sctx) {
        Database.executeBatch(new OrderFutureToPendingSweeperBatch(this.queryString));
    }

    global Database.QueryLocator start(Database.BatchableContext bctx) {
        System.debug('\n\n this.queryString => ' + this.queryString + '\n');
        return Database.getQueryLocator(this.queryString);
    }

    global void execute(Database.BatchableContext bctx, List<Order__c> scope) {
        for (Order__c order : scope) {
            order.Status__c = OrderModel_CS.STATUS_PENDING_REACCEPTANCE;
        }
        update scope;
    }

    global void finish(Database.BatchableContext bctx) {
    }

    private String createSoqlString() {
        Datetime currentDatetime = Datetime.now();
        NestableCondition orderConditions = new AndCondition();
        orderConditions
            .add(new FieldCondition('Status__c').equals(OrderModel_CS.STATUS_FUTURE_DOS))
            .add(new FieldCondition('Estimated_Delivery_Time__c').greaterThanOrEqualTo(currentDatetime.addDays(-7)))
            .add(new FieldCondition('Estimated_Delivery_Time__c').lessThan(currentDatetime));

        return new SoqlBuilder()
            .selectx(new Set<String>{'Id','Stage__c','Status__c','Estimated_Delivery_Time__c'})
            .fromx('Order__c')
            .wherex(orderConditions)
            .toSoql();
    }
}