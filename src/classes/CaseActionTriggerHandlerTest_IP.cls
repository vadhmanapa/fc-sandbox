@isTest
private class CaseActionTriggerHandlerTest_IP {
	
	private static Case testCase;
    public static TestCaseDataFactory_IP generateNewData;
	private static User testCSUser;
	private static Case_Action__c act;
	private static CaseActionTriggerHandler_IP handler;

    @isTest static void testCaseCommentsUpdate(){
        	
			generateNewData = new TestCaseDataFactory_IP();
            generateNewData.initTestData();
        	generateNewData.generateReasonsAndActions();
            testCSUser = TestCaseDataFactory_IP.csUser;
        	
        	testCase = new Case();
            testCase.ContactId = TestCaseDataFactory_IP.patientContact[0].Id;
            testCase.RecordTypeId = RecordTypes.customerCallId;
            testCase.Subject = 'This is a test email subject';
            testCase.Description = 'This is a test case Description';
            testCase.CallerType__c = 'Patient';
        	insert testCase;
        
        	ActionDetail__c testAction= [SELECT Id, Name, Action__c, ActionDetail__c FROM ActionDetail__c WHERE Id=: TestCaseDataFactory_IP.actionAndDetails[0].Id];
        
        	act = new Case_Action__c();
			act.Case__c = testCase.Id;
        	act.CaseAction__c = testAction.Action__c;
        	act.CaseActionDetail__c = testAction.ActionDetail__c;
			act.Case_Comment__c = 'This is test comment';
        	
        	insert act;
        	Case testResult = [SELECT Id, Last_Case_Comment__c FROM Case WHERE Id=: testCase.Id];
        	System.assertNotEquals('', testCase.Last_Case_Comment__c);
    }
    
    /*@isTest static void createNewActionNoStatusUpdate() {
		// Implement test code
			generateTestData();

        	act = new Case_Action__c();
			act.Case__c = testCase.Id;
			act.CaseAction__c = 'Connected to Provider';
			act.CaseActionDetail__c = 'Warm transfer to the provider';
			act.Case_Comment__c = 'This is test comment';
			act.DoNotUpdate__c = true;
			Test.startTest();
			insert act;
			Test.stopTest();
		
		Case c = [SELECT Id, CaseNumber, Last_Case_Comment__c, Status FROM Case WHERE Id=: testCase.Id LIMIT 1];
		System.assertNotEquals('', c.Last_Case_Comment__c);
		System.assertNotEquals('Closed', c.Status);
	}

	@isTest static void createNewActionWithStatusUpdate() {
		// Implement test code
			generateTestData();

        	act = new Case_Action__c();
			act.Case__c = testCase.Id;
			act.CaseAction__c = 'Connected to Provider';
			act.CaseActionDetail__c = 'Warm transfer to the provider';
			act.Case_Comment__c = 'This is test comment';
			act.DoNotUpdate__c = false;
			Test.startTest();
			insert act;
			Test.stopTest();
		
		Case c = [SELECT Id, CaseNumber, Last_Case_Comment__c, Status FROM Case WHERE Id=: testCase.Id LIMIT 1];
		System.assertNotEquals('', c.Last_Case_Comment__c);
		System.assertEquals('Closed', c.Status);
	}*/
	
	

	/*@isTest static void generateTestData(){

		testCSUser = new User();
        testCSUser.FirstName = 'CustomerCallCase';
        testCSUser.LastName = 'Tester'+ System.currentTimeMillis(); // to maintain the randomness of username
        testCSUser.Belongs_To__c = 'Customer Service';
        testCSUser.Username = 'test'+System.currentTimeMillis()+'@test.com';
        testCSUser.CommunityNickname = 'cccase';
        testCSUser.ProfileId = [SELECT Id FROM Profile WHERE Name='Customer Service Supervisor w Service Cloud' LIMIT 1].Id;
        testCSUser.Email = 'test'+System.currentTimeMillis()+'@test.com';
        testCSUser.CompanyName = 'Integra Partners';
        testCSUser.Alias = 'alias';
        testCSUser.TimeZoneSidKey = 'America/New_York';
        testCSUser.EmailEncodingKey = 'UTF-8';
        testCSUser.LanguageLocaleKey = 'en_US';
        testCSUser.LocaleSidKey = 'en_US';
        //testCSUser.UserRoleId = ur1.Id;
        insert testCSUser;

        Account testPay = new Account();
        testPay.Name = 'This is test payor';
        testPay.Type__c = 'Payer';
        testPay.Status__c = 'Active';
        insert testPay;
        
        // CREATE CONTACTS - ONE OF EACH - PATIENT, PAYOR, PROVIDER, HOSPITAL/DR, FAMILY
        Contact testPatient = new Contact();
        testPatient.FirstName = 'Sick';
        testPatient.LastName = 'I am Patient';
        testPatient.RecordTypeId = RecordTypes.patientId;
        testPatient.AccountId = testPay.Id;
        testPatient.Active__c = true;
        testPatient.Birthdate = System.today();
        testPatient.Health_Plan_ID__c = '90920-9-029-9';
        insert testPatient;

        /*Case_Reasons_Relationship__c customerTestReason1 = new Case_Reasons_Relationship__c(); 
        customerTestReason1.CaseReasonDetail__c = 'O&P';
        customerTestReason1.Action__c = 'Assistance Needed';
        customerTestReason1.ActionDetail__c = 'Escalated to Help Team';
        customerTestReason1.UpdatedCaseStatus__c = 'Closed';
        insert customerTestReason1;
        
        Case_Reasons_Relationship__c customerTestReason2 = new Case_Reasons_Relationship__c(); 
        customerTestReason2.CaseReasonDetail__c = 'O&P';
        customerTestReason2.Action__c = 'Connected to Provider';
        customerTestReason2.ActionDetail__c = 'Warm transfer to the provider';
        customerTestReason2.UpdatedCaseStatus__c = 'Closed';
        insert customerTestReason2;

        Case_Reasons_Relationship__c claimsTestReason = new Case_Reasons_Relationship__c(); 
        claimsTestReason.CaseReasonDetail__c = 'Denied correctly';
        claimsTestReason.Action__c = 'Informed';
        claimsTestReason.ActionDetail__c = 'Explained the denial to the provider';
        claimsTestReason.UpdatedCaseStatus__c = 'Closed';
        insert claimsTestReason;

        testCase = new Case();
	    testCase.ContactId = testPatient.Id;
	    testCase.RecordTypeId = RecordTypes.customerCallId;
	    testCase.Subject = 'This is a test email subject';
	    testCase.Description = 'This is a test case Description';
	    testCase.CallerType__c = 'Patient';
	    //testCase.CaseReason__c = 'New Referral Inquiry';
	    //testCase.CaseReasonDetail__c = 'O&P';

	    insert testCase;
	}*/
	
}