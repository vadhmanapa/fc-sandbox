public without sharing class PayerProfile_CS extends PayerPortalServices_CS {

	private final String pageAccountType = 'Payer';

	/******* Page Permissions *******/
	public Boolean resetPasswordPermission {get{ return portalUser.role.Reset_Password__c; }}
	public Boolean editUserPermission {get{ return portalUser.role.Edit_Users__c; }} // Added by VK on 10/12/2016 for fix permission issue

	/******* Page Interface *******/	
	public String oldPassword {get;set;}
	public String newPassword {get;set;}
	public String newPasswordConfirm {get;set;}
	
	/******* Error Messages *******/
	public String usernameErrorMsg {get;set;}
	public final String nonUniqueUsernameError = 'Username already exists';
	public final String missingUsernameError = 'Please enter a unique username';

	public Boolean oldPasswordError {get;set;}
	public Boolean newPasswordError {get;set;}
	public Boolean newPasswordSuccess {get;set;}
	public String passwordMessage {get;set;}
	
	public final String invalidPassword = 'The password entered does not meet criteria, please revise.';
	public final String validPassword = 'The password has been successfully changed.';	
	public final String incorrectOldPassword = 'The password entered does not match records.';
	public final String mismatchedPasswords = 'The passwords do not match.';
	
	/******* New User Info *******/
	public String firstName {get;set;}
	public String lastName {get;set;}
	public String phone {get;set;}
	public String email {get;set;}
	public String username {get;set;}
	
	/*
	//Populates the user data with the temp data
	public Boolean updateAllInfo(){
		
		Boolean success = true;
		displayPhoneError = false; displayEmailError = false;
		
		if (!updatePhone()){
			displayPhoneError = true;
			success = false;
		}
		
		if (!updateEmail()){
			displayEmailError = true;
			success = false;
		}
		
		if (success){
			portalUser.FirstName = tempFirstName;
			portalUser.LastName = tempLastName;
		}
		
		return success;		
	}
	
	//Checks if the phone number entered is valid, and if so, updates the user's phone number
	public Boolean updatePhone() {
		
		Pattern phone = Pattern.compile('\\(?\\d{3}\\)?\\s?\\-?\\s?\\d{3}\\s?\\-?\\s?\\d{4}');
		System.debug('Pattern compiled');
		Pattern noPhone = Pattern.compile('');
		Matcher phoneMatcher = phone.matcher(tempPhone);
		Matcher noPhoneMatcher = noPhone.matcher(tempPhone);
		
		if (phoneMatcher.matches() || noPhoneMatcher.matches()){
			portalUser.Phone = tempPhone;
			return true;
		}
		
		else{
			return false;
		}
	}
	
	//Checks if the email address entered is valid, and if so, updates the user's email address
	public Boolean updateEmail(){
		Pattern email = Pattern.compile('.*\\@.*\\..*');
		Pattern noEmail = Pattern.compile('');
		Matcher emailMatcher = email.Matcher(tempEmail);
		Matcher noEmailMatcher = noEmail.Matcher(tempEmail);
		
		if (emailMatcher.matches() || noEmailMatcher.matches()){
			portalUser.Email = tempEmail;
			return true;
		}
		
		else{
			return false;
		}
	}
	*/
	
	/********* Page Params *********/
	
	/********* Constructor and Init *********/
	public PayerProfile_CS(){}
	
	// Default page action, add additional page action functionality here
	public PageReference init(){
		
		///////////////////////////////////
		// Authentication and Permission
		///////////////////////////////////
		
		if(!isSecure && !Test.isRunningTest()){
			return DoLogout();
		}
				
		if(!authenticated){
			return DoLogout();
		}

		if(portalUser.AccountType != pageAccountType){
			return homepage;
		}
		
		///////////////////////////////////
		// Additional init
		///////////////////////////////////
		
		//Info is initialized to user info
		firstName = portalUser.instance.FirstName;
		lastName = portalUser.instance.LastName;
		phone = portalUser.instance.Phone;
		email = portalUser.instance.Email;
		username = portalUser.instance.Username__c;
		
		return null;
	}//End Init

	/********************* Page Buttons/Links  ****************************/	
	
	//Updates the system with the information the user enters
	public void saveProfile(){
		
		//Check for blank username
		if(String.isBlank(username)){
			usernameErrorMsg = missingUsernameError;
			System.debug('No username entered.');
			return;
		}
		else{
			usernameErrorMsg = '';
		}
		
		//Check if username is unique
		if(!portalUser.checkUniqueUsername(username)){
			usernameErrorMsg = nonUniqueUsernameError;
			System.debug('Username: ' + username + ' is not unique.');
			return;
		}
		else{
			usernameErrorMsg = '';
		}
		
		portalUser.instance.FirstName = firstName;		
		portalUser.instance.LastName = lastName;
		portalUser.instance.Phone = phone;
		portalUser.instance.Email = email;
		portalUser.instance.Username__c = username;
		
		System.debug('New user info: ' + portalUser.instance);
		
		update portalUser.instance;
	}
	
	public void changePassword(){
		
		// Reset error status/messages
		oldPasswordError = false;
		newPasswordError = false;
		newPasswordSuccess = false;
		passwordMessage = '';
		
		// Check data exists in prompts
		if(String.isBlank(oldPassword) || String.isBlank(newPassword) || String.isBlank(newPasswordConfirm)){
			System.debug('Error: No password data found');
			return;	
		}			
		
		// Check old password
		if(!portalUser.isCurrentPassword(oldPassword)){
			System.debug('Error: Old password incorrect.');
			passwordMessage = incorrectOldPassword;
			oldPasswordError = true;
			return;				
		}
		
		// Check that new passwords match
		if(newPassword != newPasswordConfirm){
			System.debug('Error: New passwords don\'t match.');
			passwordMessage = mismatchedPasswords;
			newPasswordError = true;
			return;
		}
		
		// Finally change the password
		if(!portalUser.changePassword(newPassword)){
			System.debug('Error: Password change failed for ' + portalUser.instance.Username__c);
			passwordMessage = invalidPassword;
			newPasswordError = true;
			return;
		}
		
		// With a new salt, the sessionId cookie needs updated
		updateAuthentication();
		
		System.debug('Password successfully changed for ' + portalUser.instance.Username__c);
		passwordMessage = validPassword;
		newPasswordSuccess = true;
	}
}