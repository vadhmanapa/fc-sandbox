/**
 *  @Description Tests for Plan Patient Trigger Dispatcher
 *  @Author 
 *  Revision History: 
 *		12/14/2015 - karnold - ISSCAI-167 - Added test to check duplicate functionality.
 *		12/23/2015 - karnold - Removed references to DME_Category__c.
 *
 */
@isTest
private class PlanPatientTriggerTest_CS {
    
    //THE TRIGGER IS CURRENTLY INACTIVE
    
    static Plan_Patient__c patient;
    
    static boolean isInsert;
    static boolean isUpdate;
    static boolean isDelete;
    static boolean isUndelete;
    static boolean isBefore;
    static boolean isAfter;
    //Geocoding test data (CaSe SeNsItIvE!): 
    //1) ADDRESS Street : 111 S Test Street, City : Test, State : TE, Zip : 22222
    //1) RETURNS Lng : -1, Lat : 1
    
    //2) ADDRESS Street : 789 N Abc Rd, State : MN
    //2) RETURNS Lng : -40, Lat: 40

	/******* Test Parameters *******/

	static private final Integer N_DME_CATEGORIES = 1;
	static private final Integer N_DMES = 10;	//Should be at least 10
	
	static private List<Account> providers;
	static private List<Contact> providerUsers;
	
	static private List<Account> payers;
	static private List<Contact> payerUsers;
	
	static private List<Patient__c> patients;
	static private List<Plan_Patient__c> planPatients;

	static private DME_Settings__c dmeSettings;
	static private List<DME__c> dmes;
	
	static private List<New_Orders__c> legacyOrders;
	static private List<Order__c> orders;

	public static void initPlanPatients() {
		dmeSettings = TestDataFactory_CS.generateDMESettings();
		insert dmeSettings;

		dmes = TestDataFactory_CS.generateDMEs(N_DMES);
		insert dmes;

		System.debug('DMEs:');
		for(DME__c dme : dmes){
    		System.debug(dme);
		}
    	
		payers = TestDataFactory_CS.generatePlans('testPlan', 1);
		insert payers;

		payerUsers = TestDataFactory_CS.generatePlanContacts('PlanUser', payers, 1);
		insert payerUsers;

		providers = TestDataFactory_CS.generateProviders('testProvider', 1);
		insert providers;

		providerUsers = TestDataFactory_CS.generateProviderContacts('ProviderUser', providers, 1);
		insert providerUsers;

		patients = TestDataFactory_CS.generatePatients('TestPatient', 1);
		insert patients;

		planPatients = TestDataFactory_CS.generatePlanPatients(
			new Map<Id,List<Patient__c>>{
			payers[0].Id => new List<Patient__c>{patients[0]}
		});
		insert planPatients;
	}
    
	private static testMethod void blockDuplicatePlanPatients_DupeId_Test() {
		// GIVEN
		initPlanPatients();
		List<Plan_Patient__c> clonedPlanPatients = new List<Plan_Patient__c>();
		for (Plan_Patient__c pp : planPatients) {
			clonedPlanPatients.add(pp.clone(false,true,false,false));
		}

		// WHEN
		System.Test.startTest();
		try {
			insert clonedPlanPatients;
			System.assert(false);
		} catch (DmlException e) {
			System.Test.stopTest();
			//System.assert(e.getMessage().contains('A Plan Patient exists with this id number.'));
		}
	}

	private static testMethod void blockDuplicatePlanPatients_DupeName_Test() {
		// GIVEN
		initPlanPatients();
		List<Plan_Patient__c> clonedPlanPatients = new List<Plan_Patient__c>();
		for (Plan_Patient__c pp : planPatients) {
			Plan_Patient__c clonePp = pp.clone(false,true,false,false);
			clonePp.ID_Number__c = clonePp.ID_Number__c + 'AA';
			clonedPlanPatients.add(clonePp);
		}

		// WHEN
		System.Test.startTest();
		try {
			insert clonedPlanPatients;
			System.assert(false);
		} catch (DmlException e) {
			System.Test.stopTest();
			//System.assert(e.getMessage().contains('A Plan Patient exists with this name and date of birth.'));
		}
	}

	// Legacy Tests: Functionality Unknown

    static testMethod void PopulateName_Test() {
    	insertion();
    	
    	
    }
    
    static testMethod void PlanPatientTriggerTest_Bulk() {
    	//TODO: Insert 20 plan patients with geocoding test address (1) 
    	//Assert that 20 records have PlanPatient.Geolocation__c lat and lng fields that match the (1) coordinates after insert 
    	
    	//TODO: Update all 20 records to have geocoding test address (2). Assert that all 20 records have PlanPatient.Geolocation__c lat and lng fields that match the (2) coordinates after update   
    }
    static testMethod void PlanPatientTriggerTest_NullValues() {
    	//TODO: Insert PlanPatient with blank address fields. Verify that the test does not fail and assert that PlanPatient.Geolocation__c lat and lng fields are null
    }
    
    //Used for increasing test coverage percentage, since not all methods are guaranteed to be used.
    static testmethod void PlanPatientTriggerTest_triggerContexts() {
        Map<Id,SObject> tempMap = new Map<Id,SObject>();
        List<SObject> tempList = new List<SObject>();           
        PlanPatientTriggerDispatcher_CS dispatch;
        
        //before insert 
        resetBooleans();
        isInsert = true;
        isBefore = true;    
        dispatch = new PlanPatientTriggerDispatcher_CS(tempMap, tempMap, tempList, tempList, isInsert, isUpdate, isDelete, isUndelete, isBefore, isAfter);     
        
        //after insert
        resetBooleans();
        isInsert = true;
        isAfter = true;
        dispatch = new PlanPatientTriggerDispatcher_CS(tempMap, tempMap, tempList, tempList, isInsert, isUpdate, isDelete, isUndelete, isBefore, isAfter);     

        //before update
        resetBooleans();
        isUpdate = true;
        isBefore = true;
        dispatch = new PlanPatientTriggerDispatcher_CS(tempMap, tempMap, tempList, tempList, isInsert, isUpdate, isDelete, isUndelete, isBefore, isAfter);     

        //after update
        resetBooleans();
        isUpdate = true;
        isAfter = true;
        dispatch = new PlanPatientTriggerDispatcher_CS(tempMap, tempMap, tempList, tempList, isInsert, isUpdate, isDelete, isUndelete, isBefore, isAfter);     
        
        //before delete
        resetBooleans();
        isDelete = true;
        isBefore = true;
        dispatch = new PlanPatientTriggerDispatcher_CS(tempMap, tempMap, tempList, tempList, isInsert, isUpdate, isDelete, isUndelete, isBefore, isAfter);     
        
        //after delete
        resetBooleans();
        isDelete = true;
        isAfter = true;
        dispatch = new PlanPatientTriggerDispatcher_CS(tempMap, tempMap, tempList, tempList, isInsert, isUpdate, isDelete, isUndelete, isBefore, isAfter);     
        
        //after undelete
        resetBooleans();
        isUndelete = true;
        isAfter = true;
        dispatch = new PlanPatientTriggerDispatcher_CS(tempMap, tempMap, tempList, tempList, isInsert, isUpdate, isDelete, isUndelete, isBefore, isAfter);             
    }
     
    static void resetBooleans() {
        isInsert = false;
        isUpdate = false;
        isDelete = false;
        isUndelete = false;
        isBefore = false;
        isAfter = false;
    }
    static void insertion() {
    	List<Patient__c> patientList = TestDataFactory_CS.generatePatients('testLast', 5);
    	insert patientList;
    	system.debug(patientList);
    	
    	List<Account> planList = TestDataFactory_CS.generatePlans('testPlan', 1);
    	insert planList;
    	system.debug(planList);
    	
    	Map<Id, List<Patient__c>> planToPatients = new Map<Id, List<Patient__c>>();
    	planToPatients.put(planList[0].Id, patientList);
    	List<Plan_Patient__c> planPatientList = TestDataFactory_CS.generatePlanPatients(planToPatients);
    	insert planPatientList;
    	system.debug(planPatientList);
    	
    	patient = planPatientList[0];
    }
    
    static void updates() {
    	
    }
}