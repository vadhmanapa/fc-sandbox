global class RecordTypes {

    //Helper functions
    public static Id getDefaultRecordType(Map<String,Schema.RecordTypeInfo> recordTypes) {
        
        for(String name : recordTypes.keySet()) {
            if(recordTypes.get(name).isDefaultRecordTypeMapping()) {
                return recordTypes.get(name).getRecordTypeId();
            }
        }
        System.debug(LoggingLevel.ERROR,'No default Case record type!');
        return null;
    }

    public static Map<String,Schema.RecordTypeInfo> getAvailableRecordTypes(Map<String,Schema.RecordTypeInfo> recordTypes) {
        Map<String,Schema.RecordTypeInfo> availableTypes = new Map<String,Schema.RecordTypeInfo>();
        
        for(String name : recordTypes.keySet()) {
            if(recordTypes.get(name).isAvailable()) {
                availableTypes.put(name,recordTypes.get(name));
            }
        }
        
        //If there is more than the master class, remove the master class
        if(availableTypes.size() > 1 && availableTypes.containsKey('Master')){
            availableTypes.remove('Master');
        }
        
        return availableTypes;
    }

    public static Map<Id,RecordType> recordTypesById {
        get {
            if(recordTypesById == null) {
                recordTypesById = new Map<Id,RecordType>([
                    SELECT
                        Id,
                        Name,   
                        DeveloperName,
                        SobjectType
                    FROM RecordType
                    WHERE IsActive = true
                ]);
            }
            return recordTypesById;
        }
        private set;
    }

    //Accounts
    public static final Map<String,Schema.RecordTypeInfo> accountTypes = Schema.SObjectType.Account.getRecordTypeInfosByName();
    public static final Id providerAccountId = (accountTypes.containsKey('Integra Provider') ? accountTypes.get('Integra Provider').getRecordTypeId() : null);
    public static final Id payorAccountId = (accountTypes.containsKey('Health Plan') ? accountTypes.get('Health Plan').getRecordTypeId() : null);
    public static Id defaultAccountId { get { return getDefaultRecordType(accountTypes); } }
    public static Map<String,Schema.RecordTypeInfo> availableAccountTypes { get { return getAvailableRecordTypes(accountTypes); } }
    
    //Contacts
    public static final Map<String,Schema.RecordTypeInfo> contactTypes = Schema.SObjectType.Contact.getRecordTypeInfosByName();
    public static final Id patientId = (contactTypes.containsKey('Patient') ? contactTypes.get('Patient').getRecordTypeId() : null);
    public static final Id payorContactId = (contactTypes.containsKey('Payor') ? contactTypes.get('Payor').getRecordTypeId() : null);
    public static final Id providerContactId = (contactTypes.containsKey('Provider') ? contactTypes.get('Provider').getRecordTypeId() : null);
    public static final Id doctorId = (contactTypes.containsKey('Hospital/DR') ? contactTypes.get('Hospital/DR').getRecordTypeId() : null);
    public static final Id payerRelationsId = (contactTypes.containsKey('Payer Relations') ? contactTypes.get('Payer Relations').getRecordTypeId() : null);
    public static final Id otherId = (contactTypes.containsKey('Other') ? contactTypes.get('Other').getRecordTypeId() : null);
    public static final Id familyId = (contactTypes.containsKey('Family') ? contactTypes.get('Family').getRecordTypeId() : null);
    public static Id defaultContactId { get { return getDefaultRecordType(contactTypes); } }
    public static Map<String,Schema.RecordTypeInfo> availableContactTypes { get { return getAvailableRecordTypes(contactTypes); } }
    
    //Call Logs
    public static final Map<String,Schema.RecordTypeInfo> callLogTypes = Schema.SObjectType.Call_Log__c.getRecordTypeInfosByName();
    public static final Id claimsSupport = (callLogTypes.containsKey('Claims Support') ? callLogTypes.get('Claims Support').getRecordTypeId() : null);
    public static final Id customerSupport = (callLogTypes.containsKey('Customer Support') ? callLogTypes.get('Customer Support').getRecordTypeId() : null);
    public static Id defaultCallLogId { get { return getDefaultRecordType(callLogTypes); } }
    public static Map<String,Schema.RecordTypeInfo> availableCallLogTypes { get { return getAvailableRecordTypes(callLogTypes); } }
    
    //Cases
    public static final Map<String,Schema.RecordTypeInfo> caseTypes = Schema.SObjectType.Case.getRecordTypeInfosByName();
    public static final Id standardCaseId = (caseTypes.containsKey('Standard') ? caseTypes.get('Standard').getRecordTypeId() : null);
    public static final Id complaintId = (caseTypes.containsKey('Complaint') ? caseTypes.get('Complaint').getRecordTypeId() : null);
    public static final Id queId = (caseTypes.containsKey('Que') ? caseTypes.get('Que').getRecordTypeId() : null);
    public static final Id emailId = (caseTypes.containsKey('Email') ? caseTypes.get('Email').getRecordTypeId() : null);
    public static final Id customerCallId = (caseTypes.containsKey('Customer Service Call') ? caseTypes.get('Customer Service Call').getRecordTypeId() : null);
    public static final Id claimsInquiryId = (caseTypes.containsKey('Claims Inquiry') ? caseTypes.get('Claims Inquiry').getRecordTypeId() : null);
    public static final Id clearHelpId = (caseTypes.containsKey('ClearHelp Email') ? caseTypes.get('ClearHelp Email').getRecordTypeId() : null);
    public static final Id claimsEmailId = (caseTypes.containsKey('Claims Email') ? caseTypes.get('Claims Email').getRecordTypeId() : null);
    public static Id defaultCaseId { get { return getDefaultRecordType(caseTypes); } }
    public static Map<String,Schema.RecordTypeInfo> availableCaseTypes { get { return getAvailableRecordTypes(caseTypes); } }

    //Additional Details
    public static final Map<String,Schema.RecordTypeInfo> additionalDetailTypes = Schema.SObjectType.AddCaseDetails__c.getRecordTypeInfosByName();
    public static final Id authorizationNumber = (additionalDetailTypes.containsKey('Authorization Number') ? additionalDetailTypes.get('Authorization Number').getRecordTypeId() : null);
    public static final Id hcpcCode = (additionalDetailTypes.containsKey('HCPC Code') ? additionalDetailTypes.get('HCPC Code').getRecordTypeId() : null);
    public static final Id orderNumber = (additionalDetailTypes.containsKey('Order Number') ? additionalDetailTypes.get('Order Number').getRecordTypeId() : null);
    public static final Id queClaimId = (additionalDetailTypes.containsKey('Que Claim ID') ? additionalDetailTypes.get('Que Claim ID').getRecordTypeId() : null);
    public static final Id refProvider = (additionalDetailTypes.containsKey('Referred Provider') ? additionalDetailTypes.get('Referred Provider').getRecordTypeId() : null);
    public static final Id zPaperRefNum = (additionalDetailTypes.containsKey('zPaper Reference Number') ? additionalDetailTypes.get('zPaper Reference Number').getRecordTypeId() : null);
    public static final Id servicingProvider = (additionalDetailTypes.containsKey('Current Servicing Provider') ? additionalDetailTypes.get('Current Servicing Provider').getRecordTypeId() : null);
    public static final Id routedReferralProvider = (additionalDetailTypes.containsKey('Referral Rerouted to New Provider') ? additionalDetailTypes.get('Referral Rerouted to New Provider').getRecordTypeId() : null);
    public static Id defaultDetailId { get { return getDefaultRecordType(additionalDetailTypes); } }
    public static Map<String,Schema.RecordTypeInfo> availableDetailTypes { get { return getAvailableRecordTypes(additionalDetailTypes); } }
    
}