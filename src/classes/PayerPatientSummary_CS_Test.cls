@isTest
private class PayerPatientSummary_CS_Test {

	/******* Provider *******/
	static private List<Account> providers;
	static private List<Contact> providerUsers;
	
	/******* Payer *******/
	static private List<Account> payers;
	static private List<Contact> payerUsers;
	
	/******* Patients *******/
	static private List<Patient__c> patients;
	static private List<Plan_Patient__c> planPatients;
	
	/******* Test Objects *******/
	static private PayerPatientSummary_CS newPatientSummary;
	static private Order__c testOrder;
	static private Plan_Patient__c testPatient;

    //Add a Test Patient to order history
    static testMethod void testPatientWithID() {
        
        init();
		createTestObjects();
        
        Integer orderCount = [select count() from Order__c];
        System.debug('# of orders in db: ' + orderCount);
        
		ApexPages.currentPage().getParameters().put('pid', testPatient.Id);
			        
        newPatientSummary.selectedOrderId = testOrder.Id;
        newPatientSummary.init();
        system.assert(newPatientSummary.orderHistory.size() == 1, 'Order History Size: ' + newPatientSummary.orderHistory.size());        
    }
    
    //Pass a null Id into 
    static testMethod void PassNullId(){
    	
    	init();
    	createTestObjects();
    	
    	ApexPages.currentPage().getParameters().put('pid', null);
			        
        newPatientSummary.selectedOrderId = testOrder.Id;
        newPatientSummary.init();
    	
    }
    
    //Create a patient and order for testing purposes
    static public void createTestObjects(){
    	
    	//Test Patient with required information
    	testPatient = new Plan_Patient__c();
    	testPatient.First_Name__c = 'A';
    	testPatient.Last_Name__c = 'B';
    	testPatient.Email_Address__c = 'C@email.com';
    	testPatient.Date_of_Birth__c = Date.newInstance(1990, 1, 1);
    	testPatient.Street__c = '123 E Street';
    	testPatient.Apartment_Number__c = '1026';
    	testPatient.City__c = 'New York';
    	testPatient.State__c = 'NY';
    	testPatient.Zip_Code__c = '00000';
    	testPatient.Phone_Number__c ='6029547148';
    	testPatient.Cell_Phone_Number__c = '4804154399';
    	testPatient.Native_Language__c = 'English';
    	testPatient.ID_Number__c = '1';
    	testPatient.Medicare_ID__c = '2';
    	testPatient.Medicaid_ID__c = '3';
    	testPatient.Medicare_Primary_Patient__c = true;
    	testPatient.Health_Plan__c = payers[0].Id;
    	insert testPatient;
    	
    	//Test Order for Test Patient 
    	testOrder = new Order__c();
    	testOrder.Plan_Patient__c = testPatient.Id;   	
    	insert testOrder;
    	
    }
    
    
    static public void init(){
    	
    	///////////////////////////////////
		// Data
		///////////////////////////////////
		
		//Payers
		payers = TestDataFactory_CS.generatePlans('TestPayer', 1);
		insert payers;
		
		payerUsers = TestDataFactory_CS.createPlanContacts('TestPlan', payers, 1);

		//Patients
		patients = TestDataFactory_CS.generatePatients('TestPatient', 1);
		insert patients;

		///////////////////////////////////
		// Page Landing
		///////////////////////////////////

		//Set the page and authentication cookie
		Test.setCurrentPage(Page.PayerPatientSummary_CS.setRedirect(true));
		
		//Log the desired user in
		TestServices_CS.login(payerUsers[0]);
    	
    	//Make Create New Patient Page
    	newPatientSummary = new PayerPatientSummary_CS();
    } 
    
}