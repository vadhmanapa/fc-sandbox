public with sharing class LookUpRollUpUtils_IP {
	/********** Referring to Code by Abhinav Gupta ----> https://github.com/abhinavguptas/Salesforce-Lookup-Rollup-Summaries***********/

	public LookUpRollUpUtils_IP() {
		
	}

	public void rollUpAction(List<Provider_Recoupment_Tracker__c> getRecoups){

		Provider_Recoupment_Tracker__c[] tracker = getRecoups;

		 /*
      First step is to create a context for LREngineForLookUp_IP, by specifying parent and child objects and
      lookup relationship field name
     */
     LREngineForLookUp_IP.Context ctx = new LREngineForLookUp_IP.Context(Recoupment__c.SobjectType, // parent object
                                            Provider_Recoupment_Tracker__c.SobjectType,  // child object
                                            Schema.SObjectType.Provider_Recoupment_Tracker__c.fields.Refund_and_Recoupment__c // relationship field name
                                            );

     ctx.add(new LREngineForLookUp_IP.RollupSummaryField(
                                            Schema.SObjectType.Recoupment__c.fields.Total_Recouped_from_Provider__c,
                                            Schema.SObjectType.Provider_Recoupment_Tracker__c.fields.Amount_Recouped__c,
                                            LREngineForLookUp_IP.RollupOperation.Sum 
                                         ));

     /* 
      Calling rollup method returns in memory master objects with aggregated values in them. 
      Please note these master records are not persisted back, so that client gets a chance 
      to post process them after rollup
      */ 
     Sobject[] masters = LREngineForLookUp_IP.rollUp(ctx, tracker);

     // Persiste the changes in master
     update masters;

	}
}