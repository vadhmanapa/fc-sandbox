/**
 *  @Description Dispatcher class to manage process for triggers on Provider_Catgeory__c
 *  @Author Cloud Software LLC, Steven Batchelor
 *  Revision History: 
 *		2016-02-19 - sbatchlor - Created file.
 *
 */
public class ProviderCategoryTriggerDispatcher_CS {
	public ProviderCategoryTriggerDispatcher_CS(Map<Id, SObject> oldMap, Map<Id, SObject> newMap, List<SObject> triggerOld, List<SObject> triggerNew, Boolean isInsert, Boolean isUpdate, Boolean isDelete, Boolean isUndelete, Boolean isBefore, Boolean isAfter) {
		if (isBefore) {
			if (isInsert) { doBeforeInsert(triggerNew); }
			else if (isUpdate) { doBeforeUpdate(oldMap, newMap, triggerOld, triggerNew); }
			else if (isDelete) { doBeforeDelete(oldMap, triggerOld); }
		} else if (isAfter) {
			if (isInsert) { doAfterInsert(newMap, triggerNew); }
			else if (isUpdate) { doAfterUpdate(oldMap, newMap, triggerOld, triggerNew); }
			else if (isDelete) { doAfterDelete(oldMap, triggerold); }
			else if (isUndelete) { doAfterUnDelete(newMap, triggerNew); }
		}
	}

	public void doBeforeInsert(List<SObject> triggerNew) {

	}

	public void doBeforeUpdate(Map<Id, SObject> oldMap, Map<Id, SObject> newMap, List<SObject> triggerOld, List<SObject> triggerNew) {

	}

	public void doBeforeDelete(Map<Id, SObject> oldMap, List<SObject> triggerOld) {

	}

	public void doAfterInsert(Map<Id, SObject> newMap, List<SObject> triggerNew) {
		updateKeywords(triggerNew);
	}

	public void doAfterUpdate(Map<Id, SObject> oldMap, Map<Id, SObject> newMap, List<SObject> triggerOld, List<SObject> triggerNew) {
		updateKeywords(triggerNew);
	}

	public void doAfterDelete(Map<Id, SObject> oldMap, List<SObject> triggerOld) {
		updateKeywords(triggerOld);
	}

	public void doAfterUndelete(Map<Id, SObject> newMap, List<SObject> triggerNew) {

	}

	/**
	* @description Method will re-generate the hidden keyword list on provider Accounts 
	*	based on the current Provider_Category__c objects.
	*/
	private void updateKeywords(List<Provider_Category__c> triggerProviderCategoryList) {

		// Get all the Accounts related to the objects that have been updated
		Map<Id, List<Provider_Category__c>> providerToCategoryMap = 
			new Map<Id, List<Provider_Category__c>> ();

		for (Provider_Category__c providerCategory : triggerProviderCategoryList) {
			providerToCategoryMap.put(providerCategory.Provider__c,
				new List<Provider_Category__c> ());
		}

		// Get all the child Provider_Category__c for each Account
		List<Provider_Category__c> providerCategoryList = [
			SELECT Name, Provider__c
			FROM Provider_Category__c
			WHERE Provider__c
			IN : providerToCategoryMap.keySet()
		];

		for (Provider_Category__c providerCategory : providerCategoryList) {
			providerToCategoryMap.get(providerCategory.Provider__c).add(providerCategory);
		}

		List<Account> acctKeywordsToUpdateList = new List<Account>();
		for (Id providerId : providerToCategoryMap.keySet()) {
			Account acct = new Account(Id = providerId);
			acct.Keywords__c = '';

			for (Provider_Category__c providerCategory : providerToCategoryMap.get(acct.Id)) {
				acct.Keywords__c += providerCategory.Name + ', ';
			}
			acct.Keywords__c = acct.Keywords__c.removeEnd(', ');

			acctKeywordsToUpdateList.add(acct);
		}

		update acctKeywordsToUpdateList;
	}
}