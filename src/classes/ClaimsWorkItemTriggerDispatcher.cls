public with sharing class ClaimsWorkItemTriggerDispatcher {

	public static Integer runCount = 0;
	
	public static void afterUpdate(List<ARAG__C> nList, Map<Id,ARAG__c> oldMap) {
		checkClaimTouches(nList,oldMap);
	}

	public static Boolean isTouched(ARAG__c newClaim, ARAG__c oldClaim) {
		
		if(oldClaim.Status__c != newClaim.Status__c) {
			system.debug(LoggingLevel.INFO,'Found status change: ' + oldClaim.Status__c + ' to ' + newClaim.Status__c);
			return true;
		}
		
		system.debug(LoggingLevel.INFO,'No status change.');
		return false;
	}

	public static void checkClaimTouches(List<ARAG__c> nList, Map<Id,ARAG__c> oldMap) {
	
		List<Claim_Work_Touch__c> toInsert = new List<Claim_Work_Touch__c>();
	
		system.debug(LoggingLevel.INFO,'**Creating claim touches**');
	
		if(runCount > 1) {
			system.debug('Only for first run...');
			return;
		}
	
		//**MAIN FOR LOOP: Processing claims**
		for(ARAG__c newClaim : nList) {
	
			system.debug(LoggingLevel.INFO,'Checking claim ' + newClaim.Name + ' for touches');
			
			ARAG__c oldClaim = oldMap.get(newClaim.Id);
			
			//Check for touch evidence
			if(!isTouched(newClaim,oldClaim)) {
				continue;
			}
			
			//Create new touch
			system.debug(LoggingLevel.INFO,'Creating new touch for ' + newClaim.Name);
			
			Claim_Work_Touch__c newTouch = new Claim_Work_Touch__c(
				Claims_Item__c = newClaim.Id, 
				From_Status__c = oldClaim.Status__c, 
				To_Status__c = newClaim.Status__c,
		        Touched_By__c = newClaim.LastModifiedById, 
		        Touched_On__c = System.now() 
		  	);
		  	
		  	toInsert.add(newTouch);
		}
		
		//NOTE TO VENKAT: Inserts should always be outside of 'for' loops
		//Insert new touches
		try {
			insert toInsert;
			System.debug(LoggingLevel.INFO,'Number of records to be attached:  ' + toInsert.size());
	    }catch(System.Dmlexception e) {
	        System.debug(LoggingLevel.ERROR,'Touch History not inserted. Job unsuccessful: ' + e.getMessage());
	    }
				
	}
}