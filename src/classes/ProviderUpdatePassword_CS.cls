public without sharing class ProviderUpdatePassword_CS extends ProviderPortalServices_CS {

	private String pageAccountType = 'Provider';

	/******* Page Interface *******/
	public String newPassword {get;set;}
	public String newPasswordConfirm {get;set;}	
	public String newPasswordMessage {get; private set;}
	
	/******* Constructor and Init *******/
	public ProviderUpdatePassword_CS(){}
	
	// Default page action, add additional page action functionality here
	public PageReference init(){

		///////////////////////////////////
		// Authentication and Permission
		///////////////////////////////////
		
		if(!isSecure && !Test.isRunningTest()){
			return DoLogout();
		}
				
		if(!authenticated){
			return DoLogout();
		}

		if(portalUser.AccountType != pageAccountType){
			return loginPage;
		}
		
		///////////////////////////////////
		// Additional init
		///////////////////////////////////
		
		newPasswordMessage = '';
		
		return null;
	}

	public PageReference changePassword(){
		
		if(portalUser != null){
			
			// Check data exists in prompts
			if(newPassword == null || newPasswordConfirm == null){
				System.debug('Error: No password data found');
				newPasswordMessage = 'Error: No password data found';
				return null;	
			}			
			
			// Check that new passwords match
			if(newPassword != newPasswordConfirm){
				System.debug('Error: New passwords don\'t match.');
				newPasswordMessage = 'Error: New passwords don\'t match.';
				return null;
			}
			
			// Finally change the password
			if(!portalUser.changePassword(newPassword)){
				System.debug('Error: Password change failed for ' + portalUser.instance.Username__c);
				newPasswordMessage = 'Error: Password change failed.';
				return null;
			}
			
			// With a new salt, the sessionId cookie needs updated
			updateAuthentication();
			
			// Set the user as password changed
			portalUser.instance.Password_Reset__c = false;
			portalUser.instance.Password_Set_Date__c = DateTime.now();
			
			update portalUser.instance;
			
			System.debug('Password successfully changed for ' + portalUser.instance.Username__c);
			newPasswordMessage = 'Password successfully changed';
		}
		else{
			System.debug('Error: No user context.');
			return DoLogout();
		}
		
		return homepage;
	}

}