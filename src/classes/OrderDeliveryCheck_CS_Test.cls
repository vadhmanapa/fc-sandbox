@isTest
public class OrderDeliveryCheck_CS_Test{
    static Order__c recurringOrder;
    
    static testMethod void OrderDeliveryCheck_Test() {
        Date d = system.today().addDays(3);
        Datetime delWindBegin = DateTime.newInstance(d.year(), d.month(), d.day(), 23, 59, 59);
        
        //Not adding type of provider to avoid future calls
        Account provider = new Account(Name = 'Provider');
        insert provider;
        
        initOrders(provider.Id, delWindBegin);
        
        OrderDeliveryCheck_CS odc = new OrderDeliveryCheck_CS();

		// Runs batch 
		System.Test.startTest();
		Database.executeBatch(odc);
		System.Test.stopTest();

        recurringOrder = [SELECT Status__c, Provider__c FROM Order__c WHERE Id = :recurringOrder.Id];
		
		// Assert that recurringOrder's status is equal to 'Provider Assigned - Auto'
        system.assertEquals(OrderModel_CS.STATUS_PROVIDER_ASSIGNED_AUTO, recurringOrder.Status__c);
		// Assert that recurringOrder's provider Id is equal to provider.Id
        system.assertEquals(provider.Id, recurringOrder.Provider__c);
    }    
    static testMethod void OrderDeliveryCheck_NoParentProvider() {
        Date d = system.today().addDays(3);
        Datetime delWindBegin = DateTime.newInstance(d.year(), d.month(), d.day(), 23, 59, 59);
        
        initOrders(null, delWindBegin);
        
        OrderDeliveryCheck_CS odc = new OrderDeliveryCheck_CS();

		// Runs batch
		System.Test.startTest();
		Database.executeBatch(odc);
		System.Test.stopTest();

        recurringOrder = [SELECT Status__c, Provider__c FROM Order__c WHERE Id = :recurringOrder.Id];

		// Asserts that recurringOrder's status to equal to 'Manually Assign Provider'
        system.assertEquals(OrderModel_CS.STATUS_MANUALLY_ASSIGN_PROVIDER, recurringOrder.Status__c);
        //Notice: If assignment process is updated, this assert may fail due to order trigger automatically assigning the record
        system.assertEquals(null, recurringOrder.Provider__c);
    }
    static testMethod void OrderDeliveryCheck_OutOfTimeWindow() {
        Date d = system.today().addDays(4);
        Datetime delWindBegin = DateTime.newInstance(d.year(), d.month(), d.day(), 0, 0, 0);
        
        Account provider = new Account(
        	Name = 'Provider',
            Type__c = 'Provider'
        );
        insert provider;
        
        initOrders(provider.Id, delWindBegin);
        
        OrderDeliveryCheck_CS odc = new OrderDeliveryCheck_CS();

		// Runs batch
		System.Test.startTest();
		Database.executeBatch(odc);
		System.Test.stopTest();

        recurringOrder = [SELECT Status__c, Provider__c FROM Order__c WHERE Id = :recurringOrder.Id];

		// Assert that recurringOrder's status is equal to 'Recurring'
		system.assertEquals(OrderModel_CS.STATUS_RECURRING, recurringOrder.Status__c);
		system.assertEquals(null, recurringOrder.Provider__c);        
    }
    static testMethod void OrderDeliverySchedule(){
    	OrderDeliverySchedule_CS ods = new OrderDeliverySchedule_CS();
    	String sch = '0 0 8 ? * L';
    	system.schedule('Test Job', sch, ods);
    }
	
    static void initOrders(Id providerId, datetime delWindBegin) {            
        Order__c parentOrder = new Order__c(Provider__c = providerId);
        insert parentOrder;
                
        recurringOrder = new Order__c(
            Status__c = OrderModel_CS.STATUS_RECURRING,
            Delivery_Window_Begin__c = delWindBegin,
            Original_Order__c = parentOrder.Id
        );
        insert recurringOrder;
    }
}