public without sharing class ProviderNotificationServices_CS {

	public static Boolean sendNotification(Messaging.SingleEmailMessage[] notifications){
		
		Boolean success = true;
		Messaging.Sendemailresult[] emailResults;

		OrgWideEmailAddress[] fromAddress = [select Id from OrgWideEmailAddress where Address = 'clearhelp@accessintegra.com'];
	
		if(fromAddress.size() == 1){
			for(Messaging.SingleEmailMessage n : notifications){
				n.setOrgWideEmailAddressId(fromAddress[0].Id);
			}
		}
		else{
			System.debug('sendNotification Error: There are ' + fromAddress.size() + ' addresses with \'clearhelp@accessintegra.com\'');
		}
		
		try{
			emailResults = Messaging.sendEmail(notifications);
		}
		catch(Exception e){
			System.debug(e.getMessage());
			emailResults = new Messaging.Sendemailresult[]{};
		}
		
		for(Messaging.Sendemailresult r : emailResults){		
			if(!r.isSuccess()){
				String errorMessage = 'notifyAssignedOrder Errors:\n';
				
				for(Messaging.Sendemailerror e : r.getErrors()){
					errorMessage += e.getMessage() + '\t\n';
				}
				
				System.debug(errorMessage);

				success = false;
			}
		}
		
		return success;
	}

	public static Boolean notifyAssignedOrder(Order__c o){
		List<EmailTemplate> template = [select Id from EmailTemplate where Name = :'Provider Notification Assigned Order' limit 1];
		List<Contact> user = [select Id,Email,Name from Contact where Id = :o.Accepted_By__c limit 1];
		
		if(template.size() == 1){
			if(user.size() == 1){
				Messaging.Singleemailmessage notification = new Messaging.Singleemailmessage();
				
				//Use an email template
				notification.setTemplateId(template[0].Id);
				notification.setTargetObjectId(user[0].Id);
				notification.setWhatId(o.Id);
				
				return sendNotification(new Messaging.SingleEmailMessage[]{notification});
			}
			else{
				System.debug('notifyAssignedOrder Error: No user to email.');
			}
		}
		else{
			System.debug('notifyAssignedOrder Error: No existing template.');
		}
		
		return false;
	}
	
	public static Boolean notifyPayerCanceledOrder(Order__c o){
		List<EmailTemplate> template = [select Id from EmailTemplate where Name = :'Provider Notification Payer Canceled Order' limit 1];
		List<Contact> user = [select Id,Email,Name from Contact where Id = :o.Accepted_By__c limit 1];
				
		if(template.size() == 1){
			if(user.size() == 1){
				Messaging.Singleemailmessage notification = new Messaging.Singleemailmessage();
				
				System.debug('UserId: ' + user[0].Id);
				System.debug('User Email: ' + user[0].Email);
				
				//Use an email template
				notification.setTemplateId(template[0].Id);
				notification.setTargetObjectId(user[0].Id);
				notification.setWhatId(o.Id);
				
				return sendNotification(new Messaging.SingleEmailMessage[]{notification});
			}
			else{
				System.debug('notifyPayerCanceledOrder Error: No user to email.');
			}
		}
		else{
			System.debug('notifyPayerCanceledOrder Error: No existing template.');
		}
		
		return false;
	}
	
	public static Boolean notifyOrderNeedsAttention(Order__c o){
		List<EmailTemplate> template = [select Id from EmailTemplate where Name = :'Provider Notification Order Needs Attention' limit 1];
		List<Contact> user = [select Id,Email,Name from Contact where Id = :o.Accepted_By__c limit 1];
			
		if(template.size() == 1){
			if(user.size() == 1){
				Messaging.Singleemailmessage notification = new Messaging.Singleemailmessage();
				
				//Use an email template
				notification.setTemplateId(template[0].Id);
				notification.setTargetObjectId(user[0].Id);
				notification.setWhatId(o.Id);
				
				return sendNotification(new Messaging.SingleEmailMessage[]{notification});
			}
			else{
				System.debug('noitifyOrderNeedsAttention Error: No user to email.');
			}
		}
		else{
			System.debug('noitifyOrderNeedsAttention Error: No existing template.');
		}
		
		return false;
	}
	
	public static Boolean notifyPasswordChange(Contact c){
		List<EmailTemplate> template = [select Id from EmailTemplate where Name = :'Provider Password Change' limit 1];
			
		if(template.size() == 1){
			if(c.Id != null && c.Email != null && c.Email != ''){
				Messaging.Singleemailmessage notification = new Messaging.Singleemailmessage();
				
				//Use an email template
				notification.setTemplateId(template[0].Id);
				notification.setTargetObjectId(c.Id);
				
				return sendNotification(new Messaging.SingleEmailMessage[]{notification});
			}
			else{
				System.debug('noitifyOrderNeedsAttention Error: No user to email.');
			}
		}
		else{
			System.debug('noitifyOrderNeedsAttention Error: No existing template.');
		}
		
		return false;
	}
}