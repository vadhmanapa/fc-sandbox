@isTest
private class CallLogRelationTriggerDispatcherTest {

    static testMethod void test() {
		Call_Log__c testLog = new Call_Log__c();
		insert testLog;
		
		Contact testContact = new Contact(
			FirstName = 'Test',
			LastName = 'Contact'
		);
		insert testContact;
		
		Call_Log_Relation__c relation = new Call_Log_Relation__c(
			Call_Log__c = testLog.Id,
			Contact__c = testContact.Id
		);
		insert relation;
		
		//Update relation
		relation = [
			SELECT
				Id,
				Call_Log__c,
				Contact__c
			FROM Call_Log_Relation__c
			WHERE Id = :relation.Id
		];
		
		system.assertEquals(testLog.Id,relation.Call_Log__c);
		system.assertEquals(testContact.Id,relation.Contact__c);
    }
}