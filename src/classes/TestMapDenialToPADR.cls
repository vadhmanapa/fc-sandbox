/**
 This is a test for Mapping from Denials to PADR
 */
@isTest
private class TestMapDenialToPADR {

   /* static testMethod void checkDenial() {
    	PADR__c testPADR = new PADR__c (Provider_Name_Text__c = 'Test Provider', Payor_Name__c = 'Test Health Plan', Bill_Number__c = '12345', 
    	                                                                   Procedure_Code_s__c = 'A6789');
         insert testPADR;
         String insertPADR = testPADR.Id;
        
        Denials__c testUpdateDenial = new Denials__c (Provider__c = 'Test Provider', Payor__c = 'Test Health Plan', Bill__c = '12345',
                                         Work_Denial__c = false, Line_Item__c = 'T4398');
        insert testUpdateDenial;
        String denyId = testUpdateDenial.Id;
        String billNum = testUpdateDenial.Bill__c;

        testUpdateDenial.Work_Denial__c = true;
        update testUpdateDenial;
        
        Denials__c searchDenial1 = [Select Id, Related_PADR__r.Id from Denials__c where Id =: denyId];

         String padrId = searchDenial1.Related_PADR__r.Id;

        List<PADR__c> denialLog = [ SELECT Id, Bill_Number__c FROM PADR__c WHERE Id =: padrId LIMIT 1];
        for( PADR__c log : denialLog ){
             System.assertEquals(1, denialLog.size(), 'The PADR was linked');
            System.assertEquals(billNum, log.Bill_Number__c, 'The denial was mapped to correct PADR');
        }
            
            //test for insert action
            
         Denials__c testClaim1 = new Denials__c (Provider__c = 'Test Provider', Payor__c = 'Test Health Plan', Bill__c = '6165',
                                         Work_Denial__c = false, Line_Item__c = 'A0002');
        insert testClaim1;
        String claimId1 = testClaim1.Id;
        String billNum1 = testClaim1.Bill__c;

        testClaim1.Work_Denial__c = true;
        update testClaim1;

        Denials__c searchClaim2 = [Select Id, Related_PADR__r.Id from Denials__c where Id =:claimId1];
        String padrId1 = searchClaim2.Related_PADR__r.Id;

        List<PADR__c> claimLog1 = [ SELECT Id, Bill_Number__c FROM PADR__c WHERE Id =: padrId1 LIMIT 1];
        for( PADR__c log1 : claimLog1 ){
             System.assertEquals(1, claimLog1.size(), 'The PADR was created');
             System.assertEquals(billNum1, log1.Bill_Number__c, 'The Denial was mapped to PADR');
        }
    }*/
}