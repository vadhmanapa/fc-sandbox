public without sharing class ProviderProfile_CS extends ProviderPortalServices_CS {

	private final String pageAccountType = 'Provider';

	/******* Page Permissions *******/
	public Boolean resetPasswordPermission {get{ return portalUser.role.Reset_Password__c; }}

	/******* Page Interface *******/	
	public String oldPassword {get;set;}
	public String newPassword {get;set;}
	public String newPasswordConfirm {get;set;}
	
	/******* Error Messages *******/
	public String usernameErrorMsg {get;set;}
	public final String nonUniqueUsernameError = 'Username already exists';
	public final String missingUsernameError = 'Please enter a unique username';

	public Boolean oldPasswordError {get;set;}
	public Boolean newPasswordError {get;set;}
	public Boolean newPasswordSuccess {get;set;}
	public String passwordMessage {get;set;}
	
	public final String invalidPassword = 'The password entered does not meet criteria, please revise.';
	public final String validPassword = 'The password has been successfully changed.';	
	public final String incorrectOldPassword = 'The password entered does not match records.';
	public final String mismatchedPasswords = 'The passwords do not match.';

	/******* New User Info *******/
	public String firstName {get;set;}
	public String lastName {get;set;}
	public String phone {get;set;}
	public String email {get;set;}
	public String username {get;set;}
	
	/******* Constructor and Init *******/
	public ProviderProfile_CS(){}
	
	// Default page action, add additional page action functionality here
	public PageReference init(){

		///////////////////////////////////
		// Authentication and Permission
		///////////////////////////////////
		
		if(!isSecure && !Test.isRunningTest()){
			return DoLogout();
		}
				
		if(!authenticated){
			return DoLogout();
		}

		if(portalUser.AccountType != pageAccountType){
			return homepage;
		}
		
		///////////////////////////////////
		// Additional init
		///////////////////////////////////
		
		firstName = portalUser.instance.FirstName;
		lastName = portalUser.instance.LastName;
		phone = portalUser.instance.Phone;
		email = portalUser.instance.Email;
		username = portalUser.instance.Username__c;	
		
		return null;
	}//End Init

	/********************* Page Buttons/Links  ****************************/		
	public void saveProfile(){
		
		//Reset the error message
		usernameErrorMsg = '';
		
		//Check for blank username
		if(String.isBlank(username)){
			usernameErrorMsg = missingUsernameError;
			System.debug('No username entered.');
			return;
		}
		
		//Check if username is unique
		if(!portalUser.checkUniqueUsername(username)){
			usernameErrorMsg = nonUniqueUsernameError;
			System.debug('Username: ' + username + ' is not unique.');
			return;
		}
		
		portalUser.instance.FirstName = firstName;		
		portalUser.instance.LastName = lastName;
		portalUser.instance.Phone = phone;
		portalUser.instance.Email = email;
		portalUser.instance.Username__c = username;
		
		System.debug('New user info: ' + portalUser.instance);
		
		update portalUser.instance;
	}
	
	public void changePassword(){
		
		// Reset error status/messages
		oldPasswordError = false;
		newPasswordError = false;
		newPasswordSuccess = false;
		passwordMessage = '';
		
		// Check data exists in prompts
		if(String.isBlank(oldPassword) || String.isBlank(newPassword) || String.isBlank(newPasswordConfirm)){
			System.debug('Error: No password data found');
			return;	
		}			
		
		// Check old password
		if(!portalUser.isCurrentPassword(oldPassword)){
			System.debug('Error: Old password incorrect.');
			passwordMessage = incorrectOldPassword;
			oldPasswordError = true;
			return;				
		}
		
		// Check that new passwords match
		if(newPassword != newPasswordConfirm){
			System.debug('Error: New passwords don\'t match.');
			passwordMessage = mismatchedPasswords;
			newPasswordError = true;
			return;
		}
		
		// Finally change the password
		if(!portalUser.changePassword(newPassword)){
			System.debug('Error: Password change failed for ' + portalUser.instance.Username__c);
			passwordMessage = invalidPassword;
			newPasswordError = true;
			return;
		}
		
		// With a new salt, the sessionId cookie needs updated
		updateAuthentication();
		
		System.debug('Password successfully changed for ' + portalUser.instance.Username__c);
		passwordMessage = validPassword;
		newPasswordSuccess = true;
	}
}