public virtual without sharing class UserModel_CS extends AbstractSObjectModel_CS { 

	//Object record
	public Contact instance { get{ if(instance == null){ instance = (Contact)record; } return instance; } set; }

	//TODO: Implementation was buggy, rework the password history
	private final Integer MAX_PAST_PASSWORDS = 3;
	/*
	*	Old passwords are stored as:
	*		0:	Old password 3
	*		1:	Old salt 3
	*		2:	Old password 2
	*		3:	Old salt 2
	*		4:	Old password 1
	*		5:	Old salt 1
	*/
	
	public static final Set<String> objectFields = new Set<String>{
		/* Standard Fields */
		'Id',
		'AccountId',
		'Email',
		'Name',
		'FirstName',
		'LastName',
		'Phone',
		/* Custom Fields */
		'Active__c',
		'Batch_ID__c',
		'Contact_Notes__c',
		'Status__c',	
		'Entity__c',
		'Entity__r.Name',
		'Entity__r.Type__c',
		'Id_Key__c',
		'Login_Attempts__c',
		'Password__c',
		'Password_History__c',
		'Password_Reset__c',
		'Password_Set_Date__c',
		'Payer_Account__c',
		'Phone_Extension__c',
		'Profile__c',
		'Provider_Account__c',
		'Role__c',
		'Salt__c',
		'Session__c',
		'Primary_Clear_Contact__c',
		'Notify_Accepted__c',
		'Notify_Cancelled__c',
		'Notify_Delivered__c',
		'Notify_Needs_Attention__c',
		'Notify_New_Order__c',
		'Notify_Password_Change__c',
		'Notify_Payer_Canceled__c',
		'Notify_Rejected__c',
		'Notify_Shipped__c',
		'TIMBASURVEYS__Survey__c',
		'Health_Plan_User_Id__c',
		'Username__c',
		'User_Type__c',
		/* Content Role Fields */
		'Content_Role__c'	
	};

	public static final Set<String> roleFields = new Set<String>{
		'Name',
		'Accept_Orders__c',
		'Approve_Recurring_Orders__c',
		'Cancel_Orders__c',
		'Change_Provider__c',
		'Create_Orders__c',
		'Create_Plan_Patients__c',
		'Create_Users__c',
		'Edit_Orders__c',
		'Edit_Plan_Patients__c',
		'Edit_Users__c',
		'Filter_by_User__c',
		'Mark_Orders_Future_DOS__c', // Sprint 2/13 - Added by Dima
		'Mark_Orders_Delivered__c',
		'Mark_Orders_Shipped__c',
		'Reject_Orders__c',
		'Reset_Password__c',
		'Reset_User_Name__c',
		'Super_User__c',
		'View_Other_Users_Orders__c',
		'View_Patients__c'
	};
	
	/******* Object Status *******/
	public Boolean updateError {get;set;}

	/******* Object Fields *******/		
	public String Id { get{ if(Id == null){ Id = instance.Id; } return Id; } set; }
	public String accountType { get{ if(AccountType == null){ AccountType = instance.Entity__r.Type__c; } return AccountType; } set;}
	public String accountName { get{ if(AccountName == null){ AccountName = instance.Entity__r.Name; } return AccountName; } set;}
	
	public Role__c role {
		get{
			if(role == null){
				SoqlBuilder query = new SoqlBuilder()
					.selectx(roleFields)
					.fromx('Role__c')
					.wherex(new FieldCondition('Id').equals(instance.Content_Role__c))
					.limitx(1);
					
				System.debug('role query: ' + query.toSoql());
					
				role = Database.query(query.toSoql());
			}
			
			return role;
		}
		private set;
	}
	
	public Boolean checkUniqueUsername(String username){
		
		Integer usernameCount = [
			select 
				count() 
			from Contact 
			where Username__c = :username and Id != :instance.Id];
		
		if(usernameCount == 0){
			return true;
		}		
		return false;
	}
	
	
	
	/******* Constructors *******/	

	public UserModel_CS(Contact c){
		super(c);
	}
	
	/******* Status Updates *******/
	public void deactivate(){
		
		instance.Active__c = false;
		
		//TODO: Legacy Data, yuck
		instance.Status__c = 'Inactive/Terminated';
		
		update instance;
	}

	/******* Communication *******/
	public Boolean sendUsernameTo(){
		if(instance != null){
			if(String.isNotBlank(instance.Username__c)){
				String subject = 'Username Recovery';
				String emailBody = 'Username: ' + instance.Username__c;
				
				if(sendEmailTo(subject,emailBody)){
					return true;
				}
				else{
					//TODO: Error message
					System.debug('sendUsernameTo Error: Failed to send email.');
					return false;
				}
			}
			else{
				//TODO: Error message for invalid username
				System.debug('sendUsernameTo Error: No username to send.');
				return false;
			}
		}
		else{
			System.debug('sendUsernameTo Error: No user context.');
		}
		
		return false;
	}

	public Boolean sendPasswordTo(String unhashedPassword){
		if(instance != null){
			if(String.isNotBlank(instance.Password__c) && String.isNotBlank(instance.Username__c)){
				String subject = 'Password Recovery';
				String emailBody = '';
				
				//TODO:REMOVE THIS LINE LATER
				emailBody += 'Username: ' + instance.Username__c + '<br/>';
				
				emailBody += 'Password: ' + unhashedPassword;
				
				if(sendEmailTo(subject,emailBody)){
					return true;
				}
				else{
					//TODO: Error message
					System.debug('sendPasswordTo Error: Failed to send email.');
					return false;
				}
			}
			else{
				//TODO: Error message
				System.debug('sendPasswordTo Error: No password to send.');
				return false;
			}
		}
		else{
			System.debug('sendPasswordTo Error: No user context.');
		}
		
		return false;
	}

	public Boolean sendEmailTo(String subject, String emailBody){
		
		if(instance != null){
			if(String.isNotBlank(instance.Email))
			{
				Messaging.Singleemailmessage mail = new Messaging.Singleemailmessage();
				mail.setToAddresses(new String[]{instance.Email});
				mail.setSubject(subject);
				mail.setHtmlBody(emailBody);
				
				return EmailServices_CS.sendEmail(new Messaging.Singleemailmessage[]{mail});
				
				/*
				List<Messaging.Sendemailresult> emailResults = Messaging.sendEmail(new Messaging.Singleemailmessage[]{mail});
				
				for(Messaging.Sendemailresult res : emailResults){
					
					// Check if the email failed
					if(!res.isSuccess()){
						String detailedErrorMsg = 'sendEmailTo: Email failed to send:\n';
						
						for(Messaging.SendEmailError e : res.getErrors()){
							detailedErrorMsg += '\t' + e.getMessage() + '\n';
						}
						
						System.debug(detailedErrorMsg);
						return false;
					}
				}
				
				System.debug('Email sent successfully');
				return true;
				*/
			}
			else{
				//TODO: Error message
				System.debug('sendEmailTo Error: Email address is invalid.');				
				return false;
			}
		}
		else{
			System.debug('sendEmailTo Error: No user context.');
		}
		
		return false;
	}
	
	/******* Password Handling *******/
	
	public Boolean isCurrentPassword(String password){
		if(instance.Password__c != null && instance.Password__c == PasswordServices_CS.hash(password, instance.Salt__c)){
			return true;
		}
		else{
			return false;
		}
	}
	
	public Boolean resetPassword(){		
		String newPassword = PasswordServices_CS.randomPassword(8);		
		return changePassword(newPassword);
	}
	
	public Boolean changePassword(String newPassword){
		
		// A new password requires a new salt
		String newSalt = PasswordServices_CS.randomSalt(12);
		
		if(PasswordServices_CS.validatePasswordRegex(newPassword)){
			
			//Update password parameters			
			instance.Salt__c = newSalt;
			instance.Password__c = PasswordServices_CS.hash(newPassword,newSalt);
			instance.Password_Set_Date__c = DateTime.now();
			
			//Update the user Id Key
			instance.Id_Key__c = PasswordServices_CS.hash(instance.Id,newSalt);
			
			update instance;
					
			return true;
		}
		else{
			return false;
		}
	}
	
	public void overridePassword(String newPassword){
		
		// A new password requires a new salt
		String newSalt = PasswordServices_CS.randomSalt(12);
		
		//Update password parameters			
		instance.Salt__c = newSalt;
		instance.Password__c = PasswordServices_CS.hash(newPassword,newSalt);
		instance.Password_Set_Date__c = DateTime.now();
		
		//Update the user Id Key
		instance.Id_Key__c = PasswordServices_CS.hash(instance.Id,newSalt);
		
		update instance;
	}
}