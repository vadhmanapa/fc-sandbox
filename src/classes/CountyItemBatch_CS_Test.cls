@isTest
public class CountyItemBatch_CS_Test { 

    public static Account testAccount;
    public static List<Provider_Locations__c> oldProvLocs;
    public static List<Provider_Location__c> newProvLoc;
    public static Provider_Location__c dummyProvLoc;
    public static County_Item__c countItem;

    static testmethod void CountyItemBatch_Test() {
        init();

        String query = 'SELECT Id, Provider_Location__c FROM County_Item__c';

        System.assertEquals(dummyProvLoc.Id, countItem.Provider_Location_New__c);

        Test.startTest();
        CountyItemBatch_CS batch = new CountyItemBatch_CS(query);
        Database.executeBatch(batch);
        Test.stopTest();

        County_Item__c ci = [SELECT Id, Provider_Location_New__c FROM County_Item__c WHERE Id =: countItem.Id];
        System.assertEquals(newProvLoc[0].Id, ci.Provider_Location_New__c);
    }

    static void init() {
        Global_Switches__c gs = new Global_Switches__c(
            Name = 'Defaults',
            Create_ProvLoc_Trigger_Off__c = true
        );
        insert gs;

        testAccount = TestDataFactory_CS.generateProviders('Test Account', 1)[0];
        insert testAccount;

        oldProvLocs = TestDataFactory_CS.generateOldProviderLocations(testAccount.Id, 1);
        oldProvLocs[0].Monday__c = 'Closed';
        oldProvLocs[0].Tuesday__c = '9:00am-5:00pm';
        oldProvLocs[0].Wednesday__c = '9:00AM-5:00PM';
        oldProvLocs[0].Thursday__c = ' 9am - 12:00pm';
        oldProvLocs[0].Friday__c = ' 9:00am - 12pm';
        oldProvLocs[0].Saturday__c = '9:30am-5:30pm';
        oldProvLocs[0].Sunday__c = '09am-05:00pm';

        insert oldProvLocs;

        newProvLoc = ProviderLocationTransferServices.transferDataToNewProviderLocationObject(oldProvLocs);

        dummyProvLoc = new Provider_Location__c();
        dummyProvLoc.Account__c = testAccount.Id;
        insert dummyProvLoc;

        US_State_County__c usc = new US_State_County__c();
        usc.State__c = 'New York';
        usc.State_Code__c = 'NY';
        insert usc;

        countItem = new County_Item__c();
        countItem.Provider_Location__c = oldProvLocs[0].Id;
        countItem.Provider_Location_New__c = dummyProvLoc.Id;
        countItem.US_State_County__c = usc.Id;
        insert countItem;
    }
}