@isTest
private class EmailMessageServicesTest {

	static Case cs;
	static String userEmail = 'test@example.com';

	static testMethod void test_checkToReopenCases(){

		//Insert case
		cs = new Case(
			Status = 'Closed'
		);
		insert cs;

		//Insert outgoing email message
		EmailMessage emOutgoing = new EmailMessage(
			ParentId = cs.Id,
			Incoming = false
		);
		insert emOutgoing;

		//Make sure case is still closed
		Case csQ = [SELECT Status, Owner.Email FROM Case WHERE Id = :cs.Id];
		System.assertEquals('Closed', csQ.Status);

		//Ensure owner does not have same email
		System.assertNotEquals(userEmail, csQ.Owner.Email, 'User and owner email match. Change one.');

		//Insert incoming email message from non-owner source
		EmailMessage emIncoming = new EmailMessage(
			ParentId = cs.Id,
			Incoming = true,
			FromAddress = userEmail
		);
		insert emIncoming;

		//Make sure case is reopened
		csQ = [SELECT Status FROM Case WHERE Id = :cs.Id];
		System.assertEquals(EmailMessageServices.REOPENED_STATUS, csQ.Status, 'Case was not reopend.');
	}

}