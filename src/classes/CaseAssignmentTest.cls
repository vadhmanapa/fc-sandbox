@isTest
private class CaseAssignmentTest {

	static testMethod void getCustomerServiceUserIds() {

		Set<Id> customerServiceUserIds = CaseAssignment.getCustomerServiceUserIds();

		system.assertEquals(customerServiceUserIds.size(), [
			SELECT Group.Name,UserOrGroupId
			FROM GroupMember
			WHERE UserOrGroupId IN :customerServiceUserIds
			AND Group.Name = :CaseAssignment.CUSTOMER_SERVICE_GROUP_NAME
		].size());
	}

	/* static testMethod void assignCaseOwners() {

		//Condition 1: User is Kristen Domenici
		System.debug(LoggingLevel.INFO,'Condition 1');
		Case condition1 = new Case(
			Subject = 'Test Case'
		);

		User u = [SELECT Id FROM User WHERE Username LIKE 'kdomenici@accessintegra.com%'];
		System.runAs(u){
			CaseAssignment.assignCaseOwner(condition1);
			System.assertEquals(u.Id,condition1.OwnerId);
		}

		//Condition 2: Complaint
		System.debug(LoggingLevel.INFO,'Condition 2');
		Case condition2 = new Case(
			Subject = 'Test Case',
			RecordTypeId = RecordTypes.complaintId
		);

		CaseAssignment.assignCaseOwner(condition2);
		System.assert(CaseAssignment.usernameMap.containsKey('krafael@integrah.com'));
		System.assertEquals(CaseAssignment.usernameMap.get('krafael@integrah.com').Id,
			condition2.OwnerId);

		//Condition 3: Que
		System.debug(LoggingLevel.INFO,'Condition 3');
		Case condition3 = new Case(
			Subject = 'Test Case',
			RecordTypeId = RecordTypes.queId
		);

		CaseAssignment.assignCaseOwner(condition3);
		System.assert(CaseAssignment.queueMap.containsKey('Que Support Inbox'));
		System.assertEquals(CaseAssignment.queueMap.get('Que Support Inbox').Id,
			condition3.OwnerId);

		//Condition 4: Email - Centerlight
		System.debug(LoggingLevel.INFO,'Condition 4');
		Case condition4 = new Case(
			Subject = 'Test Case',
			SuppliedEmail = 'test@centerlight.org',
			RecordTypeId = RecordTypes.emailId
		);

		CaseAssignment.assignCaseOwner(condition4);
		system.assert(CaseAssignment.usernameMap.containsKey('krafael@integrah.com'));
		system.assertEquals(CaseAssignment.usernameMap.get('krafael@integrah.com').Id,
			condition4.OwnerId);

		//Condition 4a: Email - Wellcare
		System.debug(LoggingLevel.INFO,'Condition 5');
		Case condition5 = new Case(
			Subject = 'Test Case',
			SuppliedEmail = 'test@wellcare.org',
			RecordTypeId = RecordTypes.emailId
		);

		CaseAssignment.assignCaseOwner(condition5);
		system.assert(CaseAssignment.usernameMap.containsKey('aseide@accessintegra.com'));
		system.assertEquals(CaseAssignment.usernameMap.get('aseide@accessintegra.com').Id,
			condition5.OwnerId); //--------------------------------------------------------------> By Venkat

		//Condition 4b: CompEmail - Villagecare
		System.debug(LoggingLevel.INFO,'Condition 6');
		Case condition6 = new Case(
			Subject = 'Test Case',
			SuppliedEmail = 'test@villagecare.org',
			RecordTypeId = RecordTypes.emailId
		);

		CaseAssignment.assignCaseOwner(condition6);
		system.assert(CaseAssignment.usernameMap.containsKey('krafael@integrah.com'));
		system.assertEquals(CaseAssignment.usernameMap.get('krafael@integrah.com').Id,
			condition6.OwnerId);

		//Condition 4c: Email - Secure-Emblemhealth
		System.debug(LoggingLevel.INFO,'Condition 7');
		Case condition7 = new Case(
			Subject = 'Test Case',
			SuppliedEmail = 'test@secure-emblemhealth.com',
			RecordTypeId = RecordTypes.emailId
		);

		CaseAssignment.assignCaseOwner(condition7);
		system.assert(CaseAssignment.usernameMap.containsKey('mnavon@accessintegra.com'));
		system.assertEquals(CaseAssignment.usernameMap.get('mnavon@accessintegra.com').Id,
			condition7.OwnerId);


		//Condition 4d: Email - Add Doctor
		System.debug(LoggingLevel.INFO,'Condition 8');
		Case condition8 = new Case(
			Subject = 'Test Add Doctor',
			RecordTypeId = RecordTypes.emailId
		);

		CaseAssignment.assignCaseOwner(condition8);
		//system.assert(CaseAssignment.usernameMap.containsKey('cadams@accessintegra.com'));
		//system.assertEquals(CaseAssignment.usernameMap.get('cadams@accessintegra.com').Id, condition8.OwnerId);
		system.assertNotEquals(null,condition8.OwnerId );

		//Condition 4d: Email - Referral
		System.debug(LoggingLevel.INFO,'Condition 9');
		Case condition9 = new Case(
			Subject = 'Test Referral',
			RecordTypeId = RecordTypes.emailId
		);

		CaseAssignment.assignCaseOwner(condition9);
		//system.assert(CaseAssignment.usernameMap.containsKey('cadams@accessintegra.com'));
		//system.assertEquals(CaseAssignment.usernameMap.get('cadams@accessintegra.com').Id, condition9.OwnerId);
		system.assertNotEquals(null,condition9.OwnerId );

		//Condition 4d: Email - Emblem
		System.debug(LoggingLevel.INFO,'Condition 10');
		Case condition10 = new Case(
			Subject = 'Test Emblem',
			RecordTypeId = RecordTypes.emailId
		);

		CaseAssignment.assignCaseOwner(condition10);
		system.assert(CaseAssignment.usernameMap.containsKey('kphommathep@accessintegra.com'));
		system.assertEquals(CaseAssignment.usernameMap.get('kphommathep@accessintegra.com').Id,
			condition10.OwnerId);
	}

	static testMethod void addDoctorMatch() {

		Case testCase1 = new Case(
			Subject = 'Please add doctor bones'
		);

		system.assertEquals(true, CaseAssignment.addDoctorMatch(testCase1));

		Case testCase2 = new Case(
			Subject = 'Please add someone named doctor bones'
		);

		system.assertEquals(true, CaseAssignment.addDoctorMatch(testCase2));

		Case testCase3 = new Case(
			Subject = 'Please add bones'
		);

		system.assertEquals(false, CaseAssignment.addDoctorMatch(testCase3));
	}

	static testMethod void referralMatch() {

		Case testCase1 = new Case(
			Subject = 'Here is a new doctor referral'
		);

		system.assertEquals(true, CaseAssignment.referralMatch(testCase1));

		Case testCase2 = new Case(
			Subject = 'I am referring a new doctor'
		);

		system.assertEquals(true, CaseAssignment.referralMatch(testCase2));

		Case testCase3 = new Case(
			Subject = 'I refer to him as doctor bones'
		);

		system.assertEquals(false, CaseAssignment.referralMatch(testCase3));
	}

	static testMethod void emblemMatch() {

		Case testCase1 = new Case(
			Subject = 'Here is an emblem case'
		);

		system.assertEquals(true, CaseAssignment.emblemMatch(testCase1));

		Case testCase2 = new Case(
			Subject = 'Test subject'
		);

		system.assertEquals(false, CaseAssignment.emblemMatch(testCase2));
	}*/ //--------------------------------------------------------------------------------------------> By Venkat 5/17/2015
}