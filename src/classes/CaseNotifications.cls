public with sharing class CaseNotifications {

	public class CaseNotificationException extends Exception {}

	public static String ADMIN_FROM_ADDRESS = 'administration@accessintegra.com';

	public static Integer DAYS_BEFORE_DUE = 1;
	
	public static final String CASE_DUE_NAME = 'Case Due Notification';
	public static final String CASE_ASSIGNED_NAME = 'Case Assigned';
	
	public static EmailTemplate dueDateTemplate {
		get {
			if(dueDateTemplate == null) {
				try {
					dueDateTemplate = [
						SELECT Id,Subject,Body
						FROM EmailTemplate 
						WHERE Name = :CASE_DUE_NAME 
						LIMIT 1];
				} catch(Exception e) {
					throw new CaseNotificationException('Cannot locate email template "' + CASE_DUE_NAME + '"');
				}
			}
			return dueDateTemplate;
		}
	}

	public static OrgWideEmailAddress adminAddress {
		get {
			if(adminAddress == null) {
				try {
					adminAddress = [
						SELECT Id
						FROM OrgWideEmailAddress 
						WHERE Address = :ADMIN_FROM_ADDRESS];
				} catch(Exception e) {
					throw new CaseNotificationException('Cannot locate org-wide email address "' + ADMIN_FROM_ADDRESS + '"');
				}
			}
			return adminAddress;
		}
	}

	public static void checkNotifications(List<Case> cases) {
		
		List<Messaging.Singleemailmessage> notifications = new List<Messaging.Singleemailmessage>();
		
		//Due date notification
		notifications.addAll(checkDueDate(cases));
		
		//Barrier for Tests
		if(Test.isRunningTest()) {
			return;
		}
		
		//Send notifications
		Messaging.sendEmail(notifications);
	}
	
	public static List<Messaging.Singleemailmessage> checkDueDate(List<Case> cases) {
		
		List<Messaging.Singleemailmessage> notifications = new List<Messaging.Singleemailmessage>();
		
		Set<Id> caseIds = new Set<Id>();
		
		for(Case c : cases) {
			
			if(c.Due_Date__c == null){
				continue;
			}
			
			Date notificationDate = c.Due_Date__c.addDays(-1*DAYS_BEFORE_DUE);
			
			if(Date.today() < notificationDate) {
				system.debug(LoggingLevel.INFO,'Case ' + c.CaseNumber + ' is not due yet.');
				continue;
			}
			
			system.debug(LoggingLevel.INFO,'Case ' + c.CaseNumber + ' is ready to be notified.');
			
			caseIds.add(c.Id);
		}
		
		return getDueDateNotifications(caseIds);
	}

	public static List<Messaging.Singleemailmessage> getDueDateNotifications(Set<Id> caseIds) {
		
		List<Messaging.Singleemailmessage> messages = new List<Messaging.Singleemailmessage>();
		
		//Check for template first
		if(dueDateTemplate == null){
			system.debug(LoggingLevel.ERROR,'No due date template found!');
			return null;
		}
		
		//Use the case record to edit the template, specifically for {!Case.Due_Date__c}
		for(Case c : [
			SELECT Id, CaseNumber, Due_Date__c, OwnerId, Owner.Name, Owner.Email
			FROM Case
			WHERE Id IN :caseIds
		]){
		
			Messaging.Singleemailmessage message = new Messaging.Singleemailmessage();
			
			system.debug(c.OwnerId + ' / ' + c.Owner.Name + ' / ' + c.Owner.Email);
			
			if(String.isBlank(c.Owner.Email)) {
				system.debug(LoggingLevel.WARN,'Case owner ' + c.Owner.Name + ' has no email address!');
				continue;
			}
		
			message.setToAddresses(new List<String>{c.Owner.Email});
			message.setOrgWideEmailAddressId(adminAddress.Id);
			
			//DO NOT assign the email template, we have to manually adjust the fields
			//message.setTemplateId(dueDateTemplate.Id);
			//message.setTargetObjectId(c.OwnerId);
			
			//REPLACE FIELDS IN TEMPLATE SUBJECT / BODY
			String subject = replaceTemplateFields(c,dueDateTemplate.Subject);
			String body = replaceTemplateFields(c,dueDateTemplate.Body);
			
			system.debug(subject);
			system.debug(body);
			
			//Add subject and body back in to message
			message.setSubject(subject);
			message.setHtmlBody(body);
			
			messages.add(message);
		}
		
		return messages;
	}

	public static String replaceTemplateFields(Case c, String input) {
		
		Map<String,String> caseFields = new Map<String,String>{
			'{!Case.CaseNumber}' => c.CaseNumber,
			'{!Case.Due_Date__c}' => String.valueOf(c.Due_Date__c)
		};	
		
		for(String key : caseFields.keySet()) {
			input = input.replace(key, caseFields.get(key));
		}
		
		return input;
	}

	/* REMOVED 2.6.14 
	 * -- NOT NEEDED --
	public static List<Messaging.Singleemailmessage> getAssignmentAlerts(Set<Id> caseIds) {
		
		List<Messaging.Singleemailmessage> messages = new List<Messaging.Singleemailmessage>();
		
		if(assignedTemplate == null){
			system.debug(LoggingLevel.ERROR,'No assignment template found!');
			return null;
		}
		
		for(Case c : [
			SELECT Id, OwnerId, Owner.Name, Owner.Email
			FROM Case
			WHERE Id IN :caseIds
		]){
			
			Messaging.Singleemailmessage message = new Messaging.Singleemailmessage();
			
			if(String.isBlank(c.Owner.Email)) {
				system.debug(LoggingLevel.WARN,'Case owner ' + c.Owner.Name + ' has no email address!');
				continue;
			}
			
			message.setSaveAsActivity(false);
			
			message.setToAddresses(new List<String>{c.Owner.Email});
			message.setOrgWideEmailAddressId(adminAddress.Id);
			
			message.setTemplateId(assignedTemplate.Id);
			message.setTargetObjectId(c.OwnerId);
			
			messages.add(message);
		}

		return messages;
	}
	*/
}