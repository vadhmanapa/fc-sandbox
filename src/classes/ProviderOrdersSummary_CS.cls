/**
 *  @Description A controller class for ProviderOrdersSummary_CS page
 *  @Author Cloud Software LLC
 *  Revision History:
 *		11/22/2016 - vadhmanapa - Sprint 11/21 - Added time zone offset to counter issue with times getting offseted due to daylight savings time.
 *		11/28/2016 - vadhmanapa - Sprint 11/28 - Changed the entity default from User to Payer, moved populateFilterBoxes() from init() to populateOrderList(),  change from o.Entered_By__c to o.Accepted_By__c
*												 so that userMap displays Accepted By users.
 */

public without sharing class ProviderOrdersSummary_CS extends ProviderPortalServices_CS {

	private ProviderOrderServices_CS prOrderServices;

	/******* Page Permissions *******/
    public Boolean filterByUserPermission {get{ return portalUser.role.Filter_by_User__c; }}

    /******* Order List *******/
    public Apexpages.Standardsetcontroller orderSetCon {get;set;}
    public List<Order__c> orderList
    {
        get{
            List<Order__c> ordList = new List<Order__c>();
            if(orderSetCon.getRecords() != null && orderSetCon.getRecords().size() != 0){
                ordList = (List<Order__c>) orderSetCon.getRecords();
            }
            return ordList;
        }
        set;
    }
    public String selectedOrderId {get;set;}

    /******* Filtering *******/
    // public String filterByStage {get{ return (filterByStage == null) ? '' : filterByStage; }set;}
    public String filterByStage {
        /*
        ** CP-80: Default Order Summary Table Sorting - SPRINT 3/20/2017 - ADDED BY DGARKUSHA
        */
        get {
            return (this.filterByStage == null) ? '' : this.filterByStage;
        }
        set {
            this.filterByStage = value;
            this.sortColumn = (this.filterByStage == 'Pending Acceptance') ? 'orderedDate' : null;
            this.sortOrder = null;
        }
    }
    public String filterBySearch {get{ return (filterBySearch == null) ? '' : filterBySearch; }set;}
    public String filterByTime {get{ return (filterByTime == null) ? '' : filterByTime; }set;}
    
    /**
     ** NOTE: STAGING FOR FUTURE RELEASE - SPRINT 12/12 - BY VK
    **/
    
    public String filterBySubStage {get{return (filterBySubStage == null) ? '' : filterBySubStage;}set;} // added by VK on 01-05-2015 for Sprint 12/12

    /******* For Filtering by User/Account *******/
    public List<SelectOption> filterByEntityTypeOptions {
    	get{
    		return new List<SelectOption>{
    			new SelectOption('User','User'),
    			new SelectOption('Payer','Health Plan')
    		};
    	}
    	set;
    }
    public String filterByEntityType {
    	get{
    		return (filterByEntityType == null) ? 'Payer' : filterByEntityType; // edited by VK on 11/28/2016 to make Payer default value
    	}
    	set;
    }

    public List<SelectOption> filterByUserOptions {get;set;}
    public String filterByUser {get{ return (filterByUser == null) ? '' : filterByUser; }set;}

    public List<SelectOption> filterByPayerOptions {get;set;}
    public String filterByPayer {get{ return (filterByPayer == null) ? '' : filterByPayer; }set;}

    /************ Interface *************/
    public Integer pageSize
    {
    	get{
    		if (pageSize == null){ pageSize = 10; }
    		return pageSize;
    	}
    	set{
    		pageSize = value;
    		if(orderSetCon != null){ orderSetCon.setPageSize(value); }
    	}
    }

	public Integer totalPages {
    	get{
    		return (Integer)Math.ceil((Decimal)orderSetCon.getResultSize() / orderSetCon.getPageSize());
    	}
    	set;
	}

    /******* Constructor *******/
    public ProviderOrdersSummary_CS(){}

	// Default page action, add additional page action functionality here
	public PageReference init(){

		///////////////////////////////////
		// Authentication and Permission
		///////////////////////////////////

		if(!isSecure && !Test.isRunningTest()){
			return DoLogout();
		}

		if(!authenticated){
			return DoLogout();
		}

		if(portalUser.AccountType != 'Provider'){
			return homepage;
		}

		///////////////////////////////////
		// Additional init
		///////////////////////////////////

		prOrderServices = new ProviderOrderServices_CS(portalUser.instance.Id);

	    //Default behavior if passed a filter that isn't included
	    if(ApexPages.currentPage().getParameters().containsKey('filter')){
	    	filterByStage = ApexPages.currentPage().getParameters().get('filter');
	    }
	    else{
	    	filterByStage = '';
	    }

	    filterByTime = 'thisWeek';
	    
	    /**********************************************************************************
         
		 ******** NOTE: TO BE DEPLOYED IN LATER SPRINTS ***********
	    if (this.STAGES_WITH_ORDERS_FOR_ALL_TIME.contains(this.filterByStage)){
	    	filterByTime = 'allTime';
            pageSize = 100;
	    }else {
	    	filterByTime = 'thisWeek';
            pageSize = 10;
	    }

		***********************************************************************************/
	    //Retrieve the unfiltered list of orders to display
	    populateOrderList();

		//Populate the filter boxes
		//populateFilterBoxes(); // commented out by VK on 11-28-2016 and moved the call function inside populateOrderList();

		return null;
	}//End Init

	public void populateOrderList(){

    	/*
        ** CP-80: Default Order Summary Table Sorting - SPRINT 3/20/2017 - ADDED BY DGARKUSHA
        */
        /*
        if (this.STAGES_WITH_ORDERS_FOR_ALL_TIME.contains(this.filterByStage)){
        
            this.filterByTime = 'allTime';
            this.pageSize = 100;
        }else {
            this.filterByTime = 'lastMonth';
            this.pageSize = 10;
        }*/

        //Mutually-exclusive searching for users/providers
    	if(filterByEntityType == 'User'){
    		filterByPayer = '';
    	}
    	else{
    		filterByUser = '';
    	}

		System.debug(
			'\nFilter terms: ' +
			'\nstage: ' + filterByStage +
			'\nuserId: ' + filterByUser +
			'\nproviderId: ' + filterByPayer +
			'\nsearchTerm: ' + filterBySearch +
			'\ntimeFrame: ' + filterByTime
		);

    	/**
	     ** NOTE: STAGING FOR FUTURE RELEASE - SPRINT 12/12 - BY VK - TO BE REPLACED WITH CURRENT orderSetCon
	     ** orderSetCon = payerOrderServices.getOrdersForSummary(filterByStage, filterBySubStage, filterByUser,filterByProvider,filterBySearch,filterByTime);
	    **/

        if (String.isNotEmpty(this.sortColumn)) this.sortColumn.trim();
        
        System.debug('\n\n sortColumn => ' + this.sortColumn + '\n');
        System.debug('\n\n sortOrder => ' + this.sortOrder + '\n');

        orderSetCon = prOrderServices.getOrdersForSummaryWithSort(filterByStage,filterBySubStage,filterByUser,filterByPayer,filterBySearch,filterByTime, new Map<String, String>{this.sortColumn => this.sortOrder});

    	if(orderSetCon != null){
    		orderSetCon.setPageSize(pageSize);
    	}

		System.debug('orderSetCon pageSize: ' + orderSetCon.getPageSize());
		System.debug('orderSetCon resultSize: ' + orderSetCon.getResultSize());
		System.debug('orderSetCon # of pages: ' + totalPages);

        populateFilterBoxes(); // added by VK on 11-28-2016 to make sure the populateFilterBoxes() is called everytime populateOrderList() is called.
    }

	public void populateFilterBoxes(){

		//Generate filter content based on order results
	    filterByUserOptions = new List<SelectOption>{new SelectOption('','All')};
	    filterByPayerOptions = new List<SelectOption>{new SelectOption('','All')};

	    //NOTE: We need to use the entire order list, not the subset, to grab info
	    List<Order__c> completeList = prOrderServices.getOrderListForSummary(filterByStage,filterBySubStage,filterByUser,filterByPayer,filterBySearch,filterByTime);

	    Map<Id,String> userMap = new Map<Id,String>();
	    Map<Id,Account> payerMap;

	    Set<Id> payerIds = new Set<Id>();

	    for(Order__c o : completeList){
	    	if(o.Accepted_By__c != null){
	    		userMap.put(o.Accepted_By__c ,o.Accepted_By__r.Name); // change from o.Entered_By__c to o.Accepted_By__c as provider needs to search by latter - by VK on 11/28/2016
	    	}
	    	if(o.Plan_Patient__r.Health_Plan__c != null){
    			payerIds.add(o.Plan_Patient__r.Health_Plan__c);
    		}
	    }

	    payerMap = new Map<Id,Account>([select Id,Name from Account where Id in :payerIds]);

	    for(Id i : userMap.keySet()){
            if(String.isNotBlank((String)i) && String.isNotBlank(userMap.get(i))){
                filterByUserOptions.add(new SelectOption(String.valueOf(i),userMap.get(i)));
            }
	    }

	    for(Id i : payerMap.keySet()){
	    	filterByPayerOptions.add(new SelectOption(String.valueOf(i),payerMap.get(i).Name));
	    }
	}
}