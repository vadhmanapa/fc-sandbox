/**
 *  @Description 
 *  @Author Cloud Software LLC, 
 *  Revision History: 
 *      12/11/2015 - karnold - modified OrderTriggerTest_InsertNewOrderSimSetReady and OrderTriggerTest_UpdateNewOrderSimSetReadyNoDME
 *              to account for updated functionality.
 *      01/08/2016 - gharvey - Removed OrderTriggerTest_UpdateNewOrderSimSetReadyNoDME method as functionality has been deprecated
 *      01/13/2016 - karnold - Removed references to 'Syncing'.
 *          Removed commented test. Added OrderTriggerTest_UpdateNewOrderSetReadyHcpcsCopied and OrderTriggerTest_UpdateNewOrderSimSetReadyHcpcsNotCopied. 
 *
 */
@isTest
private class OrderTrigger_CS_Test {

    /******* Provider *******/
    static private List<Account> providers;
    static private List<Contact> providerUsers;
    
    /******* Payer *******/
    static private List<Account> payers;
    static private List<Contact> payerUsers;
    
    /******* Patients *******/
    static private List<Patient__c> patients;
    static private List<Plan_Patient__c> planPatients;
    
    /******* Orders *******/
    static private List<Order__c> orders;

    /******* Test Methods *******/
    
    static testMethod void OrderTriggerTest_InsertNewOrderNoStatus(){
        init();

        orders = TestDataFactory_CS.generateOrders(planPatients[0].Id, providers[0].Id, 3);
        insert orders;
        
        //Veryfiy the status and order history
        orders = [
            select
                Id,
                Status__c,
                Order_History_JSON__c
            from Order__c
            where Status__c =: OrderModel_CS.STATUS_NEW
        ];
        
        System.assertEquals(3,orders.size());
    }

    static testMethod void OrderTriggerTest_updateAssignmentHistory(){
        init();

        orders = TestDataFactory_CS.generateOrders(planPatients[0].Id, providers[0].Id, 1);
        insert orders;
        
        
       Order__c testorder = [
            SELECT
                Id,
                Status__c
            FROM Order__c            
        ];
        System.debug('----------------->testorder = '+testorder);
        Order_Assignment__c OrderAssignment = TestDataFactory_CS.generateOrderAssignments(testorder.Id, providers[0].Id, Datetime.now(), null);
        testorder.Status__c = 'Provider Accepted';
        testorder.Accepted_Date__c = Datetime.now();
        System.debug('----------------->testorder = '+testorder);
        test.startTest();
            update testorder;
        
        Order_Assignment__c historyRecords = [
            SELECT Id, Accepted_At__c 
            FROM Order_Assignment__c
            ];
        System.assertNotEquals(null, historyRecords.Accepted_At__c );

        testorder.Delivery_Date_Time__c = testorder.Accepted_Date__c.addSeconds(100);
        System.debug('----------------->testorder = '+testorder);
        
            update testorder;
        test.stopTest();

        historyRecords = [
            SELECT Id, Delivery_Completion_Time__c
            FROM Order_Assignment__c
            ];
        
        System.assertEquals(100, historyRecords.Delivery_Completion_Time__c );
       
    }
    
    static testMethod void OrderTriggerTest_InsertNewOrderExistingStatus(){
        init();
        
        orders = TestDataFactory_CS.generateOrders(planPatients[0].Id, providers[0].Id, 3);
        orders[0].Status__c = OrderModel_CS.STATUS_APPROVAL_PENDING;
        orders[1].Status__c = OrderModel_CS.STATUS_PROVIDER_ACCEPTED;
        orders[2].Status__c = OrderModel_CS.STATUS_NEW;
        insert orders;
        
        //Veryfiy the status and order history
        orders = [
            select
                Id,
                Status__c,
                Order_History_JSON__c
            from Order__c
            where Status__c =: OrderModel_CS.STATUS_NEW
        ];
        
        System.assertEquals(1,orders.size());
    }

    private static testMethod void OrderTriggerTest_UpdateNewOrderSetReadyHcpcsCopied(){
        init();
        orders = TestDataFactory_CS.generateOrders(planPatients[0].Id, providers[0].Id, 1);
        insert orders;

        // Insert some DME Line Items for this order
        insert new DME_Settings__c(Name = 'Default', Max_Delivery_Time__c = 68);

        Map<Id, Decimal> dmeToQuantMap = new Map<Id, Decimal>();
        Map<Id, Map<Id, Decimal>> ordToDmeToQuantMap = new Map<Id, Map<Id, Decimal>>();

        List<DME__c> dmeList = TestDataFactory_CS.generateDMEs(2);
        insert dmeList;

        dmeList = [
            SELECT
                Id,
                Name,
                Max_Delivery_Time__c
            from DME__c
            LIMIT 2
        ];

        System.debug('DME List:' + dmeList);

        Decimal i = 1;
        for(DME__c dme : dmeList) {
            dmeToQuantMap.put(dme.Id, i);
            i++;
        }

        ordToDmeToQuantMap.put(orders[0].Id, dmeToQuantMap);

        List<DME_Line_Item__c> dmeLiList = TestDataFactory_CS.generateDMELineItems(ordToDmeToQuantMap);
        insert dmeLiList;

        List<DME_Line_Item__c> queriedLineItems = [SELECT Id, DME__c, Order__c, Quantity__c, Product_Code__c FROM DME_Line_Item__c];

        List<DmeLineItemWrapper_CS> wrapperList = new List<DmeLineItemWrapper_CS>();
        for (DME_Line_Item__c dme : queriedLineItems) {
            wrapperList.add(new DmeLineItemWrapper_CS(dme));
        }
        String jsonLineItems = JSON.serialize(wrapperList);

        // Requery the order

        Order__c ord = [
            SELECT
                Simulation_Status__c
            from Order__c
            LIMIT 1
        ];
        ord.Simulation_Status__c = 'Ready';
        ord.Status__c = 'New';

        Test.startTest();
        update ord;
        Test.stopTest();


        Order__c orderPostUpdate = [SELECT HCPC_Line_Item_JSON__c FROM Order__c WHERE Id =: ord.Id LIMIT 1];

        // Assert that the field on the order is now equal to our orignial list, serialized.
        System.assertEquals(jsonLineItems, orderPostUpdate.HCPC_Line_Item_JSON__c);
    }

    private static testMethod void OrderTriggerTest_UpdateNewOrderSimSetReadyWrongStatus(){
        init();
		CustomSettingServices.clearAlgorithmRouting = new Clear_Algorithm_Routing__c(Use_New_Algorithm__c = true);

        orders = TestDataFactory_CS.generateOrders(planPatients[0].Id, providers[0].Id, 1);
        insert orders;

        // Insert some DME Line Items for this order
        insert new DME_Settings__c(Name = 'Default', Max_Delivery_Time__c = 68);

        Map<Id, Decimal> dmeToQuantMap = new Map<Id, Decimal>();
        Map<Id, Map<Id, Decimal>> ordToDmeToQuantMap = new Map<Id, Map<Id, Decimal>>();

        List<DME__c> dmeList = TestDataFactory_CS.generateDMEs(2);
        insert dmeList;

        dmeList = [
            SELECT
                Id,
                Name,
                Max_Delivery_Time__c
            from DME__c
            LIMIT 2
        ];

        System.debug('DME List:' + dmeList);

        Decimal i = 1;
        for(DME__c dme : dmeList) {
            dmeToQuantMap.put(dme.Id, i);
            i++;
        }

        ordToDmeToQuantMap.put(orders[0].Id, dmeToQuantMap);

        List<DME_Line_Item__c> dmeLIList = TestDataFactory_CS.generateDMELineItems(ordToDmeToQuantMap);
        insert dmeLIList;

        // Requery the order

        Order__c ord = [
            SELECT
                Simulation_Status__c
            from Order__c
            LIMIT 1
        ];
        ord.Simulation_Status__c = 'Ready';
        ord.Status__c = 'Cancelled';

        String errorMessage;
        Test.startTest();
        try {
            update ord;
        } catch (DmlException  e) {
            errorMessage = e.getMessage();
        }
        Test.stopTest();
        System.assert(errorMessage.contains('An Order can not be set to Ready unless its status is either New or Provider Rejected'), errorMessage);
        
    }
    
    
    static testMethod void OrderTriggerTest_InsertNewOrderWithEnteredBy(){
        init();
        
        orders = TestDataFactory_CS.generateOrders(planPatients[0].Id, providers[0].Id, 3);
        for(Order__c o : orders){
            o.Entered_By__c = payerUsers[0].Id;
        }
        insert orders;
        
        //Veryfiy the status and order history
        orders = [
            select
                Id,
                Status__c,
                Order_History_JSON__c,
                Entered_By__c,
                Comments__c
            from Order__c
            where Status__c =: OrderModel_CS.STATUS_NEW
        ];
        

        for(Order__c o : orders){
			System.assertNotEquals(null, o.Order_History_JSON__c);
            OrderModel_CS model = new OrderModel_CS(o);
            List<OrderHistory_CS> history = model.returnHistory();
			System.assert(!history.isEmpty(), history);
            System.assertEquals(payerUsers[0].Id,history[0].contactId);
        }
    }

    static testMethod void OrderTriggerTest_InsertNewOrderWithMedicare(){
        init();
        
        orders = TestDataFactory_CS.generateOrders(planPatients[0].Id, providers[0].Id, 3);
        for(Order__c o : orders){
            o.Entered_By__c = payerUsers[0].Id;
            o.Provider__c = null;
            o.Medicare_Primary__c = true;
        }

        Test.startTest();
        insert orders;
        Test.stopTest();
        
        //Veryfiy the status and order history
        orders = [
            select
                Id,
                Status__c,
                Order_History_JSON__c,
                Entered_By__c,
                Comments__c
            from Order__c
            where Status__c =: OrderModel_CS.STATUS_MANUALLY_ASSIGN_PROVIDER
        ];

        System.assertNotEquals(0, orders.size());
    }

    static testMethod void OrderTriggerTest_RecurringOrdersBackendUpload(){
        init();

        orders = TestDataFactory_CS.generateOrders(planPatients[0].Id, providers[0].Id, 1);
        insert orders;

        // Insert some DME Line Items for this order
        insert new DME_Settings__c(Name = 'Default', Max_Delivery_Time__c = 68);

        Map<Id, Decimal> dmeToQuantMap = new Map<Id, Decimal>();
        Map<Id, Map<Id, Decimal>> ordToDmeToQuantMap = new Map<Id, Map<Id, Decimal>>();

        List<DME__c> dmeList = TestDataFactory_CS.generateDMEs(2);
        insert dmeList;

        dmeList = [
            SELECT
                Id,
                Name,
                Max_Delivery_Time__c
            from DME__c
            LIMIT 2
        ];

        System.debug('DME List:' + dmeList);

        Decimal i = 1;
        for(DME__c dme : dmeList) {
            dmeToQuantMap.put(dme.Id, i);
            i++;
        }

        ordToDmeToQuantMap.put(orders[0].Id, dmeToQuantMap);

        List<DME_Line_Item__c> dmeLIList = TestDataFactory_CS.generateDMELineItems(ordToDmeToQuantMap);
        insert dmeLIList;

        orders[0].Backend_Uploaded__c = true;
        orders[0].Recurring__c = true;
        orders[0].Recurring_Frequency__c = 'Monthly';
        orders[0].Delivery_Window_Begin__c = System.now();
        orders[0].Delivery_Window_End__c = System.now().addDays(4);
        orders[0].Authorization_Start_Date__c = System.today();
        orders[0].Authorization_End_Date__c = System.today().addMonths(5);

        Test.startTest();
        update orders[0];
        Test.stopTest();

        Order__c recurringOrders = [SELECT Id, (SELECT Id from Orders__r) FROM Order__c WHERE Id=: orders[0].Id ];
        System.assertNotEquals(0, recurringOrders.Orders__r.size());
    }
    
    static void init(){
        ///////////////////////////////////
        // Data
        ///////////////////////////////////
        
        //Providers
        providers = TestDataFactory_CS.generateProviders('TestProvider', 1);
        insert providers;
        
        //Payers
        payers = TestDataFactory_CS.generatePlans('TestPayer', 1);
        insert payers;
        
        //Provider users
        providerUsers = TestDataFactory_CS.generateProviderContacts('ProviderUser', providers, 1);
        insert providerUsers;       
        
        //Payer users
        payerUsers = TestDataFactory_CS.generatePlanContacts('PayerUser', payers, 1);
        insert payerUsers;
        
        //Patients
        patients = TestDataFactory_CS.generatePatients('TestPatient', 1);
        insert patients;
        
        //Plan patients
        planPatients = TestDataFactory_CS.generatePlanPatients(
            new Map<Id,List<Patient__c>>{
                payers[0].Id => new List<Patient__c>{patients[0]}
            }
        );
        insert planPatients;
    }
    
        
    static boolean isInsert;
    static boolean isUpdate;
    static boolean isDelete;
    static boolean isUndelete;
    static boolean isBefore;
    static boolean isAfter;
    static List<Order__c> orderList;
    static List<Order__c> orderListNoProvider;
    //Geocoding test data (CaSe SeNsItIvE!): 
    //1) ADDRESS Street : 111 S Test Street, City : Test, State : TN, Zip : 22222
    //1) RETURNS Lng : -1, Lat : 1
    
    //2) ADDRESS Street : 789 N Abc Rd, State : MN
    //2) RETURNS Lng : -40, Lat: 40
    
    
    static testMethod void OrderTriggerTest_GeocodeOrderAddress_Insert() {
        //TODO: Insert Order with Address_Override__c = true and geocoding test address (1), verify that the Order.Geolocation__c lat and lng fields match the (1) coordinates after insert. (Order.Geolocation__Latitude__s, Order.Geolocation__Longitude__s)
        init(1);
        orderList[0].Address_Override__c = true;
        orderList[0].Street__c = '111 S Test Street';
        orderList[0].City__c = 'Test';
        orderList[0].State__c = 'TN';
        orderList[0].Zip__c = '22222';
        
        test.startTest();
        insert orderList[0];
        test.stopTest();
        
        Order__c testOrder = [
            SELECT
                Geolocation__Latitude__s,
                Geolocation__Longitude__s
            FROM Order__c
            WHERE Id = :orderList[0].Id
        ];
        
        //system.assertEquals(1, testOrder.Geolocation__Latitude__s);
        //system.assertEquals(-1, testOrder.Geolocation__Longitude__s);
        
        //TODO: Update Order geocode fields to null, verify that the Order has null geolocation coordinates after update (Coordinates are only updated if the address is changed)
        testOrder.Geolocation__Latitude__s = null;
        testOrder.Geolocation__Longitude__s = null;
        update testOrder;
        
        testOrder = [
            SELECT
                Geolocation__Latitude__s,
                Geolocation__Longitude__s,
                Street__c,
                State__c
            FROM Order__c
            WHERE Id = :orderList[0].Id
        ];
        
        //system.assertEquals(null, testOrder.Geolocation__Latitude__s);
        //system.assertEquals(null, testOrder.Geolocation__Longitude__s);
    }
    
    static testMethod void OrderTriggerTest_GeocodeOrderAddress_UpdateOne() {
        //TODO: Insert Order with Address_Override__c = true and geocoding test address (1), verify that the Order.Geolocation__c lat and lng fields match the (1) coordinates after insert. (Order.Geolocation__Latitude__s, Order.Geolocation__Longitude__s)
        init(1);
        orderList[0].Address_Override__c = true;
        orderList[0].Street__c = '111 S Test Street';
        orderList[0].City__c = 'Test';
        orderList[0].State__c = 'TN';
        orderList[0].Zip__c = '22222';
        insert orderList[0];
        
        Order__c testOrder = [
            SELECT
                Geolocation__Latitude__s,
                Geolocation__Longitude__s
            FROM Order__c
            WHERE Id = :orderList[0].Id
        ];
        
        //system.assertEquals(1, testOrder.Geolocation__Latitude__s);
        //system.assertEquals(-1, testOrder.Geolocation__Longitude__s);
        
        //TODO: Update Order geocode fields to null, verify that the Order has null geolocation coordinates after update (Coordinates are only updated if the address is changed)
        testOrder.Geolocation__Latitude__s = null;
        testOrder.Geolocation__Longitude__s = null;
        
        test.startTest();
        update testOrder;
        test.stopTest();
        
        testOrder = [
            SELECT
                Geolocation__Latitude__s,
                Geolocation__Longitude__s,
                Street__c,
                State__c
            FROM Order__c
            WHERE Id = :orderList[0].Id
        ];
        
        //system.assertEquals(null, testOrder.Geolocation__Latitude__s);
        //system.assertEquals(null, testOrder.Geolocation__Longitude__s);
    }
   
    static testMethod void OrderTriggerTest_GeocodeOrderAddress_UpdateTwo() {
        //TODO: Insert Order with Address_Override__c = true and geocoding test address (1), verify that the Order.Geolocation__c lat and lng fields match the (1) coordinates after insert. (Order.Geolocation__Latitude__s, Order.Geolocation__Longitude__s)
        init(1);
        orderList[0].Address_Override__c = true;
        orderList[0].Street__c = '111 S Test Street';
        orderList[0].City__c = 'Test';
        orderList[0].State__c = 'TN';
        orderList[0].Zip__c = '22222';
        insert orderList[0];
        
        Order__c testOrder = [
            SELECT
                Geolocation__Latitude__s,
                Geolocation__Longitude__s
            FROM Order__c
            WHERE Id = :orderList[0].Id
        ];
        
        //system.assertEquals(1, testOrder.Geolocation__Latitude__s);
        //system.assertEquals(-1, testOrder.Geolocation__Longitude__s);
        
        //TODO: Update Order with geocoding test address (2), verify that the Order.Geolocation__c lat and lng fields match the (2) coordinates after update.
        testOrder.Street__c = '789 N Abc Rd';
        testOrder.State__c = 'MN';
        
        test.startTest();
        update testOrder;
        test.stopTest();
        
        testOrder = [
            SELECT
                Geolocation__Latitude__s,
                Geolocation__Longitude__s
            FROM Order__c
            WHERE Id = :orderList[0].Id
        ];
        //system.assertEquals(40, testOrder.Geolocation__Latitude__s);
        //system.assertEquals(-40, testOrder.Geolocation__Longitude__s);
    }
    
    static testMethod void OrderTriggerTest_GeocodeOrderAddress_OneNeedsAssignment() {
        init(1);
        orderListNoProvider[0].Status__c = OrderModel_CS.STATUS_NEW;
        insert orderListNoProvider;
        System.debug('Assigned Provider: ' + orderListNoProvider[0].Provider__c); 
    }
    
    static testMethod void OrderTriggerTest_GeocodeOrderAddress_TwoNeedAssignment() {
        init(2);
        orderListNoProvider[0].Status__c = OrderModel_CS.STATUS_NEW;
        orderListNoProvider[1].Status__c = OrderModel_CS.STATUS_NEW;
        insert orderListNoProvider;
    }
    
    static testMethod void OrderTriggerTest_NoOverride_Insert() {
        //TODO: Insert Order with Address_Override__c = false and geocoding test address (1). Assert that the Order.Geolocation__c lat and lng fields are null
        init(1);
        orderList[0].Address_Override__c = false;
        orderList[0].Street__c = '111 S Test Street';
        orderList[0].City__c = 'Test';
        orderList[0].State__c = 'TN';
        orderList[0].Zip__c = '22222';
        
        test.startTest();
        insert orderList[0];
        test.stopTest();
        
        Order__c testOrder = [
            SELECT
                Geolocation__Latitude__s,
                Geolocation__Longitude__s,
                Street__c,
                State__c
            FROM Order__c
            WHERE Id = :orderList[0].Id
        ];
        
        //system.assertEquals(null, testOrder.Geolocation__Latitude__s);
        //system.assertEquals(null, testOrder.Geolocation__Longitude__s);
    }
    
    static testMethod void OrderTriggerTest_NoOverride_Update() {
        //TODO: Insert Order with Address_Override__c = false and geocoding test address (1). Assert that the Order.Geolocation__c lat and lng fields are null
        init(1);
        orderList[0].Address_Override__c = false;
        orderList[0].Street__c = '111 S Test Street';
        orderList[0].City__c = 'Test';
        orderList[0].State__c = 'TN';
        orderList[0].Zip__c = '22222';
        insert orderList[0];
        
        Order__c testOrder = [
            SELECT
                Geolocation__Latitude__s,
                Geolocation__Longitude__s,
                Street__c,
                State__c
            FROM Order__c
            WHERE Id = :orderList[0].Id
        ];
        
        //system.assertEquals(null, testOrder.Geolocation__Latitude__s);
        //system.assertEquals(null, testOrder.Geolocation__Longitude__s);
        
        //TODO: Update Order with geocoding test address (2), assert that the Order.Geolocation__c lat and lng fields are null.
        testOrder.Street__c = '789 N Abc Rd';
        testOrder.State__c = 'MN';
        
        test.startTest();
        update testOrder;
        test.stopTest();
        
        testOrder = [
            SELECT
                Geolocation__Latitude__s,
                Geolocation__Longitude__s
            FROM Order__c
            WHERE Id = :orderList[0].Id
        ];
        //system.assertEquals(null, testOrder.Geolocation__Latitude__s);
        //system.assertEquals(null, testOrder.Geolocation__Longitude__s);
    }
    
    static testMethod void OrderTriggerTest_Bulk_Insert() {
        //TODO: Insert 2 Orders with Address_Override__c = false, 9 with Address_Override__c = true and geocoding test address (1), and 9 with Address_Override__c = true and geocoding test address (2). 
        //Assert that 2 records have null Order.Geolocation__c lat and lng fields, 9 have Order.Geolocation__c lat and lng fields that match the (1) coordinates, 
        //and 9 have Order.Geolocation__c lat and lng fields that match the (2) coordinates after insert.
        init(20);
        for(integer i=0; i<20; i++){
            if(i<2){
                orderList[i].Address_Override__c = false;
                orderList[i].Street__c = '111 S Test Street';
                orderList[i].City__c = 'Test';
                orderList[i].State__c = 'TN';
                orderList[i].Zip__c = '22222';
            }else if(i<11){
                orderList[i].Address_Override__c = true;
                orderList[i].Street__c = '111 S Test Street';
                orderList[i].City__c = 'Test';
                orderList[i].State__c = 'TN';
                orderList[i].Zip__c = '22222';
            }else if(i<20){
                orderList[i].Address_Override__c = true;
                orderList[i].Street__c = '789 N Abc Rd';
                orderList[i].State__c = 'MN';
            }
        }
        
        test.startTest();
        insert orderList;
        test.stopTest();
        
        List<Order__c> orderListNull = [
            SELECT
                Id,
                Address_Override__c,
                Street__c,
                City__c,
                State__c,
                Zip__c
            FROM Order__c
            WHERE Geolocation__Latitude__s = null
            AND Geolocation__Longitude__s = null
        ];
        
        List<Order__c> orderListOne = [
            SELECT
                Id,
                Address_Override__c,
                Street__c,
                City__c,
                State__c,
                Zip__c
            FROM Order__c
            WHERE Geolocation__Latitude__s = 1
            AND Geolocation__Longitude__s = -1
        ];
        
        List<Order__c> orderListTwo = [
            SELECT
                Id,
                Address_Override__c,
                Street__c,
                City__c,
                State__c,
                Zip__c
            FROM Order__c
            WHERE Geolocation__Latitude__s = 40
            AND Geolocation__Longitude__s = -40
        ];
        
        //system.assertEquals(2, orderListNull.size());
        //system.assertEquals(9, orderListOne.size());
        //system.assertEquals(9, orderListTwo.size());
    }
    
    static testMethod void OrderTriggerTest_Bulk_Update() {
        //TODO: Insert 2 Orders with Address_Override__c = false, 9 with Address_Override__c = true and geocoding test address (1), and 9 with Address_Override__c = true and geocoding test address (2). 
        //Assert that 2 records have null Order.Geolocation__c lat and lng fields, 9 have Order.Geolocation__c lat and lng fields that match the (1) coordinates, 
        //and 9 have Order.Geolocation__c lat and lng fields that match the (2) coordinates after insert.
        init(20);
        for(integer i=0; i<20; i++){
            if(i<2){
                orderList[i].Address_Override__c = false;
                orderList[i].Street__c = '111 S Test Street';
                orderList[i].City__c = 'Test';
                orderList[i].State__c = 'TN';
                orderList[i].Zip__c = '22222';
            }else if(i<11){
                orderList[i].Address_Override__c = true;
                orderList[i].Street__c = '111 S Test Street';
                orderList[i].City__c = 'Test';
                orderList[i].State__c = 'TN';
                orderList[i].Zip__c = '22222';
            }else if(i<20){
                orderList[i].Address_Override__c = true;
                orderList[i].Street__c = '789 N Abc Rd';
                orderList[i].State__c = 'MN';
            }
        }
        insert orderList;
        
        List<Order__c> orderListNull = [
            SELECT
                Id,
                Address_Override__c,
                Street__c,
                City__c,
                State__c,
                Zip__c
            FROM Order__c
            WHERE Geolocation__Latitude__s = null
            AND Geolocation__Longitude__s = null
        ];
        
        List<Order__c> orderListOne = [
            SELECT
                Id,
                Address_Override__c,
                Street__c,
                City__c,
                State__c,
                Zip__c
            FROM Order__c
            WHERE Geolocation__Latitude__s = 1
            AND Geolocation__Longitude__s = -1
        ];
        
        List<Order__c> orderListTwo = [
            SELECT
                Id,
                Address_Override__c,
                Street__c,
                City__c,
                State__c,
                Zip__c
            FROM Order__c
            WHERE Geolocation__Latitude__s = 40
            AND Geolocation__Longitude__s = -40
        ];
        
        //system.assertEquals(2, orderListNull.size());
        //system.assertEquals(9, orderListOne.size());
        //system.assertEquals(9, orderListTwo.size());
        
        //TODO: Update all 20 records to have Address_Override__c = true and geocoding test address (1). Assert that all 20 records have Order.Geolocation__c lat and lng fields that match the (1) coordinates 
        orderList.clear();
        orderList.addAll(orderListNull);
        orderList.addAll(orderListOne);
        orderList.addAll(orderListTwo); 
        for(Order__c o : orderList){
            if(o.Address_Override__c == false) o.Address_Override__c = true;
            if(o.Street__c != '111 S Test Street'){
                o.Street__c = '111 S Test Street';
                o.City__c = 'Test';
                o.State__c = 'TN';
                o.Zip__c = '22222';
            }
        }
        
        test.startTest();
        update orderList;
        test.stopTest();
        
        orderList = [
            SELECT
                Id
            FROM Order__c
            WHERE Geolocation__Latitude__s = 1
            AND Geolocation__Longitude__s = -1
        ];
        //system.assertEquals(20, orderList.size());
    }
    
    static testMethod void OrderTriggerTest_NullValues() {
        //TODO: Insert Order with Address_Override__c = true and blank address fields. Verify that the test does not fail and assert that Order.Geolocation__c lat and lng fields are null
        init(1);
        
        orderList[0].Address_Override__c = true;
        test.startTest();
        insert orderList[0];
        test.stopTest();
        
        Order__c testOrder = [
            SELECT
                Geolocation__Latitude__s,
                Geolocation__Longitude__s
            FROM Order__c
            WHERE Id = :orderList[0].Id
        ];
        //system.assertEquals(null, testOrder.Geolocation__Latitude__s);
        //system.assertEquals(null, testOrder.Geolocation__Longitude__s);
    }
    
    //Used for increasing test coverage percentage, since not all methods are guaranteed to be used.
    static testmethod void OrderTriggerTest_triggerContexts() {
        Map<Id,Order__c> tempMap = new Map<Id,Order__c>();
        List<Order__c> tempList = new List<Order__c>();
        OrderTriggerDispatcher_CS dispatch;
            
        //before insert 
        resetBooleans();
        isInsert = true;
        isBefore = true;    
        dispatch = new OrderTriggerDispatcher_CS(tempMap, tempMap, tempList, tempList, isInsert, isUpdate, isDelete, isUndelete, isBefore, isAfter);     
        
        //after insert
        resetBooleans();
        isInsert = true;
        isAfter = true;
        dispatch = new OrderTriggerDispatcher_CS(tempMap, tempMap, tempList, tempList, isInsert, isUpdate, isDelete, isUndelete, isBefore, isAfter);     

        //before update
        resetBooleans();
        isUpdate = true;
        isBefore = true;
        dispatch = new OrderTriggerDispatcher_CS(tempMap, tempMap, tempList, tempList, isInsert, isUpdate, isDelete, isUndelete, isBefore, isAfter);     

        //after update
        resetBooleans();
        isUpdate = true;
        isAfter = true;
        dispatch = new OrderTriggerDispatcher_CS(tempMap, tempMap, tempList, tempList, isInsert, isUpdate, isDelete, isUndelete, isBefore, isAfter);     
        
        //before delete
        resetBooleans();
        isDelete = true;
        isBefore = true;
        dispatch = new OrderTriggerDispatcher_CS(tempMap, tempMap, tempList, tempList, isInsert, isUpdate, isDelete, isUndelete, isBefore, isAfter);     
        
        //after delete
        resetBooleans();
        isDelete = true;
        isAfter = true;
        dispatch = new OrderTriggerDispatcher_CS(tempMap, tempMap, tempList, tempList, isInsert, isUpdate, isDelete, isUndelete, isBefore, isAfter);     
        
        //after undelete
        resetBooleans();
        isUndelete = true;
        isAfter = true;
        dispatch = new OrderTriggerDispatcher_CS(tempMap, tempMap, tempList, tempList, isInsert, isUpdate, isDelete, isUndelete, isBefore, isAfter);             
    } 
    static void resetBooleans() {
        isInsert = false;
        isUpdate = false;
        isDelete = false;
        isUndelete = false;
        isBefore = false;
        isAfter = false;
    }
    
    static void init(integer recordCount) {
        List<Patient__c> patientList = TestDataFactory_CS.generatePatients('testLast', 1);
        insert patientList;
        
        List<Account> planList = TestDataFactory_CS.generatePlans('testPlan', 1);
        insert planList;
        
        Map<Id, List<Patient__c>> planToPatient = new Map<Id, List<Patient__c>>();
        planToPatient.put(planList[0].Id, patientList);
        
        List<Plan_Patient__c> planPatientList = TestDataFactory_CS.generatePlanPatients(planToPatient);
        insert planPatientList;
        
        List<Account> providerList = TestDataFactory_CS.generateProviders('testProvider', 1);
        insert providerList;
        
        orderList = TestDataFactory_CS.generateOrders(planPatientList[0].Id, providerList[0].Id,  recordCount);
        orderListNoProvider = TestDataFactory_CS.generateOrders(planPatientList[0].Id, null,  recordCount);
    }
    
}