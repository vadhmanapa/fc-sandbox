@isTest
private class NewCallLogLinkControllerTest {

	static PageReference pageRef;
	static NewCallLogLinkController pageCon;

    static testMethod void link() {
        // TO DO: implement unit test
    }
    
    static testMethod void unlink() {
        // TO DO: implement unit test
    }
    
    static testMethod void search() {
        // TO DO: implement unit test
    }
    
    static void init() {
    	
    	Call_Log__c existingCallLog = new Call_Log__c();
    	insert existingCallLog;
    	
    	Call_Log_Relation__c newRelation = new Call_Log_Relation__c();
    	ApexPages.Standardcontroller stdCon = new ApexPages.Standardcontroller(newRelation);
    	
		pageRef = Page.NewCallLogLink;
    	pageRef.getParameters().put('callLogId',existingCallLog.Id);
    	
    	pageCon = new NewCallLogLinkController(stdCon);
    }
}