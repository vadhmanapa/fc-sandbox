@isTest
private class LookUpRollUpUtilsTest_IP {
	
	@isTest static void testOnRollUp() {
		
		//create account

		Account testAcc = new Account(Name = 'Test Roll Up', Type__c = 'Provider', Type_Of_Provider__c = 'DME');
		insert testAcc;

		// create a provider location for that account

		Provider_Location__c testPL = TestDataFactory_CS.generateProviderLocations(testAcc.Id, 1)[0];
		insert testPL;

		// create CWI

		ARAG__c testBill = new ARAG__c(Bill__c = '100110', Provider__c = 'Test Provider');
		insert testBill;

		// create a recoupment
		Recoupment__c testRec = new Recoupment__c(	Related_Claims_Work_Item__c = testBill.Id, Provider_Location_New__c = testPL.Id);
		insert testRec;

		//create recoupment tracker

		Provider_Recoupment_Tracker__c testReco1 = new Provider_Recoupment_Tracker__c(Amount_Recouped__c = 100, Refund_and_Recoupment__c = testRec.Id);
		insert testReco1;

		Recoupment__c checkMate1 = [Select Id, Name, Total_Recouped_from_Provider__c from Recoupment__c where Id=:testRec.Id];
		System.assertEquals(100, checkMate1.Total_Recouped_from_Provider__c, 'The money was copied to the currecy field');


		Provider_Recoupment_Tracker__c testReco2 = new Provider_Recoupment_Tracker__c(Amount_Recouped__c = 100, Refund_and_Recoupment__c = testRec.Id);
		insert testReco2;

		Recoupment__c checkMate2 = [Select Id, Name, Total_Recouped_from_Provider__c from Recoupment__c where Id=:testRec.Id];
		System.assertEquals(200, checkMate2.Total_Recouped_from_Provider__c, 'The money was rolled up and summed');

		testReco2.Provider_Check_Number__c = '1103232'; // just updating check 
		update testReco2;

		Recoupment__c checkMate3 = [Select Id, Name, Total_Recouped_from_Provider__c from Recoupment__c where Id=:testRec.Id];
		System.assertEquals(200, checkMate3.Total_Recouped_from_Provider__c, 'The roll up remains same as it should');

		testReco2.Amount_Recouped__c = 201;
		update testReco2;

		Recoupment__c checkMate4 = [Select Id, Name, Total_Recouped_from_Provider__c from Recoupment__c where Id=:testRec.Id];
		System.assertEquals(301, checkMate4.Total_Recouped_from_Provider__c, 'The roll up add up as it should');

		delete testReco2;

		Recoupment__c checkMate5 = [Select Id, Name, Total_Recouped_from_Provider__c from Recoupment__c where Id=:testRec.Id];
		System.assertEquals(100, checkMate5.Total_Recouped_from_Provider__c, 'The money was deducted and roll up worked');

	}
	
	
	
}