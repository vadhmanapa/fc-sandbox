public class ClaimsInquiryToCWI_IP {

    public List<Claims_Inquiry__c> toUpdate = new List<Claims_Inquiry__c>();
    public List<Claims_Inquiry__c> autoUpdate = new List<Claims_Inquiry__c>();
    List<Claims_Inquiry__c> billNotFound = new List<Claims_Inquiry__c>();

  /***** Created by Integra Developers to check the Claims Inquiry before creating ARAG *******/
  
    //create two methods -----> 1. For inserting new CWI for the bill 2. For linking the bill to exisiting CWI
    // 1. For inserting new CWI for the bill ---> get all the values and insert them
    
    // 2. For linking the bill to exisiting CWI
    
    public ClaimsInquiryToCWI_IP(){
        
    }
    
    
    public void runTrigger(List<Claims_Inquiry__c> toFilter){
        

        for (Claims_Inquiry__c cl : toFilter){
            
          if(cl.Bill_Number__c != null && cl.Create_ARAG__c != true){
                autoUpdate.add(cl);
            }
        }
        System.debug('Number of records in queue'+toUpdate.size());

        // get bill number from the list 

        if(autoUpdate.size() > 0){
            getBills(autoUpdate);
        }
    }



    private void getBills(List<Claims_Inquiry__c> toUpdate){
        
         Map<String, Claims_Inquiry__c> getBillNumbers = new Map<String, Claims_Inquiry__c>();

         // place all the bills to update in a map
        for (Claims_Inquiry__c cl : toUpdate){
            getBillNumbers.put(cl.Bill_Number__c , cl);
        }
        
        // search whether the bill exists in the system
        
        Map<String,ARAG__c> billQueryResult = new Map<String,ARAG__c>();
        List<ARAG__c> checkExistingBill = [Select Id, Bill__c from ARAG__c where Bill__c =: getBillNumbers.keySet()];
        
        
        if(checkExistingBill.size() > 0){
            
            for(ARAG__c ar : checkExistingBill){
            
            Boolean isPresent = false;
            // if the map is not empty, check whether the bill has been already added ---> to check if a duplicate bill exists
            if(!billQueryResult.isEmpty()){
                // search for the bill key value
                if (billQueryResult.containsKey(ar.Bill__c)){
                    // same bill number found in already added map, so let go of this bill
                    System.debug('Bill already exists in the map. No need to add this bill');
                }else{
                    // no duplicates found. Add to the map
                    
                    billQueryResult.put(ar.Bill__c, ar);
                }
            } else {
               // map is empty. go ahead and the first bill
                billQueryResult.put(ar.Bill__c, ar);
            }
        }
            
        }

        for(String s : getBillNumbers.keySet()){
            
            if(checkExistingBill.size() > 0){

                // this is an exisitng ARAG. No need to add it. Skip
                System.debug('Bill already exists in ARAG');
                getBillNumbers.get(s).Bill_Related__c = billQueryResult.get(s).Id;
            }
             
        }

        for(Claims_Inquiry__c c : toUpdate){

            if(billQueryResult.containsKey(c.Bill_Number__c)){
                // the bill exists, so don't do anything

                continue;
            } else{

                billNotFound.add(c);
            }
        }

        if(billNotFound.size() > 0){
            createARAG(billNotFound);
        }
    }
        
        
        
        // billQueryResult contains only bill that exists in the system. Compare it with the first map and extract the ones to be updated/created
    
    private void createARAG(List<Claims_Inquiry__c> newList){

        List<ARAG__c> createNewARAG = new List<ARAG__c>();
        Map<String, Claims_Inquiry__c> billMap = new Map<String, Claims_Inquiry__c>();

        for(Claims_Inquiry__c ci : newList){

            billMap.put(ci.Bill_Number__c, ci);

            String billNumber = '';
                String providerName = '';
                String planName = '';
                System.debug('Checking whether all needed strings are not empty');
                providerName = ci.Account_Name__c;
                System.debug('Provider Name is' +providerName);
                planName = ci.Health_Plan_Name__c;
                System.debug('Payor Name is' +planName);
                billNumber = ci.Bill_Number__c;
                System.debug('Bill Number is' +billNumber);
                createNewARAG.add(new ARAG__c(Payor_Family_text__c = planName, Provider__c = providerName, Bill__c = billNumber, Link_to_Bill__c = ci.Link_to_Bill__c,
                                                    OwnerId = UserInfo.getUserId(), Worked_By__c = ci.Assigned_To__c));

                ci.Create_ARAG__c = true;
        }

        // start insert operations
        for (ARAG__c a : createNewARAG){
            
            // loop through each insert
            Database.SaveResult MySaveResult = Database.Insert(a, false);
            if(MySaveResult.isSuccess()){
                // get the id and update it to the map
                billMap.get(a.Bill__c).Bill_Related__c = MySaveResult.getId();
            } else {
                // error happened during insert
                System.debug('Get Errors'+MySaveResult.getErrors());
            }
        }


    } 
}