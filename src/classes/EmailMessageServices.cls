public with sharing class EmailMessageServices {

	public static final String REOPENED_STATUS = 'New';

	public static void checkToReopenCases(Map<Id,EmailMessage> emailMessageMap){
		
		
		//Gather all case ids from incoming emails
		Set<Id> caseIds = new Set<Id>();
		for (EmailMessage em : emailMessageMap.values()){
			if (em.Incoming){
				caseIds.add(em.ParentId);
			}
		}
		
		//Get all closed cases related to incoming emails
		Map<Id, Case> completedCaseMap = new Map<Id, Case>([
			SELECT
				Id,
				Status,
				Owner.Email,
				Reason,
				CAT__c
			FROM Case
			WHERE IsClosed = true
			AND Id IN :caseIds
		]);
		
		//If there are none, return
		if (completedCaseMap.keySet().isEmpty()){
			return;
		}
		
		//Set all closed cases receiving emails to status = "New" unless they were sent by the Case Owner
		List<Case> casesToUpdate = new List<Case>();
		List<CAT__c> catsToUpdate = new List<CAT__c>();
		for (EmailMessage em : emailMessageMap.values()){
			if(completedCaseMap.containsKey(em.ParentId)){
				Case c = completedCaseMap.get(em.ParentId);
				if(c.Owner.Email != em.FromAddress){
					c.Status = REOPENED_STATUS;
					casesToUpdate.add(c);
				}
			}
		}
		
		update casesToUpdate;
		
	}

	public static void updateCATStatus(Map<Id,EmailMessage> emailMessageMap){
		
		
		//Gather all case ids from incoming emails
		Set<Id> caseIds = new Set<Id>();
		for (EmailMessage em : emailMessageMap.values()){
			if (em.Incoming){
				caseIds.add(em.ParentId);
			}
		}
		
		//Get all closed cases related to incoming emails
		Map<Id, Case> credCaseMap = new Map<Id, Case>([
			SELECT
				Id,
				Status,
				Owner.Email,
				Reason,
				CAT__c
			FROM Case
			WHERE Reason = 'Missing Documents'
			AND Id IN :caseIds
		]);
		
		//If there are none, return
		if (credCaseMap.keySet().isEmpty()){
			return;
		}
		
		//Set all closed cases receiving emails to status = "New" unless they were sent by the Case Owner
		List<CAT__c> catsToUpdate = new List<CAT__c>();
		for (EmailMessage em : emailMessageMap.values()){
			if(credCaseMap.containsKey(em.ParentId)){
				Case c = credCaseMap.get(em.ParentId);
				CAT__c cat = [Select id from CAT__c where id =: c.CAT__c LIMIT 1];
				cat.Credentialing_Status__c = 'Provider responded';
				catsToUpdate.add(cat);
			}
		}

		try{
			update catsToUpdate;	
		}
		catch (Exception e) {
			System.debug('Could not update CAT from Email: ' + e);
		}
		
	}
}