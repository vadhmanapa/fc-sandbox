@isTest
public class PublicKnowledgeArticleControllerTest {

    static Tutorial__kav testArticle {get;set;}
    static PublicKnowledgeArticleController pageCon {get;set;}
    
    static testMethod void PublicKnowledgeArticleController() {
        
        init();
        
        PageReference pageRef = Page.PublicKnowledgeArticle;
        pageRef.getParameters().put('aId',testArticle.KnowledgeArticleId);
        Test.setCurrentPage(pageRef);
        
        pageCon = new PublicKnowledgeArticleController();
        PublicKnowledgeServices.setPageCookie();
        pageCon.init();
        
        system.assertEquals(testArticle.KnowledgeArticleId,pageCon.article.KnowledgeArticleId);        
    }
    
    static void init() {
        
        //Create new article
		testArticle = new Tutorial__kav(
			Title = 'Test Title',
	        Summary = 'Test summary',
	        URLName = 'TestTitle'
		);
		insert testArticle;
        
        //Publish article
		testArticle = [SELECT KnowledgeArticleId FROM Tutorial__kav WHERE Id = :testArticle.Id];
		KbManagement.PublishingService.publishArticle(testArticle.KnowledgeArticleId, true);
    }
}