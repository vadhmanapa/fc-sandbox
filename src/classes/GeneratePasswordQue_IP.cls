public with sharing class GeneratePasswordQue_IP {


	public GeneratePasswordQue_IP() {
		
	}

	public void runPasswordGenerator(List<Contact> newTrigger){

		String finalPass;

		for(Contact c : newTrigger){

			if(c.Que_Admin__c == true && c.Que_Password__c == null){

				finalPass = '';
				String queId = c.FirstName;
			//PasswordServices_CS callPasswordGen = new PasswordServices_CS();
			finalPass = createPassword();

			System.debug('The password generated is:'+finalPass);
			
				if(!String.isBlank(queId)) {
					System.debug('Setting username with first and last name');
					c.Que_Unique_Login_Id__c = (queId.substring(0, 1) + c.LastName);
				}else {
					System.debug('Last Name only available. Set it as username');
					c.Que_Unique_Login_Id__c = c.LastName;
				}

				if(finalPass != null){

					c.Que_Password__c = finalPass;
				}
			}
			
		}
	}

	public String createPassword(){

		String newPass = '';
		Integer minLength = 8;
		Boolean IsExclamationPresent;
		
		do{
			newPass = PasswordServices_CS.randomPassword(minLength); // Levaraging Clear password generator

			if(newPass.contains('!')){
				IsExclamationPresent = true;
			} else {
				IsExclamationPresent = false;
			}
		} while(IsExclamationPresent);

		return newPass;
			
		
	}

}