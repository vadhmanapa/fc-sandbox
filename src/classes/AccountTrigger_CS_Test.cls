@isTest
private class AccountTrigger_CS_Test {
    static boolean isInsert;
    static boolean isUpdate;
    static boolean isDelete;
    static boolean isUndelete;
    static boolean isBefore;
    static boolean isAfter;
    
    static string street1 = '111 S Test Street';
    static string city1 = 'Test';
    static string state1 = 'TN';
    static string postalCode1 = '22222';
    static decimal lat1 = 1;
    static decimal lng1 = -1;
    
    static string street2 = '789 N Abc Rd';
    static string state2 = 'MN';
    static decimal lat2 = 40;
    static decimal lng2 = -40;
    //Geocoding test data (CaSe SeNsItIvE!): 
    //1) ADDRESS Street : 111 S Test Street, City : Test, State : TN, Zip : 22222
    //1) RETURNS Lng : -1, Lat : 1
    
    //2) ADDRESS Street : 789 N Abc Rd, State : MN
    //2) RETURNS Lng : -40, Lat: 40
    
    static testMethod void AccountTriggerTest_GeocodeAccountAddress_Insert() {
        //Insert Account with Type__c = 'Provider' and geocoding test address (1), assert that the Account.Corporate_Geolocation__c lat and lng fields match the (1) 
        //coordinates after insert. (Account.Corporate_Geolocation__Latitude__s, Account.Corporate_Geolocation__Longitude__s)
        Account[] testAccts = generateAccounts(1, true);
        setAddress1(testAccts);
        //Has @future method, use start and stop tests
        test.startTest();
        insert testAccts;
        test.stopTest();
        
        Account acctToAssert = [SELECT Corporate_Geolocation__Latitude__s, Corporate_Geolocation__Longitude__s FROM Account WHERE Id = :testAccts.get(0).Id];
        system.assertEquals(lat1, acctToAssert.Corporate_Geolocation__Latitude__s);
        system.assertEquals(lng1, acctToAssert.Corporate_Geolocation__Longitude__s);
    }
    //140218 Can't appropriately test due to inability to use startTest/stopTest more than once during a test. (insert geocoding doesn't run until after 
    //stop test, overwriting null geocode values from update)      
    /*
    static testMethod void AccountTriggerTest_GeocodeAccountAddress_UpdateNoAddressChange() {
        Account[] testAccts = generateAccounts(1, true);
        setAddress1(testAccts);
        insert testAccts;
        
        //Update Account geocode fields to null, assert that the account has null geolocation coordinates after update (Coordinates are only updated if the address is changed)
        for(Account testAcct : testAccts) {
            testAcct.Name = 'newName';
        }
        //Has @future method, use start and stop tests
        test.startTest();
        update testAccts;
        test.stopTest();
        
        Account acctToAssert = [SELECT Corporate_Geolocation__Latitude__s, Corporate_Geolocation__Longitude__s FROM Account WHERE Id = :testAccts.get(0).Id];
        system.assertEquals(null, acctToAssert.Corporate_Geolocation__Latitude__s);
        system.assertEquals(null, acctToAssert.Corporate_Geolocation__Longitude__s);
    }
    */
    static testMethod void AccountTriggerTest_GeocodeAccountAddress_UpdateAddressChange() {
        Account[] testAccts = generateAccounts(1, true);
        setAddress1(testAccts);
        insert testAccts;

        //Update Account with geocoding test address (2), assert that the Account.Corporate_Geolocation__c lat and lng fields match the (2) coordinates after update.
        setAddress2(testAccts);
        
        //Has @future method, use start and stop tests
        test.startTest();
        update testAccts;
        test.stopTest();
        
        Account acctToAssert = [SELECT Corporate_Geolocation__Latitude__s, Corporate_Geolocation__Longitude__s FROM Account WHERE Id = :testAccts.get(0).Id];
        system.assertEquals(lat2, acctToAssert.Corporate_Geolocation__Latitude__s);
        system.assertEquals(lng2, acctToAssert.Corporate_Geolocation__Longitude__s);
    }
    
    static testMethod void AccountTriggerTest_NonProvider_Insert() {
        //Insert Account with Type__c != 'Provider' and geocoding test address (1), assert that the Account.Corporate_Geolocation__c lat and lng fields are null
        Account[] testAccts = generateAccounts(1, false);
        setAddress1(testAccts);
        
        //Has @future method, use start and stop tests
        test.startTest();
        insert testAccts;
        test.stopTest();
        
        Account acctToAssert = [SELECT Corporate_Geolocation__Latitude__s, Corporate_Geolocation__Longitude__s FROM Account WHERE Id = :testAccts.get(0).Id];
        system.assertEquals(null, acctToAssert.Corporate_Geolocation__Latitude__s);
        system.assertEquals(null, acctToAssert.Corporate_Geolocation__Longitude__s);        

    }   
    static testMethod void AccountTriggerTest_NonProvider_Update() {
        Account[] testAccts = generateAccounts(1, false);
        setAddress1(testAccts);
        insert testAccts;
        
        //Update Account with geocoding test address (2), assert that the Account.Corporate_Geolocation__c lat and lng fields are null.
        setAddress2(testAccts);
        //Has @future method, use start and stop tests
        test.startTest();
        update testAccts;
        test.stopTest();
        
        Account acctToAssert = [SELECT Corporate_Geolocation__Latitude__s, Corporate_Geolocation__Longitude__s FROM Account WHERE Id = :testAccts.get(0).Id];
        system.assertEquals(null, acctToAssert.Corporate_Geolocation__Latitude__s);
        system.assertEquals(null, acctToAssert.Corporate_Geolocation__Longitude__s);
    }
    //WARNING: Check-In makes an @future call for every record, limits reached at 10 records
    static testMethod void AccountTriggerTest_Bulk_Insert() {
        //TODO: Insert 2 Accounts with Type__c != 'Provider', 9 with type__c = 'Provider' and geocoding test address (1), and 9 with type__c = 'Provider' and geocoding test address (2). 
        //Assert that 2 records have null Account.Corporate_Geolocation__c lat and lng fields, 9 have Account.Corporate_Geolocation__c lat and lng fields that match the (1) coordinates, 
        //and 9 have Account.Corporate_Geolocation__c lat and lng fields that match the (2) coordinates after insert.
        Account[] acctsToInsert = generateAccounts(2, false);
        
        Account[] address1Accts = generateAccounts(9, true);
        setAddress1(address1Accts);
        acctsToInsert.addAll(address1Accts);
        
        Account[] address2Accts = generateAccounts(9, true);
        setAddress2(address2Accts);
        acctsToInsert.addAll(address2Accts);
        for(Account acct : acctsToInsert) {
            system.debug(acct.ShippingStreet+', '+acct.ShippingCity+', '+acct.ShippingState+' '+acct.ShippingPostalCode);
        }
        //Has @future method, use start and stop tests
        test.startTest();
        insert acctsToInsert;
        test.stopTest();
        
        List<Account> acctsToAssert = [SELECT ShippingStreet, Corporate_Geolocation__Latitude__s, Corporate_Geolocation__Longitude__s FROM Account];
        for(Account acctToAssert: acctsToAssert) {
            system.debug(acctToAssert.ShippingStreet+', ('+acctToAssert.Corporate_Geolocation__Latitude__s+','+acctToAssert.Corporate_Geolocation__Longitude__s+')');        
            if(acctToAssert.ShippingStreet == null) {
                system.assertEquals(null, acctToAssert.Corporate_Geolocation__Latitude__s);
                system.assertEquals(null, acctToAssert.Corporate_Geolocation__Longitude__s);
            }else if(acctToAssert.ShippingStreet == street1) {
                system.assertEquals(lat1, acctToAssert.Corporate_Geolocation__Latitude__s);
                system.assertEquals(lng1, acctToAssert.Corporate_Geolocation__Longitude__s); 
            }else if(acctToAssert.ShippingStreet == street2) {
                system.assertEquals(lat2, acctToAssert.Corporate_Geolocation__Latitude__s);
                system.assertEquals(lng2, acctToAssert.Corporate_Geolocation__Longitude__s);
            }
        }       
    }
    
    static testMethod void AccountTriggerTest_Bulk_Update() {
        Account[] acctsToInsert = generateAccounts(2, false);
        
        Account[] address1Accts = generateAccounts(9, true);
        setAddress1(address1Accts);
        acctsToInsert.addAll(address1Accts);
        
        Account[] address2Accts = generateAccounts(9, false);
        setAddress2(address2Accts);
        acctsToInsert.addAll(address2Accts);
        insert acctsToInsert;
        
        //Update all 20 records to have Type__c = 'Provider' and geocoding test address (1). Assert that all 20 records have Account.Corporate_Geolocation__c lat and lng fields that match the (1) coordinates
        for(Account acct : acctsToInsert) {
            acct.Type__c = 'Provider';   
            acct.Type_Of_Provider__c = 'providerType';      
        }
        setAddress1(acctsToInsert);
        //Has @future method, use start and stop tests
        test.startTest();
        update acctsToInsert;
        test.stopTest();
                
        List<Account> acctsToAssert = [SELECT Corporate_Geolocation__Latitude__s, Corporate_Geolocation__Longitude__s FROM Account];
        for(Account acctToAssert : acctsToAssert) {
            system.assertEquals(lat1, acctToAssert.Corporate_Geolocation__Latitude__s);
            system.assertEquals(lng1, acctToAssert.Corporate_Geolocation__Longitude__s);
        }
    }
    
    static testMethod void AccountTriggerTest_NullValues() {
        //TODO: Insert Account with Type__c = 'Provider' and blank address fields. Verify that the test does not fail and assert that Account.Corporate_Geolocation__c lat and lng fields are null
        Account[] testAccts = generateAccounts(1, true);
        try {
            insert testAccts;
            system.assert(true);
        }catch(Exception e) {
            system.debug(e.getMessage());
            system.assert(false);   
        }
        
    }
    
    static List<Account> generateAccounts(Integer cnt, boolean isProvider) {
        Account[] testAccts = new List<Account>();
        for(integer i = 0; i < cnt; i++) {
            Account testAcct = new Account();
            testAcct.Name = 'testAccntName'+i;
            if(isProvider) {
                testAcct.Type__c = 'Provider';
                testAcct.Type_Of_Provider__c = 'providerType';
            }else {
                testAcct.Type__c = 'Not Provider';
            }
            testAccts.add(testAcct);    
        }
        return testAccts;
    }
    static void setAddress1(Account[] testAccts) {
        for(Account testAcct : testAccts) {
            testAcct.ShippingStreet = street1;
            testAcct.ShippingCity = city1;
            testAcct.ShippingState = state1;
            testAcct.ShippingPostalCode = postalCode1;
        }
    }
    static void setAddress2(Account[] testAccts) {
        for(Account testAcct : testAccts) {
            testAcct.ShippingStreet = street2;
            testAcct.ShippingCity = null;
            testAcct.ShippingState = state2;
            testAcct.ShippingPostalCode = null;
        }
    }
    
    //Used for increasing test coverage percentage, since not all methods are guaranteed to be used.
    
    static testmethod void AccountTriggerTest_triggerContexts() {
        Map<Id,SObject> tempMap = new Map<Id,SObject>();
        List<SObject> tempList = new List<SObject>();           
        AccountTriggerDispatcher_CS dispatch;
            
        //before insert 
        resetBooleans();
        isInsert = true;
        isBefore = true;    
        dispatch = new AccountTriggerDispatcher_CS(tempMap, tempMap, tempList, tempList, isInsert, isUpdate, isDelete, isUndelete, isBefore, isAfter);     
        
        //after insert
        resetBooleans();
        isInsert = true;
        isAfter = true;
        dispatch = new AccountTriggerDispatcher_CS(tempMap, tempMap, tempList, tempList, isInsert, isUpdate, isDelete, isUndelete, isBefore, isAfter);     

        //before update
        resetBooleans();
        isUpdate = true;
        isBefore = true;
        dispatch = new AccountTriggerDispatcher_CS(tempMap, tempMap, tempList, tempList, isInsert, isUpdate, isDelete, isUndelete, isBefore, isAfter);     

        //after update
        resetBooleans();
        isUpdate = true;
        isAfter = true;
        dispatch = new AccountTriggerDispatcher_CS(tempMap, tempMap, tempList, tempList, isInsert, isUpdate, isDelete, isUndelete, isBefore, isAfter);     
        
        //before delete
        resetBooleans();
        isDelete = true;
        isBefore = true;
        dispatch = new AccountTriggerDispatcher_CS(tempMap, tempMap, tempList, tempList, isInsert, isUpdate, isDelete, isUndelete, isBefore, isAfter);     
        
        //after delete
        resetBooleans();
        isDelete = true;
        isAfter = true;
        dispatch = new AccountTriggerDispatcher_CS(tempMap, tempMap, tempList, tempList, isInsert, isUpdate, isDelete, isUndelete, isBefore, isAfter);     
        
        //after undelete
        resetBooleans();
        isUndelete = true;
        isAfter = true;
        dispatch = new AccountTriggerDispatcher_CS(tempMap, tempMap, tempList, tempList, isInsert, isUpdate, isDelete, isUndelete, isBefore, isAfter);             
    } 
    static void resetBooleans() {
        isInsert = false;
        isUpdate = false;
        isDelete = false;
        isUndelete = false;
        isBefore = false;
        isAfter = false;
    }
    
}