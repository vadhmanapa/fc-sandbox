@isTest
private class ComponentControllerBaseTest_IP {

	private static PageControllerBase_IP pController;
	private static ComponentControllerBase_IP comController;
	
	@isTest static void setComponentController() {
		pController = new PageControllerBase_IP();
		comController = new ComponentControllerBase_IP();

		Test.startTest();
		comController.pageController = pController;
		comController.key = 'Test';
		Test.stopTest();

		System.assertNotEquals('', comController.key);
		System.assertNotEquals(null, comController.pageController);
	}
	
}