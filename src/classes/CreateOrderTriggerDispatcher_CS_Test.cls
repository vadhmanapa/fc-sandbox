/**
 *  @Description Test class for CreateOrderTriggerDispatcher_CS
 *  @Author Cloud Software LLC
 *  Revision History: 
 *		12/08/2015 - karnold - Change status check to refelct new functionality. Status will always be Use Old Routing at this point.
 *		12/08/2015 - karnold - Added CreateOrderTriggerDispatcher_MatchPatientWithNameDob test for new code 
 *		12/23/2015 - karnold - Removed references to DME_Category__c. Fixed failing tests. Formatted code.
 *
 */
@isTest
private class CreateOrderTriggerDispatcher_CS_Test {


	static public final List<String> orderFields = new List<String> {
		'Id',
		'Name',
		//**Other order data**
		'Accepted_By__c',
		'Accepted_Date__c',
		'Accept_Order__c',
		'Comments__c',
		'Provider_Contact__c',
		'Canceled_Order__c',
		'Delivery_Date_Time__c',
		'Delivery_Method__c',
		'Entered_By__c',
		'Estimated_Delivery_Time__c',
		'Legacy_Orders__c',
		'Manually_Assigned__c',
		'Order_Image__c',
		'Other_Canceled_Orders__c',
		'Referred__c',
		'Referred_By_First_Name__c',
		'Referred_By_Last_Name__c',
		'Referred_By_Phone__c',
		'Authorization_Number__c',
		'Provider__c',
		'Re_Assign_Order__c',
		'Status_of_Order__c',
		'Tracking_Number__c',
		'Status__c',
		//**Delivery info**
		'Street__c',
		'City__c',
		'State__c',
		'Zip__c',
		//**Patient and recipient info**
		'Patient_First_Name__c',
		'Patient_Last_Name__c',
		'Patient_Name__c',
		'Recipient_First_Name__c',
		'Recipient_Last_Name__c',
		'Patient_Phone_Number__c',
		'Patient_Mobile_Phone_Number__c',
		'Patient_Email_Address__c',
		'Patient_Date_of_Birth__c',
		'Patient_ID_Number__c',
		'Patient_Medicaid_ID__c',
		'Patient_Medicare_ID__c',
		'Patient_Native_Language__c',
		'Patient_Street__c',
		'Patient_City__c',
		'Patient_State__c',
		'Patient_Zip_Code__c',
		'Patient_Country__c',
		'Recipient_Country__c',
		'Recipient_Date_of_Birth__c',
		'Recipient_Email__c',
		'Recipient_Email_Address__c',
		'Recipient_Native_Language__c',
		'Recipient_Phone__c',
		'Recipient_Phone_Number__c',
		'Recipient_Cell_Phone__c',
		'Recipient_Relationship__c',
		//**Plan Patient Lookup**
		'Plan_Patient__r.ID_Number__c',
		'Plan_Patient__r.Date_of_Birth__c',
		'Plan_Patient__r.First_Name__c',
		'Plan_Patient__r.Last_Name__c',
		'Comments__c'
	};

	/******* Test Parameters *******/
	static private final Integer N_DMES = 10; //Should be at least 10

	/******* Provider *******/
	static private List<Account> providers;
	static private List<Contact> providerUsers;

	/******* Payer *******/
	static private List<Account> payers;
	static private List<Contact> payerUsers;

	/******* Patients *******/
	static private List<Patient__c> patients;
	static private List<Plan_Patient__c> planPatients;

	/******* DME *******/
	static private DME_Settings__c dmeSettings;
	static private List<DME__c> dmes;

	/******* Orders *******/
	static private List<New_Orders__c> legacyOrders;
	static private List<Order__c> orders;

	static private List<Order_Assignment__c> orderAssignments;

	static private List<Provider_Locations__c> providerLocations;
	static private List<Provider_Location__c> providerLocationsNew;

	static testMethod void CreateOrderTriggerDispatcher() {
		init();

		New_Orders__c legacyOrder = sampleLegacyOrder();
		legacyOrder.Provider_Location__c = providerLocations[0].Id;
		insert legacyOrder;

		SoqlBuilder newOrderQuery = new SoqlBuilder()
			.selectx(orderFields)
			.fromx('Order__c')
			.wherex(new FieldCondition('Legacy_Orders__c').equals(legacyOrder.Id))
			.limitx(1);

		List<Order__c> newOrders = Database.query(newOrderQuery.toSoql());

		if (newOrders.size() > 0) {
			compareOrders(legacyOrder, newOrders[0]);
		}
		else {
			System.assert(false, 'No orders found.');
		}
	}

	private static testMethod void CreateOrderTriggerDispatcher_MatchPatientWithNameDob() {
		init();

		// Modify order to test a Name and DoB combo match, but not match by member ID
		New_Orders__c legacyOrder = sampleLegacyOrder();
		legacyOrder.Member_Patient_ID__c = planPatients[0].ID_Number__c + '1';
		legacyOrder.D_O_B__c = planPatients[0].Date_of_Birth__c;
		legacyOrder.Patient_Name__c = planPatients[0].Name;
		legacyOrder.Provider_Location__c = ProviderLocations[0].Id;

		System.debug('Legacy Order: ' + legacyOrder);
		System.debug('Plan Patient: ' + planPatients[0]);

		insert legacyOrder;

		SoqlBuilder newOrderQuery = new SoqlBuilder()
			.selectx(orderFields)
			.fromx('Order__c')
			.wherex(new FieldCondition('Legacy_Orders__c').equals(legacyOrder.Id) )
			.limitx(1);

		List<Order__c> newOrders = Database.query(newOrderQuery.toSoql());

		System.debug('Old Order: ' + legacyOrder);
		System.debug('New Orders: ' + newOrders);

		if (newOrders.size() > 0) {
			compareOrders(legacyOrder, newOrders[0]);
		} else {
			System.assert(false, 'No orders found.');
		}
	}

	static testMethod void CreateOrderTriggerDispatcher_ParseName() {

		List<String> parse;

		String testName1 = 'John Doe';
		String testName2 = 'Doe, John';
		String testName3 = 'JohnDoe';

		parse = CreateOrderTriggerDispatcher_CS.parseName(testName1);
		System.assertEquals('John', parse[0]);
		System.assertEquals('Doe', parse[1]);
		System.assertEquals('John Doe', parse[2]);

		parse = CreateOrderTriggerDispatcher_CS.parseName(testName2);
		System.assertEquals('John', parse[0]);
		System.assertEquals('Doe', parse[1]);
		System.assertEquals('John Doe', parse[2]);

		parse = CreateOrderTriggerDispatcher_CS.parseName(testName3);
		System.assertEquals('', parse[0]);
		System.assertEquals('JohnDoe', parse[1]);
		System.assertEquals('JohnDoe', parse[2]);
	}

	static testMethod void CreateOrderTriggerDispatcher_GetCPTCodes() {
		init();

		New_Orders__c legacyOrder = sampleLegacyOrder();

		Map<String, Decimal> CPTProducts = CreateOrderTriggerDispatcher_CS.getCPTCodes(legacyOrder);

		System.assert(CPTProducts.containsKey(dmes[0].Name), 'CPT Code 1 not found.');
		System.assert(CPTProducts.containsKey(dmes[1].Name), 'CPT Code 2 not found.');
		System.assert(CPTProducts.containsKey(dmes[2].Name), 'CPT Code 3 not found.');
		System.assert(CPTProducts.containsKey(dmes[3].Name), 'CPT Code 4 not found.');
		System.assert(CPTProducts.containsKey(dmes[4].Name), 'CPT Code 5 not found.');
		System.assert(CPTProducts.containsKey(dmes[5].Name), 'CPT Code 6 not found.');
		System.assert(CPTProducts.containsKey(dmes[6].Name), 'CPT Code 7 not found.');
		System.assert(CPTProducts.containsKey(dmes[7].Name), 'CPT Code 8 not found.');
		System.assert(CPTProducts.containsKey(dmes[8].Name), 'CPT Code 9 not found.');
		System.assert(CPTProducts.containsKey(dmes[9].Name), 'CPT Code 10 not found.');

		System.assertEquals(1, CPTProducts.get(dmes[0].Name));
		System.assertEquals(2, CPTProducts.get(dmes[1].Name));
		System.assertEquals(3, CPTProducts.get(dmes[2].Name));
		System.assertEquals(4, CPTProducts.get(dmes[3].Name));
		System.assertEquals(5, CPTProducts.get(dmes[4].Name));
		System.assertEquals(6, CPTProducts.get(dmes[5].Name));
		System.assertEquals(7, CPTProducts.get(dmes[6].Name));
		System.assertEquals(8, CPTProducts.get(dmes[7].Name));
		System.assertEquals(9, CPTProducts.get(dmes[8].Name));
		System.assertEquals(10, CPTProducts.get(dmes[9].Name));
	}

	static testMethod void CreateOrderTriggerDispatcher_UpdateDMELineItemToCompleted() {
		init();

		Test.startTest();
		orders[0].Status__c = 'Delivered';
		update orders[0];
		Test.stopTest();

		Order_Assignment__c orderAssignment = [
			SELECT Id, Order_Completed__c 
			FROM Order_Assignment__c 
			WHERE Id = :orderAssignments[0].Id
		];

		System.assertEquals(true, orderAssignment.Order_Completed__c);
	}

	public static void compareOrders(New_Orders__c legacyOrder, Order__c newOrder) {

		//Other order data
		System.assertEquals(legacyOrder.Accepted_By__c, newOrder.Accepted_By__c);
		System.assertEquals(legacyOrder.Accepted_Date__c, newOrder.Accepted_Date__c);
		System.assertEquals(legacyOrder.Accept_Order__c, newOrder.Accept_Order__c);
		System.assertEquals(legacyOrder.Notes__c, newOrder.Comments__c);
		System.assertEquals(legacyOrder.Assigned_Contact__c, newOrder.Provider_Contact__c);
		System.assertEquals(legacyOrder.Canceled_Order__c, newOrder.Canceled_Order__c);
		System.assertEquals(legacyOrder.Delivery_Date__c, newOrder.Delivery_Date_Time__c.DateGmt());
		System.assertEquals(legacyOrder.Delivery_Method__c, newOrder.Delivery_Method__c);
		System.assertEquals(legacyOrder.Entered_By__c, newOrder.Entered_By__c);
		System.assertEquals(legacyOrder.Estimated_Delivery_Time__c, newOrder.Estimated_Delivery_Time__c);
		System.assertEquals(legacyOrder.Id, newOrder.Legacy_Orders__c);
		System.assertEquals(legacyOrder.Manually_Assigned__c, newOrder.Manually_Assigned__c);
		System.assertEquals(legacyOrder.Order_Image__c, newOrder.Order_Image__c);
		System.assertEquals(legacyOrder.Other_Canceled_Orders__c, newOrder.Other_Canceled_Orders__c);
		System.assertEquals(legacyOrder.Plan_Order__c, newOrder.Authorization_Number__c);
		System.assertEquals(legacyOrder.Re_Assign_Order__c, newOrder.Re_Assign_Order__c);
		System.assertEquals(legacyOrder.Tracking_Number__c, newOrder.Tracking_Number__c);
		//System.assertEquals(OrderModel_CS.STATUS_USE_OLD_ROUTING, newOrder.Status__c);

		//Referral Info
		if (legacyOrder.Person_placing_the_order__c != null) {
			List<String> parse = CreateOrderTriggerDispatcher_CS.parseName(legacyOrder.Person_placing_the_order__c);
			System.assertEquals(parse[0], newOrder.Referred_By_First_Name__c);
			System.assertEquals(parse[1], newOrder.Referred_By_Last_Name__c);
			System.assertEquals(true, newOrder.Referred__c);
		}
		System.assertEquals(legacyOrder.Physician_Phone__c, newOrder.Referred_By_Phone__c);

		//Delivery info
		System.assertEquals(legacyOrder.Street__c, newOrder.Street__c);
		System.assertEquals(legacyOrder.City__c, newOrder.City__c);
		System.assertEquals(legacyOrder.State__c, newOrder.State__c);
		System.assertEquals(legacyOrder.Zip__c, newOrder.Zip__c);

		//Patient and recipient info
		if (legacyOrder.Patient_Name__c != null) {
			List<String> parse = CreateOrderTriggerDispatcher_CS.parseName(legacyOrder.Patient_Name__c);
			System.assertEquals(parse[0], newOrder.Patient_First_Name__c);
			System.assertEquals(parse[1], newOrder.Patient_Last_Name__c);
			System.assertEquals(parse[2], newOrder.Patient_Name__c);
			System.assertEquals(parse[0], newOrder.Recipient_First_Name__c);
			System.assertEquals(parse[1], newOrder.Recipient_Last_Name__c);
		}

		// Assert now checks that the order either found a match with an id number OR a patient name and date of birth
		System.assert(legacyOrder.Member_Patient_ID__c == newOrder.Plan_Patient__r.ID_Number__c ||
		(legacyOrder.Patient_Name__c == newOrder.Plan_Patient__r.First_Name__c + ' ' + newOrder.Plan_Patient__r.Last_Name__c &&
		 legacyOrder.D_O_B__c == newOrder.Plan_Patient__r.Date_of_Birth__c)
		);

		//Plan Patient lookup
		System.assertEquals(legacyOrder.Patient_Phone__c, newOrder.Patient_Phone_Number__c);
		System.assertEquals(null, newOrder.Patient_Mobile_Phone_Number__c);
		System.assertEquals(null, newOrder.Patient_Email_Address__c);
		System.assertEquals(legacyOrder.D_O_B__c, newOrder.Patient_Date_of_Birth__c);
		System.assertEquals(legacyOrder.Member_Patient_ID__c, newOrder.Patient_ID_Number__c);
		System.assertEquals(legacyOrder.Medicaid_ID__c, newOrder.Patient_Medicaid_ID__c);
		System.assertEquals(legacyOrder.Medicare_ID__c, newOrder.Patient_Medicare_ID__c);
		System.assertEquals(null, newOrder.Patient_Native_Language__c);
		System.assertEquals(legacyOrder.Street__c, newOrder.Patient_Street__c);
		System.assertEquals(legacyOrder.City__c, newOrder.Patient_City__c);
		System.assertEquals(legacyOrder.State__c, newOrder.Patient_State__c);
		System.assertEquals(legacyOrder.Zip__c, newOrder.Patient_Zip_Code__c);
		System.assertEquals('United States', newOrder.Patient_Country__c);
		System.assertEquals('United States', newOrder.Recipient_Country__c);
		System.assertEquals(legacyOrder.D_O_B__c, newOrder.Recipient_Date_of_Birth__c);
		System.assertEquals(null, newOrder.Recipient_Email__c);
		System.assertEquals(null, newOrder.Recipient_Email_Address__c);
		System.assertEquals(null, newOrder.Recipient_Native_Language__c);
		System.assertEquals(legacyOrder.Patient_Phone__c, newOrder.Recipient_Phone__c);
		System.assertEquals(legacyOrder.Patient_Phone__c, newOrder.Recipient_Phone_Number__c);
		System.assertEquals(null, newOrder.Recipient_Cell_Phone__c);
		System.assertEquals('Patient', newOrder.Recipient_Relationship__c);

		Map<String, DME_Line_Item__c> dmeCodeToLineItem = new Map<String, DME_Line_Item__c> ();

		//DME Comparison
		for (DME_Line_Item__c li :[
			SELECT
				Name,
				Quantity__c,
				DME__c,
				DME__r.Name,
				Order__c
			FROM DME_Line_Item__c
			WHERE Order__c = :newOrder.Id
		]) {
			dmeCodeToLineItem.put(li.DME__r.Name, li);
		}

		System.debug('Found line items:');
		for (DME_Line_Item__c li : dmeCodeToLineItem.values()) {
			System.debug(li);
		}

		System.assert(dmeCodeToLineItem.containsKey(dmes[0].Name), 'CPT Code 1 not found.');
		System.assert(dmeCodeToLineItem.containsKey(dmes[1].Name), 'CPT Code 2 not found.');
		System.assert(dmeCodeToLineItem.containsKey(dmes[2].Name), 'CPT Code 3 not found.');
		System.assert(dmeCodeToLineItem.containsKey(dmes[3].Name), 'CPT Code 4 not found.');
		System.assert(dmeCodeToLineItem.containsKey(dmes[4].Name), 'CPT Code 5 not found.');
		System.assert(dmeCodeToLineItem.containsKey(dmes[5].Name), 'CPT Code 6 not found.');
		System.assert(dmeCodeToLineItem.containsKey(dmes[6].Name), 'CPT Code 7 not found.');
		System.assert(dmeCodeToLineItem.containsKey(dmes[7].Name), 'CPT Code 8 not found.');
		System.assert(dmeCodeToLineItem.containsKey(dmes[8].Name), 'CPT Code 9 not found.');
		System.assert(dmeCodeToLineItem.containsKey(dmes[9].Name), 'CPT Code 10 not found.');

		System.assertEquals(1, dmeCodeToLineItem.get(dmes[0].Name).Quantity__c);
		System.assertEquals(2, dmeCodeToLineItem.get(dmes[1].Name).Quantity__c);
		System.assertEquals(3, dmeCodeToLineItem.get(dmes[2].Name).Quantity__c);
		System.assertEquals(4, dmeCodeToLineItem.get(dmes[3].Name).Quantity__c);
		System.assertEquals(5, dmeCodeToLineItem.get(dmes[4].Name).Quantity__c);
		System.assertEquals(6, dmeCodeToLineItem.get(dmes[5].Name).Quantity__c);
		System.assertEquals(7, dmeCodeToLineItem.get(dmes[6].Name).Quantity__c);
		System.assertEquals(8, dmeCodeToLineItem.get(dmes[7].Name).Quantity__c);
		System.assertEquals(9, dmeCodeToLineItem.get(dmes[8].Name).Quantity__c);
		System.assertEquals(10, dmeCodeToLineItem.get(dmes[9].Name).Quantity__c);

	}

	public static New_Orders__c sampleLegacyOrder() {
		//Create a New_Orders__c for insertion
		New_Orders__c legacyOrder = new New_Orders__c(
			Accepted_By__c = providerUsers[0].Id,
			Accepted_Date__c = DateTime.now(),
			//Accepted_Date_in_Date_Format__c = 
			//Accepted_Order_Timing__c = 
			Accept_Order__c = true,
			Account_c__c = payers[0].Id,
			//Account_Name__c = 
			//Add_Comment__c = 
			Assigned_Contact__c = providerUsers[0].Id,
			//Auth_End_Period__c = 
			//Auth_Start_Period__c = 
			//Cal_Delivery_Date__c = 
			//Canceled_Order__c = 
			Cancelled_Order__c = true,
			City__c = 'Phoenix',
			//CPT_Code1__c = dmes[0].Name,
			//CPT_Code_2__c = dmes[1].Name,
			//CPT_Code_3__c = dmes[2].Name,
			//CPT_Code_4__c = dmes[3].Name,
			//CPT_Code_5__c = dmes[4].Name,
			//CPT_Code_6__c = dmes[5].Name,
			//CPT_Code_7__c = dmes[6].Name,
			//CPT_Code_8__c = dmes[7].Name,
			//CPT_Code_9__c = dmes[8].Name,
			//CPT_Code_10__c = dmes[9].Name,
			D_O_B__c = Date.newInstance(2014, 12, 25),
			//Delivered_Not__c = 
			//Delivered_Order_Timing__c = 
			Delivery_date__c = DateTime.now().dateGmt(),
			Delivery_Method__c = 'Store Personnel',
			//Delivery_Ticket__c = 
			Entered_By__c = payerUsers[0].Id,
			Estimated_Delivery_Time__c = DateTime.now(),
			//Estimated_vs_Delivered_Time__c = 
			//Grab_Bars__c = 
			Health_Plans__c = 'testPlan',
			Manually_Assigned__c = true,
			Medicaid_ID__c = 'MedicaidID',
			Medicare_ID__c = 'MedicareID',
			Medicare_Primary__c = true,
			Member_Patient_ID__c = planPatients[0].ID_Number__c,
			//Needs_Attention__c = 
			Notes__c = 'Some Notes',
			//Open_Orders_Age__c = 
			//Order_Active__c = 
			//Order_Created_Date__c = 
			//Order_Created_On__c = 
			//Order_Image__c = 
			//Order_Placed_to_Today__c = 
			Other_Canceled_Orders__c = '123CancelledOrderId',
			Patient_Name__c = 'testFirst TestPatient',
			Person_Placing_Order__c = 'Test Recipient',
			Person_placing_the_order__c = 'Test Recipient',
			Patient_Phone__c = '111-111-1111',
			Physician_First_Name__c = 'PhysicianFirst',
			Physician_Last_Name__c = 'PhysicianLast',
			Physician_Phone__c = '222-222-2222',
			Plan_Order__c = '123AuthNumber',
			Provider__c = providers[0].Id,
			//Provider_Workflow_Email__c = 
			Re_Assign_Order__c = true,
			State__c = 'AZ',
			Status_of_Order__c = 'In Progress',
			//Store_Name__c = 
			Street__c = 'Fremont',
			//Timeframe_From_Order_Accept_to_Deliver__c = 
			//Time_to_Accept_Order__c = 
			Tracking_Number__c = '123TrackMe',
			//Units1__c = 10,
			//Units_10__c = 10,
			//Units_2__c = 10,
			//Units_3__c = 10,
			//Units_4__c = 10,
			//Units_5__c = 10,
			//Units_6__c = 10,
			//Units_7__c = 10,
			//Units_8__c = 10,
			//Units_9__c = 10,
			Units_for_OCR__c = '1,2 3,,4 5  6,7, 8 9, 10',
			//Worflow_Update_Account_Name__c = 
			//Workflow_Update_for_Inactive__c = 
			Zip__c = '12345',
			Provider_Location_New__c = providerLocationsNew[0].Id
		);
		String CPTCodeString = dmes[0].Name + ', ' + dmes[1].Name + ',' + dmes[2].Name + ' ' + dmes[3].Name + '  ' + dmes[4].Name + ' , ';
		CPTCodeString += dmes[5].Name + ',,' + dmes[6].Name + ', ' + dmes[7].Name + ',' + dmes[8].Name + ' ' + dmes[9].Name;
		legacyOrder.CPT_code_for_OCR__c = CPTCodeString;
		System.debug('CPTCodeString: ' + CPTCodeString);
		return legacyOrder;
	}

	public static void init() {

		dmeSettings = TestDataFactory_CS.generateDMESettings();
		insert dmeSettings;

		dmes = TestDataFactory_CS.generateDMEs(N_DMES);
		insert dmes;

		System.debug('DMEs:');
		for (DME__c dme : dmes) {
			System.debug(dme);
		}

		payers = TestDataFactory_CS.generatePlans('testPlan', 1);
		insert payers;

		payerUsers = TestDataFactory_CS.generatePlanContacts('PlanUser', payers, 1);
		insert payerUsers;

		providers = TestDataFactory_CS.generateProviders('testProvider', 1);
		insert providers;

		providerLocationsNew = TestDataFactory_CS.generateProviderLocations(providers[0].Id, 1);
		insert providerLocationsNew;
		
		providerLocations = new List<Provider_Locations__c>();
		Provider_Locations__c provider_location_old = new Provider_Locations__c();
		provider_location_old.Account__c = providers[0].Id;

		providerLocations.add(provider_location_old);
		insert providerLocations;

		providerUsers = TestDataFactory_CS.generateProviderContacts('ProviderUser', providers, 1);
		insert providerUsers;

		patients = TestDataFactory_CS.generatePatients('TestPatient', 1);
		insert patients;

		planPatients = TestDataFactory_CS.generatePlanPatients(new Map<Id, List<Patient__c>> {
				payers[0].Id => new List<Patient__c> { patients[0] }
		});

		CreateOrderTriggerDispatcher_CS.picklistValToAccountId = new Map<String, Id> ();

		for (Account a : payers) {
			CreateOrderTriggerDispatcher_CS.picklistValToAccountId.put(a.Name, a.Id);
		}

		System.debug('planPatients: ' + planPatients);
		System.debug('picklistValToAccountId: ' + CreateOrderTriggerDispatcher_CS.picklistValToAccountId);

		insert planPatients;

		orders = TestDataFactory_CS.generateOrders(planPatients[0].Id, providers[0].Id, 1);
		insert orders;

		orderAssignments = new List<Order_Assignment__c>();
		orderAssignments.add( TestDataFactory_CS.generateOrderAssignments(orders[0].Id, providers[0].Id, System.now(), System.now()));
		insert orderAssignments;
	}
}