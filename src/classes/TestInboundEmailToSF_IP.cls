/**
* This test class is wriitten by Integra Developers to test InboundEmailToSF_IP.
*/
@isTest
private class TestInboundEmailToSF_IP {
    static testMethod void testMail() {
        // create a new email and envelope object
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        
        // create sample accout
        
        Account acc =  new Account();
        acc.Name = 'Integra Partners';
        acc.Type_Of_Provider__c = 'DME';
        insert acc;
        String accId = acc.Id;
        
        // sample contact info
        
        Contact con = new Contact();
        con.FirstName = 'Test';
        con.LastName = 'Check';
        con.AccountId = accId;
        con.status__c = 'Active';
        con.Email = 'someaddress@email.com';
        insert con;
        
        // setup the data for the email
        List<String> toAdd = new List<String>();
        toAdd.add('claims@salesforce.com');
        toAdd.add('clr@test.com');
        email.subject = 'Test Email';
        email.fromname = 'Test Sender';
        email.messageId = 'test id 4';
        email.fromAddress = 'someaddress@email.com';
        email.toAddresses = toAdd;
        
        // add an attachment
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        attachment.body = blob.valueOf('my attachment');
        attachment.fileName = 'testtextfile.txt';
        attachment.mimeTypeSubType = 'text/plain';
        
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
            
            // call the email service class and test it with the data in the testMethod
            InboundEmailToSF_IP emailProcess = new InboundEmailToSF_IP();
        emailProcess.handleInboundEmail(email, env);
        
        // query for the email the email service created
        Email_Log__c testEmail = [select Id, Email_Subject__c, From_Email_Address__c, Message_Id__c from Email_Log__c where From_Email_Address__c = :email.fromAddress and Message_Id__c = :email.messageId];
        
        System.assertEquals(testEmail.Message_Id__c,'test id 4');
        System.assertEquals(testEmail.From_Email_Address__c,'someaddress@email.com');
        
        // find the attachment
        Attachment a = [select name from attachment where parentId = :testEmail.id];
        
        System.assertEquals(a.name,'testtextfile.txt');
        
        Messaging.InboundEmail email2 = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env2 = new Messaging.InboundEnvelope();
        
        toAdd = new List<String>();
        toAdd.add('claims@salesforce.com');
        email2.subject = 'Test Email 2';
        email2.fromname = 'Test Sender 57';
        email2.messageId = 'test id 09';
        email2.fromAddress = 'nbijfnkjn@email.com';
        email2.toAddresses = toAdd;
        
        emailProcess.handleInboundEmail(email2, env2);
        
        Messaging.InboundEmail email3 = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env3 = new Messaging.InboundEnvelope();
        
        toAdd = new List<String>();
        toAdd.add('clearhelp@salesforce.com');
        email3.subject = 'Test Email 412';
        email3.fromname = 'Test Sender 5217';
        email3.messageId = 'test id 04219';
        email3.fromAddress = 'nbijfnk214jn@email.com';
        email3.toAddresses = toAdd;
        
        emailProcess.handleInboundEmail(email3, env3);
    }
    /* static testMethod void testMail2(){
// Account Sample 2

Account acc1 = new Account();
acc1.Name = 'Integra Partners';
acc1.Type_Of_Provider__c = 'DME';
insert acc1;

// create a new email and envelope object
Messaging.InboundEmail em = new Messaging.InboundEmail() ;
Messaging.InboundEnvelope en = new Messaging.InboundEnvelope();
// setup the data for the email
List<String> toAdd = new List<String>();
toAdd.add('complaints@accessintegra.com');
toAdd.add('test@test.com');
em.subject = 'Test Subject';
em.fromname = 'Test Email Sender';
em.messageId = 'test 123';
em.fromAddress = 'test@email.com';
em.toAddresses = toAdd;

InboundEmailToSF_IP emailProcess = new InboundEmailToSF_IP();
emailProcess.handleInboundEmail(em, en);

Issue_Log__c testEmail = [select Id, Email_Subject__c, From_Email_Address__c, Message_Id__c from Issue_Log__c where From_Email_Address__c = :em.fromAddress and Message_Id__c = :em.messageId];

System.assertEquals(testEmail.Message_Id__c,'test 123', 'Message Id checked');
System.assertEquals(testEmail.From_Email_Address__c,'test@email.com', 'Email Address checked');

}*/
}