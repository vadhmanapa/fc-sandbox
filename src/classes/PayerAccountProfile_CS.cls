public without sharing class PayerAccountProfile_CS extends PayerPortalServices_CS {
	
	private final String pageAccountType = 'Payer';
	
	private AccountServices_CS accServices;

	/********* Page Interface *********/
	public Account thisAccount {get;set;}

	/********* Constructor/Init *********/
	public PayerAccountProfile_CS(){
		
	}

	public PageReference init(){

		///////////////////////////////////
		// Authentication and Permission
		///////////////////////////////////
		
		if(!isSecure && !Test.isRunningTest()){
			return DoLogout();
		}
				
		if(!authenticated){
			return DoLogout();
		}

		if(portalUser.AccountType != pageAccountType){
			return homepage;
		}
		
		///////////////////////////////////
		// Additional init
		///////////////////////////////////
		
		accServices = new AccountServices_CS(portalUser.instance.Id);		
		thisAccount = accServices.cAccount;
		
		return null;
	}
	
}