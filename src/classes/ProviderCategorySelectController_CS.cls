/**
 *  @Description Controller for Provider HCPC Category Select that will manage the Provider Category junction objects.
 *  @Author Cloud Software LLC, Keith Arnold 
 *  Revision History: 
 *      01/06/2016 - karnold - New File
 *      02/18/2016 - sbatchelor - Removed "(Integra)" from picklist options
 *		02/22/2016 - sbatchelor - Separate multi-select picklists for each category's equipment type.
 */
public class ProviderCategorySelectController_CS {
	/* Lists for multi-select */
	public List<SelectOption> availableDMECats {get; set;}
	public List<SelectOption> currentDMECats {get; set;}

	public List<SelectOption> availableAdultCats {get; set;}
	public List<SelectOption> currentAdultCats {get; set;}

	public List<SelectOption> availablePediatricCats {get; set;}
	public List<SelectOption> currentPediatricCats {get; set;}

	private Id providerId;

	/** @description Empty constructor. Function construction is done in the init method. */
	public ProviderCategorySelectController_CS(ApexPages.StandardController controller) {}

	/** @description Constructor function. Initilize categories. */
	public void init() {
		providerId = ApexPages.currentPage().getParameters().get('id');
		currentDMECats = createCurrentDMECats();
		availableDMECats = createAvailableDMECats();

		currentAdultCats = createCurrentAdultCats();
		availableAdultCats = createAvailableAdultCats();

		currentPediatricCats = createCurrentPediatricCats();
		availablePediatricCats = createAvailablePediatricCats();
	}

	/* Save and Delete Methods */

	/** @description Inserts new and deletes old junctions objects.*/
	public void saveChanges() {

		// Turn all of the select options to provider categories
		Set<SelectOption> allCurrentCategories = new Set<SelectOption>();
		allCurrentCategories.addall(currentDMECats);
		allCurrentCategories.addall(currentPediatricCats);
		allCurrentCategories.addall(currentAdultCats);

		// Create new Provider_Category__c
		List<Provider_Category__c> newCatList = new List<Provider_Category__c>();
		for (SelectOption cat : allCurrentCategories) {
			newCatList.add(new Provider_Category__c(
				Name = cat.getLabel(),
				HCPC_Database_Category__c = cat.getValue(),
				Provider__c = providerId
			));
		}

		// Delete old Provider_Category__c
		List<Provider_Category__c> currentCatList = 
				[SELECT Id FROM Provider_Category__c WHERE Provider__c = :providerId];

		// DML
		if (!currentCatList.isEmpty()) {
			delete currentCatList;
		}
		if (!newCatList.isEmpty()) {
			insert newCatList;
		}
	}

	/* Populate SelectLists Methods*/

	/** @description Query for and create available categories. */
	private List<SelectOption> createCurrentDMECats() {
		List<Provider_Category__c> categoryDMEList = [
			SELECT 
				HCPC_Database_Category__r.Id,
				HCPC_Database_Category__r.Equipment_Type__c,
				HCPC_Database_Category__r.Database__c,
				HCPC_Database_Category__r.Name
			FROM Provider_Category__c
			WHERE Provider__c = :providerId
			AND (HCPC_Database_Category__r.Equipment_Type__c = 'DME'
			OR HCPC_Database_Category__r.Equipment_Type__c = 'DME / O&P')
			AND HCPC_Database_Category__r.Database__c = 'Integra'
			ORDER BY Name
		];

		System.debug(System.LoggingLevel.INFO, 'categoryList: ' + categoryDMEList);
		System.debug(System.LoggingLevel.INFO, 'providerId: ' + providerId);

		List<HCPC_Database_Category__c> dbCategoryList = new List<HCPC_Database_Category__c>();
		for (Provider_Category__c cat : categoryDMEList) {
			dbCategoryList.add(cat.HCPC_Database_Category__r);
		}

		return createSelectList(dbCategoryList);
	}

	/** @description Query for and create available categories. */
	private List<SelectOption> createCurrentAdultCats() {
		List<Provider_Category__c> categoryAdultList = [
			SELECT 
				HCPC_Database_Category__r.Id,
				HCPC_Database_Category__r.Equipment_Type__c,
				HCPC_Database_Category__r.Database__c,
				HCPC_Database_Category__r.Name
			FROM Provider_Category__c
			WHERE Provider__c = :providerId
			AND (HCPC_Database_Category__r.Equipment_Type__c = 'Adult O&P'
			OR HCPC_Database_Category__r.Equipment_Type__c = 'General O&P'
			OR HCPC_Database_Category__r.Equipment_Type__c = 'DME / O&P')
			AND HCPC_Database_Category__r.Database__c = 'Integra'
			ORDER BY Name
		];

		System.debug(System.LoggingLevel.INFO, 'categoryList: ' + categoryAdultList);
		System.debug(System.LoggingLevel.INFO, 'providerId: ' + providerId);

		List<HCPC_Database_Category__c> dbCategoryList = new List<HCPC_Database_Category__c>();
		for (Provider_Category__c cat : categoryAdultList) {
			dbCategoryList.add(cat.HCPC_Database_Category__r);
		}

		return createSelectList(dbCategoryList);
	}

	/** @description Query for and create available categories. */
	private List<SelectOption> createCurrentPediatricCats() {
		List<Provider_Category__c> categoryPediatricList = [
			SELECT 
				HCPC_Database_Category__r.Id,
				HCPC_Database_Category__r.Equipment_Type__c,
				HCPC_Database_Category__r.Database__c,
				HCPC_Database_Category__r.Name
			FROM Provider_Category__c
			WHERE Provider__c = :providerId
			AND (HCPC_Database_Category__r.Equipment_Type__c = 'Pediatric O&P'
			OR HCPC_Database_Category__r.Equipment_Type__c = 'General O&P'
			OR HCPC_Database_Category__r.Equipment_Type__c = 'DME / O&P')
			AND HCPC_Database_Category__r.Database__c = 'Integra'
			ORDER BY Name
		];

		System.debug(System.LoggingLevel.INFO, 'categoryList: ' + categoryPediatricList);
		System.debug(System.LoggingLevel.INFO, 'providerId: ' + providerId);

		List<HCPC_Database_Category__c> dbCategoryList = new List<HCPC_Database_Category__c>();
		for (Provider_Category__c cat : categoryPediatricList) {
			dbCategoryList.add(cat.HCPC_Database_Category__r);
		}

		return createSelectList(dbCategoryList);
	}

	/** @description Query for and create available categories. */
	private List<SelectOption> createAvailableDMECats() {
		List<HCPC_Database_Category__c> categoryPediatricList = [
			SELECT Id, Name, Database__c
			FROM HCPC_Database_Category__c
			WHERE (Equipment_Type__c = 'DME'
			OR Equipment_Type__c = 'DME / O&P')
			AND Database__c = 'Integra'
			ORDER BY Database__c, Name
		];

		System.debug(System.LoggingLevel.INFO, 'categoryList: ' + categoryPediatricList);

		List<SelectOption> completeCatList = createSelectList(categoryPediatricList);
		return removeDuplicateCats(completeCatList, currentDMECats);
	}

	/** @description Query for and create available categories. */
	private List<SelectOption> createAvailableAdultCats() {
		List<HCPC_Database_Category__c> categoryAdultList = [
			SELECT Id, Name, Database__c
			FROM HCPC_Database_Category__c
			WHERE (Equipment_Type__c = 'Adult O&P'
			OR Equipment_Type__c = 'General O&P'
			OR Equipment_Type__c = 'DME / O&P')
			AND Database__c = 'Integra'
			ORDER BY Database__c, Name
		];

		System.debug(System.LoggingLevel.INFO, 'categoryList: ' + categoryAdultList);

		List<SelectOption> completeCatList = createSelectList(categoryAdultList);
		return removeDuplicateCats(completeCatList, currentAdultCats);
	}

	/** @description Query for and create available categories. */
	private List<SelectOption> createAvailablePediatricCats() {
		List<HCPC_Database_Category__c> categoryPediatricList = [
			SELECT Id, Name, Database__c
			FROM HCPC_Database_Category__c
			WHERE (Equipment_Type__c = 'Pediatric O&P'
			OR Equipment_Type__c = 'General O&P'
			OR Equipment_Type__c = 'DME / O&P')
			AND Database__c = 'Integra'
			ORDER BY Database__c, Name
		];

		System.debug(System.LoggingLevel.INFO, 'categoryList: ' + categoryPediatricList);

		List<SelectOption> completeCatList = createSelectList(categoryPediatricList);
		return removeDuplicateCats(completeCatList, currentPediatricCats);
	}

	/** @description Create SelectOptions from Database Categories with Id as value and Name (Database) as label. */
	private static List<SelectOption> createSelectList(List<HCPC_Database_Category__c> categoryList) {
		List<SelectOption> newCats = new List<SelectOption>();

		for (HCPC_Database_Category__c cat : categoryList) {
			newCats.add(new SelectOption(cat.Id, cat.Name));
		}

		return newCats;
	}

	private static List<SelectOption> removeDuplicateCats(List<SelectOption> listToTrim, List<SelectOption> potentialDuplicates) {
		// Populate duplicate set
		Set<Id> duplicateSet = new Set<Id>();
		for (SelectOption option : potentialDuplicates) {
			duplicateSet.add(option.getValue());
		}

		List<SelectOption> trimmedCatList = new List<SelectOption>();
		for (SelectOption item : listToTrim) {
			if (!duplicateSet.contains(item.getValue())) {
				trimmedCatList.add(item);
			}
		}

		System.debug(System.LoggingLevel.INFO, 'trimmedCatList: ' + trimmedCatList);
		return trimmedCatList;
	}
}