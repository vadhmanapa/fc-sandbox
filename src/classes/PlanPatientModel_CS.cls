public without sharing class PlanPatientModel_CS extends AbstractSObjectModel_CS {

	public Plan_Patient__c instance {
		get{
			if(instance == null){ instance = (Plan_Patient__c)record; }
			return instance;
		}
		set;
	}

	public PlanPatientModel_CS(Plan_Patient__c patient){
		super(patient);
	}
	
	public static List<PlanPatientModel_CS> toList(List<Plan_Patient__c> patientList){
		
		List<PlanPatientModel_CS> patientModelList = new List<PlanPatientModel_CS>();
		
		if(patientList != null){		
			for(Plan_Patient__c p : patientList){
				patientModelList.add(new PlanPatientModel_CS(p));
			}
		}
		else{
			System.debug('PlanPatientModel_CS Error: Input list is null.');
		}
		
		return patientModelList;
	}
}