/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ProviderAssignmentScheduled_CS_Test {

	/******* Test Parameters *******/
	static final Integer N_ORDERS = 5;

	/******* Provider *******/
	static private List<Account> providers;
	static private List<Contact> providerUsers;
	
	/******* Payer *******/
	static private List<Account> payers;
	static private List<Contact> payerUsers;
	
	/******* Patients *******/
	static private List<Patient__c> patients;
	static private List<Plan_Patient__c> planPatients;
	
	/******* Orders *******/
	//There are 5 types of orders to test:
		//	1. Orders belonging to Plan Patients that have recently assigned orders
		//	2. Orders belonging to Plan Patients that have no recently assigned orders
		//	3. Orders belonging to Patients that have recently assigned orders
		//	4. Orders belonging to Patients that have no recently assigned orders
		//	5. Orders that have no Plan Patient
		
	//The structures holding orders will be local to the test methods

    static testMethod void ProviderAssignmentScheduled_Test_HasRecentlyAssigned() {
        
        //////////////////////// Begin Init ////////////////////////
        
        init();
        
        //Expiration time, this should cause orders set to this to be found by the
        // expiration process
        DateTime expirationTime = DateTime.now().addHours(-3);
		
		//For Plan Patient 0 create two orders and assign them to Provider 0
		List<Order__c> assignedOrders = TestDataFactory_CS.generateOrders(planPatients[0].Id, providers[0].Id, 2);	

		insert assignedOrders;
		
		//Set the first order to expired
		assignedOrders[0].Expiration_Time__c = expirationTime;
		
		//Set the second order to a different provider
		assignedOrders[1].Provider__c = providers[1].Id;
		
		update assignedOrders;
        
       	//////////////////////// Begin Test ////////////////////////
        
        Test.startTest();        
        
        ProviderAssignmentScheduled_CS.schedule();
               
        Test.stopTest();
        
        Order__c changedOrder = [SELECT Provider__c, Expiration_Time__c FROM Order__c WHERE Id = :assignedOrders[0].Id];
        
        System.assertEquals(providers[1].Id,changedOrder.Provider__c);
        System.assertNotEquals(expirationTime,changedOrder.Expiration_Time__c);
    }
    
    /*
    static testMethod void ProviderAssignmentScheduled_Test_HasNoRecentlyAssigned() {        
        
        //////////////////////// Begin Init ////////////////////////
        
        init();
        
        //Expiration time, this should cause orders set to this to be found by the
        // expiration process
        DateTime expirationTime = DateTime.now().addHours(-3);
		
		List<Order__c> assignedOrders = new List<Order__c>();
		
		//For Plan Patient 0 create one order and assign to Provider 0
		assignedOrders.addAll(TestDataFactory_CS.generateOrders(planPatients[0].Id, providers[0].Id, 1));
		
		//Order Geolocation
		assignedOrders[0].Geolocation__Latitude__s = 10.0;
		assignedOrders[0].Geolocation__Longitude__s = 30.0;
		
		insert assignedOrders;
		
		//Set the 1st order to expired
		assignedOrders[0].Expiration_Time__c = expirationTime;
		
		update assignedOrders;
        
       	//////////////////////// Begin Test ////////////////////////
        
        Test.startTest();        
        
        ProviderAssignmentScheduled_CS.schedule();
               
        Test.stopTest();
        
        Order__c changedOrder = [SELECT Provider__c, Expiration_Time__c FROM Order__c WHERE Id = :assignedOrders[0].Id];
        
        System.assertEquals(providers[2].Id,changedOrder.Provider__c);
        System.assertNotEquals(expirationTime,changedOrder.Expiration_Time__c);
    }

   	static testMethod void ProviderAssignmentScheduled_Test_HasNoRecentlyAssignedWithHistory() {        
        
        System.debug('Start ProviderAssignmentScheduled_Test_HasNoRecentlyAssignedWithHistory');

        //////////////////////// Begin Init ////////////////////////
        
        init();
        
        for(Account a : providers){
        	System.debug(a);
        }
        
        //Expiration time, this should cause orders set to this to be found by the
        // expiration process
        DateTime expirationTime = DateTime.now().addHours(-3);
		
		List<Order__c> assignedOrders = new List<Order__c>();
		
		//For Plan Patient 0 create one order and assign to Provider 0
		assignedOrders.addAll(TestDataFactory_CS.generateOrders(planPatients[0].Id, providers[0].Id, 1));
		
		//Order Geolocation
		assignedOrders[0].Geolocation__Latitude__s = 10.0;
		assignedOrders[0].Geolocation__Longitude__s = 30.0;
		
		//Assignment History 
		assignedOrders[0].Assignment_History__c = providers[1].Id;
		
		//For Plan Patient 1 create one order and assign to Provider 1	
		assignedOrders.addAll(TestDataFactory_CS.generateOrders(planPatients[1].Id, providers[1].Id, 1));
		
		//For Plan patient 1 create one order and assign to Provider 2
		assignedOrders.addAll(TestDataFactory_CS.generateOrders(planPatients[1].Id, providers[2].Id, 1));
		
		insert assignedOrders;
		
		//Set the 1st order to expired
		assignedOrders[0].Expiration_Time__c = expirationTime;
		
		update assignedOrders;
        
        System.debug('Final order data: ' + assignedOrders);
        
        System.debug('Order 0 test: ' + [SELECT Provider__c, Expiration_Time__c FROM Order__c WHERE Expiration_Time__c < :DateTime.now()]);
       	//////////////////////// Begin Test ////////////////////////
        
        //The expired order should be assigned to Provider 2 as it has a history of being assigned to
        // Provider 1
        
        Test.startTest();        
        
        ProviderAssignmentScheduled_CS.schedule();
               
        Test.stopTest();
        
        Order__c changedOrder = [SELECT Provider__c, Expiration_Time__c FROM Order__c WHERE Id = :assignedOrders[0].Id];
        
        System.assertEquals(providers[2].Id,changedOrder.Provider__c);
        System.assertNotEquals(expirationTime,changedOrder.Expiration_Time__c);
    }
   	*/
   	
    public static void init(){
    	///////////////////////////////////
		// Data
		///////////////////////////////////
    	
    	//Providers
		providers = TestDataFactory_CS.generateProviders('TestProvider', 3);
		
		//Provider geolocation and locale data
		for(Integer i = 0; i < providers.size(); i++){
			providers[i].Corporate_Geolocation__Latitude__s = 10.0;
	        providers[i].Corporate_Geolocation__Longitude__s = (i+1) * 10.0;
	        providers[i].Provider_Rank__c = 'Preferred';
			providers[i].Locale__c = 'US/Eastern';
		}
        
		insert providers;

		providerUsers = TestDataFactory_CS.generateProviderContacts('ProviderUser', providers, 1);
		insert providerUsers;	
		
		//Payers
		payers = TestDataFactory_CS.generatePlans('TestPayer', 1);
		insert payers;
		
		payerUsers = TestDataFactory_CS.generatePlanContacts('TestPlan', payers, 1);
		insert payerUsers;
		
		//Patients
		patients = TestDataFactory_CS.generatePatients('TestPatient', 2);	
		insert patients;
		
		//Plan patients
		planPatients = TestDataFactory_CS.generatePlanPatients(
			new Map<Id,List<Patient__c>>{
				payers[0].Id => new List<Patient__c>{patients[0], patients[1]}
			}
		);
		insert planPatients;
    }
}