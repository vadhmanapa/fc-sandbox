public without sharing class ProviderSettings_CS extends ProviderPortalServices_CS {

	private final String pageAccountType = 'Provider';

	public ProviderSettings_CS(){
		
	}
	
	public PageReference init(){

		///////////////////////////////////
		// Authentication and Permission
		///////////////////////////////////
		
		if(!isSecure && !Test.isRunningTest()){
			return DoLogout();
		}
				
		if(!authenticated){
			return DoLogout();
		}

		if(portalUser.AccountType != pageAccountType){
			return homepage;
		}
		
		///////////////////////////////////
		// Additional init
		///////////////////////////////////
				
		return null;
	}
	
	public PageReference toggleAcceptedNotification(){
		
		if(portalUser.instance.Notify_Accepted__c == false){
			System.debug('Notify_Accepted__c was ' + portalUser.instance.Notify_Accepted__c);
			portalUser.instance.Notify_Accepted__c = true;
		}
		else{
			System.debug('Notify_Accepted__c was ' + portalUser.instance.Notify_Accepted__c);
			portalUser.instance.Notify_Accepted__c = false;
		}

		update portalUser.instance;
						
		return null;
	}
	
	public PageReference toggleCanceledNotification(){
		
		if(portalUser.instance.Notify_Cancelled__c == false){
			System.debug('Notify_Cancelled__c was ' + portalUser.instance.Notify_Cancelled__c);
			portalUser.instance.Notify_Cancelled__c = true;
		}
		else{
			System.debug('Notify_Cancelled__c was ' + portalUser.instance.Notify_Cancelled__c);
			portalUser.instance.Notify_Cancelled__c = false;
		}
		
		update portalUser.instance;
						
		return null;
	}
	
	public PageReference toggleDeliveredNotification(){
		
		if(portalUser.instance.Notify_Delivered__c == false){
			System.debug('Notify_Password_Change__c was ' + portalUser.instance.Notify_Delivered__c);
			portalUser.instance.Notify_Delivered__c = true;
		}
		else{
			System.debug('Notify_Delivered__c was ' + portalUser.instance.Notify_Delivered__c);
			portalUser.instance.Notify_Delivered__c = false;
		}

		update portalUser.instance;
						
		return null;
	}
	
	public PageReference toggleNeedsAttentionNotification(){
		
		if(portalUser.instance.Notify_Needs_Attention__c == false){
			System.debug('Notify_Needs_Attention__c was ' + portalUser.instance.Notify_Needs_Attention__c);
			portalUser.instance.Notify_Needs_Attention__c = true;
		}
		else{
			System.debug('Notify_Needs_Attention__c was ' + portalUser.instance.Notify_Needs_Attention__c);
			portalUser.instance.Notify_Needs_Attention__c = false;
		}

		update portalUser.instance;
						
		return null;
	}
	
	public PageReference toggleNewOrderNotification(){
		
		if(portalUser.instance.Notify_New_Order__c == false){
			System.debug('Notify_New_Order__c was ' + portalUser.instance.Notify_New_Order__c);
			portalUser.instance.Notify_New_Order__c = true;
		}
		else{
			System.debug('Notify_New_Order__c was ' + portalUser.instance.Notify_New_Order__c);
			portalUser.instance.Notify_New_Order__c = false;
		}
		
		update portalUser.instance;
		
		return null;
	}	
	
	public PageReference togglePasswordChangeNotification(){
		
		if(portalUser.instance.Notify_Password_Change__c == false){
			System.debug('Notify_Password_Change__c was ' + portalUser.instance.Notify_Password_Change__c);
			portalUser.instance.Notify_Password_Change__c = true;
		}
		else{
			System.debug('Notify_Password_Change__c was ' + portalUser.instance.Notify_Password_Change__c);
			portalUser.instance.Notify_Password_Change__c = false;
		}

		update portalUser.instance;
						
		return null;
	}
	
	public PageReference togglePayerCanceledNotification(){
		
		if(portalUser.instance.Notify_Payer_Canceled__c == false){
			System.debug('Notify_Payer_Canceled__c was ' + portalUser.instance.Notify_Payer_Canceled__c);
			portalUser.instance.Notify_Payer_Canceled__c = true;
		}
		else{
			System.debug('Notify_Payer_Canceled__c was ' + portalUser.instance.Notify_Payer_Canceled__c);
			portalUser.instance.Notify_Payer_Canceled__c = false;
		}

		update portalUser.instance;
						
		return null;
	}
	
	public PageReference toggleRejectedNotification(){
		
		if(portalUser.instance.Notify_Rejected__c == false){
			System.debug('Notify_Rejected__c was ' + portalUser.instance.Notify_Rejected__c);
			portalUser.instance.Notify_Rejected__c = true;
		}
		else{
			System.debug('Notify_Password_Change__c was ' + portalUser.instance.Notify_Password_Change__c);
			portalUser.instance.Notify_Password_Change__c = false;
		}

		update portalUser.instance;
						
		return null;
	}	
	
	public PageReference toggleShippedNotification(){
		
		if(portalUser.instance.Notify_Shipped__c == false){
			System.debug('Notify_Shipped__c was ' + portalUser.instance.Notify_Shipped__c);
			portalUser.instance.Notify_Shipped__c = true;
		}
		else{
			System.debug('Notify_Shipped__c was ' + portalUser.instance.Notify_Shipped__c);
			portalUser.instance.Notify_Shipped__c = false;
		}

		update portalUser.instance;
						
		return null;
	}
}