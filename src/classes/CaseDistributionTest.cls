@isTest
private class CaseDistributionTest {

	static testMethod void getAssignees() {
		
		Set<Id> customerServiceUserIds = CaseAssignment.getCustomerServiceUserIds();
		List<CaseDistribution.Assignee> assignees = CaseDistribution.getAssignees(customerServiceUserIds);
		
		for(CaseDistribution.Assignee assignee : assignees) {
			system.assert(customerServiceUserIds.contains(assignee.userId));
		}			
	}
	
	static testMethod void assignCaseOwners() {
		
		Set<Id> userIds = CaseAssignment.getCustomerServiceUserIds();
		List<Id> userIdList = new List<Id>(userIds);
		List<Case> casesToInsert = new List<Case>();
		
		for(Integer i = 0; i < userIdList.size(); i++) {
			Id userId = userIdList[i];
			
			for(Integer j = 0; j <= i; j++) {
				casesToInsert.add(new Case(
					OwnerId = userId,
					Subject = 'Test Case'
				));
			}
		}
		
		insert casesToInsert;
		
		List<CaseDistribution.Assignee> assignees = 
			CaseDistribution.getAssignees(userIds);
			
		for(CaseDistribution.Assignee assignee : assignees) {
			system.debug(LoggingLevel.INFO,assignee.userId + ' / ' + assignee.caseCount);
		}
	}
}