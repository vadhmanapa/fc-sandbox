public without sharing class UserServices_CS extends AccountServices_CS{

		public static final Set<String> objectFields = new Set<String>{
		/* Standard Fields */
		'Id',
		'AccountId',
		'Email',
		'Name',
		'FirstName',
		'LastName',
		'Phone',
		/* Custom Fields */
		'Active__c',
		'Batch_ID__c',
		'Contact_Notes__c',
		'Status__c',	
		'Entity__c',
		'Entity__r.Name',
		'Entity__r.Type__c',
		'Id_Key__c',
		'Login_Attempts__c',
		'Password__c',
		'Password_History__c',
		'Password_Reset__c',
		'Password_Set_Date__c',
		'Payer_Account__c',
		'Phone_Extension__c',
		'Profile__c',
		'Provider_Account__c',
		'Role__c',
		'Salt__c',
		'Session__c',
		'Primary_Clear_Contact__c',
		'Notify_Accepted__c',
		'Notify_Cancelled__c',
		'Notify_Delivered__c',
		'Notify_Needs_Attention__c',
		'Notify_New_Order__c',
		'Notify_Password_Change__c',
		'Notify_Payer_Canceled__c',
		'Notify_Rejected__c',
		'Notify_Shipped__c',
		'TIMBASURVEYS__Survey__c',
		'Health_Plan_User_Id__c',
		'Username__c',
		'User_Type__c',
		/* Content Role Fields */
		'Content_Role__c',
		'Content_Role__r.Name',
		'Content_Role__r.Accept_Orders__c',
		'Content_Role__r.Approve_Recurring_Orders__c',
		'Content_Role__r.Cancel_Orders__c',
		'Content_Role__r.Change_Provider__c',
		'Content_Role__r.Create_Orders__c',
		'Content_Role__r.Create_Plan_Patients__c',
		'Content_Role__r.Create_Users__c',
		'Content_Role__r.Edit_Orders__c',
		'Content_Role__r.Edit_Plan_Patients__c',
		'Content_Role__r.Edit_Users__c',
		'Content_Role__r.Filter_by_User__c',
		'Content_Role__r.Mark_Orders_Delivered__c',
		'Content_Role__r.Mark_Orders_Shipped__c',
		'Content_Role__r.Reject_Orders__c',
		'Content_Role__r.Reset_Password__c',
		'Content_Role__r.Reset_User_Name__c',
		'Content_Role__r.Super_User__c',
		'Content_Role__r.View_Other_Users_Orders__c',
		'Content_Role__r.View_Patients__c'		
	};

	public UserServices_CS(String cId){
		super(cId);
	}

	public NestableCondition getBaseConditions(){
		
		//Conditions that will be used in the query
        NestableCondition conditions = new AndCondition();
        
        //Entity on contact must match portalUser's account
        conditions.add(new FieldCondition('Entity__c').equals(cAccount.Id));
        
        System.debug('Base Conditions: ' + conditions.toSoql());
        
        return conditions;
	}

	public SoqlBuilder getBaseQuery(){

        return new SoqlBuilder()
            .selectx(objectFields)
            .fromx('Contact')
            .limitx(10000)
            .wherex(getBaseConditions());
	}

	public SoqlBuilder attachToBaseQuery(NestableCondition moreConditions){
        
        NestableCondition conditions = getBaseConditions();
        
        conditions.add(moreConditions);
        
		return new SoqlBuilder()
				.selectx(objectFields)				
				.fromx('Contact')
				.limitx(10000)
				.wherex(conditions);
	}

	/*
	public SoqlBuilder getBaseQuery(){
		System.debug('cAccount: ' + cAccount);

		if(cAccount.Type__c == 'Payer'){
			return new SoqlBuilder()
				.selectx(objectFields)
				.selectx(new SoqlBuilder()
					.selectx('Id')
					.fromx('Order2__r'))
				.fromx('Contact')
				.limitx(10000)
				.wherex(new AndCondition()
					.add(new FieldCondition('Entity__c').equals(cAccount.Id))
				);		
		}else if(cAccount.Type__c == 'Provider'){
			return new SoqlBuilder()
				.selectx(objectFields)
				.selectx(new SoqlBuilder()
					.selectx('Id')
					.fromx('Order2__r'))
				.fromx('Contact')
				.limitx(10000)
				.wherex(new AndCondition()
					.add(new FieldCondition('Entity__c').equals(cAccount.Id))
				);		
		}else{
			throw new ApplicationException('Invalid AccountId');
			return null;
		}
	}
	
	public SoqlBuilder attachToBaseQuery(NestableCondition conditions){		
			
		if(cAccount.Type__c == 'Payer'){		
			return new SoqlBuilder()
				.selectx(objectFields)				
				.fromx('Contact')
				.limitx(10000)
				.wherex(new AndCondition()
					.add(new FieldCondition('Entity__c').equals(cAccount.Id))
					.add(conditions)
				);
		}else if(cAccount.Type__c == 'Provider'){
			return new SoqlBuilder()
				.selectx(objectFields)
				.fromx('Contact')
				.limitx(10000)
				.wherex(new AndCondition()
					.add(new FieldCondition('Entity__c').equals(cAccount.Id))
					.add(conditions)
				);		
		}else{
			throw new ApplicationException('UserServices_CS: Invalid AccountId');
		}
	}
	*/
	
	public ApexPages.StandardSetController getUsers(){
		//Find the list of users given the constraints:
		//	1) The searched users belong to the organization
		//	2) Excludes the user searching
		
		try{
			SoqlBuilder sb = attachToBaseQuery(new AndCondition()
									.add(new FieldCondition('Active__c',Operator.EQUALS,true))
									.add(new FieldCondition('Id',Operator.NOT_EQUALS,c.Id))
									);
			
			return new Apexpages.Standardsetcontroller(database.query(sb.toSoql()));
		}catch(Exception e){
			//TODO: throw handled exception
			throw ((e instanceOf ApplicationException) ? e : new ApplicationException(e.getMessage(), e));
		}//End catches
		
		return new Apexpages.Standardsetcontroller(new List<Contact>());			
	}
	
	public Contact getUserByName(String name){
		//Find the list of users given the constraints:
		//	1) The searching user has sufficient priveliges
		//	2) The searched users belong to the organization
		
		Contact u;
		
		try{
			SoqlBuilder sb = attachToBaseQuery(new AndCondition()
									.add(new FieldCondition('Name',Operator.EQUALS,name))
									);
			
			return database.query(sb.toSoql());
		}catch(Exception e){
			//TODO: throw handled exception
			throw ((e instanceOf ApplicationException) ? e : new ApplicationException('', e));
		}//End catches
		
		return null;			
	} 

	public Contact getUserById(String Id){
		
		Contact u;
		
		try{
			SoqlBuilder sb = attachToBaseQuery(new AndCondition()
									.add(new FieldCondition('Id',Operator.EQUALS,Id))
									);
			
			return database.query(sb.toSoql());
		}catch(Exception e){
			//TODO: throw handled exception
			throw ((e instanceOf ApplicationException) ? e : new ApplicationException('', e));
		}//End catches
		
		return null;			
	}

	public static Boolean resetUserPassword(Contact user){
		UserModel_CS userModel = new UserModel_CS(user);
		return userModel.resetPassword();		
	}
	
	public ApexPages.StandardSetController searchUsers(String searchString){
		
		NestableCondition orCondition = new OrCondition()
			.add(new FieldCondition('Name').likex('%' + searchString + '%'))
			.add(new FieldCondition('Email').likex('%' + searchString + '%'))
			.add(new FieldCondition('Username__c').likex('%' + searchString + '%'));
		
		String query = attachToBaseQuery(orCondition).toSoql();
		
		System.debug('Query string: ' + query);
									
		return new Apexpages.Standardsetcontroller(Database.getQueryLocator(query));	
	}
	
	public static Boolean checkUniqueUsername(String username){
		
		//Search from all accounts, username must be globally unique
		List<Contact> nonUniqueContacts = [
			select
				Id,
				Username__c
			from Contact
			where Username__c = :username				
		];
			
		if(nonUniqueContacts.size() == 0){
			return true;
		}
		else{
			return false;
		}
	}
	
}