@isTest
private class ProviderUsersSummary_CS_Test {

	/******* Test Parameters *******/
	static private final Integer N_USERS = 15;

	/******* Provider *******/
	static private List<Account> providers;
	static private List<Contact> providerUsers;

	/******* Test Objects *******/
	static private ProviderUsersSummary_CS usersSummary;

	/******* Test Methods *******/

	static testMethod void ProviderUsersSummary_Test_PageLanding() {
		init();
		
		Test.startTest();
		
		//Check initial state of page components
		System.assert(usersSummary.userSetCon.getPageSize() == 10, 'userSetCon pageSize ' + 
			usersSummary.userSetCon.getPageSize() + ' != 10');
		
		System.assert(usersSummary.userSetCon.getPageNumber() == 1, 'userSetCon pageNumber ' + 
			usersSummary.userSetCon.getPageSize() + ' != 1');
			
		System.assert(usersSummary.userSetCon.getResultSize() == N_USERS - 1, 'userSetCon resultSize ' + 
			usersSummary.userSetCon.getResultSize() + ' != ' + (N_USERS - 1));
		
		System.assert(usersSummary.userList.size() == usersSummary.pageSize, 'userList size ' + 
			usersSummary.userList.size() + ' != ' + usersSummary.pageSize);		
			
		Test.stopTest();	
	}

    static testMethod void ProviderUsersSummary_Test_Pagination() {
        init();
        
       	Test.startTest();

       	usersSummary.userSetCon.next();
       	
       	System.assert(usersSummary.userSetCon.getPageNumber() == 2, 'userSetCon pageNumber ' + 
			usersSummary.userSetCon.getPageSize() + ' != 2');
       	
		usersSummary.pageSize = 20;
		
		System.assert(usersSummary.userSetCon.getPageSize() == 20, 'userSetCon pageSize ' + 
			usersSummary.userSetCon.getPageSize() + ' != 20');
       	
       	Test.stopTest();
    }
    
    static testMethod void ProviderUsersSummary_Test_SearchUsers(){
    	init();
    	
    	Test.startTest();
    	
    	usersSummary.searchString = 'Test1';
    	usersSummary.searchUsers();
    	
    	for (Contact u : usersSummary.userList){
    		System.Assert(u.Name.containsIgnoreCase('Test1') || u.Username__c.containsIgnoreCase('Test1') || u.Email.containsIgnoreCase('Test1'), 'Not all records contain \'Test1\'');
    	}
    	
    	usersSummary.searchString = 'NonsenseString13297ry8q0oephrg938qad';
    	usersSummary.searchUsers();
    	
    	System.assert(usersSummary.UserList.size() == 0, 'Search let in bad records');
    	
    	Test.stopTest();
    	
    }
    
    static testMethod void ProviderUsersSummary_Test_SendUsername(){
    	init();
    	
    	Test.startTest();
    	
    	usersSummary.selectedUserId = providerUsers[1].Id;
    	usersSummary.selectedUser = new UserModel_CS(providerUsers[1]);
    	
    	System.assert(usersSummary.selectedUser != null, 'No selected user context.');
    	
    	//TODO: How to run assert with this?
    	usersSummary.sendUsername();
    }
    
    static testMethod void ProviderUsersSummary_Test_ResetPassword(){
    	init();
    	
    	Test.startTest();
    	
    	usersSummary.selectedUserId = providerUsers[1].Id;
    	usersSummary.selectedUser = new UserModel_CS(providerUsers[1]);
    	
    	System.assert(usersSummary.selectedUser != null, 'No selected user context.');
    	
    	String oldPassword = usersSummary.selectedUser.instance.Password__c;
    	
    	usersSummary.resetPassword();
    	
      	//Re-query the user
    	List<Contact> deactivatedUsers = [select Id,Password__c from Contact where Id = :usersSummary.selectedUserId limit 1];  	
    	
    	System.assert(deactivatedUsers.size() == 1, 'More than one user returned');
    	System.assert(deactivatedUsers[0].Password__c != oldPassword, 'Password not reset.');
    	
    	Test.stopTest();
    }
    
    static testMethod void ProviderUsersSummary_Test_DeactivateUser(){
    	init();
    	
    	Test.startTest();
    	
    	usersSummary.selectedUserId = providerUsers[1].Id;
    	usersSummary.selectedUser = new UserModel_CS(providerUsers[1]);
    	
    	System.assert(usersSummary.selectedUser != null, 'No selected user context.');
    	
    	usersSummary.deactivateUser();
    	
    	//Re-query the user
    	List<Contact> deactivatedUsers = [select Id,Active__c from Contact where Id = :usersSummary.selectedUserId limit 1];
    	
    	System.assert(deactivatedUsers.size() == 1, 'More than one user returned');
    	System.assert(deactivatedUsers[0].Active__c == false, 'Selected user is still active.');
    	
    	Test.stopTest();
    }
    
	static void init(){
    	
    	///////////////////////////////////
		// Data
		///////////////////////////////////
    	
    	//Providers
		providers = TestDataFactory_CS.generateProviders('TestProvider', 1);
		insert providers;
		
		//Provider users, set first user to Admin
		providerUsers = TestDataFactory_CS.createProviderContacts('ProviderUser', providers, N_USERS);
		//providerUsers[0].Role__c = 'Admin';
		providerUsers[0].Profile__c = 'Admin';
		update providerUsers;

		///////////////////////////////////
		// Page Landing
		///////////////////////////////////

		//Set the page and any parameters
		Test.setCurrentPage(Page.ProviderUsersSummary_CS.setRedirect(true));
		
		//Log the desired user in
		TestServices_CS.login(providerUsers[0]);
    	
    	//Land on the page
    	usersSummary = new ProviderUsersSummary_CS();
    	usersSummary.init();
    }
}