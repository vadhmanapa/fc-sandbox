public without sharing class GeocodingUtils_CS {
    public static final string APIKey = 'Gmjtd%7Cluur2q61nh%2Cal%3Do5-lr2wg';
    
    public static void geocodeAddresses(List<GeocodingModel_CS> geoModels) {
        system.debug('geoModels to be Geocoded : '+ geoModels);
        List<String> addresses = new List<String>();
        for(GeocodingModel_CS gm : geoModels) {
            addresses.add(gm.getAddress());
        }
        String response = makeGeocodeRequest(addresses);
        system.debug('Geo Response : '+ response);
        List<GeocodingResult> results = parseGeocodeResults(response);
        system.debug('Out of Parser Function: '+results);
        
        for(GeocodingModel_CS gm : geoModels) {
            string address = gm.getAddress();
            system.debug('Provider Address: '+address);
            for(GeocodingResult res : results) {
                system.debug('Parser Provided Location null check : '+res.providedLocation);
                system.debug('Parser Address: '+res.providedLocation.location);                
                if(res.providedLocation.location != null && address.trim() == res.providedLocation.location.trim()) {
                    system.debug('Match');
                    gm.lat = res.locations[0].latlng.lat; 
                    gm.lng = res.locations[0].latlng.lng;
                    break;
                }else{
                    system.debug('No Match : '+address+':'+res.providedLocation.location);
                    system.debug(address == res.providedLocation.location);
                }                
            }
            system.debug('finished for loop : '+gm.getAddress()+';('+gm.lat+','+gm.lng+')');
        }         
    }
    public static String makeGeocodeRequest(List<String> addresses) {
        String url = 'http://www.mapquestapi.com/geocoding/v1/batch?';
        url += 'key='+APIKey;
        url += '&thumbMaps=false&outformat=json';
        for(String address : addresses) {
            //system.debug('Address String : '+address);
            url += '&location='+address;
        }
        url = url.replace(' ','+');
        //system.debug(url);
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('GET');
        req.setHeader('Referer','http://www.salesforce.com');
        
        Http h = new Http();
        
        if(Test.isRunningTest()) {
            return getSampleGeocodeJSONString();
        }
       
        HttpResponse res = h.send(req);
        //system.debug('Res: ' + res.getBody());
        return res.getBody();
    }
    public static List<GeocodingResult> parseGeocodeResults(String jsonString) {
        List<GeocodingResult> results = new List<GeocodingResult>();
        JSONParser parser = JSON.createParser(jsonString);
        //system.debug(jsonString);
        while (parser.nextToken() != null) {
            if (parser.getCurrentToken() == JSONToken.START_ARRAY) {
                while (parser.nextToken() != null) {
                    if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                        if(parser.getCurrentName() == 'options') { break; }
                        GeocodingResult gRes = (GeocodingResult)parser.readValueAs(GeocodingResult.class);
                        results.add(gRes);
                        //system.debug('In Parser : '+gRes.providedLocation);
                        if(gRes.locations != null) {
                            for(Location l : gRes.locations) {
                                //system.debug('In Parser : '+l);
                                //system.debug('In Parser : '+l.latLng);
                            }
                        }
                        parser.skipChildren();
                    }
                }
            }
        }
        //system.debug('After Parser : '+results);
        return results;
    }
    /*140218 Not Used in first iteration
    public GeocodingModel_CS getNearestProvider(GeocodingModel_CS origin, List<GeocodingModel_CS> destinations) {       
        String response = makeDistanceRequest(origin, destinations);
        DistanceResult results = parseDistanceResults(response);
        
        for(GeocodingModel_CS gm : destinations) {
            Decimal mLat = gm.lat;
            Decimal mLng = gm.lng;
            for(integer i = 0; i < results.locations.size(); i++) {
                Decimal rLat = results.locations[i].LatLng.Lat;
                Decimal rLng = results.locations[i].LatLng.Lng;
                if(mLat == rLat && mLng == rLng) {
                    gm.travelTime = results.time_x[i];
                }else {
                    ////system.debug('No Match : ('+mLat+','+mLng+') : ('+rLat+','+rLng+')');
                }
            }
        }
        
        GeocodingModel_CS shortestTimeModel;
        for(GeocodingModel_CS gm : destinations) {
            if(shortestTimeModel == null || gm.travelTime < shortestTimeModel.travelTime) { 
                shortestTimeModel = gm;
            }
        }
        return shortestTimeModel;
    }
    
    public String makeDistanceRequest(GeocodingModel_CS origin, List<GeocodingModel_CS> destinations) {
        // Instantiate a new http object
        Http h = new Http();
        
        // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
        String url = 'http://www.mapquestapi.com/directions/v2/routematrix?key=' + APIKey; //GOOGLE 'https://maps.googleapis.com/maps/api/distancematrix/json?';
        url += '&from='+origin.lat+','+origin.lng;
        for(GeocodingModel_CS d : destinations) {
            url += '&to='+d.lat+','+d.lng;
        }

        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('GET');// Instantiate a new http object

        if(test.isRunningTest()) {
            return getSampleDistanceJSONString();
        }
        // Send the request, and return a response
        HttpResponse res = h.send(req);
        ////system.debug('Res: ' + res.getBody());
        return res.getBody();
    }
    public DistanceResult parseDistanceResults(String jsonString) {
        jsonString = jsonString.replace('"time":', '"time_x":');
        JSONparser parser = JSON.createParser(jsonString);
        DistanceResult dRes = (DistanceResult)parser.readValueAs(DistanceResult.class);
        ////system.debug(dRes); 
        return dRes;    
    }
    */

    //Geocoding Result classes  
    public class GeocodingResult {
        public ProvidedLocation providedLocation;
        public List<Location> locations;
        
        public GeocodingResult(ProvidedLocation providedLocation, List<Location> locations) {
            this.providedLocation = providedLocation;
            this.locations = locations.clone();
        }
    }
    
    public class ProvidedLocation {
        public String location;
        
        public ProvidedLocation(String location) {
            this.location = location;
        }
    }

    //Distance Result Classes
    public class DistanceResult {
        public List<Decimal> distance;
        public List<Integer> time_x;
        public List<Location> locations;
        
        public DistanceResult(List<Decimal> distance, List<Integer> time_x, List<Location> locations) {
            this.distance = distance;
            this.time_x = time_x;
            this.locations = locations;
        }
    }
    
    //Used for Both Result Classes
    public class Location {
        public LatLng latLng;
        public String geocodeQuality;
        public String geocodeQualityCode;
        
        public Location(LatLng latLng, String geocodeQuality, String geocodeQualityCode) {
            this.latLng = latLng;
            this.geocodeQuality = geocodeQuality;
            this.geocodeQualityCode = geocodeQualityCode;
        }
    }
    
    public class LatLng {
        public Decimal lng;
        public Decimal lat;
        
        public LatLng(Decimal lng, Decimal lat) {
            this.lng = lng;
            this.lat = lat;
        }
    }

    //Sample test data
    public static string getSampleGeocodeJSONString() {
        return '{"results":[{"locations":[{"latLng":{"lng":-1,"lat":1},"adminArea4":"","adminArea5Type":"City","adminArea4Type":"County","adminArea5":"","street":"","adminArea1":"US","adminArea3":"","type":"s","displayLatLng":{"lng":-1,"lat":1},"linkId":0,"postalCode":"","sideOfStreet":"N","dragPoint":false,"adminArea1Type":"Country","geocodeQuality":"COUNTRY","geocodeQualityCode":"A1XAX","adminArea3Type":"State"}],"providedLocation":{"location":"111 S Test Street, Test, TN 22222"}},{"locations":[{"latLng":{"lng":-40,"lat":40},"adminArea4":"","adminArea5Type":"City","adminArea4Type":"County","adminArea5":"","street":"","adminArea1":"US","adminArea3":"AZ","type":"s","displayLatLng":{"lng":-40,"lat":40},"linkId":0,"postalCode":"","sideOfStreet":"N","dragPoint":false,"adminArea1Type":"Country","geocodeQuality":"STATE","geocodeQualityCode":"A3XAX","adminArea3Type":"State"}],"providedLocation":{"location":"789 N Abc Rd, MN"}}],"options":{"ignoreLatLngInput":false,"maxResults":-1,"thumbMaps":false},"info":{"copyright":{"text":"&copy; 2013 MapQuest, Inc.","imageUrl":"http://api.mqcdn.com/res/mqlogo.gif","imageAltText":"&copy; 2013 MapQuest, Inc."},"statuscode":0,"messages":[]}}'; 
    }
    
    public static string getSampleDistanceJSONString() {
        return '{"allToAll":false,"distance":[0,1.77,1.08],"time":[0,256,136],"locations":[{"latLng":{"lng":-111.88245,"lat":33.364433},"adminArea4":"Maricopa","adminArea5Type":"City","adminArea4Type":"County","adminArea5":"Mesa","street":"2257 W Naranja Ave","adminArea1":"US","adminArea3":"AZ","type":"s","displayLatLng":{"lng":-111.88245,"lat":33.364433},"linkId":0,"postalCode":"85202-7370","sideOfStreet":"N","dragPoint":false,"adminArea1Type":"Country","geocodeQuality":"POINT","geocodeQualityCode":"P1AAA","adminArea3Type":"State"},{"latLng":{"lng":-111.906965,"lat":33.362121},"adminArea4":"Maricopa","adminArea5Type":"City","adminArea4Type":"County","adminArea5":"Tempe","street":"","adminArea1":"US","adminArea3":"AZ","type":"s","displayLatLng":{"lng":-111.906965,"lat":33.362121},"linkId":0,"postalCode":"85283","sideOfStreet":"N","dragPoint":false,"adminArea1Type":"Country","geocodeQuality":"ZIP","geocodeQualityCode":"Z1XAA","adminArea3Type":"State"},{"latLng":{"lng":-111.86928,"lat":33.362686},"adminArea4":"Maricopa","adminArea5Type":"City","adminArea4Type":"County","adminArea5":"Mesa","street":"","adminArea1":"US","adminArea3":"AZ","type":"s","displayLatLng":{"lng":-111.86928,"lat":33.362686},"linkId":0,"postalCode":"85202","sideOfStreet":"N","dragPoint":false,"adminArea1Type":"Country","geocodeQuality":"ZIP","geocodeQualityCode":"Z1XAA","adminArea3Type":"State"}],"manyToOne":false,"info":{"copyright":{"text":"&copy; 2013 MapQuest, Inc.","imageUrl":"http://api.mqcdn.com/res/mqlogo.gif","imageAltText":"&copy; 2013 MapQuest, Inc."},"statuscode":0,"messages":[]}}';
    }
    
}