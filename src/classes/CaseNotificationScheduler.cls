public with sharing class CaseNotificationScheduler implements Schedulable {

	public static final String jobName = 'Case Notifications';
	public static final String jobSchedule = '0 0 * * * ? *';
	
	public static String schedule() {
		String name = (Test.isRunningTest() ? jobName + ' Test' : jobName);
		String jobId = system.schedule(name, jobSchedule, new CaseNotificationScheduler());
		return jobId;
	}
	
	public void execute(SchedulableContext context) {
		Database.executeBatch(new CaseNotificationBatch());
	}
}