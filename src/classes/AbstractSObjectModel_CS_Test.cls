@isTest
private class AbstractSObjectModel_CS_Test {
	
	private static List<AccountModel_CS> accountModels;
	
	static testMethod void AbstractSObjectModel_Test(){ 
		init();
		List<sObject> objList = AbstractSObjectModel_CS.getSObjects((List<AbstractSObjectModel_CS>) accountModels);
	}
	
	private static void init(){
		List<Account> initAccts = TestDataFactory_CS.generateProviders('Test Provider', 5);
    	insert initAccts;
    	
    	accountModels = new List<AccountModel_CS>();
    	for (Account a : initAccts){
    		accountModels.add(new AccountModel_CS(a));
    	}
	}
}