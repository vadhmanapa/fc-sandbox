public with sharing class NewCaseController_IP {

    // All the required relational fields and objects
   /****

    public static User currentUser
    {
        get
        {
            if(currentUser == null) {
                currentUser = [SELECT Id, Name, UserRole.Name, Belongs_To__c FROM User WHERE Id=: UserInfo.getUserId()];
            }
            return currentUser;
        } private set;
    }
    public Case newCase {get;set;}
    public Case parentCase {get;set;}
    public List<Case> reviewAllAddedCase {get;set;}
    public Contact newContact {get;set;}
    public Contact newContactOther {get;set;}
    public AddCaseDetails__c caseDetail {get;set;}
    public List<AddCaseDetails__c> additionalDetails {get;set;}
    public Case_Action__c actionTaken {get;set;} // decides the status of the case
    public static Intake__c intakeId {get;set;}
    public Datetime entryStartTime{get;set;}
    public static Boolean isNewContactPatient {get;set;}
    public static Boolean isNewCasePatient {get;set;}
    public String accountId{get;set;}

    //Variables
    public Boolean errorFlag {get;set;}
    public String[] errorList {get;set;}
    public String selectedCallerType {get
        {
            if(String.isBlank(selectedCallerType)) {
                selectedCallerType = 'None';
            }

            return selectedCallerType;
        } set;}
    public String sectionHeader {get;set;}
    public Id insertedIntake {get;set;}
    public String lastName{get;set;}
    public String lastNameOther {get;set;}
    public Boolean isPatientCon{get;set;}
    public static Integer thisRun = 0;
    public Boolean nonCredContact {get;set;}
    public Boolean nonCredAccount {get;set;}

    public NewCaseController_IP(ApexPages.StandardController stdController){
        initCase();
        reviewAllAddedCase = new List<Case>();
        entryStartTime = Datetime.now();
        thisRun = 0;
        resetValidations();
        setParamsFromParent();
    }

    public void initCase(){
        newCase = new Case();
        if(String.isBlank(newCase.Origin)){
            newCase.Origin = 'Inbound Phone';
        }
        if(String.isNotBlank(selectedCallerType)){
            newCase.CallerType__c = selectedCallerType;
        }
        additionalDetails = new List<AddCaseDetails__c>();
        selectedCallAboutType = currentUser.Belongs_To__c == 'Claims' ? 'Payor' : '';
        selectedDetailRecType = currentUser.Belongs_To__c == 'Claims' ? 'Que Claim ID' : '';
        actionTaken = new Case_Action__c();
        caseDetail = new AddCaseDetails__c();
        isPatientCon = false;
        errorFlag = false;
        errorList = new String[]{};
        resetActionSelection();
        isNewCasePatient = false;
        isNewContactPatient = false;
        
        if(parentCase != null){
           setParamsFromParent();
        }

        if(currentUser.Belongs_To__c == 'Customer Service') {
            newCase.RecordTypeId = RecordTypes.customerCallId;
            sectionHeader = 'CALLING';
        }else if(currentUser.Belongs_To__c == 'Claims') {
            newCase.RecordTypeId = RecordTypes.claimsInquiryId;
            sectionHeader = 'INQUIRYING';
        }else {
            newCase.RecordTypeId = RecordTypes.complaintId;
            sectionHeader = 'COMPLAINING';
        }
    }

    public void setParamsFromParent(){
        // Step 1: Determine if the case was triggered from parent case or contact
        // Step 1a: If the trigger is from parent case - Add both conditions for console and classic

        String caseId;

        if(ApexPages.currentPage().getParameters().containsKey('cId')) {
            // Then the trigger point is service cloud
            caseId = ApexPages.currentPage().getParameters().get('cId').substringBefore('?');
        }else if(ApexPages.currentPage().getParameters().containsKey('returnURL')) {
            caseId = ApexPages.currentPage().getParameters().get('retURL').replace('%2F','').replace('/','');
        }else{}

        if (isValidSalesforceId( caseId, Case.class )) {
            // Failsafe to make sure we only query when there is a valid parent Id
            parentCase = [SELECT Id, AccountId, ContactId, Intake__c, SuppliedPhone, SuppliedEmail, SuppliedName, SuppliedCompany, 
                                Contact.RecordTypeId, Contact_Type__c, CaseNumber, Status, Subject, Description, CallerType__c,
                                Reason, Case_Subreason__c, DOB__c, PolicyNumber__c,Contact.Email,Origin, Patient_First_Name__c, Patient_Last_Name__c,
                                PatientHealthPlan__c, Payor__c, Provider__c
                          FROM Case
                          WHERE Id=: (Id) caseId LIMIT 1];
        }

        if(parentCase != null) {
            newCase.ParentId = parentCase.Id;
            newCase.AccountId = parentCase.AccountId;
            newCase.ContactId = parentCase.ContactId;
            newCase.SuppliedPhone = parentCase.SuppliedPhone;
            newCase.SuppliedEmail = parentCase.SuppliedEmail;
            newCase.Origin = parentCase.Origin;
            
            if(parentCase.ContactId == null){
                actionTaken.Email__c = parentCase.SuppliedEmail;
            }else{
                actionTaken.Email__c = parentCase.Contact.Email;
            }
            
            newCase.DOB__c = parentCase.DOB__c;
            newCase.PolicyNumber__c = parentCase.PolicyNumber__c;
            newCase.Intake__c = parentCase.Intake__c;
            
            if(String.isNotBlank(parentCase.CallerType__c)){
                if(parentCase.CallerType__c.contains('Email')){
                    selectedCallerType = parentCase.CallerType__c.remove(' Email').trim();
                    newCase.CallerType__c = parentCase.CallerType__c.remove(' Email').trim();
                }else{
                    selectedCallerType = parentCase.CallerType__c;
                    newCase.CallerType__c = parentCase.CallerType__c;
                }
            }else{
                newCase.CallerType__c = 'Provider';
            }
            
            newCase.Subject = parentCase.Subject;
            newCase.Description = parentCase.Description;
            if(String.isNotBlank(parentCase.SuppliedName) && parentCase.ContactId == null){
                newCase.SuppliedName = parentCase.SuppliedName;
                nonCredContact = true;
            }
            if(String.isNotBlank(parentCase.SuppliedCompany)){
                newCase.SuppliedCompany = parentCase.SuppliedCompany;
                nonCredAccount = true;
            }
        }

        // Step 1b: If the trigger is from parent case - Add both conditions for console and classic
        if(ApexPages.currentPage().getParameters().containsKey('contactId')) {
            String conId = ApexPages.currentPage().getParameters().get('contactId');
            Contact c = [SELECT Id, Name, Birthdate,FirstName, LastName, AccountId, RecordTypeId, Account.Status__c, Phone, Email, Health_Plan_ID__c FROM Contact WHERE Id=: (Id)conId LIMIT 1];
            newCase.ContactId = c.Id;
            newCase.AccountId = c.AccountId;
            newCase.SuppliedPhone = c.Phone;
            newCase.SuppliedEmail = c.Email;
            newCase.DOB__c = c.Birthdate;
            newCase.PolicyNumber__c = c.Health_Plan_ID__c;
            newCase.CallerType__c = RecordTypes.recordTypesById.get(c.RecordTypeId).Name;
            selectedCallerType = RecordTypes.recordTypesById.get(c.RecordTypeId).Name;
            actionTaken.Email__c = c.Email;
        }
    }

    public void getContactDetails(){
        //CHECK IF THE ENTERED VALUE IS A SF ID OR NOT
        String conId;
        
        if(newCase.ContactId != null){
        	conId = newCase.ContactId;
        }else if(newCase.ContactId == null && String.isNotBlank(ApexPages.currentPage().getParameters().get('contactId'))){
        	conId = ApexPages.currentPage().getParameters().get('contactId');
        }else{
        	// NO CONTACT ID FILLED
        	
        }
        // String conId = newCase.ContactId == null ? ApexPages.currentPage().getParameters().get('contactId') : newCase.ContactId;
        if(String.isNotEmpty(conId)) {
            if(isValidSalesforceId( conId, Contact.class )){
                Contact c = [SELECT Id, Name, Birthdate, AccountId, RecordTypeId, Account.Status__c, Phone, Email, Health_Plan_ID__c FROM Contact WHERE Id=: (Id)conId LIMIT 1];
                newCase.ContactId = c.Id;
                newCase.AccountId = c.AccountId;
                newCase.SuppliedPhone = c.Phone;
                newCase.SuppliedEmail = c.Email;
                newCase.DOB__c = c.Birthdate;
                newCase.PolicyNumber__c = c.Health_Plan_ID__c;
                newCase.CallerType__c = RecordTypes.recordTypesById.get(c.RecordTypeId).Name;
                selectedCallerType = RecordTypes.recordTypesById.get(c.RecordTypeId).Name;
                if(String.isBlank(actionTaken.Email__c)){actionTaken.Email__c = c.Email;}
                
            }else{
                // THEN THE ENTERED CALLER IS NOT CREDENTIALED
                nonCredContact = true;
                newCase.SuppliedName = conId;
            }
        }else{
        	// throw error
        	//errorFlag = true;
        }
        
        
        if(String.isNotBlank(newCase.AccountId)){
            if(!isValidSalesforceId(newCase.AccountId, Account.class)){
                nonCredAccount = true;
                newCase.SuppliedCompany = newCase.AccountId;
            }
        }
    }

    public void nonCredCon(){
        if(!nonCredContact){
            nonCredContact = true;
        }
    }

    public void nonCredAcc(){
        if(!nonCredAccount){
            nonCredAccount = true;
        }
    }
    
    public void resetValidations(){
        nonCredContact = false;
        nonCredAccount = false;
    }

    public void createNewContact(){
        newContactOther = new Contact();
        //set the record type
        newContactOther.RecordTypeId = RecordTypes.availableContactTypes.get(selectedCallerType).getRecordTypeId();
    }

    public void createNewPatientContact(){
        System.debug('IsPatientCon'+isNewContactPatient);
        newContact = new Contact();
        //set the record type
        newContact.RecordTypeId = RecordTypes.patientId;
    }

    public String selectedCallAboutType {get;set;}
    public  List<SelectOption> getCallAboutType(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--None--'));
        options.add(new SelectOption('Patient', 'Patient'));
        options.add(new SelectOption('Payor', 'Payor'));
        options.add(new SelectOption('Provider', 'Provider'));

        return options;
    }

    public String selectedDetailRecType {get;set;}
    public List<SelectOption> getDetailsRecType(){
        List<SelectOption> options = new List<SelectOption>();****/
        /*for(Id i: Schema.SObjectType.AddCaseDetails__c.getRecordTypeInfosById().keySet()) {
            options.add(new SelectOption(Schema.SObjectType.AddCaseDetails__c.getRecordTypeInfosById().get(i).getName(),Schema.SObjectType.AddCaseDetails__c.getRecordTypeInfosById().get(i).getName()));
        }*/
        /*for(String s: RecordTypes.availableDetailTypes.keySet()){
            if(s != 'Add Combination'){
                options.add(new SelectOption(s,s));
            }   
        }

        options.add(0, new SelectOption('','---None---'));
        return options;
    }

    public Boolean addDetailsError {get;set;}
    public static String addDetailsErrorMsg {get;set;}
    
    public void addCaseDetailsToList(){
        
        if(caseDetail != null && String.isNotBlank(selectedDetailRecType)) {
                
            addDetailsError = validateAddedDetail();
            
            if(addDetailsError != true){
                caseDetail.RecordTypeId = RecordTypes.availableDetailTypes.get(selectedDetailRecType).getRecordTypeId();
                caseDetail.AdditionalDetail__c = selectedDetailRecType;
                if(caseDetail.AdditionalDetail__c == 'Que Claim ID'){
                  newCase.QueClaimID__c = caseDetail.QueClaimID__c;
                }
                additionalDetails.add(caseDetail);
                caseDetail = new AddCaseDetails__c(); //reset case detail
            }
            
        }else{
            errorFlag = true;
        }
    }
    
    public Boolean validateAddedDetail(){
        
        addDetailsError = false;
        addDetailsErrorMsg = '';
        
        if(selectedDetailRecType == 'Authorization Number' && String.isBlank(caseDetail.AuthorizationNumber__c)){
            	addDetailsErrorMsg = 'PLEASE INPUT AN AUTHORIZATION NUMBER';
                return true;
            }
            else if(selectedDetailRecType == 'HCPC Code' && caseDetail.HCPC_Code__c == null){
                addDetailsErrorMsg = 'PLEASE INPUT A HCPC CODE';
                return true;
            }
            else if(selectedDetailRecType == 'Order Number' && caseDetail.Order__c == null){
                addDetailsErrorMsg = 'PLEASE INPUT AN ORDER NUMBER';
                return true;
            }
            else if(selectedDetailRecType == 'Que Claim ID' && String.isBlank(caseDetail.QueClaimID__c)){
                addDetailsErrorMsg = 'PLEASE INPUT A QUE CLAIM ID';
                return true;
            }
            else if(selectedDetailRecType == 'Referred Provider' && caseDetail.ReferredProvider__c == null){
                addDetailsErrorMsg = 'PLEASE INPUT A PROVIDER';
                return true;
            }
            else if(selectedDetailRecType == 'zPaper Reference Number' && caseDetail.zPaperReferenceNumber__c == null){
                return true;
            }else{
                if(currentUser.Belongs_To__c == 'Claims' && additionalDetails.size() > 0 && selectedDetailRecType == 'Que Claim ID'){
                    // can't have more than one claim ID for Claims user
                    for(AddCaseDetails__c ac : additionalDetails){
                        if(ac.AdditionalDetail__c == 'Que Claim ID'){
                            addDetailsErrorMsg = 'ONE INQUIRY CAN CONTAIN ONLY ONE CLAIM';
                            return true;
                        }
                    }
                }
            }
        
        return false;
    }

    public String actionSelected {get;set;}
    public List<SelectOption> getActions(){
        List<SelectOption> options = new List<SelectOption>();
        options.clear();
        if(String.isNotEmpty((String)newCase.CaseReasonDetail__c)) {
            Map<String,String> actions = CaseReasonRelationshipHelper_IP.actionAndDetails((String)newCase.CaseReasonDetail__c);
            try {

                /*if((CaseReasonRelationshipHelper_IP.actionAndDetails((String)newCase.CaseReasonDetail__c)).size() > 0) {
                    for(String s: CaseReasonRelationshipHelper_IP.actionAndDetails((String)newCase.CaseReasonDetail__c).keySet()) {
                        options.add(new SelectOption(s,s));
                    }
                    options.sort();
                }*/
                /*if(!actions.isEmpty()){
                    for(String s : actions.keySet()){
                        options.add(new SelectOption(s,s));
                    }
                    options.sort();
                    options.add(0, new SelectOption('','---None---'));
                }else{
                    options.add(new SelectOption('','---None---'));
                }
            } catch(Exception e) {
                System.debug(e.getMessage());
            }
            
        }else {

            options.add(new SelectOption('','---None---'));
        }
        return options;
    }

    public String actionDetailSelected{get;set;}
    public List<SelectOption> getActionDetails(){
        List<SelectOption> options = new List<SelectOption>();
        options.clear();
        if(String.isNotEmpty((String)newCase.CaseReasonDetail__c) && String.isNotBlank(actionSelected)) {
            Set<String> actionDetails = new Set<String>();
            if(String.isBlank(actionSelected)){
                actionDetails.addAll(CaseReasonRelationshipHelper_IP.actionAndDetails((String)newCase.CaseReasonDetail__c).values());
            }else{
                if((CaseReasonRelationshipHelper_IP.actionDetailsBasedOnAction((String)newCase.CaseReasonDetail__c, actionSelected)).size() > 0) {
          
                    actionDetails.addAll(CaseReasonRelationshipHelper_IP.actionDetailsBasedOnAction((String)newCase.CaseReasonDetail__c, actionSelected));
                }
            }
            
            try{
                if(!actionDetails.isEmpty()){
                    for(String s : actionDetails){
                        options.add(new SelectOption(s,s));
                    }
                    options.sort();
                    options.add(0, new SelectOption('','---None---'));
                }else{
                    options.add(new SelectOption('','---None---'));
                }
            }catch(System.Exception e){
                System.debug('Error on Action Detail'+e.getMessage());
            }
            
        }else {

            options.add(new SelectOption('','---None---'));
        }

        return options;
    }

    

    public void copyPatientInfo(){
        if(selectedCallerType == 'Patient' && selectedCallAboutType == 'Patient'){
            newCase.Patient__c = newCase.ContactId;
            newCase.Patient_Payor__c = newCase.AccountId;
        }

        if(selectedCallerType != 'Patient' && selectedCallAboutType == 'Patient'){
            if(newCase.Patient__c != null){
                Contact patCon = [SELECT Id, Name, Birthdate, AccountId, Account.Status__c, Phone, Email, Health_Plan_ID__c FROM Contact WHERE Id=: newCase.Patient__c LIMIT 1];
                newCase.Patient__c = patCon.Id;
                newCase.Patient_Payor__c = patCon.AccountId;
                newCase.DOB__c = patCon.Birthdate;
                newCase.PolicyNumber__c = patCon.Health_Plan_ID__c;
            }
        }
    }

    public void saveNewContact(){
        system.debug('Entering save new contact method');
        /*Contact con = new Contact();
        con.LastName = this.lastNameOther;
        con.FirstName = this.newContact.FirstName;
        con.AccountId = this.newContact.AccountId;
        con.Phone = this.newContact.Phone;*/

        /*if(String.isNotEmpty(newContactOther.FirstName) && String.isNotEmpty(this.lastNameOther)) {
            if(String.isEmpty(this.lastNameOther)){
                System.debug('The string is empty'+this.lastNameOther);
            }
            this.newContactOther.LastName = this.lastNameOther;
            System.debug('Contact Last Name'+newContactOther.LastName);
            // contactValidation();
            //if(!errorFlag){
                Database.SaveResult insertCon = Database.insert(this.newContactOther, true);
                if(insertCon.isSuccess()) {
                    Contact c = [SELECT Id, FirstName, LastName, AccountId, Health_Plan_ID__c, Birthdate, Phone, Email FROM Contact WHERE Id=: insertCon.getId()];
                    System.debug('Contact'+c);
                    if(c != null) {
                        newCase.ContactId = c.Id;
                        newCase.AccountId = c.AccountId;
                        newCase.SuppliedPhone = c.Phone;
                        newCase.SuppliedEmail = c.Email;
                    }
                }
            //}
        }
    }
    
    public void patientSaveRouter(){
        // NOTE: Optimize a better way to route the process
        if(String.isNotBlank(selectedCallerType) && (String.isBlank(selectedCallAboutType) || selectedCallAboutType != 'Patient')){
            saveNewPatientContact();
        }else if(String.isNotBlank(selectedCallerType) && String.isNotBlank(selectedCallAboutType) && selectedCallAboutType == 'Patient'){
            saveNewPatientFromCaseDetail();
        }else {
            
        }
    }

    public void saveNewPatientContact(){
        
        System.debug('Enter create patient');
        if(String.isNotEmpty(newContact.FirstName) && String.isNotEmpty(lastName)) {
            this.newContact.LastName = lastName;
            System.debug('Inserting Patient');
            Database.SaveResult insertCon = Database.insert(this.newContact, true);
            System.debug('Afte Inserting'+insertCon.isSuccess());
            if(insertCon.isSuccess()) {
                Contact c = [SELECT Id, FirstName, LastName, AccountId, Health_Plan_ID__c, Birthdate, Phone, Email FROM Contact WHERE Id=: insertCon.getId()];
                System.debug('Contact'+c);
                if(c != null) {
                    
                        System.debug('Contact');
                        newCase.ContactId = c.Id;
                        newCase.AccountId = c.AccountId;
                        newCase.SuppliedPhone = c.Phone;
                        newCase.SuppliedEmail = c.Email;
                        newCase.PolicyNumber__c = c.Health_Plan_ID__c;
                        newCase.DOB__c = c.Birthdate;                    
                    /*if(String.isNotBlank(selectedCallerType)){
                       
                    }*/
                /*}
            }
        }
    }
    
    public void saveNewPatientFromCaseDetail(){
        
        if(String.isNotEmpty(newContact.FirstName) && String.isNotEmpty(lastName)) {
            this.newContact.LastName = lastName;
            System.debug('Inserting Patient');
            Database.SaveResult insertCon = Database.insert(this.newContact, true);
            System.debug('Afte Inserting'+insertCon.isSuccess());
            if(insertCon.isSuccess()) {
                Contact c = [SELECT Id, FirstName, LastName, AccountId, Health_Plan_ID__c, Birthdate, Phone, Email FROM Contact WHERE Id=: insertCon.getId()];
                System.debug('Contact'+c);
                if(c != null) {
                        System.debug('Patient');
                        newCase.Patient__c = c.Id;
                        newCase.Patient_Payor__c = c.AccountId;
                        newCase.PolicyNumber__c = c.Health_Plan_ID__c;
                        newCase.DOB__c = c.Birthdate;
                    /*if(String.isNotBlank(selectedCallerType)){
                       
                    }*/
                    /*copyPatientInfo();
                }
            }
        }
    }
    
    /***************************** START DATABASE SAVE OPERATIONS **************************************/
    
    /*public PageReference save(){
            Boolean isSaveSuccess = saveThisCase();

            if(isSaveSuccess){

                
                Case ca = [SELECT Id, Account.Name, AccountId, ContactId, DOB__c, SuppliedName, SuppliedCompany, SuppliedPhone, SuppliedEmail, Contact.Name, PolicyNumber__c, PayerAccount__c, Provider_Account__c, CaseNumber, Status, Subject, Description, CaseReason__c, CaseReasonDetail__c , Intake__c
                           FROM Case
                           WHERE Id=: (Id)thisCaseId LIMIT 1];
                           
                updateCaseStatusFromActions(ca);

                initCase();
                newCase.AccountId = ca.AccountId;
                newCase.ContactId = ca.ContactId;
                newCase.SuppliedPhone = ca.SuppliedPhone;
                newCase.SuppliedEmail = ca.SuppliedEmail;
                newCase.PolicyNumber__c = ca.PolicyNumber__c;
                newCase.DOB__c = ca.DOB__c;
                newCase.Intake__c = ca.Intake__c;
                insertedIntake = ca.Intake__c;
                
                if(String.isNotBlank(ca.SuppliedName)){
                	newCase.SuppliedName = ca.SuppliedName;
                }
                
                if(String.isNotBlank(ca.SuppliedCompany)){
                	newCase.SuppliedCompany = ca.SuppliedCompany;
                }
               
                
            }else{
                //SOMETHING WENT WRONG HERE
            }

        return null;
    }
    
    public PageReference saveAndClone(){
        Boolean isSaveSuccess = saveThisCase();

            if(isSaveSuccess){

                Case ca = [SELECT Id, Account.Name, AccountId, SuppliedName, SuppliedCompany, ContactId, DOB__c, SuppliedPhone, SuppliedEmail, Contact.Name, PolicyNumber__c, 
                           PayerAccount__c, Provider_Account__c, Patient__c, CaseNumber, Status, Subject, Description, CaseReason__c, CaseReasonDetail__c , 
                           Intake__c, NonCredAccount__c, NonCredContact__c
                           FROM Case
                           WHERE Id=: (Id)thisCaseId LIMIT 1];
                           
                updateCaseStatusFromActions(ca);

                initCase();
                newCase.Intake__c = ca.Intake__c;
                insertedIntake = ca.Intake__c;
                newCase.CaseReason__c = ca.CaseReason__c;
                newCase.CaseReasonDetail__c = ca.CaseReasonDetail__c;
                if(ca.PayerAccount__c != null){
                   newCase.PayerAccount__c = ca.PayerAccount__c;
                   selectedCallAboutType = 'Payor';
                }
                if(ca.Provider_Account__c != null){
                   newCase.Provider_Account__c = ca.Provider_Account__c;
                   selectedCallAboutType = 'Provider';
                }
                if(ca.Patient__c != null){
                    newCase.Patient__c = ca.Patient__c;
                    newCase.Patient_Payor__c = ca.Patient_Payor__c;
                    newCase.PolicyNumber__c = ca.PolicyNumber__c;
                    newCase.DOB__c = ca.DOB__c;
                    selectedCallAboutType = 'Patient';
                }
                newCase.AccountId = ca.AccountId;
                newCase.ContactId = ca.ContactId;
                newCase.SuppliedPhone = ca.SuppliedPhone;
                newCase.SuppliedEmail = ca.SuppliedEmail;
                newCase.PolicyNumber__c = ca.PolicyNumber__c;
                newCase.DOB__c = ca.DOB__c;

                if(String.isNotBlank(ca.SuppliedName)){
                    newCase.SuppliedName = ca.SuppliedName;
                }
                
                if(String.isNotBlank(ca.SuppliedCompany)){
                    newCase.SuppliedCompany = ca.SuppliedCompany;
                }

            }else{
                
            }
        
        return null;
    }

    public Intake__c callerId{get;set;}
    // public List<Case> reviewDetails {get;set;}

   /* public PageReference caseReviewList(){

        Boolean isSaveSuccess = saveThisCase();

        if(isSaveSuccess){
            // CALLING THIS METHOD WILL SAVE THE CASE AND THEN RETURN A PAGEREFERENCE TO REVIEW PAGE
            Case ca = [SELECT Id, Account.Name, AccountId, ContactId, DOB__c, SuppliedPhone, SuppliedEmail, Contact.Name, PolicyNumber__c, PayerAccount__c, Provider_Account__c, CaseNumber, Status, Subject, Description, CaseReason__c, CaseReasonDetail__c , Intake__c
                           FROM Case
                           WHERE Id=: (Id)thisCaseId LIMIT 1];
            
            updateCaseStatusFromActions(ca);
            
            if(parentCase == null){
                callerId = [SELECT Id, Name, StartTime__c,AccountName__c, EndTime__c, Contact__r.Name, Contact__c, Contact__r.Account.Name FROM Intake__c WHERE Id=: insertedIntake LIMIT 1];
                callerId.EndTime__c = Datetime.now();
                callerId.Contact__c = newCase.ContactId;
                update callerId;
            }else{
                callerId = [SELECT Id, Name, StartTime__c, EndTime__c, Contact__r.Name, Contact__c, Contact__r.Account.Name FROM Intake__c WHERE Id=: parentCase.Intake__c LIMIT 1];
            }

            reviewDetails = new List<Case>();

            Map<Id, Case> caseMap = new Map<Id, Case>();
            for(Case c: [SELECT Id, CaseNumber, CaseReason__c, CaseReasonDetail__c, (SELECT Id, Case__c, AdditionalDetail__c, AuthorizationNumber__c,
                         HCPC_Code__r.Name, Order__r.Name, QueClaimID__c, ReferredProvider__r.Name, zPaperReferenceNumber__r.Name
                         FROM Additional_Case_Details__r) FROM Case WHERE Intake__c=: callerId.Id]) {

                reviewDetails.add(c);
            }

            PageReference reviewPage = Page.NewCasePageReview_IP;
            reviewPage.setRedirect(false);
            return reviewPage;
        }

        return null;
    }*/
    
    // to reset the selection for next round of selection
    
    /*public void resetActionSelection(){
        actionSelected = '';
        actionDetailSelected = '';
    }

    public static String thisCaseId {get;set;}
    public Integer runCount = 0;

    public Boolean saveThisCase(){
        //TODO: ADD VALIDATION CHECKS BEFORE SAVING THE CASE
        // METHOD DEFINITION: CALL THIS METHOD TO THE SAVE THE CURRENT newCase
        if(actionTaken.Case_Comment__c!= null && actionTaken.Case_Comment__c.length() > 500){
            errorFlag = true;
            // actionTaken.Case_Comment__c.addError('The comments cannot be longer than 500 characters');
            return false;
        }
        
        if(String.isBlank(actionSelected)){
            errorFlag = true;
            return false;
        }
        
        if(String.isBlank(actionDetailSelected)){
            errorFlag = true;
            return false;
        }
        
        if(nonCredContact){
        	newCase.NonCredContact__c = true;
        }
        
        if(nonCredAccount){
        	newCase.NonCredAccount__c = true;
        }

        // TODO : Optimize this to dynamic SOQL

        if(newCase.Patient__c != null) {
            Contact c = [SELECT Id, FirstName, LastName, Account.Name FROM Contact WHERE Id=: newCase.Patient__c LIMIT 1];
            if(c != null) {
                newCase.Patient_First_Name__c = c.FirstName;
                newCase.Patient_Last_Name__c = c.LastName;
                newCase.PatientHealthPlan__c = c.Account.Name;
            }
        }

        if(newCase.PayerAccount__c != null) {
            Account acc = [SELECT Id, Name FROM Account WHERE Id=: newCase.PayerAccount__c LIMIT 1];
            if(acc != null) {
                newCase.Payor__c = acc.Name;
            }
        }

        if(newCase.Provider_Account__c != null) {
            Account acc = [SELECT Id, Name FROM Account WHERE Id=: newCase.Provider_Account__c LIMIT 1];
            if(acc!= null) {
                newCase.Provider__c = acc.Name;
            }
        }
        
        Database.DMLOptions dmo = new Database.DMLOptions();
        dmo.AssignmentRuleHeader.useDefaultRule = false;

        try {

            if(parentCase == null){

                if(newCase.Intake__c == null && runCount < 1){
                    //THEN THE RECORD HAS NOT BEEN INSERTED YET. INSERT REC
                    Intake__c newIntake = new Intake__c(StartTime__c = entryStartTime);
                    
                    if(String.isNotBlank(newCase.SuppliedName)){
                        newIntake.CallerName__c = newCase.SuppliedName;
                    }
                    
                    if(String.isNotBlank(newCase.SuppliedCompany)){
                        newIntake.CallerCompany__c = newCase.SuppliedCompany;
                    }
                    insert newIntake;
                    insertedIntake = newIntake.Id;
                    newCase.Intake__c = newIntake.Id;
                }

                runCount++;
            }

            newCase.setOptions(dmo);
            
            Database.SaveResult insertCase = Database.insert(this.newCase, true);

            if(insertCase.isSuccess()) {
                // get the id and use it as parent
                if(additionalDetails.size() > 0) {
                    for(AddCaseDetails__c ad: additionalDetails) {
                        ad.Case__c = insertCase.getId();
                    }

                    insert additionalDetails;
                }

                if(actionTaken != null) {
                    actionTaken.Case__c = insertCase.getId();
                    actionTaken.CaseAction__c = actionSelected;
                    actionTaken.CaseActionDetail__c = actionDetailSelected;
                    actionTaken.DoNotUpdate__c = true;
                    insert actionTaken;
                }

                thisCaseId = insertCase.getId();

                return true;
            }       
        } catch(DmlException e){
            System.debug(e.getMessage());
        } catch(Exception e) {
            System.debug(e.getMessage());
        }
        return false;
    }
    
    public void updateCaseStatusFromActions(Case ca){

        try {
            Case_Reasons_Relationship__c crr = [SELECT Id, UpdatedCaseStatus__c FROM Case_Reasons_Relationship__c
                                            WHERE CaseReasonDetail__c =:newCase.CaseReasonDetail__c AND  Action__c =:actionSelected AND ActionDetail__c =:actionDetailSelected  LIMIT 1];
            if(crr != null){
               ca.Status = (String)crr.UpdatedCaseStatus__c.trim();
               ca.Debug_Log__c = 'Status Queried from Case Reason Relationship: '+ crr.UpdatedCaseStatus__c;
            }else{
               ca.Debug_Log__c = 'No Case Reason Relationship found';
            }
                
            update ca;
        } catch(Exception e) {
            System.debug(e.getMessage());
        }
    }

    public void resetErrorFlag(){
        errorFlag = false;
        errorList = new String[]{};
    }

    /*public void contactValidation(){

        if(String.isBlank(newContact.LastName)){
            errorFlag = true;
            errorList.add('LAST NAME IS REQUIRED TO STORE A CONTACT');
        }

        if(String.isBlank(newContact.FirstName)){
            errorFlag = true;
            errorList.add('FIRST NAME IS REQUIRED TO STORE A CONTACT');
        }

        if(newContact.AccountId == null){
            errorFlag = true;
            errorList.add('ACCOUNT IS REQUIRED TO STORE A CONTACT');
        }

        if(String.isBlank(newContact.Health_Plan_ID__c) && selectedCallerType == 'Patient'){
            errorFlag = true;
            errorList.add('PATIENT POLICY NUMBER IS REQUIRED TO STORE A CONTACT');
        }

        if(newContact.Birthdate == null && selectedCallerType == 'Patient'){
            errorFlag = true;
            errorList.add('PATIENT BIRTHDATE IS REQUIRED TO STORE A CONTACT');
        }

    }*/

    /*public PageReference cancel(){
        PageReference returnPg = Page.NewCasePage_IP;
        returnPg.setRedirect(true);
        return returnPg;
    }

    public PageReference exitThisPage(){
        PageReference exitPage;
        
        Boolean isSaveSuccess = saveThisCase();

        if(isSaveSuccess){
            // CALLING THIS METHOD WILL SAVE THE CASE AND THEN RETURN A PAGEREFERENCE TO REVIEW PAGE
            Case ca = [SELECT Id, Account.Name, AccountId, ContactId, DOB__c, SuppliedPhone, SuppliedEmail, Contact.Name, PolicyNumber__c, PayerAccount__c, Provider_Account__c, CaseNumber, Status, Subject, Description, CaseReason__c, CaseReasonDetail__c , Intake__c
                           FROM Case
                           WHERE Id=: (Id)thisCaseId LIMIT 1];
            
            updateCaseStatusFromActions(ca);
            
            if(parentCase == null){
                callerId = [SELECT Id, Name, StartTime__c,AccountName__c, EndTime__c, Contact__r.Name, Contact__c, Contact__r.Account.Name FROM Intake__c WHERE Id=: insertedIntake LIMIT 1];
                callerId.EndTime__c = Datetime.now();
                callerId.Contact__c = newCase.ContactId;
                update callerId;
            }

            if(parentCase != null && parentCase.Status != 'Closed') {
                parentCase.Status = 'Closed';
                update parentCase;
            }

            if(parentCase != null){
                exitPage = new PageReference('/'+parentCase.Id);
            }else{
                exitPage = new PageReference('/'+callerId.Id);
            }
            exitPage.setRedirect(true);
            return exitPage;
        }

        return null;
    }

    /**
     * Test a String to see if it is a valid SFDC  ID
     * @param  sfdcId The ID to test.
     * @param  t      The Type of the sObject to compare against
     * @return        Returns true if the ID is valid, false if it is not.
     */
    /*public static Boolean isValidSalesforceId( String sfdcId, System.Type t ){
        try {

            if ( Pattern.compile( '[a-zA-Z0-9]{15}|[a-zA-Z0-9]{18}' ).matcher( sfdcId ).matches() ){
                // Try to assign it to an Id before checking the type
                Id id = sfdcId;

                // Use the Type to construct an instance of this sObject
                sObject sObj = (sObject) t.newInstance();

                // Set the ID of the new object to the value to test
                sObj.Id = id;

                // If the tests passed, it's valid
                return true;
            }
        }catch ( Exception e ){
                // StringException, TypeException
            }

        // ID is not valid
        return false;
    }*/
}