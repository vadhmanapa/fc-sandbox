public with sharing class CallLogRelationTriggerDispatcher {

	public static void doBeforeInsert(List<Call_Log_Relation__c> nList) {
		checkExistingRelations(nList);
	}
	
	public static void doBeforeUpdate(List<Call_Log_Relation__c> nList) {
		checkExistingRelations(nList);
	}

	/*
	 *	Trigger Dispatcher Functions
	 */

	public static Integer relationCount(Call_Log_Relation__c relation) {
		
		system.debug('Checking relation: ' + relation);
		
		Integer relationCount = 0;
		
		if(relation.Account__c != null) {
			system.debug('Found Account relation.');
			relationCount++;
		}

		if(relation.Case__c != null) {
			system.debug('Found Account relation.');
			relationCount++;
		}
		
		if(relation.Claims_Inquiry__c != null) {
			system.debug('Found Account relation.');
			relationCount++;
		}
		
		if(relation.Contact__c != null) {
			system.debug('Found Account relation.');
			relationCount++;
		}
		
		if(relation.Order__c != null) {
			system.debug('Found Account relation.');
			relationCount++;
		}
		
		return relationCount;
	}

	public static void checkExistingRelations(List<Call_Log_Relation__c> nList) {
		
		for(Call_Log_Relation__c relation : nList) {
			
			//Do not allow updating
			if(relation.Id != null) {
				relation.addError('This relation cannot be edited once it is created!');
				continue;
			}
			
			if(relationCount(relation) > 1) {
				relation.addError('This relation can only have one [ Account, Contact, Case, Inquiry, or Order ].');
				continue;
			}
		}
	}
}