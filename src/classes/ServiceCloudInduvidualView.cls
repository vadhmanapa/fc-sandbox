public class ServiceCloudInduvidualView {
    Datetime myDateTime = Datetime.now();
        Datetime newdate = myDateTime.addDays(-2);
    // get the picklist date
  
    
    public List<callsGaugeData> getnCalls(){
        Integer totalCalls = 0;
        AggregateResult numCalls = [Select count(Id)callCount from Call_Log__c where RecordType.Name = 'Customer Support' AND CreatedBy.Id =: UserInfo.getUserId() AND CreatedDate = TODAY];
        List<callsGaugeData> allCalls = new List<callsGaugeData>();
        allCalls.add(new callsGaugeData(Integer.valueOf(numCalls.get('callCount'))));
        return allCalls;
    }
    public class callsGaugeData{
        public String name{get;set;}
        public Integer numCalls{get;set;}
        
        public callsGaugeData(Integer numCalls){
            this.name = UserInfo.getName();
            this.numCalls = numCalls;
            
        }
    }
    //-------------------------------------------------------------------------------------------------------------------------------------------------------------
      public List<emailGaugeData> getnEmail(){
        Integer totalCalls = 0;
        AggregateResult numEmails = [Select count(Id)emailCount from Case where RecordType.Name = 'Email' AND Owner.Id =: UserInfo.getUserId() AND Status != 'Closed'];
        List<emailGaugeData> allEmail = new List<emailGaugeData>();
        allEmail.add(new emailGaugeData(Integer.valueOf(numEmails.get('emailCount'))));
        return allEmail;
    }
    public class emailGaugeData{
        public String name{get;set;}
        public Integer numEmails{get;set;}
        
        public emailGaugeData(Integer numEmails){
            this.name = UserInfo.getName();
            this.numEmails = numEmails;
            
        }
    }
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------
    public List<caseGaugeData> getnCase(){
        Integer totalCalls = 0;
        AggregateResult numCase = [Select count(Id)caseCount from Case where RecordType.Name = 'Standard' AND Owner.Id =: UserInfo.getUserId() AND Status != 'Closed'];
        List<caseGaugeData> allCase = new List<caseGaugeData>();
        allCase.add(new caseGaugeData(Integer.valueOf(numCase.get('caseCount'))));
        return allCase;
    }
    public class caseGaugeData{
        public String name{get;set;}
        public Integer numCase{get;set;}
        
        public caseGaugeData(Integer numCase){
            this.name = UserInfo.getName();
            this.numCase = numCase;
            
        }
    }
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------------
}