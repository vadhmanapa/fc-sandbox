/*
NotificationService_CS service class that should contain methods to work with notification flows
 */
public with sharing class NotificationService_CS {

    private Boolean isScheduled;
    private Boolean isAsync;

    public NotificationService_CS() {
        this.isScheduled = System.isScheduled();
        this.isAsync = (System.isBatch() || System.isFuture() || System.isQueueable());
    }

    /*
    sendOrderNotification - general entry point to send email notification for any Order change
    Can be extended with different email conditions

    @param newOrdersById - Trigger.newMap context
    @param oldOrdersById - Trigger.oldMap context
     */
    public void sendOrderNotification(Map<Id, Order__c> newOrdersById, Map<Id, Order__c> oldOrdersById) {
        if (this.isScheduled) {
            sendNotificationWhenOrderRemovedFromFutureDos(newOrdersById, oldOrdersById);
        }
    }

    /*
    sendNotificationWhenOrderRemovedFromFutureDos sends notification email when Order records move
    from “Future DOS” category.

    @param newOrdersById - Trigger.newMap context
    @param oldOrdersById - Trigger.oldMap context
     */
    public void sendNotificationWhenOrderRemovedFromFutureDos(Map<Id, Order__c> newOrdersById, Map<Id, Order__c> oldOrdersById) {
        Map<Id, Order__c> oldOrdersWithChangedFutureDosStatus = getOldOrdersWithChangedFutureDosStatus(newOrdersById, oldOrdersById);
        String emailTemplateName = Future_Dos_Setting__c.getInstance().Un_Mark_Future_Dos_Email_Template__c;
        //If conditions were not met or there is no email template in custom setting -> do not proceed
        if (oldOrdersWithChangedFutureDosStatus.isEmpty() || String.isEmpty(emailTemplateName)) return;

        List<EmailTemplate> emailTemplates = [
            SELECT
                Id
            FROM EmailTemplate
            WHERE Name = :emailTemplateName
            LIMIT 1
        ];
        //If email template specified in custom setting is not found -> do not proceed
        if (emailTemplates.isEmpty()) return;

        List<Messaging.SingleEmailMessage> emails = createOrderEmails(emailTemplates[0].Id, oldOrdersWithChangedFutureDosStatus);
        List<Messaging.SendEmailResult> sendEmailResults = Messaging.sendEmail(emails);
        for (Messaging.SendEmailResult result : sendEmailResults) {
            if (!result.isSuccess()) {
                System.debug('\n\n result.getErrors() => ' + result.getErrors() + '\n');
            }
        }
    }

    /*
    getOldOrdersWithChangedFutureDosStatus filters Order records by checking that Order changed its Future DOS category to other

    @param newOrdersById - Trigger.newMap context
    @param oldOrdersById - Trigger.oldMap context
    @return map of filtered Orders from Trigger.oldMap context
     */
    private Map<Id, Order__c> getOldOrdersWithChangedFutureDosStatus(Map<Id, Order__c> newOrdersById, Map<Id, Order__c> oldOrdersById) {
        Map<Id, Order__c> ordersWithChangedFutureDosStatus = new Map<Id, Order__c>();
        for (Order__c newOrder : newOrdersById.values()) {
            Order__c oldOrder = oldOrdersById.get(newOrder.Id);
            if (oldOrder.Status__c == OrderModel_CS.STATUS_FUTURE_DOS && newOrder.Status__c != oldOrder.Status__c) {
                ordersWithChangedFutureDosStatus.put(oldOrder.Id, oldOrder);
            }
        }
        return ordersWithChangedFutureDosStatus;
    }

    /*
    createOrderEmails create a list of emails that should be send to Order's Accepted By Contact

     @param emailTemplateId - Email Template Id
     @param ordersById - collection of Order records
     @return list of emails for sending
     */
    private List<Messaging.SingleEmailMessage> createOrderEmails(Id emailTemplateId, Map<Id, Order__c> ordersById) {
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        for (Order__c order : ordersById.values()) {
            if (order.Accepted_By__c != null) {
                Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                email.setTargetObjectId(order.Accepted_By__c);
                email.setTemplateId(emailTemplateId);
                email.setWhatId(order.Id);
                emails.add(email);
            }
        }
        return emails;
    }

}