/**
 *  @Description Services class to access custom settings within code safely.
 *		To add new settings: 
 *			Add a new @testVisible private static variable of the type with a get method.
 *			Add a new getter method.
 *  @Author Cloud Software LLC
 *  Revision History: 
 *		01/07/2015 - karnold - Added header. Added section. Added setting for Clear_Algorithm_Routing__c
 *
 */
public class CustomSettingServices { 

	@TestVisible private static final String DEFAULT_NAME = 'Defaults';

	/* Private Custom Setting Objects */

	@TestVisible private static Global_Switches__c globalSwitches {
		get{
			if (globalSwitches == null) {
				if (Global_Switches__c.getAll().containsKey(DEFAULT_NAME)){
					globalSwitches = Global_Switches__c.getInstance(DEFAULT_NAME);
				} else {
					globalSwitches = new Global_Switches__c();
				}
			}
			return globalSwitches;
		} set;
	}

	@TestVisible private static Address_Encryption_Key__c addressEncrtyptionKey {
		get{
			if (addressEncrtyptionKey == null) {
				if (Address_Encryption_Key__c.getAll().containsKey(DEFAULT_NAME)){
					addressEncrtyptionKey = Address_Encryption_Key__c.getInstance(DEFAULT_NAME);
				} else {
					addressEncrtyptionKey = new Address_Encryption_Key__c();
				}
			}
			return addressEncrtyptionKey;
		} set;
	}

	@TestVisible private static Clear_Algorithm_Routing__c clearAlgorithmRouting {
		get{
			if (clearAlgorithmRouting == null) {
				if (Clear_Algorithm_Routing__c.getAll().containsKey(DEFAULT_NAME)){
					clearAlgorithmRouting = Clear_Algorithm_Routing__c.getInstance(DEFAULT_NAME);
				} else {
					clearAlgorithmRouting = new Clear_Algorithm_Routing__c();
				}
			}
			return clearAlgorithmRouting;
		} set;
	}

	/* Getter Methods */

	public static Boolean getCreateOrderTriggersOff() {
		return returnBoolean(globalSwitches, 'Create_Order_Trigger_Off__c', false);
	}

	public static Boolean getCreateProvLocTriggersOff() {
		return returnBoolean(globalSwitches, 'Create_ProvLoc_Trigger_Off__c', false);
	}

	public static String getEncryptionKey() {
		return returnString(addressEncrtyptionKey, 'Key__c', EncodingUtil.base64Encode(Crypto.generateAesKey(128)));
	}

	public static Boolean getUseNewAlgorithm() {
		return returnBoolean(clearAlgorithmRouting, 'Use_New_Algorithm__c', false);
	}

	/* Object Return Method */

	private static Boolean returnBoolean(SObject customSetting, String fieldName, Boolean defaultValue) {
		if ((Boolean) customSetting.get(fieldName) != null){
			return (Boolean) customSetting.get(fieldName);
		} else {
			return defaultValue;
		}
	}

	private static String returnString(SObject customSetting, String fieldName, String defaultValue) {
		if ((String) customSetting.get(fieldName) != null){
			return (String) customSetting.get(fieldName);
		} else {
			return defaultValue;
		}
	}
}