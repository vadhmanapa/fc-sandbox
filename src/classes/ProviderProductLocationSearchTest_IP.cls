@isTest
private class ProviderProductLocationSearchTest_IP
{
	// initialize test variables
	private static User u;
	private static ProviderProductLocationSearchService_IP scc;
	private static County_Item__c ci;
	private static Integer resultCount;

	private static void initializeTest()
	{
		// Profile
		Profile p = [Select id from Profile where Name ='Integra Standard User'];
		Double i = Math.random()*10000; // for user name

		// Create user
		u = new User(LastName='User1'+i, 
						  Alias='use',
						  Email='test'+i+'@test.com',
						  Username='testing'+i+'@testingcenter.com',
						  CommunityNickname='nick',
						  EmailEncodingKey='UTF-8',
						  LanguageLocaleKey='en_US',
						  LocaleSidKey='en_US',
						  ProfileId = p.Id,
						  TimeZoneSidKey='America/New_York');
		insert u;

		// Then

		System.runAs(u){
			//---------------create Account----------------

			Account activeAcc = new Account(Name = 'Test Account', Type_Of_Provider__c = 'DME', Status__c = 'Active');
			insert activeAcc;

			// ------------US State and Counties----------------

			Set<String> states = new Set<String> { 'Alabama', 'Alaska', 'New York', 'New Jersey' };
			Set<String> testCountyNames = new Set<String> { 'Queens', 'Kings', 'Spades', 'Diamonds' };
			List<US_State_County__c> allIns = new List<US_State_County__c> ();
			for (String s : states) {
				for (String con : testCountyNames) {
					US_State_County__c newCounty = new US_State_County__c(Name = con, State__c = s);
					allIns.add(newCounty);
				}
			}

			insert allIns;

			//------------Create Provider Location--------------------

			Provider_Location__c plActive = new Provider_Location__c(Account__c = activeAcc.Id, Name_DBA__c = 'Test Account Store', Store_Status__c = 'Active');
			insert plActive;

			Provider_Location__c plActiveSec = new Provider_Location__c(Account__c = activeAcc.Id, Name_DBA__c = 'Test Account Store 2', Store_Status__c = 'Active');
			insert plActiveSec;

			Provider_Location__c plInactive = new Provider_Location__c(Account__c = activeAcc.Id, Name_DBA__c = 'Test Account Store', Store_Status__c = 'Closed', Store_Closure_Date__c = System.today());
			insert plInactive;

			Provider_Location__c plActiveNat = new Provider_Location__c(Account__c = activeAcc.Id, All_States_and_Counties__c = true, Name_DBA__c = 'Test Account Store 2', Store_Status__c = 'Active');
			insert plActiveNat;

			//----------Create Counties Served-----------------------

			List<County_Item__c> listServiced = new List<County_Item__c>();

			for(US_State_County__c us: [Select Id,Name, State__c from US_State_County__c where State__c = 'New York']) {
				listServiced.add(new County_Item__c(Provider_Location_New__c = plActive.Id, US_State_County__c = us.Id,State__c = us.State__c, County_Name__c = us.Name));
				listServiced.add(new County_Item__c(Provider_Location_New__c = plActiveSec.Id, US_State_County__c = us.Id, State__c = us.State__c, County_Name__c = us.Name));
			}

			insert listServiced;
			listServiced.clear();

			for(US_State_County__c us: [Select Id,Name, State__c from US_State_County__c where State__c = 'Alaska' LIMIT 2]) {
				listServiced.add(new County_Item__c(Provider_Location_New__c = plActiveSec.Id, US_State_County__c = us.Id, State__c = us.State__c, County_Name__c = us.Name));
			}

			insert listServiced;

			//------------ HCPC Database Categories------------------

			Set<String> products = new Set<String> { 'Apnea Monitor and Supplies', 'Bone Growth Stimulator', 'Cranial Orthotics', 'Diabetic Shoes' };
			List<HCPC_Database_Category__c> productIns = new List<HCPC_Database_Category__c>();
			for(String st: products) {
				productIns.add(new HCPC_Database_Category__c(Name = st, Database__c = 'Integra',Equipment_Type__c = 'DME'));
			}
			insert productIns;

			//-------------HCPC Provider Categories------------------

			List<Provider_Category__c> cats = new List<Provider_Category__c>();
			for(HCPC_Database_Category__c hcpc: [SELECT Id, Name, Database__c from HCPC_Database_Category__c]) {
				cats.add(new Provider_Category__c(Name = hcpc.Name, HCPC_Database_Category__c = hcpc.Id, Provider__c = activeAcc.Id));
			}

			insert cats;

			//----------- State Medicaid Identifier------------------

			State_Identifier__c si = new State_Identifier__c(Medicaid__c = '4464646454', State__c = 'New York', Provider_Location_New__c = plActive.Id);
			insert si;

			//----------load page--------------
			PageReference pgRef = Page.ProviderSearchPage;
			Test.setCurrentPageReference(pgRef);

			//--------- load the controller--------

			scc = new ProviderProductLocationSearchService_IP();
			scc.getStateNames();
			scc.getProductDatabaseCategories();
		}

	}

	private static testMethod void selectStateOnlyTest()
	{
		// Given
		initializeTest();

		// When
		Test.startTest();
		scc.stateSelected = 'New York';
		scc.processDispatcher();
		resultCount = scc.locationsTable.size();
		Test.stopTest();

		// Then
		System.assert(resultCount > 0);
	}

	private static testMethod void selectStateAndProductTest()
	{
		// Given
		initializeTest();

		// When
		HCPC_Database_Category__c hc = [SELECT Id from HCPC_Database_Category__c WHERE Name = 'Apnea Monitor and Supplies'];
		Test.startTest();
		scc.stateSelected = 'New York';
		scc.productSelected = hc.Id; //send as ID
		scc.processDispatcher();
		resultCount = scc.locationsTable.size();
		Test.stopTest();

		// Then
		System.assert(resultCount > 0);
	}

	private static testMethod void selectStateAndCountyTest()
	{
		// Given
		initializeTest();

		// When
		Test.startTest();
		scc.stateSelected = 'New York';
		scc.userInputForCounty = 'Queens';
		scc.processDispatcher();
		resultCount = scc.locationsTable.size();
		Test.stopTest();

		// Then
		System.assert(resultCount > 0);
	}

	private static testMethod void selectStateAndMedicaidTest()
	{
		// Given
		initializeTest();

		// When
		Test.startTest();
		scc.stateSelected = 'New York';
		scc.showMedicaidOnly = true;
		scc.processDispatcher();
		resultCount = scc.locationsTable.size();
		Test.stopTest();

		// Then
		System.assert(resultCount > 0);
	}
}