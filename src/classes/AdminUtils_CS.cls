public with sharing class AdminUtils_CS {

	private final Contact c;

	public AdminUtils_CS(ApexPages.StandardController stdController){
		this.c = (Contact)stdController.getRecord();
		system.debug('Contac: ' + c);
	}

	public PageReference logMeIn(){
   		
   		Contact contactDetails = [
   			SELECT
   				Salt__c,
   				Entity__r.Type__c
   			FROM Contact
   			WHERE Id = :c.Id
   		];
   		
   		// Get the session Id and hash
   		String hashedUserId = PasswordServices_CS.hash(c.Id,contactDetails.Salt__c);
		String hashedSessionId = PasswordServices_CS.hash(PortalServices_CS.sessionId,contactDetails.Salt__c);
		System.debug('Hashed userId: ' + hashedUserId);
		System.debug('Hashed sessionId: ' + hashedSessionId);
		
		// Set the userId and sessionId
		Cookie userIdCookie = new Cookie('userid', hashedUserId, null, 3600, true);
		Cookie sessionCookie = new Cookie('sessionid', hashedSessionId, null, 3600, true);
		ApexPages.currentPage().setCookies(new Cookie[]{userIdCookie,sessionCookie});
		
		System.debug('isSandbox ? ' + PortalServices_CS.isRunningInSandbox());
		
		if(contactDetails.Entity__r.Type__c == 'Payer'){
			return Page.PayerHomeDashboard_CS;
		}
		else if(contactDetails.Entity__r.Type__c == 'Provider'){
			return Page.ProviderHomeDashboard_CS;
		}	
		else{
			return null;
		}
	}
}