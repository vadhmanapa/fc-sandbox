public class GeocodingModel_CS {
	public String RecordId;
	public String street;
	public String city;
	public String state;
	public String postalCode;
	public String country;
	public Decimal lat;
	public Decimal lng;
	public Integer travelTime;
	
	
	public String getAddress() {
		String address = '';
		if(street != null)		{ address += street+', '; 	}
		if(city != null) 		{ address += city+', ';		}
		if(state != null) 		{ address += state+' ';			}		
		if(postalCode != null)	{ address += postalCode;	}
		if(country != null) 	{ address += ', '+country;	}
		return address;
	}
	public String getCompressedAddress() {
		//Mapquest returns the address without spaces, used for address matching
		return getAddress().remove(' ');
	}
}