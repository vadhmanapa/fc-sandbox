@isTest
private class CaseNotificationBatchTest {

    static testMethod void CaseNotificationBatch() {

		Contact testContact = new Contact(
			FirstName = 'Test',
			LastName = 'Contact'
		);
		insert testContact;
		
		Case testCase = new Case(
			Subject = 'Test Case',
			ContactId = testContact.Id,
			Due_Date__c = Date.today()
		);
		insert testCase;
		
		Test.startTest();
		
		Database.executeBatch(new CaseNotificationBatch());
		
		Test.stopTest();
    }
}