/*===============================================================================================
 * Cloud Software LLC
 * Name: DeliveryCompletionBatch
 * Description: DeliveryCompletionBatch class file
 * Created Date: 9/30/2015
 * Created By: Andrew Nash (Cloud Software)
 * 
 * 	Date Modified			Modified By			Description of the update
 *	SEP 30, 2015			Andrew Nash			Created
 ==============================================================================================*/
public class DeliveryCompletionBatch implements Database.Batchable<sObject> {
	
	//===========================================================================================
	//=================================== BATCH QUERY START =====================================
	//===========================================================================================
	public Database.QueryLocator start(Database.BatchableContext bc) {
		
		// Create the query string for the SELECT part of the dynamic query
		String queryString = 'SELECT Id, Delivery_Completion_Time__c, Order__c ';
		// Add the FROM part of the dynamic query to the query string
		queryString += 'FROM Order_Assignment__c ';
		// Add the WHERE part of the dynamic query to the query string
		queryString += 'WHERE Unassigned_At__c = null';

		// Return the processed query string as query results
		return Database.GetQueryLocator(queryString); 

	}
	//===========================================================================================
	//================================== BATCH QUERY EXECUTE ====================================
	//===========================================================================================
	public void execute(Database.BatchableContext bc, List<sObject> scope) {
		
		// Execute the batch by calling the service class that processes delivery completion
		DeliveryCompletionBatchServices.processDeliveryCompletion((List<Order_Assignment__c>)scope);

	}
	//===========================================================================================
	//================================== BATCH QUERY FINISH ====================================
	//===========================================================================================
	public void finish(Database.BatchableContext bc) {



	}

}