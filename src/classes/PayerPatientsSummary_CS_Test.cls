@isTest
private class PayerPatientsSummary_CS_Test {

	/******* Test Parameters *******/
	static private final Integer N_PATIENTS = 15;

	/******* Payer *******/
	static private List<Account> payers;
	static private List<Contact> payerUsers;

	/******* Test Objects *******/
	static private PayerPatientsSummary_CS cont;
	public static Contact user;
	public static Account plan;
	public static Account provider;
	public static List<Plan_Patient__c> patientList;

	/******* Test Methods *******/
	
    static testMethod void PayerPatientsSummary_CS_PageLanding() {
        init();
		
		//Page Landing
		PageReference pr = Page.PayerPatientsSummary_CS;
		Test.setCurrentPage(pr);
		TestServices_CS.login(user);
		
		//Page Init
        cont = new PayerPatientsSummary_CS();		
		cont.init();
		
		//Check initial state of page components:
		
		System.debug('Error after initialization');
		
		//Page size is 10
		System.assert(cont.patientSetCon.getPageSize() == 10, 'patientSetCon pageSize ' + 
			cont.patientSetCon.getPageSize() + ' != 10');
		System.debug('Error after page size check');
		
		//Starts on page 1
		System.assert(cont.patientSetCon.getPageNumber() == 1, 'patientSetCon pageNumber ' + 
			cont.patientSetCon.getPageSize() + ' != 1');
		System.debug('Error after page number check');
		
		//Number of records is 15	
		System.assert(cont.patientSetCon.getResultSize() == N_PATIENTS, 'patientSetCon resultSize ' + 
			cont.patientSetCon.getResultSize() + ' != ' + (N_PATIENTS));
		System.debug('Error after number of records check');
		
		//Page size variable matches actual page size
		System.assert(cont.patientList.size() == cont.pageSize, 'patientList size ' + 
			cont.patientList.size() + ' != ' + cont.pageSize);
		System.debug('Error after 2nd page size check');
		
		//Check that since there are 15 records, totalPages = 2
		System.assert(cont.totalPages == 2, 'Total Pages ' + cont.totalPages + ' != 2');
		
    }

    static testMethod void PayerPatientsSummary_CS_permissionTests() {
        init();

        Role__c adminRole = TestDataFactory_CS.generateAdminRole();
        insert adminRole;
        user.Content_Role__c = adminRole.Id;
        update user;

        //Page Landing
        PageReference pr = Page.PayerPatientsSummary_CS;
        Test.setCurrentPage(pr);
        TestServices_CS.login(user);

        //Page Init
        cont = new PayerPatientsSummary_CS();
        cont.init();
        System.assert(cont.showCreatePatients, cont.portalUser);
        System.assert(cont.viewPatientsPermission, cont.portalUser);
        System.assert(cont.editUserPermission, cont.portalUser);
        System.assert(cont.editPatientsPermission, cont.portalUser);
    }
    
    static testMethod void PayerPatientsSummary_CS_PageLandingNoAuth(){
    	init();
    	
    	//Page Landing
		PageReference pr = Page.PayerPatientsSummary_CS;
		Test.setCurrentPage(pr);
		
		//Page Init
        cont = new PayerPatientsSummary_CS();		
		cont.init();
    }

    static testMethod void PayerPatientsSummary_CS_searchPatient_NoSearchString() {
        init();

        //Page Landing
        PageReference pr = Page.PayerPatientsSummary_CS;
        Test.setCurrentPage(pr);
        TestServices_CS.login(user);

        //Page Init
        cont = new PayerPatientsSummary_CS();
        cont.init();

        cont.searchPatient();
        System.assert(String.isNotEmpty(cont.searchStringMessage));
    }

    static testMethod void PayerPatientsSummary_CS_searchPatient() {
        init();

        //Page Landing
        PageReference pr = Page.PayerPatientsSummary_CS;
        Test.setCurrentPage(pr);
        TestServices_CS.login(user);

        //Page Init
        cont = new PayerPatientsSummary_CS();
        cont.init();

        cont.filterBySearch = 'test';
        cont.searchPatient();
        System.assert(String.isEmpty(cont.searchStringMessage));
    }
    
    static testMethod void PayerPatientsSummary_Test_Pagination() {
        init();
		
		//Page Landing
		PageReference pr = Page.PayerPatientsSummary_CS;
		Test.setCurrentPage(pr);
		TestServices_CS.login(user);
		
		//Page Init
        cont = new PayerPatientsSummary_CS();		
		cont.init();
		
		//Navigate to page 2 and test navigation
       	cont.patientSetCon.next();
       	System.assert(cont.patientSetCon.getPageNumber() == 2, 'userSetCon pageNumber ' + 
			cont.patientSetCon.getPageNumber() + ' != 2');
       	
       	//Set page size variable to 20 and check that it updates the actual page size
		cont.pageSize = 20;
		System.assert(cont.patientSetCon.getPageSize() == 20, 'userSetCon pageSize ' + 
			cont.patientSetCon.getPageSize() + ' != 20');
       	
    }
    
    public static void init() {
    	List<Account> providerList = TestDataFactory_CS.generateProviders('Test Provider', 1);
    	provider = providerList[0];
    	insert providerList;
    	
    	List<Account> payerList = TestDataFactory_CS.generatePlans('Test Payer', 1);
    	plan = payerList[0];
    	insert payerList;
    	
    	patientList = TestDataFactory_CS.generatePlanPatientsPlanOnly(plan.Id, N_PATIENTS);
    	insert patientList;

    	user = TestDataFactory_CS.createPlanContacts('Admin', payerList, 1)[0];
    	
    	List<Order__c> orderList = TestDataFactory_CS.generateOrders(patientList[0].Id, provider.Id, 3);
    	for(Order__c o : orderList){
    		o.Entered_By__c = user.Id;
    		o.Status__c = 'New';
    	}
    	insert orderList;
	}
}