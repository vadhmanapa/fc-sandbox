@isTest
private class PayerAccountProfile_CS_Test {
	public static Account payer;
	public static Contact user;
	
    static testMethod void PayerAccountProfile_CS_PageLanding() {
        init();
        Test.setCurrentPage(Page.PayerAccountProfile_CS);
        TestServices_CS.login(user);
        
        PayerAccountProfile_CS cont = new PayerAccountProfile_CS();
        cont.init();
    }
    
    static testMethod void PayerAccountProfile_CS_NotAuthenticated(){
    	init();
    	Test.setCurrentPage(Page.PayerAccountProfile_CS);
    	
    	PayerAccountProfile_CS cont = new PayerAccountProfile_CS();
    	cont.init();
    }
    
    static testMethod void PayerAccountProfile_CS_initMismatch(){
    	init();
    	Account provider = TestDataFactory_CS.generateProviders('TestProvider', 1)[0];
    	insert provider;
    	user.Entity__c = provider.Id;
    	update user;
    	Test.setCurrentPage(Page.PayerAccountProfile_CS);
    	TestServices_CS.login(user);
    	
    	PayerAccountProfile_CS cont = new PayerAccountProfile_CS();
    	cont.init();
    }
    
    public static void init(){
    	List<Account> initAccts = TestDataFactory_CS.generatePlans('TestPayer', 1);
    	payer = initAccts[0];
    	insert payer;
    	
    	user = TestDataFactory_CS.createPlanContacts('Admin', initAccts, 1)[0];
    }
}