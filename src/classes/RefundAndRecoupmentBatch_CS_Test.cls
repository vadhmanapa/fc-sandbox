@isTest
public class RefundAndRecoupmentBatch_CS_Test { 
    
    public static Account testAccount;
    public static List<Provider_Locations__c> oldProvLocs;
    public static List<Provider_Location__c> newProvLoc;
    public static Provider_Location__c dummyProvLoc;
    public static Recoupment__c refRec;
    public static ARAG__c workItem;

    static testmethod void RefundAndRecoupmentBatch_Test() {
        init();

        String query = 'SELECT Id, Provider_Location__c FROM Recoupment__c';

        System.assertEquals(dummyProvLoc.Id, refRec.Provider_Location_New__c);
        
		RefundAndRecoupmentBatch_CS batch = new RefundAndRecoupmentBatch_CS(query);

        Test.startTest();
        Database.executeBatch(batch);
        Test.stopTest();

        Recoupment__c updatedRefRec = [SELECT Id, Provider_Location_New__c FROM Recoupment__c WHERE Id =: refRec.Id];
        System.assertEquals(newProvLoc[0].Id, updatedRefRec.Provider_Location_New__c);
    }

    static void init() {
        Global_Switches__c gs = new Global_Switches__c(
            Name = 'Defaults',
            Create_ProvLoc_Trigger_Off__c = true
        );
        insert gs;

        testAccount = TestDataFactory_CS.generateProviders('Test Account', 1)[0];
        insert testAccount;

        oldProvLocs = TestDataFactory_CS.generateOldProviderLocations(testAccount.Id, 1);
        oldProvLocs[0].Monday__c = 'Closed';
        oldProvLocs[0].Tuesday__c = '9:00am-5:00pm';
        oldProvLocs[0].Wednesday__c = '9:00AM-5:00PM';
        oldProvLocs[0].Thursday__c = ' 9am - 12:00pm';
        oldProvLocs[0].Friday__c = ' 9:00am - 12pm';
        oldProvLocs[0].Saturday__c = '9:30am-5:30pm';
        oldProvLocs[0].Sunday__c = '09am-05:00pm';

        insert oldProvLocs;

        newProvLoc = ProviderLocationTransferServices.transferDataToNewProviderLocationObject(oldProvLocs);

        dummyProvLoc = new Provider_Location__c();
        dummyProvLoc.Account__c = testAccount.Id;
        insert dummyProvLoc;

        workItem = New ARAG__c();
        insert workItem;

        refRec = new Recoupment__c();
        refRec.Related_Claims_Work_Item__c = workItem.Id;
        refRec.Provider_Location__c = oldProvLocs[0].Id;
        refRec.Provider_Location_New__c = dummyProvLoc.Id;
        insert refRec;
    }
}