@isTest
private class UserTriggerTest_CS {
	
	/******* Provider *******/
	static private List<Account> providers;
	static private List<Contact> providerUsers;

	/******* Test Objects *******/
	static private Contact user;
	
	/******* Test Methods - First 3 methods for test coverage only: trigger dispatcher action only on update *******/
	
	static testMethod void testInsert(){
		init();
		insert user;
	}
	
	static testMethod void testDelete(){
		init();
		insert user;
		delete user;
	}
	
	static testMethod void testUndelete(){
		init();
		insert user;
		delete user;
		undelete user;
	}
	
	//Tests the after update trigger
	static testMethod void testUpdate(){
		init();
		user.Password__c = 'oldPassword';
		insert user;
		user.Password__c = 'newPassword';
		update user;
	}
	
	static void init(){
    	
    	///////////////////////////////////
		// Data
		///////////////////////////////////
    	
    	//Providers
		providers = TestDataFactory_CS.generateProviders('TestProvider', 1);
		insert providers;
		
		//Provider users, set user roles/profiles
		providerUsers = TestDataFactory_CS.generateProviderContacts('ProviderUser', providers, 1);
		user = providerUsers[0];
    }
}