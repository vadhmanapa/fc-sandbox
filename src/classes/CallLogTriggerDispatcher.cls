public with sharing class CallLogTriggerDispatcher {

	public static void beforeInsert(List<Call_Log__c> nList) {
		
	}
	
	public static void beforeUpdate(List<Call_Log__c> nList) {
		
	}

	public static void updateCallLogTimestamp(Call_Log__c log, Task logTask) {
		
		if (logTask.skyplatform__Call_Start_Date_Time__c != null) {
			system.debug(LoggingLevel.INFO,'Updating received time: ' + logTask.skyplatform__Call_Start_Date_Time__c);
			log.Call_Received__c = logTask.skyplatform__Call_Start_Date_Time__c;
		}
		
		if (logTask.skyplatform__Call_Start_Date_Time__c != null) {
			
			DateTime endTime = logTask.skyplatform__Call_Start_Date_Time__c.addSeconds(logTask.CallDurationInSeconds);
			
			system.debug(LoggingLevel.INFO,'Updating end time: ' + endTime);
			log.Call_Ended__c = endTime;
		}
	}

	public static Boolean checkNewTaskId(Call_Log__c oLog, Call_Log__c nLog) {
		
		if (oLog != null &&
			nLog.Call_Task_Id__c != null &&
			oLog.Call_Task_Id__c != nLog.Call_Task_Id__c) 
		{
			system.debug(LoggingLevel.INFO,'Call log ' + nLog.Name + 
				'\'s Task has been updated from ' + oLog.Call_Task_Id__c + 
				' to ' + nLog.Call_Task_Id__c);
			return true;
		}
		
		return false;
	}
	
	public static void updateCallLogTask(List<Call_Log__c> nList, Map<Id,Call_Log__c> oldMap) {
		
		Map<Id,Call_Log__c> taskIdToLog = new Map<Id,Call_Log__c>();
		
		for(Call_Log__c nLog : nList) {
			
			Call_Log__c oldLog = (oldMap.containsKey(nLog.Id) ? oldMap.get(nLog.Id) : null);
			
			if (checkNewTaskId(nLog,oldLog)) {
				taskIdToLog.put(nLog.Call_Task_Id__c,nLog);
			}
		}

		Map<Id,Task> taskMap = new Map<Id,Task>([
			SELECT 
				Id,
				skyplatform__Call_Start_Date_Time__c,
				CallDurationInSeconds
			FROM Task
			WHERE Id IN :taskIdToLog.keySet()
		]);
		
		for(Call_Log__c nLog : taskIdToLog.values()) {
			
			if(!taskMap.containsKey(nLog.Call_Task_Id__c)) {
				system.debug(LoggingLevel.WARN,'Cannot find Task ' + nLog.Call_Task_Id__c);
				continue;
			}
			
			Task logTask = taskMap.get(nLog.Call_Task_Id__c);
			
			updateCallLogTimestamp(nLog,logTask);
		}
	}
}