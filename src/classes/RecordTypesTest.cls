@isTest
private class RecordTypesTest {
	@isTest static void testDefaultRecordType(){
		Map<String,Schema.RecordTypeInfo> accountTypes = Schema.SObjectType.Account.getRecordTypeInfosByName();
		Id defaultId;
		Test.startTest();
		defaultId = RecordTypes.getDefaultRecordType(accountTypes);
		Test.stopTest();

		System.assertNotEquals(null, defaultId);
	}

	@isTest static void testAvailableRecordTypes(){
		Map<String,Schema.RecordTypeInfo> contactTypes = Schema.SObjectType.Contact.getRecordTypeInfosByName();
		Map<String,Schema.RecordTypeInfo> availableRectypes = new Map<String,Schema.RecordTypeInfo>();
		Map<Id,RecordType> testRecTypes = new Map<Id,RecordType>();
		Test.startTest();
		availableRectypes.putAll(RecordTypes.getAvailableRecordTypes(contactTypes));
		testRecTypes = RecordTypes.recordTypesById;
		Test.stopTest();

		System.assertNotEquals(0, availableRectypes.size());
		System.assertNotEquals(0, testRecTypes.size());
	}
}