public virtual without sharing class ProviderPortalServices_CS extends PortalServices_CS {

	public final String pageAccountType = 'Provider';

	public Boolean portalMember = false;

	public ProviderPortalServices_CS(){
		super();
		
		if(portalUser != null && portalUser.AccountType == pageAccountType){
			portalMember = true;
		}
	}

	/********* Page Interface *********/
	public String inboxCount {
		get{
			if(authenticated){
				ProviderOrderServices_CS orderService = new ProviderOrderServices_CS(portalUser.instance.Id);
				
				if(orderService != null){				
					return String.valueOf(orderService.returnAggregateOrdersByStage(OrderModel_CS.STAGE_PENDING_ACCEPTANCE));
				}
				else{
					System.debug('ProviderPortalServices: OrderService creation error.');
				}	 
			}
			else{
				System.debug('ProviderPortalServices: Authentication error.');
			}
			
			return '';
		}
		set;
	}

	/******* Page Content *******/

	public String providerURLPath
	{
		get
		{
			if(providerURLPath == null)
			{
				providerURLPath = GetResourceURL('ProviderPortalSupportFiles');
			}
			return('/ProviderPortal' + providerURLPath);
		}
		set;
	}

	public String pdfheader 
	{
		get
		{
			if(pdfheader == null)
			{
				pdfheader  = '<head>';
				pdfheader += '	<meta charset="utf-8">';
				pdfheader += '	<meta name="viewport" content="initial-scale = 1, user-scalable = no">';
				pdfheader += '	<title>Integra - Providers</title>';
				pdfheader += '';
				pdfheader += '	<!--Load Theme CSS -->';
				pdfheader += '	<link rel="stylesheet" href="http://cs-accessintegra.cs30.force.com' + ProviderURLPath + '/css/normalize.css">';
				pdfheader += '	<link rel="stylesheet" href="http://cs-accessintegra.cs30.force.com' + ProviderURLPath + '/css/foundation.css">';
				pdfheader += '	<link rel="stylesheet" href="http://cs-accessintegra.cs30.force.com' + ProviderURLPath + '/css/general_foundicons.css">';
				pdfheader += '	<link rel="stylesheet" href="http://cs-accessintegra.cs30.force.com' + ProviderURLPath + '/css/styles.css">';
				pdfheader += '	<link rel="stylesheet" href="http://cs-accessintegra.cs30.force.com' + ProviderURLPath + '/css/order.css">';
				pdfheader += '	<link rel="stylesheet" href="http://cs-accessintegra.cs30.force.com' + ProviderURLPath + '/css/summary-orders.css">';
				pdfheader += '	<link rel="stylesheet" href="http://cs-accessintegra.cs30.force.com' + ProviderURLPath + '/css/notification.css">';
				pdfheader += '	<link rel="stylesheet" href="http://cs-accessintegra.cs30.force.com' + ProviderURLPath + '/css/datepicker.css">';
				pdfheader += '	<link rel="stylesheet" href="http://cs-accessintegra.cs30.force.com' + ProviderURLPath + '/css/responsive-tables.css">';
				pdfheader += '';
				pdfheader += '';
				pdfheader += '	<!--Load JS Files -->';
				pdfheader += '	<script src="' + ProviderURLPath + '/js/modernizr.js"></script>';
				pdfheader += '	<script src="' + ProviderURLPath + '/js/chart.min.js"></script>';
				pdfheader += '	<!--[if lt IE 8]>';
				pdfheader += '	<link rel="stylesheet" href="' + ProviderURLPath + '/stylesheets/general_foundicons_ie7.css">';
				pdfheader += '	<script src="' + ProviderURLPath + '/js/excanvas.js"></script>';
				pdfheader += '	<![endif]-->';
				pdfheader += '';
				pdfheader += '</head>';
				pdfheader += '';
				pdfheader += '<body>';
				pdfheader += '<!--[if lt IE 8]>';
				pdfheader += ' <div id="ie">Hello, we see you\'re using an <strong>outdated version of Internet Explorer</strong>. This site will remain readable for you but you will be missing out on some more advanced features and layout that only more recent, standards-compliant, browsers like <a href="http://www.firefox.com/" title="Firefox web browser | Faster, more secure, &amp; customizable">Firefox</a>, <a href="http://www.apple.com/safari/download/" title="Apple - Safari - Download Safari - Download the world&#8217;s fastest and most innovative browser for Mac and PC">Safari</a>, <a href="http://www.google.com/chrome" title="Google Chrome - Een nieuwe browser downloaden">Google Chrome</a> or <a href="http://www.browserforthebetter.com/download.html" title="Internet Explorer 8 - Browser for the Better">Internet Explorer 9</a> support. You might also see the odd glitch here and there. If you can upgrade, we encourage you to do so, you\'ll like it.</div>';
				pdfheader += '<![endif]-->';

			}
			return(pdfheader);
		}
		
		set;
	}

	/*
	public String header 
	{
		get
		{
			if(header == null)
			{
				header  = '<head>';
				header += '	<meta charset="utf-8">';
				header += '	<meta name="viewport" content="initial-scale = 1, user-scalable = no">';
				header += '	<title>Integra - Providers</title>';
				header += '';
				header += '	<!--Load Theme CSS -->';
				header += '	<link rel="stylesheet" href="' + ProviderURLPath + '/css/normalize.css">';
				header += '	<link rel="stylesheet" href="' + ProviderURLPath + '/css/foundation.css">';
				header += '	<link rel="stylesheet" href="' + ProviderURLPath + '/css/general_foundicons.css">';
				header += '	<link rel="stylesheet" href="' + ProviderURLPath + '/css/styles.css">';
				header += '	<link rel="stylesheet" href="' + ProviderURLPath + '/css/order.css">';
				header += '	<link rel="stylesheet" href="' + ProviderURLPath + '/css/summary-orders.css">';
				header += '	<link rel="stylesheet" href="' + ProviderURLPath + '/css/notification.css">';
				header += '	<link rel="stylesheet" href="' + ProviderURLPath + '/css/datepicker.css">';
				header += '	<link rel="stylesheet" href="' + ProviderURLPath + '/css/responsive-tables.css">';
				header += '';
				header += '';
				header += '	<!--Load JS Files -->';
				header += '	<script src="' + ProviderURLPath + '/js/modernizr.js"></script>';
				header += '	<script src="' + ProviderURLPath + '/js/chart.min.js"></script>';
				header += '	<!--[if lt IE 8]>';
				header += '	<link rel="stylesheet" href="' + ProviderURLPath + '/stylesheets/general_foundicons_ie7.css">';
				header += '	<script src="' + ProviderURLPath + '/js/excanvas.js"></script>';
				header += '	<![endif]-->';
				header += '';
				header += '</head>';
				header += '';
				header += '<body>';
				header += '<!--[if lt IE 8]>';
				header += ' <div id="ie">Hello, we see you\'re using an <strong>outdated version of Internet Explorer</strong>. This site will remain readable for you but you will be missing out on some more advanced features and layout that only more recent, standards-compliant, browsers like <a href="http://www.firefox.com/" title="Firefox web browser | Faster, more secure, &amp; customizable">Firefox</a>, <a href="http://www.apple.com/safari/download/" title="Apple - Safari - Download Safari - Download the world&#8217;s fastest and most innovative browser for Mac and PC">Safari</a>, <a href="http://www.google.com/chrome" title="Google Chrome - Een nieuwe browser downloaden">Google Chrome</a> or <a href="http://www.browserforthebetter.com/download.html" title="Internet Explorer 8 - Browser for the Better">Internet Explorer 9</a> support. You might also see the odd glitch here and there. If you can upgrade, we encourage you to do so, you\'ll like it.</div>';
				header += '<![endif]-->';

			}
			return(header);
		}
		
		set;
	}

	

	public String nav
	{
		get
		{
			if(nav == null)
			{
				nav  = '<!--header-->';
				nav += '<div class="header">';
				nav += '	<div class="row">';
				nav += '		<div class="logo large-3 small-12 columns">';
				nav += '			<a href="providerhomedashboard_cs"><img src="' + ProviderURLPath + '/images/integra-logo.png" alt="Integra for Providers"></a>';
				nav += '		</div>';
				nav += '		<!--right navigation-->';
				nav += '		<nav class="large-9 small-12 columns" role="navigation">';
				nav += '		<ul class="menu">';
				nav += '			<li><a class="ico-1" href="ProviderInbox_CS">Inbox <span class="inbox">' + inboxCount + '</span></a></li>';
				nav += '			<li><a class="ico-2" href="ProviderOrdersSummary_CS">My Orders</a></li>';
				nav += '			<li><a class="ico-3" href="ProviderProfile_CS">My Profile</a></li>';
				nav += '			<li><a class="ico-4" href="ProviderSettings_CS">Settings</a></li>';
				nav += '			<li><a class="ico-5" href="ProviderLogout_CS">Logout</a></li>';
				nav += '		</ul>';
				nav += '		</nav>';
				nav += '	</div>';
				nav += '</div><!--end header-->';
			}
			return(nav);
		}
		set;
	}

	public String footer
	{
		get
		{
			if(footer == null)
			{
				footer  = '	<div class="site-footer">';
				footer += '	';
				footer += '		<div class="row">';
				footer += '			<div class="footer">';
				footer += '				<div class="copyright large-6 small-12 columns">';
				footer += '					<a href="#">Copyright 2013, Integra Partners LLC</a>';
				footer += '				</div>';
				footer += '';
				footer += '				<div class="large-6 small-12 columns">';
				footer += '					<ul>';
				footer += '						<li><a href="ProviderTermsConditions">Terms &amp; Conditions</a></li>';
				footer += '						<li><a href="ProviderPrivacyPolicy">Privacy Policy</a></li>';
				footer += '						<li><a href="mailto:clearhelp@accessintegra.com">Need Help?</a></li>';
				footer += '					</ul>';
				footer += '				</div>';
				footer += '			</div>';
				footer += '		</div>';
				footer += '';
				footer += '		<!--For Development Only-->';
				footer += '		<?php function curPageName() { return substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1);} ?>';
				footer += '		<p class="text-secondary text-center"><?php echo "Page ID = ".curPageName();?></p>';
				footer += '	</div><!--site-footer-->';
				footer += '';
				footer += '';
				footer += '	<script>';
				footer += '	$(document).foundation();';
				footer += '	</script>';
				footer += '</body>';
				footer += '</html>';
			}
			return(footer);
		}
		set;
	}
	*/
}