/**
 *  @Description 
 *  @Author Cloud Software LLC
 *  Revision History: 
 *		12/23/2015 - karnold - Added header, removed references to DME_Category__c.
 *
 */
public class DMEUtils_CS {

	public static final String errorMsg_DMECodeExists = 'This DME Product Code already exists, please revise or update existing DME Product.';

	public static DME_Settings__c dmeSettings = DME_Settings__c.getInstance('Default');
	public static List<DME__c> databaseDMEList;

	/* Check First Run */

	public static boolean firstRun = true;

	public static boolean isFirstRun() {
		return firstRun;
	}
	public static void setFirstRunFalse() {
		firstRun = false;
	}

	public static boolean beforeUpdateFirstRun = true;

	public static boolean isBeforeUpdateFirstRun() {
		return beforeUpdateFirstRun;
	}

	public static void setBeforeUpdateFirstRunFalse() {
		beforeUpdateFirstRun = false;
	}

	//Preventing duplicated DMECode entries
	public static void validateDMECode(List<DME__c> dmeList) {

		// Get existing DME's
		databaseDMEList = [Select Name From DME__c];

		for (Integer i = 0; i < dmeList.size(); i++) {

			//Validate against dme's in the list
			for (Integer j = i + 1; j < dmeList.size(); j++) {
				if (dmeList[i].Name == dmeList[j].Name) {
					dmeList[i].Name.addError(errorMsg_DMECodeExists);
					break;
				}
			}

			//Validate against existing dme's
			for (DME__c existingDME : databaseDMEList) {
				if (dmeList[i].Id != existingDME.Id && dmeList[i].Name == existingDME.Name) {
					dmeList[i].Name.addError(errorMsg_DMECodeExists);
					break;
				}
			}
		}
	}

	public static void BeforeInsertProcessor(List<DME__c> dmeList) {
		for (DME__c dme : dmeList) {
			setDefaultMaxDeliveryTime(dme);
		}
	}

	private static void setDefaultMaxDeliveryTime(DME__c dme) {

		Boolean categoryFound = false;
		system.debug('dme: ' + dme);
		system.debug('setting: ' + dmeSettings);

		if (dme.Max_Delivery_Time__c == null) { //Set default max delivery time
			dme.Max_Delivery_Time__c = dmeSettings.Max_Delivery_Time__c;
		}

	}

}