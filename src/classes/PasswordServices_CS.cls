public without sharing class PasswordServices_CS {

	/* Constants */
	private static final Integer MINIMUM_SALT_LENGTH = 12;
	private static final Integer MINIMUM_PASSWORD_LENGTH = 8;

	private static final String passwordCriteria = '^(?=.*[A-Z]+)(?=.*[!@#$&*])(?=.*[0-9])(?!.*(.)\\1).{' + String.valueOf(MINIMUM_PASSWORD_LENGTH) + ',}$';
	private static final String passwordCharacters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$&*';

	public static String hash(String input, String salt){
		return EncodingUtil.base64encode(Crypto.generateDigest('SHA-512', Blob.valueOf(input + salt)));
	}	
	
	public static Boolean validatePasswordRegex(String password){
		Pattern p = Pattern.compile(passwordCriteria);
		
		if(password != null){
			Matcher m = p.matcher(password);
			
			if(m.matches()){
				return true;
			}
			else{
				return false;
			}
		}
		else{
			return false;
		}
		
	}
	
	public static String randomPassword(Integer length){

		String newPassword;
		Integer generatorCount = 0;
		
		if(length < MINIMUM_PASSWORD_LENGTH){
			length = MINIMUM_PASSWORD_LENGTH;
		}
		
		do{
			newPassword = '';
			
			for(Integer i = 0; i < length; i++){
				Integer idx = Integer.valueOf(math.roundToLong(math.random()*(passwordCharacters.length()-1)));
				newPassword += passwordCharacters.subString(idx,idx+1);
			}
		
			generatorCount++;
			
		}while(!validatePasswordRegex(newPassword));
		
		System.debug('randomPassword(): Took ' + generatorCount + ' number of loops for new password.');
		
		return newPassword;
	}
	
	public static String randomSalt(Integer length){
		
		String newSalt;
		Integer generatorCount = 0;
		
		if(length < MINIMUM_SALT_LENGTH){
			length = MINIMUM_SALT_LENGTH;
		}
		
		do{
			newSalt = '';
			
			for(Integer i = 0; i < length; i++){
				Integer idx = Integer.valueOf(math.roundToLong(math.random()*(passwordCharacters.length()-1)));
				newSalt += passwordCharacters.subString(idx,idx+1);
			}
		
			generatorCount++;
			
		}while(!validatePasswordRegex(newSalt));
		
		System.debug('randomSalt(): Took ' + generatorCount + ' number of loops for new salt.');
		
		return newSalt;	
	}
	
}