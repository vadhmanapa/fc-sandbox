/**
 *  @Description 
 *  @Author Cloud Software LLC
 *  Revision History: 
 *      12/18/2015 - karnold - ISSCI-167 - Added try catch to handle errors from DML statements.
 *		02/16/2016 - karnold - Formatted Code.
 *
 */
public without sharing class PayerCreateNewPatient_CS extends PayerPortalServices_CS {

	public Plan_Patient__c newPatient { get; set; }
	public List<SelectOption> stateSelectOptions { get; set; } //140221 LR
	private final String pageAccountType = 'Payer';

	public PayerCreateNewPatient_CS() {
		newPatient = new Plan_Patient__c();
	}

	public PageReference init() {

		///////////////////////////////////
		// Authentication and Permission
		///////////////////////////////////

		if (!isSecure && !Test.isRunningTest()) {
			return DoLogout();
		}

		if (!authenticated) {
			return DoLogout();
		}

		if (portalUser.AccountType != pageAccountType) {
			return homepage;
		}

		///////////////////////////////////
		// Additional init
		///////////////////////////////////
		populateStatePicklistOptions();
		return null;
	}
	//140221 LR
	public void populateStatePicklistOptions() {
		stateSelectOptions = new List<SelectOption> ();
		stateSelectOptions.add(new SelectOption('AL', 'Alabama'));
		stateSelectOptions.add(new SelectOption('AK', 'Alaska'));
		stateSelectOptions.add(new SelectOption('AZ', 'Arizona'));
		stateSelectOptions.add(new SelectOption('AR', 'Arkansas'));
		stateSelectOptions.add(new SelectOption('CA', 'California'));
		stateSelectOptions.add(new SelectOption('CO', 'Colorado'));
		stateSelectOptions.add(new SelectOption('CT', 'Connecticut'));
		stateSelectOptions.add(new SelectOption('DE', 'Delaware'));
		stateSelectOptions.add(new SelectOption('FL', 'Florida'));
		stateSelectOptions.add(new SelectOption('GA', 'Georgia'));
		stateSelectOptions.add(new SelectOption('HI', 'Hawaii'));
		stateSelectOptions.add(new SelectOption('ID', 'Idaho'));
		stateSelectOptions.add(new SelectOption('IL', 'Illinois'));
		stateSelectOptions.add(new SelectOption('IN', 'Indiana'));
		stateSelectOptions.add(new SelectOption('IA', 'Iowa'));
		stateSelectOptions.add(new SelectOption('KS', 'Kansas'));
		stateSelectOptions.add(new SelectOption('KY', 'Kentucky'));
		stateSelectOptions.add(new SelectOption('LA', 'Louisiana'));
		stateSelectOptions.add(new SelectOption('ME', 'Maine'));
		stateSelectOptions.add(new SelectOption('MD', 'Maryland'));
		stateSelectOptions.add(new SelectOption('MA', 'Massachusetts'));
		stateSelectOptions.add(new SelectOption('MI', 'Michigan'));
		stateSelectOptions.add(new SelectOption('MN', 'Minnesota'));
		stateSelectOptions.add(new SelectOption('MS', 'Mississippi'));
		stateSelectOptions.add(new SelectOption('MO', 'Missouri'));
		stateSelectOptions.add(new SelectOption('MT', 'Montana'));
		stateSelectOptions.add(new SelectOption('NE', 'Nebraska'));
		stateSelectOptions.add(new SelectOption('NV', 'Nevada'));
		stateSelectOptions.add(new SelectOption('NH', 'New Hampshire'));
		stateSelectOptions.add(new SelectOption('NJ', 'New Jersey'));
		stateSelectOptions.add(new SelectOption('NM', 'New Mexico'));
		stateSelectOptions.add(new SelectOption('NY', 'New York'));
		stateSelectOptions.add(new SelectOption('NC', 'North Carolina'));
		stateSelectOptions.add(new SelectOption('ND', 'North Dakota'));
		stateSelectOptions.add(new SelectOption('OH', 'Ohio'));
		stateSelectOptions.add(new SelectOption('OK', 'Oklahoma'));
		stateSelectOptions.add(new SelectOption('OR', 'Oregon'));
		stateSelectOptions.add(new SelectOption('PA', 'Pennsylvania'));
		stateSelectOptions.add(new SelectOption('RI', 'Rhode Island'));
		stateSelectOptions.add(new SelectOption('SC', 'South Carolina'));
		stateSelectOptions.add(new SelectOption('SD', 'South Dakota'));
		stateSelectOptions.add(new SelectOption('TN', 'Tennessee'));
		stateSelectOptions.add(new SelectOption('TX', 'Texas'));
		stateSelectOptions.add(new SelectOption('UT', 'Utah'));
		stateSelectOptions.add(new SelectOption('VT', 'Vermont'));
		stateSelectOptions.add(new SelectOption('VA', 'Virginia'));
		stateSelectOptions.add(new SelectOption('WA', 'Washington'));
		stateSelectOptions.add(new SelectOption('WV', 'West Virginia'));
		stateSelectOptions.add(new SelectOption('WI', 'Wisconsin'));
		stateSelectOptions.add(new SelectOption('WY', 'Wyoming'));
	}
	public PageReference CreateRecord() {

		newPatient.Health_Plan__c = portalUser.instance.Entity__c;
		newPatient.Name = newPatient.First_Name__c + ' ' + newPatient.Last_Name__c;

		System.debug('New Patient: ' + newPatient);

		// Added code to catch database errors and display to user.
		Database.SaveResult result = Database.insert(newPatient, false);
		if (!result.isSuccess()) {
			System.debug('Error:');

			for (Database.Error e : result.getErrors()) {
				System.debug(e.getMessage());
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
			}
		} else {
			PageReference pr = Page.PayerPatientSummary_CS;
			pr.getParameters().put('pid', newPatient.Id);
			pr.setRedirect(true);

			return pr;
		}

		return null;
	}


}