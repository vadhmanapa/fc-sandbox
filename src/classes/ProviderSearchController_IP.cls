public with sharing class ProviderSearchController_IP {
	
	public String stateSelected{get;set;}
	public Boolean includeMed {get; set;}
	//public String toSelect{get; set;}// pass value to the command page
	// will be used for county search
	public Set<Id> providersForThisState = new Set<Id>();
	public Map<Id, Provider_Locations__c> pLocMap {get;set;}
	public Provider_Locations__c[] providerList {get; set;}
	public Set<String> stateNames = new Set<String>();
	public Map<String,String> stateCountyList = new Map<String,String>();
	public List<String> countyNames = new List<String>();
	public Integer countResult {get; set;}
	public String searchString{get; set;}
	public String searchProduct{get; set;}
	public Set<Id> idFromMap = new Set<Id>();

	public ProviderSearchController_IP() {
		
		// load up both the picklist values
		
		/*for(US_State_County__c u : [Select Id, Name, State__c, 	State_Code__c from US_State_County__c]){

			if(u.State__c != null){
				stateCountyList.put(u.State__c, u.State_Code__c );
				stateNames.add(u.State__c);
			}
		}*/

		stateCountyList = new Map<String, String>{'Alabama'=>'ALABAMA',
											'Alaska'=>'ALASKA',
											'Arizona'=>'ARIZONA',
											'Arkansas'=>'ARKANSAS',
											'California'=>'CALIFORNIA',
											'Colorado'=>'COLORADO',
											'Connecticut'=>'CONNECTICUT',
											'Delaware'=>'DELAWARE',
											'District of Columbia'=>'DISTRICT OF COLUMBIA',
											'Florida'=>'FLORIDA',
											'Georgia'=>'GEORGIA',
											'Hawaii'=>'HAWAII',
											'Idaho'=>'IDAHO',
											'Illinois'=>'ILLINOIS',
											'Indiana'=>'INDIANA',
											'Iowa'=>'IOWA',
											'Kansas'=>'KANSAS',
											'Kentucky'=>'KENTUCKY',
											'Louisiana'=>'LOUISIANA',
											'Maine'=>'MAINE',
											'Maryland'=>'MARYLAND',
											'Massachusetts'=>'MASSACHUSETTS',
											'Michigan'=>'MICHIGAN',
											'Minnesota'=>'MINNESOTA',
											'Mississippi'=>'MISSISSIPPI',
											'Missouri'=>'MISSOURI',
											'Montana'=>'MONTANA',
											'Nebraska'=>'NEBRASKA',
											'Nevada'=>'NEVADA',
											'New Hampshire'=>'NEW HAMPSHIRE',
											'New Jersey'=>'NEW JERSEY',
											'New Mexico'=>'NEW MEXICO',
											'New York'=>'NEW YORK',
											'North Carolina'=>'NORTH CAROLINA',
											'North Dakota'=>'NORTH DAKOTA',
											'Ohio'=>'OHIO',
											'Oklahoma'=>'OKLAHOMA',
											'Oregon'=>'OREGON',
											'Pennsylvania'=>'PENNSYLVANIA',
											'Rhode Island'=>'RHODE ISLAND',
											'South Carolina'=>'SOUTH CAROLINA',
											'South Dakota'=>'SOUTH DAKOTA',
											'Tennessee'=>'TENNESSEE',
											'Texas'=>'TEXAS',
											'Utah'=>'UTAH',
											'Vermont'=>'VERMONT',
											'Virginia'=>'VIRGINIA',
											'Washington'=>'WASHINGTON',
											'West Virginia'=>'WEST VIRGINIA',
											'Wisconsin'=>'WISCONSIN',
											'Wyoming'=>'WYOMING'
											};

		// set values on page load for first run

		stateSelected = 'New York'; // most searched
		includeMed = false; //******** make it default to true for now
		searchString = null;
	}

	public List<SelectOption> getStateNames(){

		List<SelectOption> states = new List<SelectOption>();
		for(String s : stateCountyList.keySet() ){

			states.add(new SelectOption(s, stateCountyList.get(s)));
		}

		return states;
	}

	// this runs the first query when state is changed

	public Set<Id> runCountyServedSearch(String selectedState, String searchedCounty){

		System.debug('*****State Selected is********'+selectedState);
		// Variables declaration
		String getLocationsFromCountiesServed = 'Select Id, Provider_Location__c, State__c, County_Name__c FROM County_Item__c'; // general query
		County_Item__c[] collectCountiesForStateSelected = new County_Item__c[]{};// for D-SOQL
		Set<Id> getProvLoc = new Set<Id>();
		getLocationsFromCountiesServed+= ' WHERE State__c=: selectedState';// adding the filter

			if(!String.isEmpty(searchedCounty)) {
				// if searchstring is present, add county name filter
				getLocationsFromCountiesServed+= ' AND (County_Name__c LIKE \'%' + searchedCounty + '%\')';
			}

			System.debug('************ Final Query formed is**************'+getLocationsFromCountiesServed);
			collectCountiesForStateSelected = Database.query(getLocationsFromCountiesServed);

			if(collectCountiesForStateSelected.size() > 0) {
				for(County_Item__c c: collectCountiesForStateSelected) {
					getProvLoc.add(c.Provider_Location__c);
				}

			} else {
				getProvLoc = new Set<Id>();
				//return null;
			}	

		return getProvLoc;

	}


	// run this for product level search

	public Map<Id, Provider_Locations__c> productSearch(String prodSearched, Map<Id, Provider_Locations__c>filterIds){

		// do a SOSL search
		System.debug('*****Product Searched is********'+prodSearched);

		Provider_Locations__c[] soslIds = new Provider_Locations__c[]{};
		Map<Id, Provider_Locations__c> filteredLocForProduct = new Map<Id, Provider_Locations__c>();
		idFromMap.clear();
		idFromMap.addAll(filterIds.keySet());

		String soslSearch = 'FIND'; // ' '+ '\'%' + searchProduct + '%\''+' ';
		soslSearch+= ' '+ '\'{' + searchProduct + '*}\''+' ';
        soslSearch+= 'IN ALL FIELDS RETURNING Provider_Locations__c(Id, Name_DBA__c,Store_Status__c, Adult_Orthotic_and_Prosthetic_Services__c,Pediatric_Orthotic_Prosthetic_Services__c, Durable_Medical_Equipment_Services__c, Languages__c, Location_Phone__c WHERE Id IN: idFromMap)';
		
		System.debug('***************Final form of SOSL:'+soslSearch);
				
		List<List<sObject>> soslResult = Search.query(soslSearch);
		soslIds = ((List<Provider_Locations__c>)soslResult[0]);

		if(soslIds.size() > 0) {
			for(Provider_Locations__c pLoc: soslIds) {
				filteredLocForProduct.put(pLoc.Id, pLoc);
			}
		}

		if(!filteredLocForProduct.isEmpty()) {
			
			for(Id p: filterIds.keySet()) {
				if(!filteredLocForProduct.containsKey(p)){
					filterIds.remove(p);
				}
			}
		} else {

			filterIds = new Map<Id,Provider_Locations__c>();
		}

		return filterIds; // final map
	}

	//run this for medicaid search

	public Map<Id, Provider_Locations__c> medicaidSearch(String stateSearched, Map<Id, Provider_Locations__c>filterIds){

		System.debug('*****Searching Medicaid Only Providers in:********');
		State_Identifier__c[] medSearch = new State_Identifier__c[]{};
		Map<Id, State_Identifier__c> filteredLocForMedicaid = new Map<Id, State_Identifier__c>();
		 idFromMap.clear();
		 idFromMap.addAll(filterIds.keySet());

		medSearch = Database.query('Select Id, Medicaid__c, Provider_Location__c, State__c from State_Identifier__c where Provider_Location__c IN:idFromMap AND Medicaid__c != null');
		if(medSearch.size() > 0) {
			for(State_Identifier__c si: medSearch) {
				if((si.State__c != null) && (si.State__c).containsIgnoreCase(stateSearched)){

					filteredLocForMedicaid.put(si.Provider_Location__c, si);
				}
			}
		} 

		if(!filteredLocForMedicaid.isEmpty()) {
			for(Id ip: filterIds.keySet()) {
				if(!filteredLocForMedicaid.containsKey(ip)) {
					filterIds.remove(ip);
				}
			}
		} else {
			
			filterIds = new Map<Id, Provider_Locations__c>();
		}
		
		return filterIds;
	}

	public void availableProviders(){

		//providersForThisState.clear();
		List <Provider_Locations__c> pLoc = new List <Provider_Locations__c>();
		Set<Id> pLocId = new Set<Id>();
		pLocMap = new Map<Id, Provider_Locations__c>();
		Map<Id, Provider_Locations__c> clonepLocMap = new Map<Id, Provider_Locations__c>();
		//Set<Id> providersForThisStateCounty = new Set<Id>();
		//County_Item__c[] countyHolder = new County_Item__c[]{};
		//String cString;
		String plSearch;
		String actString = '%'+'Active'+'%';

						// Search for provider - medicaid included

		/********** Changing Algorithm to have Provider Location visibility**********/
		// Step1 - Search for Provider Location based on stateSelected
		//get the filtered Ids from runCountyServedSearch()

		pLocId.clear();
		System.debug('*******Entering County Served Search************');

		pLocId.addAll(runCountyServedSearch(stateSelected,searchString)); // includes county search also
		
		
		if(!pLocId.isEmpty()) {
			
			// first query all the attached provider location tied to counties served.
			plSearch = 'Select Id, Name_DBA__c,Store_Status__c, Adult_Orthotic_and_Prosthetic_Services__c,Pediatric_Orthotic_Prosthetic_Services__c, Durable_Medical_Equipment_Services__c, Languages__c, Location_Phone__c FROM Provider_Locations__c WHERE ';
			plSearch+= 'Id =:pLocId AND ';
			plSearch+= 'Account__r.Status__c = \'Active\' AND Store_Status__c = \'Active\' AND Name_DBA__c!= null'; 
			//plSearch+= 'Location_State__c =:stateSelected ORDER BY Name_DBA__c';
			pLoc = Database.query(plSearch);
		} /*else {
			
			pLoc = null;
		}*/

		if(pLoc.size() > 0 ){
			
			pLocMap.putAll(pLoc);
			System.debug('Size of pLocMap'+pLocMap.size());

			// product search
				if(!String.isEmpty(searchProduct)) {
					//pLocId.clear();
					clonepLocMap.clear();
					clonepLocMap = pLocMap.clone();
					pLocMap.clear();
						
					pLocMap.putAll(productSearch(searchProduct, clonepLocMap));
					
					
				}

				//medicaid search
				if(includeMed) {
					clonepLocMap.clear();
					clonepLocMap = pLocMap.clone();
					pLocMap.clear();

					pLocMap.putAll(medicaidSearch(stateSelected,clonepLocMap));
					
				}

				//final map

				providerList = new Provider_Locations__c[]{};

				    if(!pLocMap.isEmpty()) {
				    	
				    	System.debug('Entering final mapping into provider list. Size of pLocMap'+pLocMap.size());

				    	providerList = pLocMap.values();

				    	countResult = (Integer) providerList.size();

				    } else {

				    	System.debug('*****Search result gave zero results*****');
				    	providerList = null;
				    }
		} else {
			System.debug('******** No providers found for this state*********');
			providerList = null;
		}

		
		
		
		
		//System.debug('Number of items queried'+pLoc.size());
		/*for(Provider_Locations__c pl: pLoc) {
			pLocMap.put(pl.Id, pl);
		}*/


		// Step 2 ---> Include search filters

		/*if(!pLocMap.isEmpty()) {

			//Step 2a ---> Search for County
			Set<id> collectUnmatched = new Set<Id>();
			Map<Id, County_Item__c> countyHolderMap = new Map<Id, County_Item__c>();
			pLocId.addAll(pLocMap.keySet());
			if(!String.isBlank(searchString)) {
				
				cString = 'Select Id, Provider_Location__c, US_State_County__c, County_Name__c, State__c FROM County_Item__c WHERE Provider_Location__c =:pLocId AND (County_Name__c LIKE \'%' + searchString + '%\')';
				System.debug('The query formed is'+cString);
				countyHolder = Database.query(cString);
				for(County_Item__c ci: countyHolder) {
					countyHolderMap.put(ci.Provider_Location__c, ci);
				}
                //Loop through the values and remove ones that do not match

                if(countyHolderMap.size() > 0){

                	System.debug('Entering matching and removal of couties from map');
                	System.debug('Map size before deletion'+pLocMap.size());

                	for(Id i: pLocMap.keySet()) {
                		
                		if(!countyHolderMap.containsKey(i)) {
                			pLocMap.remove(i);
                		}
                	}
                	
                	System.debug('Map after deletion'+pLocMap.size());

                } else {
                	
                	System.debug('*****Search result for county search gave zero results*****');
                	pLocMap.clear();
                }

                System.debug('*****Exiting county search. Size of pLocMap is*****'+pLocMap.size());
			}*/

			/*if(!String.isBlank(searchProduct)) {
				
				System.debug('Search Product is'+searchProduct);

				pLocId.clear();
				// search pLocMap for the keyword
				String soslSearch = '';
				Provider_Locations__c[] soslIds = new Provider_Locations__c[]{};
				pLocId.addAll(pLocMap.keySet());
				System.debug('Size of providerForThisStateCounty'+pLocId.size());

				soslSearch = 'FIND';
								// ' '+ '\'%' + searchProduct + '%\''+' ';
				soslSearch+= ' '+ '\'{' + searchProduct + '*}\''+' ';

				soslSearch+= 'IN ALL FIELDS RETURNING Provider_Locations__c(Id, Name_DBA__c,Store_Status__c, Adult_Orthotic_and_Prosthetic_Services__c,Pediatric_Orthotic_Prosthetic_Services__c, Durable_Medical_Equipment_Services__c, Languages__c, Location_Phone__c WHERE Id IN: pLocId)';
				System.debug('Final form of SOSL:'+soslSearch);
				
				List<List<sObject>> soslResult = Search.query(soslSearch);
				soslIds = ((List<Provider_Locations__c>)soslResult[0]);

				System.debug('Size of SOSL'+soslIds.size());

				if(!soslIds.isEmpty()){

					System.debug('Entering matching and removal of products from map');
					
					pLocMap.clear();
					pLocMap.putAll(soslIds); // delete the previous values and add sosl search results. Filters have already been applied before search. So would only return exact result

					// now remove

					System.debug('Map after deletion'+pLocMap.size());

			    } else {
			    	
			    	System.debug('*****Search result for product search gave zero results*****');
			    	// pLocMap.clear();

			    }
			
			System.debug('*****Exiting product search. Size of pLocMap is*****'+pLocMap.size());

		   }*/

		   // Step 2c ----> If Medicaid is included

		   /*if(includeMed) {

		   	pLocId.clear();
		   	pLocId.addAll(pLocMap.keySet());
		   	System.debug('Size of pLocId'+pLocId.size());
		   	
		   	String stateName = stateCountyList.get(stateSelected);

		   	stateName.toLowerCase();
		   	System.debug('The state full Name is'+stateName);
		   	State_Identifier__c[] medSearch = new State_Identifier__c[]{};
		   	
		   	medSearch = Database.query('Select Id, Medicaid__c, Provider_Location__c, State__c from State_Identifier__c where Provider_Location__c =:pLocId AND Medicaid__c != null');
		   	
		   		if(!medSearch.isEmpty()) {

		   			Map<Id, State_Identifier__c> medSearchMap = new Map<Id, State_Identifier__c>();
		   			String copyState = '';
		   			for(State_Identifier__c s: medSearch) {
		   				
		   				copyState = s.State__c;
		   				System.debug('State text'+copyState);

		   				if(copyState.containsIgnoreCase(stateName)) {
		   					
		   					System.debug('Contains state'+s.Provider_Location__c);
		   					medSearchMap.put(s.Provider_Location__c, s);
		   				}
		   			}

		   			for(Id pId: pLocMap.keySet()) {
		   				if(!medSearchMap.containsKey(pId)) {
		   					pLocMap.remove(pId);
		   				}
		   			}
		   		} else {
		   			System.debug('No Medicaid for selected state');
		   			pLocMap.clear();
		   		}
		   		
		    }*/

		   /* providerList = new Provider_Locations__c[]{};

		    if(!pLocMap.isEmpty()) {
		    	
		    	System.debug('Entering final mapping into provider list. Size of pLocMap'+pLocMap.size());

		    	providerList = pLocMap.values();

		    	countResult = (Integer) providerList.size();

		    } else {

		    	System.debug('*****Search result gave zero results*****');
		    	providerList = null;
		    }
		    
		} else {

			System.debug('*****Search result gave zero results*****');
			providerList = null;
		}*/
    }

}