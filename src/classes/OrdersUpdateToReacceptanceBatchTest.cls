@isTest
private class OrdersUpdateToReacceptanceBatchTest {
	private static void initTestOrders(){
        
        List<Order__c> testOrders = new List<Order__c>();
        
        for(Integer i = 0;i < 10; i++){
            Order__c o = new Order__c();
            o.Status__c = 'Future DOS';
            o.Authorization_Number__c = 'Test'+i+(Integer) Math.random();
            o.Estimated_Delivery_Time__c = System.now().addDays(3);
            testOrders.add(o);
        }
        for(Integer i = 0;i < 10; i++){
            Order__c o = new Order__c();
            o.Status__c = 'Future DOS';
            o.Authorization_Number__c = 'Test'+i+(Integer) Math.random();
            o.Estimated_Delivery_Time__c = System.now().addDays(4);
            testOrders.add(o);
        }
        for(Integer i = 0;i < 10; i++){
            Order__c o = new Order__c();
            o.Status__c = 'Future DOS';
            o.Authorization_Number__c = 'Test'+i+(Integer) Math.random();
            o.Estimated_Delivery_Time__c = System.now().addDays(5);
            testOrders.add(o);
        }
        
        insert testOrders;
    }
    
    /*static testMethod void testOrderUpdateToPending(){
        String dayOfWeek = System.now().format('EEEE');
        
        initTestOrders();
        OrdersUpdateToReacceptanceBatch ofp = new OrdersUpdateToReacceptanceBatch();
        
        Test.startTest();
		Database.executeBatch(ofp);
		Test.stopTest();
        
        List<Order__c> ordersUpdated = [SELECT Id FROM Order__c WHERE Status__c = 'Pending Re-acceptance'];
        
        if(dayOfWeek == 'Saturday' || dayOfWeek == 'Sunday'){
            System.assertEquals(0, ordersUpdated.size()); // the batch should not run on Saturdays and Sundays
        }else{
            System.assertNotEquals(0, ordersUpdated.size());
        }
    }*/
    
    @isTest
    static void testScheduleWithSomeFutureDosOrders() {
        initTestOrders();
        Test.startTest();
        String cronExp = '0 0 4 ? * FRI';
        System.schedule('Test Reacceptance Job', cronExp, new OrdersUpdateToReacceptSchedule());
        Test.stopTest();
    }

    @isTest
    static void testScheduleWithNoOrders() {
        Test.startTest();
        String cronExp = '0 0 4 ? * SUN';
        System.schedule('Test Reacceptance Job', cronExp, new OrdersUpdateToReacceptSchedule());
        Test.stopTest();
    }
}