@isTest
public class GeocodingUtils_CS_Test {
    public static Decimal lat1 = 1;
    public static Decimal lng1 = -1;
    public static String street1 = '111 S Test Street';
    
    public static Decimal lat2 = 40;
    public static Decimal lng2 = -40;
    public static String street2 = '789 N Abc Rd';
    //Geocoding test data (CaSe SeNsItIvE!): 
    //1) ADDRESS Street : 111 S Test Street, City : Test, State : TN, Zip : 22222
    //1) RETURNS Lng : -1, Lat : 1
    
    //2) ADDRESS Street : 789 N Abc Rd, State : MN
    //2) RETURNS Lng : -40, Lat: 40
    
    static testMethod void GeocodingUtilsTest_geocodeAddresses() {
        //TODO: Test overall geocodeAddress functionality by passing geocoding models, assert that the geoModel.lat and geoModel.lng fields match the appropriate coordinates        
        List<GeocodingModel_CS> testGeoModels = new List<GeocodingModel_CS>();
        testGeoModels.add(initGeocodingModel1());
        testGeoModels.add(initGeocodingModel2());
        
        GeocodingUtils_CS.geocodeAddresses(testGeoModels);
        
        for(GeocodingModel_CS geoModel : testGeoModels) {
            if(geoModel.Street == street1) {
                system.assertEquals(lat1, geoModel.lat);
                system.assertEquals(lng1, geoModel.lng);
            }else if(geoModel.Street == street2) {
                system.assertEquals(lat2, geoModel.lat);
                system.assertEquals(lng2, geoModel.lng);
            }
        }
    }
    
    static testMethod void GeocodingUtilsTest_makeGeocodeRequest() {
        //TODO: Call method, assert that the returned string = GeocodingUtils.getSampleGeocodeJSONString()
        List<String> testList = new List<String>();
        String res = GeocodingUtils_CS.makeGeocodeRequest(testList);
        
        system.assert(res.startsWith('{"results":'));
        system.assert(res == GeocodingUtils_CS.getSampleGeocodeJSONString());
    }
    
    static testMethod void GeocodingUtilsTest_parseGeocodeResults() {
        //TODO: Pass GeocodingUtils.getSampleGeocodeJSONString, assert the values of returned GeocodingResult instances, ProvidedLocation instances, Location instances, and LatLng instances                
        String parseMe = GeocodingUtils_CS.getSampleGeocodeJSONString();
        
        List<GeocodingUtils_CS.GeocodingResult> resList = GeocodingUtils_CS.parseGeocodeResults(parseMe);
        system.assertEquals(2, resList.size());
        
        //Creates 2 geocoding results with the intended test data to check parsing is done correctly
        GeocodingUtils_CS.ProvidedLocation provLoc1 = new GeocodingUtils_CS.ProvidedLocation('111 S Test Street, Test, TN 22222');
        GeocodingUtils_CS.ProvidedLocation provLoc2 = new GeocodingUtils_CS.ProvidedLocation('789 N Abc Rd, MN');
        GeocodingUtils_CS.LatLng latLng1 = new GeocodingUtils_CS.LatLng(-1, 1);
        GeocodingUtils_CS.LatLng latLng2 = new GeocodingUtils_CS.LatLng(-40, 40);
        GeocodingUtils_CS.Location location1 = new GeocodingUtils_CS.Location(latLng1, 'COUNTRY', 'A1XAX');
        GeocodingUtils_CS.Location location2 = new GeocodingUtils_CS.Location(latLng2, 'STATE', 'A3XAX');
        List<GeocodingUtils_CS.Location> locations1 = new List<GeocodingUtils_CS.Location>();
        locations1.add(location1);
        List<GeocodingUtils_CS.Location> locations2 = new List<GeocodingUtils_CS.Location>();
        locations2.add(location2);
        GeocodingUtils_CS.GeocodingResult geoResult1 = new GeocodingUtils_CS.GeocodingResult(provLoc1,locations1);
        GeocodingUtils_CS.GeocodingResult geoResult2 = new GeocodingUtils_CS.GeocodingResult(provLoc2,locations2);
		
        system.assertEquals(resList[0].providedLocation.location, geoResult1.providedLocation.location);
        system.assertEquals(resList[0].Locations[0].latLng.lng, geoResult1.Locations[0].latLng.lng);
        system.assertEquals(resList[0].Locations[0].latLng.lat, geoResult1.Locations[0].latLng.lat);
        system.assertEquals(resList[0].Locations[0].geocodeQuality, geoResult1.Locations[0].geocodeQuality);
        system.assertEquals(resList[0].Locations[0].geocodeQualityCode, geoResult1.Locations[0].geocodeQualityCode);
        
		system.assertEquals(resList[1].providedLocation.location, geoResult2.providedLocation.location);
        system.assertEquals(resList[1].Locations[0].latLng.lng, geoResult2.Locations[0].latLng.lng);
        system.assertEquals(resList[1].Locations[0].latLng.lat, geoResult2.Locations[0].latLng.lat);
        system.assertEquals(resList[1].Locations[0].geocodeQuality, geoResult2.Locations[0].geocodeQuality);
        system.assertEquals(resList[1].Locations[0].geocodeQualityCode, geoResult2.Locations[0].geocodeQualityCode);
        
        system.debug('Locations: ' + resList[0].locations);
    }
    
    //TODO: test distance functionality (After the assignment process has been built out)
    
    static GeocodingModel_CS initGeocodingModel1() {
        GeocodingModel_CS geoModel = new GeocodingModel_CS();
        geoModel.RecordId = '1';
        geoModel.street = '111 S Test Street';
        geoModel.city = 'Test';
        geoModel.state = 'TN';
        geoModel.postalCode = '22222';
        return geoModel;
    }
    static GeocodingModel_CS initGeocodingModel2() {
        GeocodingModel_CS geoModel = new GeocodingModel_CS();
        geoModel.RecordId = '2';
        geoModel.street = '789 N Abc Rd';
        geoModel.state = 'MN';
        return geoModel;
    }
}