public without sharing class PayerUsersSummary_CS extends PayerPortalServices_CS {

	private final String pageAccountType = 'Payer';
	private UserServices_CS userServices;

	/********* Page Params *********/
	public String selectedUserId {get;set;}
	public UserModel_CS selectedUser {get;set;}
	
	public String searchString {get;set;}
	//public boolean noSearchResults {get;set;}
	
	/**** User Permissions****/
	public Boolean editUserPermission {get{ return portalUser.role.Edit_Users__c; }} // Added by VK on 10/12/2016 for fix permission issue
	
	/********* Page Interface *********/
	public ApexPages.Standardsetcontroller userListSetCon {get;set;}

    public List<Contact> userListRecords 
    {
        get{
            if(userListSetCon.getRecords() != null){
           		return (List<Contact>) userListSetCon.getRecords();
            }
            return new List<Contact>();
        }
        private set;
    }
    
    public Integer pageSize {
		get{
			if(pageSize == null){ pageSize = 10; }
			return pageSize;
		}
		set{
			pageSize = value;			
			if(userListSetCon != null){	userListSetCon.setPageSize(value); }
		}
	}

	public Integer totalPages {
    	get{
    		return (Integer)Math.ceil((Decimal)userListSetCon.getResultSize() / userListSetCon.getPageSize());
    	}
    	set;
	}
	
	/********* Constructor/Init *********/
	public PayerUsersSummary_CS(){
		pageSize = 10;
	}
	
	// Default page action, add additional page action functionality here
	public PageReference init(){
		
		///////////////////////////////////
		// Authentication and Permission
		///////////////////////////////////
		
		if(!isSecure && !Test.isRunningTest()){
			return DoLogout();
		}
				
		if(!authenticated){
			return DoLogout();
		}

		if(portalUser.AccountType != pageAccountType){
			return homepage;
		}
		
		///////////////////////////////////
		// Additional init
		///////////////////////////////////
		
		userServices = new userServices_CS(portalUser.Id);
		
		userListSetCon = userServices.getUsers();
		userListSetCon.setPageSize(pageSize);
		
		//No records have been searched yet
		//noSearchResults = false;
		
		return null;
	}//End Init
	
	/********* Page Buttons/Actions *********/
	
	public void searchUsers(){
		
		System.debug('Search Users Initiated');
		
		userListSetCon = userServices.searchUsers(searchString);
		
    	if(userListSetCon.getRecords().size() > 0){
           	//noSearchResults = false;
           	//System.debug('User List: ' + userListRecords);
        }
        else{
        	//noSearchResults = true;
        	System.debug('No search results');
        }
	}
	
	/********* Page Buttons/Actions on Columns *********/	
	public PageReference sendUsername(){
		
		System.debug('Selected User Id: ' + selectedUserId);

		if(selectedUserId != null){
			selectedUser = new UserModel_CS(userServices.getUserById(selectedUserId));			
			selectedUser.sendUsernameTo();
		}
		else{
			System.debug('sendUsername Error: selectedUserId is null.');
		}
		return null;
	}
	
	public PageReference resetPassword(){
		
		if(selectedUserId != null){
			String newPassword = PasswordServices_CS.randomPassword(8);
			
			if(selectedUser.changePassword(newPassword)){
				selectedUser = new UserModel_CS(userServices.getUserById(selectedUserId));	
				selectedUser.sendPasswordTo(newPassword);
			}
			else{
				System.debug('resetPassword Error: Password Failed to change');
			}
		}
		else{
			System.debug('resetPassword Error: selectedUserId is null.');
		}
		return null;
	}
	
	public PageReference deactivateUser(){
		
		if(selectedUserId != null){
			selectedUser = new UserModel_CS(userServices.getUserById(selectedUserId));	
			selectedUser.deactivate();
		}
		else{
			System.debug('deactivateUser Error: selectedUserId is null.');
		}
		return null;
	}
}