@isTest
private class ProviderOrderSummary_CS_Test {
    public static Order__c testOrder;
    public static List<DME_Line_Item__c> lineItems;
    public static Plan_Patient__c planPatient;
    public static Account provider;
    public static Contact user;
    
    /*
    static testMethod void ProviderOrderSummary_CS_Test() {
        init();
        Test.setCurrentPage(Page.ProviderOrderSummary_CS);
        ApexPages.currentPage().getParameters().put('oId', testOrder.Id);
        TestServices_CS.login(user);
        
        ProviderOrderSummary_CS cont = new ProviderOrderSummary_CS();
        String orderCount = cont.pendingOrderCount;
        system.assertEquals('', orderCount);
        
        cont.init();
        orderCount = cont.pendingOrderCount;
        system.assertEquals('1', orderCount);
        system.assertEquals(testOrder.Id, cont.orderModel.instance.Id);
        system.assertEquals(planPatient.Id, cont.thisPatient.Id);
        system.assertEquals(3, cont.lineItemList.size());
        
        DateTime exp = system.now().addHours(2);
        cont.orderModel.instance.Expiration_Time__c = exp;
        DateTime est = system.now().addDays(1);
        cont.orderModel.instance.Estimated_Delivery_Time__c = est;
        cont.acceptOrder();
        system.assertEquals(exp, [SELECT Expiration_Time__c FROM Order__c WHERE Id = :testOrder.Id].Expiration_Time__c);
        system.assertEquals(est, [SELECT Estimated_Delivery_Time__c FROM Order__c WHERE Id = :testOrder.Id].Estimated_Delivery_Time__c);
        system.assertEquals('Provider Accepted', [SELECT Status__c FROM Order__c WHERE Id = :testOrder.Id].Status__c);
    }
    */
    
    static testMethod void ProviderOrderSummary_CS_initNotAuth(){
        init();
        Test.setCurrentPage(Page.ProviderOrderSummary_CS);
        ApexPages.currentPage().getParameters().put('oId', testOrder.Id);
        
        ProviderOrderSummary_CS cont = new ProviderOrderSummary_CS();
        cont.init();
    }
    
    static testMethod void ProviderOrderSummary_CS_initMissMatch(){
        init();
        user.Entity__c = [
            SELECT
                Health_Plan__c
            FROM Plan_Patient__c
            WHERE Id = :planPatient.Id
        ].Health_Plan__c;
        update user;
        
        Test.setCurrentPage(Page.ProviderOrderSummary_CS);
        ApexPages.currentPage().getParameters().put('oId', testOrder.Id);
        TestServices_CS.login(user);
        
        ProviderOrderSummary_CS cont = new ProviderOrderSummary_CS();
        cont.init();
    }
    
    static testMethod void ProviderOrderSummary_CS_initNoOrder(){
        init();
        Test.setCurrentPage(Page.ProviderOrderSummary_CS);
        TestServices_CS.login(user);
        
        ProviderOrderSummary_CS cont = new ProviderOrderSummary_CS();
        cont.init();
    }

    static testMethod void ProviderOrderSummary_CS_initNoOrderFound(){
        init();
        Test.setCurrentPage(Page.ProviderOrderSummary_CS);
        ApexPages.currentPage().getParameters().put('oId', '000000000000000');
        TestServices_CS.login(user);

        ProviderOrderSummary_CS cont = new ProviderOrderSummary_CS();
        cont.init();
    }
    
    /*
    static testMethod void ProviderOrderSummary_CS_initNoLineItems(){
        init();
        delete lineItems;
        Test.setCurrentPage(Page.ProviderOrderSummary_CS);
        ApexPages.currentPage().getParameters().put('oId', testOrder.Id);
        TestServices_CS.login(user);
        
        System.debug(ApexPages.currentPage().getUrl());
        
        ProviderOrderSummary_CS cont = new ProviderOrderSummary_CS();
        lineItems = cont.lineItemList;
        system.assertEquals(0, lineItems.size());
    }
    */
    
    static testMethod void ProviderOrderSummary_CS_acceptOrderExpired(){
        ProviderOrderSummary_CS cont = initPageController();

        cont.estimatedDate = System.now().addHours(-2).format('M/d/yyyy h:mm a');
        PageReference pRef = cont.acceptOrder();
        System.assertEquals(null, pRef, cont.errorMessage);
    }
    
    static testMethod void ProviderOrderSummary_CS_acceptOrderInvalidEstimatedDelivery(){
        ProviderOrderSummary_CS cont = initPageController();

        cont.estimatedDate = String.valueOf(System.now().addHours(2));
        PageReference pRef = cont.acceptOrder();
        System.assertEquals(null, pRef, cont.errorMessage);
    }

    static testMethod void ProviderOrderSummary_CS_acceptOrderNoEstimatedDelivery(){
        ProviderOrderSummary_CS cont = initPageController();

        PageReference pRef = cont.acceptOrder();
        System.assertEquals(null, pRef, cont.errorMessage);
    }

    static testMethod void ProviderOrderSummary_CS_acceptOrder(){
        ProviderOrderSummary_CS cont = initPageController();

        cont.estimatedDate = System.now().addHours(2).format('M/d/yyyy h:mm a');
        PageReference pRef = cont.acceptOrder();
        System.assert(String.isEmpty(cont.errorMessage));
    }
    
    static testMethod void ProviderOrderSummary_CS_rejectOrder(){
        init();
        Test.setCurrentPage(Page.ProviderOrderSummary_CS);
        ApexPages.currentPage().getParameters().put('oId', testOrder.Id);
        TestServices_CS.login(user);
        
        ProviderOrderSummary_CS cont = new ProviderOrderSummary_CS();
        cont.init();

        List<SelectOption> rejectionReasons = cont.getRejectionReasons();
        cont.rejectionReason = OrderModel_CS.STATUS_PROVIDER_REJECTED_OUT_OF_STOCK;
        cont.otherRejectionReason = 'Test Reason';
        cont.rejectOrder();
        system.assertEquals(OrderModel_CS.STATUS_PROVIDER_REJECTED_OUT_OF_STOCK, [SELECT Status__c FROM Order__c WHERE Id = :testOrder.Id].Status__c);
        system.assertEquals('Test Reason', [SELECT Reason_for_Rejection__c FROM Order__c WHERE Id = :testOrder.Id].Reason_for_Rejection__c);
    }
    
    static testMethod void ProviderOrderSummary_CS_rejectOrderNoReason(){
        init();
        Test.setCurrentPage(Page.ProviderOrderSummary_CS);
        ApexPages.currentPage().getParameters().put('oId', testOrder.Id);
        TestServices_CS.login(user);
        
        ProviderOrderSummary_CS cont = new ProviderOrderSummary_CS();
        cont.init();
        
        cont.rejectOrder();
    }
    
    static testMethod void ProviderOrderSummary_CS_rejectOrderNoExplanation(){
        init();
        Test.setCurrentPage(Page.ProviderOrderSummary_CS);
        ApexPages.currentPage().getParameters().put('oId', testOrder.Id);
        TestServices_CS.login(user);

        ProviderOrderSummary_CS cont = new ProviderOrderSummary_CS();
        cont.init();
        
        cont.rejectionReason = 'Provider Rejected - Other';
        cont.rejectOrder();
    }

    static testMethod void ProviderOrderSummary_CS_createNewMessage_BlankNoteAndNoFlagChanged() {
        ProviderOrderSummary_CS cont = initPageController();

        cont.createNewMessage();
        System.assert(String.isNotBlank(cont.noteErrorMsg));
    }

    static testMethod void ProviderOrderSummary_CS_createNewMessage_flagAttentionNoReason() {
        ProviderOrderSummary_CS cont = initPageController();

        cont.flagAttention = true;
        cont.createNewMessage();
        System.assert(String.isNotBlank(cont.noteErrorMsg));
    }

    static testMethod void ProviderOrderSummary_CS_createNewMessage_flagAttentionWithReason() {
        ProviderOrderSummary_CS cont = initPageController();

        cont.flagAttention = true;
        List<SelectOption> attentionReasons = cont.getAttentionReasons();
        cont.attentionReason = 'Delivery Information Incomplete';
        cont.noteBody = 'Some text here';
        cont.createNewMessage();
        System.assert(String.isBlank(cont.noteErrorMsg));
    }

    static testMethod void ProviderOrderSummary_CS_changeFutureDosNullParameters() {
        ProviderOrderSummary_CS cont = initPageController();
        String orderStatus = cont.orderModel.instance.Status__c;

        cont.changeFutureDos();
        System.assertEquals(cont.orderModel.instance.Status__c, orderStatus);
    }

    static testMethod void ProviderOrderSummary_CS_changeFutureDosEmptyNote() {
        ProviderOrderSummary_CS cont = initPageController();
        String orderStatus = cont.orderModel.instance.Status__c;

        cont.isMarkedAsFutureDos = true;
        cont.changeFutureDos();
        System.assertEquals(cont.errorMessage, Label.NoFutureDosNoteEntered);
        System.assertEquals(cont.orderModel.instance.Status__c, orderStatus);
    }

    static testMethod void ProviderOrderSummary_CS_changeFutureDos_mark() {
        ProviderOrderSummary_CS cont = initPageController();
        System.assertNotEquals(cont.orderModel.instance.Status__c, OrderModel_CS.STATUS_FUTURE_DOS);

        cont.isMarkedAsFutureDos = true;
        cont.futureDosNote = 'Test Note';
        cont.changeFutureDos();
        System.assertEquals(cont.orderModel.instance.Status__c, OrderModel_CS.STATUS_FUTURE_DOS);
    }

    static testMethod void ProviderOrderSummary_CS_changeFutureDos_unmark() {
        ProviderOrderSummary_CS cont = initPageController();
        System.assertEquals(cont.isOrderFutureDos, false);

        cont.isMarkedAsFutureDos = true;
        cont.futureDosNote = 'Mark Future DOS';
        cont.changeFutureDos();
        System.assertEquals(cont.isOrderFutureDos, true);

        Test.startTest();
        cont.isMarkedAsFutureDos = false;
        cont.futureDosNote = 'Un-Mark Future DOS';
        cont.changeFutureDos();
        System.assertEquals(cont.orderModel.instance.Status__c, OrderModel_CS.STATUS_PENDING_REACCEPTANCE);
        Test.stopTest();
    }

    static testMethod void ProviderOrderSummary_CS_changeFutureDos_getNumberOfDaysBetweenExpectedDeliveryDateAndCurrentDate() {
        ProviderOrderSummary_CS cont = initPageController();
        System.assertEquals(cont.numberOfDaysBetweenExpectedDeliveryDateAndCurrentDate, 0, cont.numberOfDaysBetweenExpectedDeliveryDateAndCurrentDate);
    }

    static testMethod void ProviderOrderSummary_CS_checkPermission() {
        init();
        Role__c adminRole = TestDataFactory_CS.generateAdminRole();
        insert adminRole;
        user.Content_Role__c = adminRole.Id;
        update user;

        Test.setCurrentPage(Page.ProviderOrderSummary_CS);
        ApexPages.currentPage().getParameters().put('oId', testOrder.Id);
        TestServices_CS.login(user);

        Test.startTest();
        ProviderOrderSummary_CS cont = new ProviderOrderSummary_CS();
        cont.init();
        
        System.assertEquals(cont.acceptOrderPermission, true);
        System.assertEquals(cont.rejectOrderPermission, true);
        System.assertEquals(cont.deliveryOrderPermission, true);
        System.assertEquals(cont.shippingOrderPermission, true);
        System.assertEquals(cont.markFutureDosOrderPermission, true);
        Test.stopTest();
    }

    static testMethod void ProviderOrderSummary_CS_createNewMessage_unflagAttention() {
        ProviderOrderSummary_CS cont = initPageController();

        cont.unflagAttention = true;
        cont.createNewMessage();
        System.assert(String.isBlank(cont.noteErrorMsg));

        cont.flagAttention = true;
        cont.attentionReason = 'Delivery Information Incomplete';
        cont.createNewMessage();
        System.assert(String.isBlank(cont.noteErrorMsg));

        Test.startTest();
        cont.unflagAttention = true;
        cont.createNewMessage();
        System.assert(String.isBlank(cont.noteErrorMsg));
        Test.stopTest();
    }

    static testMethod void ProviderOrderSummary_CS_print() {
        ProviderOrderSummary_CS cont = initPageController();

        PageReference pRef = cont.print();
        System.assertNotEquals(null, pRef);
    }
    
    /*    
    static testMethod void ProviderOrderSummary_CS_markDelivered(){
        init();
        Test.setCurrentPage(Page.ProviderOrderSummary_CS);
        ApexPages.currentPage().getParameters().put('oId', testOrder.Id);
        TestServices_CS.login(user);
        
        ProviderOrderSummary_CS cont = new ProviderOrderSummary_CS();
        cont.init();
        
        DateTime del = system.now().addHours(-6);
        cont.orderModel.instance.Delivery_Date_Time__c = del;
        update cont.orderModel.instance;
        //cont.markDelivered();
        
        system.assertEquals(del, [SELECT Delivery_Date_Time__c FROM Order__c WHERE Id = :testOrder.Id].Delivery_Date_Time__c);
        system.assertEquals('Delivered', [SELECT Status__c FROM Order__c WHERE Id = :testOrder.Id].Status__c);
        system.assertEquals('Store Personnel', [SELECT Delivery_Method__c FROM Order__c WHERE Id = :testOrder.Id].Delivery_Method__c);
    }
    */
    
    static testMethod void ProviderOrderSummary_CS_markDeliveredNoDate(){
        ProviderOrderSummary_CS cont = initPageController();
        
        PageReference pRef = cont.markDelivered();
        System.assertEquals(null, pRef, cont.errorMessage);
    }

    static testMethod void ProviderOrderSummary_CS_markDeliveredInvalidDeliveryDate(){
        ProviderOrderSummary_CS cont = initPageController();

        cont.deliveredDate = String.valueOf(System.now());
        PageReference pRef = cont.markDelivered();
        System.assertEquals(null, pRef, cont.errorMessage);
    }

    static testMethod void ProviderOrderSummary_CS_markDeliveredFutureDate(){
        ProviderOrderSummary_CS cont = initPageController();

        cont.deliveredDate = System.now().addHours(2).format('M/d/yyyy h:mm a');
        PageReference pRef = cont.markDelivered();
        System.assertEquals(null, pRef, cont.errorMessage);
    }

    static testMethod void ProviderOrderSummary_CS_markDelivered(){
        ProviderOrderSummary_CS cont = initPageController();

        cont.deliveredDate = System.now().addHours(-2).format('M/d/yyyy h:mm a');
        PageReference pRef = cont.markDelivered();
        System.assert(String.isEmpty(cont.errorMessage));
    }

    static testMethod void ProviderOrderSummary_CS_changeEstimatedDeliveryDate_EmptyDeliveryDate(){
        ProviderOrderSummary_CS cont = initPageController();

        PageReference pRef = cont.changeEstimatedDeliveryDate();
        System.assertEquals(null, pRef, cont.errorMessage);
    }

    static testMethod void ProviderOrderSummary_CS_changeEstimatedDeliveryDate_BadTimeFormat(){
        ProviderOrderSummary_CS cont = initPageController();

        cont.estimatedDelDate = String.valueOf(System.now());
        PageReference pRef = cont.changeEstimatedDeliveryDate();
        System.assertEquals(null, pRef, cont.errorMessage);
    }

    static testMethod void ProviderOrderSummary_CS_changeEstimatedDeliveryDate_PastDate(){
        ProviderOrderSummary_CS cont = initPageController();

        cont.estimatedDelDate = System.now().addHours(-2).format('M/d/yyyy h:mm a');
        PageReference pRef = cont.changeEstimatedDeliveryDate();
        System.assertEquals(null, pRef, cont.errorMessage);
    }

    /*static testMethod void ProviderOrderSummary_CS_changeEstimatedDeliveryDate_NoReasonEntered(){
        ProviderOrderSummary_CS cont = initPageController();

        cont.estimatedDelDate = System.now().addHours(2).format('M/d/yyyy h:mm a');
        PageReference pRef = cont.changeEstimatedDeliveryDate();
        System.assertEquals(null, pRef, cont.errorMessage);
    }*/

    static testMethod void ProviderOrderSummary_CS_changeEstimatedDeliveryDate(){
        ProviderOrderSummary_CS cont = initPageController();

        cont.estimatedDelDate = System.now().addHours(2).format('M/d/yyyy h:mm a');
        cont.changeDeliveryReason = 'Some Reason';
        PageReference pRef = cont.changeEstimatedDeliveryDate();
        System.assert(String.isEmpty(cont.errorMessage), cont.errorMessage);
    }
    
    static testMethod void ProviderOrderSummary_CS_markShipped(){
        init();
        Test.setCurrentPage(Page.ProviderOrderSummary_CS);
        testOrder.Accepted_Date__c = Datetime.now();
        update testOrder;
        ApexPages.currentPage().getParameters().put('oId', testOrder.Id);
        TestServices_CS.login(user);
        
        ProviderOrderSummary_CS cont = new ProviderOrderSummary_CS();
        cont.init();


        List<SelectOption> deliveryMethods = cont.getDeliveryMethods();
        cont.deliveryMethod = 'Test Method';
        cont.trackingNumber = 'Test Number';
        cont.markShipped();
        system.assertEquals('Test Method', [SELECT Delivery_Method__c FROM Order__c WHERE Id = :testOrder.Id].Delivery_Method__c);
        system.assertEquals('Test Number', [SELECT Tracking_Number__c FROM Order__c WHERE Id = :testOrder.Id].Tracking_Number__c);
    }
    
    static testMethod void ProviderOrderSummary_CS_markShippedNoMethod(){
        init();
        Test.setCurrentPage(Page.ProviderOrderSummary_CS);
        ApexPages.currentPage().getParameters().put('oId', testOrder.Id);
        TestServices_CS.login(user);
        
        ProviderOrderSummary_CS cont = new ProviderOrderSummary_CS();
        cont.init();

        cont.trackingNumber = 'Test Number';
        PageReference pRef = cont.markShipped();
        System.assertEquals(null, pRef);
    }
    
    static testMethod void ProviderOrderSummary_CS_markShippedNoTrackingNumber(){
        init();
        Test.setCurrentPage(Page.ProviderOrderSummary_CS);
        ApexPages.currentPage().getParameters().put('oId', testOrder.Id);
        TestServices_CS.login(user);
        
        ProviderOrderSummary_CS cont = new ProviderOrderSummary_CS();
        cont.init();

        cont.deliveryMethod = 'Test Method';
        PageReference pRef = cont.markShipped();
        System.assertEquals(null, pRef);
    }
    
    static testMethod void PayerOrderSummary_Test_AttachFile(){
        init();
        Test.setCurrentPage(Page.ProviderOrderSummary_CS);
        ApexPages.currentPage().getParameters().put('oId', testOrder.Id);
        TestServices_CS.login(user);
        
        ProviderOrderSummary_CS cont = new ProviderOrderSummary_CS();
        cont.init();
        
        cont.attachment = new Attachment(Name='testAttachment',Body=Blob.valueOf('Unit Test Attachment Body'));
        
        cont.uploadAttachment();
        System.assertEquals(cont.attachmentSuccess,cont.attachmentUploadMessage);
        
        List<Attachment> attachments = cont.currentAttachments;
        System.assertEquals(1,attachments.size());

        Test.startTest();
        cont.selectedAttachmentId = attachments[0].Id;
        cont.deleteAttachment();
        Test.stopTest();
    }

    private static ProviderOrderSummary_CS initPageController() {
        init();
        Test.setCurrentPage(Page.ProviderOrderSummary_CS);
        ApexPages.currentPage().getParameters().put('oId', testOrder.Id);
        TestServices_CS.login(user);

        ProviderOrderSummary_CS cont = new ProviderOrderSummary_CS();
        cont.init();

        return cont;
    }
    
    public static void init(){
        TestDataFactory_CS tdf = new TestDataFactory_CS();
        testOrder = tdf.ordersToReturn[0];
        testOrder.Status__c = 'New';
        update testOrder;
        lineItems = tdf.lineItemsToReturn;
        planPatient = tdf.planPatientsToReturn[0];
        provider = tdf.providersToReturn[0];
        user = tdf.providerContactsToReturn[0];
        user.Id_Key__c = PasswordServices_CS.hash(user.Id, user.Salt__c);
        update user;
    }

    static testMethod void testForPendingReacceptanceNoReason(){
        TestDataFactory_CS tdf = new TestDataFactory_CS();
        testOrder = tdf.ordersToReturn[0];
        testOrder.Status__c = 'Pending Re-acceptance';
        testOrder.Estimated_Delivery_Time__c = System.now();
        update testOrder;
        lineItems = tdf.lineItemsToReturn;
        planPatient = tdf.planPatientsToReturn[0];
        provider = tdf.providersToReturn[0];
        user = tdf.providerContactsToReturn[0];
        user.Id_Key__c = PasswordServices_CS.hash(user.Id, user.Salt__c);
        update user;

        Test.setCurrentPage(Page.ProviderOrderSummary_CS);
        ApexPages.currentPage().getParameters().put('oId', testOrder.Id);
        TestServices_CS.login(user);

        ProviderOrderSummary_CS cont = new ProviderOrderSummary_CS();
        cont.init();

        cont.estimatedDate = System.now().addDays(15).format('M/d/yyyy h:mm a');
        PageReference pRef = cont.acceptOrder();
        System.assert(String.isNotEmpty(cont.errorMessage));
        System.assert(String.isNotEmpty(cont.displayExpectedDate));
    }

    static testMethod void testForPendingReacceptanceWithReason(){
        TestDataFactory_CS tdf = new TestDataFactory_CS();
        testOrder = tdf.ordersToReturn[0];
        testOrder.Status__c = 'Pending Re-acceptance';
        testOrder.Estimated_Delivery_Time__c = System.now();
        update testOrder;
        lineItems = tdf.lineItemsToReturn;
        planPatient = tdf.planPatientsToReturn[0];
        provider = tdf.providersToReturn[0];
        user = tdf.providerContactsToReturn[0];
        user.Id_Key__c = PasswordServices_CS.hash(user.Id, user.Salt__c);
        update user;

        Test.setCurrentPage(Page.ProviderOrderSummary_CS);
        ApexPages.currentPage().getParameters().put('oId', testOrder.Id);
        TestServices_CS.login(user);

        ProviderOrderSummary_CS cont = new ProviderOrderSummary_CS();
        cont.init();

        cont.estimatedDate = System.now().addDays(15).format('M/d/yyyy h:mm a');
        cont.changeEstimatedDateInfo = 'This is a test';
        PageReference pRef = cont.acceptOrder();
        System.assert(String.isEmpty(cont.errorMessage));
    }
}