/**
* @description This class is used to delete all orders with the status of draft. This is a
*		batchable ad scheduable class.
* @author Cloud Software LLC, Tom Sharp
* Revision History:
*		05/12/2016 - Tom Sharp - Created.
*		05/15/2016 - karnold - Moved schedulable to this class and added comments.
*/
public class DeleteDraftOrdersBatch implements Database.Batchable<sObject>, Schedulable {
	private String query;
	private static string schedule = '0 0 * * * ?'; // Scheduled to run every hour

	// Batchable methods

	public DeleteDraftOrdersBatch() {
		query = 
			'SELECT ' +
				'Id ' +
			'FROM Order__c ' +
			'WHERE Status__c = \'Draft\'';
	}

	/**
	* @description Populates the scope of the batch with all orders that are in status "Draft"
	*/
	public Database.QueryLocator start(Database.BatchableContext context) {
		return Database.getQueryLocator(query);
	}
	
	/**
	* @description Deletes all the records passed into the method
	*/
	public void execute(Database.BatchableContext context, List<Order__c> scope) {
		delete scope;
	}
	
	/**
	* @description No finish actions at this time.
	*/
	public void finish(Database.BatchableContext context) {}

	// Schedulable methods

	/**
	* @description Schedules this class to run based on the cron String in schedule.
	*/
	public static String scheduleJob() {
		DeleteDraftOrdersBatch job = new DeleteDraftOrdersBatch();
		return System.schedule('Delete Draft Orders', schedule, job);
	}

	/**
	* @description Executes the batch part of this class.
	*/
	public void execute(SchedulableContext context) {
		DeleteDraftOrdersBatch batch = new DeleteDraftOrdersBatch();
		Database.executeBatch(batch); 
	}

	// Getters and Setters

	public void setQuery(String query) {
		this.query = query;
	}
}