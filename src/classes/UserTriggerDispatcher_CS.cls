public without sharing class UserTriggerDispatcher_CS {

	public UserTriggerDispatcher_CS(Map<Id,SObject> oldMap, Map<Id,SObject> newMap, List<SObject> triggerOld, List<SObject> triggerNew, Boolean isInsert, Boolean isUpdate, Boolean isDelete, Boolean isUndelete, Boolean isBefore, Boolean isAfter){
		if(isBefore) {
            if(isInsert) 		{ doBeforeInsert(triggerNew); 								}
            else if(isUpdate) 	{ doBeforeUpdate(oldMap, newMap, triggerOld, triggerNew);	}
            else if(isDelete) 	{ doBeforeDelete(oldMap, triggerOld); 						}           
        }else if(isAfter) {
            if(isInsert) 		{ doAfterInsert(newMap, triggerNew); 						}
            else if(isUpdate) 	{ doAfterUpdate(oldMap, newMap, triggerOld, triggerNew); 	}
            else if(isDelete) 	{ doAfterDelete(oldMap, triggerold); 						}
            else if(isUndelete)	{ doAfterUnDelete(newMap, triggerNew); 						}        
        }
	}
	
	/****************************************** START TRIGGER FUNCTIONS ******************************************/
    
    public void doBeforeInsert(List<SObject> triggerNew) {
		
    }
    
    public void doBeforeUpdate(Map<Id,SObject> oldMap, Map<Id,SObject> newMap, List<SObject> triggerOld, List<SObject> triggerNew) {
    	
    	for(Contact c : (Contact[])triggerNew){
    		
    		Contact oldContact = ((Contact)oldMap.get(c.Id));
    		Contact newContact = ((Contact)newMap.get(c.Id));
    		
    		//Check for Active__c switching from false to true, reset the attempt count
    		if(	oldContact.Active__c == false &&
    			newContact.Active__c == true)
    		{
    			newContact.Login_Attempts__c = 0;
    		}    	
    	}
    }
    
    public void doBeforeDelete(Map<Id,SObject> oldMap, List<SObject> triggerOld) {
    
    }   
    
    public void doAfterInsert(Map<Id,SObject> newMap, List<SObject> triggerNew) {
		
    }    
    
    public void doAfterUpdate(Map<Id,SObject> oldMap, Map<Id,SObject> newMap, List<SObject> triggerOld, List<SObject> triggerNew) {
    	
    	for(Contact c : (Contact[])triggerNew){
    		
    		Contact oldContact = ((Contact)oldMap.get(c.Id));
    		Contact newContact = ((Contact)newMap.get(c.Id));
    		
    		//TODO: For now DO NOT notify when the password is changed from null to a value. This is to attempt to prevent
    		// a new user from being notified of a password change.
    		
    		System.debug('oldPassword/newPassword : ' + oldContact.Password__c + '/' +
    			newContact.Password__c);
    		
    		if(	oldContact.Password__c != null &&
    			oldContact.Password__c != newContact.Password__c){
    			ProviderNotificationServices_CS.notifyPasswordChange(c);
    		}
    	}
    }  
      
    public void doAfterDelete(Map<Id,SObject> oldMap, List<SObject> triggerOld) {
    
    }
     
    public void doAfterUndelete(Map<Id,SObject> newMap, List<SObject> triggerNew) {
    
    }    
    
	/****************************************** END TRIGGER FUNCTIONS ******************************************/
}