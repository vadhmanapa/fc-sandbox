public class DMELineItemDispatcher_CS { 
	
	public DMELineItemDispatcher_CS(Map<Id,DME_Line_Item__c> oldMap, Map<Id,DME_Line_Item__c> newMap, List<DME_Line_Item__c> triggerOld, List<DME_Line_Item__c> triggerNew, Boolean isInsert, Boolean isUpdate, Boolean isDelete, Boolean isUndelete, Boolean isBefore, Boolean isAfter){
        if(isBefore) {
            if(isInsert)        { doBeforeInsert(oldMap, newMap, triggerOld, triggerNew);	}
            else if(isUpdate)   { doBeforeUpdate(oldMap, newMap, triggerOld, triggerNew);   }
            else if(isDelete)   { doBeforeDelete(oldMap, triggerOld);                       }           
        }else if(isAfter) {
            if(isInsert)        { doAfterInsert(newMap, triggerNew);                        }
            else if(isUpdate)   { doAfterUpdate(oldMap, newMap, triggerOld, triggerNew);    }
            else if(isDelete)   { doAfterDelete(oldMap, triggerold);                        }
            else if(isUndelete) { doAfterUnDelete(newMap, triggerNew);                      }        
        }
    }

    /****************************************** START TRIGGER FUNCTIONS ******************************************/
    
    public void doBeforeInsert(Map<Id,DME_Line_Item__c> oldMap, Map<Id,DME_Line_Item__c> newMap, List<DME_Line_Item__c> triggerOld, List<DME_Line_Item__c> triggerNew) {
        system.debug('DMELineItemDispatcher: doBeforeInsert()');

		//Collect DME and Order Ids from each DME Line Item
		Set<Id> dmeIds = new Set<Id>();
		Set<Id> orderIds = new Set<Id>();
		for(DME_Line_Item__c li : triggerNew) {
			dmeIds.add(li.DME__c);
			orderIds.add(li.Order__c);
		}

		//Query for DMEs
		Map<Id, DME__c> dmeIdToDME = new Map<Id, DME__c>([
			SELECT 
				Id,
				Max_Delivery_Time__c,
				Miscellaneous__c
			FROM DME__c
			WHERE Active__c = true
			AND Id IN :dmeIds
		]);

		//Query for Orders
		Map<Id, Order__c> orderIdToOrder = new Map<Id, Order__c>([
			SELECT Comments__c
			FROM Order__c
			WHERE Id IN :orderIds
		]);

		System.debug('Trigger new: ' + triggerNew);

		//Iterate over all DME Line Items
		for(DME_Line_Item__c li : triggerNew) {
			
			//Set the "Deliver By" time
			setDeliverBy(li, dmeIdToDME.get(li.DME__c)); 

			//If the DME is miscellaneous, throw an error if the order does not have comments
			miscMustHaveComments(li, dmeIdToDME.get(li.DME__c), orderIdToOrder.get(li.Order__c));
		}
    }    
    
    public void doBeforeUpdate(Map<Id,SObject> oldMap, Map<Id,SObject> newMap, List<SObject> triggerOld, List<SObject> triggerNew) {
         system.debug('DMELineItemDispatcher: doBeforeUpdate()');
    }
    
    public void doBeforeDelete(Map<Id,SObject> oldMap, List<SObject> triggerOld) {
    	system.debug('DMELineItemDispatcher: doBeforeDelete()');
    }   
    
    public void doAfterInsert(Map<Id,SObject> newMap, List<SObject> triggerNew) {
        system.debug('DMELineItemDispatcher: doAfterInsert()');
    }    
    
    public void doAfterUpdate(Map<Id,SObject> oldMap, Map<Id,SObject> newMap, List<SObject> triggerOld, List<SObject> triggerNew) {
        system.debug('DMELineItemDispatcher: doAfterUpdate()');
    }  
      
    public void doAfterDelete(Map<Id,SObject> oldMap, List<SObject> triggerOld) {
    	system.debug('DMELineItemDispatcher: doAfterDelete()');
    }
     
    public void doAfterUndelete(Map<Id,SObject> newMap, List<SObject> triggerNew) {
    	system.debug('DMELineItemDispatcher: doAfterUndelete()');
    }

	//Sets the "Deliver By time" on DME Line Item from Max Delivery Time on DME
	public void setDeliverBy(DME_Line_Item__c li, DME__c dme) {
		if(dme.Max_Delivery_Time__c != null) {
			datetime dt = System.now().addHours(Integer.valueOf(dme.Max_Delivery_Time__c));
			dt = dt.addSeconds(60 - dt.second());
			dt = dt.addMinutes(60 - dt.minute());
			li.Deliver_By__c = dt;
		}
	}

	//If the DME is miscellaneous, this method adds an error on the DME Line Item if the order does not have comments
	public void miscMustHaveComments(DME_Line_Item__c li, DME__c dme, Order__c o) {
		System.debug('DME: ' + dme);
		System.debug('Order: ' + o);
		if (dme.Miscellaneous__c && String.isBlank(o.Comments__c)) {
			System.debug('Adding error');
			li.addError('Orders with miscellaneous DMEs must have comments');
		}
	}
}