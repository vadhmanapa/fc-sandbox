public without sharing class ProviderUsersSummary_CS extends ProviderPortalServices_CS {

	private final String pageAccountType = 'Provider';

	private AccountServices_CS accServices;
	private UserServices_CS userServices;
	
	/******* Page Permission *******/
	public Boolean createUserPermission {get{ return portalUser.role.Create_Users__c; }}
	public Boolean resetPasswordPermission {get{ return portalUser.role.Reset_Password__c; }}
	
	/********* Page Interface *********/
	public ApexPages.Standardsetcontroller userSetCon {get;set;}

    public List<Contact> userList 
    {
        get{
            if(userSetCon.getRecords() != null){
           		return (List<Contact>) userSetCon.getRecords();
            }
            return new List<Contact>();
        }
        private set;
    }

	/********* Page Params *********/
	public String selectedUserId {get;set;}
	public UserModel_CS selectedUser {get;set;}
	
	public String searchString {get;set;}
	
	public Integer pageSize {
		get{
			if(pageSize == null){ pageSize = 10; }
			return pageSize;
		}
		set{
			pageSize = value;			
			if(userSetCon != null){	userSetCon.setPageSize(value); }
		}
	}

	public Integer totalPages {
    	get{
    		return (Integer)Math.ceil((Decimal)userSetCon.getResultSize() / userSetCon.getPageSize());
    	}
    	set;
	}

	/********* Constructor/Init *********/
	public ProviderUsersSummary_CS(){}

	public PageReference init(){

		///////////////////////////////////
		// Authentication and Permission
		///////////////////////////////////
		
		if(!isSecure && !Test.isRunningTest()){
			return DoLogout();
		}
				
		if(!authenticated){
			return DoLogout();
		}

		System.debug('Checking user account type: ' + portalUser.AccountType + ' / page account type ' + pageAccountType);

		if(portalUser.AccountType != pageAccountType){
			return homepage;
		}

		///////////////////////////////////
		// Additional init
		///////////////////////////////////

		//accServices = new AccountServices_CS(portalUser.instance.Id);
		userServices = new UserServices_CS(portalUser.instance.Id);
		
		//userSetCon = accServices.getPortalUsers();
		userSetCon = userServices.getUsers();
		userSetCon.setPageSize(pageSize);
		system.debug('Size of userSetCon: ' + userSetCon.getResultSize());
		
		return null;
	}

	
	/********* Page Buttons/Actions on Columns *********/	
	public void searchUsers(){
		
		System.debug('Search Users Initiated');
		
		userSetCon = userServices.searchUsers(searchString);
		
    	if(userSetCon.getRecords() != null){
           	System.debug('User List: ' + userList);
        }
        else{
        	System.debug('No search results');
        }
	}
	
	public void sendUsername(){

		if(String.isNotBlank(selectedUserId)){

			selectedUser = new UserModel_CS(userServices.getUserById(selectedUserId));
			
			if(selectedUser.sendUsernameTo()){
				System.debug('Sending username to ' + selectedUser.instance.Email + ' succeeded!');
			}
			else{
				System.debug('Error: Sending username to ' + selectedUser.instance.Email + ' failed.');
			}
		}
		else{
			System.debug('sendUsername Error: selectedUserId = null');
		}
	}
	
	public void resetPassword(){

		if(String.isNotBlank(selectedUserId)){
			
			selectedUser = new UserModel_CS(userServices.getUserById(selectedUserId));
			
			String newPassword = PasswordServices_CS.randomPassword(8);
			
			if(selectedUser.changePassword(newPassword)){
				selectedUser.sendPasswordTo(newPassword);
			}
			else{
				System.debug('resetPassword Error: Password Failed to change');
			}
		}
		else{
			System.debug('resetPassword Error: selectedUserId = null');
		}
	}
	
	public void deactivateUser(){
		
		if(String.isNotBlank(selectedUserId)){
			
			selectedUser = new UserModel_CS(userServices.getUserById(selectedUserId));
			
			selectedUser.deactivate();
			
			System.debug('deactivateUser: User ' + selectedUser.instance.Username__c + ' deactivated.');
		}
		else{
			System.debug('deactivateUser Error: selectedUserId = null');
		}
	}
}