public without sharing class OrderHistory_CS {
	public String contactId {get;set;}
	public String contactName {get;set;}	
	public String accountName {get;set;}
	public String orderStatus {get;set;}
	public Datetime timestamp {get;set;}
	
	public OrderHistory_CS(String contactId, String contactName, String accountName, String orderStatus){
		this.contactId = contactId;
		this.contactName = contactName;		
		this.accountName = accountName;
		this.orderStatus = orderStatus;
		this.timestamp = System.now();
	}
}