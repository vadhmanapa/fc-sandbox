/**
 *  @Description Test code for coverage on MultiselectController
 *  @Author Salesforce.com
 *  Revision History: 
 *		01/07/2016 - karnold - Pulled in code, added header.
 *
 */
@isTest
private class MultiselectControllerTest {
    static testMethod void testMultiselectController() {
        MultiselectController c = new MultiselectController();
        
        c.leftOptions = new List<SelectOption>();
        c.rightOptions = new List<SelectOption>();

        c.leftOptionsHidden = 'A&a&b&b&C&c';
        c.rightOptionsHidden = '';
        
        System.assertEquals(c.leftOptions.size(), 3);
        System.assertEquals(c.rightOptions.size(), 0);
    }
}