@isTest
private class NewCaseFromCallLogControllerTest {

	static PageReference pageRef;
	static NewCaseFromCallLogController pageCon;

    static void init() {
    	
    	Call_Log__c existingCallLog = new Call_Log__c();
    	insert existingCallLog;
    	
		pageRef = Page.NewCallLogLink;
    	pageRef.getParameters().put('logId',existingCallLog.Id);
    	
    	pageCon = new NewCaseFromCallLogController();
    }
}