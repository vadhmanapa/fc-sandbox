public class OrderMessage_CS {
	public String recipient1{get;set;}
	public String recipient2{get;set;}
	public String recipient3{get;set;}
    public String fromName{get;set;}
    public Boolean urgent{get;set;}
    public String message{get;set;} 
    public String orderNumber{get;set;}
    public Id orderId{get;set;}
    public Datetime timestamp{get;set;}
    
    public OrderMessage_CS(){
    	
    }//End empty constructor 
     
    public OrderMessage_CS(List<String> toNames, String fromName, String message, Boolean urgent, String orderNumber, Id orderId)
    {
    	System.debug('### toNames in OrderMessage_CS contructor: ' + toNames);
	
		Integer recipientCount = toNames.size();
	
		System.debug('### recipientCount: ' + recipientCount);
	
		if(recipientCount > 0) recipient1 = toNames[0];
		if(recipientCount > 1) recipient2 = toNames[1];
		if(recipientCount > 2) recipient3 = toNames[2];
		
        this.fromName = fromName;
        this.message = message;
        this.urgent = urgent;
        this.orderNumber = orderNumber;     
        this.orderId = orderId;
        this.timestamp = System.now();
        
        System.debug('### this: ' + this);
    }
}