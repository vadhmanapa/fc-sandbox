public with sharing class ShoreTelUtils {

	public static String getCallerPhone(Task logTask) {
		return logTask.skyenterprise__Caller_ID__c;
	}
	
	public static String getAgentPhone(Task logTask) {
		return logTask.skyenterprise__Caller_ID__c;
	}
	
	public static DateTime getCallStart(Task logTask) {
		return logTask.skyplatform__Call_Start_Date_Time__c;
	}
	
	public static DateTime getCallEnd(Task logTask) {
		return logTask.skyplatform__Call_Start_Date_Time__c.addSeconds(logTask.CallDurationInSeconds);
	}
	
	public static Integer getCallDurationSeconds(Task logTask) {
		return logTask.CallDurationInSeconds;
	}
	
	public static Boolean isIncomingCallTask(Task t) {
		
		if(t == null || t.Subject == null) {
			system.debug(LoggingLevel.WARN,'Insufficient task data.');
			return false;
		}
		
		if(t.Subject.contains('Incoming Call From')) {
			system.debug(LoggingLevel.INFO,'Found Call Log task ' + t.Subject);
			return true;
		}
		
		return false;
	}
	
	public static void updateCallLogFromTask(Task logTask) {
		
		system.debug(LoggingLevel.INFO,'Finding call log for task: ' + logTask);
		
		List<Call_Log__c> callLogs = [
			SELECT
				Id,
				Call_Received__c,
				Call_Ended__c,
				OwnerId
			FROM Call_Log__c
			WHERE OwnerId = :logTask.OwnerId
			ORDER BY CreatedDate DESC
			LIMIT 1
		];
		
		if(callLogs.size() == 0) {
			system.debug(LoggingLevel.ERROR,'No recent call logs found for user');
			return;
		}
		
		system.debug(LoggingLevel.INFO,'Found call log: ' + callLogs);
		
		Call_Log__c log = callLogs[0];
		
		//Sanity check
		Integer logDiffSec = Integer.valueOf( Math.abs(log.Call_Received__c.getTime() - getCallStart(logTask).getTime()) / 1000 );
		Integer logDiffMin = logDiffSec / 60;
		Integer logDiffHr = logDiffMin / 60;
		
		system.debug(LoggingLevel.INFO,'Sanity check -- log open: ' + log.Call_Received__c + ' | task open: ' + getCallStart(logTask));
		system.debug(LoggingLevel.INFO,'Difference: ' + logDiffSec + ' secs / ' + logDiffMin + ' min / ' + logDiffHr + ' hr');
		
		if(logDiffMin > 10) {
			system.debug(LoggingLevel.ERROR,'Large difference in log open times. Potential mismatch.');
			return;
		}
		
		CallLogUtils.updateCallLogTimestamp(callLogs[0], logTask);
	}
	
	/*
	public static void updateCallLogsFromTasks(List<Task> tasks) {
		
		Map<Id,Task> callLogIdToTask = new Map<Id,Task>();
	
		Set<Id> taskOwners = new Set<Id>();
		
		for(Task t : tasks) {
			taskOwners.add(t.Assigned_To__c);
		}
	
		Map<Id,Call_Log__c> userToLatestCallLog = new Map<Id,Call_Log__c>();
		
		for(User u : [
			
			SELECT
				Id,
				(			
					SELECT
						Id,
						Call_Received__c,
						Call_Ended__c,
						OwnerId
					FROM Call
					ORDER BY CreatedDate DESC
					LIMIT 1
				)
			FROM User
			WHERE Id IN :taskOwners
			ORDER BY CreatedDate DESC
		]){
			userToLatestCallLog.put(u.Id,u.);
		}
		
		for(Task t : tasks) {
			
			if(!userToLatestCallLog.containsKey(t.Assigned_To__c)) {
				system.debug(LoggingLevel.INFO,'Could not find')
			}
			
		}
		
		for(Call_Log__c cl : toUpdate) {
			
			if(!callLogIdToTask.containsKey(cl.Id)) {
				system.debug(LoggingLevel.WARN,'Could not find Call Log ' + cl.Id);
				continue;
			}
			
			Task logTask = callLogIdToTask.get(cl.Id);
			
			CallLogUtils.updateCallLogTimestamp(cl,logTask);
		}
		
		update toUpdate;
	}
	*/
	
	//TODO: This is not bulkified! It is difficult to bulkify finding
	//	the most recent record based on OwnerId
	public static void updateCallLogs(List<Task> tasks) {
		
		/* Potentially bulkified process
		List<Task> toProcess = new List<Task>();
		
		for(Task t : tasks) {
			if (isIncomingCallTask(t)) {
				toProcess.add(t);
			}
		}
		
		updateCallLogsFromTask(toProcess);
		*/
		
		if(tasks.size() > 1) {
			system.debug(LoggingLevel.WARN,'The "updateCallLogs" process is not yet bulkified. This call could result in error!');
		}
		
		//TODO: Temporary single process, could break for large uploads
		for(Task t : tasks) {
			if (isIncomingCallTask(t)) {
				updateCallLogFromTask(t);
			}
		}
	}
}