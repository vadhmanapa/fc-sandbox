public with sharing class AccountModel_CS extends AbstractSObjectModel_CS {

    public Account instance {
        get{
            if(instance == null){ instance = (Account)record; }
            return instance;
        }
        set;
    }

	public static List<AccountModel_CS> getModels(List<Account> records){
		List<AccountModel_CS> models = new List<AccountModel_CS>();
		
		for(Account a : records){
			models.add(new AccountModel_CS(a));
		}
		
		return models;
	}

	public AccountModel_CS(Account a){
		super(a);
	}
	
	public String getAddressCityStateZip(){
		String address = '';
		
		if(String.isNotBlank(instance.ShippingCity) && String.isNotBlank(instance.ShippingState)){
			address += instance.ShippingCity + ', ' + instance.ShippingState;
		}
		else if(String.isNotBlank(instance.ShippingCity)){
			address += instance.ShippingCity;
		}
		else if(String.isNotBlank(instance.ShippingState)){
			address += instance.ShippingState;
		}

		if(String.isNotBlank(instance.ShippingPostalCode)){
			address += ' ' + instance.ShippingPostalCode;
		}
		
		return address;
	}
}