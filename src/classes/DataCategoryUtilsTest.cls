@isTest
public class DataCategoryUtilsTest {

    //Eric Munson 2.17.15
    //
    //	Unfortunately there is no way to create test data for categories,
    //	therefore I only test if the returned values are nonzero, i.e.
    //	there are data categories in the system
    
    static testMethod void getDataCategoriesFromGroup() {
        
        List<Schema.DataCategory> tutorialCategories = 
            DataCategoryUtils.getDataCategoriesFromGroup('KnowledgeArticleVersion','Que');
        
        system.assert(tutorialCategories.size() > 0, 'No categories found for Tutorials.');
    }
    
    static testMethod void getAllDataCategoriesFromGroup() {
        
        Map<String,Schema.DataCategory> tutorialCategories = 
            DataCategoryUtils.getAllDataCategoriesFromGroup('KnowledgeArticleVersion','Que');
        
        system.assert(tutorialCategories.size() > 0, 'No categories found for Tutorials.');        
    }

    //Eric Munson 2.17.15
    //
	//	This should be adaquately tested by the above methods. Due to the limitations of
	//	creating Data Category test data I would not rely on this to be unit tested    
    static testMethod void getAllSubCategories() {
     	system.assert(true);   
    }    
}