@isTest
private class ProviderHomeDashboard_CS_Test {
	public static List<Order__c> orderList;
	public static Account provider;
	public static Contact user;
	public static PortalServices_CS ps;
	
    static testMethod void ProviderHomeDashboard_CS_Test() {
        //Initialize TestPlatform
        init();
        Test.setCurrentPage(Page.ProviderHomeDashboard_CS);
        TestServices_CS.login(user);
        
        ProviderHomeDashboard_CS cont = new ProviderHomeDashboard_CS();
        system.assertEquals(true, cont.authenticated);
        cont.init();
        orderList = cont.recentOrderList;
        system.assertEquals(1, cont.recentOrderSetCon.getResultSize());
        system.assertEquals(1, orderList.size());
        system.assertEquals('1', cont.pendingAcceptanceCountString);
        system.assertEquals('1', cont.inProgressCountString);
        system.assertEquals('1', cont.needsAttentionCountString);
        system.assertEquals('1', cont.cancelledCountString);
    }
    
    static testMethod void ProviderHomeDashboard_CS_newRecentOrderList(){
    	initNoOrders();
    	Test.setCurrentPage(Page.ProviderHomeDashboard_CS);
    	TestServices_CS.login(user);
    	
    	ProviderHomeDashboard_CS cont = new ProviderHomeDashboard_CS();
    	orderList = cont.recentOrderList;
    	system.assertEquals(0, orderList.size());
    }
    
    static testMethod void ProviderHomeDashboard_CS_notAuthenticated(){
    	initNoOrders();
    	Test.setCurrentPage(Page.ProviderHomeDashboard_CS);
    	
    	ProviderHomeDashboard_CS cont = new ProviderHomeDashboard_CS();
    	cont.init();
	}
	
	static testMethod void ProviderHomeDashboard_CS_wrongAccountType(){
		initNoOrders();
		Test.setCurrentPage(Page.ProviderHomeDashboard_CS);
		
		Account plan = TestDataFactory_CS.generatePlans('TestPlan', 1)[0];
		insert plan;
		
		user.Entity__c = plan.Id;
		update user;
		
		TestServices_CS.login(user);
		ProviderHomeDashboard_CS cont = new ProviderHomeDashboard_CS();
		cont.init();
	}
    
    static void init(){
    	TestDataFactory_CS tdf = new TestDataFactory_CS();
    	provider = tdf.providersToReturn[0];
    	user = tdf.providerContactsToReturn[0];
    	
    	user.Id_Key__c = PasswordServices_CS.hash(user.Id, user.Salt__c);
    	update user;
    	
    	orderList = TestDataFactory_CS.generateOrders(tdf.planPatientsToReturn[0].Id, provider.Id, 3);
    	orderList.add(tdf.ordersToReturn[0]);
    	orderList[0].Status__c = 'Needs Attention - Delivery Information Incomplete';
    	orderList[1].Status__c = OrderModel_CS.STATUS_PROVIDER_ACCEPTED;
    	orderList[2].Status__c = OrderModel_CS.STATUS_NEW;
    	orderList[3].Status__c = OrderModel_CS.STATUS_CANCELLED;
    	upsert orderList;
    }
    
    static void initNoOrders(){
    	List<Account> initAccts = TestDataFactory_CS.generateProviders('TestProv', 1);
    	provider = initAccts[0];
    	insert provider;
    	
    	user = TestDataFactory_CS.createProviderContacts('Admin', initAccts, 1)[0];
    }
}