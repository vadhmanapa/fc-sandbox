public class CaseActionTriggerHandler_IP 
{
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;
    
    public CaseActionTriggerHandler_IP(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
    }
        
    /*public void OnBeforeInsert(Case_Action__c[] newAction){
        
    }*/
    
    public void OnAfterInsert(Case_Action__c[] newActions){
        List<CaseComment> updateParentCaseComm = new List<CaseComment>();
        Set<Id> parentCaseIds = new Set<Id>();
        for(Case_Action__c c: newActions) {
            if(String.isNotEmpty(c.Case_Comment__c)) {
                updateParentCaseComm.add(new CaseComment(ParentId = c.Case__c, CommentBody = c.Case_Comment__c));
            }
            parentCaseIds.add(c.Case__c);
            
            /*if(!c.DoNotUpdate__c){

                Case_Reasons_Relationship__c crr = [SELECT Id, UpdatedCaseStatus__c FROM Case_Reasons_Relationship__c
                                                WHERE Action__c =: c.CaseAction__c AND ActionDetail__c =: c.CaseActionDetail__c LIMIT 1];

                Case cs = [SELECT Id, Status FROM Case WHERE Id =: c.Case__c LIMIT 1];
                cs.Status = crr.UpdatedCaseStatus__c;
                update cs;
            }*/
            
        }

        if(!updateParentCaseComm.isEmpty()) {
            insert updateParentCaseComm;
        }
    }
    
   /* @future public static void OnAfterInsertAsync(Set<ID> newActionIDs){
        
    }
    
    public void OnBeforeUpdate(Case_Action__c[] oldActions, Case_Action__c[] updatedActions, Map<ID, Case_Action__c> ActionMap){
        
    }
    
    public void OnAfterUpdate(Case_Action__c[] oldActions, Case_Action__c[] updatedActions, Map<ID, Case_Action__c> ActionMap){
        
    }
    
    @future public static void OnAfterUpdateAsync(Set<ID> updatedActionIDs){
        
    }
    
    public void OnBeforeDelete(Case_Action__c[] ActionsToDelete, Map<ID, Case_Action__c> ActionMap){
        
    }
    
    public void OnAfterDelete(Case_Action__c[] deletedActions, Map<ID, Case_Action__c> ActionMap){
        
    }
    
    @future public static void OnAfterDeleteAsync(Set<ID> deletedActionIDs){
        
    }
    
    public void OnUndelete(Case_Action__c[] restoredActions){
        
    }
    
    public boolean IsTriggerContext{
        get{ return m_isExecuting;}
    }
    
    public boolean IsVisualforcePageContext{
        get{ return !IsTriggerContext;}
    }
    
    public boolean IsWebServiceContext{
        get{ return !IsTriggerContext;}
    }
    
    public boolean IsExecuteAnonymousContext{
        get{ return !IsTriggerContext;}
    }*/
}