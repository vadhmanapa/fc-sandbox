@isTest
public class ClaimsAuditControllerTest_IP {
    static testMethod void calculateScore(){
        //create CWI
        ARAG__c testAR = new ARAG__c(Bill__c = '3242323');
        insert testAR;
	    //create Claims Work Audit
	    Claim_Work_Audit__c testAudit = new Claim_Work_Audit__c(Audited_Claims_Work_Item__c = testAR.Id,
                                                                Bill__c = testAR.Bill__c,
                                                                Was_the_correct_issue_identified__c = 'No');
        insert testAudit;
        
        Claim_Work_Audit__c checkScorePart1No = [Select Id, Score__c from Claim_Work_Audit__c where Id=: testAudit.Id];
        System.assertEquals(0, checkScorePart1No.Score__c, 'For Part1 No, match');
        
        testAudit.Was_the_correct_issue_identified__c = 'Yes';
        update testAudit;
        
        Claim_Work_Audit__c checkScorePart1Yes = [Select Id, Score__c from Claim_Work_Audit__c where Id=: testAudit.Id];
        System.assertEquals(25, checkScorePart1Yes.Score__c, 'For Part1 Yes, match');
        
        testAudit.Did_the_CRS_have_the_correct_resolution__c = 'No';
        update testAudit;
        
        Claim_Work_Audit__c checkScorePart2No = [Select Id, Score__c from Claim_Work_Audit__c where Id=: testAudit.Id];
        System.assertEquals(25, checkScorePart2No.Score__c, 'For Part2 No, match');
        
        testAudit.Did_the_CRS_have_the_correct_resolution__c = 'Yes';
        update testAudit;
        
        Claim_Work_Audit__c checkScorePart2Yes = [Select Id, Score__c from Claim_Work_Audit__c where Id=: testAudit.Id];
        System.assertEquals(50, checkScorePart2Yes.Score__c, 'For Part2 Yes, match');
        
        testAudit.Were_correct_actions_taken__c = 'No';
        update testAudit;
        
        Claim_Work_Audit__c checkScorePart3No = [Select Id, Score__c from Claim_Work_Audit__c where Id=: testAudit.Id];
        System.assertEquals(50, checkScorePart3No.Score__c, 'For Part3 No, match');
        
        testAudit.Were_correct_actions_taken__c = 'Yes';
        update testAudit;
        
        Claim_Work_Audit__c checkScorePart3Yes = [Select Id, Score__c from Claim_Work_Audit__c where Id=: testAudit.Id];
        System.assertEquals(75, checkScorePart3Yes.Score__c, 'For Part3 Yes, match');
        
        testAudit.Were_notes_entered__c = 'No';
        update testAudit;
        
        Claim_Work_Audit__c checkScorePart4No = [Select Id, Score__c from Claim_Work_Audit__c where Id=: testAudit.Id];
        System.assertEquals(75, checkScorePart4No.Score__c, 'For Part4 No, match');
        
        testAudit.Were_notes_entered__c = 'Yes';
        update testAudit;
        
        Claim_Work_Audit__c checkScorePart4Yes = [Select Id, Score__c from Claim_Work_Audit__c where Id=: testAudit.Id];
        System.assertEquals(100, checkScorePart4Yes.Score__c, 'For Part4 Yes, match');
        
    }
}