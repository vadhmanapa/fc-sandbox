/*===============================================================================================
 * Cloud Software LLC
 * Name: ProviderLocationHoursController
 * Description: Controller class for ProviderLocationHours page
 * Created Date: 11/25/2015
 * Created By: Kelly Sharp (Cloud Software)
 * 
 * 	Date Modified			Modified By			Description of the update
 *	11/25/2015				Kelly Sharp 		Created
 ==============================================================================================*/
public class ProviderLocationHoursController {

	public Provider_Location__c store {get;set;}
	public Decimal open_sunday_min {get;set;}
	public Decimal open_sunday_hrs {get;set;}
	public string open_sunday_ampm {get;set;}
	public Decimal closing_sunday_min {get;set;}
	public Decimal closing_sunday_hrs {get;set;}
	public string closing_sunday_ampm {get;set;}
	public List<SelectOption> ampm {get;set;}
	public List<SelectOption> min {get;set;}
	public List<SelectOption> hours {get;set;}
	public Decimal open_monday_min {get;set;}
	public Decimal open_monday_hrs {get;set;}
	public string open_monday_ampm {get;set;}
	public Decimal closing_monday_min {get;set;}
	public Decimal closing_monday_hrs {get;set;}
	public string closing_monday_ampm {get;set;}	
	public Decimal open_tuesday_min {get;set;}
	public Decimal open_tuesday_hrs {get;set;}
	public string open_tuesday_ampm {get;set;}
	public Decimal closing_tuesday_min {get;set;}
	public Decimal closing_tuesday_hrs {get;set;}
	public string closing_tuesday_ampm {get;set;}
	public Decimal open_wednesday_min {get;set;}
	public Decimal open_wednesday_hrs {get;set;}
	public string open_wednesday_ampm {get;set;}
	public Decimal closing_wednesday_min {get;set;}
	public Decimal closing_wednesday_hrs {get;set;}
	public string closing_wednesday_ampm {get;set;}
	public Decimal open_thursday_min {get;set;}
	public Decimal open_thursday_hrs {get;set;}
	public string open_thursday_ampm {get;set;}
	public Decimal closing_thursday_min {get;set;}
	public Decimal closing_thursday_hrs {get;set;}
	public string closing_thursday_ampm {get;set;}
	public Decimal open_friday_min {get;set;}
	public Decimal open_friday_hrs {get;set;}
	public string open_friday_ampm {get;set;}
	public Decimal closing_friday_min {get;set;}
	public Decimal closing_friday_hrs {get;set;}
	public string closing_friday_ampm {get;set;}
	public Decimal open_saturday_min {get;set;}
	public Decimal open_saturday_hrs {get;set;}
	public string open_saturday_ampm {get;set;}
	public Decimal closing_saturday_min {get;set;}
	public Decimal closing_saturday_hrs {get;set;}
	public string closing_saturday_ampm {get;set;}
	private ApexPages.StandardController controller {get; set;}
	public Boolean error {get;set;}


	public ProviderLocationHoursController(ApexPages.StandardController controller){

		// Use this line when moved to page 
		getProviderLocation(ApexPages.currentPage().getParameters().get('id'));
		System.debug(store);

		ampm = new List<SelectOption>();
		ampm.add(new SelectOption('am', 'AM'));
		ampm.add(new SelectOption('pm', 'PM'));
			
		min = new List<SelectOption>();
		min.add(new SelectOption('0.0', '00'));
		min.add(new SelectOption('0.5', '30'));		 
		
		hours = new List<SelectOption>();
		hours.add(new SelectOption('1', '1'));
		hours.add(new SelectOption('2', '2'));
		hours.add(new SelectOption('3', '3'));
		hours.add(new SelectOption('4', '4'));
		hours.add(new SelectOption('5', '5'));
		hours.add(new SelectOption('6', '6'));
		hours.add(new SelectOption('7', '7'));
		hours.add(new SelectOption('8', '8'));
		hours.add(new SelectOption('9', '9'));
		hours.add(new SelectOption('10', '10'));
		hours.add(new SelectOption('11', '11'));
		hours.add(new SelectOption('12', '12'));
		
				
		initTimes(store);

	}

	public void getProviderLocation(Id locationId){
		store = [
			SELECT
				Name,
				Name_DBA__c,
				Sunday_Opening_Time__c,
				Sunday_Closing_Time__c,
				Sunday_Is_Open__c,
				Sunday_Is_24_Hours__c,
				Monday_Opening_Time__c,
				Monday_Closing_Time__c,
				Monday_Is_Open__c,
				Monday_Is_24_Hours__c,
				Tuesday_Opening_Time__c,
				Tuesday_Closing_Time__c,
				Tuesday_Is_Open__c,
				Tuesday_Is_24_Hours__c,
				Wednesday_Opening_Time__c,
				Wednesday_Closing_Time__c,
				Wednesday_Is_Open__c,
				Wednesday_Is_24_Hours__c,
				Thursday_Opening_Time__c,
				Thursday_Closing_Time__c,
				Thursday_Is_Open__c,
				Thursday_Is_24_Hours__c,
				Friday_Opening_Time__c,
				Friday_Closing_Time__c,
				Friday_Is_Open__c,
				Friday_Is_24_Hours__c,
				Saturday_Opening_Time__c,
				Saturday_Closing_Time__c,
				Saturday_Is_Open__c,
				Saturday_Is_24_Hours__c
			FROM Provider_Location__c
			WHERE Id = :locationId
		];
	}

	public void initTimes(Provider_Location__c store){

		if(store.Sunday_Opening_Time__c != null){
			open_sunday_hrs = math.floor(store.Sunday_Opening_Time__c);
			open_sunday_min = math.abs(store.Sunday_Opening_Time__c - open_sunday_hrs);
			if(open_sunday_hrs > 12){
				open_sunday_hrs = open_sunday_hrs-12;
				open_sunday_ampm = 'pm';
			}else{
				open_sunday_ampm = 'am';
			}
		}else{
			open_sunday_hrs = 9;
			open_sunday_min = .0;
			open_sunday_ampm = 'am';
		}

		if(store.Sunday_closing_Time__c != null){
			closing_sunday_hrs = math.floor(store.Sunday_closing_Time__c);
			if(closing_sunday_hrs > 12){
				closing_sunday_min = math.abs(store.Sunday_closing_Time__c - closing_sunday_hrs);
				closing_sunday_hrs = closing_sunday_hrs-12;
				closing_sunday_ampm = 'pm';
			}else{
				closing_sunday_min = math.abs(store.Sunday_closing_Time__c - closing_sunday_hrs);
				closing_sunday_ampm = 'am';
			}
		}else{
			closing_sunday_hrs = 5;
			closing_sunday_min = .0;
			closing_sunday_ampm = 'pm';
		}

		//Monday

		if(store.Monday_Opening_Time__c != null){
			open_monday_hrs = math.floor(store.Monday_Opening_Time__c);
			open_monday_min = math.abs(store.Monday_Opening_Time__c - open_monday_hrs);
			if(open_monday_hrs > 12){
				open_monday_hrs = open_monday_hrs-12;
				open_monday_ampm = 'pm';
			}else{
				open_monday_ampm = 'am';
			}
		}else{
			open_monday_hrs = 9;
			open_monday_min = .0;
			open_monday_ampm = 'am';
		}

		if(store.Monday_closing_Time__c != null){
			closing_monday_hrs = math.floor(store.Monday_closing_Time__c);
			if(closing_monday_hrs > 12){
				closing_monday_min = math.abs(store.Monday_closing_Time__c - closing_monday_hrs);
				closing_monday_hrs = closing_monday_hrs-12;
				closing_monday_ampm = 'pm';
			}else{
				closing_monday_min = math.abs(store.Monday_closing_Time__c - closing_monday_hrs);
				closing_monday_ampm = 'am';
			}
		}else{
			closing_monday_hrs = 5;
			closing_monday_min = .0;
			closing_monday_ampm = 'pm';
		}

		//Tuesday

		if(store.Tuesday_Opening_Time__c != null){
			open_tuesday_hrs = math.floor(store.Tuesday_Opening_Time__c);
			open_tuesday_min = math.abs(store.Tuesday_Opening_Time__c - open_tuesday_hrs);
			if(open_tuesday_hrs > 12){
				open_tuesday_hrs = open_tuesday_hrs-12;
				open_tuesday_ampm = 'pm';
			}else{
				open_tuesday_ampm = 'am';
			}
		}else{
			open_tuesday_hrs = 9;
			open_tuesday_min = .0;
			open_tuesday_ampm = 'am';
		}

		if(store.Tuesday_closing_Time__c != null){
			closing_tuesday_hrs = math.floor(store.Tuesday_closing_Time__c);
			if(closing_tuesday_hrs > 12){
				closing_tuesday_min = math.abs(store.Tuesday_closing_Time__c - closing_tuesday_hrs);
				closing_tuesday_hrs = closing_tuesday_hrs-12;
				closing_tuesday_ampm = 'pm';
			}else{
				closing_tuesday_min = math.abs(store.Tuesday_closing_Time__c - closing_tuesday_hrs);
				closing_tuesday_ampm = 'am';
			}
		}else{
			closing_tuesday_hrs = 5;
			closing_tuesday_min = .0;
			closing_tuesday_ampm = 'pm';
		}

		//Wednesday

		if(store.Wednesday_Opening_Time__c != null){
			open_wednesday_hrs = math.floor(store.Wednesday_Opening_Time__c);
			open_wednesday_min = math.abs(store.Wednesday_Opening_Time__c - open_wednesday_hrs);
			if(open_wednesday_hrs > 12){
				open_wednesday_hrs = open_wednesday_hrs-12;
				open_wednesday_ampm = 'pm';
			}else{
				open_wednesday_ampm = 'am';
			}
		}else{
			open_wednesday_hrs = 9;
			open_wednesday_min = .0;
			open_wednesday_ampm = 'am';
		}

		if(store.Wednesday_closing_Time__c != null){
			closing_wednesday_hrs = math.floor(store.Wednesday_closing_Time__c);
			if(closing_wednesday_hrs > 12){
				closing_wednesday_min = math.abs(store.Wednesday_closing_Time__c - closing_wednesday_hrs);
				closing_wednesday_hrs = closing_wednesday_hrs-12;
				closing_wednesday_ampm = 'pm';
			}else{
				closing_wednesday_min = math.abs(store.Wednesday_closing_Time__c - closing_wednesday_hrs);
				closing_wednesday_ampm = 'am';
			}
		}else{
			closing_wednesday_hrs = 5;
			closing_wednesday_min = .0;
			closing_wednesday_ampm = 'pm';
		}

		//Thursday

		if(store.Thursday_Opening_Time__c != null){
			open_thursday_hrs = math.floor(store.Thursday_Opening_Time__c);
			open_thursday_min = math.abs(store.Thursday_Opening_Time__c - open_thursday_hrs);
			if(open_thursday_hrs > 12){
				open_thursday_hrs = open_thursday_hrs-12;
				open_thursday_ampm = 'pm';
			}else{
				open_thursday_ampm = 'am';
			}
		}else{
			open_thursday_hrs = 9;
			open_thursday_min = .0;
			open_thursday_ampm = 'am';
		}

		if(store.Thursday_closing_Time__c != null){
			closing_thursday_hrs = math.floor(store.Thursday_closing_Time__c);
			if(closing_thursday_hrs > 12){
				closing_thursday_min = math.abs(store.Thursday_closing_Time__c - closing_thursday_hrs);
				closing_thursday_hrs = closing_thursday_hrs-12;
				closing_thursday_ampm = 'pm';
			}else{
				closing_thursday_min = math.abs(store.Thursday_closing_Time__c - closing_thursday_hrs);
				closing_thursday_ampm = 'am';
			}
		}else{
			closing_thursday_hrs = 5;
			closing_thursday_min = .0;
			closing_thursday_ampm = 'pm';
		}

		//Friday

		if(store.Friday_Opening_Time__c != null){
			open_friday_hrs = math.floor(store.Friday_Opening_Time__c);
			open_friday_min = math.abs(store.Friday_Opening_Time__c - open_friday_hrs);
			if(open_friday_hrs > 12){
				open_friday_hrs = open_friday_hrs-12;
				open_friday_ampm = 'pm';
			}else{
				open_friday_ampm = 'am';
			}
		}else{
			open_friday_hrs = 9;
			open_friday_min = .0;
			open_friday_ampm = 'am';
		}

		if(store.Friday_closing_Time__c != null){
			closing_friday_hrs = math.floor(store.Friday_closing_Time__c);
			if(closing_friday_hrs > 12){
				closing_friday_min = math.abs(store.Friday_closing_Time__c - closing_friday_hrs);
				closing_friday_hrs = closing_friday_hrs-12;
				closing_friday_ampm = 'pm';
			}else{
				closing_friday_min = math.abs(store.Friday_closing_Time__c - closing_friday_hrs);
				closing_friday_ampm = 'am';
			}
		}else{
			closing_friday_hrs = 5;
			closing_friday_min = .0;
			closing_friday_ampm = 'pm';
		}

		//Saturday

		if(store.Saturday_Opening_Time__c != null){
			open_saturday_hrs = math.floor(store.Saturday_Opening_Time__c);
			open_saturday_min = math.abs(store.Saturday_Opening_Time__c - open_saturday_hrs);
			if(open_saturday_hrs > 12){
				open_saturday_hrs = open_saturday_hrs-12;
				open_saturday_ampm = 'pm';
			}else{
				open_saturday_ampm = 'am';
			}
		}else{
			open_saturday_hrs = 9;
			open_saturday_min = .0;
			open_saturday_ampm = 'am';
		}

		if(store.Saturday_closing_Time__c != null){
			closing_saturday_hrs = math.floor(store.Saturday_closing_Time__c);
			if(closing_saturday_hrs > 12){
				closing_saturday_min = math.abs(store.Saturday_closing_Time__c - closing_saturday_hrs);
				closing_saturday_hrs = closing_saturday_hrs-12;
				closing_saturday_ampm = 'pm';
			}else{
				closing_saturday_min = math.abs(store.Saturday_closing_Time__c - closing_saturday_hrs);
				closing_saturday_ampm = 'am';
			}
		}else{
			closing_saturday_hrs = 5;
			closing_saturday_min = .0;
			closing_saturday_ampm = 'pm';
		}
	}

	/**
	* @description Recalculates time fields from "time" format to "decimal"
	*/
	public void timeToDec(){
		error = false;
		//Sunday
		//Open Hours
		if(open_sunday_ampm == 'pm'){
			open_sunday_hrs = open_sunday_hrs +12;
		}
		store.Sunday_Opening_Time__c = open_sunday_hrs + open_sunday_min;
			
		if(open_sunday_hrs > 12){
			open_sunday_hrs = open_sunday_hrs - 12; //put back for display
		}

		//Closed Hours
		if(closing_sunday_ampm == 'pm'){
			closing_sunday_hrs = closing_sunday_hrs +12;
		}
		store.Sunday_Closing_Time__c = closing_sunday_hrs + closing_sunday_min;
			
		if(closing_sunday_hrs > 12){
			closing_sunday_hrs = closing_sunday_hrs - 12; //put back for display
		}
		//Validate open time is before close time 
		System.debug(store.Sunday_Closing_Time__c);
		System.debug(store.Sunday_Opening_Time__c);
		if(store.Sunday_Closing_Time__c < store.Sunday_Opening_Time__c){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Sunday Closing Time Can Not Be Before Opening'); 
			ApexPages.addMessage(myMsg);
			System.debug(myMsg);
			error = true;
		}

		//Monday

				//Open Hours
		if(open_monday_ampm == 'pm'){
			open_monday_hrs = open_monday_hrs +12;
		}
		store.Monday_Opening_Time__c = open_monday_hrs + open_monday_min;
			
		if(open_monday_hrs > 12){
			open_monday_hrs = open_monday_hrs - 12; //put back for display
		}

		//Closed Hours
		if(closing_monday_ampm == 'pm'){
			closing_monday_hrs = closing_monday_hrs +12;
		}
		store.Monday_Closing_Time__c = closing_monday_hrs + closing_monday_min;
			
		if(closing_monday_hrs > 12){
			closing_monday_hrs = closing_monday_hrs - 12; //put back for display
		}
		//Validate open time is before close time
		System.debug(store.Monday_Closing_Time__c);
		System.debug(store.Monday_Opening_Time__c);
		if(store.Monday_Closing_Time__c < store.Monday_Opening_Time__c){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Monday Closing Time Can Not Be Before Opening'); 
			ApexPages.addMessage(myMsg);
			System.debug(myMsg);
			error = true;
		}

		//Tuesday

		//Open Hours
		if(open_tuesday_ampm == 'pm'){
			open_tuesday_hrs = open_tuesday_hrs +12;
		}
		store.Tuesday_Opening_Time__c = open_tuesday_hrs + open_tuesday_min;
			
		if(open_tuesday_hrs > 12){
			open_tuesday_hrs = open_tuesday_hrs - 12; //put back for display
		}

		//Closed Hours
		if(closing_tuesday_ampm == 'pm'){
			closing_tuesday_hrs = closing_tuesday_hrs +12;
		}
		store.Tuesday_Closing_Time__c = closing_tuesday_hrs + closing_tuesday_min;
			
		if(closing_tuesday_hrs > 12){
			closing_tuesday_hrs = closing_tuesday_hrs - 12; //put back for display
		}
		//Validate open time is before close time 
		System.debug(store.Tuesday_Closing_Time__c);
		System.debug(store.Tuesday_Opening_Time__c);
		if(store.Tuesday_Closing_Time__c < store.Tuesday_Opening_Time__c){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Tuesday Closing Time Can Not Be Before Opening'); 
			ApexPages.addMessage(myMsg);
			System.debug(myMsg);
			error = true;
		}

		//Wednesday

		//Open Hours
		if(open_wednesday_ampm == 'pm'){
			open_wednesday_hrs = open_wednesday_hrs +12;
		}
		store.Wednesday_Opening_Time__c = open_wednesday_hrs + open_wednesday_min;
			
		if(open_wednesday_hrs > 12){
			open_wednesday_hrs = open_wednesday_hrs - 12; //put back for display
		}

		//Closed Hours
		if(closing_wednesday_ampm == 'pm'){
			closing_wednesday_hrs = closing_wednesday_hrs +12;
		}
		store.Wednesday_Closing_Time__c = closing_wednesday_hrs + closing_wednesday_min;
			
		if(closing_wednesday_hrs > 12){
			closing_wednesday_hrs = closing_wednesday_hrs - 12; //put back for display
		}
		//Validate open time is before close time 
		System.debug(store.Wednesday_Closing_Time__c);
		System.debug(store.Wednesday_Opening_Time__c);
		if(store.Wednesday_Closing_Time__c < store.Wednesday_Opening_Time__c){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Wednesday Closing Time Can Not Be Before Opening'); 
			ApexPages.addMessage(myMsg);
			System.debug(myMsg);
			error = true;
		}


		//Thursday

		//Open Hours
		if(open_thursday_ampm == 'pm'){
			open_thursday_hrs = open_thursday_hrs +12;
		}
		store.Thursday_Opening_Time__c = open_thursday_hrs + open_thursday_min;
			
		if(open_thursday_hrs > 12){
			open_thursday_hrs = open_thursday_hrs - 12; //put back for display
		}

		//Closed Hours
		if(closing_thursday_ampm == 'pm'){
			closing_thursday_hrs = closing_thursday_hrs +12;
		}
		store.Thursday_Closing_Time__c = closing_thursday_hrs + closing_thursday_min;
			
		if(closing_thursday_hrs > 12){
			closing_thursday_hrs = closing_thursday_hrs - 12; //put back for display
		}
		//Validate open time is before close time 
		System.debug(store.Thursday_Closing_Time__c);
		System.debug(store.Thursday_Opening_Time__c);
		if(store.Thursday_Closing_Time__c < store.Thursday_Opening_Time__c){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Thursday Closing Time Can Not Be Before Opening'); 
			ApexPages.addMessage(myMsg);
			System.debug(myMsg);
			error = true;
		}

		//Friday

		//Open Hours
		if(open_friday_ampm == 'pm'){
			open_friday_hrs = open_friday_hrs +12;
		}
		store.Friday_Opening_Time__c = open_friday_hrs + open_friday_min;
			
		if(open_friday_hrs > 12){
			open_friday_hrs = open_friday_hrs - 12; //put back for display
		}

		//Closed Hours
		if(closing_friday_ampm == 'pm'){
			closing_friday_hrs = closing_friday_hrs +12;
		}
		store.Friday_Closing_Time__c = closing_friday_hrs + closing_friday_min;
			
		if(closing_friday_hrs > 12){
			closing_friday_hrs = closing_friday_hrs - 12; //put back for display
		}
		//Validate open time is before close time 
		System.debug(store.Friday_Closing_Time__c);
		System.debug(store.Friday_Opening_Time__c);
		if(store.Friday_Closing_Time__c < store.Friday_Opening_Time__c){
			ApexPages.Message eMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Friday Closing Time Can Not Be Before Opening'); 
			ApexPages.addMessage(eMsg);
			System.debug(eMsg);
			error = true;
		}

		//Saturday

		//Open Hours
		if(open_saturday_ampm == 'pm'){
			open_saturday_hrs = open_saturday_hrs +12;
		}
		store.Saturday_Opening_Time__c = open_saturday_hrs + open_saturday_min;
			
		if(open_saturday_hrs > 12){
			open_saturday_hrs = open_saturday_hrs - 12; //put back for display
		}

		//Closed Hours
		if(closing_saturday_ampm == 'pm'){
			closing_saturday_hrs = closing_saturday_hrs +12;
		}
		store.Saturday_Closing_Time__c = closing_saturday_hrs + closing_saturday_min;
			
		if(closing_saturday_hrs > 12){
			closing_saturday_hrs = closing_saturday_hrs - 12; //put back for display
		}
		//Validate open time is before close time 
		System.debug(store.Saturday_Closing_Time__c);
		System.debug(store.Saturday_Opening_Time__c);
		if(store.Saturday_Closing_Time__c < store.Saturday_Opening_Time__c){
			error = true;
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Saturday Closing Time Can Not Be Before Opening'); 
			ApexPages.addMessage(myMsg);
			System.debug(myMsg);
			error = true;
			
		}
	}

	public void updateLocation(){
		try {
			update store;
		} catch (Exception e) {
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()); 
			ApexPages.addMessage(myMsg);
		}
	}
}