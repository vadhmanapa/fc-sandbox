@isTest
private class CaseReasonRelationshipHelperTest_IP {
    public static TestCaseDataFactory_IP generateNewData;
    private static User testUser;

    @isTest static void testGetCaseReasonsFromCallerType(){
        generateNewData = new TestCaseDataFactory_IP();
        generateNewData.initTestData();
        generateNewData.generateReasonsAndActions();
        List<SelectOption> getReasons = new List<SelectOption>();

        Test.startTest();
        getReasons = CaseReasonRelationshipHelper_IP.caseReasonPicklist('Patient', 'Customer Service Call');
        Test.stopTest();

        System.assertNotEquals(1, getReasons.size(), 'The picklist returned only all option');
    }

    @isTest static void testGetCaseReasonsFromEmptyCallerType(){
        generateNewData = new TestCaseDataFactory_IP();
        generateNewData.initTestData();
        generateNewData.generateReasonsAndActions();
        List<SelectOption> getReasons = new List<SelectOption>();

        Test.startTest();
        getReasons = CaseReasonRelationshipHelper_IP.caseReasonPicklist('', '');
        Test.stopTest();

        System.assertEquals(1, getReasons.size(), 'The picklist returned only None option');
    }

    @isTest static void testGetCaseReasonDetailsFromEmptyReason(){
        generateNewData = new TestCaseDataFactory_IP();
        generateNewData.initTestData();
        generateNewData.generateReasonsAndActions();
        List<SelectOption> getReasonDetails = new List<SelectOption>();

        Test.startTest();
        getReasonDetails = CaseReasonRelationshipHelper_IP.caseReasonDetailsPicklist('');
        Test.stopTest();

        System.assertEquals(1, getReasonDetails.size(), 'The picklist returned only None option');
    }

    @isTest static void testGetCaseReasonDetailsFromReason(){
        generateNewData = new TestCaseDataFactory_IP();
        generateNewData.initTestData();
        generateNewData.generateReasonsAndActions();
        List<SelectOption> getReasonDetails = new List<SelectOption>();

        Test.startTest();
        getReasonDetails = CaseReasonRelationshipHelper_IP.caseReasonDetailsPicklist(TestCaseDataFactory_IP.reasons[0].Id);
        Test.stopTest();

        System.assertNotEquals(1, getReasonDetails.size(), 'The picklist returned all option');
    }
    
    @isTest static void testGetCaseReasonValueFromId(){
        generateNewData = new TestCaseDataFactory_IP();
        generateNewData.initTestData();
        generateNewData.generateReasonsAndActions();
        CaseReason__c testReason = new CaseReason__c();

        Test.startTest();
        testReason = CaseReasonRelationshipHelper_IP.getCaseReasonValue(TestCaseDataFactory_IP.reasons[0].Id);
        Test.stopTest();

        System.assertNotEquals(null, testReason, 'Case Reason was returned');
    }

     @isTest static void testGetCaseActionFromEmptyReasonDetail(){
        generateNewData = new TestCaseDataFactory_IP();
        generateNewData.initTestData();
        generateNewData.generateReasonsAndActions();
        List<SelectOption> getAction = new List<SelectOption>();

        Test.startTest();
        getAction = CaseReasonRelationshipHelper_IP.actionDetailsPicklist('','');
        Test.stopTest();

        System.assertEquals(1, getAction.size(), 'The picklist returned only None option');
    }

    @isTest static void testGetCaseActionFromReasonDetail(){
        generateNewData = new TestCaseDataFactory_IP();
        generateNewData.initTestData();
        generateNewData.generateReasonsAndActions();
        List<SelectOption> getAction = new List<SelectOption>();

        Test.startTest();
        getAction = CaseReasonRelationshipHelper_IP.actionDetailsPicklist(TestCaseDataFactory_IP.reasonDetail[0].Id, 'Tier 1');
        Test.stopTest();

        System.assertNotEquals(1, getAction.size(), 'The picklist returned only None option');
    }
    
    @isTest static void testGetCaseReasonDetailValueFromId(){
        generateNewData = new TestCaseDataFactory_IP();
        generateNewData.initTestData();
        generateNewData.generateReasonsAndActions();
        CaseReasonDetail__c testReasonDetail = new CaseReasonDetail__c();

        Test.startTest();
        testReasonDetail = CaseReasonRelationshipHelper_IP.getCaseReasonDetailValue(TestCaseDataFactory_IP.reasonDetail[0].Id);
        Test.stopTest();

        System.assertNotEquals(null, testReasonDetail, 'Case Reason Detail was returned');
    }

    @isTest static void testGetCaseActionDetailFromEmptyAction(){
        generateNewData = new TestCaseDataFactory_IP();
        generateNewData.initTestData();
        generateNewData.generateReasonsAndActions();
        List<SelectOption> getActionDetails = new List<SelectOption>();

        Test.startTest();
        getActionDetails = CaseReasonRelationshipHelper_IP.actionDetailsPicklist(TestCaseDataFactory_IP.reasonDetail[0].Id, 'Tier 1');
        Test.stopTest();

        System.assertNotEquals(1, getActionDetails.size(), 'The picklist returned only None option');
    }
    
    @isTest static void testGetCaseActionDetailValueFromId(){
        generateNewData = new TestCaseDataFactory_IP();
        generateNewData.initTestData();
        generateNewData.generateReasonsAndActions();
        ActionDetail__c testAction = new ActionDetail__c();

        Test.startTest();
        testAction = CaseReasonRelationshipHelper_IP.getActionValues(TestCaseDataFactory_IP.actionAndDetails[0].Id);
        Test.stopTest();

        System.assertNotEquals(null, testAction , 'Case Action Detail was returned');
    }

     @isTest static void testGetCaseActionDetailFromAction(){
        generateNewData = new TestCaseDataFactory_IP();
        generateNewData.initTestData();
        generateNewData.generateReasonsAndActions();
        List<SelectOption> getActionDetails = new List<SelectOption>();
        ActionDetail__c acd = [SELECT Id, Name, Action__c FROM ActionDetail__c WHERE Id=: TestCaseDataFactory_IP.actionAndDetails[0].Id LIMIT 1];

        Test.startTest();
        getActionDetails = CaseReasonRelationshipHelper_IP.actionDetailsPicklist(TestCaseDataFactory_IP.reasonDetail[0].Id, acd.Action__c, 'Tier 1');
        Test.stopTest();

        System.assertNotEquals(1, getActionDetails.size(), 'The picklist returned only None option');
    }
    
    @isTest static void testGetCaseActionDetailFromActionTier2(){
        generateNewData = new TestCaseDataFactory_IP();
        generateNewData.initTestData();
        generateNewData.generateReasonsAndActions();
        List<SelectOption> getActionDetails = new List<SelectOption>();
        ActionDetail__c acd = [SELECT Id, Name, Action__c FROM ActionDetail__c WHERE Id=: TestCaseDataFactory_IP.actionAndDetails[0].Id LIMIT 1];

        Test.startTest();
        getActionDetails = CaseReasonRelationshipHelper_IP.actionDetailsPicklist(TestCaseDataFactory_IP.reasonDetail[0].Id, acd.Action__c, 'Tier 2');
        Test.stopTest();

        System.assertNotEquals(1, getActionDetails.size(), 'The picklist returned only None option');
    }
    
     @isTest static void testGetCaseActionDetailFromActionTier3(){
        generateNewData = new TestCaseDataFactory_IP();
        generateNewData.initTestData();
        generateNewData.generateReasonsAndActions();
        List<SelectOption> getActionDetails = new List<SelectOption>();
        ActionDetail__c acd = [SELECT Id, Name, Action__c FROM ActionDetail__c WHERE Id=: TestCaseDataFactory_IP.actionAndDetails[0].Id LIMIT 1];

        Test.startTest();
        getActionDetails = CaseReasonRelationshipHelper_IP.actionDetailsPicklist(TestCaseDataFactory_IP.reasonDetail[0].Id, acd.Action__c, 'Tier 3');
        Test.stopTest();

        System.assertNotEquals(1, getActionDetails.size(), 'The picklist returned only None option');
    }
}