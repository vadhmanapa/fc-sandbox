@isTest
private class CIQWorkflowTest {

  /********************** 
    NOTE: DEPRECATED BY VENKAT ADHMANAPA 
    MOVING THE CODE TO SALESFORCE NATIVE CASE FUNCTIONALITY
  **********************/
  
    /*static testMethod void validateCIQWorkflow(){
        
        // create test user
        string randomName = string.valueof(Datetime.now()).replace('-','').replace(':','').replace(' ','');
        String userName = randomName + 'testadfhcheck@test.com';
        Profile p = [Select id from Profile where Name ='Claims w Service Cloud'];
        User testUser = new User();
        testUser.FirstName = 'tesdfst';
        testUser.LastName = 'cheafgck';
        testUser.Username = userName;
        testUser.Email = randomName+userName;
        testUser.Alias = 'tefadhst';
        testUser.Team__c = 'Fadoo';
        testUser.CommunityNickname = 'testforemail';
        testUser.TimeZoneSidKey = 'America/Los_Angeles';
        testUser.EmailEncodingKey = 'ISO-8859-1';
        testUser.ProfileId = p.Id;
        testUser.LanguageLocaleKey = 'en_US';
        testUser.LocaleSidKey = 'en_US';
        testUser.Assign_Email__c = true;
        testUser.IsActive = true;
        insert testUser;
        String testUserId = testUser.Id;
        
        // create a test account - can be the provider or payor
        Account testAccount = new Account();
        testAccount.Name = 'Affinity';
        testAccount.OwnerId = testUser.ID;
        insert testAccount;
        
        Email_Log__c testlog7 = new Email_Log__c(From_Email_Address__c = 'test@test.com',To_Email_Address__c = 'claims@accessintegra.com', 
                                                 All_To_Addresses__c = 'claims@accessintegra.com' , CC_Addresses__c = 'claims@accessintegra.com', 
                                                 Email_Subject__c = 'test subject', Email_Content__c = 'test content'); 
        String testId7 = testlog7.Id;
        insert testlog7;
        
        //List<Email_Log__c> checkLog7 = [SELECT Id, Assigned_To__c FROM Email_Log__c WHERE Id = :testId7  LIMIT 1];
        
        Claims_Inquiry__c ci = new Claims_Inquiry__C(Provider__c=testAccount.Id,
                                                     Inquiry_via__c = 'Email',
                                                     Inquiry_Reason__c = '2nd/3rd insurance billing',
                                                     Inquiry_Sub_Reason__c = 'EOB Unavailable',
                                                     Action_Taken__C= 'Needs an outbound call to the payor to obtain EOB', 
                                                     Health_Plan__c=testAccount.Id, Internal_Inquiry_Details__c = 'foo',
                                                     Email_Log__c = testlog7.id, Bill_Number__c = '12345678910',
                                                     Status_of_Inquiry__c = 'Open');
        insert ci;
        
        Claims_Inquiry__c testCI = [SELECT Id, Assigned_To_Name__c, Date_Time_of_Inquiry__c FROM Claims_Inquiry__c WHERE id=:ci.Id];
        system.assertEquals('Evie Contaldo', testCI.Assigned_To_name__c, 'Assigned to Evie Contaldo');
        
       //--------------------------------------------------
        testAccount.Name = 'CenterLight Healthcare';
        update testAccount;
        ci = new Claims_Inquiry__C(Provider__c=testAccount.Id, Inquiry_via__c = 'Email',
                                   Inquiry_Reason__c = '2nd/3rd insurance billing',
                                   Inquiry_Sub_Reason__c = 'EOB Unavailable',
                                   Action_Taken__C= 'Needs an outbound call to the payor to obtain EOB', 
                                   Health_Plan__c=testAccount.Id, Email_Log__c = testlog7.id, Internal_Inquiry_Details__c = 'foo',  
                                   Bill_Number__c = '12345678910', Status_of_Inquiry__c = 'Open');
        insert ci;
        
        testCI = [SELECT Id, Assigned_To_Name__c, Date_Time_of_Inquiry__c FROM Claims_Inquiry__c WHERE id=:ci.Id];
      	system.assertEquals('Evie Contaldo', testCI.Assigned_To_name__c, 'Assigned to Evie'); 
        //-------------------------------------------------------
                testAccount.Name = 'Empire BlueCross BlueShield HealthPlus';
        update testAccount;
        ci = new Claims_Inquiry__C(Provider__c=testAccount.Id, Inquiry_via__c = 'Email',
                                   Inquiry_Reason__c = '2nd/3rd insurance billing',
                                   Inquiry_Sub_Reason__c = 'EOB Unavailable',
                                   Action_Taken__C= 'Needs an outbound call to the payor to obtain EOB', 
                                   Health_Plan__c=testAccount.Id, Email_Log__c = testlog7.id, Internal_Inquiry_Details__c = 'foo', 
                                   Bill_Number__c = '12345678910', Status_of_Inquiry__c = 'Open');
        insert ci;
        
        testCI = [SELECT Id, Assigned_To_Name__c, Date_Time_of_Inquiry__c FROM Claims_Inquiry__c WHERE id=:ci.Id];
        system.assertEquals('Evie Contaldo', testCI.Assigned_To_name__c, 'Assigned to Evie'); 
        
        //-------------------------------------------------------
        testAccount.Name = 'Healthfirst';
        update testAccount;
        ci = new Claims_Inquiry__C(Provider__c=testAccount.Id, Inquiry_via__c = 'Email',
                                   Inquiry_Reason__c = '2nd/3rd insurance billing',
                                   Inquiry_Sub_Reason__c = 'EOB Unavailable',
                                   Action_Taken__C= 'Needs an outbound call to the payor to obtain EOB', 
                                   Health_Plan__c=testAccount.Id, Email_Log__c = testlog7.id, Internal_Inquiry_Details__c = 'foo', 
                                   Bill_Number__c = '12345678910', Status_of_Inquiry__c = 'Open');
        insert ci;
        
        testCI = [SELECT Id, Assigned_To_Name__c, Date_Time_of_Inquiry__c FROM Claims_Inquiry__c WHERE id=:ci.Id];
		  system.assertEquals('Evie Contaldo', testCI.Assigned_To_name__c, 'Assigned to Evie Contaldo');
        
        //-------------------------------------------------------
        system.assertEquals(testlog7.CreatedDate, ci.Date_Time_of_Inquiry__c, 'Inquiry datetime carried over from email');
        //-------------------------------------------------------
        ci = new Claims_Inquiry__C(Provider__c=testAccount.Id,
                                   Inquiry_via__c = 'Call',
                                   Inquiry_Reason__c = 'Transfer to CS');
        insert ci;
        
        testCI = [SELECT Id, Status_of_Inquiry__c FROM Claims_Inquiry__c WHERE id=:ci.Id];
        system.assertEquals('Closed', testCI.Status_of_Inquiry__c, 'Closed');
        
        //-------------------------------------------------------
        ci = new Claims_Inquiry__C(Provider__c=testAccount.Id, Inquiry_via__c = 'Email',
                                   Inquiry_Reason__c = '2nd/3rd insurance billing',
                                   Inquiry_Sub_Reason__c = 'EOB Unavailable',
                                   Action_Taken__C= 'Needs an outbound call to the payor to obtain EOB', 
                                   Health_Plan__c=testAccount.Id, Email_Log__c = testlog7.id, Internal_Inquiry_Details__c = 'foo', 
                                   Bill_Number__c = '12345678910', Status_of_Inquiry__c = 'Open');
        insert ci;
        ci.Case_Created__c = true;
        update ci;
        testCI = [SELECT Id, Related_Case__c FROM Claims_Inquiry__c WHERE id=:ci.Id];
        system.assertNotEquals(NULL, testCI.Related_Case__c, 'Created a case');
        
        //-------------------------------------------------------
        ci = new Claims_Inquiry__C(Provider__c=testAccount.Id, Inquiry_via__c = 'Email',
                                   Inquiry_Reason__c = 'Payor related issues',
                                   Inquiry_Sub_Reason__c = 'Amerigroup recoupments',
                                   Action_Taken__C= 'Explained the issue', 
                                   Health_Plan__c=testAccount.Id, Email_Log__c = testlog7.id, Internal_Inquiry_Details__c = 'foo', 
                                   Bill_Number__c = '12345678910', Status_of_Inquiry__c = 'Open');
        insert ci;
        testCI = [SELECT Id, Team_Responsible__c FROM Claims_Inquiry__c WHERE id=:ci.Id];
        system.assertNotEquals(Null, testCI.Team_Responsible__c, 'ND Responsible');
        
        //-------------------------------------------------------
        ci = new Claims_Inquiry__C(Provider__c=testAccount.Id, Inquiry_via__c = 'Email',
                                   Inquiry_Reason__c = 'Disbursement',
                                   Inquiry_Sub_Reason__c = 'Disbursement status',
                                   Action_Taken__C= 'Needs an expert as payment posted in Dr.com but not in Que', 
                                   Health_Plan__c=testAccount.Id, Email_Log__c = testlog7.id, Internal_Inquiry_Details__c = 'foo', 
                                   Bill_Number__c = '12345678910', Status_of_Inquiry__c = 'Open');
        insert ci;
        testCI = [SELECT Id, Team_Responsible__c, Assigned_to_name__c FROM Claims_Inquiry__c WHERE id=:ci.Id];
        system.assertEquals('Payment & Denial Resolution', testCI.Team_Responsible__c, 'PADR Responsible');
        system.assertEquals('Jerone Kadnar', testCI.Assigned_To_name__c, 'Assigned to Jerone Kadnar');
        
        //-------------------------------------------------------
        ci = new Claims_Inquiry__C(Provider__c=testAccount.Id, Inquiry_via__c = 'Email',
                                   Inquiry_Reason__c = 'Disbursement',
                                   Inquiry_Sub_Reason__c = 'Disbursement status',
                                   Action_Taken__C= 'Needs an expert as claims on EOP posted with $0', 
                                   Health_Plan__c=testAccount.Id, Email_Log__c = testlog7.id, Internal_Inquiry_Details__c = 'foo', 
                                   Bill_Number__c = '12345678910', Status_of_Inquiry__c = 'Open');
        insert ci;
        testCI = [SELECT Id, Team_Responsible__c, Assigned_to_name__c FROM Claims_Inquiry__c WHERE id=:ci.Id];
        system.assertEquals('Stacey Rolon', testCI.Assigned_To_name__c, 'Assigned to Stacey Rolon');
                        
        //-------------------------------------------------------
        ci = new Claims_Inquiry__C(Provider__c=testAccount.Id, Inquiry_via__c = 'Email',
                                   Inquiry_Reason__c = 'Claim status available',
                                   Inquiry_Sub_Reason__c = 'Claim paid correctly',
                                   Action_Taken__C= 'Explained claim details to provider', 
                                   Health_Plan__c=testAccount.Id, Email_Log__c = testlog7.id, Internal_Inquiry_Details__c = 'foo', 
                                   Bill_Number__c = '12345678910', Status_of_Inquiry__c = 'Open',
                                   Resolved_by__c = testUser.Id, Resolved_Date_time__c = system.now());
        insert ci;
        testCI = [SELECT Id, Team_Responsible__c, Assigned_to_name__c FROM Claims_Inquiry__c WHERE id=:ci.Id];
        system.assertNotEquals(Null, testCI.Assigned_To_name__c, 'Assigned to self');
        
        //-------------------------------------------------------
        ci = new Claims_Inquiry__C(Provider__c=testAccount.Id, Inquiry_via__c = 'Call',
                                   Issue_Summary__c = 'foo bar',
                                   Health_Plan__c=testAccount.Id, Email_Log__c = testlog7.id, Internal_Inquiry_Details__c = 'foo', 
                                   Bill_Number__c = '12345678910', Status_of_Inquiry__c = 'Open');
        insert ci;
        //testCI = [SELECT Id, Team_Responsible__c, Assigned_to_name__c FROM Claims_Inquiry__c WHERE id=:ci.Id];
        //system.assertNotEquals(Null, testCI.Assigned_To_name__c, 'Assigned to self');
    }*/
}