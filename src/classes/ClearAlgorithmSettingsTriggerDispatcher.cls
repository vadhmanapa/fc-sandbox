/**
 *  @Description 
 *  @Author Cloud Software LLC, 
 *  Revision History: 
 *
 *
 */
public without sharing class ClearAlgorithmSettingsTriggerDispatcher{
	public CLearAlgorithmSettingsTriggerDispatcher(Map<Id,CLear_Algorithm_Settings__c> oldMap, Map<Id,CLear_Algorithm_Settings__c> newMap, List<CLear_Algorithm_Settings__c> triggerOld, List<CLear_Algorithm_Settings__c> triggerNew, Boolean isInsert, Boolean isUpdate, Boolean isDelete, Boolean isUndelete, Boolean isBefore, Boolean isAfter){
	
		if(isBefore) {
			if(isInsert) { doBeforeInsert(triggerNew); }
			else if(isUpdate) { doBeforeUpdate(oldMap, newMap, triggerOld, triggerNew); }
			//else if(isDelete) { doBeforeDelete(oldMap, triggerOld); }          
		}else if(isAfter) {
			//if(isInsert) { doAfterInsert(newMap, triggerNew); }
			//else if(isUpdate) {doAfterUpdate(oldMap, newMap, triggerOld, triggerNew); }
			//else if(isDelete) { doAfterDelete(oldMap, triggerOld); }
			//else if(isUndelete){ doAfterUnDelete(newMap, triggerNew); }       
		}
	}

	public void doBeforeInsert(List<CLear_Algorithm_Settings__c> triggerNew){
		// Check that a new record is of the correct status (Draft) and not active.
		for (Clear_Algorithm_Settings__c cas : triggerNew) {
			ClearAlgorithmSettingsServices.blockActivationForNewRecord(cas);
			ClearAlgorithmSettingsServices.blockNonDraftStatusForNewRecord(cas);
			ClearAlgorithmSettingsServices.blockDateFieldsForNewRecords(cas);
		}
	}

	public void doBeforeUpdate(Map<Id,CLear_Algorithm_Settings__c> oldMap, Map<Id,CLear_Algorithm_Settings__c> newMap, List<CLear_Algorithm_Settings__c> triggerOld, List<CLear_Algorithm_Settings__c> triggerNew) {
		ClearAlgorithmSettingsServices.addReadPermissionsForActive(triggerNew);
		
		Boolean newActiveSettingExists = false;
		for (Id casId : oldMap.keySet()) {
			ClearAlgorithmSettingsServices.blockDateFieldUpdates(oldMap.get(casId), newMap.get(casId));
			ClearAlgorithmSettingsServices.blockActivationIfNotApproved(oldMap.get(casId), newMap.get(casId));
			ClearAlgorithmSettingsServices.blockEditForApproved(oldMap.get(casId), newMap.get(casId));
			ClearAlgorithmSettingsServices.blockActivationForFormerlyActiveRecord(oldMap.get(casId), newMap.get(casId));
			if (ClearAlgorithmSettingsServices.setActivationDateOnNewActiveRecord(oldMap.get(casId), newMap.get(casId))) {
				newActiveSettingExists = true;
				System.debug('New active setting was updated; will deactivate old settings');
			}
		}

		System.debug('Updating: ' + triggerNew);
		System.debug('newActiveSettingExists: ' + newActiveSettingExists);
		if (newActiveSettingExists) {
			ClearAlgorithmSettingsServices.deactivateOtherSettings();
		}
	}

	//public void doBeforeDelete(Map<Id,CLear_Algorithm_Settings__c> oldMap, List<CLear_Algorithm_Settings__c> triggerOld) {

	//}

	//public void doAfterInsert(Map<Id,CLear_Algorithm_Settings__c> newMap, List<CLear_Algorithm_Settings__c> triggerNew) {

	//}

	//public void doAfterUpdate(Map<Id,CLear_Algorithm_Settings__c> oldMap, Map<Id,CLear_Algorithm_Settings__c> newMap, List<CLear_Algorithm_Settings__c> triggerOld, List<CLear_Algorithm_Settings__c> triggerNew) {

	//}

	//public void doAfterDelete(Map<Id,CLear_Algorithm_Settings__c> oldMap, List<CLear_Algorithm_Settings__c> triggerOld) {

	//}

	//public void doAfterUndelete(Map<Id,CLear_Algorithm_Settings__c> newMap, List<CLear_Algorithm_Settings__c> triggerNew) {

	//}

}