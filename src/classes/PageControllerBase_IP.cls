/*****
 * @Description: Controller Component Communication
 * @Author: https://developer.salesforce.com/page/Controller_Component_Communication
******/
public with sharing virtual class PageControllerBase_IP {
    
    private ComponentControllerBase_IP myComponentController;
    private Map<String, ComponentControllerBase_IP>componentControllerMap; //new
  
    public virtual ComponentControllerBase_IP getMyComponentController() {
        return myComponentController;
    }

    //new getter for the hashmap
    public virtual Map<String, ComponentControllerBase_IP> getComponentControllerMap(){
        return componentControllerMap;
    }

    //new method for putting value in the hashmap
    public virtual void setComponentControllerMap(String key, ComponentControllerBase_IP compController){

        if(componentControllerMap == null)
            componentControllerMap = new Map<String, ComponentControllerBase_IP>();
        componentControllerMap.put(key,compController); 
    }

    public virtual void setComponentController(ComponentControllerBase_IP compController) {
        myComponentController = compController;
    }
   
    public final User currentUser {get;set;}
    public CaseDetailsWrapper caseLog {get;set;}

    public PageControllerBase_IP getThis() {
        return this;
    }
  
    public PageControllerBase_IP(){
        /*if(ApexPages.currentPage().getParameters().containsKey('casId')){
          caseDetails = getCaseInfo(ApexPages.currentPage().getParameters().get('casId'));
        }*/

        currentUser = [SELECT Id, FirstName, LastName, Name, UserRole.Name, Belongs_To__c, Actions_Access__c FROM User WHERE Id=: UserInfo.getUserId()];
        caseLog = new CaseDetailsWrapper();
    }

    public with sharing class CaseDetailsWrapper
    {
        // Each wrapper should have a case, additional details and action set

        public Case caseDetails {get;set;}
        public List<AddCaseDetails__c> additionalCaseDetails {get;set;}
        public Case_Action__c userAction {get;set;}

        public CaseDetailsWrapper()
        {
            // Instantiate on every call
            caseDetails = new Case();
            additionalCaseDetails = new List<AddCaseDetails__c>();
            userAction = new Case_Action__c();
        }
    }
}