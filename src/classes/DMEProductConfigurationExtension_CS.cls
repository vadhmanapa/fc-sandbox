/**
 *  @Description 
 *  @Author Cloud Software LLC
 *  Revision History: 
 *		12/23/2015 - karnold - Added header, removed references to DME_Category__c
 *		01/12/2016 - karnold - Added WHERE Active__c = true to the base query for DME__c. 
 *			Formatted code. Removed some duplicate comments and standardized vertical whitespace.
 *			Removed commented code.
 *
 */
public without sharing class DMEProductConfigurationExtension_CS {

	/* ERROR/PAGE MESSAGES */

	public static final String errorMsg_FailedToQueryDME = 'An error occured retrieving the DME Product List.';
	public static final String errorMsg_NoDMEsFound = 'No DMEs have been found with the given filter criteria';
	public static final String errorMsg_InsertDME = 'An error has occured while inserting new DME products.';
	public static final String errorMsg_UpdateDME = 'An error has occured while updating existing DME Products';
	public static final String errorMsg_generic = 'An error has occured, your administrator has been notified.';
	public static String DisplayMessage { get; set; }

	/* sObject Collections */

	public List<DME__c> dmeList { get; set; }
	public List<DMEWrapper> newDMEList { get; set; }

	/* Page Search/Filter Options */

	public List<SelectOption> categoryFilterList { get; set; }
	public String categoryFilter { get; set; }
	public String productCodeFilter { get; set; }
	private Boolean hasWhereClause { get; set; }

	/* PAGINATION */

	public Integer pageSize { get; set; }
	public Integer pageNumber { get; set; }
	public List<SelectOption> pageSizeOptions { get; set; }
	public ApexPages.StandardSetController setCon { get; set; }
	public Boolean hasNext { get; set; }
	public Boolean hasPrevious { get; set; }

	/* USER SELECTIONS */

	public Integer selectedTempID { get; set; }
	public Boolean displayNewBlock { get; set; }

	/* CONSTSTANTS  */

	private static final Integer DEFAULT_PAGE_NUMBER = 1;
	private static final Integer DEFAULT_PAGE_SIZE = 10;

	/*    End Parameters     */

	public DMEProductConfigurationExtension_CS() {

		initPageDetails();
		addNewDME();
		initDMEList();

	} //End Constructor

	public void initDMEList() {

		String baseQuery = '';
		// Fields
		baseQuery = 'Select id, Active__c, Description__c, Medicare_Limits__c, Keywords__c, Max_Delivery_Time__c, Name, Setup_Time__c, Volume__c ';
		baseQuery += 'From DME__c ';
		// Default WHERE clause
		baseQuery += 'WHERE Active__c = true';
		hasWhereClause = true;
		// Filtering
		baseQuery += filterByCategory(categoryFilter);
		baseQuery += filterByProductCode(productCodeFilter);
		system.debug('### baseQuery: ' + baseQuery);

		try {
			setCon = new Apexpages.Standardsetcontroller(Database.getQueryLocator(baseQuery));
			System.debug('Querying');
			setCon.setPageSize(pageSize);
			setCon.setPageNumber(pageNumber);
			prepareView();
		} catch(QueryException qe) {
			Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, errorMsg_FailedToQueryDME + qe.getMessage()));
		} catch(Exception e) {
			Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, errorMsg_generic + ' : ' + e.getMessage()));
		}

		System.debug('DME List: ' + newDMEList);

	}

	public void addNewDME() {

		DMEWrapper dme = new DMEWrapper();
		newDMEList.add(dme);

	} //End addNewDME

	public void removeDME() {
		system.debug('### BEFORE REMOVE: ' + newDMEList.size());
		system.debug(selectedTempID);
		for (Integer i = 0; i < newDMEList.size(); i++) {
			system.debug(newDMEList[i].tempID);
			//Remove DME from list
			if (selectedTempID != null && newDMEList[i].tempID == selectedTempID) {
				newDMEList.remove(i);
			}
		}
		system.debug('### AFTER REMOVE: ' + newDMEList.size());

	} //End removeDME

	public Pagereference saveNewDMEs() {
		system.debug('### IN Save');
		Boolean inError = false;
		List<DME__c> dmeToInsert = new List<DME__c> ();
		try {
			if (newDMEList.size() != 0) {
				for (DMEWrapper dw : newDMEList) {
					dmeToInsert.add(dw.Dme);
				}
				system.debug('dmeToInsert: ' + dmeToInsert);
				insert dmeToInsert;
			} else {
				Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, 'There are no DME Products to save!'));
			} //end size condition
		} catch(DMLException dmle) {
			Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, errorMsg_InsertDME));
			system.debug(dmle.getMessage());
			inError = true;
		} catch(Exception e) {
			system.debug(e.getMessage());
			Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, errorMsg_generic));
			inError = true;
		}
		//clear new list and add a fresh DME product 
		newDmeList = new List<DMEWrapper> ();
		DMEWrapper nDME = new DMEWrapper();
		newDMEList.add(nDME);

		if (!inError) {
			DisplayMessage = 'Your DME Products have been added to the database!';
		}
		return null;
	} //End saveNewDMEs

	public void updateExistingDMEs() {
		List<DME__c> dmeToUpdate = new List<DME__c> ();

		try {
			if (dmeList.size() != 0) {
				dmeToUpdate.addAll(dmeList);
				system.debug('updateExisting dmeToInsert: ' + dmeToUpdate);
				update dmeToUpdate;
			} else {
				//Should never get here, if so another error occured
				Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, errorMsg_generic));
			} //End size condition

		} catch(DMLException dmle) {
			Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, errorMsg_InsertDME));
			system.debug(dmle.getMessage());
		} catch(Exception e) {
			system.debug(e.getMessage());
			Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, errorMsg_generic));
		}
		DisplayMessage = 'Your DME Products have been updated!';
	} //End updateExistingDMEs

	public void showHideNewBlock() {
		if (!displayNewBlock) {
			displayNewBlock = true;
		} else {
			displayNewBlock = false;
		}
	} //End showHideNewBlock

	private void initPageDetails() {
		pageNumber = 1;
		pageSize = DEFAULT_PAGE_SIZE;
		initPageSizeOptions();
		newDMEList = new List<DMEWrapper> ();
		displayNewBlock = false;
	} //End initPageDetails


	private void prepareView() {
		system.debug('IN prepareView');
		dmeList = new List<DME__c> ();
		system.debug('setCon records: ' + setCon.getRecords());
		if (setCon != null) {
			dmeList = setCon.getRecords();
			getHasNext();
			getHasPrevious();
		} else {
			Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.WARNING, errorMsg_NoDMEsFound));
		} //End setCon null condition


	} //End prepareView


	private String filterByCategory(String cat) {
		String s = '';
		//Do not add to query string if filter category hasn't been selected
		if (cat == null || cat == '') {
			return s;
		}
		if (!hasWhereClause) {
			s = 'Where ';
			hasWhereClause = true;
		} else {
			s = 'AND ';
		}
		s += '\'' + cat + '\'';

		return s;
	} //End filterByCategory

	private String filterByProductCode(String code) {
		//TODO: Find all products LIKE productcode provided
		String s = '';
		if (code == null || code == '') {
			return s;
		}
		if (!hasWhereClause) {
			s = 'Where ';
			hasWhereClause = true;
		} else {
			s = 'AND ';
		}

		s += 'Name LIKE ';
		s += '\'' + code + '%' + '\'';

		return s;
	} //END filterByProductCode

	/* Pagination Block */

	public Boolean getHasNext() {
		system.debug('### IN getHasNext');
		hasNext = setCon.getHasNext();
		system.debug('### hasNext: ' + hasNext);
		return setCon.getHasNext();
	} //END getHasNext

	public Boolean getHasPrevious() {
		system.debug('### IN getHasPrevious');
		hasPrevious = setCon.getHasPrevious();
		system.debug('### hasPrevious: ' + hasPrevious);
		return setCon.getHasPrevious();
	} //End getHasPrevious

	public void getNext() {
		system.debug('### In getNext');
		system.debug('### setCon: ' + setCon);
		try {
			Integer maxPage = (Integer) (setCon.getResultSize() / pageSize);
			if (Math.mod(setCon.getResultSize(), pageSize) > 0) {
				maxPage += 1;
			}
			pageNumber = maxPage > pageNumber ? pageNumber + 1 : pageNumber;
			setCon.setPageNumber(pageNumber);
			prepareView();
		} catch(Exception e) {
			system.debug('getNext error: ' + e.getMessage());
			Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, errorMsg_generic));
		}

	} //End getNext

	public void getPrevious() {
		system.debug('### IN getPrevious');
		try {
			pageNumber = pageNumber > 1 ? pageNumber - 1 : pageNumber;
			setCon.setPageNumber(pageNumber);
			prepareView();
		} catch(Exception e) {
			system.debug('getPrevious error: ' + e.getMessage());
			Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, errorMsg_generic));
		}

	} //End getPrevious

	public void getFirst() {

		try {
			pageNumber = 1;
			setCon.setPageNumber(pageNumber);
			prepareView();
		} catch(Exception e) {
			system.debug('getPrevious error: ' + e.getMessage());
			Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, errorMsg_generic));
		}

	} //End getFirst

	public void getLast() {

		try {
			Integer maxPage = (Integer) (setCon.getResultSize() / pageSize);
			if (Math.mod(setCon.getResultSize(), pageSize) > 0) maxPage += 1;
			pageNumber = maxPage;
			setCon.setPageNumber(pageNumber);
			system.debug('### getLast::Page#: ' + setCon.getPageNumber());
			prepareView();
		} catch(Exception e) {
			system.debug('getPrevious error: ' + e.getMessage());
			Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, errorMsg_generic));
		}
	} //End getLast

	private void initPageSizeOptions() {
		system.debug('### in initPAgeSizeOptions');
		pageSizeOptions = new List<SelectOption> ();
		pageSizeOptions.add(new SelectOption('10', '10'));
		pageSizeOptions.add(new SelectOption('20', '20'));
		pageSizeOptions.add(new SelectOption('50', '50'));
		pageSizeOptions.add(new SelectOption('100', '100'));
	} //end initPageSizeOptions

	/* INNER CLASSES */

	public class DMEWrapper {

		public Integer tempID { get; set; }
		public DME__c dme { get; set; }

		public DMEWrapper() {
			dme = new DME__c();
			resetTempID();
		}

		private void resetTempID() {
			this.tempID = Math.abs(Crypto.getRandomInteger());
		}
	}
}