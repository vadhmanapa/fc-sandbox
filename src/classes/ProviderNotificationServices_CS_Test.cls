/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ProviderNotificationServices_CS_Test {

	public static Account plan;
	public static Account provider;
	public static Contact user;
	public static List<Order__c> orderList;	
	public static List<EmailTemplate> templates;

    static testMethod void ProviderOrdersSummary_CS_sendNotification() {
        init();
		
		Messaging.Singleemailmessage notification = new Messaging.Singleemailmessage();

		notification.setToAddresses(new String[]{user.Email});
		notification.setSubject('Test Subject');
		notification.setHtmlBody('Test Body');
		
		//No way to test sending an email in a test class
		System.assert(ProviderNotificationServices_CS.sendNotification(new Messaging.SingleEmailMessage[]{notification}));
    }
    
    static testMethod void notifyAssignedOrder() {
    	init();
    	System.assert(ProviderNotificationServices_CS.notifyAssignedOrder(orderList[0]));
    }
    
    static testMethod void notifyPayerCanceledOrder() {
    	init();
    	System.assert(ProviderNotificationServices_CS.notifyPayerCanceledOrder(orderList[0]));
    }
    
    static testMethod void notifyOrderNeedsAttention() {
    	init();
    	System.assert(ProviderNotificationServices_CS.notifyOrderNeedsAttention(orderList[0]));
    }
    
    static testMethod void notifyPasswordChange() {
    	init();
    	System.assert(ProviderNotificationServices_CS.notifyPasswordChange(user));
    }
    
    public static void init() {
    	List<Account> providerList = TestDataFactory_CS.generateProviders('Test Provider', 1);
    	provider = providerList[0];
    	insert providerList;
    	
    	List<Account> payerList = TestDataFactory_CS.generatePlans('Test Payer', 1);
    	plan = payerList[0];
    	insert payerList;
    	
    	List<Plan_Patient__c> patientList = TestDataFactory_CS.generatePlanPatientsPlanOnly(plan.Id, 1);
    	insert patientList;  	
    	
    	user = TestDataFactory_CS.generateProviderContacts('Admin', providerList, 1)[0];
    	insert user;
    	
    	orderList = TestDataFactory_CS.generateOrders(patientList[0].Id, provider.Id, 3);
    	for(Order__c o : orderList){
    		o.Accepted_By__c = user.Id;
    	}
    	insert orderList;
    	
    }
}