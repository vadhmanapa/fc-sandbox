public with sharing class AdjustmentRequestAgent_IP {

	public Map<Id, ARAG__c> billMap = new Map<Id, ARAG__c>();

	public AdjustmentRequestAgent_IP() {
		
	}

	public void runOnInsert(List<Adjustments__c> newTrigger){

		// for each value in insert trigger, update the values in CWI
		Set<Id> getIds = new Set<Id>();
		for(Adjustments__c a : newTrigger){

			getIds.add(a.Claims_Work_Item__c);
		}

		if(getIds.size() > 0){

			for(ARAG__c ag : [Select Id, Write_Off__c,
										 Written_Off_in_Billing_System__c,
										 Written_Off_by__c,
										 Write_Off_Approval_Date__c,
										 Write_Off_Approved__c,
										 Write_Off_Date__c,
										 Write_Off_Denial_Reason__c,
										 Write_Off_Denied__c,
										 Uploaded_Date__c,
										 Write_Off_Notes__c,
										 Write_Off_Reason__c,
										 Write_off_Requested_by__c,
										 Write_Off_Type__c,
										 Submit_for_Write_Off_Review__c,
										 Write_off_Request_Date__c from ARAG__c where Id =: getIds]){

				billMap.put(ag.Id, ag);
			}
		}

		fnInsertUpdate(newTrigger, billMap);

	}

	private void fnInsertUpdate(List<Adjustments__c> newTrigger, Map<Id, ARAG__c> matchedBills){

		for(Adjustments__c ad : newTrigger){

			ARAG__c bill = matchedBills.get(ad.Claims_Work_Item__c);

			if(bill != null){

				bill.Write_Off__c = ad.Adjust_Amount__c;
				bill.Write_Off_Approved__c = ad.Write_Off_Approved__c;
				bill.Write_Off_Date__c = ad.Write_Off_Date__c;
				bill.Write_Off_Denied__c = ad.Write_Off_Denied__c;
				bill.Write_Off_Notes__c = ad.Adjustment_Notes__c;
				bill.Write_Off_Reason__c = ad.Adjustment_Reason__c;
				bill.Write_Off_Type__c = ad.Write_Off_Type__c;
				bill.Written_Off_by__c = ad.Written_Off_By__c;
				bill.Write_Off_Approval_Date__c = ad.Write_Off_Approval_Date__c;
				bill.Write_Off_Denial_Reason__c = ad.Write_Off_Denial_Reason__c;
				bill.Write_off_Request_Date__c = ad.Adjustment_Request_Date__c;
				bill.Write_off_Requested_by__c = ad.Adjustment_Requested_By__c;
				bill.Written_Off_in_Billing_System__c = ad.Written_Off_in_Billing_System__c;
				bill.Submit_for_Write_Off_Review__c = ad.Submit_Adjustment_Review__c;
			}
		}

		update matchedBills.values();

	}

	public static void fnDelete(List<Adjustments__c> toDelete){
		Set<Id> deleteId = new Set<Id>();
		for(Adjustments__c a : toDelete){
			deleteId.add(a.Claims_Work_Item__c);
		}

		Map<Id, ARAG__c> deleteMap = new Map<Id, ARAG__c>([
									    Select Id, Name,
										Write_Off__c,
									    Written_Off_in_Billing_System__c,
										Written_Off_by__c,
										Write_Off_Approval_Date__c,
										Write_Off_Approved__c,
										Write_Off_Date__c,
										Write_Off_Denial_Reason__c,
										Write_Off_Denied__c,
										Uploaded_Date__c,
										Write_Off_Notes__c,
										Write_Off_Reason__c,
										Write_off_Requested_by__c,
										Write_Off_Type__c,
										Submit_for_Write_Off_Review__c,
										Write_off_Request_Date__c,
										(Select Id,
										 Adjust_Amount__c,
										 Write_Off_Approved__c,
										 Write_Off_Date__c,
									     Write_Off_Denied__c,
										 Adjustment_Notes__c,
										 Adjustment_Reason__c,
										 Write_Off_Type__c,
										 Written_Off_By__c,
										 Write_Off_Approval_Date__c,
										 Write_Off_Denial_Reason__c,
										 Adjustment_Request_Date__c,
										 Adjustment_Requested_By__c,
										 Written_Off_in_Billing_System__c,
										 Submit_Adjustment_Review__c,
										 Claims_Work_Item__c 
										 FROM Write_Off_Reviews__r
										 ORDER BY CreatedDate DESC)
										 FROM ARAG__c WHERE Id IN: deleteId]);

		for(Adjustments__c ad : toDelete){
			ARAG__c ar = deleteMap.get(ad.Claims_Work_Item__c);
			if(ar != null){
				// check for size of Adjustments
				System.debug('**********Number of Adjustments found*********'+ar.Write_Off_reviews__r.size());
				if(ar.Write_Off_Reviews__r.size() > 0){

					System.debug('**********Adjustments found. Entering update values************');

					Adjustments__c previousAdj = ar.Write_Off_Reviews__r[0];

					ar.Write_Off__c = previousAdj.Adjust_Amount__c;
					ar.Write_Off_Approved__c = previousAdj.Write_Off_Approved__c;
					ar.Write_Off_Date__c = previousAdj.Write_Off_Date__c;
					ar.Write_Off_Denied__c = previousAdj.Write_Off_Denied__c;
					ar.Write_Off_Notes__c = previousAdj.Adjustment_Notes__c;
					ar.Write_Off_Reason__c = previousAdj.Adjustment_Reason__c;
					ar.Write_Off_Type__c = previousAdj.Write_Off_Type__c;
					ar.Written_Off_By__c = previousAdj.Written_Off_By__c;
					ar.Write_Off_Approval_Date__c = previousAdj.Write_Off_Approval_Date__c;
					ar.Write_Off_Denial_Reason__c = previousAdj.Write_Off_Denial_Reason__c;
				    ar.Write_off_Request_Date__c = previousAdj.Adjustment_Request_Date__c;
					ar.Write_off_Requested_by__c = previousAdj.Adjustment_Requested_By__c;
					ar.Written_Off_in_Billing_System__c = previousAdj.Written_Off_in_Billing_System__c;
					ar.Submit_for_Write_Off_Review__c = previousAdj.Submit_Adjustment_Review__c;

				} else{
					System.debug('******** No Other Adjustments Found***********');
					ar.Write_Off__c = null;
					ar.Write_Off_Approved__c = false;
					ar.Write_Off_Date__c = null;
					ar.Write_Off_Denied__c = false;
					ar.Write_Off_Notes__c = '';
					ar.Write_Off_Reason__c = '';
					ar.Write_Off_Type__c = '';
					ar.Written_Off_By__c = null;
					ar.Write_Off_Approval_Date__c = null;
					ar.Write_Off_Denial_Reason__c = '';
				    ar.Write_off_Request_Date__c = null;
					ar.Write_off_Requested_by__c = null;
					ar.Written_Off_in_Billing_System__c = false;
					ar.Submit_for_Write_Off_Review__c = false;
				}
			} else{
				System.debug('************** ARAG not found*************');
				continue;
			}
		}

		update deleteMap.values();

	}
}