@isTest
private class AdditionalDetailsControllerTest_IP {
	
	public static TestCaseDataFactory_IP generateNewData;
	private static AdditionalDetailsController_IP additionalDetails;
	private static User testUser;

	@isTest static void checkAdditionalDetailsPicklist() {

		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		testUser = TestCaseDataFactory_IP.csUser;
		List<SelectOption> detailsRecType = new List<SelectOption>();

		System.runAs(testUser){
			Test.startTest();
			additionalDetails = new AdditionalDetailsController_IP();
			detailsRecType = additionalDetails.getDetailsRecType();
			Test.stopTest();
		}

		System.assertNotEquals(0, detailsRecType.size(), 'This is a test to check if the addiitonal detail types picklist is loaded when called');
	}
	
	@isTest static void checkControllerInitialization(){

		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		testUser = TestCaseDataFactory_IP.csUser;

		System.runAs(testUser){
			Test.startTest();
			additionalDetails = new AdditionalDetailsController_IP();
			Test.stopTest();
		}

		System.assertEquals('', additionalDetails.selectedDetailRecType, 'Test if variable is blank when assign to value is not passed');
		System.assertEquals(false, additionalDetails.isClaimsTeam, 'Test if the boolean is false as this is not a claims user');
		System.assertEquals(0, additionalDetails.counter, 'The counter should be 0 when the class is called');
	}

	@isTest static void checkInitWithClaims(){
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		testUser = TestCaseDataFactory_IP.csUser;

		System.runAs(testUser){
			additionalDetails = new AdditionalDetailsController_IP();
			Test.startTest();
			additionalDetails.belongsTo = 'Claims';
			Test.stopTest();
		}

		System.assertEquals('Que Claim ID', additionalDetails.selectedDetailRecType, 'The value was set according to Claims');
		System.assertEquals(true, additionalDetails.isClaimsTeam, 'The value was set according to Claims');
	}

	// Validation tests

	@isTest static void checkAddingNullAuth(){
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		testUser = TestCaseDataFactory_IP.csUser;

		System.runAs(testUser){
			additionalDetails = new AdditionalDetailsController_IP();
			additionalDetails.newDetail.AuthorizationNumber__c = '';
			additionalDetails.selectedDetailRecType = 'Authorization Number';
			Test.startTest();
			additionalDetails.addDetailsToList();
			Test.stopTest();
		}

		System.assertEquals(0, additionalDetails.additionalDetailsList.size(), 'The null validation for auth number is verified');
	}

	@isTest static void checkAddingNullHCPC(){
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		testUser = TestCaseDataFactory_IP.csUser;

		System.runAs(testUser){
			additionalDetails = new AdditionalDetailsController_IP();
			additionalDetails.newDetail.HCPC_Code__c = null;
			additionalDetails.selectedDetailRecType = 'HCPC Code';
			Test.startTest();
			additionalDetails.addDetailsToList();
			Test.stopTest();
		}

		System.assertEquals(0, additionalDetails.additionalDetailsList.size(), 'The null validation for null HCPC Code is verified');
	}

	@isTest static void checkAddingNullOrder(){
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		testUser = TestCaseDataFactory_IP.csUser;

		System.runAs(testUser){
			additionalDetails = new AdditionalDetailsController_IP();
			additionalDetails.newDetail.Order__c = null;
			additionalDetails.selectedDetailRecType = 'Order Number';
			Test.startTest();
			additionalDetails.addDetailsToList();
			Test.stopTest();
		}

		System.assertEquals(0, additionalDetails.additionalDetailsList.size(), 'The null validation for null order is verified');
	}

	@isTest static void checkAddingNullQueClaim(){
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		testUser = TestCaseDataFactory_IP.csUser;

		System.runAs(testUser){
			additionalDetails = new AdditionalDetailsController_IP();
			additionalDetails.newDetail.QueClaimID__c = null;
			additionalDetails.selectedDetailRecType = 'Que Claim ID';
			Test.startTest();
			additionalDetails.addDetailsToList();
			Test.stopTest();
		}

		System.assertEquals(0, additionalDetails.additionalDetailsList.size(), 'The null validation for null Que claim is verified');
	}

	@isTest static void checkAddingNullReferredProvider(){
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		testUser = TestCaseDataFactory_IP.csUser;

		System.runAs(testUser){
			additionalDetails = new AdditionalDetailsController_IP();
			additionalDetails.newDetail.ReferredProvider__c = null;
			additionalDetails.selectedDetailRecType = 'Referred Provider';
			Test.startTest();
			additionalDetails.addDetailsToList();
			Test.stopTest();
		}

		System.assertEquals(0, additionalDetails.additionalDetailsList.size(), 'The null validation for null referred provider is verified');
	}

	@isTest static void checkAddingNullzPaperNum(){
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		testUser = TestCaseDataFactory_IP.csUser;

		System.runAs(testUser){
			additionalDetails = new AdditionalDetailsController_IP();
			additionalDetails.newDetail.zPaperReferenceNumber__c = null;
			additionalDetails.selectedDetailRecType = 'zPaper Reference Number';
			Test.startTest();
			additionalDetails.addDetailsToList();
			Test.stopTest();
		}

		System.assertEquals(0, additionalDetails.additionalDetailsList.size(), 'The null validation for null zPaper Reference Number is verified');
	}

	@isTest static void checkAddingMultipleClaimsInOneInquiry(){
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		testUser = TestCaseDataFactory_IP.csUser;

		System.runAs(testUser){
			additionalDetails = new AdditionalDetailsController_IP();
			additionalDetails.belongsTo = 'Claims';
			additionalDetails.newDetail.QueClaimID__c = '56551161';
			additionalDetails.selectedDetailRecType = 'Que Claim ID';
			additionalDetails.addDetailsToList();
			additionalDetails.newDetail.QueClaimID__c = '565511987';
			additionalDetails.selectedDetailRecType = 'Que Claim ID';
			Test.startTest();
			additionalDetails.addDetailsToList();
			Test.stopTest();
		}

		System.assertEquals(1, additionalDetails.additionalDetailsList.size(), 'One inquiry can have only one claim validation verified');
	}

	@isTest static void checkAddingInquiry(){
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		testUser = TestCaseDataFactory_IP.csUser;

		System.runAs(testUser){
			additionalDetails = new AdditionalDetailsController_IP();
			additionalDetails.belongsTo = 'Claims';
			additionalDetails.newDetail.QueClaimID__c = '56551161';
			additionalDetails.selectedDetailRecType = 'Que Claim ID';
			Test.startTest();
			additionalDetails.addDetailsToList();
			Test.stopTest();
		}

		System.assertEquals(1, additionalDetails.additionalDetailsList.size(), 'Detail added after validation verified');
	}

	@isTest static void checkRemovingAddedInquiry(){
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		testUser = TestCaseDataFactory_IP.csUser;

		System.runAs(testUser){
			additionalDetails = new AdditionalDetailsController_IP();
			additionalDetails.belongsTo = 'Claims';
			additionalDetails.newDetail.QueClaimID__c = '56551161';
			additionalDetails.selectedDetailRecType = 'Que Claim ID';
			additionalDetails.addDetailsToList();
			Test.startTest();
			PageReference pgRef = Page.NewCaseEntryPage_IP;
			pgRef.getParameters().put('index', '0');
			Test.setCurrentPage(pgRef);
			additionalDetails.removeDetailsFromList();
			Test.stopTest();
		}

		System.assertEquals(0, additionalDetails.additionalDetailsList.size(), 'Detail removed after validation verified');
	}

	@isTest static void checkPassingListToAdd(){
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		testUser = TestCaseDataFactory_IP.csUser;
		List<AddCaseDetails__c> testList = new List<AddCaseDetails__c>();

		System.runAs(testUser){
			additionalDetails = new AdditionalDetailsController_IP();
			additionalDetails.belongsTo = 'Claims';
			additionalDetails.newDetail.QueClaimID__c = '56551161';
			additionalDetails.selectedDetailRecType = 'Que Claim ID';
			additionalDetails.addDetailsToList();
			Test.startTest();
			testList = additionalDetails.detailsToAdd();
			Test.stopTest();
		}

		System.assertEquals(1, testList.size(), 'Passing value back to calling variable verified');
	}
	
}