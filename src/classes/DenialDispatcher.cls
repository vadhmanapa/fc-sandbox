public with sharing class DenialDispatcher {
	public DenialDispatcher(Map<Id,Denials__c> oldMap, Map<Id,Denials__c> newMap, List<Denials__c> triggerOld, List<Denials__c> triggerNew, Boolean isInsert, Boolean isUpdate, Boolean isDelete, Boolean isUndelete, Boolean isBefore, Boolean isAfter){
        if(isBefore) {
            if(isInsert) { doBeforeInsert(triggerNew); }
            else if(isUpdate) { doBeforeUpdate(oldMap, newMap, triggerOld, triggerNew); }
            else if(isDelete) { doBeforeDelete(oldMap, triggerOld); }           
        }else if(isAfter) {
            if(isInsert) { doAfterInsert(newMap, triggerNew); }
            else if(isUpdate) {doAfterUpdate(oldMap, newMap, triggerOld, triggerNew); }
            else if(isDelete) { doAfterDelete(oldMap, triggerold); }
            else if(isUndelete){ doAfterUnDelete(newMap, triggerNew); }        
        }
    }
    
    public void doBeforeInsert(List<Denials__c> triggerNew) {

		doMapWorkItemsToDenials(triggerNew);

    }
    
    public void doBeforeUpdate(Map<Id,Denials__c> oldMap, Map<Id,Denials__c> newMap, List<Denials__c> triggerOld, List<Denials__c> triggerNew) {
    
    	doMapWorkItemsToDenials(triggerNew);
    	  
    }
    
    public void doBeforeDelete(Map<Id,Denials__c> oldMap, List<Denials__c> triggerOld) {
    
    }    
    
    public void doAfterInsert(Map<Id,Denials__c> newMap, List<Denials__c> triggerNew) {

    }   
     
    public void doAfterUpdate(Map<Id,Denials__c> oldMap, Map<Id,Denials__c> newMap, List<Denials__c> triggerOld, List<Denials__c> triggerNew) {
    
    }    
    
    public void doAfterDelete(Map<Id,Denials__c> oldMap, List<Denials__c> triggerOld) {
    
    }    
    
    public void doAfterUndelete(Map<Id,Denials__c> newMap, List<Denials__c> triggerNew) {
    
    }    

	/*
	 *	Trigger Funtions*
	 */
	
	private void doMapWorkItemsToDenials (List<Denials__c> triggerNew) {
		
		List<ARAG__c> matchedBillUpdate = new List<ARAG__c>();
		
		//Collect
		
		Set<String> matchedBillIds = new Set<String>();
		
		for(Denials__c deny : triggerNew){
			matchedBillIds.add(deny.Bill__c);
		}

		//Query
		
		List<ARAG__c> matchedBills = [
			SELECT Id, Bill__c, Total_Due__c, Account_Number__c, Payor_Family_text__c, Patient_Name__c,
                       Billed_Date__c, Visit_Date__c, Status__c 
          	FROM ARAG__c 
          	WHERE Bill__c IN :matchedBillIds 
          	ORDER BY CreatedDate DESC];
          	
		//Re-map
		
		Map<String,List<ARAG__c>> billIdToMatchedBills = new Map<String,List<ARAG__c>>();
		
		for(ARAG__c a : matchedBills) {
			if(billIdToMatchedBills.containsKey(a.Bill__c)){
				billIdToMatchedBills.get(a.Bill__c).add(a);
			} else {
				billIdToMatchedBills.put(a.Bill__c,new List<ARAG__c>{a});
			}
		}
		
		//Execute
		
    	for(Denials__c deny : triggerNew){	

			// skip if denial is not denied
        	if(deny.Work_Denial__c == false){
        		continue;
        	}
            
         	// check if CWI was found
         	if(!billIdToMatchedBills.containsKey(deny.Bill__c)){        
         		continue;
         	}
            
         	// based on bill number in denial, search CWI for a match
         	ARAG__c matchedBill = billIdToMatchedBills.get(deny.Bill__c)[0];
    	
            // update denial
            deny.Related_CWI__c = matchedBill.Id;
            
            deny.Account_Number__c = matchedBill.Account_Number__c;
            deny.Patient_Name__c = matchedBill.Patient_Name__c;
            deny.Billed_Date__c = matchedBill.Billed_Date__c;
            deny.Amount_Outstanding__c = matchedBill.Total_Due__c;
            deny.Payor__c = matchedBill.Payor_Family_text__c;
            deny.Visit_Date__c = matchedBill.Visit_Date__c;

			//Update bill            
            matchedBill.Status__c = 'Denied';

    		matchedBillUpdate.add(matchedBill);
     	}
     	
     	//Update
     	
     	update matchedBillUpdate;
	}
	
	private void doMapDenialToPADR(List<Denials__c> triggerNew){
		/* This trigger is to map Denials to PADR. If the bill number is not found in PADR, then create a new record. If the bill number exists in PADR, then map it to that record. 
		List<PADR__c> toMap = new List<PADR__c>();
		List<PADR__c> query = new List <PADR__c>();
		List<PADR__c> toInsert = new List<PADR__c>();
		String recordId;
		for(Denials__c deny : triggerNew){
		   if (deny.Work_Denial__c == true){
		       query = [Select Id, Bill_Number__c, Procedure_Code_s__c, CreatedDate from PADR__c where Bill_Number__c =: deny.Bill__c ORDER BY  CreatedDate DESC];
		       if (query.size() > 0){
		           System.debug('Entering mapping trigger');
		           query[0].Procedure_Code_s__c = query[0].Procedure_Code_s__c + ','+deny.Line_Item__c;
		           update query;
		           deny.Related_PADR__c = query[0].Id;	           
		           System.debug ('The Denial is mapped to PADR');           
		       } else {
		               System.debug ('Entering insert trigger');
		               String providerName = deny.Provider__c;
		                   PADR__c newDeny = new PADR__c(PADR_Type__c = 'Denial', Bill_Number__c = deny.Bill__c, Provider_Name_Text__c = deny.Provider__c, Procedure_Code_s__c = deny.Line_Item__c,
		                                                                                     Claim__c = deny.Claim_Number__c, Payor_Name__c = deny.Payor__c );
		                    toInsert.add (newDeny);
		                    try {
	                          Database.SaveResult[] srMapList = Database.insert(toInsert, false);
	                            for (Database.SaveResult dsr : srMapList) {
	                                if (dsr.isSuccess()) {
	                                // Operation was successful, so get the ID of the record that was processed
	                                System.debug('Successfully inserted PADR. PADR Record ID: ' + dsr.getId());
	                                // isSuccess = true;
	                                recordId = dsr.getId();
	                            }else {
	                                // Operation failed, so get all errors                
	                                for(Database.Error err : dsr.getErrors()) {
	                                    System.debug('The following error has occurred.');                    
	                                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
	                                    System.debug('PADR__c fields that affected this error: ' + err.getFields());
	                                }
	                            }
	                            }
	                        }catch (System.Dmlexception e) {
	                                    System.debug('PADR not inserted. Job unsuccessful ' + e.getMessage());
	                            }
	                        if (recordId != null){
	                                    deny.Related_PADR__c  = recordId;
	                                    System.debug('The Denials is mapped to PADR');
	                                }else {
	                                    System.debug('Some issue happened');
	                                }
		       }
		   }
		}*/
	}
}