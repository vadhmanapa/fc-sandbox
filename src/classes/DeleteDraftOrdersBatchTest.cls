/**
* @description This class tests the DeleteDraftOrdersBatch class
* @author Cloud Software LLC, Tom Sharp
* Revision History:
*		05/12/2016 - Tom Sharp - Created.  
*		05/15/2016 - karnold - Added schedulable tests to class. Refactored batch test to use setter method.
*/
@isTest
private class DeleteDraftOrdersBatchTest { 
	private static List<Order__c> testOrderList;
	private static DeleteDraftOrdersBatch batch;
	private static String CRON_EXP = '0 0 * * * ?';

	/**
	* @description This method creates initial data required for testing.
	*/
	private static void init() {

		// Create draft order
		testOrderList = TestDataFactory_CS.generateOrders(null, null, 10);
		testOrderList[0].Status__c = 'Draft';
		insert testOrderList;

		// Instantiate batch
		batch = new DeleteDraftOrdersBatch();
	}

	/**
	* @description This method tests that orders with the status of draft are successfully deleted 
	*/
	private static testmethod void DeleteDraftOrdersBatch_Success() {
		init();

		batch.setQuery(
			'SELECT ' +
				'Id ' +
			'FROM Order__c ' +
			'WHERE Status__c = \'Draft\''
		);

		System.Test.startTest();
		Database.executeBatch(batch, 200);
		System.Test.stopTest();

		// Get order Ids
		Set<Id> orderIdSet = new Set<Id>();

		for (Order__c o : testOrderList) {
			orderIdSet.add(o.Id);
		}

		// Assert that the order was deleted
		System.assertEquals(9, [SELECT Id FROM Order__c WHERE Id IN :orderIdSet].size());
	}

	// Schedulable tests

	/**
	* @description This method tests that the next fire time of the scheduler is correct.
	*/
	private static testmethod void DeleteDraftOrdersScheduler_Test() {
		Test.startTest();
		String jobId = System.schedule('DeleteDraftOrdersSchedulerTest', CRON_EXP, new DeleteDraftOrdersBatch());
		CronTrigger ct = [
			SELECT 
				Id, 
				CronExpression, 
				TimesTriggered,
				NextFireTime
			FROM CronTrigger 
			WHERE id = :jobId
		];

		//asserts that the cron expressions are equal
		System.assertEquals(CRON_EXP, ct.CronExpression);
		//asserts that the batch has not been triggered yet
		System.assertEquals(0, ct.TimesTriggered);
		//asserts that the next fire time is the next day
		System.assertEquals(String.valueOf(DateTime.now().addHours(1).hour()), String.valueOf(ct.NextFireTime.hour()));
		Test.stopTest();
	}
}