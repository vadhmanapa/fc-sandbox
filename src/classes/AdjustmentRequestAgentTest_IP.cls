@isTest
private class AdjustmentRequestAgentTest_IP {
	
	 static testMethod void testAdjustmentTrigger() {
		// Create a ARAG
		ARAG__c a = new ARAG__c(
            Patient_Name__c = 'Tester',
            Status__c = 'Waiting on Payor',
            Pending_Action__c = 'Need to Draft Appeal'		
        );
        insert a;
        system.debug('ARAG inserted!');

		// Create two Adjustment records

		Adjustments__c ad1 = new Adjustments__c(Adjustment_Reason__c = 'Allowable Exceeded', 
												Adjust_Amount__c = 250, 
												Adjustment_Request_Date__c = System.today(), 
												Claims_Work_Item__c = a.Id);
		insert ad1;
		System.debug('Inserted 1st Adjustment');

		ARAG__c ar1 = [Select Id, 
							  Name, 
							  Write_Off_Reason__c, 
							  Write_Off__c from ARAG__c where Id=: ad1.Claims_Work_Item__c];

		System.assertEquals(ad1.Adjustment_Reason__c, ar1.Write_Off_Reason__c, 'The write reason matches adjustment reason');
		System.assertEquals(ad1.Adjust_Amount__c, ar1.Write_Off__c, 'The write amount matches adjustment amount');

		System.debug('************ stage 1 check complete********************');

		Adjustments__c ad2 = new Adjustments__c(Adjustment_Reason__c = 'Balance Credit', 
												Adjust_Amount__c = 350, 
												Adjustment_Request_Date__c = System.today()+1, 
												Claims_Work_Item__c = a.Id);
		insert ad2;
		System.debug('Inserted 2nd Adjustment');

		ARAG__c ar2 = [Select Id, 
							  Name, 
							  Write_Off_Reason__c, 
							  Write_Off__c from ARAG__c where Id=: ad2.Claims_Work_Item__c];

		System.assertEquals(ad2.Adjustment_Reason__c, ar2.Write_Off_Reason__c, 'The write reason matches adjustment reason');
		System.assertEquals(ad2.Adjust_Amount__c, ar2.Write_Off__c, 'The write amount matches adjustment amount');

		System.debug('************ stage 2 check complete********************');

		ad2.Adjust_Amount__c = 500;
		update ad2;

		System.debug('Adjustment 2 updated');

		ARAG__c ar3 = [Select Id, 
							  Name, 
							  Write_Off_Reason__c, 
							  Write_Off__c from ARAG__c where Id=: ad1.Claims_Work_Item__c];

		System.assertEquals(ad2.Adjust_Amount__c, ar3.Write_Off__c, 'The write amount matches adjustment amount');

		System.debug('************ stage 3 check complete********************');

		delete ad2;
		System.debug('Adjustment 2 deleted');

		ARAG__c ar4 = [Select Id, 
							  Name, 
							  Write_Off_Reason__c, 
							  Write_Off__c from ARAG__c where Id=: a.Id];

		System.assertEquals(ar4.Write_Off_Reason__c, ad1.Adjustment_Reason__c, 'The previous write off reason was filled');
		System.assertEquals(ar4.Write_Off__c, ad1.Adjust_Amount__c, 'The previous write off amount was filled in');

		System.debug('************ stage 4 check complete********************');

		delete ad1;
		System.debug('Adjustment 1 deleted');

		ARAG__c ar5 = [Select Id, 
							  Name, 
							  Write_Off_Reason__c, 
							  Write_Off__c from ARAG__c where Id=: a.Id];

		System.assertEquals(ar5.Write_Off_Reason__c, null, 'The write off reason is empty now');
		System.assertEquals(ar5.Write_Off__c, null, 'The write off amount is empty now');




	}
	
	
}