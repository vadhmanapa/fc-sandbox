public class RevenueTrackingForPayerRelations {
    // controller for visualforce chart which shows the Revenue Tracking
            
        public static String currentYear;
        public static String lastYear;
        public static String nextYear;
        public Integer totalCalls = 0;
        public Double pwpQ1 = 0,pwpQ2 = 0,pwpQ3 = 0,pwpQ4 = 0, pwpActual = 0;
        public Double pwpNextQ1=0, pwpNextQ2=0, pwpNextQ3=0, pwpNextQ4 =0, pwpFutureQ1 = 0; // to hold the divident pwp based on implementation
        public Double finalQ1=0, finalQ2=0, finalQ3=0, finalQ4=0, finalNextQ1=0;
        public AggregateResult revenueTracking;
        public AggregateResult nextYearRevenue;

    public RevenueTrackingForPayerRelations(){

        Date myDate = System.today();
        currentYear = String.valueOf(myDate.year());
        lastYear = String.valueOf((myDate.year())-1);
        nextYear = String.valueOf((myDate.year())+1);

        //------------------------------------------------ Opportunity Query----------------------------------------------------------------

        List<Opportunity> pwpOpps = [Select PWP_with_Market_Share__c,Potential_Implementation__c from Opportunity where RecordType.Name = 'Payer Relations' AND Potential_Implementation__c != null];
        for(Opportunity o : pwpOpps){
            // calculation for last year
            
             if(o.Potential_Implementation__c.contains(lastYear)){    
                System.debug('Implementation'+o.Potential_Implementation__c);
                System.debug('PWP'+o.PWP_with_Market_Share__c);
                
                if(o.PWP_with_Market_Share__c == null){
                    pwpActual = 0;
                } else {
                    pwpActual = (o.PWP_with_Market_Share__c/4);
                }
                
                if(o.Potential_Implementation__c.contains('Q1')){
                    System.debug('Q1');
                    // track for Q2, Q3, Q4, Next Q1
                   // pwpQ1 = pwpQ1 + pwpActual;
                   pwpNextQ1 += pwpActual;
                  
                }
                else if(o.Potential_Implementation__c.contains('Q2')){
                    System.debug('Q2');
                   pwpNextQ2 += pwpActual;
                   pwpNextQ1 += pwpActual;
                }
                else if(o.Potential_Implementation__c.contains('Q3')){
                    System.debug('Q3');
                    pwpNextQ3 += pwpActual;
                    pwpNextQ2 += pwpActual;
                    pwpNextQ1 += pwpActual;
                }
                else if(o.Potential_Implementation__c.contains('Q4')){
                    System.debug('Q4');
                    pwpNextQ4 += pwpActual;
                    pwpNextQ3 += pwpActual;
                    pwpNextQ2 += pwpActual;
                    pwpNextQ1 += pwpActual;
                }
            }
            
            // calculation for current year
            if(o.Potential_Implementation__c.contains(currentYear)){    
                System.debug('Implementation'+o.Potential_Implementation__c);
                System.debug('PWP'+o.PWP_with_Market_Share__c);
                
                if(o.PWP_with_Market_Share__c == null){
                    pwpActual = 0;
                } else {
                    pwpActual = (o.PWP_with_Market_Share__c/4);
                }
                
                if(o.Potential_Implementation__c.contains('Q1')){
                    System.debug('Q1');
                   // pwpQ1 = pwpQ1 + pwpActual;
                   pwpQ2 += pwpActual;
                   pwpQ3 += pwpActual;
                   pwpQ4 += pwpActual;
                   pwpFutureQ1 += pwpActual;
                }
                else if(o.Potential_Implementation__c.contains('Q2')){
                    System.debug('Q2');
                   pwpQ3 += pwpActual;
                   pwpQ4 += pwpActual;
                   pwpFutureQ1 += pwpActual;
                }
                else if(o.Potential_Implementation__c.contains('Q3')){
                    pwpQ4 += pwpActual;
                    pwpFutureQ1 += pwpActual;
                }
                else if(o.Potential_Implementation__c.contains('Q4')){
                    System.debug('Q4');
                   pwpFutureQ1 += pwpActual;
                }
            }
        }
        
        // final summation of of pwp for all quarters
        finalQ1 = pwpNextQ1;
        finalQ2 = pwpNextQ2 + pwpQ2;
        finalQ3 = pwpNextQ3 + pwpQ3;
        finalQ4 = pwpNextQ4 + pwpQ4;
        finalNextQ1 = pwpFutureQ1;
        
        System.debug('PWP for all Quarter'+pwpQ1 +pwpQ2 +pwpQ3 +pwpQ4);

        //*******************************************************************Revenue Aggregate Query**************************************************************

        revenueTracking = [Select SUM(Q1_Revenue__c)revenueQ1,SUM(Q2_Revenue__c)revenueQ2,SUM(Q3_Revenue__c)revenueQ3,SUM(Q4_Revenue__c)revenueQ4, SUM(Q1_Plan_Revenue__c)planRevenueQ1,
                                          SUM(Q2_Plan_Revenue__c)planRevenueQ2,SUM(Q3_Plan_Revenue__c)planRevenueQ3,SUM(Q4_Plan_Revenue__c)planRevenueQ4 from Integra_Revenue_Tracking__c where 
                                          Name =: currentYear];
        nextYearRevenue = [Select SUM(Q1_Revenue__c)revenueQ1, SUM(Q1_Plan_Revenue__c)planRevenueQ1 from Integra_Revenue_Tracking__c where Name =: nextYear];
        
        System.debug('Revenues' +revenueTracking.get('revenueQ1') +revenueTracking.get('revenueQ2') +revenueTracking.get('revenueQ3') +revenueTracking.get('revenueQ4'));
    }
    
    
    public List<revenueData> getRevenueData(){
        
        /* AggregateResult payorRevenueQ1 = [Select SUM(PWP_1__c)pwpQ1, SUM(Q1_Revenue__c)revenueQ1, SUM(Q1_Plan_Revenue__c)planRevenueQ1 from Payor_Revenue_Tracking__c];
        AggregateResult payorRevenueQ2 = [Select SUM(PWP_2__c)pwpQ2, SUM(Q2_Revenue__c)revenueQ2, SUM(Q2_Plan_Revenue__c)planRevenueQ2 from Payor_Revenue_Tracking__c];
        AggregateResult payorRevenueQ3 = [Select SUM(PWP_3__c)pwpQ3, SUM(Q3_Revenue__c)revenueQ3, SUM(Q3_Plan_Revenue__c)planRevenueQ3 from Payor_Revenue_Tracking__c];
        AggregateResult payorRevenueQ4 = [Select SUM(PWP_4__c)pwpQ4, SUM(Q4_Revenue__c)revenueQ4, SUM(Q4_Plan_Revenue__c)planRevenueQ4 from Payor_Revenue_Tracking__c]; */
        
        
        
        
        List<revenueData> allRevenues = new List<revenueData>();
        
        allRevenues.add(new revenueData((currentYear+' ' + 'Q1'), finalQ1, Double.valueOf(revenueTracking.get('revenueQ1')), Double.valueOf(revenueTracking.get('planRevenueQ1'))));
        allRevenues.add(new revenueData((currentYear+' ' + 'Q2'), finalQ2, Double.valueOf(revenueTracking.get('revenueQ2')), Double.valueOf(revenueTracking.get('planRevenueQ2'))));
        allRevenues.add(new revenueData((currentYear+' ' + 'Q3'), finalQ3, Double.valueOf(revenueTracking.get('revenueQ3')), Double.valueOf(revenueTracking.get('planRevenueQ3'))));
        allRevenues.add(new revenueData((currentYear+' ' + 'Q4'), finalQ4, Double.valueOf(revenueTracking.get('revenueQ4')), Double.valueOf(revenueTracking.get('planRevenueQ4'))));
        allRevenues.add(new revenueData((nextYear+' ' + 'Q1'), finalNextQ1, Double.valueOf(nextYearRevenue.get('revenueQ1')), Double.valueOf(nextYearRevenue.get('planRevenueQ1'))));
        return allRevenues;

        // For Dashboard -- 4

          }
    public class revenueData{
        public String name{get;set;}
        public Decimal PWPWMS{get;set;}
        public Decimal ActualRevenue{get;set;}
        public Decimal PlanRevenue{get;set;}
        public Decimal axisValue{get; set;}
        
        public revenueData(String name, Double PWPWMS, Double ActualRevenue, Double PlanRevenue){
            //String s = ( Decimal.valueOf(payorRevenue==null||payorRevenue.trim()==''?'0':payorRevenue).setScale(2) + 0.001 ).format();
            this.name = name;
            this.PWPWMS = (PWPWMS != null) ? (((Decimal)(PWPWMS/1000000)).setScale(2)) : 0;
            this.ActualRevenue = (ActualRevenue!= null) ? (((Decimal)(ActualRevenue/1000000)).setScale(2)) : 0;
            this.PlanRevenue = (PlanRevenue!= null) ? (((Decimal)(PlanRevenue/1000000)).setScale(2)) : 0;
            
            
            // '$' + ' ' + 's.substring(0,s.length()-1)'
        }
    }

    public class cumulativePWP{
        public String name{get;set;}
        public Decimal PWPQ1{get;set;}
        public Decimal PWPQ2{get;set;}
        public Decimal PWPQ3{get;set;}
        public Decimal PWPQ4{get;set;}
        public Decimal ActualRevenue{get;set;}
        public Decimal PlanRevenue{get;set;}
        //public Decimal axisValue{get; set;}


        public cumulativePWP(String name, Double PWPQ1, Double PWPQ2, Double PWPQ3, Double PWPQ4, Double ActualRevenue, Double PlanRevenue){

            this.name = name;
            this.PWPQ1 = (PWPQ1 != null) ? (((Decimal)(PWPQ1/1000000)).setScale(2)) : 0;
            this.PWPQ2 = (PWPQ1 != null) ? (((Decimal)(PWPQ2/1000000)).setScale(2)) : 0;
            this.PWPQ3 = (PWPQ1 != null) ? (((Decimal)(PWPQ3/1000000)).setScale(2)) : 0;
            this.PWPQ4 = (PWPQ1 != null) ? (((Decimal)(PWPQ4/1000000)).setScale(2)) : 0;
            this.ActualRevenue = (ActualRevenue!= null) ? (((Decimal)(ActualRevenue/1000000)).setScale(2)) : 0;
            this.PlanRevenue = (PlanRevenue!= null) ? (((Decimal)(PlanRevenue/1000000)).setScale(2)) : 0;
        }
    }

    public List<cumulativePWP> getCumulativePWP(){

        List<cumulativePWP> allValues = new List<cumulativePWP>();
        
        allValues.add(new cumulativePWP((currentYear+' ' + 'Q1'), finalQ1, 0, 0, 0, Double.valueOf(revenueTracking.get('revenueQ1')), Double.valueOf(revenueTracking.get('planRevenueQ1'))));
        allValues.add(new cumulativePWP((currentYear+' ' + 'Q2'), finalQ1, finalQ2, 0, 0, Double.valueOf(revenueTracking.get('revenueQ2')), Double.valueOf(revenueTracking.get('planRevenueQ2'))));
        allValues.add(new cumulativePWP((currentYear+' ' + 'Q3'), finalQ1, finalQ2, finalQ3, 0, Double.valueOf(revenueTracking.get('revenueQ3')), Double.valueOf(revenueTracking.get('planRevenueQ3'))));
        allValues.add(new cumulativePWP((currentYear+' ' + 'Q4'), finalQ1, finalQ2, finalQ3, finalQ4, Double.valueOf(revenueTracking.get('revenueQ4')), Double.valueOf(revenueTracking.get('planRevenueQ4'))));
        //allRevenues.add(new revenueData((nextYear+' ' + 'Q1'), finalNextQ1, Double.valueOf(nextYearRevenue.get('revenueQ1')), Double.valueOf(nextYearRevenue.get('planRevenueQ1'))));
        return allValues;

    }
    
}