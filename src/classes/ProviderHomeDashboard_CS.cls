/**
 *  @Description A controller class for ProviderHomeDashboard_CS page
 *  @Author Cloud Software LLC
 *  Revision History:
 */

public without sharing class ProviderHomeDashboard_CS extends ProviderPortalServices_CS {

	private final String pageAccountType = 'Provider';

	private ProviderOrderServices_CS providerOrderServices; 
	
	/********* Constants *********/
	private static final Integer DISPLAY_N_RECENT_ORDERS = 7;
	
	/********* Page Params *********/
	//NOTE: Can use transient keyword?
	public Integer pendingAcceptanceCount {get;set;}
	public Integer inProgressCount {get;set;} 
	public Integer needsAttentionCount {get;set;}
	public Integer cancelledCount {get;set;}

	public String pendingAcceptanceCountString {get;set;}
	public String inProgressCountString {get;set;} 
	public String needsAttentionCountString {get;set;}
	public String cancelledCountString {get;set;}

	public String filter {get;set;}

    public Apexpages.Standardsetcontroller recentOrderSetCon {get;set;}
    public List<Order__c> recentOrderList
    {
        get{
            if(recentOrderSetCon != null){
                return (List<Order__c>) recentOrderSetCon.getRecords();
            }
            return new List<Order__c>();
        }
        set;
    }
	
	public String selectedOrderId {get;set;}
	
	/********* Constructor/Init *********/
	public ProviderHomeDashboard_CS(){

	}
	
	public PageReference init(){

		///////////////////////////////////
		// Authentication and Permission
		///////////////////////////////////
		
		if(!isSecure && !Test.isRunningTest()){
			return DoLogout();
		}
		
		if(!authenticated){
			return DoLogout();
		}

		if(portalUser.AccountType != pageAccountType){
			return homepage;
		}
		
		///////////////////////////////////
		// Additional init
		///////////////////////////////////	

		providerOrderServices = new ProviderOrderServices_CS(portalUser.instance.Id);

		// Only look for orders with stage "Pending Acceptance"
		Map<String,String> filter = new Map<String,String>{ 
			OrderModel_CS.STAGE => OrderModel_CS.STAGE_PENDING_ACCEPTANCE
		};

		recentOrderSetCon = providerOrderServices.getNewestOrders(DISPLAY_N_RECENT_ORDERS, filter);
		
		updateOrderCounts();		
		return null;
	}
	
	public void updateOrderCounts(){

		pendingAcceptanceCount = 0;
		inProgressCount = 0;
		needsAttentionCount = 0;
		cancelledCount = 0;
		
		for(AggregateResult ar : providerOrderServices.returnAggregateOrdersGroupedByStatus()){
			
			System.debug('Order Results: Status ' + ar.get('Status__c') + ' Count ' + ar.get('expr0'));
			
			if(ar.get('Status__c') != null){
				if(OrderModel_CS.orderStatusToStage.get(String.valueOf(ar.get('Status__c'))) == OrderModel_CS.STAGE_NEEDS_ATTENTION){
					needsAttentionCount += Integer.valueOf(ar.get('expr0'));
				}
				else if(OrderModel_CS.orderStatusToStage.get(String.valueOf(ar.get('Status__c'))) == OrderModel_CS.STAGE_IN_PROGRESS){
					inProgressCount += Integer.valueOf(ar.get('expr0'));
				}
				else if(OrderModel_CS.orderStatusToStage.get(String.valueOf(ar.get('Status__c'))) == OrderModel_CS.STAGE_PENDING_ACCEPTANCE){
					pendingAcceptanceCount += Integer.valueOf(ar.get('expr0'));
				}
				else if(OrderModel_CS.orderStatusToStage.get(String.valueOf(ar.get('Status__c'))) == OrderModel_CS.STAGE_CANCELLED){
					cancelledCount += Integer.valueOf(ar.get('expr0'));
				}
			}
		}
		
		pendingAcceptanceCountString = String.valueOf(pendingAcceptanceCount);
		inProgressCountString = String.valueOf(inProgressCount);
		needsAttentionCountString = String.valueOf(needsAttentionCount);
		cancelledCountString = String.valueOf(cancelledCount);
		
	}//End updateOrderCounts

}//END ProviderHomeDashboardController_CS