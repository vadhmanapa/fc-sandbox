@isTest
private class TestUtilityClassForIP {
    static testMethod void TestUtilityClassForIP(){
        // Create a test String
        String[] testString = new String[]{'test@string.com;testing@abcd.com;tested@xyz.com','abcd','123'};
        String[] testString2 = new String[]{};
        //call utility class
        UtilityClassForIP testSplit = new UtilityClassForIP();
        String testResult = testSplit.StringSplitter(testString);
        String testResult2 = testSplit.StringSplitter(testString2);
        // assert the test results
        system.assertEquals('test@string.com;testing@abcd.com;tested@xyz.com;abcd;123', 
                            testResult, 'If equal then string was split');
        system.assertEquals(null, testResult2, 'If equal then test for null string is pass');
    }

}