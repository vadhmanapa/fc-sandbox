public class OrderBatch_CS implements Database.Batchable<SObject>{ 

	String queryString;

	public OrderBatch_CS(String query) {
		this.queryString = query;
	}

	public OrderBatch_CS() {

		SoqlBuilder query = new SoqlBuilder()
			.selectx('Id')
			.selectx('Provider_Locations__c')
			.fromx('Order__c')
			.wherex(new FieldCondition('Provider_Location__c').equals(null));
			
		queryString = query.toSoql();

	}

	public Database.QueryLocator start(Database.BatchableContext context) {
		
		return Database.getQueryLocator(queryString);
	}

	public void execute(Database.BatchableContext context, List<Order__c> scope) {
		System.debug(scope);
		List<Order__c> ordList = ProviderLocationTransferServices.updateOrderProviderLocation(scope);
		update ordList;
	}

	public void finish(Database.BatchableContext context) {

	}
}