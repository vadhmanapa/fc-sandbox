@isTest
private class CaseActionControllerExtensionv2Test_IP {
    
    public static TestCaseDataFactory_IP generateNewData;
    public static CaseActionControllerExtensionv2_IP caseActionCon;
    private static User testUser;

    @isTest static void testLoadControllerComponents()
    {
        generateNewData = new TestCaseDataFactory_IP();
        generateNewData.initTestData();
        testUser = TestCaseDataFactory_IP.csUser;

        Case testCase = new Case();
        Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Payor0' LIMIT 1];
        Account prAcc = [SELECT Id, Name FROM Account WHERE Name = 'Provider0' LIMIT 1];
        Contact c = [SELECT Id, Name, Birthdate,FirstName, LastName, Account.Name, AccountId, RecordTypeId, Account.Status__c, Phone, Email, Health_Plan_ID__c 
                    FROM Contact WHERE Health_Plan_ID__c= '123450' AND AccountId =: acc.Id LIMIT 1];

        Case cas = new Case();

        cas.AccountId = acc.Id;
        cas.ContactId = c.Id;
        cas.Subject = 'This is test';
        cas.Description = 'This is test';
        cas.CallerType__c = 'Patient';
        cas.RecordTypeId = RecordTypes.caseTypes.get('Customer Service Call').getRecordTypeId();
        //cas.CaseReasonDetailId__c = TestCaseDataFactory_IP.reasonDetail[0].Id;
        //cas.CaseActionId__c = TestCaseDataFactory_IP.actionAndDetails[0].Id;
        
        insert cas;

        Case_Action__c testAct = new Case_Action__c();

        CaseReasonComponentController_IP testReasonComp = new CaseReasonComponentController_IP();
        CaseActionandDetailsController_IP testCaseActionComp = new CaseActionandDetailsController_IP();

        System.runAs(testUser){
            PageReference testPg = Page.CaseActionOverride_IP;
            Test.setCurrentPage(testPg);
            testPg.getParameters().put('cId', String.valueOf(cas.Id));
            ApexPages.StandardController testCon = new ApexPages.StandardController(testAct);
            
            Test.startTest();
            caseActionCon = new CaseActionControllerExtensionv2_IP(testCon);
            caseActionCon.reasonCompController = testReasonComp;
            caseActionCon.actionandDetails = testCaseActionComp;
            Test.stopTest();
        }

        System.assertNotEquals(null, testReasonComp, 'The reason component and controller was loaded');
        System.assertNotEquals(null, testCaseActionComp, 'The reason case action and details and controller was loaded');
        System.assertNotEquals(null, caseActionCon.caseLog.userAction.Case__c);
        System.assertEquals(cas.Id, caseActionCon.caseLog.userAction.Case__c);
    }

    /*@isTest static void testCaseActionWithNoReason(){

        generateNewData = new TestCaseDataFactory_IP();
        generateNewData.initTestData();
        testUser = TestCaseDataFactory_IP.csUser;

        Case testCase = new Case();
        Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Payor0' LIMIT 1];
        Account prAcc = [SELECT Id, Name FROM Account WHERE Name = 'Provider0' LIMIT 1];
        Contact c = [SELECT Id, Name, Birthdate,FirstName, LastName, Account.Name, AccountId, RecordTypeId, Account.Status__c, Phone, Email, Health_Plan_ID__c 
                    FROM Contact WHERE Health_Plan_ID__c= '123450' AND AccountId =: acc.Id LIMIT 1];

        Case cas = new Case();

        cas.AccountId = acc.Id;
        cas.ContactId = c.Id;
        cas.Subject = 'This is test';
        cas.Description = 'This is test';
        cas.CallerType__c = 'Patient';
        cas.RecordTypeId = RecordTypes.caseTypes.get('Customer Service Call').getRecordTypeId();
        //cas.CaseReasonDetailId__c = TestCaseDataFactory_IP.reasonDetail[0].Id;
        //cas.CaseActionId__c = TestCaseDataFactory_IP.actionAndDetails[0].Id;
        
        insert cas;

        Case_Action__c testAct = new Case_Action__c();

        CaseReasonComponentController_IP testReasonComp = new CaseReasonComponentController_IP();
        CaseActionandDetailsController_IP testCaseActionComp = new CaseActionandDetailsController_IP();

        PageReference testResult;

        System.runAs(testUser){

            PageReference testPg = Page.CaseActionOverride_IP;
            Test.setCurrentPage(testPg);
            testPg.getParameters().put('cId', String.valueOf(cas.Id));
            ApexPages.StandardController testCon = new ApexPages.StandardController(testAct);
            caseActionCon = new CaseActionControllerExtension_IP(testCon);
            caseActionCon.reasonCompController = new CaseReasonComponentController_IP();
            caseActionCon.reasonCompController.recordTypeId = cas.RecordTypeId;
            caseActionCon.reasonCompController.selectedCallerType = 'Patient';

            Test.startTest();
            testResult = caseActionCon.save();
            Test.stopTest();
        }

        System.assertEquals(null, testResult);

    }*/

    @isTest static void testCaseActionWithNoReasonDetail(){

        generateNewData = new TestCaseDataFactory_IP();
        generateNewData.initTestData();
        generateNewData.generateReasonsAndActions();
        testUser = TestCaseDataFactory_IP.csUser;

        Case testCase = new Case();
        Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Payor0' LIMIT 1];
        Account prAcc = [SELECT Id, Name FROM Account WHERE Name = 'Provider0' LIMIT 1];
        Contact c = [SELECT Id, Name, Birthdate,FirstName, LastName, Account.Name, AccountId, RecordTypeId, Account.Status__c, Phone, Email, Health_Plan_ID__c 
                    FROM Contact WHERE Health_Plan_ID__c= '123450' AND AccountId =: acc.Id LIMIT 1];

        Case cas = new Case();

        cas.AccountId = acc.Id;
        cas.ContactId = c.Id;
        cas.Subject = 'This is test';
        cas.Description = 'This is test';
        cas.CallerType__c = 'Patient';
        cas.RecordTypeId = RecordTypes.caseTypes.get('Customer Service Call').getRecordTypeId();
        //cas.CaseReasonDetailId__c = TestCaseDataFactory_IP.reasonDetail[0].Id;
        //cas.CaseActionId__c = TestCaseDataFactory_IP.actionAndDetails[0].Id;
        
        insert cas;

        Case_Action__c testAct = new Case_Action__c();

        PageReference testResult;

        System.runAs(testUser){

            PageReference testPg = Page.CaseActionOverride_IP;
            Test.setCurrentPage(testPg);
            testPg.getParameters().put('cId', String.valueOf(cas.Id));
            ApexPages.StandardController testCon = new ApexPages.StandardController(testAct);
            caseActionCon = new CaseActionControllerExtensionv2_IP(testCon);

            caseActionCon.reasonCompController = new CaseReasonComponentController_IP();
            caseActionCon.reasonCompController.recordTypeId = cas.RecordTypeId;
            caseActionCon.reasonCompController.selectedCallerType = 'Patient';
            
            //CaseReason__c cr = [SELECT Id FROM CaseReason__c WHERE Id=:TestCaseDataFactory_IP.reasons[0].Id LIMIT 1];
            //caseActionCon.reasonCompController.getReasons();
            caseActionCon.reasonCompController.reasonSelected = TestCaseDataFactory_IP.reasons[0].Id ;
            
            //caseActionCon.reasonCompController.getReasonDetails();
            //CaseReasonDetail__c crd = [SELECT Id FROM CaseReasonDetail__c WHERE Id=: TestCaseDataFactory_IP.reasonDetail[0].Id LIMIT 1];
            caseActionCon.reasonCompController.reasonDetailSelected = TestCaseDataFactory_IP.reasonDetail[0].Id;
            caseActionCon.reasonCompController.setParentControllerValues();

            ActionDetail__c testAction= [SELECT Id, Name, Action__c FROM ActionDetail__c WHERE Id=: TestCaseDataFactory_IP.actionAndDetails[0].Id];
            
            caseActionCon.actionandDetails = new CaseActionandDetailsController_IP();
            caseActionCon.actionandDetails.reasonDetailSelected = TestCaseDataFactory_IP.reasonDetail[0].Id;
            
            //caseActionCon.actionandDetails.getActions();
            caseActionCon.actionandDetails.actionSelected = testAction.Action__c;
            //caseActionCon.actionandDetails.getActionDetails();
            caseActionCon.actionAndDetails.actionDetailSelected = testAction.Id;

            Test.startTest();
            testResult = caseActionCon.save();
            Test.stopTest();
        }

        System.assertEquals(null, testResult);
    }

    @isTest static void testInsertNewCaseAction(){

        generateNewData = new TestCaseDataFactory_IP();
        generateNewData.initTestData();
        generateNewData.generateReasonsAndActions();
        testUser = TestCaseDataFactory_IP.csUser;

        Case testCase = new Case();
        Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Payor0' LIMIT 1];
        Account prAcc = [SELECT Id, Name FROM Account WHERE Name = 'Provider0' LIMIT 1];
        Contact c = [SELECT Id, Name, Birthdate,FirstName, LastName, Account.Name, AccountId, RecordTypeId, Account.Status__c, Phone, Email, Health_Plan_ID__c 
                    FROM Contact WHERE Health_Plan_ID__c= '123450' AND AccountId =: acc.Id LIMIT 1];

        Case cas = new Case();

        cas.AccountId = acc.Id;
        cas.ContactId = c.Id;
        cas.Subject = 'This is test';
        cas.Description = 'This is test';
        cas.CallerType__c = 'Patient';
        cas.RecordTypeId = RecordTypes.caseTypes.get('Customer Service Call').getRecordTypeId();
        
        insert cas;

        Case_Action__c testAct = new Case_Action__c();

        PageReference testResult;

        System.runAs(testUser){

            PageReference testPg = Page.CaseActionOverride_IP;
            Test.setCurrentPage(testPg);
            testPg.getParameters().put('cId', String.valueOf(cas.Id));
            ApexPages.StandardController testCon = new ApexPages.StandardController(testAct);
            caseActionCon = new CaseActionControllerExtensionv2_IP(testCon);

            caseActionCon.reasonCompController = new CaseReasonComponentController_IP();
            caseActionCon.reasonCompController.recordTypeId = cas.RecordTypeId;
            caseActionCon.reasonCompController.selectedCallerType = 'Patient';
            
            CaseReason__c cr = [SELECT Id,Reason__c FROM CaseReason__c WHERE Id=:TestCaseDataFactory_IP.reasons[0].Id LIMIT 1];
            caseActionCon.caseLog.caseDetails.CaseReason__c =  cr.Reason__c;
            caseActionCon.caseLog.caseDetails.CaseReasonId__c = cr.Id;
            
            //caseActionCon.reasonCompController.getReasonDetails();
            CaseReasonDetail__c crd = [SELECT Id, CaseReasonDetail__c FROM CaseReasonDetail__c WHERE Id=: TestCaseDataFactory_IP.reasonDetail[0].Id LIMIT 1];
            caseActionCon.reasonCompController.reasonDetailSelected = TestCaseDataFactory_IP.reasonDetail[0].Id;
            caseActionCon.reasonCompController.setParentControllerValues();

            caseActionCon.caseLog.caseDetails.CaseReasonDetail__c = crd.CaseReasonDetail__c;
            caseActionCon.caseLog.caseDetails.CaseReasonDetailId__c = crd.Id;

            ActionDetail__c testAction= [SELECT Id, Name, Action__c, ActionDetail__c FROM ActionDetail__c WHERE Id=: TestCaseDataFactory_IP.actionAndDetails[0].Id];
            
            /*caseActionCon.actionandDetails = new CaseActionandDetailsController_IP();
            caseActionCon.actionandDetails.reasonDetailSelected = TestCaseDataFactory_IP.reasonDetail[0].Id;
            caseActionCon.caseLog.userAction.CaseAction__c = testAction.Action__c;
            caseActionCon.caseLog.userAction.CaseActionDetail__c = testAction.ActionDetail__c;
            caseActionCon.caseLog.userAction.Case_Comment__c = 'Test Action';*/
            caseActionCon.actionDetailId = testAction.Id;
            //caseActionCon.actionandDetails.getActions();
            //caseActionCon.actionandDetails.actionSelected = testAction.Action__c;
            //caseActionCon.actionandDetails.getActionDetails();
            //caseActionCon.actionAndDetails.actionDetailSelected = testAction.Id;

            Test.startTest();
            testResult = caseActionCon.save();
            Test.stopTest();
        }

        System.assertNotEquals(null, testResult);
    }

    @isTest static void testNoCaseReasonDetailError(){

        generateNewData = new TestCaseDataFactory_IP();
        generateNewData.initTestData();
        generateNewData.generateReasonsAndActions();
        testUser = TestCaseDataFactory_IP.csUser;

        Case testCase = new Case();
        Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Payor0' LIMIT 1];
        Account prAcc = [SELECT Id, Name FROM Account WHERE Name = 'Provider0' LIMIT 1];
        Contact c = [SELECT Id, Name, Birthdate,FirstName, LastName, Account.Name, AccountId, RecordTypeId, Account.Status__c, Phone, Email, Health_Plan_ID__c 
                    FROM Contact WHERE Health_Plan_ID__c= '123450' AND AccountId =: acc.Id LIMIT 1];

        Case cas = new Case();

        cas.AccountId = acc.Id;
        cas.ContactId = c.Id;
        cas.Subject = 'This is test';
        cas.Description = 'This is test';
        cas.CallerType__c = 'Patient';
        cas.RecordTypeId = RecordTypes.caseTypes.get('Customer Service Call').getRecordTypeId();
        
        insert cas;

        Case_Action__c testAct = new Case_Action__c();

        PageReference testResult;

        System.runAs(testUser){

            PageReference testPg = Page.CaseActionOverride_IP;
            Test.setCurrentPage(testPg);
            testPg.getParameters().put('cId', String.valueOf(cas.Id));
            ApexPages.StandardController testCon = new ApexPages.StandardController(testAct);
            caseActionCon = new CaseActionControllerExtensionv2_IP(testCon);

            caseActionCon.reasonCompController = new CaseReasonComponentController_IP();
            caseActionCon.reasonCompController.recordTypeId = cas.RecordTypeId;
            caseActionCon.reasonCompController.selectedCallerType = 'Patient';
            
            CaseReason__c cr = [SELECT Id,Reason__c FROM CaseReason__c WHERE Id=:TestCaseDataFactory_IP.reasons[0].Id LIMIT 1];
            caseActionCon.caseLog.caseDetails.CaseReason__c =  cr.Reason__c;
            caseActionCon.caseLog.caseDetails.CaseReasonId__c = cr.Id;
            
            //caseActionCon.reasonCompController.getReasonDetails();
            /*CaseReasonDetail__c crd = [SELECT Id, CaseReasonDetail__c FROM CaseReasonDetail__c WHERE Id=: TestCaseDataFactory_IP.reasonDetail[0].Id LIMIT 1];
            caseActionCon.reasonCompController.reasonDetailSelected = TestCaseDataFactory_IP.reasonDetail[0].Id;
            caseActionCon.reasonCompController.setParentControllerValues();*/

            Test.startTest();
            testResult = caseActionCon.save();
            Test.stopTest();
        }

        System.assertEquals(null, testResult);
    }

    @isTest static void testNoCaseActionError(){

        generateNewData = new TestCaseDataFactory_IP();
        generateNewData.initTestData();
        generateNewData.generateReasonsAndActions();
        testUser = TestCaseDataFactory_IP.csUser;

        Case testCase = new Case();
        Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Payor0' LIMIT 1];
        Account prAcc = [SELECT Id, Name FROM Account WHERE Name = 'Provider0' LIMIT 1];
        Contact c = [SELECT Id, Name, Birthdate,FirstName, LastName, Account.Name, AccountId, RecordTypeId, Account.Status__c, Phone, Email, Health_Plan_ID__c 
                    FROM Contact WHERE Health_Plan_ID__c= '123450' AND AccountId =: acc.Id LIMIT 1];

        Case cas = new Case();

        cas.AccountId = acc.Id;
        cas.ContactId = c.Id;
        cas.Subject = 'This is test';
        cas.Description = 'This is test';
        cas.CallerType__c = 'Patient';
        cas.RecordTypeId = RecordTypes.caseTypes.get('Customer Service Call').getRecordTypeId();
        
        insert cas;

        Case_Action__c testAct = new Case_Action__c();

        PageReference testResult;

        System.runAs(testUser){

            PageReference testPg = Page.CaseActionOverride_IP;
            Test.setCurrentPage(testPg);
            testPg.getParameters().put('cId', String.valueOf(cas.Id));
            ApexPages.StandardController testCon = new ApexPages.StandardController(testAct);
            caseActionCon = new CaseActionControllerExtensionv2_IP(testCon);

            caseActionCon.reasonCompController = new CaseReasonComponentController_IP();
            caseActionCon.reasonCompController.recordTypeId = cas.RecordTypeId;
            caseActionCon.reasonCompController.selectedCallerType = 'Patient';
            
            CaseReason__c cr = [SELECT Id,Reason__c FROM CaseReason__c WHERE Id=:TestCaseDataFactory_IP.reasons[0].Id LIMIT 1];
            caseActionCon.caseLog.caseDetails.CaseReason__c =  cr.Reason__c;
            CaseReasonDetail__c crd = [SELECT Id, CaseReasonDetail__c FROM CaseReasonDetail__c WHERE Id=: TestCaseDataFactory_IP.reasonDetail[0].Id LIMIT 1];
            caseActionCon.caseLog.caseDetails.CaseReasonDetail__c = crd.CaseReasonDetail__c;
            
            //caseActionCon.reasonCompController.getReasonDetails();
            /*CaseReasonDetail__c crd = [SELECT Id, CaseReasonDetail__c FROM CaseReasonDetail__c WHERE Id=: TestCaseDataFactory_IP.reasonDetail[0].Id LIMIT 1];
            caseActionCon.reasonCompController.reasonDetailSelected = TestCaseDataFactory_IP.reasonDetail[0].Id;
            caseActionCon.reasonCompController.setParentControllerValues();*/

            ActionDetail__c testAction= [SELECT Id, Name, Action__c, ActionDetail__c FROM ActionDetail__c WHERE Id=: TestCaseDataFactory_IP.actionAndDetails[0].Id];
            caseActionCon.caseLog.userAction.CaseAction__c = '';
            caseActionCon.caseLog.userAction.CaseActionDetail__c = testAction.ActionDetail__c;

            Test.startTest();
            testResult = caseActionCon.save();
            Test.stopTest();
        }

        System.assertEquals(null, testResult);
    }

    @isTest static void testNoCaseActionDetailError(){

        generateNewData = new TestCaseDataFactory_IP();
        generateNewData.initTestData();
        generateNewData.generateReasonsAndActions();
        testUser = TestCaseDataFactory_IP.csUser;

        Case testCase = new Case();
        Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Payor0' LIMIT 1];
        Account prAcc = [SELECT Id, Name FROM Account WHERE Name = 'Provider0' LIMIT 1];
        Contact c = [SELECT Id, Name, Birthdate,FirstName, LastName, Account.Name, AccountId, RecordTypeId, Account.Status__c, Phone, Email, Health_Plan_ID__c 
                    FROM Contact WHERE Health_Plan_ID__c= '123450' AND AccountId =: acc.Id LIMIT 1];

        Case cas = new Case();

        cas.AccountId = acc.Id;
        cas.ContactId = c.Id;
        cas.Subject = 'This is test';
        cas.Description = 'This is test';
        cas.CallerType__c = 'Patient';
        cas.RecordTypeId = RecordTypes.caseTypes.get('Customer Service Call').getRecordTypeId();
        
        insert cas;

        Case_Action__c testAct = new Case_Action__c();

        PageReference testResult;

        System.runAs(testUser){

            PageReference testPg = Page.CaseActionOverride_IP;
            Test.setCurrentPage(testPg);
            testPg.getParameters().put('cId', String.valueOf(cas.Id));
            ApexPages.StandardController testCon = new ApexPages.StandardController(testAct);
            caseActionCon = new CaseActionControllerExtensionv2_IP(testCon);

            caseActionCon.reasonCompController = new CaseReasonComponentController_IP();
            caseActionCon.reasonCompController.recordTypeId = cas.RecordTypeId;
            caseActionCon.reasonCompController.selectedCallerType = 'Patient';
            
            CaseReason__c cr = [SELECT Id,Reason__c FROM CaseReason__c WHERE Id=:TestCaseDataFactory_IP.reasons[0].Id LIMIT 1];
            caseActionCon.caseLog.caseDetails.CaseReason__c =  cr.Reason__c;
            CaseReasonDetail__c crd = [SELECT Id, CaseReasonDetail__c FROM CaseReasonDetail__c WHERE Id=: TestCaseDataFactory_IP.reasonDetail[0].Id LIMIT 1];
            caseActionCon.caseLog.caseDetails.CaseReasonDetail__c = crd.CaseReasonDetail__c;
            
            //caseActionCon.reasonCompController.getReasonDetails();
            /*CaseReasonDetail__c crd = [SELECT Id, CaseReasonDetail__c FROM CaseReasonDetail__c WHERE Id=: TestCaseDataFactory_IP.reasonDetail[0].Id LIMIT 1];
            caseActionCon.reasonCompController.reasonDetailSelected = TestCaseDataFactory_IP.reasonDetail[0].Id;
            caseActionCon.reasonCompController.setParentControllerValues();*/

            ActionDetail__c testAction= [SELECT Id, Name, Action__c, ActionDetail__c FROM ActionDetail__c WHERE Id=: TestCaseDataFactory_IP.actionAndDetails[0].Id];
            caseActionCon.caseLog.userAction.CaseAction__c = testAction.Action__c;
            caseActionCon.caseLog.userAction.CaseActionDetail__c = '';

            Test.startTest();
            testResult = caseActionCon.save();
            Test.stopTest();
        }

        System.assertEquals(null, testResult);
    }

    @isTest static void testCancelAction(){

        generateNewData = new TestCaseDataFactory_IP();
        generateNewData.initTestData();
        generateNewData.generateReasonsAndActions();
        testUser = TestCaseDataFactory_IP.csUser;

        Case testCase = new Case();
        Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Payor0' LIMIT 1];
        Account prAcc = [SELECT Id, Name FROM Account WHERE Name = 'Provider0' LIMIT 1];
        Contact c = [SELECT Id, Name, Birthdate,FirstName, LastName, Account.Name, AccountId, RecordTypeId, Account.Status__c, Phone, Email, Health_Plan_ID__c 
                    FROM Contact WHERE Health_Plan_ID__c= '123450' AND AccountId =: acc.Id LIMIT 1];

        Case cas = new Case();

        cas.AccountId = acc.Id;
        cas.ContactId = c.Id;
        cas.Subject = 'This is test';
        cas.Description = 'This is test';
        cas.CallerType__c = 'Patient';
        cas.RecordTypeId = RecordTypes.caseTypes.get('Customer Service Call').getRecordTypeId();
        
        insert cas;

        Case_Action__c testAct = new Case_Action__c();

        PageReference testResult;

        System.runAs(testUser){

            PageReference testPg = Page.CaseActionOverride_IP;
            Test.setCurrentPage(testPg);
            testPg.getParameters().put('cId', String.valueOf(cas.Id));
            ApexPages.StandardController testCon = new ApexPages.StandardController(testAct);
            caseActionCon = new CaseActionControllerExtensionv2_IP(testCon);

            Test.startTest();
            testResult = caseActionCon.cancel();
            Test.stopTest();
        }

        System.assertNotEquals(null, testResult);
    }
}