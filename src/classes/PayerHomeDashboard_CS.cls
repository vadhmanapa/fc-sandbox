public without sharing class PayerHomeDashboard_CS extends PayerPortalServices_CS {
	
	private final String pageAccountType = 'Payer';
	
	private PayerOrderServices_CS payerOrderServices;	
	
	public Contact currentUser{get;set;}
	
	/******** Interface *******/	
	public String orderSearchString {get;set;}
	
	public String completedCountString {get;set;}
	public String pendingAcceptCountString {get;set;}
	public String inProgressCountString {get;set;}
	public String needsAttentionCountString {get;set;}
	public String recurringCountString {get;set;}
	public String cancelledCountString {get;set;}
	
	// NOTE: If there are changes to the status mappings, do so here and in "updateOrderCounts"
	
	public List<SelectOption> lookbackTimes {
		get{
			return new List<SelectOption>{
				new SelectOption('today','Today'),
				new SelectOption('thisweek','Week To Date'),
				new SelectOption('lastweek','Last Week'),
				new SelectOption('thismonth','Month To Date'),
				new SelectOption('lastmonth','Last Month')
			};
		}
		private set;
	}
	public String lookbackTime {get;set;}
	
	public String filter {get;set;}
	
	/**************************/
	
	public PayerHomeDashboard_CS(){}
	
	//Required Page action
	public PageReference init(){
		
		///////////////////////////////////
		// Authentication and Permission
		///////////////////////////////////
		
		if(!isSecure && !Test.isRunningTest()){
			return DoLogout();
		}
				
		if(!authenticated){
			return DoLogout();
		}

		if(portalUser.AccountType != pageAccountType){
			return homepage;
		}
		
		///////////////////////////////////
		// Additional init
		///////////////////////////////////	

		payerOrderServices = new PayerOrderServices_CS(portalUser.instance.Id);		

		// Default to today;
		lookbackTime = 'today';
		
		updateOrderCounts();
		
		return null;
	}
	
	public PageReference updateOrderCounts(){

		Integer completedCount = 0;
		Integer pendingAcceptCount = 0;
		Integer inProgressCount = 0;
		Integer needsAttentionCount = 0;
		Integer recurringCount = 0;
		Integer cancelledCount = 0;

		Date startOfWeek;
		
		if(Date.today().toStartOfWeek() == Date.today()){
			startOfWeek = Date.today().addDays(-6);
		} else {
			startOfWeek = Date.today().toStartOfWeek().addDays(1);
		}

		Date startDate;
		Date stopDate;
		
		// TODO: Use a map instead of this large if/else block
		//Note: Map adds just as much complexity. You're adding two 1->2 mapping
		if(lookbackTime == 'today'){
			startDate = Date.today();
			stopDate = Date.today().addDays(1);
		}		
		else if(lookbackTime == 'thisweek'){
			//Sets start to Monday
			startDate = startOfWeek;
			stopDate = startOfWeek.addDays(7);
		}
		else if(lookbackTime == 'lastweek'){
			startDate = startOfWeek.addDays(-7);
			// Keep in mind we use Monday for the SoW, Sunday is end of last week
			stopDate = startOfWeek;
		}
		else if(lookbackTime == 'thismonth'){
			startDate = Date.today().toStartOfMonth();
			stopDate = Date.today().toStartOfMonth().addMonths(1);
		}
		else if(lookbackTime == 'lastmonth'){
			startDate = Date.today().toStartOfMonth().addMonths(-1);
			stopDate = Date.today().toStartOfMonth();
		}
		
		for(AggregateResult ar : payerOrderServices.returnAggregateOrdersGroupedByStatus(startDate,stopDate)){
			
			System.debug('Status: ' + String.valueOf(ar.get('Status__c')) + ' Stage: ' + OrderModel_CS.orderStatusToStage.get(String.valueOf(ar.get('Status__c'))) + ' count: ' + Integer.valueOf(ar.get('expr0')));
			
			if(ar.get('Status__c') != null){								
				if(OrderModel_CS.orderStatusToStage.get(String.valueOf(ar.get('Status__c'))) == OrderModel_CS.STAGE_COMPLETED){
					completedCount += Integer.valueOf(ar.get('expr0'));
				}
				else if(OrderModel_CS.orderStatusToStage.get(String.valueOf(ar.get('Status__c'))) == OrderModel_CS.STAGE_NEEDS_ATTENTION){
					needsAttentionCount += Integer.valueOf(ar.get('expr0'));
				}
				else if(OrderModel_CS.orderStatusToStage.get(String.valueOf(ar.get('Status__c'))) == OrderModel_CS.STAGE_IN_PROGRESS){
					inProgressCount += Integer.valueOf(ar.get('expr0'));
				}
				else if(OrderModel_CS.orderStatusToStage.get(String.valueOf(ar.get('Status__c'))) == OrderModel_CS.STAGE_PENDING_ACCEPTANCE){
					pendingAcceptCount += Integer.valueOf(ar.get('expr0'));
				}
				else if(OrderModel_CS.orderStatusToStage.get(String.valueOf(ar.get('Status__c'))) == OrderModel_CS.STAGE_CANCELLED){
					cancelledCount += Integer.valueOf(ar.get('expr0'));
				}
				else if(OrderModel_CS.orderStatusToStage.get(String.valueOf(ar.get('Status__c'))) == OrderModel_CS.STAGE_RECURRING){
					recurringCount += Integer.valueOf(ar.get('expr0'));
				}
			}
			
			System.debug('Order Results: Status ' + ar.get('Status__c') + ' Count ' + ar.get('expr0') + '/ Stage ' + OrderModel_CS.orderStatusToStage.get(String.valueOf(ar.get('Status__c'))));
		}

		completedCountString = String.valueOf(completedCount);
		pendingAcceptCountString = String.valueOf(pendingAcceptCount);
		inProgressCountString = String.valueOf(inProgressCount);
		needsAttentionCountString = String.valueOf(needsAttentionCount);
		recurringCountString = String.valueOf(recurringCount);
		cancelledCountString = String.valueOf(cancelledCount);		
		
		return null;
	}
	//************* Not for Phase 2 ***************//
	/*
	public PageReference createNewOrder(){
		//TODO
		return null;
	}
	*/
}