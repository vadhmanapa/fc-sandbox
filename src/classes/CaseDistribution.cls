global with sharing class CaseDistribution {

	//Exception Handling
	public class CaseDistributionException extends Exception {}
	
	//Assignee
	//	Used for sorting based on special criteria for Cases owned by Users
	global class Assignee implements Comparable {
		global Id userId;
		global List<Case> cases;
		
		//Only set by constructor
		global Integer caseCount {get; private set;}
		global DateTime latestCreatedDate {get;private set;}
		
		global Assignee(Id userId, List<Case> cases) {
			
			//Do not continue if improper input
			if(userId == null) {
				throw new CaseDistributionException('Attempt to use Assignee with null user!');
			} else if(cases == null) {
				throw new CaseDistributionException('Attempt to use Assignee with null Case list!');
			}
			
			this.userId = userId;
			this.cases = cases;
			
			caseCount = cases.size();
			
			//Finish if no cases
			if(cases.size() == 0){
				return;
			}
			
			//Find most recent 'created date'
			latestCreatedDate = cases[0].CreatedDate;
			
			for(Case c : cases) {
				if(c.CreatedDate > latestCreatedDate) {
					latestCreatedDate = c.CreatedDate;
				}
			}
		}
		
		global void incrementCaseCount(){
			caseCount++;
		}
		
		global Integer compareTo(Object compareToObj) {
			
			Assignee compareTo = (Assignee)compareToObj;
			
			//Ascend by caseCount
			if(caseCount < compareTo.caseCount) {
				return -1;
			} else if(caseCount > compareTo.caseCount) {
				return 1;
			} else {
				//Case count is the same, ascend by latestCase				
				if(latestCreatedDate < compareTo.latestCreatedDate) {
					return -1;
				} else if(latestCreatedDate > compareTo.latestCreatedDate) {
					return 1;
				} else {
					return 0;
				}
			}
		}
	}

	public List<Assignee> assignees {get;set;}

	public CaseDistribution(Set<Id> userIds){
		
		if(userIds == null){
			throw new CaseDistributionException('Cannot create CaseDistribution with null userIds');
		}
		
		assignees = getAssignees(userIds);
	}

	public Id getNextAssignee() {
		
		if(assignees.size() == 0) {
			System.debug(LoggingLevel.WARN,'No customer service users to assign!');
			return null;
		}
		
		//Sort the list and return the zero-indexed value
		assignees.sort();
		
		//Simulated increment for the purposes of producing a correct 'next assignee'
		assignees[0].incrementCaseCount();
		
		return assignees[0].userId;
	}

	public static List<Assignee> getAssignees(Set<Id> userIds) {
		
		List<Assignee> assignees = new List<Assignee>();
		Map<Id,List<Case>> userIdToCases = new Map<Id,List<Case>>();
		
		for(Id userId : userIds) {
			userIdToCases.put(userId,new List<Case>());
		}
		
		for(Case c : [
			SELECT Id,CaseNumber,OwnerId,CreatedDate
			FROM Case 
			WHERE OwnerId IN :userIds
			ORDER BY CreatedDate ASC
		]){			
			userIdToCases.get(c.OwnerId).add(c);
		}
		
		for(Id userId : userIdToCases.keySet()) {
			assignees.add(new Assignee(userId,userIdToCases.get(userId)));
		}
		
		return assignees;
	}

	
	/* DEPRECATED 3.9.15 - ERIC MUNSON
	public static void assignCaseOwners(List<Case> records, Set<Id> userIds) {
		
		List<Case> toProcess = new List<Case>();
		String debugLog = 'Searching for owners...\n';	
        
        Set<Id> assigneeIds = userIds;
        
        if(assigneeIds.size() == 0) {
            debugLog += 'No Assignees found!\n';
            
            for(Case c : records) {
                CaseUtils.debugOnCase(c,debugLog);
            }
            return;
        }
        
        List<Assignee> assignees = getAssignees(assigneeIds); 
        
        for(Assignee a : assignees) {
			debugLog += 'Assignee: ' + a.Id + ' / Count: ' + a.caseCount + '\n';
		}
        
        Id nextAssignee = getNextAsignee(assignees);
            
        if(nextAssignee == null) {
            debugLog += 'No next assignee found!\n';
            
            for(Case c : records) {
                CaseUtils.debugOnCase(c,debugLog);
            }
            
            return;
        }
        
        debugLog += 'Found owner: ' + nextAssignee + '\n';
        
		//Check for record type
		for(Case c : records) {
			
			if(c.RecordTypeId != RecordTypes.emailId) {
				debugLog += 'No rules to process Case...\n';
				continue;
			}

			CaseUtils.debugOnCase(c,debugLog);
			c.OwnerId = nextAssignee;
		}
	}
	*/
}