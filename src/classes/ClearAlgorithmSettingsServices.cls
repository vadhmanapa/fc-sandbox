/**
 *	@Description Methods and variables for the Clear Algorithm Settings obect and processes.
 *	@Author Cloud Software LLC
 *	Revision History: 
 *		11/18/2015 - karnold - ICA-299 - Added constants for the status field. 
 *			Reformatted queries. Organized methods. Added comments.
 *			Added: blockNonDraftStatusForNewRecord, blockActivationIfNotApproved, blockDateFieldsForNewRecords, 
 *			and blockDateFieldUpdates
 *
 */
public without sharing class ClearAlgorithmSettingsServices {

	/* Constants */
	public static final String STATUS_DRAFT = 'Draft';
	public static final String STATUS_PENDING_APPROVAL = 'Pending Approval';
	public static final String STATUS_APPROVED = 'Approved';

	/* Validation on New Records */

	/**
	*	@Description (Used on insert) Adds an error to a record if active is checked.
	*/
	public static void blockActivationForNewRecord(Clear_Algorithm_Settings__c cas) {
		if (cas.Active__c) {
			cas.Active__c.addError('Only approved records can be activated.');
		}
	}

	/**
	*	@Description (Used on insert) Adds an error to a record if the status is not draft.
	*/
	public static void blockNonDraftStatusForNewRecord(Clear_Algorithm_Settings__c cas) {
		if (cas.Status__c != STATUS_DRAFT) {
			cas.Status__c.addError('New records can only have a status of "Draft".');
		}
	}

	/**
	*	@Description (Used on insert) Adds an error to a record if any date fields 
	*	(Activated_On__c, Approved_On__c, Deactivated_On__c) are not null.
	*/
	public static void blockDateFieldsForNewRecords(Clear_Algorithm_Settings__c cas) {
		if (cas.Activated_On__c != null) {
			cas.Activated_On__c.addError('New records cannot have an activated date.');
		} 
		if (cas.Approved_On__c != null) {
			cas.Approved_On__c.addError('New records cannot have an approved date.');
		} 
		if (cas.Deactivated_On__c != null) {
			cas.Deactivated_On__c.addError('New records cannot have an deactivated date.');
		} 
	}

	/* Sharing Updates */

	/**
	*	@Description	Adds shares for Clear_Algorithm_Settings__c
	*/
	public static void addReadPermissionsForActive(List<Clear_Algorithm_Settings__c> triggerNew){
		List<Clear_Algorithm_Settings__Share> casShares = new List<Clear_Algorithm_Settings__Share>();
		List<PermissionSetAssignment> psaList = [
			SELECT 
				PermissionSetId, 
				AssigneeId, 
				PermissionSet.Name 
			FROM PermissionSetAssignment 
			WHERE PermissionSet.Name = 'Clear_Algorithm_Settings_Administrator' 
			OR PermissionSet.Name = 'Clear_Algorithm_Settings_Administrator_with_Approve_Permission'
		];
		for(Clear_Algorithm_Settings__c cas : triggerNew) {
			for(PermissionSetAssignment psa : psaList) {
				if(psa.AssigneeId != cas.OwnerId && cas.Status__c == STATUS_APPROVED) {
					casShares.add(new Clear_Algorithm_Settings__share(
						ParentId=cas.Id, 
						UserOrGroupId=psa.AssigneeId, 
						AccessLevel='Read'));
				}
			}
		}
		System.debug('Inserting CAS Shares: ' + casShares);
		Database.insert(casShares, false);
	}

	/* Validation on Updated Records */

	/**
	*  @Description	(Used on update) Adds an error to a record if the user tries to activate a record that has not been approved.
	*/
	public static void blockActivationIfNotApproved(Clear_Algorithm_Settings__c oldSettings, Clear_Algorithm_Settings__c newSettings) {
		if (oldSettings.Status__c != STATUS_APPROVED && newSettings.Active__c) { 
			newSettings.Active__c.addError('Only approved records can be activated.');
		}
	}

	/**
	*	@Description	(Used on update) Adds an error to a record if the user tries to change any of the date while in draft status
	*		(Activated_On__c, Approved_On__c, Deactivated_On__c).
	*/
	public static void blockDateFieldUpdates(Clear_Algorithm_Settings__c oldSettings, Clear_Algorithm_Settings__c newSettings) {
		if (oldSettings.Activated_On__c != newSettings.Activated_On__c) {
			newSettings.Activated_On__c.addError('The activated on date cannot be changed.');
		}
		if (oldSettings.Status__c != STATUS_PENDING_APPROVAL && oldSettings.Approved_On__c != newSettings.Approved_On__c) {
			newSettings.Approved_On__c.addError('The approved on date cannot be changed.');
		}
		if ((oldSettings.Deactivated_On__c != newSettings.Deactivated_On__c) && 
				!(oldSettings.Active__c && !newSettings.Active__c)) {	
			// Error should be thrown always EXCEPT when the old record is active and the new record is not active
			newSettings.Deactivated_On__c.addError('The deactivated on date cannot be changed.');
		}
	}

	/**
	*	@Description	(Used on update) Will check if there have been edits other than changeing Active__c and add an error if so.
	*/
	public static void blockEditForApproved(Clear_Algorithm_Settings__c oldSettings, Clear_Algorithm_Settings__c newSettings){
		if(oldSettings.Status__c == STATUS_APPROVED) {
			if(oldSettings.Active__c != newSettings.Active__c) {
				Clear_Algorithm_Settings__c clonedSettings = newSettings.clone(true, true, true, true);
				clonedSettings.Active__c = oldSettings.Active__c;
				System.debug('Cloned and Old record: ');
				System.debug(clonedSettings);
				System.debug(oldSettings);
				if (oldSettings != clonedSettings && 
						oldSettings.Deactivated_On__c == clonedSettings.Deactivated_On__c) {
					newSettings.addError('Cannot edit an approved record except to activate it.');
				}
			} else {
				newSettings.addError('Cannot edit an approved record except to activate it.');
			}
		}
	}
    
	/**
	*	@Description	(Used on update) Check if record has been previously activated and add an error if it is marked as active.
	*/
	public static void blockActivationForFormerlyActiveRecord(Clear_Algorithm_Settings__c oldSettings, Clear_Algorithm_Settings__c newSettings) {
		if (oldSettings.Activated_On__c != null && newSettings.Active__c) {
			newSettings.addError('Records that used to be active must be cloned before they can be re-activated.');
		}
	}

	/* Activating and Deactivating Records */

	/**
	*  @Description	(Used on update) Checks if a record has been updated and sets the activated date if it has been.
	*  @retrun	Returns true if a record has been activated, false otherwise.
	*/
	public static Boolean setActivationDateOnNewActiveRecord(Clear_Algorithm_Settings__c oldSettings, Clear_Algorithm_Settings__c newSettings) {
		if (!oldSettings.Active__c && newSettings.Active__c) {
			newSettings.Activated_On__c = Datetime.now();
			return true;
		}
		return false;
	}

	/**
	*  @Description	Queries for all active settings, sets them to non-active and sets their deactivated date.
	*/
	public static void deactivateOtherSettings() {
		List<Clear_Algorithm_Settings__c> activeSettings = [
			SELECT 
				Active__c
			FROM Clear_Algorithm_Settings__c
			WHERE Active__c = true
		];

		if (!activeSettings.isEmpty()) {
			System.debug('Deactivating: ' + activeSettings);
			for (Clear_Algorithm_Settings__c cas : activeSettings) {
				cas.Active__c = false;
				cas.Deactivated_On__c = DateTime.now();
			}
			update activeSettings;
		}
	}

}