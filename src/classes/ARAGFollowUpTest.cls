@isTest
public class ARAGFollowUpTest {
    static testMethod void ARAGFollowUpTest() {
	
		//Query for a user			 
        User u = [SELECT Id FROM User LIMIT 1][0];
        Map<String, String> rMap = new Map<String,String>();
        for(RecordType r : ([Select Id, Name from RecordType where sObjectType = 'ARAG_Follow_Up__c'])){

            rMap.put(r.Name, r.Id);
        }
		 		
        //Create a new Claims Work Item or ARAG
        ARAG__c a = new ARAG__c(
            Patient_Name__c = 'Tester',
            Status__c = 'Waiting on Payor',
            Pending_Action__c = 'Need to Draft Appeal',
            Worked_By__c = u.Id		
        );
        insert a;
        system.debug('ARAG inserted!');
        
                                                                                                                //1st CRS Follow Up
        ARAG_Follow_Up__c afu1 = new ARAG_Follow_Up__c(
            Completed_Date__c = date.today()+1,
            Pending_Action__c = 'Need to Draft Appeal',
			Related_CWI__c = a.Id,
            Worked_By__c = u.Id,
			Collector_s_Notes__c = 'Old note',
            Resolution_Action__c = 'Level 3 Escalation',
            CRS_Correction_Type__c = 'Policy ID Correction',
            Completed__c = true,
            RecordTypeId = rMap.get('CRS Follow-Up')          
        );
        insert afu1;
        system.debug('ARAG Follow-Up inserted!');
		
        // test for afu1
        
        List<ARAG__c> checkList1 = [
            SELECT 
            	Id, 
            	Completed_Date__c,
            	Pending_Action__c,
            	Worked_By__c, 
				Collector_s_Notes__c,
            	Resolution_Action__c,
            	CRS_Correction_Type__c,
            	Completed__c
            FROM ARAG__c 
            WHERE Id = :a.Id
        ];
        
        for(ARAG__c arg : checkList1){
            //system.assertEquals(date.today(),arg.Completed_Date__c);
            system.assertEquals('Need to Draft Appeal',arg.Pending_Action__c);
            system.assertEquals('Old note',arg.Collector_s_Notes__c);
            system.assertEquals('Level 3 Escalation',arg.Resolution_Action__c);
            system.assertEquals('Policy ID Correction',arg.CRS_Correction_Type__c);
            system.assertEquals(true,arg.Completed__c);
        }
        
		                                                                                                      //1st Denial
        ARAG_Follow_Up__c afu2 = new ARAG_Follow_Up__c(
            
            Related_CWI__c = a.Id,
            Denial_Reason__c = 'Authorization Issue',
            Denial_Notes__c = 'test for denial',
            PADR_Action__c = 'Appeal Submitted',
            PADR_Correction_Type__c = 'Modifier and Charge Correction',
            Denial_Worked_by__c = u.Id,
            Denial_Worked_Date_and_Time__c = System.now()+2,
            RecordTypeId = rMap.get('Denial')
        );
        insert afu2;
		//test for afu2
		
         List<ARAG__c> checkList2 = [
            SELECT 
                Id, 
                Denial_Reason__c,
                Denial_Notes__c,
                PADR_Action__c,
                Correction_Type__c,
                Denial_Worked_by__c,
                Date_Denial_Worked__c
            FROM ARAG__c 
            WHERE Id = :a.Id
        ];
        
        for(ARAG__c arg : checkList2){
            //system.assertEquals(date.today(),arg.Completed_Date__c);
            system.assertEquals('Authorization Issue',arg.Denial_Reason__c);
            system.assertEquals('test for denial',arg.Denial_Notes__c);
            system.assertEquals('Appeal Submitted',arg.PADR_Action__c);
            system.assertEquals('Modifier and Charge Correction', arg.Correction_Type__c);
            
        }
        
                                                                                                            // 1st Scrubbing
        ARAG_Follow_Up__c afu4 = new ARAG_Follow_Up__c(
            
            Related_CWI__c = a.Id,
            Scrub_Type__c = 'Bill Scrub Rule',
            Billing_Issue1__c = 'Claim billed before PT serviced',
            Procedure_Codes__c = 'A4527',
            Corrective_Action__c = 'Demographic information corrected; claim re-transmitted',
            Scrubbed_by__c = u.Id,
            Scrubbed_Date_Time__c = System.now()+3,
            RecordTypeId = rMap.get('Scrubbing')
        );
        insert afu4;
		
        //test afu4
        
        List<ARAG__c> checkList3 = [
            SELECT 
                Id, 
                Scrub_Type__c,
                Billing_Issue__c,
                Procedure_Code_s__c,
                Corrective_Action__c,
                Scrubbed_by__c,
                Scrubbed_Date__c
            FROM ARAG__c 
            WHERE Id = :a.Id
        ];
        
        for(ARAG__c arg : checkList3){
            //system.assertEquals(date.today(),arg.Completed_Date__c);
            system.assertEquals('Bill Scrub Rule',arg.Scrub_Type__c);
            system.assertEquals('Claim billed before PT serviced',arg.Billing_Issue__c);
            system.assertEquals('A4527',arg.Procedure_Code_s__c);
            system.assertEquals('Demographic information corrected; claim re-transmitted',arg.Corrective_Action__c);
        }
        
                                                                                                            // 2nd Scrubbing
        ARAG_Follow_Up__c afu5 = new ARAG_Follow_Up__c(
            
            Related_CWI__c = a.Id,
            Scrub_Type__c = 'Bill Scrub Rule',
            Billing_Issue1__c = 'Claim billed before PT serviced',
            Procedure_Codes__c = 'A4527, A5467',
            Corrective_Action__c = 'Demographic information corrected; claim re-transmitted',
            Scrubbed_by__c = u.Id,
            Scrubbed_Date_Time__c = System.now()+4,
            RecordTypeId = rMap.get('Scrubbing')
        );
        insert afu5;
        system.debug('Second ARAG Follow-Up inserted!');
        
        //test afu 5
        
        List<ARAG__c> checkList4 = [
            SELECT 
                Id, 
                Scrub_Type__c,
                Billing_Issue__c,
                Procedure_Code_s__c,
                Corrective_Action__c,
                Scrubbed_by__c,
                Scrubbed_Date__c
            FROM ARAG__c 
            WHERE Id = :a.Id
        ];
        
        for(ARAG__c arg : checkList4){
            //system.assertEquals(date.today(),arg.Completed_Date__c);
            system.assertEquals('Bill Scrub Rule',arg.Scrub_Type__c);
            system.assertEquals('Claim billed before PT serviced',arg.Billing_Issue__c);
            system.assertEquals('A4527, A5467',arg.Procedure_Code_s__c);
            system.assertEquals('Demographic information corrected; claim re-transmitted',arg.Corrective_Action__c);
        }

                                                                                                        // 2nd denial

        ARAG_Follow_Up__c afu6 = new ARAG_Follow_Up__c(
            
            Related_CWI__c = a.Id,
            Denial_Reason__c = 'Authorization Issue',
            Denial_Notes__c = 'test for denial 2',
            PADR_Action__c = 'Appeal Submitted',
            PADR_Correction_Type__c = 'Modifier and Charge Correction',
            Denial_Worked_by__c = u.Id,
            Denial_Worked_Date_and_Time__c = System.now()+2,
            RecordTypeId = rMap.get('Denial')
        );
        insert afu6;
        //test for afu2
        
         List<ARAG__c> checkList5 = [
            SELECT 
                Id, 
                Denial_Reason__c,
                Denial_Notes__c,
                PADR_Action__c,
                Correction_Type__c,
                Denial_Worked_by__c,
                Date_Denial_Worked__c
            FROM ARAG__c 
            WHERE Id = :a.Id
        ];
        
        for(ARAG__c arg : checkList5){
            //system.assertEquals(date.today(),arg.Completed_Date__c);
            system.assertEquals('Authorization Issue',arg.Denial_Reason__c);
            system.assertEquals('test for denial 2',arg.Denial_Notes__c);
            system.assertEquals('Appeal Submitted',arg.PADR_Action__c);
            system.assertEquals('Modifier and Charge Correction', arg.Correction_Type__c);
            
        }

                                                                                                         // 2nd CRS Follow Up

        ARAG_Follow_Up__c afu7 = new ARAG_Follow_Up__c(
            Completed_Date__c = date.today()+1,
            Pending_Action__c = 'Need to Draft Appeal',
            Related_CWI__c = a.Id,
            Worked_By__c = u.Id,
            Collector_s_Notes__c = 'Old note 2',
            Resolution_Action__c = 'Level 3 Escalation',
            CRS_Correction_Type__c = 'Policy ID Correction',
            Completed__c = true,
            RecordTypeId = rMap.get('CRS Follow-Up')          
        );
        insert afu7;
        system.debug('ARAG Follow-Up inserted!');
        
        // test for afu1
        
        List<ARAG__c> checkList6 = [
            SELECT 
                Id, 
                Completed_Date__c,
                Pending_Action__c,
                Worked_By__c, 
                Collector_s_Notes__c,
                Resolution_Action__c,
                CRS_Correction_Type__c,
                Completed__c
            FROM ARAG__c 
            WHERE Id = :a.Id
        ];
        
        for(ARAG__c arg : checkList6){
            //system.assertEquals(date.today(),arg.Completed_Date__c);
            system.assertEquals('Need to Draft Appeal',arg.Pending_Action__c);
            system.assertEquals('Old note 2',arg.Collector_s_Notes__c);
            system.assertEquals('Level 3 Escalation',arg.Resolution_Action__c);
            system.assertEquals('Policy ID Correction',arg.CRS_Correction_Type__c);
            system.assertEquals(true,arg.Completed__c);
        }
		
		//Check that the fields in the newest ARAG Follow-Up have been mapped to the parent and that              
		//the parent has been updated.
		
		
		//Check that when the newest child object has been deleted, the fields on the parent object 
		//are updated with the fields of the second most recently created child record.
		// delete in reverse - last one first to check whether other scrubbing record is retained

        //--------------------------------------------------------Delete Scrubbing-------------------------------------------------
		delete afu5;                                                                              // delete 2nd Scrubbing
        
		List<ARAG__c> deleteList1 = [
            SELECT 
            	Id, 
                Scrub_Type__c,
                Billing_Issue__c,
                Procedure_Code_s__c,
                Corrective_Action__c,
                Scrubbed_by__c,
                Scrubbed_Date__c
            FROM ARAG__c 
            WHERE Id = :a.Id
        ];
        
        for(ARAG__c arg : deleteList1){
        	system.assertEquals('Bill Scrub Rule',arg.Scrub_Type__c);
            system.assertEquals('Claim billed before PT serviced',arg.Billing_Issue__c);
            system.assertEquals('A4527',arg.Procedure_Code_s__c);
            system.assertEquals('Demographic information corrected; claim re-transmitted',arg.Corrective_Action__c);
        }
        
		//Check that when all child records are deleted, the fields on the parent object are updated to null.
		// delete afu4 scrubbing --> so set back to null
		delete afu4;                                                                          // delete 1st Scrubbing
        
		List<ARAG__c> deleteList3 = [
            SELECT 
            	Id, 
                Scrub_Type__c,
                Billing_Issue__c,
                Procedure_Code_s__c,
                Corrective_Action__c,
                Scrubbed_by__c,
                Scrubbed_Date__c
            FROM ARAG__c 
            WHERE Id = :a.Id
        ];
        
        for(ARAG__c arg : deleteList3){
        	system.assertEquals(null ,arg.Scrub_Type__c);
            system.assertEquals(null ,arg.Billing_Issue__c);
            system.assertEquals(null ,arg.Procedure_Code_s__c);
            system.assertEquals(null ,arg.Corrective_Action__c);
        }
        
        //----------------------------------------------------------- Delete Denial----------------------------------------------------

        delete afu6; // should populate the next denial on line                                                 // delete 2nd Denial
        List<ARAG__c> deleteList6 = [
            SELECT 
                Id, 
                Denial_Reason__c,
                Denial_Notes__c,
                PADR_Action__c,
                Correction_Type__c,
                Denial_Worked_by__c,
                Date_Denial_Worked__c
            FROM ARAG__c 
            WHERE Id = :a.Id
        ];
        
        for(ARAG__c arg : deleteList6){
            //system.assertEquals(date.today(),arg.Completed_Date__c);
            system.assertEquals('Authorization Issue' ,arg.Denial_Reason__c);
            system.assertEquals('test for denial' ,arg.Denial_Notes__c);
            system.assertEquals('Appeal Submitted' ,arg.PADR_Action__c);
            system.assertEquals('Modifier and Charge Correction', arg.Correction_Type__c);
        }


        delete afu2; // should set values to null as there are no more denials                                  //delete 1st denial
        List<ARAG__c> deleteList4 = [
            SELECT 
                Id, 
                Denial_Reason__c,
                Denial_Notes__c,
                PADR_Action__c,
                Correction_Type__c,
                Denial_Worked_by__c,
                Date_Denial_Worked__c
            FROM ARAG__c 
            WHERE Id = :a.Id
        ];
        
        for(ARAG__c arg : deleteList4){
            //system.assertEquals(date.today(),arg.Completed_Date__c);
            system.assertEquals(null ,arg.Denial_Reason__c);
            system.assertEquals(null ,arg.Denial_Notes__c);
            system.assertEquals(null ,arg.PADR_Action__c);
            system.assertEquals(null , arg.Correction_Type__c);
            
        }
        
        //----------------------------------------------------------------Delete CRS Follow Up------------------------------------------------
        // delete afu1 ----> reset CRS
        delete afu7;                                                                                            // delete 2nd CRS Follow Up

        List<ARAG__c> deleteList7 = [
            SELECT 
                Id, 
                Completed_Date__c,
                Pending_Action__c,
                Worked_By__c, 
                Collector_s_Notes__c,
                Resolution_Action__c,
                CRS_Correction_Type__c,
                Completed__c
            FROM ARAG__c 
            WHERE Id = :a.Id
        ];
        
        for(ARAG__c arg : deleteList7){
            //system.assertEquals(date.today(),arg.Completed_Date__c); 
            system.assertEquals('Need to Draft Appeal',arg.Pending_Action__c);
            system.assertEquals('Old note',arg.Collector_s_Notes__c);
            system.assertEquals('Level 3 Escalation' ,arg.Resolution_Action__c);
            system.assertEquals('Policy ID Correction' ,arg.CRS_Correction_Type__c);
            system.assertEquals(true ,arg.Completed__c);
        }

        delete afu1;

        List<ARAG__c> deleteList5 = [
            SELECT 
            	Id, 
            	Completed_Date__c,
            	Pending_Action__c,
            	Worked_By__c, 
				Collector_s_Notes__c,
            	Resolution_Action__c,
            	CRS_Correction_Type__c,
            	Completed__c
            FROM ARAG__c 
            WHERE Id = :a.Id
        ];
        
        for(ARAG__c arg : deleteList5){
            //system.assertEquals(date.today(),arg.Completed_Date__c);
            system.assertEquals(null,arg.Pending_Action__c);
            system.assertEquals(null ,arg.Collector_s_Notes__c);
            system.assertEquals(null ,arg.Resolution_Action__c);
            system.assertEquals(null ,arg.CRS_Correction_Type__c);
            system.assertEquals(false ,arg.Completed__c);
        }
        
        
        //Create another ARAG Follow-Up
        /*ARAG_Follow_Up__c afu3 = new ARAG_Follow_Up__c(
			Related_CWI__c = a.Id       
        );
        insert afu3;
        system.debug('Final ARAG Follow-Up inserted!');
        
        afu3 = [SELECT Id, Worked_By__c FROM ARAG_Follow_Up__c WHERE Id = :afu3.Id][0];
        system.debug('The Worked_By__c field equals ' + afu3.Worked_By__c);
        
        //Check if Worked_By__c has been populated.
        system.assert(afu3.Worked_By__c != null);*/   
	}
}