public without sharing class DMEModel_CS extends AbstractSObjectModel_CS {
	
	public DME__c instance {
		get{
			if(instance == null){ instance = (DME__c)record; }
			return instance;
		}
		set;
	}
	
	public static List<DMEModel_CS> getDMEModelList(List<DME__c> DMEs){
		List<DMEModel_CS> models = new List<DMEModel_CS>();
		
		for(DME__c d : DMEs){
			models.add(new DMEModel_CS(d));
		}
		
		return models;
	}
	
	public DMEModel_CS(DME__c dme){
		super(dme);
	}
}