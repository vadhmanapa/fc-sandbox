@isTest
private class CaseNotificationsTest {

	static Case testCase;

	static testMethod void checkNotifications() {
		
		init();
	
		CaseNotifications.getDueDateNotifications(new Set<Id>{testCase.Id});
	}

    static testMethod void misc() {
        EmailTemplate tmp1 = CaseNotifications.dueDateTemplate;
        OrgWideEmailAddress tmp2 = CaseNotifications.adminAddress;
    }
    
    static void init() {
    	
    	Contact testContact = new Contact(
    		FirstName = 'Test',
    		LastName = 'Contact',
    		Email = 'test@test.com'
    	);
    	insert testContact;
    	
    	testCase = new Case(
    		Subject = 'Test Subject',
    		ContactId = testContact.Id,
    		Due_Date__c = Date.today(),
    		OwnerId = '00530000005epuc'
    	);
    	
    	system.debug('Case owner: ' + testCase.OwnerId);
    	
    	insert testCase;
    	
    	system.debug('Outputting cases owned by users');
    	
    	for(Case c : [
    		SELECT c.Owner.Name, c.Owner.Email, c.Owner.Type, c.OwnerId 
    		FROM Case c 
    		WHERE Owner.Type IN ('Group')])
    	{
    		system.debug(c.CaseNumber + ' / ' + c.Owner.Name + ' / ' + c.Owner.Email);
    	}
    }
}