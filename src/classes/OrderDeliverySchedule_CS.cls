/**
 * @description class will run given batch on specified jobSchedule dates
 * @author Cloud Software, LLC 
 *	Revision History
 *		07/28/2016 - saguirre - Made execute to call OrderDeliveryCheck Batch
 **/
global class OrderDeliverySchedule_CS implements schedulable{
	
	public static final String jobName = 'Delivery Schedule Process';
	public static final String jobSchedule = '0 0 0 * * ?';
	
	public static void schedule(){

		String name = (Test.isRunningTest() ? jobName + ' Test' : jobName);
		
		String jobId = System.schedule(
						name, 
		        		jobSchedule, 
		        		new OrderDeliverySchedule_CS()
	        			);

		System.debug('jobId: ' + jobId);
	}
	
	/**
	 * @description executes the OrderDeliveryCheck_CS batch with 200 Orders at a time.
	**/
	global void execute(SchedulableContext SC) {
      OrderDeliveryCheck_CS odc = new OrderDeliveryCheck_CS(); 
	  Database.executeBatch(odc, 200);
   }
}