public with sharing class PublicKnowledgeSearchController {

	public Map<String,String> pageParams = ApexPages.currentPage().getParameters();

	public String searchString {get; set;}
	public List<Tutorial__kav> searchResults {get; set;}
	public List<TutorialKavWrapper> tkwResults {get; set;}

	public PublicKnowledgeSearchController() {
		
		if(pageParams.containsKey('search')) {
			searchString = pageParams.get('search');
		}
		
		search();
	}
	
	public PageReference init(){
		return PublicKnowledgeServices.checkForReferrer();
	}
	
	public void search() {
		
		if(String.isBlank(searchString)) {
			searchResults = new List<Tutorial__kav>();
			tkwResults = new List<TutorialKavWrapper>();
			return;
		}
		
		List<String> searchStrings = new List<String>();
		searchStrings.add(searchString);
		String soslSearchString = searchString.replace(' ', '* ');
		
		String specialCharString = checkForQueSpecialCharacter(searchString);
		if (specialCharString != null) { 
			soslSearchString += specialCharString;
			searchStrings.add(specialCharString.trim());
		}
		
		//Results for other fields
		List<List<SObject>> soslResults = [
			FIND :soslSearchString
			IN ALL FIELDS
			RETURNING Tutorial__kav (Title, ArticleNumber, Summary, Content__c, Keywords__c, KnowledgeArticleId WHERE PublishStatus = 'Online' AND IsLatestVersion = true AND Language = 'en_US')
		];
		
		//system.debug('### ' + soslResults);
		
		//searchResults.addAll((List<Tutorial__kav>) soslResults[0]);
		searchResults = (List<Tutorial__kav>) soslResults[0];
		tkwResults = new List<TutorialKavWrapper>();
		for(Tutorial__kav tk : searchResults) {
			if (isExtraneousResult(tk, soslSearchString)){
				continue;
			}
			TutorialKavWrapper ntk = new TutorialKavWrapper(tk, searchStrings);
			tkwResults.add(ntk);
		}
		tkwResults.sort();
		
		system.debug(searchResults);
		system.debug(tkwResults);
		/*
		searchResults = [
            SELECT Title,Summary,Content__c,KnowledgeArticleId 
            FROM Tutorial__kav
            WHERE PublishStatus = 'Online' 
            AND IsLatestVersion = true
            AND Language = 'en_US'
            AND (Title LIKE :('%' + searchString + '%')
                 OR Keywords__c LIKE :('%' + searchString + '%'))
        ];
        
        
        [
        SELECT Title, Summary, KnowledgeArticleId 
        FROM KnowledgeArticleVersion
        WHERE PublishStatus = 'Online'
        AND IsLatestVersion = true
        AND Language = 'en_US'
        AND (Title LIKE :('%' + searchString + '%')
        OR Keywords__c LIKE :('%' + searchString + '%'))
        ];
        */
	}
	
	public Boolean isExtraneousResult(Tutorial__kav tk, String searchString){
		
		List<String> searchTerms = searchString.split(' ');
		for (String term : searchTerms){
			term = term.toLowerCase();
			//Ensure that one of the search terms is actually in the article
			if (
				(tk.Title != null && tk.Title.toLowerCase().contains(term)) ||
				(tk.Summary != null && tk.Summary.toLowerCase().contains(term)) ||
				(tk.Content__c != null && tk.Content__c.toLowerCase().contains(term)) ||
				(tk.Keywords__c != null && tk.Keywords__c.toLowerCase().contains(term)) ||
				(tk.ArticleNumber != null && tk.ArticleNumber.toLowerCase().contains(term))
			){
				return false;
			}
			
		}
		return true;
	}
	
	// Checks if the user entered the search term 'que' so that we can also search for 'qūe'
	public String checkForQueSpecialCharacter(String checkString) {
		List<String> searchWords = checkString.split(' ');
		String newSearch = '';
		Boolean specialCharNeeded = false;
		
		for (String s :searchWords) {
			if (s.equalsIgnoreCase('que')) { 
				specialCharNeeded = true;
				s = 'qūe';
			}
			newSearch += ' ' + s;
		}
		
		if (specialCharNeeded) { return newSearch; }
		else { return null; }
	}
	
	// Wrapper Class for Tutorial__Kav. Used to store and modify the Public Knowledge Article's Title and Content for the Public Knowledge Search Page.
	public class TutorialKavWrapper implements Comparable{
		public Tutorial__kav originalArticle {get; set;}
		public String titleBolded {get; set;}
		public String articleNumber {get; set;}
		public String contentTruncated {get; set;}
		public Id knowledgeArticleId {get; set;}
		public String searchString {get; set;}
		public List<String> searchStrings {get; set;}
		//public String debugSorting {get; set;}

		public TutorialKavWrapper(Tutorial__kav originalArticle, List<String> searchStrings) {
			this.originalArticle = originalArticle;
			this.knowledgeArticleId = originalArticle.KnowledgeArticleId;
			this.searchString = searchStrings[0];
			this.articleNumber = originalArticle.ArticleNumber;
			while (this.articleNumber.subString(0,1) == '0') {
	    		this.articleNumber = this.articleNumber.subString(1);
    		}
    		List<String> titleSplit = originalArticle.Title.split(': ', 2);
    		if (titleSplit.size() == 2) {
    			this.titleBolded = titleSplit[1];
    		}else {
				this.titleBolded = originalArticle.Title;
    		}
			this.searchStrings = searchStrings;
			this.contentTruncated = '';
			
			//Truncate the Content on the first instance of the search string
			if (!String.isBlank(originalArticle.Content__c)) {
				this.contentTruncated = getTruncatedContent(originalArticle.Content__c.stripHtmlTags(), searchStrings);
			}
			
			//We should bold each string passed in as a seperate search term
			for (String s :searchStrings) {
				this.titleBolded = boldSearchTermInTitle(this.titleBolded, s);			
				this.contentTruncated = boldSearchTermInContent(this.contentTruncated, s);
			}
			
			//TEST
			//this.debugSorting = this.debugSorting();
		}
		
		//Comparison method
		public Integer compareTo(Object compareTo) {
			TutorialKavWrapper wrapperCompareTo = (TutorialKavWrapper) compareTo;
			String thisContentLC = '';
			String compareContentLC = '';
			Boolean thisTitleContainsString = false;
			Boolean compareTitleContainsString = false;
			
			if (this.originalArticle.Content__c != null && wrapperCompareTo.originalArticle.Content__c != null) {
				thisContentLC = this.originalArticle.Content__c.toLowerCase();
				compareContentLC = wrapperCompareTo.originalArticle.Content__c.toLowerCase();
			}
			String thisTitleLC = this.originalArticle.Title.toLowerCase();
			String compareTitleLC = wrapperCompareTo.originalArticle.Title.toLowerCase();
			
			//First, check if the title contains the search term.  If one does, it should come first
			for (String s :this.searchStrings) {
				if (thisTitleLC.contains(s.toLowerCase())) {
				 	thisTitleContainsString = true;
					System.debug(compareTitleLC + ' (B) contains the search term.');
					break; 
				} 
			}
			
			for (String s :wrapperCompareTo.searchStrings) {
				if (compareTitleLC.contains(s.toLowerCase())) {
				 	compareTitleContainsString = true; 
					System.debug(thisTitleLC + ' (A) contains the search term.');
					break; 
				} 
			}
			
			if (thisTitleContainsString && !compareTitleContainsString){
				System.debug('A comes first');
				return -1;
			} else if (!thisTitleContainsString && compareTitleContainsString) {
				System.debug('B comes first');
				return 1;
			}
			System.debug('Titles are equal.');
			
			//Next, compare the number of times the search term appears in the article content
			Integer thisNumberOfTimesInContent = 0;
			Integer compareNumberOfTimesInContent = 0;
			
			for (String s :this.searchStrings) {
				thisNumberOfTimesInContent += thisContentLC.countMatches(s.toLowerCase());
			}
			
			for (String s :wrapperCompareTo.searchStrings) {
				compareNumberOfTimesInContent += compareContentLC.countMatches(s.toLowerCase());
			}
			System.debug ('A has search term ' + thisNumberOfTimesInContent + ' times.');
			System.debug ('B has search term ' + compareNumberOfTimesInContent + ' times.');
			
			if (thisNumberOfTimesInContent != compareNumberOfTimesInContent){
				System.debug('Difference in times: ' + (thisNumberOfTimesInContent - compareNumberOfTimesInContent));
				return compareNumberOfTimesInContent - thisNumberOfTimesInContent;
			}
			
			//Next, compare the number of views (Possible functionality to add)
			
			//Finally, compare alphabetically by title
			System.debug ('Returning A first? ' + (thisTitleLC.compareTo(compareTitleLC) > 0));
			return -thisTitleLC.compareTo(compareTitleLC);
			
		}
		
		/*
		//Sorting debug method
		public String debugSorting(){
			String debug = '';
			
			String thisContentLC = this.originalArticle.Content__c.toLowerCase();
			String thisSearchLC = this.searchString.toLowerCase();
			String thisTitleLC = this.originalArticle.Title.toLowerCase();
			
			Boolean thisTitleContainsString = thisTitleLC.contains(thisSearchLC);
			debug += 'Title contains string: ' + thisTitleContainsString + '\n';
			Integer thisNumberOfTimesInContent = thisContentLC.countMatches(thisSearchLC);
			debug += 'Number of times search appears in content: ' + thisNumberOfTimesInContent + '\n';
			
			return debug;
		} */
		
		// Method used to search the Public Knowledge Article's title for the Search Term. 
		// If the title contains the search term the title will be returned with the search term in bold. 
		// If the search term doesn't exist in the title, the title will be returned unaltered. 
		public String boldSearchTermInTitle(String title, String searchTerm) {
			Integer searchLength = searchTerm.length();
			Integer index = 0;
			Integer searchStartIndex = title.indexOfIgnoreCase(searchTerm, index);
			
			while (searchStartIndex != -1) {
				title = title.subString(0, searchStartIndex + searchLength) + '</span>' + title.substring(searchStartIndex + searchLength, title.length());
				title = title.subString(0, searchStartIndex) + '<span style="font-weight: bold">' + title.substring(searchStartIndex, title.length());
				index = searchStartIndex + searchLength + 40;
				searchStartIndex = title.indexOfIgnoreCase(searchTerm, index);
			}
			
			return title;
		}
		
		public String boldSearchTermInContent(String truncCont, String searchTerm) {
			Integer searchLength = searchTerm.length();
			Integer index = 0;
			Integer searchStartIndex = truncCont.indexOfIgnoreCase(searchTerm, index);
			
			while (searchStartIndex != -1) {
				truncCont = truncCont.subString(0, searchStartIndex + searchLength) + '</span>' + truncCont.subString(searchStartIndex + searchLength, truncCont.length());
				truncCont = truncCont.subString(0, searchStartIndex) + '<span style="font-weight: bold">' + truncCont.subString(searchStartIndex, truncCont.length());
				index = searchStartIndex + searchLength + 40;
				searchStartIndex = truncCont.indexOfIgnoreCase(searchTerm, index);
			}

			return truncCont;
		}
		
		// Method used to find the search term in the Public Knowledge Article's content.
		// If the search term exists, the content will be truncated to 100 characters around the search term, and returned with the term bolded.
		// If the search term doesn't exist, the first 100 characters of the content will be returned.
		// If the truncated content is from the middle of the article, '...' will be appended/prepended as necessary and all partial words will be eliminated
		public String getTruncatedContent(String origCont, List<String> searchTerms) {
			Integer indexOfSearch;
			Integer startIndex;
			Integer endIndex;
			String truncCont;
			
			//Search through the content for each search term.  Truncate the content at the first instance of a search term.
			for (String s :searchTerms) {
				indexOfSearch = origCont.indexOfIgnoreCase(s);
				if(indexOfSearch != -1){
					startIndex = indexOfSearch > 49 ? indexOfSearch - 50 : 0;
					endIndex = startIndex + 100 < origCont.length() ? startIndex + 100 : origCont.length();
					truncCont = origCont.substring(startIndex, endIndex);
					while(startIndex != 0 && String.isNotBlank(truncCont.subString(0, 1))) {
						truncCont = truncCont.subString(1, truncCont.length());
					}
					while(endIndex != origCont.length() && String.isNotBlank(truncCont.subString(truncCont.length()-1,truncCont.length()))) {
						truncCont = truncCont.subString(0, truncCont.length()-1);
					}
					if(endIndex != origCont.length()){
						truncCont =  truncCont + '...';
					}
					if(startIndex != 0) {
						truncCont =  '...' + truncCont;
					}
					return truncCont;
				}
				
			}
			
			//If we did not return truncated content yet, we did not find the search term.  Take the first 100 chars.
			
			startIndex = 0;
			endIndex = origCont.length() > 100 ? 100 : origCont.length();
			truncCont = origCont.subString(startIndex, endIndex);
			while(endIndex != origCont.length() && String.isNotBlank(truncCont.subString(truncCont.length()-1,truncCont.length()))) {
				truncCont = truncCont.subString(0, truncCont.length()-1);
			}
			if(endIndex == origCont.length()){
				return truncCont;
			} else {
				return truncCont + '...';
			}
		}
	}
}