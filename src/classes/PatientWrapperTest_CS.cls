@isTest
private class PatientWrapperTest_CS {
	
	public static List<Account> planList;
	public static List<Contact> planAdminList;
	public static List<Patient__c> patientsToReturn;
	public static List<Account> plansToReturn;
	public static Map<Id, List<Patient__c>> planToPatient;
	public static List<Plan_Patient__c> planPatientsToReturn;
	public static List<Account> providersToReturn;
	public static List<Order__c> ordersToReturn;
	public static Contact c;
	
	
    static testMethod void myUnitTest() {
       
   		init();
		
		PatientWrapper_CS cont = new PatientWrapper_CS(planPatientsToReturn[0]);
	    
	    
	    
	    test.startTest();
	    
	    
	    
	    test.stopTest();
    }
    
    
    public static void init() {
		
		planList = TestDataFactory_CS.generatePlans('testPlan', 1);
    	insert planList;
    	system.assertNotEquals(null, planList[0].Id);
    	
    	planAdminList = TestDataFactory_CS.generatePlanContacts('Admin', planList, 1);
    	insert planAdminList;
    	system.assertNotEquals(null, planAdminList[0].Id);
    	
    	patientsToReturn = TestDataFactory_CS.generatePatients('testLast', 1);
    	insert patientsToReturn;
    	
    	plansToReturn = TestDataFactory_CS.generatePlans('testPlan', 1);
    	insert plansToReturn;
    	
    	planToPatient = new Map<Id, List<Patient__c>>();
    	planToPatient.put(plansToReturn[0].Id, patientsToReturn);
    	
    	planPatientsToReturn = TestDataFactory_CS.generatePlanPatients(planToPatient);
    	insert planPatientsToReturn;
    	
    	providersToReturn = TestDataFactory_CS.generateProviders('testProvider', 1);
    	insert providersToReturn;
    	
    	
    	ordersToReturn = TestDataFactory_CS.generateOrders(planPatientsToReturn[0].Id, providersToReturn[0].Id,  1);
    	insert ordersToReturn;
    	
    	
    	c = [select Id, Active__c, Password__c, Salt__c, Entity__r.Type__c from Contact where Id = :planAdminList[0].Id];
    	
    	Cookie userIdCookie = new Cookie('userid', c.Id, null, 3600, true);
		ApexPages.currentPage().setCookies(new Cookie[]{userIdCookie});
		
		
	}
}