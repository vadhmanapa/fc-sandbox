/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PayerSettings_CS_Test {

	/******* Test Parameters *******/
	static final Integer N_ORDERS = 5;

	/******* Payer *******/
	static private List<Account> payers;
	static private List<Contact> payerUsers;
	
	/******* Test Objects *******/
	static private PayerSettings_CS settings;
    
    static testMethod void PayerSettings_CS_Test_PageLanding() {
        init();
        
        settings.init();
        
        System.assertEquals(Page.PayerSettings_CS.getURL(),ApexPages.currentPage().getURL());
    }
    
	static void init(){
    	
    	///////////////////////////////////
		// Data
		///////////////////////////////////
		
		//Payers
		payers = TestDataFactory_CS.generatePlans('TestPayer', 1);
		insert payers;
		
		//Payer users
		payerUsers = TestDataFactory_CS.createPlanContacts('PayerUser', payers, 1);

		///////////////////////////////////
		// Page Landing
		///////////////////////////////////

		//Set the page and authentication cookie
		Test.setCurrentPage(Page.PayerSettings_CS.setRedirect(true));
		
		//Log the desired user in
		TestServices_CS.login(payerUsers[0]);
    	
    	//Make provider portal services
    	settings = new PayerSettings_CS();
    }  
}