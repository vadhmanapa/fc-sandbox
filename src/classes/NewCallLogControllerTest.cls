@isTest
private class NewCallLogControllerTest {
	
	static PageReference pageRef;
	static NewCallLogController pageCon;
	static Call_Log__c existingCallLog;

    static testMethod void test() {
    	init();
    	pageCon.init();
    	
    }
            
    static void init() {
    	
    	existingCallLog = new Call_Log__c();
    	insert existingCallLog;
    	
    	ApexPages.Standardcontroller stdCon = new ApexPages.Standardcontroller(existingCallLog);
    	
		pageRef = Page.NewCallLogLink;
    	pageRef.getParameters().put('logId',existingCallLog.Id);
    	
    	pageCon = new NewCallLogController(stdCon);
    }
}