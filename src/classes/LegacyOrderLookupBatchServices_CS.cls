public class LegacyOrderLookupBatchServices_CS { 

	//Accepts Legacy Orders which were originally archived, finds their corresponding Order in the system and set the lookup on Order
	public static void processLegacyOrders(List<New_Orders__c> legacyOrders) {
		
		//Concatenate the Patient Name, Created Date, and Auth Code of each legacy order to create an identifier and map to a list of all legacy orders with that identifier
		//Then query for orders and map the identifier to a list of orders
		Map<String, List<New_Orders__c>> identifierToLegacyOrderList = new Map<String, List<New_Orders__c>>();
		Map<String, List<Order__c>> identifierToOrderList = new Map<String, List<Order__c>>();
		
		//But first, add Patient Name and Auth Code to a set for querying orders
		Set<String> patientNames = new Set<String>();
		Set<String> authCodes = new Set<String>();
		for (New_Orders__c lo : legacyOrders) {
			patientNames.add(lo.Patient_Name__c);
			authCodes.add(lo.Plan_Order__c);

			String identifier = lo.Patient_Name__c + lo.Plan_Order__c + lo.Original_Created_Date__c.Date();
			if (!identifierToLegacyOrderList.containsKey(identifier)) {
				identifierToLegacyOrderList.put(identifier, new List<New_Orders__c>());
				identifierToOrderList.put(identifier, new List<Order__c>());
			}
			identifierToLegacyOrderList.get(identifier).add(lo);
		}
		System.debug('identifierToLegacyOrderList: ' + identifierToLegacyOrderList);

		//Query for orders and map identifier to a list of orders
		for (Order__c o : [
			SELECT Authorization_Number__c, CreatedDate, Legacy_Orders__c, Patient_Name__c, Status__c
			FROM Order__c
			WHERE Patient_Name__c IN :patientNames
			AND Authorization_Number__c IN :authCodes
			AND Order_Image__c LIKE 'https://shreddr.captricity.com%' //Was created from a legacy order 
		]) {
			String identifier = o.Patient_Name__c + o.Authorization_Number__c + o.CreatedDate.Date();
			if (identifierToOrderList.containsKey(identifier)) {
				identifierToOrderList.get(identifier).add(o);
			}
		}
		System.debug('identifierToOrderList: ' + identifierToOrderList);

		//Iterate over legacy order identifiers and update orders with lookups and legacy orders with error messages
		List<Order__c> ordersToUpdate = new List<Order__c>();
		List<New_Orders__c> legacyOrdersToUpdate = new List<New_Orders__c>();
		for (String identifier : identifierToLegacyOrderList.keySet()) {

			System.debug('identifier: ' + identifier);
			
			//Get number orders that have not been cancelled
			List<Order__c> activeOrders = new List<Order__c>();
			for (Order__c o : identifierToOrderList.get(identifier)) {
				if (o.Status__c != 'Cancelled') {
					activeOrders.add(o);
				}
			}
			System.debug('activeOrders: ' + activeOrders);
			System.debug('identifierToLegacyOrderList.get(identifier).size(): ' + identifierToLegacyOrderList.get(identifier).size());
			System.debug('identifierToOrderList.get(identifier).size(): ' + identifierToOrderList.get(identifier).size());

			//Case 1: 1 Legacy Order -> 1 Order - Set lookup on that order
			if (identifierToLegacyOrderList.get(identifier).size() == 1 && identifierToOrderList.get(identifier).size() == 1) {
				identifierToOrderList.get(identifier)[0].Legacy_Orders__c = identifierToLegacyOrderList.get(identifier)[0].Id;
				ordersToUpdate.add(identifierToOrderList.get(identifier)[0]);
			}
			//Case 2: N Legacy Orders -> 1 Active Order
			else if (activeOrders.size() == 1) {

				//Check if HCPC codes/units match
				Boolean HCPCsAreMatching = true;
				for (Integer i = 1; i < identifierToLegacyOrderList.get(identifier).size(); i++) {
					if (identifierToLegacyOrderList.get(identifier)[i].CPT_code_for_OCR__c != identifierToLegacyOrderList.get(identifier)[0].CPT_code_for_OCR__c
					|| identifierToLegacyOrderList.get(identifier)[i].Units_for_OCR__c != identifierToLegacyOrderList.get(identifier)[0].Units_for_OCR__c) {
						HCPCsAreMatching = false;
					}
				}
				//Case 2a(i): HCPCs/units match - Set lookup on active order from Legacy Order w/ HCPC info
				if (HCPCsAreMatching) {
					activeOrders[0].Legacy_Orders__c = identifierToLegacyOrderList.get(identifier)[0].Id;
					ordersToUpdate.add(activeOrders[0]);
				}
				//Case 2a(ii): HCPCs/units do not match - Error "HCPCs or units do not match"
				else {
					identifierToLegacyOrderList.get(identifier)[0].Lookup_Reset_Failed__c = true;
					identifierToLegacyOrderList.get(identifier)[0].Lookup_Reset_Failed_Reason__c = getErrorMessageForLegacyOrder(
						identifierToLegacyOrderList.get(identifier), 
						activeOrders,
						'HCPCs or units do not match potential duplicate legacy orders with Ids'
					);
					legacyOrdersToUpdate.add(identifierToLegacyOrderList.get(identifier)[0]);
				}
			}
			//Case 3: Error "Multiple valid orders"
			else {
				//TODO: add error message for no matching orders
				identifierToLegacyOrderList.get(identifier)[0].Lookup_Reset_Failed__c = true;
				if (activeOrders.size() > 0) {
					if (identifierToLegacyOrderList.get(identifier).size() > 1) {
						identifierToLegacyOrderList.get(identifier)[0].Lookup_Reset_Failed_Reason__c = getErrorMessageForLegacyOrder(
							identifierToLegacyOrderList.get(identifier),
							activeOrders,
							'Multiple valid orders for this legacy order and legacy orders with Ids'
						);
					}else {
						identifierToLegacyOrderList.get(identifier)[0].Lookup_Reset_Failed_Reason__c = getErrorMessageForLegacyOrder(
							identifierToLegacyOrderList.get(identifier),
							activeOrders,
							'Multiple valid orders'
						);
					}
				}else if (identifierToOrderList.get(identifier).size() > 0) {
					if (identifierToLegacyOrderList.get(identifier).size() > 1) {
						identifierToLegacyOrderList.get(identifier)[0].Lookup_Reset_Failed_Reason__c = getErrorMessageForLegacyOrder(
							identifierToLegacyOrderList.get(identifier),
							identifierToOrderList.get(identifier),
							'Multiple valid orders for this legacy order and legacy orders with Ids'
						);
					}else {
						identifierToLegacyOrderList.get(identifier)[0].Lookup_Reset_Failed_Reason__c = getErrorMessageForLegacyOrder(
							identifierToLegacyOrderList.get(identifier),
							identifierToOrderList.get(identifier),
							'Multiple valid orders'
						);
					}
				}else {
					if (identifierToLegacyOrderList.get(identifier).size() > 1) {
						identifierToLegacyOrderList.get(identifier)[0].Lookup_Reset_Failed_Reason__c = getErrorMessageForLegacyOrder(
							identifierToLegacyOrderList.get(identifier),
							identifierToOrderList.get(identifier),
							'No valid orders for this legacy order and legacy orders with Ids'
						);
					}else {
						identifierToLegacyOrderList.get(identifier)[0].Lookup_Reset_Failed_Reason__c = getErrorMessageForLegacyOrder(
							identifierToLegacyOrderList.get(identifier),
							identifierToOrderList.get(identifier),
							'No valid orders for this legacy order'
						);
					}
				}
				legacyOrdersToUpdate.add(identifierToLegacyOrderList.get(identifier)[0]);
			}
		}
		update ordersToUpdate;
		update legacyOrdersToUpdate;
	}

	@testVisible
	//Accepts Legacy orders, orders, and a base message and concatenates an error message
	private static String getErrorMessageForLegacyOrder(List<New_Orders__c> legacyOrders, List<Order__c> orders, String baseMessage) {
		String failureMessage = baseMessage;
		if (legacyOrders.size() > 0) {
			failureMessage += ': ';
			for (Integer i = 1; i < legacyOrders.size(); i++) {
				failureMessage += legacyOrders[i].Id + ', ';
			}
		}
		if (orders.size() > 0) {
			failureMessage += 'Potential matching order(s): ';
			for (Order__c o : orders) {
				failureMessage += o.Id + ', ';
			}
		}
		return failureMessage.removeEnd(', ');
	}
}