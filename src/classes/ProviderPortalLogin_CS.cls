public without sharing class ProviderPortalLogin_CS {

	private final String pageAccountType = 'Provider';
	private final String redirectAccountType = 'Payer';
	public UserModel_CS portalUser;

	/******* Login Page *******/
	public String username {get;set;}
	public String password {get;set;}

	// Number of days or months before a password needs reset
	public static final Integer PASSWORD_RESET_DAYS = 180;
	public static final Integer PASSWORD_RESET_MONTHS = 6;
	
	public static final Long MINIMUM_ATTEMPT_DELAY_MS = 1000;	
	public static final Integer MAX_ATTEMPT_COUNT = 5;
	
	/******* Record of Last Login Attempt *******/
	public DateTime lastAttempt;

	//TODO: Implement the attemptCount on the record and reset to zero at some
	//	time interval (such as every hour);

	/******* Forgot Password Page *******/
	public String userEmailAddress {get;set;}

	/******* Page Messages *******/
	public String forgotPasswordMsg {get;set;}

	/******* Error Reporting *******/
	public transient String errLogin {get;set;}
	public transient String errForgotPass {get;set;}
	
	public static final String invalidLogin = 'Invalid username and/or password.';
	public static final String deactivatedUser = 'The user is not activated. Please contact Integra Support.';
	public static final String notClearEnabled = 'The user does not have permission to access Clear. Please contact Integra Support.';
	public static final String exceededAttempts = 'You have exceeded the maximum login attempts (' + MAX_ATTEMPT_COUNT + '). The user will be deactivated.';
	public static final String missingUsername = 'Missing username to retrieve password.';
	public static final String missingEmail = 'Enter an email address to retrieve username.';
	public static final String invalidUserData = 'The user is not configured properly. Please contact Integra Support.';

	public Boolean usernameError {get;set;}
	public Boolean passwordError {get;set;}
	public String loginErrorMessage {get;set;}
	public Boolean redirectError {get;set;}

	/******* Constructor *******/
	public ProviderPortalLogin_CS(){}
	
	public PageReference init(){
		
		//Check if on secure site
		if(!PortalServices_CS.isSecure && !Test.isRunningTest()){
			return new PageReference(Site.getBaseSecureURL());
		}
		
		//Set errors to false
		usernameError = false;
		passwordError = false;
		redirectError = false;
		loginErrorMessage = '';
		
		return null;
	}
	
	public PageReference login(){
		
		//Reset errors to false
		usernameError = false;
		passwordError = false;
		redirectError = false;
		loginErrorMessage = '';
		
		// Check delta submit time
		DateTime submitTime = DateTime.now();
		System.debug('Sumbmitting at ' + submitTime);
		
		if(lastAttempt != null && !Test.isRunningTest()){
			
			Long dAttemptTime = submitTime.getTime() - lastAttempt.getTime();
			
			System.debug('Last Attempt: ' + lastAttempt + ' / dSubmitTime (ms): ' + dAttemptTime);
			
			if(dAttemptTime < MINIMUM_ATTEMPT_DELAY_MS){
				System.debug('Error: ' + dAttemptTime + ' < ' + MINIMUM_ATTEMPT_DELAY_MS);
				
				loginErrorMessage = 'You must wait at least 1 second between login attempts';
				return null;
			}
		}		
		
		// Store the most recent attempt timestamp as the last
		lastAttempt = submitTime;
		
		// Check username entered and retrieve contact
		List<Contact> userList = new List<Contact>();
		
		if(String.isNotBlank(username) && String.isNotBlank(password)){
			userList = [
				SELECT 
					Id,
					Active__c,
					Username__c,
					Password__c,
					Salt__c,
					Password_Set_Date__c,
					Password_Reset__c,
					Entity__r.Type__c,
					Entity__r.Clear_Enabled__c,
					Login_Attempts__c
				FROM Contact 
				WHERE Username__c = :username
				AND (Entity__r.Type__c = :pageAccountType OR Entity__r.Type__c = :redirectAccountType)
				LIMIT 1
			];
		}
		else{
			System.debug('Missing username/password info');
			usernameError = true;
			passwordError = true;
			loginErrorMessage = invalidLogin;
			return null;			
		}
		
		if(userList.size() == 0){
			System.debug('Error: No user with username ' + username + ' found.');
			
			usernameError = true;
			passwordError = true;
			loginErrorMessage = invalidLogin;
			return null;
		}
		
		//New handling for payers logging into provider portal
		if (userList[0].Entity__r.Type__c == redirectAccountType) {
			System.debug('Error: Username ' + username + ' belongs to Payer Portal. Redirecting.');
			redirectError = true;
			return null;
		}
		
		portalUser = new UserModel_CS(userList[0]);
		
		// Check if account is active		
		if(portalUser.instance.Active__c != true){
			System.debug('Error: User ' + username + ' is not currently active.');
			
			loginErrorMessage = deactivatedUser;		
			return null;
		}
		
		if(portalUser.instance.Entity__r.Clear_Enabled__c != true) {
			System.debug('Error: User ' + username + ' is not currently Clear enabled.');
			
			loginErrorMessage = notClearEnabled;		
			return null;
		}
		
		// Check if password is correct
		if(portalUser.instance.Password__c != null && portalUser.instance.Password__c != ''){
			if(portalUser.instance.Password__c != PasswordServices_CS.hash(password,portalUser.instance.Salt__c)){
				System.debug('Error: Failed to login ' + username + ' with password "' + password + '"');

				System.debug('Attempt #: ' + portalUser.instance.Login_Attempts__c + ' of ' + MAX_ATTEMPT_COUNT);

				// Increment attempt counter
				portalUser.instance.Login_Attempts__c++;
				
				//Message to notify failed login
				usernameError = true;
				passwordError = true;
				
				//Check if we have exceeded login attempts
				if(portalUser.instance.Login_Attempts__c >= MAX_ATTEMPT_COUNT){			
		
					//Deactivate user
					portalUser.instance.Active__c = false;
		
					loginErrorMessage = exceededAttempts;
				}
				else{
					loginErrorMessage = invalidLogin + '<br/>Remaining login attempts: ' + 
						(MAX_ATTEMPT_COUNT - Integer.valueOf(portalUser.instance.Login_Attempts__c));
				}
				
				update portalUser.instance;
					
				return null;
			}			
		}
		else{
			loginErrorMessage = invalidUserData;
			return null;
		}	

		// Successful login!
		System.debug('Successful Login.');
		
		//Reset the login attempts
		portalUser.instance.Login_Attempts__c = 0;
		
		// Get the session Id and hash
		String hashedUserId = PasswordServices_CS.hash(portalUser.instance.Id,portalUser.instance.Salt__c);
		String hashedSessionId = PasswordServices_CS.hash(PortalServices_CS.sessionId,portalUser.instance.Salt__c);
		System.debug('Hashed SessionId: ' + hashedSessionId);
		
		// Set the userId and sessionId
		Cookie userIdCookie = new Cookie('userid', hashedUserId, null, 3600, PortalServices_CS.isSecure);
		Cookie sessionCookie = new Cookie('sessionid', hashedSessionId, null, 3600, PortalServices_CS.isSecure);
		ApexPages.currentPage().setCookies(new Cookie[]{userIdCookie,sessionCookie});
		
		// Check if password has expired			
		if(checkPasswordExpired()){
			System.debug('Password has expired.');
			
			portalUser.instance.Password_Reset__c = true;
			update portalUser.instance;

			return Page.ProviderUpdatePassword_CS.setRedirect(true);
		}

		// Go to the homepage
		update portalUser.instance;
		return Page.ProviderHomeDashboard_CS.setRedirect(true);
	}

	private Boolean checkPasswordExpired(){
		if(	portalUser != null && portalUser.instance != null){

			if( portalUser.instance.Password_Set_Date__c != null ){
				System.debug('Password created: ' + portalUser.instance.Password_Set_Date__c);
			
				Integer daysSinceReset = portalUser.instance.Password_Set_Date__c.Date().daysBetween(Date.today());				
				System.debug('Days since reset: ' + daysSinceReset);
			
				if(daysSinceReset > PASSWORD_RESET_DAYS){
					System.debug('Password has expired.');
					return true;
				}
				else{
					System.debug('Password created: ' + portalUser.instance.Password_Set_Date__c + ' will expire on ' + portalUser.instance.Password_Set_Date__c.addDays(PASSWORD_RESET_DAYS));
				}
			}
			else{
				//TODO: Temporary fix//
				System.debug('checkPasswordExpired: Password_Set_Date__c is null, setting to current time (temporary fix).');
				portalUser.instance.Password_Set_Date__c = DateTime.now();
				///////////////////////
			}
		}
		else{
			System.debug('checkPasswordExpired: User contact is null.');
		}
		
		return false;
	}
	
	/******* Forgot Username/password ******/

	public PageReference forgotPassword(){
		
		//TODO: Hack, fix later to use PortalServices
		UserModel_CS loginUser;
		
		if(String.isNotBlank(username)){
			System.debug('Forgot Password for username: ' + username);			
			
			try{
				//Get user
				Contact c = [select Id,Id_Key__c,Email,Username__c,Password__c,Salt__c from Contact where Username__c = :username];
				System.debug('User ' + username + ' email: ' + c.Email);
				
				if(String.isNotBlank(c.Email)){	
				
					loginUser = new UserModel_CS(c);
				
					//Reset their password
					String newPassword = PasswordServices_CS.randomPassword(8);
					System.debug('newPassword: ' + newPassword);
					
					loginUser.changePassword(newPassword);
					
					update loginUser.instance;
					
					//Email the temporary password
					String emailBody = '';
					
					//TODO: Remove username
					emailBody += 'Username: ' + loginUser.instance.Username__c + '<br/>';
					emailBody += 'Temporary Password: ' + newPassword;
					
					String subject = 'Temporary Password';
					
					loginUser.sendEmailTo(subject, emailBody);
					
					forgotPasswordMsg = 'An email has been sent to ' + username;
					System.debug('Email sent to ' + loginUser.instance.Email);
				}
				else{
					forgotPasswordMsg = 'There was an error sending the user ' + username;
					System.debug('User ' + username + ' has no email.');
				}
			}
			catch(Exception e){
				//throw ((e instanceOf ApplicationException) ? e : new ApplicationException('forgotPassword(): Could not find user ' + username,e));
				forgotPasswordMsg = 'There was an error retrieving the user ' + username;
				System.debug('Failed to create new password for user: ' + username + '. ' + e.getMessage());
			}
		}
		else{
			errLogin = missingUsername;
		}
		
		return null;
	}

	public PageReference forgotUsername(){
		return Page.ProviderForgotUsername_CS;
	}

	public PageReference retrieveUsername(){
		
		PageReference pr = null;
		
		if(userEmailAddress != null && userEmailAddress != ''){
			try{
				//TODO: What if there are multiple of the same user email? Such as with test accounts?
				Contact c = [select Id,Username__c,Email from Contact where Email = :userEmailAddress limit 1];
				
				if(c != null){
					System.debug('Sending username: ' + c.Username__c + ' to email: ' + c.Email);
					
					//Email the username
					Messaging.Singleemailmessage message = new Messaging.Singleemailmessage();
					message.setToAddresses(new String[]{c.Email});
					message.setSubject('Integra Username');
					message.setHtmlBody('Your username: ' + c.Username__c);
					
					EmailServices_CS.sendEmail(new Messaging.Singleemailmessage[]{message});
					//Messaging.sendEmail(new Messaging.Singleemailmessage[]{mail});
				}
				else{
					// TODO: Error message on page about bad username
				}
			}
			catch(Exception e){
				// TODO: Error message on page about bad username
				throw ((e instanceOf ApplicationException) ? e : new ApplicationException('Failed to retrieve username.',e));		
			}		
		
			userEmailAddress = '';
		}
		else{
			errForgotPass = missingEmail;
		}		

		return pr;
	}
}