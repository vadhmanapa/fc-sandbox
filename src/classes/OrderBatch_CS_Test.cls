@isTest
public class OrderBatch_CS_Test { 

	public static Account testAccount;
	public static List<Provider_Locations__c> oldProvLocs;
	public static List<Provider_Location__c> newProvLoc;
	public static Provider_Location__c dummyProvLoc;
	public static Order__c ord;
	
	static testmethod void OrderBatch_Test() {
		init();

		String query = 'SELECT Id, Provider_Locations__c FROM Order__c';

		System.assertEquals(dummyProvLoc.Id, ord.Provider_Location__c);

		Test.startTest();
		OrderBatch_CS batch = new OrderBatch_CS(query);
		Database.executeBatch(batch);
		Test.stopTest();

		Order__c updatedOrd = [SELECT Id, Provider_Location__c FROM Order__c WHERE Id =: ord.Id];
		System.assertEquals(newProvLoc[0].Id, updatedOrd.Provider_Location__c);
	}

	static void init() {
		Global_Switches__c gs = new Global_Switches__c(
			Name = 'Defaults',
			Create_ProvLoc_Trigger_Off__c = true
		);
		insert gs;

		testAccount = TestDataFactory_CS.generateProviders('Test Account', 1)[0];
		insert testAccount;

		oldProvLocs = TestDataFactory_CS.generateOldProviderLocations(testAccount.Id, 1);
		oldProvLocs[0].Monday__c = 'Closed';
		oldProvLocs[0].Tuesday__c = '9:00am-5:00pm';
		oldProvLocs[0].Wednesday__c = '9:00AM-5:00PM';
		oldProvLocs[0].Thursday__c = ' 9am - 12:00pm';
		oldProvLocs[0].Friday__c = ' 9:00am - 12pm';
		oldProvLocs[0].Saturday__c = '9:30am-5:30pm';
		oldProvLocs[0].Sunday__c = '09am-05:00pm';

		insert oldProvLocs;

		newProvLoc = ProviderLocationTransferServices.transferDataToNewProviderLocationObject(oldProvLocs);

		dummyProvLoc = new Provider_Location__c();
		dummyProvLoc.Account__c = testAccount.Id;
		insert dummyProvLoc;

		Account healthPlan = TestDataFactory_CS.generatePlans('Test Health Plan', 1)[0];
		insert healthPlan;

		Plan_Patient__c pp = TestDataFactory_CS.generatePlanPatientsPlanOnly(healthPlan.Id, 1)[0];
		insert pp;

		ord = TestDataFactory_CS.generateOrders(pp.Id, testAccount.Id, 1)[0];
		ord.Provider_Locations__c = oldPRovLocs[0].Id;
		ord.Provider_Location__c = dummyProvLoc.Id;
		insert ord;
	}
}