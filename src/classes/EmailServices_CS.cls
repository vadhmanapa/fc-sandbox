public without sharing class EmailServices_CS {

	public static String globalEmailSender = 'clearhelp@accessintegra.com';

	public static Boolean sendEmail(Messaging.SingleEmailMessage[] messages){
				
		Boolean success = true;
		Messaging.Sendemailresult[] emailResults;
		
		OrgWideEmailAddress[] fromAddress = [select Id from OrgWideEmailAddress where Address = :globalEmailSender];
		
		if(fromAddress.size() == 1){
			for(Messaging.SingleEmailMessage n : messages){
				n.setOrgWideEmailAddressId(fromAddress[0].Id);
			}
		}
		else{
			System.debug('sendNotification Error: There are ' + fromAddress.size() + ' addresses with \'clearhelp@accessintegra.com\'');
		}
		
		try{
			emailResults = Messaging.sendEmail(messages);
		}
		catch(Exception e){
			System.debug(e.getMessage());
			emailResults = new Messaging.Sendemailresult[]{};
		}
		
		for(Messaging.Sendemailresult r : emailResults){		
			if(!r.isSuccess()){
				String errorMessage = 'notifyAssignedOrder Errors:\n';
				
				for(Messaging.Sendemailerror e : r.getErrors()){
					errorMessage += e.getMessage() + '\t\n';
				}
				
				System.debug(errorMessage);

				success = false;
			}
		}
		
		return success;
	}
}