@isTest
public class DenialTrigger_CS_Test {
	static testMethod void DenialTrigger_CS_Test() {
		
		//Create new Account for new PADR
		Account acc = new Account(
			RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payer Relations').getRecordTypeId(),
			Name = 'Test'
		);
		insert acc;
		system.debug('Account inserted!');
        
        //Create new Claims Work Item/ARAG
        ARAG__c a = new ARAG__c(
        	Patient_Name__c = 'Tester',
            Status__c = 'Unassigned',
            Bill__c = '123456',
            
            Account_Number__c = '789',
            Billed_Date__c = date.today(),
            Payor_Family_text__c = 'Payor', 		//This field is called Payor__c on Denial
            Visit_Date__c = date.today()       
        );
        insert a;
		system.debug('ARAG inserted!');
					
		//Create new PADR
		PADR__c p = new PADR__c(
			PADR_Type__c = 'Denial',
			Provider__c = acc.Id,
			Bill_Number__c = '123456',
			Health_Plan__c = acc.Id,
			Related_ARAG__c = a.Name
		);
		insert p;
        
        //Create new Denial 
        Denials__c d = new Denials__c(
            Provider__c = 'Test Provider',
            Related_CWI__c = a.Id,  					  		
			Bill__c = '123456',
            Work_Denial__c = false
        );
        insert d;
		system.debug('Denial inserted!');
        system.debug('Old status: ' + a.Status__c);
        
        //Query for the old status on ARAG
        ARAG__c arg = [SELECT Id, Status__c FROM ARAG__c WHERE Id = :a.Id];
        system.debug('New status: ' + arg.Status__c);
        
        //Query for fields on Denial
        Denials__c d1 = [SELECT Id, Account_Number__c, Billed_Date__c, Payor__c, Visit_Date__c FROM Denials__c WHERE Id = :d.Id];
        
		//Check that the status on Claims Work Item has NOT been updated and that the fields on Denial have NOT been updated.
        system.assertEquals('Unassigned',arg.Status__c);
        system.assertEquals(null,d1.Account_Number__c);
        system.assertEquals(null,d1.Billed_Date__c);
        system.assertEquals(null,d1.Payor__c);							//This field is called Payor_Family_text__c on ARAG
        system.assertEquals(null,d1.Visit_Date__c);
        
        //Update Denial record
        d.Work_Denial__c = true;
        update d;
		system.debug('Checkbox marked!');
		
        //Query for the new status on ARAG
        ARAG__c arg2 = [SELECT Id, Status__c FROM ARAG__c WHERE Id = :a.Id];
        system.debug('New status: ' + arg.Status__c);
        
        //Query for fields on Denial
        Denials__c d2 = [SELECT Id, Account_Number__c, Billed_Date__c, Payor__c, Visit_Date__c FROM Denials__c WHERE Id = :d.Id];
        
		//Check that the status on Claims Work Item has been updated and that the fields on Denial have been updated.
        system.assertEquals('Denied',arg2.Status__c);
        system.assertEquals('789',d2.Account_Number__c);
        system.assertEquals(date.today(),d2.Billed_Date__c);
        system.assertEquals('Payor',d2.Payor__c);							//This field is called Payor_Family_text__c on ARAG
        system.assertEquals(date.today(),d2.Visit_Date__c);
        
        //Check that status can be updated to a different value.
        a.Status__c = 'Unassigned';
        update a;
		system.debug('ARAG Status changed back.');
		
        system.assertEquals('Unassigned',arg.Status__c);
        
        //Create a second new Denial with the same Bill Id
        Denials__c dd = new Denials__c(
            Provider__c = 'Test Provider',
            Related_CWI__c = a.Id,  					  		
			Bill__c = '123456',
            Work_Denial__c = true
        );
        insert dd;
		system.debug('Second Denial inserted!');
		
		//Query for the new status on ARAG
        ARAG__c arg3 = [SELECT Id, Status__c FROM ARAG__c WHERE Id = :a.Id];
        system.debug('New status: ' + arg.Status__c);
        
        //Query for fields on Denial
        Denials__c d3 = [SELECT Id, Account_Number__c, Billed_Date__c, Payor__c, Visit_Date__c FROM Denials__c WHERE Id = :dd.Id];
        
		//Check that the status on Claims Work Item has been updated and that the fields on Denial have been updated.
        system.assertEquals('Denied',arg3.Status__c);
        system.assertEquals('789',d3.Account_Number__c);
        system.assertEquals(date.today(),d3.Billed_Date__c);
        system.assertEquals('Payor',d3.Payor__c);							//This field is called Payor_Family_text__c on ARAG
        system.assertEquals(date.today(),d3.Visit_Date__c);
        
        /* Test Class for MapDenialToPADR
         	PADR__c testPADR = new PADR__c (Provider_Name_Text__c = 'Test Provider', Payor_Name__c = 'Test Health Plan', Bill_Number__c = '12345', 
                                                                         Procedure_Code_s__c = 'A6789');
         insert testPADR;
         String insertPADR = testPADR.Id;
        
        Denials__c testUpdateDenial = new Denials__c (Provider__c = 'Test Provider', Payor__c = 'Test Health Plan', Bill__c = '12345',
                                         Work_Denial__c = false, Line_Item__c = 'T4398');
        insert testUpdateDenial;
        String denyId = testUpdateDenial.Id;
        String billNum = testUpdateDenial.Bill__c;

        testUpdateDenial.Work_Denial__c = true;
        update testUpdateDenial;
        
        Denials__c searchDenial1 = [Select Id, Related_PADR__r.Id from Denials__c where Id =: denyId];

         String padrId = searchDenial1.Related_PADR__r.Id;

        List<PADR__c> denialLog = [ SELECT Id, Bill_Number__c FROM PADR__c WHERE Id =: padrId LIMIT 1];
        for( PADR__c log : denialLog ){
             System.assertEquals(1, denialLog.size(), 'The PADR was linked');
            System.assertEquals(billNum, log.Bill_Number__c, 'The denial was mapped to correct PADR');
        }
            
            //test for insert action
            
         Denials__c testClaim1 = new Denials__c (Provider__c = 'Test Provider', Payor__c = 'Test Health Plan', Bill__c = '6165',
                                         Work_Denial__c = false, Line_Item__c = 'A0002');
        insert testClaim1;
        String claimId1 = testClaim1.Id;
        String billNum1 = testClaim1.Bill__c;

        testClaim1.Work_Denial__c = true;
        update testClaim1;

        Denials__c searchClaim2 = [Select Id, Related_PADR__r.Id from Denials__c where Id =:claimId1];
        String padrId1 = searchClaim2.Related_PADR__r.Id;

        List<PADR__c> claimLog1 = [ SELECT Id, Bill_Number__c FROM PADR__c WHERE Id =: padrId1 LIMIT 1];
        for( PADR__c log1 : claimLog1 ){
             System.assertEquals(1, claimLog1.size(), 'The PADR was created');
             System.assertEquals(billNum1, log1.Bill_Number__c, 'The Denial was mapped to PADR');
	*/
	}
}