@isTest
private class PasswordServices_CS_Test {

    static testMethod void validatePasswordRegex_Test() {
        //Test both pass and fail cases
        String regexPass = 'abcABC123!';
        String regexFailTooShort = 'aB1!';
        String regexFailNoCapital = 'abcabc123!';
        String regexFailNoNumber = 'abcabcabc!';
        String regexFailNoSpecial = 'abcABC123';
        String regexFailNeighboring = 'aabcABC123!';
        
        System.assert(PasswordServices_CS.validatePasswordRegex(regexPass) == true, 'Failed good regex test.');
        System.assert(PasswordServices_CS.validatePasswordRegex(regexFailTooShort) == false, 'Passed bad regex too short.');
        System.assert(PasswordServices_CS.validatePasswordRegex(regexFailNoCapital) == false, 'Passed bad regex no capitals.');
        System.assert(PasswordServices_CS.validatePasswordRegex(regexFailNoNumber) == false, 'Passed bad regex no number.');
        System.assert(PasswordServices_CS.validatePasswordRegex(regexFailNoSpecial) == false, 'Passed bad regex no special characters.');
        System.assert(PasswordServices_CS.validatePasswordRegex(regexFailNeighboring) == false, 'Passed bad regex neighboring similar characters.');
    }
    
    static testMethod void randomPassword_Test(){
    	String randomPassword = '';
    	
    	//Validate generate standard 8-character password
    	randomPassword = PasswordServices_CS.randomPassword(8);
    	System.assert(PasswordServices_CS.validatePasswordRegex(randomPassword),'Failed to generate 8-character random password');
    	
    	//Validate when requested is too small    	
    	randomPassword = PasswordServices_CS.randomPassword(6);
    	System.assert(randomPassword.length() == 8, 'Minimum password length not enforced.');
    	System.assert(PasswordServices_CS.validatePasswordRegex(randomPassword), 'Failed regex.');
    	
    	//Validate when requested is greater than 8
    	randomPassword = PasswordServices_CS.randomPassword(10);
    	System.assert(randomPassword.length() == 10, 'Longer password not created');
    	System.assert(PasswordServices_CS.validatePasswordRegex(randomPassword), 'Failed regex.');
    }
    
    static testMethod void randomSalt_Test(){
    	String randomSalt = '';
    	
    	//Validate generate standard 12-character salt
    	randomSalt = PasswordServices_CS.randomSalt(12);
    	System.assert(PasswordServices_CS.validatePasswordRegex(randomSalt),'Failed to generate 12-character random salt');
    	
    	//Validate when requested is too small    	
    	randomSalt = PasswordServices_CS.randomSalt(10);
    	System.assert(randomSalt.length() == 12, 'Minimum password length not enforced.');
    	System.assert(PasswordServices_CS.validatePasswordRegex(randomSalt), 'Failed regex.');
    	
    	//Validate when requested is greater than 8
    	randomSalt = PasswordServices_CS.randomSalt(14);
    	System.assert(randomSalt.length() == 14, 'Longer password not created');
    	System.assert(PasswordServices_CS.validatePasswordRegex(randomSalt), 'Failed regex.');
    }
}