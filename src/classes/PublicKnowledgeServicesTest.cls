@isTest
private class PublicKnowledgeServicesTest {

    static testMethod void testReferrals() {
    	//Create custom settings
    	Referral_URL__c refUrl = new Referral_URL__c(
    		Name = 'Test',
    		Domain__c = 'www.test.com'
    	);
    	insert refUrl;
    	PageReference pr = Page.PublicKnowledgeHome;
    	Test.setCurrentPage(pr);
    	PageReference redirect = PublicKnowledgeServices.checkForReferrer();
    	System.assertEquals(PublicKnowledgeServices.REDIRECT_URL, redirect.getURL());
    	
    	pr.getHeaders().put('Referer', 'www.REDIRECT.com');
    	Test.setCurrentPage(pr);
    	System.debug(ApexPages.CurrentPage().getHeaders());
    	
    	redirect = PublicKnowledgeServices.checkForReferrer();
    	System.debug(redirect);
    	System.debug(redirect.getURL());
    	System.assertEquals(PublicKnowledgeServices.REDIRECT_URL, redirect.getURL());
    	
    	pr.getHeaders().put('Referer', 'http://www.test.com');
    	Test.setCurrentPage(pr);
    	redirect = PublicKnowledgeServices.checkForReferrer();
    	System.assertEquals(null, redirect);
    	
    }
    
    static testMethod void testCookies() {
    	
    	PageReference pr = Page.PublicKnowledgeHome;
    	Test.setCurrentPage(pr);
    	Cookie testCookie = PublicKnowledgeServices.getPageCookie();
    	System.assertEquals(null, testCookie);
    	PublicKnowledgeServices.setPageCookie();
    	testCookie = PublicKnowledgeServices.getPageCookie();
    	System.assertNotEquals(null, testCookie);
    	System.assert(PublicKnowledgeServices.pageCookieValid());
    	
    }
}