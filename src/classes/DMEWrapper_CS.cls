public class DMEWrapper_CS {
	public DME__c dmeRef{get;set;}
	public Integer tempID {get;set;}
	public Boolean selected {get;set;}
	
	public DMEWrapper_CS(){
		dmeRef = new DME__c();
		selected = false; 
		resetTempId();
	}//End Constructor
	
	public DMEWrapper_CS(DME__c d){
		this.dmeRef = d;
		selected = false;
		resetTempID();
	}//End Constructor with DME
	
	private void resetTempId(){
		this.tempID = Math.abs(Crypto.getRandomInteger());
	}//End resetTempId
	
}//End DMEWrapper_CS