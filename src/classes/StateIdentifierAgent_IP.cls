public with sharing class StateIdentifierAgent_IP {
    public StateIdentifierAgent_IP() {
        
    }

    public static void stateMedicaidIU(List<State_Identifier__c> newValues){

        //get the provider location into a map

        Set<Id> getProviderLocation = new Set<Id>();
        Map<Id, Provider_Location__c> proMap = new Map<Id, Provider_Location__c>(); 

        for(State_Identifier__c si : newValues){

            getProviderLocation.add(si.Provider_Location_New__c);   
        }

        System.debug('Number of Provider Locations added'+getProviderLocation.size());

        if(getProviderLocation.size() > 0){

            List<Provider_Location__c> queryProviderLocations = [Select Id,
                                                                        OSPHD__c,
                                                                        Alabama__c,
                                                                        Alaska__c,
                                                                        Arizona__c,
                                                                        Arkansas__c,
                                                                        California__c,
                                                                        Colorado__c,
                                                                        Connecticut__c,
                                                                        Delaware__c,
                                                                        Florida_1__c,
                                                                        Florida_2__c,
                                                                        Georgia__c,
                                                                        Hawaii__c,
                                                                        Idaho__c,
                                                                        Illinois__c,
                                                                        Indiana__c,
                                                                        Iowa__c,
                                                                        Kansas__c,
                                                                        Kentucky__c,
                                                                        Louisiana__c,
                                                                        Maine__c,
                                                                        Maryland__c,
                                                                        Massachusetts__c,
                                                                        Michigan__c,
                                                                        Minnesota__c,
                                                                        Mississippi__c,
                                                                        Missouri__c,
                                                                        Montana__c,
                                                                        Nebraska__c,
                                                                        Nevada__c,
                                                                        New_Hampshire__c,
                                                                        New_Jersey__c,
                                                                        New_Mexico__c,
                                                                        New_York__c,
                                                                        North_Carolina__c,
                                                                        North_Dakota__c,
                                                                        Ohio__c,
                                                                        Oklahoma__c,
                                                                        Oregon__c,
                                                                        Pennsylvania__c,
                                                                        Rhode_Island__c,
                                                                        South_Carolina__c,
                                                                        South_Dakota__c,
                                                                        Tennessee__c,
                                                                        Texas__c,
                                                                        Utah__c,
                                                                        Vermont__c,
                                                                        Virginia__c,
                                                                        Washington__c,
                                                                        West_Virginia__c,
                                                                        Wisconsin__c,
                                                                        Wyoming__c from Provider_Location__c where Id =: getProviderLocation];
            if(queryProviderLocations.size() > 0){

                for(Provider_Location__c pr: queryProviderLocations) {
                   
                    proMap.put(pr.Id, pr);
                }
            }
        }

        for(State_Identifier__c s : newValues){

            // map the record based on picklist

            // Case:1 - Alabama

            if(s.State__c == 'Alabama'){

                proMap.get(s.Provider_Location_New__c).Alabama__c = s.Medicaid__c;
                continue;
            }

            // Case:2 - Alaska

            if(s.State__c == 'Alaska'){

                proMap.get(s.Provider_Location_New__c).Alaska__c = s.Medicaid__c;
                continue;
            }

            // Case:3 - Arizona

            if(s.State__c == 'Arizona'){

                proMap.get(s.Provider_Location_New__c).Arizona__c = s.Medicaid__c;
                continue;
            }

            // Case:4 - Arkansas

            if(s.State__c == 'Arkansas'){

                proMap.get(s.Provider_Location_New__c).Arkansas__c = s.Medicaid__c;
                continue;
            }

            // Case:5 - California 

            if(s.State__c == 'California'){

                proMap.get(s.Provider_Location_New__c).California__c = s.Medicaid__c;
                proMap.get(s.Provider_Location_New__c).OSPHD__c = s.OSPHD__c;
                continue;
            }

            // Case:6 - Colorado

            if(s.State__c == 'Colorado'){

                proMap.get(s.Provider_Location_New__c).Colorado__c = s.Medicaid__c;
                continue;
            }

            // Case:7 - Conneticut

            if(s.State__c == 'Connecticut'){

                proMap.get(s.Provider_Location_New__c).Connecticut__c = s.Medicaid__c;
                continue;
            }

            // Case:8 - Delaware

            if(s.State__c == 'Delaware'){

                proMap.get(s.Provider_Location_New__c).Delaware__c = s.Medicaid__c;
                continue;
            }

            // Case:9a - Florida

            if(s.State__c == 'Florida1'){

                proMap.get(s.Provider_Location_New__c).Florida_1__c = s.Medicaid__c;
                continue;
            }

            // Case:9b - Florida

            if(s.State__c == 'Florida2'){

                proMap.get(s.Provider_Location_New__c).Florida_2__c = s.Medicaid__c;
                continue;
            }

            // Case:10 - Georgia

            if(s.State__c == 'Georgia'){

                proMap.get(s.Provider_Location_New__c).Georgia__c = s.Medicaid__c;
                continue;
            }

            // Case:11 - Hawaii

            if(s.State__c == 'Hawaii'){

                proMap.get(s.Provider_Location_New__c).Hawaii__c = s.Medicaid__c;
                continue;
            }

            // Case:12 - Idaho

            if(s.State__c == 'Idaho'){

                proMap.get(s.Provider_Location_New__c).Idaho__c = s.Medicaid__c;
                continue;
            }

            // Case:13 - Illinois

            if(s.State__c == 'Illinois'){

                proMap.get(s.Provider_Location_New__c).Illinois__c = s.Medicaid__c;
                continue;
            }

            // Case:14 - Indiana

            if(s.State__c == 'Indiana'){

                proMap.get(s.Provider_Location_New__c).Indiana__c = s.Medicaid__c;
                continue;
            }

            // Case:15 - Iowa

            if(s.State__c == 'Iowa'){

                proMap.get(s.Provider_Location_New__c).Iowa__c = s.Medicaid__c;
                continue;
            }

            // Case:16 - Kansas

            if(s.State__c == 'Kansas'){

                proMap.get(s.Provider_Location_New__c).Kansas__c = s.Medicaid__c;
                continue;
            }

            // Case:17 - Kentucky

            if(s.State__c == 'Kentucky'){

                proMap.get(s.Provider_Location_New__c).Kentucky__c = s.Medicaid__c;
                continue;
            }

            // Case:18 - Louisiana

            if(s.State__c == 'Louisiana'){

                proMap.get(s.Provider_Location_New__c).Louisiana__c = s.Medicaid__c;
                continue;
            }

            // Case:19 - Maine

            if(s.State__c == 'Maine'){

                proMap.get(s.Provider_Location_New__c).Maine__c = s.Medicaid__c;
                continue;
            }

            // Case:20 - Maryland

            if(s.State__c == 'Maryland'){

                proMap.get(s.Provider_Location_New__c).Maryland__c = s.Medicaid__c;
                continue;
            }

            // Case:21 - Massachusetts

            if(s.State__c == 'Massachusetts'){

                proMap.get(s.Provider_Location_New__c).Massachusetts__c = s.Medicaid__c;
                continue;
            }

            // Case:22 - Michigan

            if(s.State__c == 'Michigan'){

                proMap.get(s.Provider_Location_New__c).Michigan__c = s.Medicaid__c;
                continue;
            }

            // Case:23 - Minnesota

            if(s.State__c == 'Minnesota'){

                proMap.get(s.Provider_Location_New__c).Minnesota__c = s.Medicaid__c;
                continue;
            }

            // Case:24 - Mississippi

            if(s.State__c == 'Mississippi'){

                proMap.get(s.Provider_Location_New__c).Mississippi__c = s.Medicaid__c;
                continue;
            }

            // Case:25 - Missouri

            if(s.State__c == 'Missouri'){

                proMap.get(s.Provider_Location_New__c).Missouri__c = s.Medicaid__c;
                continue;
            }

            // Case:26 - Montana

            if(s.State__c == 'Montana'){

                proMap.get(s.Provider_Location_New__c).Montana__c = s.Medicaid__c;
                continue;
            }

            // Case:27 - Nebraska

            if(s.State__c == 'Nebraska'){

                proMap.get(s.Provider_Location_New__c).Nebraska__c = s.Medicaid__c;
                continue;
            }

            // Case:28 - Nevada

            if(s.State__c == 'Nevada'){

                proMap.get(s.Provider_Location_New__c).Nevada__c = s.Medicaid__c;
                continue;
            }

            // Case:29 - New Hampshire

            if(s.State__c == 'New Hampshire'){

                proMap.get(s.Provider_Location_New__c).New_Hampshire__c = s.Medicaid__c;
                continue;
            }

            // Case:30 - New Jersey

            if(s.State__c == 'New Jersey'){

                proMap.get(s.Provider_Location_New__c).New_Jersey__c = s.Medicaid__c;
                continue;
            }

            // Case:31 - New Mexico

            if(s.State__c == 'New Mexico'){

                proMap.get(s.Provider_Location_New__c).New_Mexico__c = s.Medicaid__c;
                continue;
            }

            // Case:32 - New York

            if(s.State__c == 'New York'){

                proMap.get(s.Provider_Location_New__c).New_York__c = s.Medicaid__c;
                continue;
            }

            // Case:33 - North Carolina

            if(s.State__c == 'North Carolina'){

                proMap.get(s.Provider_Location_New__c).North_Carolina__c = s.Medicaid__c;
                continue;
            }

            // Case:34 - North Dakota

            if(s.State__c == 'North Dakota'){

                proMap.get(s.Provider_Location_New__c).North_Dakota__c = s.Medicaid__c;
                continue;
            }

            // Case:35 - Ohio

            if(s.State__c == 'Ohio'){

                proMap.get(s.Provider_Location_New__c).Ohio__c = s.Medicaid__c;
                continue;
            }

            // Case:36 - Oklahoma

            if(s.State__c == 'Oklahoma'){

                proMap.get(s.Provider_Location_New__c).Oklahoma__c = s.Medicaid__c;
                continue;
            }

            // Case:37 - Oregon

            if(s.State__c == 'Oregon'){

                proMap.get(s.Provider_Location_New__c).Oregon__c = s.Medicaid__c;
                continue;
            }

            // Case:38 - Pennsylvania

            if(s.State__c == 'Pennsylvania'){

                proMap.get(s.Provider_Location_New__c).Pennsylvania__c = s.Medicaid__c;
                continue;
            }

            // Case:39 - Rhode Island

            if(s.State__c == 'Rhode Island'){

                proMap.get(s.Provider_Location_New__c).Rhode_Island__c = s.Medicaid__c;
                continue;
            }

            // Case:40 - South Carolina

            if(s.State__c == 'South Carolina'){

                proMap.get(s.Provider_Location_New__c).South_Carolina__c = s.Medicaid__c;
                continue;
            }

            // Case:41 - South Dakota

            if(s.State__c == 'South Dakota'){

                proMap.get(s.Provider_Location_New__c).South_Dakota__c = s.Medicaid__c;
                continue;
            }

            // Case:42 - Tennessee

            if(s.State__c == 'Tennessee'){

                proMap.get(s.Provider_Location_New__c).Tennessee__c = s.Medicaid__c;
                continue;
            }

            // Case:43 - Texas

            if(s.State__c == 'Texas'){

                proMap.get(s.Provider_Location_New__c).Texas__c = s.Medicaid__c;
                continue;
            }

            // Case:44 - Utah

            if(s.State__c == 'Utah'){

                proMap.get(s.Provider_Location_New__c).Utah__c = s.Medicaid__c;
                continue;
            }

            // Case:45 - Vermont

            if(s.State__c == 'Vermont'){

                proMap.get(s.Provider_Location_New__c).Vermont__c = s.Medicaid__c;
                continue;
            }

            // Case:46 - Virginia

            if(s.State__c == 'Virginia'){

                proMap.get(s.Provider_Location_New__c).Virginia__c = s.Medicaid__c;
                continue;
            }

            // Case:47 - Washington

            if(s.State__c == 'Washington'){

                proMap.get(s.Provider_Location_New__c).Washington__c = s.Medicaid__c;
                continue;
            }

            // Case:48 - West Virginia

            if(s.State__c == 'West Virginia'){

                proMap.get(s.Provider_Location_New__c).West_Virginia__c = s.Medicaid__c;
                continue;
            }

            // Case:49 - Wisconsin

            if(s.State__c == 'Wisconsin'){

                proMap.get(s.Provider_Location_New__c).Wisconsin__c = s.Medicaid__c;
                continue;
            }

            // Case:50 - Wyoming

            if(s.State__c == 'Wyoming'){

                proMap.get(s.Provider_Location_New__c).Wyoming__c = s.Medicaid__c;
                continue;
            }

        }

        update proMap.values();

    }

    public static void stateMedicaidDel(List<State_Identifier__c> toDelete){

        Set<Id> getProviderLocation = new Set<Id>();
        Map<Id, Provider_Location__c> proMap = new Map<Id, Provider_Location__c>(); 

        for(State_Identifier__c si : toDelete){

            getProviderLocation.add(si.Provider_Location_New__c);   
        }

        System.debug('Number of Provider Locations added'+getProviderLocation.size());

        if(getProviderLocation.size() > 0){

            List<Provider_Location__c> queryProviderLocations = [Select Id, Alabama__c,
                                                                             Alaska__c,
                                                                             Arizona__c,
                                                                             Arkansas__c,
                                                                             California__c,
                                                                             Colorado__c,
                                                                             Connecticut__c,
                                                                             Delaware__c,
                                                                             Florida_1__c,
                                                                             Florida_2__c,
                                                                             Georgia__c,
                                                                             Hawaii__c,
                                                                             Idaho__c,
                                                                             Illinois__c,
                                                                             Indiana__c,
                                                                             Iowa__c,
                                                                             Kansas__c,
                                                                             Kentucky__c,
                                                                             Louisiana__c,
                                                                             Maine__c,
                                                                             Maryland__c,
                                                                             Massachusetts__c,
                                                                             Michigan__c,
                                                                             Minnesota__c,
                                                                             Mississippi__c,
                                                                             Missouri__c,
                                                                             Montana__c,
                                                                             Nebraska__c,
                                                                             Nevada__c,
                                                                             New_Hampshire__c,
                                                                             New_Jersey__c,
                                                                             New_Mexico__c,
                                                                             New_York__c,
                                                                             North_Carolina__c,
                                                                             North_Dakota__c,
                                                                             Ohio__c,
                                                                             Oklahoma__c,
                                                                             Oregon__c,
                                                                             Pennsylvania__c,
                                                                             Rhode_Island__c,
                                                                             South_Carolina__c,
                                                                             South_Dakota__c,
                                                                             Tennessee__c,
                                                                             Texas__c,
                                                                             Utah__c,
                                                                             Vermont__c,
                                                                             Virginia__c,
                                                                             Washington__c,
                                                                             West_Virginia__c,
                                                                             Wisconsin__c,
                                                                             Wyoming__c from Provider_Location__c where Id =: getProviderLocation];
            if(queryProviderLocations.size() > 0){

                for(Provider_Location__c pr: queryProviderLocations) {
                   
                    proMap.put(pr.Id, pr);
                }
            }
        }

        for(State_Identifier__c s : toDelete){

            // map the record based on picklist

            // Case:1 - Alabama

            if(s.State__c == 'Alabama'){

                proMap.get(s.Provider_Location_New__c).Alabama__c = '';
                continue;
            }

            // Case:2 - Alaska

            if(s.State__c == 'Alaska'){

                proMap.get(s.Provider_Location_New__c).Alaska__c = '';
                continue;
            }

            // Case:3 - Arizona

            if(s.State__c == 'Arizona'){

                proMap.get(s.Provider_Location_New__c).Arizona__c = '';
                continue;
            }

            // Case:4 - Arkansas

            if(s.State__c == 'Arkansas'){

                proMap.get(s.Provider_Location_New__c).Arkansas__c = '';
                continue;
            }

            // Case:5 - California 

            if(s.State__c == 'California'){

                proMap.get(s.Provider_Location_New__c).California__c = '';
                proMap.get(s.Provider_Location_New__c).OSPHD__c = '';
                continue;
            }

            // Case:6 - Colorado

            if(s.State__c == 'Colorado'){

                proMap.get(s.Provider_Location_New__c).Colorado__c = '';
                continue;
            }

            // Case:7 - Conneticut

            if(s.State__c == 'Connecticut'){

                proMap.get(s.Provider_Location_New__c).Connecticut__c = '';
                continue;
            }

            // Case:8 - Delaware

            if(s.State__c == 'Delaware'){

                proMap.get(s.Provider_Location_New__c).Delaware__c = '';
                continue;
            }

            // Case:9a - Florida

            if(s.State__c == 'Florida1'){

                proMap.get(s.Provider_Location_New__c).Florida_1__c = '';
                continue;
            }

            // Case:9b - Florida

            if(s.State__c == 'Florida2'){

                proMap.get(s.Provider_Location_New__c).Florida_2__c = '';
                continue;
            }

            // Case:10 - Georgia

            if(s.State__c == 'Georgia'){

                proMap.get(s.Provider_Location_New__c).Georgia__c = '';
                continue;
            }

            // Case:11 - Hawaii

            if(s.State__c == 'Hawaii'){

                proMap.get(s.Provider_Location_New__c).Hawaii__c = '';
                continue;
            }

            // Case:12 - Idaho

            if(s.State__c == 'Idaho'){

                proMap.get(s.Provider_Location_New__c).Idaho__c = '';
                continue;
            }

            // Case:13 - Illinois

            if(s.State__c == 'Illinois'){

                proMap.get(s.Provider_Location_New__c).Illinois__c = '';
                continue;
            }

            // Case:14 - Indiana

            if(s.State__c == 'Indiana'){

                proMap.get(s.Provider_Location_New__c).Indiana__c = '';
                continue;
            }

            // Case:15 - Iowa

            if(s.State__c == 'Iowa'){

                proMap.get(s.Provider_Location_New__c).Iowa__c = '';
                continue;
            }

            // Case:16 - Kansas

            if(s.State__c == 'Kansas'){

                proMap.get(s.Provider_Location_New__c).Kansas__c = '';
                continue;
            }

            // Case:17 - Kentucky

            if(s.State__c == 'Kentucky'){

                proMap.get(s.Provider_Location_New__c).Kentucky__c = '';
                continue;
            }

            // Case:18 - Louisiana

            if(s.State__c == 'Louisiana'){

                proMap.get(s.Provider_Location_New__c).Louisiana__c = '';
                continue;
            }

            // Case:19 - Maine

            if(s.State__c == 'Maine'){

                proMap.get(s.Provider_Location_New__c).Maine__c = '';
                continue;
            }

            // Case:20 - Maryland

            if(s.State__c == 'Maryland'){

                proMap.get(s.Provider_Location_New__c).Maryland__c = '';
                continue;
            }

            // Case:21 - Massachusetts

            if(s.State__c == 'Massachusetts'){

                proMap.get(s.Provider_Location_New__c).Massachusetts__c = '';
                continue;
            }

            // Case:22 - Michigan

            if(s.State__c == 'Michigan'){

                proMap.get(s.Provider_Location_New__c).Michigan__c = '';
                continue;
            }

            // Case:23 - Minnesota

            if(s.State__c == 'Minnesota'){

                proMap.get(s.Provider_Location_New__c).Minnesota__c = '';
                continue;
            }

            // Case:24 - Mississippi

            if(s.State__c == 'Mississippi'){

                proMap.get(s.Provider_Location_New__c).Mississippi__c = '';
                continue;
            }

            // Case:25 - Missouri

            if(s.State__c == 'Missouri'){

                proMap.get(s.Provider_Location_New__c).Missouri__c = '';
                continue;
            }

            // Case:26 - Montana

            if(s.State__c == 'Montana'){

                proMap.get(s.Provider_Location_New__c).Montana__c = '';
                continue;
            }

            // Case:27 - Nebraska

            if(s.State__c == 'Nebraska'){

                proMap.get(s.Provider_Location_New__c).Nebraska__c = '';
                continue;
            }

            // Case:28 - Nevada

            if(s.State__c == 'Nevada'){

                proMap.get(s.Provider_Location_New__c).Nevada__c = '';
                continue;
            }

            // Case:29 - New Hampshire

            if(s.State__c == 'New Hampshire'){

                proMap.get(s.Provider_Location_New__c).New_Hampshire__c = '';
                continue;
            }

            // Case:30 - New Jersey

            if(s.State__c == 'New Jersey'){

                proMap.get(s.Provider_Location_New__c).New_Jersey__c = '';
                continue;
            }

            // Case:31 - New Mexico

            if(s.State__c == 'New Mexico'){

                proMap.get(s.Provider_Location_New__c).New_Mexico__c = '';
                continue;
            }

            // Case:32 - New York

            if(s.State__c == 'New York'){

                proMap.get(s.Provider_Location_New__c).New_York__c = '';
                continue;
            }

            // Case:33 - North Carolina

            if(s.State__c == 'North Carolina'){

                proMap.get(s.Provider_Location_New__c).North_Carolina__c = '';
                continue;
            }

            // Case:34 - North Dakota

            if(s.State__c == 'North Dakota'){

                proMap.get(s.Provider_Location_New__c).North_Dakota__c = '';
                continue;
            }

            // Case:35 - Ohio

            if(s.State__c == 'Ohio'){

                proMap.get(s.Provider_Location_New__c).Ohio__c = '';
                continue;
            }

            // Case:36 - Oklahoma

            if(s.State__c == 'Oklahoma'){

                proMap.get(s.Provider_Location_New__c).Oklahoma__c = '';
                continue;
            }

            // Case:37 - Oregon

            if(s.State__c == 'Oregon'){

                proMap.get(s.Provider_Location_New__c).Oregon__c = '';
                continue;
            }

            // Case:38 - Pennsylvania

            if(s.State__c == 'Pennsylvania'){

                proMap.get(s.Provider_Location_New__c).Pennsylvania__c = '';
                continue;
            }

            // Case:39 - Rhode Island

            if(s.State__c == 'Rhode Island'){

                proMap.get(s.Provider_Location_New__c).Rhode_Island__c = '';
                continue;
            }

            // Case:40 - South Carolina

            if(s.State__c == 'South Carolina'){

                proMap.get(s.Provider_Location_New__c).South_Carolina__c = '';
                continue;
            }

            // Case:41 - South Dakota

            if(s.State__c == 'South Dakota'){

                proMap.get(s.Provider_Location_New__c).South_Dakota__c = '';
                continue;
            }

            // Case:42 - Tennessee

            if(s.State__c == 'Tennessee'){

                proMap.get(s.Provider_Location_New__c).Tennessee__c = '';
                continue;
            }

            // Case:43 - Texas

            if(s.State__c == 'Texas'){

                proMap.get(s.Provider_Location_New__c).Texas__c = '';
                continue;
            }

            // Case:44 - Utah

            if(s.State__c == 'Utah'){

                proMap.get(s.Provider_Location_New__c).Utah__c = '';
                continue;
            }

            // Case:45 - Vermont

            if(s.State__c == 'Vermont'){

                proMap.get(s.Provider_Location_New__c).Vermont__c = '';
                continue;
            }

            // Case:46 - Virginia

            if(s.State__c == 'Virginia'){

                proMap.get(s.Provider_Location_New__c).Virginia__c = '';
                continue;
            }

            // Case:47 - Washington

            if(s.State__c == 'Washington'){

                proMap.get(s.Provider_Location_New__c).Washington__c = '';
                continue;
            }

            // Case:48 - West Virginia

            if(s.State__c == 'West Virginia'){

                proMap.get(s.Provider_Location_New__c).West_Virginia__c = '';
                continue;
            }

            // Case:49 - Wisconsin

            if(s.State__c == 'Wisconsin'){

                proMap.get(s.Provider_Location_New__c).Wisconsin__c = '';
                continue;
            }

            // Case:50 - Wyoming

            if(s.State__c == 'Wyoming'){

                proMap.get(s.Provider_Location_New__c).Wyoming__c = '';
                continue;
            }

        }

        update proMap.values();

    }
}