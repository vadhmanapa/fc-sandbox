public class CaseAssignment {

/********************************
*** Commented out by Venkat as of 09/21/2015 as all the assignment process is handled by Case Assignment Rules.
*********************************/

    //Used for testing, set to false for normal behavior
    private static final Boolean TEST = false;

    //Used for "add doctor" matching
    private static final Integer N_WORD_LOOKAHEAD = 4;

    //Used for Customer Service Group
    public static final String CUSTOMER_SERVICE_GROUP_NAME = 'Email to Case Assignment';

    public static Set<Id> getCustomerServiceUserIds() {

        //Unfortunately the TYPEOF and UserOrGroupId.Type for polymorphic queries
        //are unavailable for GroupMembers. Collect the ids and then use a second query.
        Set<Id> customerServiceUserIds = new Set<Id>();

        List<GroupMember> customerServiceMembers = [
            SELECT Id,GroupId,UserOrGroupId
            FROM GroupMember
            WHERE Group.Name = :CUSTOMER_SERVICE_GROUP_NAME
        ];

        for(GroupMember member : customerServiceMembers) {

            if(member.UserOrGroupId.getSObjectType() != User.getSObjectType()) {
                system.debug(LoggingLevel.INFO,'Member ' + member.UserOrGroupId + ' is not a User.');
                continue;
            }

            system.debug(LoggingLevel.INFO,'Found Customer Service User: ' + member.UserOrGroupId);
            customerServiceUserIds.add(member.UserOrGroupId);
        }

        Map<Id,User> users = new Map<Id,User>([
            SELECT Id
            FROM User
            WHERE Id IN :customerServiceUserIds
            AND IsActive = true
        ]);

        return users.keySet();
    }

    //NOTE: These usernames are normalized to exclude domain suffix
    public static Map<String,User> usernameMap {
        get {
            if(usernameMap == null){
                usernameMap = new Map<String,User>();
                for(User u: [
                    SELECT Id,Name,Username
                    FROM User
                    WHERE IsActive = true
                ]){
                    usernameMap.put(normalizeUsername(u.Username),u);
                }
            }
            return usernameMap;
        }
        private set;
    }

    public static Map<String,Group> queueMap {
        get {
            if(queueMap == null) {
                queueMap = new Map<String,Group>();
                for(Group queue : [
                    SELECT Name
                    FROM Group
                    WHERE Type = 'Queue'
                ]){
                    queueMap.put(queue.Name,queue);
                }
            }
            return queueMap;
        }
    }

    //IMPORTANT: REMOVE USERNAME DOMAIN SUFFIX
    public static String normalizeUsername(String username) {
        return username.substring(0,username.lastIndexOf('.com')+4);
    }

    //Main assignment process, one case
    /*public static void assignCaseOwner(Case record) {
        assignCaseOwners(new List<Case>{record});
    }

    //Main assignment process, multiple cases
    public static void assignCaseOwners(List<Case> records) {

        for(Case c : records) {

            //Setup notification
            c.Send_Assignment_Notification__c = true;

            CaseUtils.debugOnCase(c,'== Starting Assignment Process ==');

            //TEST CONDITION
            if(TEST) {
                CaseUtils.debugOnCase(c,'Test assignment to clouddev');
                assignOwner(c,'cloudsoftware@accessintegra.com');
                continue;
            }

            //Condition 1: Kristen Domenici
            CaseUtils.debugOnCase(c,'Check: Username: Kristen Domenici');
            if(normalizeUsername(UserInfo.getUserName()) == 'kdomenici@accessintegra.com') {
                CaseUtils.debugOnCase(c,'Match: Username: Kristen Domenici');
                assignOwner(c,'kdomenici@accessintegra.com');
                continue;
            }

            //Condition 2: Complaint
            CaseUtils.debugOnCase(c,'Check: Complaint');
            if( c.RecordTypeId == RecordTypes.complaintId) {
                CaseUtils.debugOnCase(c,'Match: Complaint');
                assignOwner(c,'krafael@integrah.com');
                continue;
            }

            //Condition 3: Que
            CaseUtils.debugOnCase(c,'Check: Que Support');
            if( c.RecordTypeId == RecordTypes.queId) {
                CaseUtils.debugOnCase(c,'Match: Que Support');
                assignOwner(c,'Que Support Inbox');
                continue;
            }

            //Condition 4: Email Cases
            if( c.RecordTypeId == RecordTypes.emailId) {

                if(String.isBlank(c.SuppliedEmail)){
                    CaseUtils.debugOnCase(c,'No supplied email found.');
                }
                else{
                    CaseUtils.debugOnCase(c,'Found email: ' + c.SuppliedEmail);
                }

                //Subcondition 4a: Centerlight
                CaseUtils.debugOnCase(c,'Check: Centerlight email domain');
                if( String.isNotBlank(c.SuppliedEmail) &&
                    c.SuppliedEmail.contains('@centerlight.org'))
                {
                    CaseUtils.debugOnCase(c,'Match: Centerlight email domain');
                    assignOwner(c,'krafael@integrah.com');
                    continue;
                }

                //Subcondition 4b: Wellcare
                CaseUtils.debugOnCase(c,'Check: Wellcare email domain');
                if( String.isNotBlank(c.SuppliedEmail) &&
                    c.SuppliedEmail.contains('@wellcare.org'))
                {
                    CaseUtils.debugOnCase(c,'Match: Wellcare email domain');
                    assignOwner(c,'aseide@accessintegra.com');
                    continue;
                }

                //Subcondition 4c: Villagecare
                CaseUtils.debugOnCase(c,'Check: Villagecare email domain');
                if( String.isNotBlank(c.SuppliedEmail) &&
                    c.SuppliedEmail.contains('@villagecare.org'))
                {
                    CaseUtils.debugOnCase(c,'Match: Villagecare email domain');
                    assignOwner(c,'krafael@integrah.com'); // Changed by Venkat on 05/20/2015 as Elissa left.
                    continue;
                }

                //Subcondition 4d: Secure-Emblemhealth
                CaseUtils.debugOnCase(c,'Check: Secure-emblemhealth email domain');
                if( String.isNotBlank(c.SuppliedEmail) &&
                    c.SuppliedEmail.contains('@secure-emblemhealth.com'))
                {
                    CaseUtils.debugOnCase(c,'Match: secure-emblemhealth email domain');
                    assignOwner(c,'mnavon@accessintegra.com');
                    continue;
                }

                //Subcondition 4e: Add Doctor
                CaseUtils.debugOnCase(c,'Check: Add doctor criteria');
                if( addDoctorMatch(c) ) {
                    CaseUtils.debugOnCase(c,'Match: Add doctor criteria');
                    CaseDistribution dist = new CaseDistribution(new Set<Id>{
                        usernameMap.get('echa@accessintegra.com').Id
                        //usernameMap.get('salmer@accessintegra.com').Id	// karnold - 12/2/2015 - removed because user is deactivated
                    });
                    c.OwnerId = dist.getNextAssignee();
                    CaseUtils.debugOnCase(c,'Case owner: ' + c.OwnerId);
                    continue;
                }

                //Subcondition 4f: Referral
                CaseUtils.debugOnCase(c,'Check: Referral criteria');
                if( referralMatch(c) ) {
                    CaseUtils.debugOnCase(c,'Match: Referral criteria');
                    // assignOwner(c,'cadams@accessintegra.com');
                    // modified by Venkat as Christina Adams left Integra
                    CaseDistribution cDist = new CaseDistribution(new Set<Id>{
                        usernameMap.get('echa@accessintegra.com').Id
                        //usernameMap.get('salmer@accessintegra.com').Id	// karnold - 12/2/2015 - removed because user is deactivated
                    });
                    c.OwnerId = cDist.getNextAssignee();
                    CaseUtils.debugOnCase(c,'Case owner: ' + c.OwnerId);
                    continue;
                }

                //Subcondition 4g: Emblem
                CaseUtils.debugOnCase(c,'Check: Emblem criteria');
                if( emblemMatch(c) ) {
                    CaseUtils.debugOnCase(c,'Match: Emblem criteria');
                    assignOwner(c,'kphommathep@accessintegra.com');
                    continue;
                }

                //Fallback: Case Distribution
                CaseUtils.debugOnCase(c,'Fallback: Customer Service Distribution');
                CaseDistribution dist = new CaseDistribution(getCustomerServiceUserIds());
                c.OwnerId = dist.getNextAssignee();
                CaseUtils.debugOnCase(c,'Case owner: ' + c.OwnerId);
                continue;
            }

            //Fallback, assign to current user or Case owner
            if(c.OwnerId == null){
                CaseUtils.debugOnCase(c,'Fallback: Current user');
                assignOwner(c,normalizeUsername(UserInfo.getUserName()));
            }
            else {
                CaseUtils.debugOnCase(c,'Fallback: Current owner ' + c.OwnerId);
            }
        }
    }

    //Assign the entity to the case
    private static void assignOwner(Case c, String userOrGroupName) {

        if(usernameMap.containsKey(userOrGroupName)){
            c.OwnerId = usernameMap.get(userOrGroupName).Id;
            return;
        }
        else if(queueMap.containsKey(userOrGroupName)){
            c.OwnerId = queueMap.get(userOrGroupName).Id;
            return;
        }

        CaseUtils.debugOnCase(c,'Error: Couldn\'t assign "' + userOrGroupName + '"');
    }

    //Advanced search for 'add doctor' criteria
    @TestVisible
    public static Boolean addDoctorMatch(Case c) {

        //Only search subject
        if(String.isBlank(c.Subject)) {
            CaseUtils.debugOnCase(c,'No case subject!');
            return false;
        }

        String subject = c.Subject.toLowerCase();

        //Add doctor criteria
        List<String> tokens = subject.split(' ',0);
        // modified by Venkat for more simpler routing process
        Integer countMatches = 0;// added to count the number of matching doc keywords
        List<String> possibleDocKeywords = new List<String>{'doc','doctor','dr','dr.','physician','md'};
            for (String str : possibleDocKeywords){
                if (subject.contains(str)){
                    countMatches++;
                }
            }
        if (countMatches > 0){

            return true;

        } else{

             return false;
        }
        //-----------------------------------------------------------------------------------------------------------------------------------------------------
        //Look at each word in the subject
       /* for(Integer i = 0; i < tokens.size(); i++){

            String token = tokens[i];

            if( token == 'add' || token == 'refer'){

                for(Integer j = i; j < i + N_WORD_LOOKAHEAD; j++) {

                    //Because we can't use multiple exit criteria in a for loop...
                    if(j >= tokens.size()) {
                        break;
                    }

                    if( tokens[j] == 'doctor' ||
                        tokens[j] == 'dr' ||
                        tokens[j] == 'dr.')
                    {
                        return true;
                    }
                }
            }
        } // NOTE: commented out already. On case of re-activating comment out till here.

    }

    //Advanced search for referral criteria
    @TestVisible
    private static Boolean referralMatch(Case c) {

        //Only search subject
        if(String.isBlank(c.Subject)) {
            CaseUtils.debugOnCase(c,'No case subject!');
            return false;
        }

        String subject = c.Subject.toLowerCase();

        //Referral criteria
        if( subject.contains('referral') ||
            subject.contains('referring') )
        {
            return true;
        }

        return false;
    }

    //Advanced serach for emblem criteria
    @TestVisible
    private static Boolean emblemMatch(Case c){

        //Only search subject
        if(String.isBlank(c.Subject)) {
            CaseUtils.debugOnCase(c,'No case subject!');
            return false;
        }

        String subject = c.Subject.toLowerCase();

        if(subject.contains('emblem')) {
            return true;
        }

        return false;
    }*/
}