public class relatedAccountsController {

    public String selectedId {get;set;}
    public Boolean shouldRedirect {get;set;}
    public String redirectUrl {get;set;}
    
    public List<Account> childList {
        get {
            if (childList == null) {
                childList = [
                    SELECT
                        Name,
                        Health_of_Relationship__c,
                        Estimated_Revenue_Per_Member__c,
                        Total_Members__c,
                        Actual_DMEPOS_Spend__c,
                        Estimated_DMEPOS_Spend__c
                    FROM Account
                    WHERE ParentId = :ApexPages.currentPage().getParameters().get('Id')
                ];
            }
            System.debug('Page id: ' + ApexPages.currentPage().getParameters().get('Id'));
            System.debug('Child list: ' + childList);
            return childList;
        }
        set;
    }
    
    public relatedAccountsController(ApexPages.StandardController sc) {
        shouldRedirect = false;
    }
    
    public void goToAccount() {
        shouldRedirect = true;
        System.debug('Selected Account Id: ' + selectedId);
        PageReference returnPage = new PageReference('/' + selectedId);
        returnPage.setRedirect(true);
        redirectUrl = returnPage.getUrl();
    }
}