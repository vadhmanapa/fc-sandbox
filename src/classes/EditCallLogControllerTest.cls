@isTest
private class EditCallLogControllerTest {

	static PageReference pageRef;
	static EditCallLogController pageCon;

	static Call_log__c newCallLog;
	static List<Call_Log__c> existingCallLogs;
	
	static Account providerAccount;
	static Account payorAccount;
	static Contact provider;

    static testMethod void resetCallerInfo() {
    	init();
    	
    	pageCon.log.Caller_Account__c = providerAccount.Id;
    	pageCon.log.Caller_Contact__c = provider.Id;
    	pageCon.resetCallerInfo();
    	
    	//Caller info
    	System.assertEquals(provider.FirstName,pageCon.log.Caller_First_Name__c);
    	System.assertEquals(provider.LastName,pageCon.log.Caller_Last_Name__c);
    	System.assertEquals(provider.Phone,pageCon.log.Caller_Phone__c);
    	System.assertEquals('Provider',pageCon.log.Caller_Type__c);
    	
    	pageCon.log.Call_Reason__c = 'Test';
    	pageCon.log.Call_Result__c = 'Test';
    	pageCon.log.Caller_Recontact_PL__c = 'Test';	
    	pageCon.close();
    	
    	//Log status 	
    	System.assertEquals('Closed',pageCon.log.Status__c);
    }
    
    static testMethod void newContact() {
		init();
		
		pageCon.newContact = new Contact(
			AccountId = providerAccount.Id,
			FirstName = 'New',
			LastName = 'Contact',
			Phone = '222-222-2222',
			RecordTypeId = RecordTypes.providerContactId
		);    	
    	
    	pageCon.saveNewContact();
    	
    	Contact newContact = [
    		SELECT Id,FirstName,LastName,RecordTypeId
    		FROM Contact
    		WHERE FirstName = 'New'
    		AND LastName = 'Contact'
    	];
    	
    	System.assertEquals('New',newContact.FirstName);
    	System.assertEquals('Contact',newContact.LastName);
    	System.assertEquals(RecordTypes.providerContactId,newContact.RecordTypeId);
    }
    
    static testMethod void newPatient() {
    	init();
		
		pageCon.newPatient = new Contact(
			AccountId = providerAccount.Id,
			FirstName = 'New',
			LastName = 'Patient',
			Phone = '222-222-2222',
			RecordTypeId = RecordTypes.patientId
		);    	
    	
    	pageCon.saveNewPatient();
    	
    	Contact newContact = [
    		SELECT Id,FirstName,LastName,RecordTypeId
    		FROM Contact
    		WHERE FirstName = 'New'
    		AND LastName = 'Patient'
    	];
    	
    	System.assertEquals('New',newContact.FirstName);
    	System.assertEquals('Patient',newContact.LastName);
    	System.assertEquals(RecordTypes.patientId,newContact.RecordTypeId);
    }
    
    static testMethod void newCase() {
    	init();
		
		//New case relies on Caller info
		pageCon.log.Caller_Account__c = providerAccount.Id;
    	pageCon.log.Caller_Contact__c = provider.Id;
    	pageCon.resetCallerInfo();
    	
    	//Set Case info and save
    	pageCon.newCase.Subject = 'Test Case';
    	pageCon.updateCaseContact();
    	pageCon.saveNewCase();
    	
    	//The controller now shows the Case
		System.assertNotEquals(0,pageCon.callerCaseList.size());
    }
    
    static testMethod void newInquiry() {
    	init();
    	
    	//New inquiry relies on Caller info
		pageCon.log.Caller_Account__c = providerAccount.Id;
    	pageCon.log.Caller_Contact__c = provider.Id;
    	pageCon.resetCallerInfo();
    	
    	//Set inquiry info and save
    	pageCon.newInquiry.Contact__c = provider.Id;
    	pageCon.newInquiry.Provider__c = providerAccount.Id;
    	pageCon.newInquiry.Health_Plan__c = payorAccount.Id;
    	pageCon.newInquiry.Bill_Number__c = '12345';
    	pageCon.newInquiry.Issue_Summary__c = 'Test summary';
    	pageCon.saveNewInquiry();
    	
    	System.debug([SELECT Id,Contact__c,Provider__c FROM Claims_Inquiry__c]);
    	
    	//The controller now shows the inquiry
    	System.assertNotEquals(0,pageCon.callerInquiryList.size());
    }
    
    static testMethod void coverage(){
    	init();
    	
    	//Cover any selectlists
    	List<SelectOption> sol = pageCon.callReasonDetailOptions;
    	sol = pageCon.queSupportDetailOptions;
    	sol = pageCon.contactRecordTypesSelectOptions;
    	sol = EditCallLogController.linkOptions;
    	
    	//Cover the record type maps for contact
    	Map<String, Map<String, Boolean>> testMap = pageCon.recordTypeIdToFieldToRequired;
    	testMap = pageCon.recordTypeIdToFieldToRendered;
    }
    
    static void init() {
    	
    	providerAccount = new Account(
    		Name = 'Provider Account',
    		RecordTypeId = RecordTypes.providerAccountId
    	);
    	insert providerAccount;
    	
    	payorAccount = new Account(
    		Name = 'Payor Account',
    		RecordTypeId = RecordTypes.payorAccountId
    	);
    	insert payorAccount;
    	
    	provider = new Contact(
    		AccountId = providerAccount.Id,
    		FirstName = 'Test',
    		LastName = 'Contact',
    		Phone = '111-111-1111',
    		Email = 'test@test.com',
    		RecordTypeId = RecordTypes.providerContactId,
			Status__c = 'Active'
    	);
    	insert provider;
    	
    	newCallLog = new Call_Log__c();
    	insert newCallLog;
    	
    	existingCallLogs = new List<Call_Log__c>();
    	for(Integer i = 0; i < 5; i++){
    		existingCallLogs.add(new Call_Log__c(
    			Caller_Account__c = providerAccount.Id,
    			Caller_Contact__c = provider.Id
    		));
    	}
		insert existingCallLogs;
		
		ApexPages.Standardcontroller stdCon = new ApexPages.Standardcontroller(newCallLog);
    	
    	pageCon = new EditCallLogController(stdCon);
    	pageCon.init();
    }
}