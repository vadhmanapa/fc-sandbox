public with sharing class AdditionalDetailsController_IP extends ComponentControllerBase_IP {

    public Boolean initialized {get{if (initialized == null) initialized = false; return initialized;}set;}
    public String selectedDetailRecType {get;set;}
	public AddCaseDetails__c newDetail {get;set;}
	public String addDetailsErrorMsg {get;set;}
	public Boolean addDetailsError {get;set;}
	public Integer counter {get;set;}
    public String belongsTo {get;set{
        if(String.isNotBlank(value)){
            belongsTo = value;
            if(!initialized){
                resetController();
                initialized = true;
            }
        }
    }}
    public Boolean isClaimsTeam {get;set;} // holds the value passed from controller to check if the current claim entry is for claim or other
    public List<SelectOption> getDetailsRecType(){
        List<SelectOption> options = new List<SelectOption>();
        
        for(String s: RecordTypes.availableDetailTypes.keySet()){
            if(s != 'Add Combination'){
                options.add(new SelectOption(s,s));
            }   
        }

        options.add(0, new SelectOption('','---None---'));
        return options;
    }
    
    public List<AdditionalDetailsWrapper> additionalDetailsList {get;set;}

    public AdditionalDetailsController_IP(){
        resetController();
    }

    public void resetController(){

        if(belongsTo == 'Claims') {
            selectedDetailRecType = 'Que Claim ID';
            isClaimsTeam = true;
        }else {
            selectedDetailRecType = '';
            isClaimsTeam = false;
        }

        newDetail = new AddCaseDetails__c();
        additionalDetailsList = new List<AdditionalDetailsWrapper>();
        counter = 0;
    }

    public void addDetailsToList(){

    	if(validateAddedDetail(newDetail)) {
    		// add the detail to wrapper list
    		newDetail.AdditionalDetail__c = selectedDetailRecType;
            newDetail.RecordTypeId = RecordTypes.additionalDetailTypes.get(selectedDetailRecType).getRecordTypeId();
    		AdditionalDetailsWrapper adw = new AdditionalDetailsWrapper(newDetail);
    		adw.selectedCounter = counter;
    		additionalDetailsList.add(adw);
    		selectedDetailRecType = ''; // reset
    		newDetail = new AddCaseDetails__c(); // reset
    		counter++;
    	}
    }

    public PageReference removeDetailsFromList(){
    	/*List<Integer> toDelete = new List<Integer>();
    	if(!additionalDetailsList.isEmpty()) {
    		for(Integer i = 0; i < additionalDetailsList.size(); i++) {
    			if(additionalDetailsList[i].isDelete == 'true') {
    				toDelete.add(i);
    			}
    		}
    	}

    	for(Integer x: toDelete) {
    		additionalDetailsList.remove(x);
    		if(additionalDetailsList.isEmpty()) {
    			additionalDetailsList = new List<AdditionalDetailsWrapper>();
    		}
    	}*/

    	Integer param = Integer.valueOf(ApexPages.currentPage().getParameters().get('index'));
    	for(Integer i = 0; i < additionalDetailsList.size(); i++) {
    		if(additionalDetailsList[i].selectedCounter == param) {
    			additionalDetailsList.remove(i);
    		}
    	}

    	counter--;
    	return null;
    }

    public Boolean validateAddedDetail( AddCaseDetails__c ad){
        
        addDetailsError = false;
        addDetailsErrorMsg = '';
        
        if(selectedDetailRecType == 'Authorization Number' && String.isBlank(ad.AuthorizationNumber__c)){
        	// addDetailsErrorMsg = 'PLEASE INPUT AN AUTHORIZATION NUMBER';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'PLEASE INPUT AN AUTHORIZATION NUMBER'));
        	addDetailsError = true;
            return false;
        }
        else if(selectedDetailRecType == 'HCPC Code' && ad.HCPC_Code__c == null){
            //addDetailsErrorMsg = 'PLEASE INPUT A HCPC CODE';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'PLEASE INPUT A HCPC CODE'));
            addDetailsError = true;
            return false;
        }
        else if(selectedDetailRecType == 'Order Number' && ad.Order__c == null){
            //addDetailsErrorMsg = 'PLEASE INPUT AN ORDER NUMBER';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'PLEASE INPUT AN ORDER NUMBER'));
            addDetailsError = true;
            return false;
        }
        else if(selectedDetailRecType == 'Que Claim ID' && String.isBlank(ad.QueClaimID__c)){
            //addDetailsErrorMsg = 'PLEASE INPUT A QUE CLAIM ID';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'PLEASE INPUT A QUE CLAIM ID'));
            addDetailsError = true;
            return false;
        }
        else if(selectedDetailRecType == 'Referred Provider' && (ad.ReferredProvider__c == null || String.isBlank(ad.ReferredProvider__c))){
            //addDetailsErrorMsg = 'PLEASE INPUT A PROVIDER';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'PLEASE INPUT A PROVIDER'));
            addDetailsError = true;
            return false;
        }
        else if(selectedDetailRecType == 'zPaper Reference Number' && ad.zPaperReferenceNumber__c == null){
            return false;
        }else if(additionalDetailsList.size() > 0){
           // Only when the issue is about claims, each cannot contain more than one claim
           if(isClaimsTeam){
                // then this check should happen
                for(AdditionalDetailsWrapper a : additionalDetailsList)
                {
                    if(String.isNotBlank(a.detailAdded.QueClaimID__c)){
                        //addDetailsErrorMsg = 'ONE INQUIRY CAN HAVE ONE CLAIM ONLY';
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'ONE INQUIRY CAN HAVE ONE CLAIM ONLY'));
                        addDetailsError = true;
                        return false;
                    }
                }
            }
        }

    	return true;
    }

    public List<AddCaseDetails__c> detailsToAdd(){
        List<AddCaseDetails__c> toInsert = new List<AddCaseDetails__c>();

        if(additionalDetailsList.size() > 0) {
            for(AdditionalDetailsWrapper a: additionalDetailsList) {
                AddCaseDetails__c newAd = a.detailAdded;
                toInsert.add(newAd);
            }
        }

        return toInsert;
    }

	//create a wrapper class
	public class AdditionalDetailsWrapper {
		public Integer selectedCounter {get;set;}
		public AddCaseDetails__c detailAdded {get;set;}
		public AdditionalDetailsWrapper(){

		}

		public AdditionalDetailsWrapper(AddCaseDetails__c aDetail){
			//isDelete = false;
			this.detailAdded = aDetail;
		}
	}

    public void setParentControllerValues(){

        if(additionalDetailsList.size() > 0) {
            for(AdditionalDetailsWrapper a: additionalDetailsList) {
                AddCaseDetails__c newAd = a.detailAdded;
                newAd.Case__c = pageController.caseLog.caseDetails.Id;
                pageController.caseLog.additionalCaseDetails.add(newAd);
            }

            insert pageController.caseLog.additionalCaseDetails;
        }
    }
}