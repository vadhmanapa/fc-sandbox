public with sharing class OrderStatusMetadataWrapper_IP {
    
    	/*************************************************************
         ** NOTE: STAGING FOR FUTURE RELEASE - SPRINT 12/12 - BY VK
		**************************************************************/
	
	public OrderStatusMetadataWrapper_IP() {
		
	}

	private static Set<String> allOrderStatus {
		get{
			if(allOrderStatus == null) {
                allOrderStatus = new Set<String>();
				Schema.DescribeFieldResult orderStatusPicklist = Order__c.Status__c.getDescribe();
				List<Schema.PicklistEntry> orderStatusPicklistVals = orderStatusPicklist.getPicklistValues();
				for(Schema.PicklistEntry sc: orderStatusPicklistVals) {
					// add only active values
					if(sc.isActive()) {
						allOrderStatus.add(sc.getValue());
					}
				}
			}
			return allOrderStatus;	
		} set;
	}

	// Cached Map of metadata
	@TestVisible private static  Map<String,List<Metadata>> orderStatusMapCache = new Map<String,List<Metadata>>();
    public static final String providerOtherReason = 'Needs Payor Attention - Other (Please describe the details in the notes)';
    public static final String payerOtherReason = 'Needs Provider Attention - Other (Please describe the details in the notes)';
 
    // Public getter
    public static List<Metadata> getOrderStatusMappingMDT(String entityType)
    {
        // Check the cache
        List<Metadata> orderStat_Cached = orderStatusMapCache.get(entityType);
        String queryString = 'SELECT DeveloperName,Active__c,DisplayLabel__c,OrderStatusMapped__c,Stage__c,VisibletoPayor__c,VisibletoProvider__c FROM Order_Status_Mapping__mdt WHERE Active__c = TRUE';
      
        if(entityType == 'Provider') { queryString+= ' AND VisibletoProvider__c = TRUE';}
        else if (entityType == 'Payor') { queryString+= ' AND VisibletoPayor__c = TRUE';}
        else {}
 
        // Not found, so query the metadata
        if (orderStat_Cached == null)
        {
            List<Order_Status_Mapping__mdt> oStatMapping = new List<Order_Status_Mapping__mdt>();
 			orderStat_Cached = new List<Metadata>();

            try
            {
                oStatMapping = Database.query(queryString);
            }
            catch(System.QueryException sqe)
            {
                System.debug(loggingLEVEL.ERROR, 'Metadata with this entity name not found:'+entityType);
                System.debug(loggingLEVEL.INFO, 'Query Exception ERROR' +sqe);
            }
 
            // Wrap the metadata using the inner class
            // orderStat_Cached = new List<Metadata>(oStatMapping);
            
            for(Order_Status_Mapping__mdt o: oStatMapping) {
            	Metadata m = new Metadata(o);
            	orderStat_Cached.add(m);
            	System.debug(loggingLEVEL.INFO, 'Added metadata'+o);
            }
 
            // Add it to the cache
            orderStatusMapCache.put(entityType, orderStat_Cached);
        }
 
        // Return the wrapped metadata
        return orderStat_Cached;
    }

    public static Map<String, String> getProviderNAQ(){

    	Map<String, String> providerNAQReasons = new Map<String, String>();

    	for(Metadata mdt: getOrderStatusMappingMDT('Provider')) {
    		// This is to check if the Metadata value is mapped to order status
    		
    		if(allOrderStatus.contains(mdt.orderStatusMapped)) {
    			providerNAQReasons.put(mdt.displayLabel, mdt.orderStatusMapped);
    		}else {
    			System.debug(loggingLevel.INFO, 'METADATA NOT AVAILABLE IN ORDER STATUS:'+mdt.displayLabel);
    		}
    	}

    	return providerNAQReasons;
    }

    public static Map<String, String> getPayorNAQ(){

    	Map<String, String> payorNAQReasons = new Map<String, String>();

    	for(Metadata mdt: getOrderStatusMappingMDT('Payor')) {
    		// This is to check if the Metadata value is mapped to order status
    		
    		if(allOrderStatus.contains(mdt.orderStatusMapped)) {
    			payorNAQReasons.put(mdt.displayLabel, mdt.orderStatusMapped);
    		}else {
    			System.debug(loggingLevel.INFO, 'METADATA NOT AVAILABLE IN ORDER STATUS:'+mdt.displayLabel);
    		}
    	}

    	return payorNAQReasons;
    }

    public static Map<String, String> getAllNAQ(){

    	Map<String, String> reasons = new Map<String, String>();

    	for(Metadata mdt: getOrderStatusMappingMDT('All')) {
    		// This is to check if the Metadata value is mapped to order status
    		
    		if(allOrderStatus.contains(mdt.orderStatusMapped)) {
    			reasons.put(mdt.displayLabel, mdt.orderStatusMapped);
    		}else {
    			System.debug(loggingLevel.INFO, 'METADATA NOT AVAILABLE IN ORDER STATUS:'+mdt.displayLabel);
    		}
    	}

    	return reasons;
    }

    public static String statusToDisplay(String instanceStatus){
    	String status;
    	for(Metadata mdt: getOrderStatusMappingMDT('All')) {
    		if(instanceStatus == mdt.orderStatusMapped) {
    			status = mdt.oStage+ ' - '+mdt.displayLabel;
    		}
    	}
    	return status;
    }

    public static Set<String> allActiveOrderStatus(){
    	return allOrderStatus;
    }
 
    // Inner class to wrap the metadata
    public class Metadata
    {
        // Properties represent the Fields in the Metadata
        public Boolean isActive {get; set;}
        public String displayLabel {get; set;}
        public String orderStatusMapped {get;set;}
        public String oStage {get;set;}
        public Boolean isVisibletoPayor {get;set;}
        public Boolean isVisibletoProvider {get;set;}
        public String developerName {get;set;}
        public Id metaId {get;set;}
 
        // Constructor used when creating test values
        public Metadata()
        {
        }
 
        // Construct from a concrete metadata instance
        public Metadata(Order_Status_Mapping__mdt metadata)
        {
            this.isActive = metadata.Active__c;
            this.displayLabel = (String) metadata.DisplayLabel__c;
            this.orderStatusMapped = (String) metadata.OrderStatusMapped__c;
            this.oStage = (String) metadata.Stage__c;
            this.isVisibletoPayor = metadata.VisibletoPayor__c;
            this.isVisibletoProvider = metadata.VisibletoProvider__c;
            this.developerName = (String) metadata.DeveloperName;
            this.metaId = metadata.Id;
        }
    }
}