public with sharing class NewCaseOverrideController {

	public Case newCase {get;set;}
	public Contact caseContact {get;set;}
	
	public String CASE_QUE_RECORD_TYPE_ID {get; set;}
	
	public String newCaseType {
		get{
            if(RecordTypes.recordTypesById.containsKey(newCase.RecordTypeId)) {
				return RecordTypes.recordTypesById.get(newCase.RecordTypeId).Name;
            }
            return '';
		}
	}

	public NewCaseOverrideController(ApexPages.Standardcontroller stdCon) {
		
		//Set Que Record Type Id for Case
		CASE_QUE_RECORD_TYPE_ID = '012300000019Su0AAE';
		
		newCase = (Case)stdCon.getRecord();
		User u = [Select Id, Name,Que_Category__c from User where Id=: UserInfo.getUserId() LIMIT 1];
		newCase.OwnerId = UserInfo.getUserId();
		newCase.RecordTypeId = RecordTypes.defaultCaseId;
		newCase.Priority = 'Standard';
        if(u.Que_Category__c != null){
            newCase.Que_Category__c = u.Que_Category__c;
        }
        newCase.Status = 'New';
		
		if(ApexPages.currentPage().getParameters().containsKey('contactId')) {
			Id contactId = ApexPages.currentPage().getParameters().get('contactId');
			
			if(contactId != null) {
				newCase.ContactId = contactId;
				
				updateCaseContact(); 
			}
		}
	}

	public void updateCaseContact() {
		
		if(newCase.ContactId == null) {
			return;
		}
		
		caseContact = [
			SELECT AccountId,Phone,MobilePhone,Email
			FROM Contact
			WHERE Id = :newCase.ContactId
		];
		
		CaseUtils.CopyFromContact(caseContact, newCase);
		
		//Report error
		if (String.isBlank(newCase.SuppliedPhone) || 
			String.isBlank(newCase.SuppliedEmail))
		{
			PageUtils.addWarning('The Contact record is missing information (Phone, Email) that' +
				' is vital for recontact and notifications. Please enter the missing information manually.');
		}
	}

	public PageReference save() {
		
		if(newCase.ContactId == null && newCase.RecordTypeId != CASE_QUE_RECORD_TYPE_ID) {
			newCase.ContactId.addError('Please enter a Contact');
			return null;
		}
		
		if(newCase.AccountId == null && newCase.RecordTypeId == CASE_QUE_RECORD_TYPE_ID) {
			newCase.AccountId.addError('Please enter an Account');
			return null;
		}
        
        if(newCase.SuppliedEmail == null && newCase.RecordTypeId == CASE_QUE_RECORD_TYPE_ID) {
			newCase.AccountId.addError('Please enter an Account');
			return null;
		}
		
		if(String.isBlank(newCase.Subject) && newCase.RecordTypeId != CASE_QUE_RECORD_TYPE_ID) {
			newCase.Subject.addError('Please enter a Subject');
			return null;
		}
		
		Database.DMLOptions dmo = new Database.DMLOptions();
		/*dmo.emailHeader.triggerUserEmail = true;
		dmo.assignmentRuleHeader.useDefaultRule = true;*/
		
		System.debug(LoggingLevel.INFO,'Supplied Email: ' + newCase.SuppliedEmail);
		
		if(String.isNotBlank(newCase.SuppliedEmail)) {
			dmo.emailHeader.triggerAutoResponseEmail = true;
			dmo.emailHeader.triggerOtherEmail = true;
		}
		
		// FOR USE WITH INACTIVE ASSIGNMENT RULES
		try {
			AssignmentRule caseAssignmentRule = [
				SELECT Id
				FROM AssignmentRule
				WHERE Name = 'Email Assignment'
			];

			//dmo.assignmentRuleHeader.useDefaultRule = false;
			dmo.assignmentRuleHeader.assignmentRuleId = caseAssignmentRule.Id;
			
		} catch (Exception e) {
			System.debug(LoggingLevel.INFO,e.getMessage());
			dmo.assignmentRuleHeader.useDefaultRule = true;
		}
		
		newCase.setOptions(dmo);
		
		try {
			insert newCase;
		} catch(Exception e) {
			return null;
		}
		
		return new PageReference('/' + newCase.Id);
	}
}