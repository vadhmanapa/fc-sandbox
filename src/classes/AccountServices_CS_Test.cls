@isTest
private class AccountServices_CS_Test {
	public static List<Contact> userList;
	public static Account provider;
	public static AccountServices_CS cont;
	
    static testMethod void Constructor_Test() {
    	init();
     	
    	Account a = cont.cAccount;
    	system.assertEquals(provider.Id, a.Id);
    }
    
    static testMethod void Constructor_IdException_Test() {
    	init();
    	
    	String badId = '0000';
    	try{
    		cont = new AccountServices_CS(badId);
    	}
    	catch (ApplicationException e) {}
    }
    
    static testMethod void getProviders() {
    	init();
    	
    	ApexPages.Standardsetcontroller setCon = AccountServices_CS.getProviders();
    	System.assertEquals(1, setCon.getResultSize());
    	System.assertEquals(provider.Id,setCon.getRecords()[0].Id);
    }
	
	static testMethod void getPreferredProviders() {
		init();
		
		ApexPages.Standardsetcontroller setCon = AccountServices_CS.getPreferredProviders();
    	System.assertEquals(0, setCon.getResultSize());
		
		provider.Provider_Rank__c = 'Preferred';
		update provider;
    	
    	setCon = AccountServices_CS.getPreferredProviders();
    	System.assertEquals(1, setCon.getResultSize());
    	System.assertEquals(provider.Id,setCon.getRecords()[0].Id);
	}
	
	static testMethod void searchProviders() {
		init();
		
		ApexPages.Standardsetcontroller setCon = AccountServices_CS.searchProviders('test');
		System.assertEquals(1, setCon.getResultSize());
    	System.assertEquals(provider.Id,setCon.getRecords()[0].Id);
	}
	
	static testMethod void searchNoResults() {
		init();
		
		ApexPages.Standardsetcontroller setCon = AccountServices_CS.searchProviders('q8r3hoy');
		System.assertEquals(0, setCon.getResultSize());
	}
	
    public static void init(){
    	List<Account> initAccts = TestDataFactory_CS.generateProviders('Test Provider', 1);
    	insert initAccts;
    	provider = initAccts[0];
    	
    	userList = TestDataFactory_CS.generateProviderContacts('Admin', initAccts, 5);
    	insert userList;
    	
    	cont = new AccountServices_CS(userList[0].Id);
    }
}