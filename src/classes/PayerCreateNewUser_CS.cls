/*
* @description Controller for the PayerCreateNewUser_CS page in the payer portal.
* @author Cloud Software LLC.
*	Revision History:
*		07/15/2016 - karnold - Added header. Added functionality to the page to allow 
*			users to select the content role of the user they are creating.
*		07/21/2016 - karnold - IMOR-52 - Added Health_Plan_Visible__c = True condition to Content Role select list.
*		12/06/2016 - vadhmanapa - Sprint 12/5 - Added Status__c = Active and defaulting user record type to Payor on payer page.
*
*/
public without sharing class PayerCreateNewUser_CS extends PayerPortalServices_CS {

	private final String pageAccountType = 'Payer';
	UserServices_CS userServices {get;set;}

	/******* Error Messages *******/
	private final String nonUniqueUsernameError = 'Username already exists';
	private final String missingUsernameError = 'Please enter a unique username';
	public String usernameErrorMsg {get;set;}

	/********* Page Interface Params *********/
	public Contact newUser {get;set;}
	public String username {get;set;}
	public List<SelectOption> contentRoleSelectOptions {get;set;}
	public Id selectedContentRoleId {get;set;}
	
	/********* Constructor/Init *********/
	public PayerCreateNewUser_CS(){}
	
	// Default page action, add additional page action functionality here
	public PageReference init(){
		
		///////////////////////////////////
		// Authentication and Permission
		///////////////////////////////////
		
		if(!isSecure && !Test.isRunningTest()){
			return DoLogout();
		}
				
		if(!authenticated){
			return DoLogout();
		}

		if(portalUser.AccountType != pageAccountType){
			return homepage;
		}
			
		if(!portalUser.role.Create_Users__c){
			return homepage;
		}
		
		///////////////////////////////////
		// Additional init
		///////////////////////////////////
		newUser = new Contact();
		userServices = new UserServices_CS(portalUser.instance.Id);
		
		// Create picklist of roles to display on page
		contentRoleSelectOptions = new List<SelectOption>();

		// This is the default value for the picklist. 
		// We will default this to the the value of the current User's role later.
		contentRoleSelectOptions.add(new SelectOption('', ''));

        for (Role__c role : [SELECT Name FROM Role__c WHERE Health_Plan_Visible__c = True LIMIT 10000]) {
			// If the role is the same as the current user's role, set the role to be the defaul value of the picklist.
			if (role.Id == portalUser.instance.Content_Role__c) {
				contentRoleSelectOptions[0] = new SelectOption(role.Id, role.Name);
			} else {
				contentRoleSelectOptions.add(new SelectOption(role.Id, role.Name));
			}
        }

		return null;
		
	}//End Init
	
	/********* Page Buttons/Actions *********/
	public PageReference createNewUser(){
		
		//Check for username
		if(String.isBlank(username)){
			usernameErrorMsg = missingUsernameError;
			System.debug('createNewUser: Error, username not entered');
			return null;
		}
		
		//Check for unique username
		if(UserServices_CS.checkUniqueUsername(username)){			
			newUser.Username__c = username;
			System.debug('createNewUser: Username ' + username + ' is unique.');
		}
		else{
			usernameErrorMsg = nonUniqueUsernameError;
			System.debug('createNewUser: Error, username ' + username + ' is not unique.');
			return null;
		}
		
		//system.debug('\nNew user First Name:  ' + newUser.FirstName);
		
		///////////////////////////////////
		// Add required user details here
		///////////////////////////////////
		
		// Assign the new user to this account
		newUser.Entity__c = portalUser.instance.Entity__c;	
		
		// Set the new user's role to the one that was selected in the picklist
		newUser.Content_Role__c = selectedContentRoleId;
        
        // Assign to Payer record type
        newUser.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Payor').getRecordTypeId(); // Added by VK - 12/06/2016
		
		//system.debug('\nNew user:  ' + newUser);

		// Make the user active 
		// TODO: Should this be the case?
		newUser.Active__c = true;
        newUser.Status__c = 'Active'; // Added by VK - 12/06/2016
		
		
		// TODO: LEGACY DATA
		newUser.AccountId = portalUser.instance.AccountId;
		newUser.Health_Plan_User_Id__c = newUser.Username__c;

		// Attempt to insert
		Database.Saveresult newUserResult;	
		system.debug('\nNew User Result:' + newUserResult);

		try{
			newUserResult = Database.insert(newUser);
									
			if(newUserResult.isSuccess()){
				system.debug('\nNew User Result:' + newUserResult);

				// Give the new user a password and salt
				UserServices_CS.resetUserPassword(newUser);

				update newUser;

				// Reset the details for the next new user
				newUser = new Contact();
				
				system.debug('Successfully created new user ' + newUser.Username__c);
			}
			else{
				system.debug('Failed to create new user ' + newUser.Username__c);
				return null;
			}
		}
		catch(Exception e){
			System.debug('Exception: Failed to create new user ' + newUser.Username__c + ' : ' + e.getMessage());
		}
		
		return Page.PayerUsersSummary_CS;
	}
}