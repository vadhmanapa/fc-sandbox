@isTest
public class CountyCountControllerTest_IP {
	
	 static testMethod void TestCountyController() {
		
		PageReference pageRef = Page.WellcareCountyCoverage;
        Test.setCurrentPageReference(pageRef);

        Account testProvider = new Account(Name = 'Test Claim Provider', Type_Of_Provider__c = 'DME');
    	insert testProvider;

        Campaign testIl = new Campaign(Name = 'Illinois Wellcare', IsActive = true);
        insert testIl;

        Opportunity testOpp = new Opportunity(Name = 'Test Opp', AccountId = testProvider.Id, CloseDate = System.today(), StageName = 'Received "Completed" App', 
        									  CampaignId = testIl.Id);
        insert testOpp;

        List<US_State_County__c> testCount = new List<US_State_County__c>();

        String[] ilCounties = new String[]{'Adams',
        								   'Alexander',
        								   'Bond',
        								   'Boone'}; 
        for(String s : ilCounties){

        	testCount.add(new US_State_County__c(State__c = 'Illinois', Name = s));

        }

        insert testCount;

        List<County_Item__c> ilOppCount = new List<County_Item__c>();

        for( US_State_County__c u : [Select Id, Name from US_State_County__c where State__c = 'Illinois']){

        	ilOppCount.add(new County_Item__c(Opportunity__c = testOpp.Id, US_State_County__c = u.Id));
        }

        insert ilOppCount;

        CountyCountController_IP testIlController = new CountyCountController_IP();
        String testName = 'test IL';
        Integer testSize = 100;
        Integer totalCount = 102;
        CountyCountController_IP.PieDataIllinois il = new CountyCountController_IP.PieDataIllinois(testName, testSize, totalCount);
        List<CountyCountController_IP.PieDataIllinois> testIlList = testIlController.getIllinoisData();

        Campaign testCa = new Campaign(Name = 'California Wellcare', IsActive = true);
        insert testCa;

        Opportunity testOpp2 = new Opportunity(Name = 'Test Opp 2', AccountId = testProvider.Id, CloseDate = System.today(), StageName = 'Received "Completed" App', 
        									  CampaignId = testCa.Id);
        insert testOpp2;

        List<US_State_County__c> testCount2 = new List<US_State_County__c>();

        String[] caCounties = new String[]{'Alpine',
        								   'Amador',
        								   'Butte',
        								   'Calaveras'}; 
        for(String s : caCounties){

        	testCount2.add(new US_State_County__c(State__c = 'California', Name = s));

        }

        insert testCount2;

        List<County_Item__c> caOppCount = new List<County_Item__c>();

        for( US_State_County__c u : [Select Id, Name from US_State_County__c where State__c = 'California']){

        	caOppCount.add(new County_Item__c(Opportunity__c = testOpp2.Id, US_State_County__c = u.Id));
        }

        insert caOppCount;

        CountyCountController_IP testCaController = new CountyCountController_IP();
        String testNameCa = 'test CA';
        Integer testSizeCa = 100;
        Integer totalCountCa = 112;
        CountyCountController_IP.PieDataCal ca = new CountyCountController_IP.PieDataCal(testNameCa, testSizeCa);
        List<CountyCountController_IP.PieDataCal> testCaList = testCaController.getCalData();

        Campaign testTx = new Campaign(Name = 'Texas Wellcare', IsActive = true);
        insert testTx;

        Opportunity testOpp3 = new Opportunity(Name = 'Test Opp 3', AccountId = testProvider.Id, CloseDate = System.today(), StageName = 'Received "Completed" App', 
        									  CampaignId = testTx.Id);
        insert testOpp3;

        List<US_State_County__c> testCount3 = new List<US_State_County__c>();

        String[] txCounties = new String[]{'Alpine',
        								   'Amador',
        								   'Butte',
        								   'Calaveras'}; 
        for(String s : txCounties){

        	testCount3.add(new US_State_County__c(State__c = 'Texas', Name = s));

        }

        insert testCount3;

        List<County_Item__c> txOppCount = new List<County_Item__c>();

        for( US_State_County__c u : [Select Id, Name from US_State_County__c where State__c = 'Texas']){

        	txOppCount.add(new County_Item__c(Opportunity__c = testOpp3.Id, US_State_County__c = u.Id));
        }

        insert txOppCount;

        CountyCountController_IP testTxController = new CountyCountController_IP();
        String testNameTx = 'test TX';
        Integer testSizeTx = 100;
        Integer totalCountTx = 254;
        CountyCountController_IP.PieDataTex tx = new CountyCountController_IP.PieDataTex(testNameTx, testSizeTx);
        List<CountyCountController_IP.PieDataTex> testTxList = testTxController.getTexData();

        Campaign testHw = new Campaign(Name = 'Hawaii Wellcare', IsActive = true);
        insert testHw;

        Opportunity testOpp4 = new Opportunity(Name = 'Test Opp 4', AccountId = testProvider.Id, CloseDate = System.today(), StageName = 'Received "Completed" App', 
        									  CampaignId = testHw.Id);
        insert testOpp4;

        List<US_State_County__c> testCount4 = new List<US_State_County__c>();

        String[] hwCounties = new String[]{'Hawaii',
        								   'Maui',
        								   'Kuai',
        								   'Honululu'}; 
        for(String s : hwCounties){

        	testCount4.add(new US_State_County__c(State__c = 'Hawaii', Name = s));

        }

        insert testCount4;

        List<County_Item__c> hwOppCount = new List<County_Item__c>();

        for( US_State_County__c u : [Select Id, Name from US_State_County__c where State__c = 'Hawaii']){

        	hwOppCount.add(new County_Item__c(Opportunity__c = testOpp4.Id, US_State_County__c = u.Id));
        }

        insert hwOppCount;

        CountyCountController_IP testHwController = new CountyCountController_IP();
        String testNameHw = 'test HW';
        Integer testSizeHw = 2;
        Integer totalCountHw = 4;
        CountyCountController_IP.PieDataHaw hw = new CountyCountController_IP.PieDataHaw(testNameHw, testSizeHw);
        List<CountyCountController_IP.PieDataHaw> testHwList = testHwController.getHawData();
	}
	
	
	
}