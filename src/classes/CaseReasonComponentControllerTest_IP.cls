@isTest
private class CaseReasonComponentControllerTest_IP
{
	public static TestCaseDataFactory_IP generateNewData;
	private static CaseReasonComponentController_IP caseReasonCon;
	private static User testUser;

	@isTest static void checkReasonAndDetailsPicklistWithNoOutput()
	{
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		generateNewData.generateReasonsAndActions();
		testUser = TestCaseDataFactory_IP.csUser;
		List<SelectOption> allReasons = new List<SelectOption>();
		List<SelectOption> allReasonDetails = new List<SelectOption>();

		System.runAs(testUser){
			Test.startTest();
			caseReasonCon = new CaseReasonComponentController_IP();
			allReasons = caseReasonCon.getReasons();
			allReasonDetails = caseReasonCon.getReasonDetails();
			Test.stopTest();
		}

		System.assertEquals(0, allReasons.size(), 'No reasons were queried without required inputs');
		System.assertEquals(0, allReasonDetails.size(), 'No reason details were queried without required inputs');
	}

	@isTest static void checkReasonPicklistWithOutput()
	{
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		generateNewData.generateReasonsAndActions();
		testUser = TestCaseDataFactory_IP.csUser;
		List<SelectOption> allReasons = new List<SelectOption>();

		System.runAs(testUser){
			caseReasonCon = new CaseReasonComponentController_IP();
			caseReasonCon.recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Customer Service Call').getRecordTypeId();
			caseReasonCon.selectedCallerType = 'Patient';

			Test.startTest();
			allReasons = caseReasonCon.getReasons();
			Test.stopTest();
		}

		System.assertNotEquals(0, allReasons.size(), 'Reasons weere queried based on inputs');
	}

	@isTest static void checkReasonDetailsPicklistWithOutput()
	{
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		generateNewData.generateReasonsAndActions();
		testUser = TestCaseDataFactory_IP.csUser;
		List<SelectOption> allReasonDetails = new List<SelectOption>();

		System.runAs(testUser){
			caseReasonCon = new CaseReasonComponentController_IP();
			caseReasonCon.recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Customer Service Call').getRecordTypeId();
			caseReasonCon.selectedCallerType = 'Patient';
			caseReasonCon.reasonSelected = (String) TestCaseDataFactory_IP.reasons[0].Id;

			Test.startTest();
			allReasonDetails = caseReasonCon.getReasonDetails();
			Test.stopTest();
		}

		System.assertNotEquals(0, allReasonDetails.size(), 'Reasons weere queried based on inputs');
	}

	@isTest static void testAutoFillFromCase()
	{
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		generateNewData.generateReasonsAndActions();
		testUser = TestCaseDataFactory_IP.csUser;
		Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Payor0' LIMIT 1];
		Account prAcc = [SELECT Id, Name FROM Account WHERE Name = 'Provider0' LIMIT 1];
		Contact c = [SELECT Id, Name, Birthdate,FirstName, LastName, Account.Name, AccountId, RecordTypeId, Account.Status__c, Phone, Email, Health_Plan_ID__c 
					FROM Contact WHERE Health_Plan_ID__c= '123450' AND AccountId =: acc.Id LIMIT 1];

		Case cas = new Case();
		cas.AccountId = acc.Id;
		cas.ContactId = c.Id;
		cas.CaseReasonId__c = TestCaseDataFactory_IP.reasons[0].Id;
		cas.CaseReasonDetailId__c = TestCaseDataFactory_IP.reasonDetail[0].Id;
		insert cas;

		System.runAs(testUser){
			caseReasonCon = new CaseReasonComponentController_IP();
			caseReasonCon.recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Customer Service Call').getRecordTypeId();
			Test.startTest();
			caseReasonCon.thisCase  = cas;
			//caseReasonCon.caseReasonProperties();
			Test.stopTest();
		}

		System.assertNotEquals('', caseReasonCon.reasonSelected, 'The reason was filled from parent case');
		System.assertNotEquals('', caseReasonCon.reasonDetailSelected, 'The reason was filled from parent case');
		//System.assertNotEquals(null, caseReasonCon.crDetail, 'The reason details were queried');
	}

	@isTest static void testAutoFillFromCaseWithEmptyReasons()
	{
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		generateNewData.generateReasonsAndActions();
		testUser = TestCaseDataFactory_IP.csUser;
		Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Payor0' LIMIT 1];
		Account prAcc = [SELECT Id, Name FROM Account WHERE Name = 'Provider0' LIMIT 1];
		Contact c = [SELECT Id, Name, Birthdate,FirstName, LastName, Account.Name, AccountId, RecordTypeId, Account.Status__c, Phone, Email, Health_Plan_ID__c 
					FROM Contact WHERE Health_Plan_ID__c= '123450' AND AccountId =: acc.Id LIMIT 1];

		Case cas = new Case();
		cas.AccountId = acc.Id;
		cas.ContactId = c.Id;
		cas.CaseReasonId__c = '';
		cas.CaseReasonDetailId__c = '';
		insert cas;

		System.runAs(testUser){
			caseReasonCon = new CaseReasonComponentController_IP();
			caseReasonCon.recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Customer Service Call').getRecordTypeId();
			Test.startTest();
			caseReasonCon.thisCase  = cas;
			//caseReasonCon.caseReasonProperties();
			Test.stopTest();
		}

		System.assertEquals('', caseReasonCon.reasonSelected, 'The reason was filled from parent case');
		System.assertEquals('', caseReasonCon.reasonDetailSelected, 'The reason was filled from parent case');
		//System.assertNotEquals(null, caseReasonCon.crDetail, 'The reason details were queried');
	}

	@isTest static void testResetValidations()
	{
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		generateNewData.generateReasonsAndActions();
		testUser = TestCaseDataFactory_IP.csUser;
		Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Payor0' LIMIT 1];
		Account prAcc = [SELECT Id, Name FROM Account WHERE Name = 'Provider0' LIMIT 1];
		Contact c = [SELECT Id, Name, Birthdate,FirstName, LastName, Account.Name, AccountId, RecordTypeId, Account.Status__c, Phone, Email, Health_Plan_ID__c 
					FROM Contact WHERE Health_Plan_ID__c= '123450' AND AccountId =: acc.Id LIMIT 1];

		Case cas = new Case();
		cas.AccountId = acc.Id;
		cas.ContactId = c.Id;
		cas.CaseReasonId__c = TestCaseDataFactory_IP.reasons[0].Id;
		cas.CaseReasonDetailId__c = TestCaseDataFactory_IP.reasonDetail[0].Id;
		insert cas;

		System.runAs(testUser){
			caseReasonCon = new CaseReasonComponentController_IP();
			caseReasonCon.recordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Customer Service Call').getRecordTypeId();
			Test.startTest();
			caseReasonCon.thisCase  = cas;
			caseReasonCon.resetReasonDetailValidation();
			Test.stopTest();
		}
		
		System.assertNotEquals('', caseReasonCon.reasonSelected, 'The reason was filled from parent case');
		System.assertEquals('', caseReasonCon.reasonDetailSelected, 'The reason filled from parent case was reset');
	}
}