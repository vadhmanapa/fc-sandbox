public without sharing class TestServices_CS {
	public static Boolean login(Contact user){
		if(user.Id != null){
   			String hashedUserId = PasswordServices_CS.hash(user.Id,user.Salt__c);
	   		String hashedSessionid = PasswordServices_CS.hash(PortalServices_CS.sessionId,user.Salt__c);
	   		
	   		Cookie userIdCookie = new Cookie('userid',hashedUserId, null, 3600, PortalServices_CS.isSecure);
			Cookie sessionCookie = new Cookie('sessionid', hashedSessionid, null, 3600, PortalServices_CS.isSecure);
			
			ApexPages.currentPage().setCookies(new Cookie[]{userIdCookie,sessionCookie});
			
			return true;
   		}
   		else{
   			System.debug('TestServices.Login Error: No user context.');
   		}
   		
   		return false;
	}
}