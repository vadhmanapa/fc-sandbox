public with sharing class ClaimsAuditController_IP {

	public ClaimsAuditController_IP() {
	}
    
    public static void calculateAuditScore(List<Claim_Work_Audit__c> newTrigger){
        
        for(Claim_Work_Audit__c cwa : newTrigger){
            
            Double totalScore = 0;
            // Step 1: Check first yes or no
            if(cwa.Was_the_correct_issue_identified__c == 'Yes'){
                totalScore = 25;
            } else{
                cwa.Score__c = totalScore;
                continue;
            }
            
            //Step 2
            
            if(cwa.Did_the_CRS_have_the_correct_resolution__c == 'Yes'){
                totalScore+= 25;
            } else{
                cwa.Score__c = totalScore;
                continue;
            }
            
            //Step 3
            
            if(cwa.Were_correct_actions_taken__c == 'Yes'){
                totalScore+= 25;
            }else{
                cwa.Score__c = totalScore;
                continue;
            }
            
            //Step 4
            
            if(cwa.Were_notes_entered__c == 'Yes'){
                totalScore+= 25;
            }else{
                cwa.Score__c = totalScore;
                continue;
            }
            
            cwa.Score__c = totalScore;
        }
    }
}