/**
 *  @Description Batch to set Miscellaneous DME line item Quantity to null
 *  @Author Cloud Software LLC, Kelly Sharp
 *  Revision History: 
 *		12/09/2015 - Created
 *		12/11/2015 - karnold - reformatted header, removed extra lines, change query to build from a string.
 */
global class MiscellaneousDMEQuantityDelete implements Database.Batchable<SObject> {

	global MiscellaneousDMEQuantityDelete() {}

	global Database.QueryLocator start(Database.BatchableContext context) {
		//get list of DME line items
		String query = 'SELECT '+
				'Quantity__c '+
			'FROM DME_Line_Item__c '+
			'WHERE DME__r.Miscellaneous__c = true';

		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext context, List<DME_Line_Item__c> scope) {
		
		// loop through all the DME line items and set the quantity to null
		for (DME_Line_Item__c li : scope) {
			li.Quantity__c = null;
		}
		update scope;
	}

	global void finish(Database.BatchableContext context) {
		
	}
}