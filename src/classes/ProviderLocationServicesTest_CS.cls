/**
 *  @Description Test class for ProviderLocationServices_CS
 *  @Author Cloud Software LLC
 *  Revision History: 
 *		01/11/2016 - karnold - Formatted code. Added header.
 *			Changed providerLocationServicesTest to use a void method call.
 *			Removed commented code.
 *
 */
@isTest
public class ProviderLocationServicesTest_CS {
	public static Set<Id> providerLocationIdSet;
	public static List<Provider_Location__c> providerLocationList;
	public static List<Account> providerList;

	public static void init() {

		providerList = TestDataFactory_CS.generateProviders('test', 1);
		insert providerList;

		providerLocationList = TestDataFactory_CS.generateProviderLocations(providerList.get(0).Id, 2);

		providerLocationList.get(0).Location_Street__c = '111 S Test Street';
		providerLocationList.get(0).Location_City__c = 'Test';
		providerLocationList.get(0).Location_State__c = 'TN';
		providerLocationList.get(0).Location_Zip_Code__c = '22222';

		providerLocationList.get(1).Location_Street__c = '789 N Abc Rd';
		providerLocationList.get(1).Location_State__c = 'MN';

		insert providerLocationList;
	}

	static testMethod void providerLocationServicesTest() {
		init();

		List<Provider_Location__c> geocodedProviderLocationList;

		System.Test.startTest();

		ProviderLocationServices_CS.geocodeProviderLocations(providerLocationList);

		System.Test.stopTest();

		System.debug('Provider Location Services Test:' + providerLocationList);

		System.assertEquals(1, providerLocationList.get(0).MALatitude__c);
		System.assertEquals(- 1, providerLocationList.get(0).MALongitude__c);

		System.assertEquals(40, providerLocationList.get(1).MALatitude__c);
		System.assertEquals(- 40, providerLocationList.get(1).MALongitude__c);
	}
}