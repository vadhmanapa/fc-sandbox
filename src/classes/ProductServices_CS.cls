/**
 *  @Description A services class to run verification on search queries and query DME objects.
 *  @Author Cloud Software LLC
 *  Revision History:
 *		12/23/2015 - karnold - Added header, removed product category from retrieveDMEProducts to deprecate object
 *		12/23/2015 - karnold - Removed references to DME_Category__c.
 *		01/12/2016 - karnold - Formatted code. Removed commented code.
 *		06/30/2016 - karnold - IMOR-40 - Added to the regex matching expression in retrieveDMEProducts
 *			to allow for lowercase entry of product codes.
 *		09/13/2016 - jmiller - IMOR-93 - Added HCPC Authorization fields to DME_Line_Item query in retrieveDLIsForOrder
 *		11/28/2016 - vadhmanapa - Sprint 11/28 - Removed OrCondition for typeB search which prevented the search of keywords
 */

public without sharing class ProductServices_CS {

	private static final Set<String> productFields = new Set<String> {
		'Id',
		'Active__c',
		'Description__c',
		'Medicare_Limits__c',
		'Keywords__c',
		'Max_Delivery_Time__c',
		'Name',
		'Setup_Time__c',
		'Volume__c',
		'Miscellaneous__c',
		'Multiple_Line_Items__c'
	};

	public static SoqlBuilder getBaseQuery() {

		SoqlBuilder query = new SoqlBuilder()
			.fromx('DME__c')
			.wherex(new FieldCondition('Active__c').equals(true)) // Add Active__c = true to query
			.limitx(10000);

		for (String f : productFields) {
			query.selectx(f);
		}

		return query;
	}

	public static SoqlBuilder attachToBaseQuery(NestableCondition conditions) {
		// Add Active__c = true to query
		conditions.add(new FieldCondition('Active__c').equals(true));

		SoqlBuilder query = new SoqlBuilder()
			.selectx(productFields)
			.fromx('DME__c')
			.wherex(conditions)
			.limitx(10000);

		return query;
	}

	/* Public Static Methods */

	/**
	 * @description Validates a search string against the two patterns and performs the query for related objects:
	 *		TypeA: A00 or a00 / ^[a-zA-Z]{1}[0-9]{2}$
	 *		TypeB: AA or aa / ^[a-zA-Z]{2}$
	 */
	public static List<DME__c> retrieveDMEProducts(String searchString) {

		Pattern searchTypeA = Pattern.compile('^[a-zA-Z]{1}[0-9]{2}[A-Z0-9]*$');
		Pattern searchTypeB = Pattern.compile('^[a-zA-Z]{2}[A-Z0-9]*$');

		Matcher typeAMatch = searchTypeA.matcher(searchString);
		Matcher typeBMatch = searchTypeB.matcher(searchString);

		SoqlBuilder query;
		NestableCondition searchConditions = new AndCondition();

		if (String.isNotBlank(searchString)) {

			//Check if the string matches the patterns, search by product code if so, otherwise search by description
			// remove OrCondition for typeB search which prevented the search of keywords - 11/28/2016
			if (typeAMatch.matches()) {
				System.debug('searchProducts: searchString "' + searchString + '" matches criteria.');

				searchConditions.add(new FieldCondition('Name').likex(searchString + '%'));
			} else {
				System.debug('searchProducts: searchString "' + searchString + '" does not match criteria.');
				searchConditions.add(new FieldCondition('Description__c').likex('%' + searchString + '%'));
			}
		}

		query = attachToBaseQuery(searchConditions);
		System.debug('retrieveDMEProducts() query: ' + query.toSoql());

		return Database.query(query.toSoql());
	}

	public static List<DME_Line_Item__c> retrieveDLIsForOrder(Order__c o) {

		if (o != null) {
			List<DME_Line_Item__c> dlis = [
				Select
					Id,
					Name,
					Order__c,
					Quantity__c,
					RR_NU__c,
					Deliver_By__c,
					Delivery_Window_Begin__c,
					Delivery_Window_End__c,
					DME__c,
					DME__r.Name,
					DME__r.Description__c,
					DME__r.Medicare_Limits__c,
					DME__r.Miscellaneous__c,
					Product_Code__c,
					Product_Name__c,
					Authorization_Number__c,
					Recurring_Frequency__c,
					Authorization_Start_Date__c,
					Authorization_End_Date__c
				From DME_Line_Item__c
				Where Order__c = :o.Id
				Order By DME__r.Name asc
			];

			return dlis;
		} else {
			return new List<DME_Line_Item__c> ();
		}

	}

	public static FieldCondition filterByProductCode(String productCode) {
		return new FieldCondition('name').likex(productCode + '%');
	}
}