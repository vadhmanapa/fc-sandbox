@isTest
private class CreateCATFromDocuSignTest_IP {
	
	@isTest static void testDocuSignToCAT() {
		
		//create user

		Profile p = [Select id from Profile where Name ='Credentialing Team'];
		RecordType rt = [Select Id, Name from RecordType where sObjectType = 'Account' and name = 'Integra Provider'] ;
		User u = new User(LastName='User', 
						  Alias='use',
						  Email='testuser@test.com',
						  Username='testingtriggeruser@testingcenter.com',
						  CommunityNickname='testing',
						  EmailEncodingKey='UTF-8',
						  LanguageLocaleKey='en_US',
						  LocaleSidKey='en_US',
						  ProfileId = p.Id,
						  TimeZoneSidKey='America/New_York');
		insert u;

		Round_Robin__c rr = new Round_Robin__c(User__c = u.Id,
											Function__c = 'Credentialing CATs',
											Active__c = True);
		insert rr;

		Cred_Trigger_CS__c CTCS = new Cred_Trigger_CS__c();
		CTCS.Active__c = True;
		insert CTCS;

		System.runAs(u){

			// create a test account

			Account testAcc = new Account(Name='Test Account', 
						        Type_Of_Provider__c = 'DME',
						        RecordType = rt);
			insert testAcc;
			
			System.assertNotEquals(null, testAcc.Id, 'Account Created');
			System.debug('Test Acc: ' + testAcc.ID);
			Datetime dt = System.now().addDays(3);
			// create types of Docusign contract

			dsfs__DocuSign_Status__c iniCon = new dsfs__DocuSign_Status__c(dsfs__Subject__c = 'Network Application', dsfs__Sent_Date_Time__c = System.now(),
																			dsfs__Completed_Date_Time__c = dt, dsfs__Envelope_Status__c = 'Completed', dsfs__Company__c = testAcc.Id);
			insert iniCon;

			// check if initial contract was created
			System.assertNotEquals(null, iniCon.Id, 'Inital Contract Created');
			CAT__c iniConTest = [Select Id, Name, New_Application_Received__c, Application_Completed_Date__c from CAT__c where DocuSign_Link__c =: iniCon.Id];
			System.assertNotEquals(null, iniConTest, 'Created CAT');
			System.assertEquals(iniCon.dsfs__Completed_Date_Time__c, iniConTest.New_Application_Received__c, 'Date was copied');
			System.assertEquals(iniCon.dsfs__Completed_Date_Time__c, iniConTest.Application_Completed_Date__c, 'Date was copied');

			dsfs__DocuSign_Status__c reCon = new dsfs__DocuSign_Status__c(dsfs__Subject__c = 'Recredential Application Package', dsfs__Sent_Date_Time__c = System.now(),
																			dsfs__Completed_Date_Time__c = dt, dsfs__Envelope_Status__c = 'Completed', dsfs__Company__c = testAcc.Id);
			insert reCon;

			CAT__c reConTest = [Select Id, Name, Application_Completed_Date__c, Credentialing_Status__c from CAT__c where DocuSign_Link__c =: reCon.Id];
			System.assertNotEquals(null, reConTest, 'Created CAT');
			System.assertEquals(reCon.dsfs__Completed_Date_Time__c, reConTest.Application_Completed_Date__c, 'Date was copied');
			System.assertEquals('Ready for triage', reConTest.Credentialing_Status__c, 'Status was updated');

			
		}
		
	}
	
	
}