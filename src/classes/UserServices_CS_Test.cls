@isTest
private class UserServices_CS_Test {

	/******* Test Parameters *******/
	static private final Integer N_PROVIDER_USERS = 10;
	static private final Integer N_PAYER_USERS = 10;
	
	/******* Provider *******/
	static private List<Account> providers;
	static private List<Contact> providerUsers;

	/******* Payer *******/
	static private List<Account> payers;
	static private List<Contact> payerUsers;

	/******* Test Objects *******/
	static private UserServices_CS providerUserServices;
	static private UserServices_CS payerUserServices;

	static testMethod void UserServices_Test_getUsers(){
		init();
		
		System.assert(providerUserServices.getUsers().getResultSize() == N_PROVIDER_USERS - 1,
			'Results size ' + providerUserServices.getUsers().getResultSize() + ' != ' + (N_PROVIDER_USERS - 1));
			
		System.assert(payerUserServices.getUsers().getResultSize() == N_PAYER_USERS - 1,
			'Results size ' + payerUserServices.getUsers().getResultSize() + ' != ' + (N_PAYER_USERS - 1));
	}

	static testMethod void UserServices_Test_getUserByName(){
		init();
		
		System.assert(providerUserServices.getUserByName('testFirst0 testLast0').Id == providerUsers[0].Id,
			'Couldn\'t find provider user 0 with default name.');
			
		System.assert(payerUserServices.getUserByName('testFirst0 testLast0').Id == payerUsers[0].Id,
			'Couldn\'t find payer user 0 with default name.');
	}
	
	static testMethod void UserServices_Test_getUserById(){
		init();
		
		System.assert(providerUserServices.getUserById(providerUsers[0].Id).Id == providerUsers[0].Id,
			'Couldn\'t find provider user 0 with Id ' + providerUsers[0].Id + '.');
			
		System.assert(payerUserServices.getUserById(payerUsers[0].Id).Id == payerUsers[0].Id,
			'Couldn\'t find payer user 0 with Id ' + payerUsers[0].Id + '.');
	}
	
	static testMethod void UserServices_Test_resetUserPassword(){
		init();
		
		UserServices_CS.resetUserPassword(providerUsers[0]);
	}

    static void init(){
    	
    	///////////////////////////////////
		// Data
		///////////////////////////////////
    	
    	//Providers
		providers = TestDataFactory_CS.generateProviders('TestProvider', 1);
		insert providers;
		
		//Provider users, set user roles/profiles
		providerUsers = TestDataFactory_CS.generateProviderContacts('ProviderUser', providers, N_PROVIDER_USERS);
		insert providerUsers;

		//Payers
		payers = TestDataFactory_CS.generatePlans('TestPayer',1);
		insert payers;
		
		//Payer users
		payerUsers = TestDataFactory_CS.generatePlanContacts('PayerUser', payers, N_PAYER_USERS);
		insert payerUsers;

		providerUserServices = new UserServices_CS(providerUsers[0].Id);
		payerUserServices = new UserServices_CS(payerUsers[0].Id);
    }

}