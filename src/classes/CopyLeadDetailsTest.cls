@isTest
private class CopyLeadDetailsTest {
	
	@isTest (seeAllData = true)
    static void test_method_one() {
		// Implement test code

		Profile p = [Select id from Profile where Name ='Integra Standard User'];
		User u = new User(LastName='User', 
						  Alias='use',
						  Email='testuser@test.com',
						  Username='testingtriggeruser@testingcenter.com',
						  CommunityNickname='testing',
						  EmailEncodingKey='UTF-8',
						  LanguageLocaleKey='en_US',
						  LocaleSidKey='en_US',
						  ProfileId = p.Id,
						  TimeZoneSidKey='America/New_York');
		insert u;

		system.runAs(u){

		Lead testLead = new Lead(LastName='Test last Name', 
			                     Company='Test Company',
			                     Status = 'Open', 
			                     Type__c = 'Provider',
			                     Type_of_Provider__c= 'DME',
			                     County__c = 'NewYork',
			                     State__c = 'New York');
		insert testLead;

		Lead checkLead = [SELECT Id,
					         State FROM Lead where Id=: testLead.Id];

		System.assertEquals('NY', checkLead.State, 'Two character state code copied ');

		Provider_Product_Identifier__c testProd1 = new Provider_Product_Identifier__c(Product_Name__c='Oxygen');
		insert testProd1;
		
		Provider_Product_Identifier__c testProd2 = new Provider_Product_Identifier__c(Product_Name__c='Diabetic');
		insert testProd2;

		Provider_Product_Item__c testProvProd1 = new Provider_Product_Item__c(Lead__c = testLead.Id, Product_Identifier__c= testProd1.Id);
		insert testProvProd1;

		Provider_Product_Item__c testProvProd2 = new Provider_Product_Item__c(Lead__c = testLead.Id, Product_Identifier__c= testProd2.Id);
		insert testProvProd2;

		US_State_County__c testCounty1 = new US_State_County__c(Name='NewYork', State__c='New York');
		insert testCounty1;

		US_State_County__c testCounty2 = new US_State_County__c(Name='Alabama', State__c='Alabama');
		insert testCounty2;

		County_Item__c testItem1 = new County_Item__c(Lead__c = testLead.Id, US_State_County__c = testCounty1.Id);
		insert testItem1;

		County_Item__c testItem2 = new County_Item__c(Lead__c = testLead.Id, US_State_County__c = testCounty2.Id);
		insert testItem2;

		// convert the lead
		Account a = new Account(Name='Test Account', 
						        Type_Of_Provider__c = 'DME');
		insert a;
		//create test opportunity
		Opportunity testOpp = new Opportunity(Name = 'Test Lead Opp', 
											  AccountId = a.Id, 
											  CloseDate = System.today(), 
											  StageName='Open');
		insert testOpp;

		Database.LeadConvert testCon = new Database.LeadConvert();
		testCon.setLeadId(testLead.Id);
		testCon.setDoNotCreateOpportunity(false);
		testCon.setConvertedStatus('Contract Executed');

		Database.LeadConvertResult testConRes = Database.ConvertLead(testCon);
		System.assert(testConRes.isSuccess());

		Lead testLe = [Select Id, ConvertedAccountId, ConvertedOpportunityId, County__c from Lead where Id=:testLead.Id ];

		Account aTest = [Select Id,County__c  from Account where Id=:testLe.ConvertedAccountId];

		System.assertEquals(testLead.County__c, aTest.County__c, 'The County value was copied from Lead to Opoortunity');

		Opportunity oTest = [Select Id, Name, Corp_County__c from Opportunity where Id=:testLe.ConvertedOpportunityId];

		System.assertEquals(testLead.County__c, oTest.Corp_County__c, 'The county value was copied from Lead to Opportunity');

		List<Provider_Product_Item__c> testList = [Select Lead__c, Opportunity__c, Product_Identifier__c from Provider_Product_Item__c where Lead__c =: testLead.Id];

		for(Provider_Product_Item__c pp : testList){

			System.assertEquals(pp.Opportunity__c,testLe.ConvertedOpportunityId, 'The products were mapped to opportunity');
		}

		List<County_Item__c> testItems = [Select Lead__c, Opportunity__c from County_Item__c where Id=: testLead.Id];

		for(County_Item__c pi : testItems){

			System.assertEquals(pi.Opportunity__c,testLe.ConvertedOpportunityId,'The County Items were mapped to Opoortunity');
		}

		}
	}
	
}