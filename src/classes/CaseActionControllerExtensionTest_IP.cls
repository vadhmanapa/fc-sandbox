@isTest
private class CaseActionControllerExtensionTest_IP {
    
    /*private static User testCSUser;
    private static User testClaimUser;
    private static CaseActionControllerExtension_IP con;
    private static Case testCase;
    private static Case testClaimCase;
    private static Case_Action__c act;

    @isTest static void checkIfParentCaseIsFilled() {
        generateTestData();
        System.runAs(testCSUser){
            PageReference pageRef = Page.CaseActionOverride_IP;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('cId', String.valueOf(testCase.Id));
            ApexPages.StandardController scc = new ApexPages.StandardController(act);
            Test.startTest();
            con = new CaseActionControllerExtension_IP(scc);
            Test.stopTest();
        }

        System.assertNotEquals(null, con.parentCase);
    }

    @isTest static void checkIfParentCaseReasonsIsFilled() {
        generateTestData();
        testCase.CaseReasonDetail__c = '';
        update testCase;

        System.runAs(testCSUser){
            PageReference pageRef = Page.CaseActionOverride_IP;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('cId', String.valueOf(testCase.Id));
            ApexPages.StandardController scc = new ApexPages.StandardController(act);
            Test.startTest();
            con = new CaseActionControllerExtension_IP(scc);
            Test.stopTest();
        }

        System.assertNotEquals(null, con.parentCase);
        System.assertEquals(true, con.raiseFlag);
    }

    @isTest static void checkActionsPicklist() {
        generateTestData();
        List<SelectOption> testActions;
        System.runAs(testCSUser){
            PageReference pageRef = Page.CaseActionOverride_IP;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('cId', String.valueOf(testCase.Id));
            ApexPages.StandardController scc = new ApexPages.StandardController(act);
            con = new CaseActionControllerExtension_IP(scc);
            Test.startTest();
            testActions = con.getActions();
            Test.stopTest();
        }

        System.assertNotEquals(0, testActions.size());
    }

    @isTest static void checkActionDetailsPicklistWithoutAction() {
        generateTestData();
        List<SelectOption> testActionDetails;
        System.runAs(testCSUser){
            PageReference pageRef = Page.CaseActionOverride_IP;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('cId', String.valueOf(testCase.Id));
            ApexPages.StandardController scc = new ApexPages.StandardController(act);
            con = new CaseActionControllerExtension_IP(scc);
            Test.startTest();
            testActionDetails = con.getActionDetails();
            Test.stopTest();
        }

        System.assertNotEquals(0, testActionDetails.size());
    }

    @isTest static void checkActionDetailsPicklistWithAction() {
        generateTestData();
        List<SelectOption> testActionDetails;
        System.runAs(testCSUser){
            PageReference pageRef = Page.CaseActionOverride_IP;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('cId', String.valueOf(testCase.Id));
            ApexPages.StandardController scc = new ApexPages.StandardController(act);
            con = new CaseActionControllerExtension_IP(scc);
            con.actionSelected = 'Assistance Needed';
            Test.startTest();
            testActionDetails = con.getActionDetails();
            Test.stopTest();
        }

        System.assertNotEquals(0, testActionDetails.size());
    }

    @isTest static void checkLongComments() {
        generateTestData();
        PageReference newPgRef;
        System.runAs(testCSUser){
            PageReference pageRef = Page.CaseActionOverride_IP;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('cId', String.valueOf(testCase.Id));
            ApexPages.StandardController scc = new ApexPages.StandardController(act);
            con = new CaseActionControllerExtension_IP(scc);
            con.actionSelected = 'Assistance Needed';
            con.actionDetailSelected = 'Escalated to Help Team';
            String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
            String randStr = '';
            while (randStr.length() < 520) {
               Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
               randStr += chars.substring(idx, idx+1);
            }
            con.logAction.Case_Comment__c = randStr;
            Test.startTest();
            newPgRef = con.save();
            Test.stopTest();
        }

        System.assertEquals(null, newPgRef);
        System.assertEquals(true, con.raiseFlag);
    }

    @isTest static void checkLongCommentsForClaims() {
        generateTestData();
        PageReference newPgRef;
        System.runAs(testClaimUser){
            PageReference pageRef = Page.CaseActionOverride_IP;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('cId', String.valueOf(testClaimCase.Id));
            ApexPages.StandardController scc = new ApexPages.StandardController(act);
            con = new CaseActionControllerExtension_IP(scc);
            con.actionSelected = 'Informed';
            con.actionDetailSelected = 'Explained the denial to the provider';
            String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
            String randStr = '';
            while (randStr.length() < 1010) {
               Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
               randStr += chars.substring(idx, idx+1);
            }
            con.logAction.Case_Comment__c = randStr;
            Test.startTest();
            newPgRef = con.save();
            Test.stopTest();
        }

        System.assertEquals(null, newPgRef);
        System.assertEquals(true, con.raiseFlag);
    }

    @isTest static void saveWithoutAction() {
        generateTestData();
        PageReference newPgRef;
        System.runAs(testCSUser){
            PageReference pageRef = Page.CaseActionOverride_IP;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('cId', String.valueOf(testCase.Id));
            ApexPages.StandardController scc = new ApexPages.StandardController(act);
            con = new CaseActionControllerExtension_IP(scc);
            con.actionDetailSelected = 'Escalated to Help Team';
       
            Test.startTest();
            newPgRef = con.save();
            Test.stopTest();
        }

        System.assertEquals(null, newPgRef);
        System.assertEquals(true, con.raiseFlag);
    }

    @isTest static void saveWithoutActionDetail() {
        generateTestData();
        PageReference newPgRef;
        System.runAs(testCSUser){
            PageReference pageRef = Page.CaseActionOverride_IP;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('cId', String.valueOf(testCase.Id));
            ApexPages.StandardController scc = new ApexPages.StandardController(act);
            con = new CaseActionControllerExtension_IP(scc);
            con.actionSelected = 'Assistance Needed';
       
            Test.startTest();
            newPgRef = con.save();
            Test.stopTest();
        }

        System.assertEquals(null, newPgRef);
        System.assertEquals(true, con.raiseFlag);
    }

    @isTest static void checkSaveCaseAction() {
        generateTestData();
        PageReference newPgRef;
        System.runAs(testCSUser){
            PageReference pageRef = Page.CaseActionOverride_IP;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('cId', String.valueOf(testCase.Id));
            ApexPages.StandardController scc = new ApexPages.StandardController(act);
            con = new CaseActionControllerExtension_IP(scc);
            con.actionSelected = 'Assistance Needed';
            con.actionDetailSelected = 'Escalated to Help Team';
            String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
            String randStr = '';
            while (randStr.length() < 499) {
               Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
               randStr += chars.substring(idx, idx+1);
            }
            con.logAction.Case_Comment__c = randStr;
            Test.startTest();
            newPgRef = con.save();
            Test.stopTest();
        }

        Case c = [SELECT Id, CaseNumber, (SELECT Id FROM Case_Actions__r) FROM Case WHERE Id=: testCase.Id];
        System.assertNotEquals(null, newPgRef);
        System.assertEquals(false, con.raiseFlag);
        System.assertNotEquals(0, c.Case_Actions__r.size());
    }
    

    @isTest static void generateTestData(){

        testCSUser = new User();
        testCSUser.FirstName = 'CustomerCallCase';
        testCSUser.LastName = 'Tester'+ System.currentTimeMillis(); // to maintain the randomness of username
        testCSUser.Belongs_To__c = 'Customer Service';
        testCSUser.Username = 'test'+System.currentTimeMillis()+'@test.com';
        testCSUser.CommunityNickname = 'cccase';
        testCSUser.ProfileId = [SELECT Id FROM Profile WHERE Name='Customer Service Supervisor w Service Cloud' LIMIT 1].Id;
        testCSUser.Email = 'test'+System.currentTimeMillis()+'@test.com';
        testCSUser.CompanyName = 'Integra Partners';
        testCSUser.Alias = 'alias';
        testCSUser.TimeZoneSidKey = 'America/New_York';
        testCSUser.EmailEncodingKey = 'UTF-8';
        testCSUser.LanguageLocaleKey = 'en_US';
        testCSUser.LocaleSidKey = 'en_US';
        //testCSUser.UserRoleId = ur1.Id;
        insert testCSUser;
    
        testClaimUser = new User();
        testClaimUser.FirstName = 'ClaimsCallCase';
        testClaimUser.LastName = 'Tester'+ System.currentTimeMillis(); // to maintain the randomness of username
        testClaimUser.Username = 'test'+System.currentTimeMillis()+'@test.com';
        testClaimUser.CommunityNickname = 'cclaims';
        testClaimUser.Belongs_To__c = 'Claims';
        testClaimUser.ProfileId = [SELECT Id FROM Profile WHERE Name='Claims w Service Cloud' LIMIT 1].Id;
        testClaimUser.Email = 'test'+System.currentTimeMillis()+'@test.com';
        testClaimUser.CompanyName = 'Integra Partners';
        testClaimUser.Alias = 'alias2';
        testClaimUser.TimeZoneSidKey = 'America/New_York';
        testClaimUser.EmailEncodingKey = 'UTF-8';
        testClaimUser.LanguageLocaleKey = 'en_US';
        testClaimUser.LocaleSidKey = 'en_US';
        //testClaimUser.UserRoleId = ur2.Id;
        insert testClaimUser;

        Account testPay = new Account();
        testPay.Name = 'This is test payor';
        testPay.Type__c = 'Payer';
        testPay.Status__c = 'Active';
        insert testPay;
        
        // CREATE CONTACTS - ONE OF EACH - PATIENT, PAYOR, PROVIDER, HOSPITAL/DR, FAMILY
        Contact testPatient = new Contact();
        testPatient.FirstName = 'Sick';
        testPatient.LastName = 'I am Patient';
        testPatient.RecordTypeId = RecordTypes.patientId;
        testPatient.AccountId = testPay.Id;
        testPatient.Active__c = true;
        testPatient.Birthdate = System.today();
        testPatient.Health_Plan_ID__c = '90920-9-029-9';
        insert testPatient;

        Case_Reasons_Relationship__c customerTestReason1 = new Case_Reasons_Relationship__c(); 
        customerTestReason1.CaseReasonDetail__c = 'O&P Item Request';
        customerTestReason1.Action__c = 'Assistance Needed';
        customerTestReason1.ActionDetail__c = 'Escalated to Help Team';
        customerTestReason1.UpdatedCaseStatus__c = 'Closed';
        insert customerTestReason1;
        
        Case_Reasons_Relationship__c customerTestReason2 = new Case_Reasons_Relationship__c(); 
        customerTestReason2.CaseReasonDetail__c = 'O&P Item Request';
        customerTestReason2.Action__c = 'Connected to Provider';
        customerTestReason2.ActionDetail__c = 'Warm transfer to the provider';
        customerTestReason2.UpdatedCaseStatus__c = 'Closed';
        insert customerTestReason2;

        Case_Reasons_Relationship__c claimsTestReason = new Case_Reasons_Relationship__c(); 
        claimsTestReason.CaseReasonDetail__c = 'Denied correctly';
        claimsTestReason.Action__c = 'Informed';
        claimsTestReason.ActionDetail__c = 'Explained the denial to the provider';
        claimsTestReason.UpdatedCaseStatus__c = 'Closed';
        insert claimsTestReason;

        testCase = new Case();
        testCase.ContactId = testPatient.Id;
        testCase.RecordTypeId = RecordTypes.customerCallId;
        testCase.Subject = 'This is a test email subject';
        testCase.Description = 'This is a test case Description';
        testCase.CallerType__c = 'Patient';
        testCase.CaseReason__c = 'New Referral Inquiry';
        testCase.CaseReasonDetail__c = 'O&P Item Request';

        insert testCase;

        testClaimCase = new Case();
        testClaimCase.ContactId = testPatient.Id;
        testClaimCase.RecordTypeId = RecordTypes.claimsEmailId;
        testClaimCase.Subject = 'This is a test email subject';
        testClaimCase.Description = 'This is a test case Description';
        testClaimCase.CallerType__c = 'Provider Email';
        testClaimCase.CaseReason__c = 'Available Claim Status';
        testClaimCase.CaseReasonDetail__c = 'Denied correctly';

        insert testClaimCase;

        act = new Case_Action__c();
    }*/
    
}