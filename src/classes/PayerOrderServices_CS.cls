/**
 *  @Description Services class for searching providers, patients, and orders
 *  @Author Cloud Software LLC
 *  Revision History: 
 *		12/09/2015 - karnold - ISSCI-171 - Added searching for plan patient medicare or medicaid ID
 *		04/18/2015 - karnold - IMOR-08 - Added to search functionality to also search order 
 *			and patient for medicare, medicaid, and id number
 *
 */
public virtual without sharing class PayerOrderServices_CS extends OrderServices_CS {

	public PayerOrderServices_CS(String cId) {
		super(cId);
	}
    
     /*
	 ** NOTE: STAGING FOR FUTURE RELEASE - SPRINT 12/12 - BY VK - TO BE ADDED **/
    
	 public static List<String> providerNeedsAttention {
		get{
			if(providerNeedsAttention == null) {
                providerNeedsAttention = new List<String>();
                for(String s : OrderStatusMetadataWrapper_IP.getProviderNAQ().keySet()){
                    providerNeedsAttention.add(OrderStatusMetadataWrapper_IP.getProviderNAQ().get(s));
                }
			}
			return providerNeedsAttention;
		}set;
	}
    
    public static List<String> payorNeedsAttention{
        get{
            if(payorNeedsAttention == null){
                payorNeedsAttention = new List<String>();
                for(String s : OrderStatusMetadataWrapper_IP.getPayorNAQ().keySet()){
                    payorNeedsAttention.add(OrderStatusMetadataWrapper_IP.getPayorNAQ().get(s));
                }
            }
            return payorNeedsAttention;
        }set;
    }

	public Apexpages.Standardsetcontroller getOrdersByProvider(String providerId) {
        Apexpages.Standardsetcontroller retStandController;
		try {
			SoqlBuilder sb = attachToBaseQuery(new AndCondition()
				.add(new FieldCondition('Provider__c', Operator.EQUALS, providerId)));
            retStandController = new Apexpages.Standardsetcontroller(Database.getQueryLocator(sb.toSoql()));
		}
		catch(Exception e) {
			//Todo: improve error message
			throw((e instanceOf ApplicationException) ? e : new ApplicationException('Invalid providerId: ' + providerId, e));
			System.debug(e.getMessage());
		}
		return retStandController;
	}

	public Apexpages.Standardsetcontroller getOrdersByPatientId(String Id) {

		try {
			SoqlBuilder sb = attachToBaseQuery(new AndCondition()
				.add(new FieldCondition('Plan_Patient__c', Operator.EQUALS, Id))
			);
			return new ApexPages.Standardsetcontroller(Database.getQueryLocator(sb.toSoql()));
		}
		catch(Exception e) {
			System.debug(e.getMessage());
		}

		return null;
	}

		/**
	     ** NOTE: STAGING FOR FUTURE RELEASE - SPRINT 12/19 - BY VK - TO BE REPLACED WITH CURRENT getOrdersForSummary
	     ** public ApexPages.Standardsetcontroller getOrdersForSummary(String stage, String subStage, String userId, String providerId, String searchTerm, String timeWindow) { // added substage by VK for Sprint 12/12
	    **/
	public ApexPages.Standardsetcontroller getOrdersForSummary(String stage,String subStage, String userId, String payerId, String searchTerm, String timeWindow) {
        // adding subStage filter
		return getOrdersForSummaryWithSort(stage, subStage, userId, payerId, searchTerm, timeWindow, null);
	}

    public ApexPages.Standardsetcontroller getOrdersForSummaryWithSort(String stage, String subStage, String userId, String providerId, String searchTerm, String timeWindow, Map<String, String> sortColumn) {
		try {

			NestableCondition topFilterLogic = new AndCondition();
			topFilterLogic.add(new FieldCondition('Stage__c', Operator.NOT_EQUALS, 'Draft'));
			if (String.isNotBlank(stage)) {
				System.debug('getOrdersForSummary: Searching for stage: ' + stage);
				topFilterLogic.add(new FieldCondition('Stage__c', Operator.EQUALS, stage));
			}
            
            /**************** NOTE: STAGING FOR FUTURE RELEASE - SPRINT 12/12 - BY VK - TO BE ADDED******************/
            
            if(String.isNotBlank(subStage) && String.isNotEmpty(subStage)){
                System.debug('getOrdersForSummary: Searching for sub stage: ' + subStage);
                if(subStage == 'My Attention'){
                    // this means that the status queried should be payor based and needs provider to flag or un-flag as Needs Attention
                    topFilterLogic.add(new SetCondition('Status__c').INX(providerNeedsAttention));
                }
                else if(subStage == 'Other Attention'){
                    topFilterLogic.add(new SetCondition('Status__c').INX(payorNeedsAttention));
                }
                else{}
            }

			if (String.isNotBlank(userId)) {
				System.debug('getOrdersForSummary: Searching for user: ' + userId);

				//Changed logic for Care Navigator lookup field, instead of Entered By - 6/29/15
				//topFilterLogic.add(new FieldCondition('Entered_By__c',Operator.EQUALS,userId));
				topFilterLogic.add(new FieldCondition('Case_Manager__c', Operator.EQUALS, userId));
			}

			if (String.isNotBlank(providerId)) {
				System.debug('getOrdersForSummary: Searching for provider: ' + providerId);
				topFilterLogic.add(new FieldCondition('Provider__c', Operator.EQUALS, providerId));
			}

			if (String.isNotBlank(searchTerm)) {
				searchTerm = searchTerm.trim();
				System.debug('getOrdersForSummary: Searching with term: ' + searchTerm);
				topFilterLogic.add(new OrCondition()
					.add(new FieldCondition('Name').likex('%' + searchTerm + '%'))
					.add(new FieldCondition('Authorization_Number__c').likex('%' + searchTerm + '%'))
					.add(new FieldCondition('Patient_First_Name__c').likex('%' + searchTerm + '%'))
					.add(new FieldCondition('Patient_Last_Name__c').likex('%' + searchTerm + '%'))
					.add(new FieldCondition('Plan_Patient__r.First_Name__c').likex('%' + searchTerm + '%'))
					.add(new FieldCondition('Plan_Patient__r.Last_Name__c').likex('%' + searchTerm + '%'))
					.add(new FieldCondition('Plan_Patient__r.Name').likex('%' + searchTerm + '%'))
					.add(new FieldCondition('Plan_Patient__r.ID_Number__c').likex('%' + searchTerm + '%')) // Added seaching for patient medicare or medicaid ids
					.add(new FieldCondition('Plan_Patient__r.Medicare_Id__c').likex('%' + searchTerm + '%'))
					.add(new FieldCondition('Plan_Patient__r.Medicaid_Id__c').likex('%' + searchTerm + '%'))
					.add(new FieldCondition('Patient_ID_Number__c').likex('%' + searchTerm + '%'))
					.add(new FieldCondition('Patient_Medicaid_ID__c').likex('%' + searchTerm + '%')) // Added seaching for patient medicare or medicaid ids
					.add(new FieldCondition('Patient_Medicare_ID__c').likex('%' + searchTerm + '%'))
				);
			}

			if (String.isNotBlank(timeWindow) && timeWindow != 'allTime') {
				/*
				  Date startOfWeek;
				 
				  if(Date.today().toStartOfWeek() == Date.today()){
				  startOfWeek = Date.today().addDays(-6);
				  } else {
				  startOfWeek = Date.today().toStartOfWeek().addDays(1);
				  }
				 
				  DateTime start;
				  DateTime stop;
				 
				  if(timeWindow == 'thisWeek'){
				  start = DateTime.newInstance(startOfWeek,Time.newInstance(0,0,0,0));
				  stop = DateTime.now();
				  }
				  else if(timeWindow == 'lastWeek'){
				  start = DateTime.newInstance(startOfWeek.addDays(-7), Time.newInstance(0,0,0,0));
				  stop = DateTime.newInstance(startOfWeek, Time.newInstance(0,0,0,0));
				  }
				  else if(timeWindow == 'thisMonth'){
				  start = DateTime.newInstance(Date.today().toStartOfMonth(),Time.newInstance(0,0,0,0));
				  stop = DateTime.now();
				  }
				  else if(timeWindow == 'lastMonth'){
				  start = DateTime.newInstance(Date.today().toStartOfMonth().addDays(-1).toStartOfMonth(),Time.newInstance(0,0,0,0));
				  stop = DateTime.newInstance(Date.today().toStartOfMonth(),Time.newInstance(0,0,0,0));
				  }
				  else{
				  start = DateTime.newInstance(startOfWeek.addDays(1),Time.newInstance(0,0,0,0));
				  stop = DateTime.now();
				  }
				 
				  topFilterLogic.add(new AndCondition()
				  .add(new FieldCondition('CreatedDate').greaterThan(start))
				  .add(new FieldCondition('CreatedDate').lessThanOrEqualTo(stop)));
				 */
				if (timeWindow == 'thisWeek') {
					topFilterLogic.add(OrderServices_CS.thisWeek('CreatedDate'));
				}
				else if (timeWindow == 'lastWeek') {
					topFilterLogic.add(OrderServices_CS.lastWeek('CreatedDate'));
				}
				else if (timeWindow == 'thisMonth') {
					topFilterLogic.add(OrderServices_CS.thisMonth('CreatedDate'));
				}
				else if (timeWindow == 'lastMonth') {
					topFilterLogic.add(OrderServices_CS.lastMonth('CreatedDate'));
				}
				else {
					topFilterLogic.add(OrderServices_CS.thisWeek('CreatedDate'));
				}
			}

			SoqlBuilder sb = attachToBaseQueryWithoutSorting(topFilterLogic);

			/*if (stage == 'Needs Attention') {
				System.debug('Ordering by LastModifiedDate_Notes__c');
				sb.orderByx(new OrderBy('LastModifiedDate_Notes__c').descending().nullsLast());
			}
			else {
				sb.orderByx(new OrderBy('CreatedDate').descending().nullsLast());
			}*/

			String sortColumnValue = OrderServices_CS.defaultTableSortColumn;
			String sortColumnOrder = OrderServices_CS.defaultTableSortOrder;
			if (sortColumn != null) {
				sortColumn.remove(null);
				for (String column : sortColumn.keySet()) {
					sortColumnValue = column;
					sortColumnOrder = (sortColumn.get(sortColumnValue) != null && String.isNotEmpty(sortColumn.get(sortColumnValue)))
						? sortColumn.get(sortColumnValue) : sortColumnOrder;
				}
			}
			System.debug('Ordering by DOM ID (' + sortColumnValue + ') ' + sortColumnOrder);
            if (orderTableColumns.containsKey(sortColumnValue)) {
                OrderBy sortCondition = new OrderBy(orderTableColumns.get(sortColumnValue));
                if ('DESC'.equalsIgnoreCase(sortColumnOrder)) {
                    sortCondition.descending();
                } else {
                    sortCondition.ascending();
                }
                sortCondition.nullsLast();
                sb.orderByx(sortCondition);
            }

			System.debug('Final query: ' + sb.toSoql());

			return new ApexPages.Standardsetcontroller(Database.getQueryLocator(sb.toSoql()));
		}
		catch(Exception e) {
			System.debug(e.getMessage());
		}

		return null;
	}

	//This is used exclusively for finding the list of users/providers to be used on the payer orders summary
	public List<Order__c> getOrderListForSummary(String stage, String subStage, String userId, String providerId, String searchTerm, String timeWindow) {
		try {

			NestableCondition topFilterLogic = new AndCondition();

			if (stage != null && stage != '') {
				System.debug('getOrdersForSummary: Searching for stage: ' + stage);
				topFilterLogic.add(new FieldCondition('Stage__c', Operator.EQUALS, stage));
			}
            
            /**************** NOTE: STAGING FOR FUTURE RELEASE - SPRINT 12/12 - BY VK - TO BE ADDED******************/
            
            if(String.isNotBlank(subStage) && String.isNotEmpty(subStage)){
                System.debug('getOrderListForSummary: Searching for sub stage: ' + subStage);
                if(subStage == 'My Attention'){
                    // this means that the status queried should be payor based and needs provider to flag or un-flag as Needs Attention
                    topFilterLogic.add(new SetCondition('Status__c').INX(providerNeedsAttention));
                }
                else if(subStage == 'Other Attention'){
                    topFilterLogic.add(new SetCondition('Status__c').INX(payorNeedsAttention));
                }
                else{}
            }

			if (userId != null && userId != '') {
				System.debug('getOrdersForSummary: Searching for user: ' + userId);

				//Changed logic for Care Navigator lookup field, instead of Entered By - 6/29/15
				//topFilterLogic.add(new FieldCondition('Entered_By__c',Operator.EQUALS,userId));
				topFilterLogic.add(new FieldCondition('Case_Manager__c', Operator.EQUALS, userId));
			}

			if (providerId != null && providerId != '') {
				System.debug('getOrdersForSummary: Searching for provider: ' + providerId);
				topFilterLogic.add(new FieldCondition('Provider__c', Operator.EQUALS, providerId));
			}

			if (searchTerm != null && searchTerm != '') {
				System.debug('getOrdersForSummary: Searching with term: ' + searchTerm);
				topFilterLogic.add(new OrCondition()
					.add(new FieldCondition('Name').likex('%' + searchTerm + '%'))
					.add(new FieldCondition('Authorization_Number__c').likex('%' + searchTerm + '%'))
					.add(new FieldCondition('Patient_First_Name__c').likex('%' + searchTerm + '%'))
					.add(new FieldCondition('Patient_Last_Name__c').likex('%' + searchTerm + '%'))
					.add(new FieldCondition('Plan_Patient__r.First_Name__c').likex('%' + searchTerm + '%'))
					.add(new FieldCondition('Plan_Patient__r.Last_Name__c').likex('%' + searchTerm + '%'))
					.add(new FieldCondition('Plan_Patient__r.Name').likex('%' + searchTerm + '%'))
					.add(new FieldCondition('Plan_Patient__r.ID_Number__c').likex('%' + searchTerm + '%')) // Added seaching for patient medicare or medicaid ids
					.add(new FieldCondition('Plan_Patient__r.Medicare_Id__c').likex('%' + searchTerm + '%'))
					.add(new FieldCondition('Plan_Patient__r.Medicaid_Id__c').likex('%' + searchTerm + '%'))
					.add(new FieldCondition('Patient_ID_Number__c').likex('%' + searchTerm + '%'))
					.add(new FieldCondition('Patient_Medicaid_ID__c').likex('%' + searchTerm + '%')) // Added seaching for patient medicare or medicaid ids
					.add(new FieldCondition('Patient_Medicare_ID__c').likex('%' + searchTerm + '%'))
				);
			}

			if (String.isNotBlank(timeWindow) && timeWindow != 'allTime') {
				/*
				  Date startOfWeek;
				 
				  if(Date.today().toStartOfWeek() == Date.today()){
				  startOfWeek = Date.today().addDays(-6);
				  } else {
				  startOfWeek = Date.today().toStartOfWeek().addDays(1);
				  }
				 
				  DateTime start;
				  DateTime stop;
				 
				  if(timeWindow == 'thisWeek'){
				  start = DateTime.newInstance(startOfWeek.addDays(1),Time.newInstance(0,0,0,0));
				  stop = DateTime.now();
				  }
				  else if(timeWindow == 'lastWeek'){
				  start = DateTime.newInstance(startOfWeek.addDays(-1).toStartOfWeek().addDays(1), Time.newInstance(0,0,0,0));
				  stop = DateTime.newInstance(startOfWeek, Time.newInstance(0,0,0,0));
				  }
				  else if(timeWindow == 'thisMonth'){
				  start = DateTime.newInstance(Date.today().toStartOfMonth(),Time.newInstance(0,0,0,0));
				  stop = DateTime.now();
				  }
				  else if(timeWindow == 'lastMonth'){
				  start = DateTime.newInstance(Date.today().toStartOfMonth().addDays(-1).toStartOfMonth(),Time.newInstance(0,0,0,0));
				  stop = DateTime.newInstance(Date.today().toStartOfMonth().addDays(-1),Time.newInstance(0,0,0,0));
				  }
				  else{
				  start = DateTime.newInstance(startOfWeek.addDays(1),Time.newInstance(0,0,0,0));
				  stop = DateTime.now();
				  }
				 
				  topFilterLogic.add(new AndCondition()
				  .add(new FieldCondition('CreatedDate').greaterThan(start))
				  .add(new FieldCondition('CreatedDate').lessThanOrEqualTo(stop)));
				 */
				if (timeWindow == 'thisWeek') {
					topFilterLogic.add(OrderServices_CS.thisWeek('CreatedDate'));
				}
				else if (timeWindow == 'lastWeek') {
					topFilterLogic.add(OrderServices_CS.lastWeek('CreatedDate'));
				}
				else if (timeWindow == 'thisMonth') {
					topFilterLogic.add(OrderServices_CS.thisMonth('CreatedDate'));
				}
				else if (timeWindow == 'lastMonth') {
					topFilterLogic.add(OrderServices_CS.lastMonth('CreatedDate'));
				}
				else {
					topFilterLogic.add(OrderServices_CS.thisWeek('CreatedDate'));
				}
			}

			SoqlBuilder sb = attachToBaseQuery(topFilterLogic);

			return Database.query(sb.toSoql());
		}
		catch(Exception e) {
			System.debug(e.getMessage());
		}

		return null;
	}
} //End PayerOrderServices_CS