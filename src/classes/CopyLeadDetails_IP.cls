public with sharing class CopyLeadDetails_IP {
	public CopyLeadDetails_IP() {
		
	}

	public static void runBeforeUpdate(List<Lead> newTrigger){

		Map<Id, Lead> getConvertedLeads = new Map<Id,Lead>();// to hold the converted leads
		Map<Id, RecordType> rMap = new Map<Id, RecordType>([Select Id, Name from RecordType where sObjectType = 'Lead']);
		Map<Id, Lead> getConvertedAcc = new Map<Id, Lead>();
		Map<Id, Lead> getConvertedOpp = new Map<Id, Lead>();
		
		
		for(Lead le : newTrigger){

			// run only for provider record type for now

			if(le.isConverted == true){

				getConvertedLeads.put(le.Id, le);
				getConvertedAcc.put(le.ConvertedAccountId, le);
				getConvertedOpp.put(le.ConvertedOpportunityId, le);
			}
			
		}

		System.debug('Number of records to be converted'+getConvertedLeads.size());

		// query all the related child records

		List<Account> updateAcc = [Select Id, Name, County__c from Account where Id=:getConvertedAcc.keyset()];
		if(updateAcc.size() > 0){

			for(Account acc : updateAcc){

				acc.County__c = getConvertedAcc.get(acc.Id).County__c;
				acc.DMEnsion__c = getConvertedAcc.get(acc.Id).DMEnsion__c;
			}

		}

		List<Opportunity> updateOpp = [Select Id, Name, Corp_County__c from Opportunity where Id=:getConvertedOpp.keyset()];
		if(updateOpp.size() > 0){

			for(Opportunity o : updateOpp){

				o.Corp_County__c = getConvertedOpp.get(o.Id).County__c;
				o.DMEnsion__c = getConvertedOpp.get(o.Id).DMEnsion__c;
			}
		}

		
		List<County_Item__c> updateCounty = [Select Id, 
													Name,
													Lead__r.Id,
													Opportunity__c FROM County_Item__c where Lead__r.Id =:getConvertedLeads.keyset()];
		if(updateCounty.size() > 0){

			for(County_Item__c ci : updateCounty){

				Lead thisLead = getConvertedLeads.get(ci.Lead__r.Id);
				ci.Opportunity__c = thisLead.ConvertedOpportunityId;
			}

		

		}											
			

		List<Provider_Product_Item__c> updateProducts = [Select Id, 
																Name,
																Lead__r.Id,
																Opportunity__c FROM Provider_Product_Item__c where Lead__r.Id =:getConvertedLeads.keyset()];

		if(updateProducts.size() > 0){

			for(Provider_Product_Item__c ppi : updateProducts){

				Lead proLead = getConvertedLeads.get(ppi.Lead__r.Id);
				ppi.Opportunity__c = proLead.ConvertedOpportunityId;
			}

		

		}

		System.debug('Updating Accounts');
		update updateAcc;
		System.debug('Updating Opportunity');
		update updateOpp;
		System.debug('Updating County Items');
		update updateCounty;
		System.debug('Updating Product Items');	
		update updateProducts;
	}
}