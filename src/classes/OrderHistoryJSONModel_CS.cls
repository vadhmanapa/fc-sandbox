//Class structure to handle Order History JSON field on Order
public class OrderHistoryJSONModel_CS { 
	
	public Datetime timestamp;
	public String orderStatus;
	public String contactName;
	public String contactId;
	public String accountName;

	public static List<OrderHistoryJSONModel_CS> parseList(String json) {
		return (List<OrderHistoryJSONModel_CS>) System.JSON.deserialize(json, List<OrderHistoryJSONModel_CS>.class);
	}
}