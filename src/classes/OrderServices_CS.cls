/**
* @description Class to provide a stnadard way of accessing the order object.
* @author Cloud Software LLC
*	Revision History:
*		05/23/2016 - karnold - Added the exclusion of draft orders in getBaseConditions.
*/
public virtual without sharing class OrderServices_CS extends AccountServices_CS{

	// Uses the context of the contact and account for all searches

	private final Set<String> objectFields = new Set<String>{
		'Id',
		'Name',
		'CreatedDate',
		'Accepted_By__c',
		'Accepted_By__r.Name',
		'Accepted_By__r.Phone',
		'Accepted_By__r.Entity__r.Name',
		'Accepted_By__r.Entity__r.Phone',
		'Accepted_Date__c',
		'Accept_Order__c',
		'Address_Override__c',
		'Apartment_Number__c',
		'Assignment_History__c',
		'Authorization_End_Date__c',
		'Authorization_Number__c',
		'Authorization_Start_Date__c',
		'Canceled_Order__c',
        'Canceled_Order_Description__c',
		'Case_Manager__c',
		'Case_Manager__r.Name',
		'City__c',
		'Comments__c',
		'Communication_E_mail__c',
		'Delivery_Date__c',
		'Delivery_Date_Time__c',
		'Delivery_Instructions__c',
		'Delivery_Method__c',
		'Delivery_Window_Begin__c',
		'Delivery_Window_End__c',
		'Entered_By__c',
		'Entered_By__r.Name',
		'Estimated_Delivery_Time__c',
		'Estimated_vs_Deliver__c',
		'Expiration_Time__c',
		'Geolocation__Latitude__s',
		'Geolocation__Longitude__s',
		'ICD10_A__c',
		'ICD10_B__c',
		'ICD10_C__c',
		'ICD10_D__c',
		'In_Progress_Timestamp__c',
		'Integra_Comments__c',
		'LastModifiedDate_Notes__c',
		'Legacy_Orders__c',
		'Manually_Assigned__c',
		'Message_Body__c',
		'Message_History_JSON__c',
		'Message_is_Urgent__c',
		'Message_Pending__c',
		'Order_History_JSON__c',
		'Order_Image__c',
		'Order_Inactivity_Tracking__c',
		'Orders_Inactive__c',
		'Original_Order__c',
		'Original_Order__r.Name',
		'Other_Canceled_Orders__c',
		'Parent_Provider__c',
		'Patient_Apartment_Number__c',
		'Patient_City__c',
		'Patient_Country__c',
		'Patient_Date_of_Birth__c',
		'Patient_Email_Address__c',
		'Patient_First_Name__c',
		'Patient_ID_Number__c',
		'Patient_Last_Name__c',
		'Patient_Medicaid_ID__c',
		'Patient_Medicare_ID__c',
		'Patient_Mobile_Phone_Number__c',
		'Patient_Name__c',
		'Patient_Native_Language__c',
		'Patient_Phone_Number__c',
		'Patient_State__c',
		'Patient_Street__c',
		'Patient_Zip_Code__c',
		'Payer__c',
		'Pending_Message_E_mail_Sent__c',
		'Plan_Patient__c',
		'Plan_Patient__r.Name',
		'Plan_Patient__r.ID_Number__c',
		'Plan_Patient__r.Native_Language__c',
		'Plan_Patient__r.Health_Plan__c',
		'Plan_Patient__r.Health_Plan__r.Name',
		'Plan_Patient__r.Health_Plan__r.Phone',
		'Plan_Patient__r.Phone_Number__c',
		'Plan_Patient__r.Cell_Phone_Number__c',
		'Provider__c',
		'Provider__r.Name',
		'Provider__r.Phone',
		'Provider_Assignment_History__c',
		'Provider_Contact__c',
		'Provider_Contact__r.Name',
		'Provider_Contact_E_mail__c',
		'Provider_Location__c',
		'Provider_Phone__c',
		'Reason_for_Rejection__c',
		'Re_Assign_Order__c',
		'Recipient_Cell_Phone__c',
		'Recipient_Country__c',
		'Recipient_Date_of_Birth__c',
		'Recipient_Email__c',
		'Recipient_Email_Address__c',
		'Recipient_First_Name__c',
		'Recipient_Last_Name__c',
		'Recipient_Native_Language__c',
		'Recipient_Phone__c',
		'Recipient_Phone_Number__c',
		'Recipient_Relationship__c',
		'Recurring__c',
		'Recurring_Frequency__c',
		'Recurring_Period__c',
		'Referred__c',
		'Referred_By_NPI__c',
		'Referred_By_Fax__c',
		'Referred_By_First_Name__c',
		'Referred_By_Last_Name__c',
		'Referred_By_Phone__c',
		'Rejected_Order__c',
		'Stage__c',
		'State__c',
		'Status__c',
		'Status_of_Order__c',
		'Street__c',
		'Timeframe_for_Order_Created_to_Delivered__c',
		'Timeframe_from_Order_Accept_to_Deliver__c',
		'Timeframe_from_Order_Created_to_Accepted__c',
		'Timeframe_to_Accepted_hh_mm_ss__c',
		'Tracking_Number__c',
		'Track_In_Progress__c',
		'Track_Needs_Attention__c',
		'Track_Pending_Acceptance__c',
		'Zip__c',
		'Medicare_Primary__c', // added by Venkat on 09/28/2016 for Medicare VNS Implementation
		'NeedsAttentionReason__c' //Staging for future - Sprint 12/12 - by VK
	};

	// Order table column headers dom ids to fields mapping
	protected final Map<String, String> orderTableColumns = new Map<String, String> {
		'orderName' => 'Name',
		'orderedDate' => 'CreatedDate',
		'insuranceId' => 'Plan_Patient__r.ID_Number__c',
		'patient' => 'Plan_Patient__r.Name',
		'authCode' => 'Authorization_Number__c',
		'healthPlan' => 'Plan_Patient__r.Health_Plan__r.Name',
		'acceptedDate' => 'Accepted_Date__c',
		'expectedDate' => 'Estimated_Delivery_Time__c',
		'deliveredDate' => 'Delivery_Date_Time__c',
		'acceptedBy' => 'Accepted_By__r.Name',
		'provider' => 'Provider__r.Name',
		'careNavigator' => 'Case_Manager__r.Name',
		'status' => 'NeedsAttentionReason__c'
	};

	public static final String defaultTableSortColumn = 'expectedDate';
	public static final String defaultTableSortOrder = 'ASC';
	
	//TODO: Depricate - Functionality should be in ProductServices
	private final set<String> lineItemFields = new Set<String>{
		'Deliver_By__c',
		'Delivery_Window_Begin__c',
		'Delivery_Window_End__c',
		'DME__c',
		'Order__c',
		'Quantity__c',
		'RR_NU__c'
	};

	public static NestableCondition thisWeek(String field){
		DateTime startOfWeek;
				
		if(Date.today().toStartOfWeek() == Date.today()){
			startOfWeek = DateTime.newInstance(Date.today().addDays(-6),Time.newInstance(0,0,0,0));
		} else {
			startOfWeek = DateTime.newInstance(Date.today().toStartOfWeek().addDays(1),Time.newInstance(0,0,0,0));
		}
		
		DateTime start = startOfWeek;
		DateTime stop = startOfWeek.addDays(7);
		
		system.debug(LoggingLevel.INFO,'start: ' + start + ' / stop: ' + stop);
		
		return new AndCondition()
			.add(new FieldCondition(field).greaterThanOrEqualTo(start))
			.add(new FieldCondition(field).lessThan(stop));
	}
	
	public static NestableCondition lastWeek(String field){
		DateTime startOfWeek;
				
		if(Date.today().toStartOfWeek() == Date.today()){
			startOfWeek = DateTime.newInstance(Date.today().addDays(-6),Time.newInstance(0,0,0,0));
		} else {
			startOfWeek = DateTime.newInstance(Date.today().toStartOfWeek().addDays(1),Time.newInstance(0,0,0,0));
		}
		
		DateTime start = startOfWeek.addDays(-7);
		DateTime stop = startOfWeek;
		
		system.debug(LoggingLevel.INFO,'start: ' + start + ' / stop: ' + stop);
		
		return new AndCondition()
			.add(new FieldCondition(field).greaterThanOrEqualTo(start))
			.add(new FieldCondition(field).lessThan(stop));
	}
	
	public static NestableCondition thisMonth(String field){

		DateTime start = DateTime.newInstance(Date.today().toStartOfMonth(),Time.newInstance(0,0,0,0));
		DateTime stop = start.addMonths(1);
		
		system.debug(LoggingLevel.INFO,'start: ' + start + ' / stop: ' + stop);
		
		return new AndCondition()
			.add(new FieldCondition(field).greaterThanOrEqualTo(start))
			.add(new FieldCondition(field).lessThan(stop));
	}
	
	public static NestableCondition lastMonth(String field){
		
		DateTime start = DateTime.newInstance(Date.today().toStartOfMonth().addMonths(-1),Time.newInstance(0,0,0,0));
		DateTime stop = DateTime.newInstance(Date.today().toStartOfMonth(),Time.newInstance(0,0,0,0));
		
		system.debug(LoggingLevel.INFO,'start: ' + start + ' / stop: ' + stop);
		
		return new AndCondition()
			.add(new FieldCondition(field).greaterThanOrEqualTo(start))
			.add(new FieldCondition(field).lessThan(stop));
	}
	
	public OrderServices_CS(String cId){
		super(cId);
	}

	public NestableCondition getBaseConditions(){
		
		//Conditions that will be used in the query
        NestableCondition conditions = new AndCondition();
        
        //Check for account type
        if(cAccount.Type__c == 'Payer'){
			conditions.add(new FieldCondition('Plan_Patient__r.Health_Plan__c').equals(cAccount.Id));	
		}else if(cAccount.Type__c == 'Provider'){
			conditions.add(new FieldCondition('Provider__c').equals(cAccount.Id));	
		}else{
			throw new ApplicationException('Invalid AccountId');
			return null;
		}
        
		//Check for go live date
		if(cAccount.Go_Live_Date__c != null){
            conditions.add(new FieldCondition('DAY_ONLY(CreatedDate)').greaterThanOrEqualTo(cAccount.Go_Live_Date__c));
        }

        //Check for expiration
        if(cAccount.Type__c == 'Provider'){
	        conditions.add(new OrCondition()
	        	.add(new FieldCondition('Expiration_Time__c').greaterThan(DateTime.now()))
	        	.add(new FieldCondition('Expiration_Time__c').equals(null)));
        }
		
		// Exclude simulation orders
		conditions.add(new FieldCondition('RecordTypeId').notEquals([SELECT Id FROM RecordType WHERE Name = 'Simulation Order'].Id));
		// Exclude Draft orders
		conditions.add(new FieldCondition('Status__c').notEquals(OrderModel_CS.STATUS_DRAFT));
        //Exclude orders with Stage as Error - Added by VK on 1/11/2017
        conditions.add(new Fieldcondition('Stage__c').notEquals('Error'));
        
        System.debug('Base Conditions: ' + conditions.toSoql());
        
        return conditions;
	}

	public SoqlBuilder getBaseQuery(){

        return new SoqlBuilder()
            .selectx(objectFields)
            .fromx('Order__c')
            .limitx(10000)
            .wherex(getBaseConditions());
	}

    public SoqlBuilder attachToBaseQueryWithoutSorting(NestableCondition moreConditions) {
        NestableCondition conditions = getBaseConditions();

        conditions.add(moreConditions);
        return new SoqlBuilder()
            .selectx(objectFields)
            .fromx('Order__c')
            .limitx(10000)
            .wherex(conditions);
    }

    public SoqlBuilder attachToBaseQuery(NestableCondition moreConditions){
        SoqlBuilder sb = attachToBaseQueryWithoutSorting(moreConditions);

        sb.orderByx(new OrderBy('CreatedDate').descending())
            .orderByx(new OrderBy('Name').descending());
        return sb;
	}
	
	public Apexpages.Standardsetcontroller getOrdersByFilters(Map<String, String> filterMap){
		AndCondition ac = new AndCondition();
		if(filterMap.keySet().size() > 0){
			for(String field : filterMap.keySet()){
				if(filterMap.get(field) != '' && filterMap.get(field) != null){
					ac.add(new FieldCondition(field).equals(filterMap.get(field)));
				}
			}
			
			try{
				SoqlBuilder sb = attachToBaseQuery(ac);
				return new Apexpages.Standardsetcontroller(Database.getQueryLocator(sb.toSoql()));				
			}
			catch(Exception e){
				System.debug(e.getMessage());	
			}
			return null;
		}else{
			try{
				SoqlBuilder sb = getBaseQuery();
				return new Apexpages.Standardsetcontroller(Database.getQueryLocator(sb.toSoql()));				
			}
			catch(Exception e){
				System.debug('Error Message: ' + e.getMessage());	
			}
			return null;
		}
	}
	
	public Order__c getOrderById(String orderId){
		
		Order__c o;
		try{
			SoqlBuilder sb = attachToBaseQuery(new AndCondition()
											.add(new FieldCondition('Id').equals(orderId))
										);
			o = database.query(sb.toSoql());
		}catch(Exception e){
			//TODO: throw handled exception
			//throw ((e instanceOf ApplicationException) ? e : new ApplicationException('', e));
		}//End catches
		return o;
	}//returnOrder

	public Apexpages.Standardsetcontroller getOrders(){

		try{
			SoqlBuilder sb = getBaseQuery();
			return new Apexpages.Standardsetcontroller(Database.getQueryLocator(sb.toSoql()));				
		}
		catch(Exception e){
			System.debug(e.getMessage());	
		}
	
		return null;
	}
	
	public Apexpages.Standardsetcontroller searchOrders(String searchString){
		try{
			NestableCondition orCondition = new OrCondition()
				.add(new FieldCondition('Authorization_Number__c').likex('%' + searchString + '%'))
				.add(new FieldCondition('Plan_Patient__r.First_Name__c').likex('%' + searchString + '%'))
				.add(new FieldCondition('Plan_Patient__r.Last_Name__c').likex('%' + searchString + '%'))
				.add(new FieldCondition('Plan_Patient__r.Name').likex('%' + searchString + '%'));
			
			SoqlBuilder sb = attachToBaseQuery(orCondition);
			return new Apexpages.Standardsetcontroller(Database.getQueryLocator(sb.toSoql()));
		}catch(Exception e){
			system.debug(e.getMessage());
		}
		return null;
	}
	
	public Apexpages.Standardsetcontroller getRecurringOrders(String orderId){
		
		try{
			SoqlBuilder sb = attachToBaseQuery(new AndCondition()
						.add(new FieldCondition('Original_Order__c',Operator.EQUALS,orderId))
						);
			System.debug('SOQL for recurring orders: ' + sb);
			return new Apexpages.Standardsetcontroller(Database.getQueryLocator(sb.toSoql()));
		}
		catch(Exception e){
			System.debug(e.getMessage());
		}
	
		return null;		
	}
	
	public Apexpages.Standardsetcontroller getNewestOrders(Integer n){
		
		try{
			SoqlBuilder sb = getBaseQuery().orderByx(new List<OrderBy>{
				new OrderBy('CreatedDate').descending()
			}).limitx(n);
			
			return new Apexpages.Standardsetcontroller(Database.getQueryLocator(sb.toSoql()));
		}
		catch(Exception e){
			System.debug(e.getMessage());
		}
	
		return null;
	}
	
	public ApexPages.StandardSetController getNewestOrders(Integer n, Map<String,String> filterMap){

		SoqlBuilder query;

		if(filterMap.keySet().size() > 0){
			
			AndCondition ac = new AndCondition();
			
			// Collect field filters
			for(String field : filterMap.keySet()){
				if(filterMap.get(field) != '' && filterMap.get(field) != null){
					ac.add(new FieldCondition(field).equals(filterMap.get(field)));
				}
			}

			query = attachToBaseQuery(ac);
		}
		else{
			query = getBaseQuery();
		}
		
		query.orderByx(new List<OrderBy>{
				new OrderBy('CreatedDate').descending()
			}).limitx(n);
	
		return new Apexpages.Standardsetcontroller(Database.getQueryLocator(query.toSoql()));		
	}
	
	public Apexpages.Standardsetcontroller getOrdersByEstimatedDeliveryDate(DateTime start, DateTime stop){
		
		try{
			SoqlBuilder sb = attachToBaseQuery(new AndCondition()
									.add(new FieldCondition('Estimated_Delivery_Time__c',Operator.GREATER_THAN_OR_EQUAL_TO,start))
									.add(new FieldCondition('Estimated_Delivery_Time__c',Operator.LESS_THAN_OR_EQUAL_TO,stop))
									);
			
            System.debug(sb.toSoql());
            
            return new Apexpages.Standardsetcontroller(Database.getQueryLocator(sb.toSoql()));
		}
		catch(Exception e){
			System.debug(e.getMessage());	
		}
	
		return null;
	}

	/*
	//TODO: If used, refactor for datetime
	public Apexpages.Standardsetcontroller getOrdersByDeliveryDate(Date start, Date stop){
		
		try{
			SoqlBuilder sb = attachToBaseQuery(new AndCondition()
									.add(new FieldCondition('Delivery_Date_Time__c',Operator.GREATER_THAN_OR_EQUAL_TO,start))
									.add(new FieldCondition('Delivery_Date_Time__c',Operator.LESS_THAN_OR_EQUAL_TO,stop))
									);
			return new Apexpages.Standardsetcontroller(Database.getQueryLocator(sb.toSoql()));	
		}catch(Exception e){
			System.debug(e.getMessage());	
		}
	
		return null;
	}
	*/
	
	public Apexpages.Standardsetcontroller getOrdersByStatus(String status){

		System.debug('Status filter: ' + status);

		try{
			SoqlBuilder sb = attachToBaseQuery(new AndCondition()
									.add(new FieldCondition('Status__c',Operator.LIKEX,status))
									);
			System.debug('getOrdersByStatus query: ' + sb.toSoql(new SoqlOptions().wildcardStringsInLikeOperators()));
			return new Apexpages.Standardsetcontroller(Database.getQueryLocator(sb.toSoql(new SoqlOptions().wildcardStringsInLikeOperators())));
		}catch(Exception e){
			System.debug('getOrdersByStatus: ' + e.getMessage());	
		}
	
		return null;
	}
	
	public Apexpages.Standardsetcontroller getOrdersByStage(String stage){
		
		if(String.isBlank(stage)){
			throw new ApplicationException('The stage cannot be null');
		}
			
		try{
			SoqlBuilder sb = attachToBaseQuery(new AndCondition()
									.add(new FieldCondition('Stage__c',Operator.EQUALS,stage))
									);
									
			System.debug('Query: ' + sb.toSoql());
			
			return new Apexpages.Standardsetcontroller(Database.getQueryLocator(sb.toSoql()));						
		}catch(Exception e){
			//TODO: throw handled exception 
			system.debug('getOrdersByStage: ' + e.getMessage());
			
		}//End trycatch	
		return null;
	}//End getOrdersByStage
	
	public Apexpages.Standardsetcontroller getOrdersByZip(String zip){

		try{
			SoqlBuilder sb = attachToBaseQuery(new AndCondition()
						.add(new FieldCondition('Zip__c',Operator.LIKEX,zip))
						);
			return new Apexpages.Standardsetcontroller(Database.getQueryLocator(sb.toSoql()));
		}
		catch(Exception e){
			System.debug(e.getMessage());	
		}
	
		return null;
	}
	
	public Apexpages.Standardsetcontroller getOrdersByStateCity(String state, String city){

		try{
			SoqlBuilder sb = attachToBaseQuery(new AndCondition()
						.add(new FieldCondition('City__c',Operator.EQUALS,city))
						.add(new FieldCondition('State__c',Operator.EQUALS,state))
						);
			return new Apexpages.Standardsetcontroller(Database.getQueryLocator(sb.toSoql()));	
		}
		catch(Exception e){
			System.debug(e.getMessage());	
		}
	
		return null;
	}
	
	public Apexpages.Standardsetcontroller getOrdersByLocation(String zip, String state, String city, String street, String apt){
		
		try{
			SoqlBuilder sb = attachToBaseQuery(new AndCondition()
						.add(new FieldCondition('Zip__c',Operator.EQUALS,zip))
						.add(new FieldCondition('State__c',Operator.EQUALS,state))
						.add(new FieldCondition('City__c',Operator.EQUALS,city))
						.add(new FieldCondition('Street__c',Operator.EQUALS,street))
						.add(new FieldCondition('Apartment_Number__c',Operator.EQUALS,apt))
						);	
			return new Apexpages.Standardsetcontroller(Database.getQueryLocator(sb.toSoql()));
		}
		catch(Exception e){
			System.debug(e.getMessage());	
		}
	
		return null;
	}

	public virtual Integer returnAggregateOrdersByStatus(String status /*TODO: Time limits? */){
		
		System.debug('returnAggregateOrdersByStatus: Status = ' + status);
		
		Integer orderCount = 0;
		
		//TODO: realistic limit on query
		try{
			orderCount = [	select COUNT() 
							from Order__c
							where Plan_Patient__r.Health_Plan__c = :cAccount.Id and 
								Status__c = :status and
								DAY_ONLY(convertTimezone(CreatedDate)) >= :cAccount.Go_Live_Date__c and
								RecordTypeId != :[SELECT Id FROM RecordType WHERE Name = 'Simulation Order'].Id
							];
		}
		catch(Exception e){
			throw ((e instanceOf ApplicationException) ? e : new ApplicationException('Failed aggregate query for Status: ' + status + ' Count: ' + orderCount,e));
		}
		
		return orderCount;		
	}
	
	public virtual Integer returnAggregateOrdersByStage(String stage /*TODO: Time limits? */){
		
		System.debug('returnAggregateOrdersByStage: Stage = ' + stage);
		
		NestableCondition conditions = getBaseConditions();
		
		conditions.add(new FieldCondition('Stage__c').Equals(stage));
		
		SoqlBuilder query = new SoqlBuilder()
			.selectCount()
			.fromx('Order__c')
			.wherex(conditions);
		
		System.debug('returnAggregateOrdersByStage query: ' + query.toSoql());
		
		return Database.countQuery(query.toSoql());			
	}

	//return a grouped list of Orders by status and map the count
	//TODO: depricate and use Stage grouping instead
	public List<AggregateResult> returnAggregateOrdersGroupedByStatus(){
		
		AggregateResult[] orders;

		try{
			orders = [	select Status__c, COUNT(Id) 
						from Order__c
						where Provider__c = :cAccount.Id and
								DAY_ONLY(convertTimezone(CreatedDate)) >= :cAccount.Go_Live_Date__c and
								RecordTypeId != :[SELECT Id FROM RecordType WHERE Name = 'Simulation Order'].Id
						group by Status__c
						limit 50000
						];
			
			return orders;
		}
		catch(Exception e){
			throw ((e instanceOf ApplicationException) ? e : new ApplicationException('Failed aggregate query for Status__c.',e));
		}
		
		return new List<AggregateResult>();
		
	}//End returnOrdersByStatus
	
	//return a grouped list of Orders by status and map the count
	//TODO: depricate and use Stage grouping instead
	public List<AggregateResult> returnAggregateOrdersGroupedByStatus(Date start, Date stop){
		
		AggregateResult[] orders;
		
		System.debug('Start Date: ' + start + 'Stop Date: ' + stop);
		
		//TODO: realistic limit on query
		try{
			orders = [	select Status__c, COUNT(Id) 
						from Order__c
						where Plan_Patient__r.Health_Plan__c = :cAccount.Id and 
							DAY_ONLY(convertTimezone(CreatedDate)) >= :start and 
							DAY_ONLY(convertTimezone(CreatedDate)) < :stop and
							DAY_ONLY(convertTimezone(CreatedDate)) >= :cAccount.Go_Live_Date__c and
								RecordTypeId != :[SELECT Id FROM RecordType WHERE Name = 'Simulation Order'].Id
						group by Status__c
						limit 50000
						];
		}
		catch(Exception e){
			throw ((e instanceOf ApplicationException) ? e : new ApplicationException('Failed aggregate query for Status__c.',e));
		}
		
		return orders;
	}//End returnOrdersByStatus
	
	//Return a grouped list of Orders by stage and map the count
	/*//Cannot group by Stage__c
	public List<AggregateResult> returnAggregateOrdersGroupedByStage(Date start, Date stop){
		
		AggregateResult[] orders;
		
		System.debug('Start Date: ' + start + 'Stop Date: ' + stop);
		
		try{
			orders = [	select Stage__c, COUNT(Id) 
						from Order__c
						where Plan_Patient__r.Health_Plan__c = :cAccount.Id and 
							DAY_ONLY(CreatedDate) >= :start and DAY_ONLY(CreatedDate) <= :stop
						group by Stage__c
						limit 50000
						];
		}
		catch(Exception e){
			throw ((e instanceOf ApplicationException) ? e : new ApplicationException('Failed aggregate query for Status__c.',e));
		}
		
		return orders;
	}//End returnOrdersByStage
	*/
	
	//TODO: Return orders grouped by a particular user
	//Limit query back period to 3 months
	/*public List<AggregateResult> returnAggregateOrdersGroupedByStatus(Id userId){
		
		AggregateResult[] orders;
		
		System.debug('Start Date: ' + start + 'Stop Date: ' + stop);
		
		
		try{
			orders = [	select Status__c, COUNT(Id) 
						from Order__c
						where DAY_ONLY(CreatedDate) >= :start and DAY_ONLY(CreatedDate) <= :stop
						group by Stage__c
						limit 50000
						];
		}
		catch(Exception e){
			throw ((e instanceOf ApplicationException) ? e : new ApplicationException('Failed aggregate query for Status__c.',e));
		}
		
		return orders;
		
	}//End returnOrdersByStageAndUser	*/	
	//TODO: Depricate - Functionality should've been in ProductServices
	public Apexpages.Standardsetcontroller getOrderLineItems(Order__c o){
		if(o != null && o.Id != null){
			
			try{
				
				SoqlBuilder query = new SoqlBuilder()
					.selectx(lineItemFields)
					.fromx('DME_Line_Item__c')
					.limitx(50000)
					.wherex(new AndCondition()
					.add(new FieldCondition('Order__c').equals(o.Id)));
				
				return new Apexpages.Standardsetcontroller(Database.getQueryLocator(query.toSoql()));
				
			}
			catch(Exception e){
				//TODO: Handle
				System.debug('getOrderLineItems: ' + e.getMessage());
			}		
		}
		else{
			System.debug('getOrderLineItems: Order is null');
		}
		
		return null;
	}
}