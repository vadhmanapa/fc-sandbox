@isTest
private class OrderStatusMetadataWrapperTest_IP {

		/*************************************************************
         ** NOTE: STAGING FOR FUTURE RELEASE - SPRINT 12/12 - BY VK
		**************************************************************/
    
    @isTest static void test_Payor_NAQ() {
		// Implement test code
		Map<String, String> resultList = OrderStatusMetadataWrapper_IP.getPayorNAQ();

		System.assertNotEquals(null, resultList.size(), 'The Metadata was quried and populated');
		// System.assertNotEquals(null, osmWrapper.orderStatusMapCache.size(), 'The Metadata was quried and populated');
	}
	
	@isTest static void test_Provider_NAQ() {
		// Implement test code
		Map<String, String> resultList = OrderStatusMetadataWrapper_IP.getProviderNAQ();

		System.assertNotEquals(null, resultList.size(), 'The Metadata was quried and populated');
	}

	@isTest static void test_All_NAQ() {
		// Implement test code
		Map<String, String> resultList = OrderStatusMetadataWrapper_IP.getAllNAQ();

		System.assertNotEquals(null, resultList.size(), 'The Metadata was quried and populated');
	}

	@isTest static void test_status_display() {
		// Implement test code
		String testString = 'Needs Attention - Documentation Needed';
		String result = OrderStatusMetadataWrapper_IP.statusToDisplay(testString);

		System.assertEquals('Needs Attention - Documentation Needed', result, 'String matched');
	}

	@isTest static void test_All_ActiveStatus() {
		// Implement test code
		Set<String> resultList = OrderStatusMetadataWrapper_IP.allActiveOrderStatus();

		System.assertNotEquals(null, resultList.size(), 'The Metadata was quried and populated');
	}
	
}