/**
 *  @Description Test class for ProductServices_CS
 *  @Author Cloud Software LLC, 
 *  Revision History: 
 *		01/06/2015 - karnold - Changed ProductServices_CS_Test_retrieveDMEProducts and ProductServices_CS_Test_retrieveDMEProductsWildAppendOnly to new method call without categories.
 *			Removed ProductServices_CS_Test_retrieveDMECategoryOptions because retrieveDMECategoryOptions call does not exist anymore
 *			Removed ProductServices_CS_Test_filterByCategory because category filtering no longer exists.
 *			Modified init to remove categories references.
 *
 */
@isTest
private class ProductServices_CS_Test {
	public static List<DME__c> dmeList;
	
	static testmethod void ProductServices_CS_Test_getBaseQuery() {
		init();
		SoqlBuilder baseQuery = ProductServices_CS.getBaseQuery();
	}
	
	static testmethod void ProductServices_CS_Test_attachToBaseQuery() {
		init();
		NestableCondition nc1 = new AndCondition().add(new FieldCondition('Name').likex('%' + 'A00' + '%'));
		SoqlBuilder attachedQuery1 = ProductServices_CS.attachToBaseQuery(nc1);
		ApexPages.Standardsetcontroller setCon1 = new ApexPages.Standardsetcontroller(Database.getQueryLocator(attachedQuery1.toSoql()));
		System.Assert(setCon1.getResultSize() > 0, 'No records like A00');
		
		NestableCondition nc2 = new AndCondition().add(new FieldCondition('Name').likex('%' + 'NonsenseStringFhui3748iwuefDf' + '%'));
		SoqlBuilder attachedQuery2 = ProductServices_CS.attachToBaseQuery(nc2);
		ApexPages.Standardsetcontroller setCon2 = new ApexPages.Standardsetcontroller(Database.getQueryLocator(attachedQuery2.toSoql()));
		System.Assert(setCon2.getResultSize() == 0, 'Filter does not work properly.');
	}
	
	static testmethod void ProductServices_CS_Test_retrieveDMEProducts(){
		init();
		
		List<DME__c> dmeTestList = ProductServices_CS.retrieveDMEProducts('A00');
		system.assertEquals(3, dmeTestList.size());
	}
	
	static testmethod void ProductServices_CS_Test_retrieveDMEProductsWildAppendOnly() {
		init();
		
		List<DME__c> dmeTestList = ProductServices_CS.retrieveDMEProducts('00');
		System.debug('DME List: ' + dmeList);
		system.assertEquals(0, dmeTestList.size());
	}
	
	static testmethod void ProductServices_CS_Test_DLIs(){
		TestDataFactory_CS datafactory = new TestDataFactory_CS();
		List<Order__c> orderList = datafactory.getOrderList();
		
		List<DME_Line_Item__c> lineItems = ProductServices_CS.retrieveDLIsForOrder(orderList[0]);
		system.assertEquals(3, lineItems.size());
	}
	
	static testmethod void ProductServices_CS_Test_filterByProductCode() {
		init();
		SoqlBuilder baseQuery = ProductServices_CS.getBaseQuery();
		
		FieldCondition productFilter = ProductServices_CS.filterByProductCode('A0001');
		String queryWithProductFilter = baseQuery.wherex(productFilter).toSoql();
		ApexPages.Standardsetcontroller setCon = new ApexPages.Standardsetcontroller(Database.getQueryLocator(queryWithProductFilter));
		System.Assert(setCon.getResultSize() == 1, 'Product filter lets in ' + setCon.getResultSize() + ' records.');

	}
	
	static void init(){
		DME_Settings__c dmeSettings = TestDataFactory_CS.generateDMESettings();
		insert dmeSettings;
		
		dmeList = TestDataFactory_CS.generateDMEs(3);
		insert dmeList;
	}
}