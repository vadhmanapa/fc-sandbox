public with sharing class CATTriggerHandler_IP {
    public class MyException extends Exception{}    

    public Map<Id, String> rTypeMap = new Map<Id, String>();

    public CATTriggerHandler_IP() {
        
        for(RecordType rt: [Select Id, Name from RecordType where sObjectType = 'CAT__c']) {
            rTypeMap.put(rt.Id, rt.Name);
        }
        System.debug('Number of record type queried'+rTypeMap.size());
    }

    public CAT_Metric__c createCATM(CAT__c c, String metric, String tatType){
        return new CAT_Metric__C(
            Metric_Type__c = metric,
            CAT__c = c.id,
            Start_Time__c = Datetime.now(),
            TAT_Type__c = tatType);
    }

    public CAT_Metric__c updateCATM(CAT_Metric__c catm){
        catm.End_Time__c = Datetime.now();
        catm.Completed_By__c = UserInfo.getUserId();
        return catm;
    }

    public Map<String, List<CAT_Metric__c>> runCredWorkflow(Map<Id, CAT__c> catMap){
        //RecordType recordTypeMeasurement = [Select Id from RecordType where sObjectType = 'CAT__c' AND Name = 'Measurement' LIMIT 1];
        List<CAT__c> catList = [Select Id, Credentialing_Status__c, On_Hold__c, (Select Id, Metric_Type__c, Start_Time__c, End_Time__c, On_Hold__c, TAT_Type__c from CAT_Metrics__r ORDER BY Start_Time__c DESC) from CAT__c where Id =: catMap.keySet()];
        List<CAT_Metric__c> catmToInsert = new List <CAT_Metric__c>();
        List<CAT_Metric__c> catmToUpdate = new List <CAT_Metric__c>();
        Map<String, Map<String,List<String>>> m = createMap();

        for(CAT__c cL: catList){
            CAT__c oldCAT = cL;
            CAT__c cat = catMap.get(cL.id);
            if (oldCAT.CAT_Metrics__r.size() > 0){
                List<String> l = m.get(oldCAT.Credentialing_Status__c).get('nextState');
                CAT_Metric__c c = oldCAT.CAT_Metrics__r[0];
                System.debug('foobar: '+ cat.On_Hold__c);
                if (cat.Credentialing_Status__c != oldCAT.Credentialing_Status__c){
                    if (cat.On_Hold__c == True){
                        cat.addError('Cannot change status while CAT is on hold');
                        continue;
                    }
                    else if (l != null && l.size() == 0){
                        cat.addError('Wrong Status');
                    }
                    else if (l == null){
                        catmToInsert.add(createCATM(cat,cat.Credentialing_Status__c, m.get(cat.Credentialing_Status__c).get('TATType')[0]));
                    }
                    else{
                        if (cat.Credentialing_Status__c == 'Expired' || cat.Credentialing_Status__c == 'Withdrawn'){
                            if (c != null){
                                catmToUpdate.add(updateCATM(c));
                            }
                        }
                        else{
                            for(String s: l){
                                if (cat.Credentialing_Status__c == s){
                                    if (cat.Credentialing_Status__c != 'Cred complete' && cat.Credentialing_Status__c != 'Cred requirements not met' && cat.Credentialing_Status__c != 'Expired' && cat.Credentialing_Status__c != 'Withdrawn'){
                                        catmToInsert.add(createCATM(cat,cat.Credentialing_Status__c, m.get(cat.Credentialing_Status__c).get('TATType')[0]));
                                    }
                                    if (c != null){
                                        catmToUpdate.add(updateCATM(c));
                                    }
                                }
                            }
                        }
                    }
                    if (catmToInsert.size() == 0 && catmToUpdate.size() == 0){
                        cat.addError(' Acceptable next status: ' + String.join(l, ', '));
                    }
                    
                }
                else{
                    if (cat.On_Hold__c == True && c.TAT_Type__c != 'Active'){
                        cat.addError('Cannot put a CAT on hold in this status');
                    }
                    else if (cat.On_Hold__c == True && oldCAT.On_Hold__c == False){
                        c.On_Hold__c = True;
                        catmToUpdate.add(c);
                    }
                    else if (cat.On_Hold__c == False && oldCAT.On_Hold__c == True){
                        c.On_Hold__c = False;
                        catmToUpdate.add(c);
                    }
                }
            }
            else{
                try{
                    catmToInsert.add(createCATM(cat,cat.Credentialing_Status__c, m.get(cat.Credentialing_Status__c).get('TATType')[0]));
                }
                catch (Exception e){
                    System.debug('Got error: ' + e);
                }
            }
            
        }
        
        //insert catmToInsert;
        //update catmToUpdate;
        return new Map<String, List<CAT_Metric__c>>{'Insert' => catmToInsert, 'Update' => catmToUpdate};
    }

    public Map<String, Map<String,List<String>>> createMap(){
        Map<String, Map<String,List<String>>> m = new Map <String, Map<String,List<String>>>{
            'Start' => new Map<String, List<String>>{'nextState'=> new List<String>{'Application received'}, 'TATType' => new List<String>{'Wait'}},
            'Application received' => new Map<String, List<String>>{'nextState'=> new List<String>{'Ready for triage'}, 'TATType' => new List<String>{'Wait'}},
            'Ready for triage' => new Map<String, List<String>>{'nextState'=> new List<String>{'In triage'}, 'TATType' => new List<String>{'Wait'}},
            'In triage' => new Map<String, List<String>>{'nextState'=> new List<String>{'Ready for CVO Prep','In CVO Prep','1st document submission form sent', 'Ready for final checklist', 'In final checklist'}, 'TATType' => new List<String>{'Active'}},
            '1st document submission form sent' => new Map<String, List<String>>{'nextState'=> new List<String>{'2nd document submission form sent', 'Ready for CVO Prep', 'In CVO Prep', 'Provider responded', 'Ready for final checklist', 'In final checklist', 'Awaiting document submission'}, 'TATType' => new List<String>{'Provider'}},
            '2nd document submission form sent' => new Map<String, List<String>>{'nextState'=> new List<String>{'3rd document submission form sent', 'Ready for CVO Prep', 'In CVO Prep', 'Provider responded', 'Ready for final checklist', 'In final checklist', 'Awaiting document submission'}, 'TATType' => new List<String>{'Provider'}},
            '3rd document submission form sent' => new Map<String, List<String>>{'nextState'=> new List<String>{'Escalated to ND', 'Ready for CVO Prep', 'In CVO Prep', 'Provider responded', 'Ready for final checklist', 'In final checklist', 'Awaiting document submission'}, 'TATType' => new List<String>{'Provider'}},
            'Provider responded' => new Map<String, List<String>>{'nextState'=> new List<String>{'Awaiting document submission','2nd document submission form sent', '3rd document submission form sent', 'Ready for CVO Prep', 'In CVO Prep', 'Ready for final checklist', 'In final checklist'}, 'TATType' => new List<String>{'Wait'}},
            'Awaiting document submission' => new Map<String, List<String>>{'nextState'=> new List<String>{'2nd document submission form sent', '3rd document submission form sent', 'Ready for CVO Prep', 'In CVO Prep', 'Provider responded', 'Escalated to ND', 'Ready for final checklist', 'In final checklist'}, 'TATType' => new List<String>{'Provider'}},
            'Escalated to ND' => new Map<String, List<String>>{'nextState'=> new List<String>{'Ready for CVO Prep', 'In CVO Prep', 'Cred requirements not met', 'Ready for final checklist', 'In final checklist'}, 'TATType' => new List<String>{'Wait'}},
            'Ready for CVO prep' => new Map<String, List<String>>{'nextState'=> new List<String>{'In CVO Prep'}, 'TATType' => new List<String>{'Wait'}},
            'In CVO prep' => new Map<String, List<String>>{'nextState'=> new List<String>{'Sent to CVO'}, 'TATType' => new List<String>{'Active'}},
            'Sent to CVO' => new Map<String, List<String>>{'nextState'=> new List<String>{'Ready for final checklist'}, 'TATType' => new List<String>{'CVO'}},
            //'Returned from CVO' => new Map<String, List<String>>{'nextState'=> new List<String>{'Verification issues', 'Ready for final checklist', 'In final checklist'}, 'TATType' => new List<String>{'Wait'}},
            'Ready for final checklist' => new Map<String, List<String>>{'nextState'=> new List<String>{'In final checklist'}, 'TATType' => new List<String>{'Wait'}},
            'In final checklist' => new Map<String, List<String>>{'nextState'=> new List<String>{'Ready for checklist QC', 'Ready for data entry', 'Verification issues'}, 'TATType' => new List<String>{'Active'}},
            'Verification issues' => new Map<String, List<String>>{'nextState'=> new List<String>{'Going to committee', 'Ready for checklist QC', 'Ready for data entry'}, 'TATType' => new List<String>{'Provider'}},
            'Going to committee' => new Map<String, List<String>>{'nextState'=> new List<String>{'Cred requirements not met', 'Ready for checklist QC', 'Ready for data entry'}, 'TATType' => new List<String>{'Wait'}},
            'Ready for checklist QC' => new Map<String, List<String>>{'nextState'=> new List<String>{'In checklist QC'}, 'TATType' => new List<String>{'Wait'}},
            'In checklist QC' => new Map<String, List<String>>{'nextState'=> new List<String>{'Ready for data entry', 'Ready for checklist edits', 'Verification issues'}, 'TATType' => new List<String>{'Active'}},
            'Ready for checklist edits' => new Map<String, List<String>>{'nextState'=> new List<String>{'In checklist edits'}, 'TATType' => new List<String>{'Wait'}},
            'In checklist edits' => new Map<String, List<String>>{'nextState'=> new List<String>{'Ready for data entry'}, 'TATType' => new List<String>{'Active'}},
            'Ready for data entry' => new Map<String, List<String>>{'nextState'=> new List<String>{'In data entry'}, 'TATType' => new List<String>{'Wait'}},
            'In data entry' => new Map<String, List<String>>{'nextState'=> new List<String>{'Ready for data QC'}, 'TATType' => new List<String>{'Active'}},
            'Ready for data QC' => new Map<String, List<String>>{'nextState'=> new List<String>{'In data QC'}, 'TATType' => new List<String>{'Wait'}},
            'In data QC' => new Map<String, List<String>>{'nextState'=> new List<String>{'Ready for data edits', 'Ready for sign off'}, 'TATType' => new List<String>{'Active'}},
            'Ready for data edits' => new Map<String, List<String>>{'nextState'=> new List<String>{'In data edits'}, 'TATType' => new List<String>{'Wait'}},
            'In data edits' => new Map<String, List<String>>{'nextState'=> new List<String>{'Ready for sign off'}, 'TATType' => new List<String>{'Active'}},
            'Ready for sign off' => new Map<String, List<String>>{'nextState'=> new List<String>{'Sign off complete'}, 'TATType' => new List<String>{'Wait'}},
            'Sign off complete' => new Map<String, List<String>>{'nextState'=> new List<String>{'Cred complete'}, 'TATType' => new List<String>{'Wait'}},
            'Cred complete' => new Map<String, List<String>>{'nextState'=> new List<String>{'Cred complete'}, 'TATType' => new List<String>{'Wait'}},
            'Cred requirements not met' => new Map<String, List<String>>{'nextState'=> new List<String>{'Cred requirements not met'}, 'TATType' => new List<String>{'Wait'}},
            'Expired' => new Map<String, List<String>>{'nextState'=> new List<String>{'Expired'}, 'TATType' => new List<String>{'Wait'}},
            'Withdrawn' => new Map<String, List<String>>{'nextState'=> new List<String>{'Withdrawn'}, 'TATType' => new List<String>{'Wait'}}
        };
        return m;
    }


    public void addCATM(List<CAT__c> triggerValues){
        List<CAT_Metric__c> catmToInsert = new List <CAT_Metric__c>();
        Map<String, Map<String,List<String>>> m = createMap();
        for (CAT__c cat: triggerValues){
            if(rTypeMap.get(cat.RecordTypeId) == 'Credentialing Application' && cat.Credentialing_Status__c != null){
                catmToInsert.add(createCATM(cat,cat.Credentialing_Status__c, m.get(cat.Credentialing_Status__c).get('TATType')[0]));    
            }
        }
        insert catmToInsert;
    }

    public void fireEmails(List<CAT__c> triggerValues, Map<Id, CAT__c> triggerOldMap){
        OrgWideEmailAddress owea = [Select Id from orgwideemailaddress where address = 'credentialing@accessintegra.com'];
        for (CAT__c cat : triggerValues){
            if (cat.Case_Created__c == True && triggerOldMap.get(cat.Id).Case_Created__c == False){
                List<Case> c = [select Id, ContactId  from Case where CAT__c =: cat.Id and Subject like '%Missing Documents%' order by createddate desc LIMIT 1];
                //String email = [select email from contact where id=:c[0].contactid][0].email;
                List<Attachment> a = [select Id, Name, Body from Attachment where parentid=: cat.Id order by createddate desc limit 1];
                if (!c.isEmpty()){
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

                    // Send as Credentialing@accessintegra.com
                    mail.setOrgWideEmailAddressId(owea.Id);
                    // Strings to hold the email addresses to which you are sending the email.
                    //String[] toAddresses = new String[] {email}; 

                    // Assign the addresses for the To and CC lists to the mail object.
                    //mail.setToAddresses(toAddresses);

                    // Specify the address used when the recipients reply to the email. 
                    mail.setReplyTo('credentialing@accessintegra.com');

                    // Specify the name used as the display name.
                    //mail.setSenderDisplayName('Integra Partners Credentialing Team');

                    // Specify the subject line for your email address.
                    //mail.setSubject('New Case Created : ' + c[0].Id);

                    // Set to True if you want to BCC yourself on the email.
                    mail.setBccSender(False);

                    // Optionally append the salesforce.com email signature to the email.
                    // The email address of the user executing the Apex Code will be used.
                    mail.setUseSignature(false);

                    // Relating it to the case and contact
                    mail.setTargetObjectId(c[0].ContactId);
                    mail.setWhatId(c[0].Id);

                    // Add the attachment
                    Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                    efa.setFileName(a[0].Name);
                    efa.setBody(a[0].Body);
                    mail.setFileAttachments(new Messaging.EmailFileAttachment[]{efa});


                    // Email Template
                    mail.setTemplateId([Select id from EmailTemplate where name = 'Credentialing Case Missing Docs' LIMIT 1].id);
                    //mail.setTargetObjectId(c[0].id);
                    // Send the email you have created.
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });


                    System.debug('Sent email');
                }

            }
        }

    }
    public Map<String, List<CAT_Metric__c>> runTrigger(List<CAT__c> triggerValues, Map<Id, CAT__c> triggerOldMap, Map<Id, CAT__c> triggerNewMap){
        Map<String, List<CAT_Metric__c>> catmToReturn = new Map<String, List<CAT_Metric__c>>();
        Set<Id> getAccountIds = new Set<Id>();
        Map<Id, Account> accMap = new Map<Id, Account>();
        Map<Id, String> getUserInfo = new Map<Id, String>();
        List<Case> createCase = new List<Case>(); // to hold all the new cases
        List<Provider_Location__c> accountPl = new List<Provider_Location__c>();
        RecordType rtCase = [Select Id, Name from RecordType where sObjectType = 'Case' AND Name = 'Credentialing' LIMIT 1];
        String commentBuild = '';
        BusinessHours bh = [SELECT Id, MondayStartTime, MondayEndTime  FROM BusinessHours WHERE Name='Credentialing'];
        List<Round_Robin__c> roundRobinList = [select Id, last_turn__c, User__c from round_robin__C where active__c = True and function__c = 'Credentialing CATs' order by last_turn__c asc];
        Map<Id, Round_Robin__c> rrToUpdate = new Map<Id, Round_Robin__c>();
        Map<Id, CAT__c> catsToRun = new Map<Id, CAT__c>();
        CATMetricHandler CMH = new CATMetricHandler();

        for(User u : [Select Id, Name from User]){
            getUserInfo.put(u.Id, u.Name);
        }
        
        for(CAT__c c: triggerValues) {
            getAccountIds.add(c.Account__c);
        }

        if(!getAccountIds.isEmpty()) {
            
            for(Account a : [Select Id, 
                                    Name,
                                    Status__c,
                                    Credentialing_Status__c,
                                    Initial_Credential_Date__c,
                                    Application_Status_Comments__c,
                                    Activation_Date__c,
                                    Current_Credential_Date__c,
                                    Credential_Date_Expiration__c,
                                    Status_Reason__c,
                                    Close_Status_Reason__c,
                                    Close_Term_Date__c,
                                    Application_Received__c,
                                    Recred_Application_Received__c,
                                    Missing_Info_Contact_Date__c,
                                    Credentialing_Completed__c,
                                    Recredentialing__c,
                                    Recredentialed_by__c,
                                    Credentialed_by__c,
                                    Application_Removed_Date__c,
                                    Application_Completed_Date__c,
                                    Demographic_Changes__c,
                                    Demographic_Change_Last__c,
                                    Account_ReOpened_On__c,
                                    (Select Id, Store_Status__c, Store_Closure_Date__c from Provider_Locations__r)
                                    FROM Account WHERE Id=: getAccountIds]){
                accMap.put(a.Id, a);
            }
            
            Integer i = 0;
            for(CAT__c cat: triggerValues) {
                // get the record type and decide the action
                Datetime dt = System.now();

                if(cat.Application_Status_Comments__c != null){
                    
                    commentBuild = ''; // always start with empty text
                    commentBuild = dt.format('MM/dd/yyyy HH:mm:ss','America/New_York')+ ' - '; // first get the name
                    commentBuild+= getUserInfo.get(cat.LastModifiedById)+ ' - '+ cat.Application_Status_Comments__c;// add date and comments
                    accMap.get(cat.Account__c).Application_Status_Comments__c = commentBuild;
                }
               
                //Case 1: Intial Credentialing

                if(rTypeMap.get(cat.RecordTypeId) == 'Credentialing Application') {
                       //Round robin code to assign a CAT to a rep
                    if (cat.Assigned_To__c == null && roundRobinList.size()>0){
                        Round_Robin__c rr = roundRobinList[Math.mod(i,roundRobinList.size())];
                        cat.Assigned_To__c = rr.User__c;
                        rr.Last_Turn__c = Datetime.now();
                        if (rrToUpdate.containsKey(rr.Id)){
                            rrToUpdate.remove(rr.Id);    
                        }
                        rrToUpdate.put(rr.Id, rr);
                    }

                    try{
                        //List of values to be fed to the method
                        catsToRun.put(cat.Id, triggerNewMap.get(cat.id));   
                    }
                    catch (System.NullPointerException e){
                        System.debug('Not a problem: ' + e);
                        //if (cat.Credentialing_Status__c != 'Ready for triage'){
                        //  cat.addError('Starting status is: Application received');
                        //}
                    }
                    
                    accMap.get(cat.Account__c).Credentialing_Status__c = cat.Credentialing_Status__c;
                    accMap.get(cat.Account__c).Application_Completed_Date__c = (cat.Application_Completed_Date__c).date();
                    accMap.get(cat.Account__c).Application_Received__c = (cat.Application_Completed_Date__c).date();


                    if (cat.Credentialing_Status__c == 'Sign off complete' && cat.Sign_Off_Date__c == null){
                        cat.Sign_Off_Date__c = Datetime.now();
                    }
                    else if ((cat.Credentialing_Status__c == 'Cred complete' || cat.Credentialing_Status__c == 'Expired' || cat.Credentialing_Status__c == 'Cred requirements not met' || cat.Credentialing_Status__c == 'Withdrawn') && cat.CAT_Completed_Date__c == null){
                        cat.CAT_Completed_Date__c = Datetime.now(); 
                    }

                    if (cat.Sign_Off_Date__c != null){
                        cat.Next_Credentialing_Date__c = cat.Sign_Off_Date__c.addYears(3);
                    }
                        
                    if (cat.CAT_Completed_Date__c != null && cat.Application_Completed_Date__c != null){
                        cat.App_TAT__c = CMH.calculateTAT(bh, cat.Application_Completed_Date__c, cat.CAT_Completed_Date__c, 'BD');
                    }


                    if (cat.Application_Type__c == 'Initial Credentialing'){
                        if (cat.Sign_Off_Date__c == null && accMap.get(cat.Account__c).Status__c != 'Active'){
                            accMap.get(cat.Account__c).Status__c = 'Pending Credentialing';
                        }
                        
                        if (cat.CAT_Completed_Date__c != null && cat.Credentialing_Status__c == 'Cred complete' && cat.Sign_Off_Date__c != null){
                            // Update inital credentialing fields on the account
                            accMap.get(cat.Account__c).Initial_Credential_Date__c = cat.Sign_Off_Date__c.date();
                            accMap.get(cat.Account__c).Credentialing_Completed__c = cat.Sign_Off_Date__c.date();
                            accMap.get(cat.Account__c).Credential_Date_Expiration__c = cat.Next_Credentialing_Date__c.date();
                            accMap.get(cat.Account__c).Credentialed_by__c = cat.Completed_By__c;
                            accMap.get(cat.Account__c).Current_Credential_Date__c = cat.Sign_Off_Date__c.date(); // Last Credential Date will be Credential completed date in this case  
                            
                            // Update the Account Reopened field on the account in case the field was populated
                            accMap.get(cat.Account__c).Account_ReOpened_On__c = cat.ReOpened_On__c;

                            // Clear the closed fields
                            accMap.get(cat.Account__c).Close_Status_Reason__c = null;
                            accMap.get(cat.Account__c).Status_Reason__c = null;
                            accMap.get(cat.Account__c).Close_Term_Date__c = null;
                            accMap.get(cat.Account__c).Application_Removed_Date__c = null;

                        }
                        
                    }
                    else if (cat.Application_Type__c == 'Recredentialing'){
                        accMap.get(cat.Account__c).Recred_Application_Received__c = (cat.Application_Completed_Date__c).date();
                        if (cat.CAT_Completed_Date__c != null && cat.Credentialing_Status__c == 'Cred complete' && cat.Sign_Off_Date__c != null){
                            accMap.get(cat.Account__c).Current_Credential_Date__c = cat.Sign_Off_Date__c.date();
                            accMap.get(cat.Account__c). Credential_Date_Expiration__c = cat.Next_Credentialing_Date__c.date();
                            accMap.get(cat.Account__c).Recredentialed_by__c = cat.Completed_By__c;
                        }
                    }


                    if(cat.Credentialing_Status__c == '1st document submission form sent' && cat.Case_Created__c != true){
                        List<Attachment> a = [select Id from Attachment where parentid=: cat.Id order by createddate desc limit 1];
                        if (a.isEmpty()){
                            cat.addError('Please attach the Document Submission Form before saving');
                        }
                        else{
                            accMap.get(cat.Account__c).Missing_Documents__c = cat.Missing_Documentation__c;
                            // Create a copy of CAT object
                            createCase.add(new Case(CAT__c = cat.Id, 
                                                Reason = 'Missing Documents', 
                                                Status = 'New', 
                                                Missing_Documentation__c = cat.Missing_Documentation__c,
                                                Subject = cat.Name+' - '+' Missing Documents ',
                                                Description = 'Missing Document Request sent to '+ accMap.get(cat.Account__c).Name,
                                                AccountId = cat.Account__c,
                                                RecordTypeId = rtCase.Id,
                                                ContactId = cat.Provider_Contact__c));
                            cat.Case_Created__c = true;
                        }
                        

                    }
                }

                //Case 2: Recredentialing

                //Case 3: Close Account

                else if(rTypeMap.get(cat.recordTypeId) == 'Close Account') {
                    
                    accMap.get(cat.Account__c).Close_Status_Reason__c = cat.Closed_Status_Reason__c;
                    accMap.get(cat.Account__c).Status_Reason__c = cat.Closed_Status_Explanation__c;
                    accMap.get(cat.Account__c).Close_Term_Date__c = cat.Closed_Term_Date__c;
                    accMap.get(cat.Account__c).Application_Removed_Date__c = cat.Application_Removed_Date__c;
                    accMap.get(cat.Account__c).Status__c = cat.Closed_Status__c;
                    accountPl = accMap.get(cat.Account__c).Provider_Locations__r;
                    if(!accountPl.isEmpty()){
                        for (Provider_Location__c p : accountPl){
                            p.Store_Status__c = 'Closed';
                            p.Store_Closure_Date__c = cat.Closed_Term_Date__c;
                        }
                    }
                }

                //Case 4: Re-Open Account

                //Case 5: Roster Update

                else if(rTypeMap.get(cat.RecordTypeId) == 'Demographic Update') {
                    
                    accMap.get(cat.Account__c).Missing_Info_Contact_Date__c = cat.Missing_Info_Contact_Date__c;
                    accMap.get(cat.Account__c).Demographic_Changes__c = cat.Update_Category__c;
                    accMap.get(cat.Account__c).Demographic_Change_Last__c = System.today();

                }
                
               // Case 6: Not Meeting Credential Requirements
               
                //else if(rTypeMap.get(cat.RecordTypeId) == 'Not Meeting Requirements'){
                //    accMap.get(cat.Account__c).Status__c = 'Not Meet Credentialing Requirements';
                //    accMap.get(cat.Account__c).Not_Meeting_Cred_Requirement_Reason__c = cat.Not_Meeting_Cred_Requirement_Reason__c;
                //} 

                i++;      

            }

            try{
                if(!createCase.isEmpty()){
                    Database.insert(createCase);
                }
            }
            catch(DmlException e){
                System.debug('Error'+e.getMessage());
            }
            
            catmToReturn = runCredWorkflow(catsToRun);
            
            update catmToReturn.get('Update');
            insert catmToReturn.get('Insert');
            update rrToUpdate.values();
            update accMap.values();
            update accountPl;
            
        }
        return catmToReturn;
    }

}