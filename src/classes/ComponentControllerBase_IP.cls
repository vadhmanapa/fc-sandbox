public with sharing virtual class ComponentControllerBase_IP {

 // Identify the component with key and retrieve the controller
	public String key{ get;
	    set{
	      if(value != null){
		        Key  = value;
		        if(pageController != null)
		           pageController.setComponentControllerMap(Key, this);
	        }
	    }
	}

	public PageControllerBase_IP pageController { get; 
	    set {
          if (value != null) {
            	pageController = value;
            	pageController.setComponentController(this);
          	}
	    }
	}
    
    // public String selectedCallAboutType {get;set;}
    
}