public without sharing class DMELineItemModel_CS extends AbstractSObjectModel_CS {

	public DME_Line_Item__c instance {
		get{
			if(instance == null){ instance = (DME_Line_Item__c)record; }
			return instance;
		}
		set;
	}
	
	public static List<DME_Line_Item__c> getRecords(List<DMELineItemModel_CS> models){
				
		List<DME_Line_Item__c> objects = new List<DME_Line_Item__c>();
		
		for(DMELineItemModel_CS m : models){
			objects.add(m.instance);
		}
		
		return objects;
	}
	
	public static List<DMELineItemModel_CS> getModels(List<DME_Line_Item__c> records){
		List<DMELineItemModel_CS> models = new List<DMELineItemModel_CS>();
		
		for(DME_Line_Item__c r : records){
			models.add(new DMELineItemModel_CS(r));
		}
		
		return models;
	}

	// Checks that DME Line Item has a valid quantity. Will return true if DME Line Item has a Quantity. Otherwise will return false
	public Boolean isValidDME{
		get{
			if(instance.Quantity__c > 0) {
				return true;
			} else {
				return false;
			}
		}
		set;
	}
	
	/******* Temporary Fields for info on DME *******/
	public String itemCode {get;set;}
	public String productName {get;set;}
	
	/******* Constructors and Init ********/
	public DMELineItemModel_CS(DME_Line_Item__c lineItem){
		super(lineItem);
	}
}