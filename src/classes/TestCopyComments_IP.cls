@isTest
private class TestCopyComments_IP {
      
      static testMethod void TestCopyComments() {
            // Implement test code
        // TO DO: implement unit test
        // create test user
     User testUser = new User();
      testUser.FirstName = 'test';
      testUser.LastName = 'check';
      testUser.Username = 'testcheck@IntegraTest.com';
      testUser.Email = 'test@test.com';
      testUser.Alias = 'test';
      testUser.CommunityNickname = 'testforemail';
      testUser.TimeZoneSidKey = 'America/Los_Angeles';
      testUser.EmailEncodingKey = 'ISO-8859-1';
      testUser.ProfileId = '00e300000028QgNAAU';
      testUser.LanguageLocaleKey = 'en_US';
      testUser.LocaleSidKey = 'en_US';
      insert testUser;
      String testUserId = testUser.Id; 

      // create an objective

      Objective__c testObj = new Objective__c(Name = 'test objective', Department__c = 'Claims');
      insert testObj;
      String objId = testObj.Id;

      // create an initiative

      Initiative__c testInitiative = new Initiative__c ( Objective__c = objId, Comments_Update__c = 'test insert');
      insert testInitiative;
      String iniId = testInitiative.Id;

      // create a note

      Notes__c addNote = new Notes__c (Initiative__c = iniId, Note__c = 'test note');
      insert addNote;

      List <Initiative__c> checkSuccess = [Select Id, Comments_Update__c from Initiative__c where Id =: iniId];

      System.assertEquals(1, checkSuccess.size(), 'The initiative check is success');
      System.assertNotEquals('test insert', checkSuccess[0].Comments_Update__c, 'The comments section was updated');

       // test null comments

      Initiative__c testNullInitiative = new Initiative__c ( Objective__c = objId, Comments_Update__c = '');
      insert testNullInitiative;
      String iniNullId = testNullInitiative.Id;

      Notes__c addNullNote = new Notes__c (Initiative__c = iniNullId, Note__c = 'test null');
      insert addNullNote;

      List <Initiative__c> checkNullSuccess = [Select Id, Comments_Update__c from Initiative__c where Id =: iniId];

      System.assertEquals(1, checkNullSuccess.size(), 'The initiative check is success');
      System.assertNotEquals('', checkNullSuccess[0].Comments_Update__c, 'The comments section was updated');
      }
      
}