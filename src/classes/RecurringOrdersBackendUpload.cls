public class RecurringOrdersBackendUpload {

    public Map<Id,List <DME_Line_Item__c>> orderLineItems; // collect parent order and it's related DME Line Items for recurring orders

    public void RecurringOrdersBackendUpload (List <Order__c> newOrders)
    {
       // List <Order__c> newOrderList = new List <Order__c> ();
       orderLineItems = new Map<Id,List <DME_Line_Item__c>>();
       List <Order__c> finalList = new List <Order__c>();
       Set<Id> orderIds = new Set<Id>();

        for (Order__c O : newOrders)
        {
            orderIds.add(O.Id);

            /*if(O.Backend_Uploaded__c == True && O.Recurring__c == True && O.Recurring_Frequency__c != Null)
            {
        		newOrderList.add(O);
            }*/
        }

        List<Order__c> ordersWithLineItems = [SELECT Id, (SELECT Id, DME__c, Order__c, Quantity__c, Authorization_Start_Date__c, Authorization_End_Date__c, Recurring_Frequency__c, RR_NU__c,
                                                          Authorization_Number__c FROM DMEs__r) FROM Order__c WHERE Id IN: orderIds];
        if(!ordersWithLineItems.isEmpty()){
            for(Order__c o : ordersWithLineItems){
                orderLineItems.put(o.Id, o.DMEs__r);
            }
        }

        System.debug('Line Item size'+orderLineItems.size());
        System.debug('Line Item size'+orderLineItems);

        if (newOrders.size() > 0)
        {

            for(Order__c ord : newOrders){

                finalList.addAll(spawnRecurringOrders(ord));

                ord.Backend_Uploaded__c = false;
            }
        }

        System.debug('Final List size'+finalList.size());

        try{

            insert finalList;

        }catch(Exception e){

            System.debug(LoggingLevel.Error, e);

        }

        spawnRecurringLineItems(finalList);
        //update newOrderList;

    }

    public List<Order__c> spawnRecurringOrders(Order__c newOrder){


        //No matter what, we need to dump the old recurring orders after potential click "back"
        List<Order__c> recurringOrders = new List<Order__c>();

        //Only proceed if all recurring information is given
        if(	newOrder.Recurring__c && String.isNotBlank(newOrder.Recurring_Frequency__c) &&
        	newOrder.Authorization_Start_Date__c != null && newOrder.Authorization_End_Date__c != null &&
        	newOrder.Delivery_Window_Begin__c != null && newOrder.Delivery_Window_End__c != null)
        {

        	//Seconds are used to span the length of the delivery window
            Integer deliveryWindowSeconds = ((newOrder.Delivery_Window_End__c.getTime() -
                        newOrder.Delivery_Window_Begin__c.getTime())/1000).intValue();

            /*if(newOrder.Recurring_Frequency__c == 'daily'){
                System.debug('Creating recurring orders daily.');

                //Start from the next increment of the delivery date
                Date counter = newOrder.Delivery_Window_Begin__c.addDays(1).date();

                //Increment from the delivery date and only add orders that fall inside the window
                while(counter <= newOrder.Authorization_End_Date__c){

                	if(counter > newOrder.Authorization_Start_Date__c && counter <= newOrder.Authorization_End_Date__c){
                		System.debug('Creating order for: ' + counter);

	                    //Create the recurring order and set relevant parameters
	                    Order__c newRecurringOrder = newOrder.clone(false,true,true,false);
	                    newRecurringOrder.Delivery_Window_Begin__c = DateTime.newInstance(counter,newRecurringOrder.Delivery_Window_Begin__c.time());
	                    newRecurringOrder.Delivery_Window_End__c = newRecurringOrder.Delivery_Window_Begin__c.addSeconds(deliveryWindowSeconds);

	                    System.debug('New Recurring Order: ' + newRecurringOrder);

	                    //Add to list
	                    recurringOrders.add(newRecurringOrder);
                	}
                	else{
                		System.debug('Date: ' + counter + ' is outside the window');
                	}

                    //Increment by one day
                    counter = counter.addDays(1);
                }
            }
            else if(newOrder.Recurring_Frequency__c == 'weekly'){
                System.debug('Creating recurring orders weekly.');

                //Start from the next increment of the delivery date
                Date counter = newOrder.Delivery_Window_Begin__c.addDays(7).date();

                //Increment from the delivery date and only add orders that fall inside the window
                while(counter <= newOrder.Authorization_End_Date__c){

                	if(counter > newOrder.Authorization_Start_Date__c && counter <= newOrder.Authorization_End_Date__c){
                		System.debug('Creating order for: ' + counter);

	                    //Create the recurring order and set relevant parameters
	                    Order__c newRecurringOrder = newOrder.clone(false,true,true,false);
	                    newRecurringOrder.Delivery_Window_Begin__c = DateTime.newInstance(counter,newRecurringOrder.Delivery_Window_Begin__c.time());
	                    newRecurringOrder.Delivery_Window_End__c = newRecurringOrder.Delivery_Window_Begin__c.addSeconds(deliveryWindowSeconds);

	                    System.debug('New Recurring Order: ' + newRecurringOrder);

	                    //Add to list
	                    recurringOrders.add(newRecurringOrder);
                	}
                	else{
                		System.debug('Date: ' + counter + ' is outside the window');
                	}

                    //Increment by one week
                    counter = counter.addDays(7);
                }
            }*/
            if(newOrder.Recurring_Frequency__c == 'monthly'){
                System.debug('Creating recurring orders monthly.');

                //Start from the next increment of the delivery date
                Date counter = newOrder.Delivery_Window_Begin__c.addMonths(1).date();

                //Increment from the delivery date and only add orders that fall inside the window
                while(counter <= newOrder.Authorization_End_Date__c){

                	if(counter > newOrder.Authorization_Start_Date__c && counter <= newOrder.Authorization_End_Date__c){
                		System.debug('Creating order for: ' + counter);

	                    //Create the recurring order and set relevant parameters
	                    Order__c newRecurringOrder = newOrder.clone(false,true,true,false);
	                    newRecurringOrder.Delivery_Window_Begin__c = DateTime.newInstance(counter,newRecurringOrder.Delivery_Window_Begin__c.time());
	                    newRecurringOrder.Delivery_Window_End__c = newRecurringOrder.Delivery_Window_Begin__c.addSeconds(deliveryWindowSeconds);
                        newRecurringOrder.Backend_Uploaded__c = false;
                        newRecurringOrder.Status__c = 'Recurring';
                        newRecurringOrder.Original_Order__c = newOrder.Id;
                        newRecurringOrder.Provider__c = null;
                        newRecurringOrder.Accepted_By__c = null;
                        newRecurringOrder.Accepted_Date__c = null;
                        newRecurringOrder.Estimated_Delivery_Time__c = null;
                        newRecurringOrder.Message_History_JSON__c = null;
                        newRecurringOrder.Order_History_JSON__c = null;
                        newRecurringOrder.Integra_Comments__c = null;
                        newRecurringOrder.Delivery_Date_Time__c = null;
                        

	                    System.debug('New Recurring Order: ' + newRecurringOrder);

	                    //Add to list
	                    recurringOrders.add(newRecurringOrder);
                	}
                	else{
                		System.debug('Date: ' + counter + ' is outside the window');
                	}

                    //Increment by one month
                    counter = counter.addMonths(1);
                }
            }
            else{
                System.debug('spawnRecurringOrders: Error, frequency "' + newOrder.Recurring_Frequency__c + '" not supported.');
                return new List<Order__c>();
            }

        }
        return recurringOrders;

        /*if(recurringOrders.size() > 0){

            //Make the final assignments for each recurring order
            for(Order__c o : recurringOrders){
                o.Plan_Patient__c = thisPatient.Id;
                o.Payer__c = portalUser.instance.Entity__c;
                o.Original_Order__c = newOrder.Id;
                o.Status__c = 'Recurring';
            }

            //insert recurringOrders;
    }*/
   }
   public void spawnRecurringLineItems(List<Order__c> recurringOrds){
     List<DME_Line_Item__c> recurringLineItems = new List<DME_Line_Item__c>();

            //For each of the inserted orders
            for(Integer i = 0; i < recurringOrds.size(); i++){

                System.debug('submitOrder: Inserted recurring order ' + recurringOrds[i].Id);

                //Get a copy of the line items
                //List<DME_Line_Item__c> lineItemCopies = orderLineItems.get(recurringOrds[i].Original_Order__c);

                /*for(DME_Line_Item__c dli : lineItemCopies){
                    dli.Order__c = recurringOrds[i].Id;
                }*/
                for(DME_Line_Item__c dli : orderLineItems.get(recurringOrds[i].Original_Order__c)){
                    System.debug('Line Item');
                   recurringLineItems.add(new DME_Line_Item__c(DME__c = dli.DME__c, Order__c = recurringOrds[i].Id, Quantity__c = dli.Quantity__c, Authorization_Start_Date__c = dli.Authorization_Start_Date__c,
                                                              Authorization_End_Date__c = dli.Authorization_End_Date__c, Recurring_Frequency__c = dli.Recurring_Frequency__c, RR_NU__c = dli.RR_NU__c,
                                                              Authorization_Number__c = dli.Authorization_Number__c));
                }

                //recurringLineItems.addAll(lineItemCopies);
            }
            insert recurringLineItems;
   }
}