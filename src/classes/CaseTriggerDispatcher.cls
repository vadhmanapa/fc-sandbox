public with sharing class CaseTriggerDispatcher {

    private Boolean m_isExecuting = false;
    private Integer BatchSize = 0;
    public static Integer runCount = 0;
    public Case[] casesToRollUp;
    
    public CaseTriggerDispatcher(Boolean isExecuting, Integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
        casesToRollUp = new Case[]{};
    }

    public void OnBeforeInsert(Case[] newCases){

        if(runCount > 0) {
            system.debug(LoggingLevel.WARN,'=== Run count barrier ===');
            return;
        }

        String claimsEmailRecId = RecordTypes.claimsEmailId;
        String clearHelpRecId = RecordTypes.clearHelpId;
        String adminEmailRecId = RecordTypes.emailId;

        Map<Id, Intake__c> mapIntakeToCase = new Map<Id, Intake__c>();
        List<Case> intakeCases = new List<Case>();

        Map<String, Case> emailCases = new Map<String, Case>();
        List<Case> noContactCases = new List<Case>();

        for(Case ca: newCases) {
            // get the email Id's for the email
            System.debug('The contact Id is'+ca.ContactId);
            if((ca.RecordTypeId == claimsEmailRecId || ca.RecordTypeId == clearHelpRecId || ca.RecordTypeId == adminEmailRecId) && ca.ContactId == null) {
                emailCases.put(ca.SuppliedEmail, ca);        
            }
        }

        Map<String, List<Contact>> contactsByEmail = new Map<String, List<Contact>>();

        if(!emailCases.isEmpty()) {
            List<Contact> contactSearch = [SELECT Id, Name, RecordType.Name, Email, AccountId, Status__c 
                                            FROM Contact 
                                            WHERE Email IN:emailCases.keySet() 
                                            ORDER BY RecordType.Name DESC];
            if(!contactSearch.isEmpty()) {
                for(Contact con: contactSearch) {
                    if(!contactsByEmail.isEmpty()) {
                        // search the map if the email is already available
                        if(contactsByEmail.containsKey(con.Email)) {
                            // the email already has a list. add the value to that list
                            contactsByEmail.get(con.Email).add(con);
                        }else {
                            contactsByEmail.put(con.Email, new List<Contact>{con});
                        }
                    }else {
                        // just go ahead and add the first item
                        contactsByEmail.put(con.Email, new List<Contact>{con});
                    }
                }
            }
        }

        // now compare
        for(Case c: emailCases.values()) {

            if(c.RecordTypeId == clearHelpRecId || c.RecordTypeId == adminEmailRecId) {
                // for Clear emails, just add the contact if one value is found
                if(contactsByEmail.containsKey(c.SuppliedEmail)) {
                    c.ContactId = contactsByEmail.get(c.SuppliedEmail)[0].Id;
                    c.Debug_Log__c = 'Multiple contacts found. Added the contact by querying the email';
                    continue;
                }
            }

            /*if(c.Origin == 'Administration Email') {
                // for admin emails, just add the contact if one value is found
                if(contactsByEmail.containsKey(c.SuppliedEmail)) {
                    c.ContactId = contactsByEmail.get(c.SuppliedEmail)[0].Id;
                    c.Debug_Log__c = 'Multiple contacts found. Added the contact by querying the email';
                    continue;
                }
            }*/

            if(c.Origin == 'Claims Email' && c.RecordTypeId == claimsEmailRecId) {
                
                 // for Claims email, if contact is not found, then drop into non cred bucket
                if(contactsByEmail.containsKey(c.SuppliedEmail)) {

                    for(Contact ct: contactsByEmail.get(c.SuppliedEmail)) {
                        if(ct.Status__c == 'Active' && (ct.RecordType.Name == 'Provider' || ct.RecordType.Name == 'Integra Roster')) {
                            // this is the right contact
                            c.ContactId = ct.Id;
                            c.Debug_Log__c = 'Multiple contacts found. Added the contact by querying the email';
                            break;
                        }
                    }
                    // continue; 
                }else {
                    // the contact is not found. This is non cred contact
                    c.NonCredContact__c = true;
                    c.Debug_Log__c = 'No contact found. This is a non cred contact';
                }

                if(c.Intake__c == null) {

                    Intake__c intake = new Intake__c();
                    intake.StartTime__c = Datetime.now();
                    intake.EndTime__c = Datetime.now();
                    if(c.ContactId != null){
                        intake.Contact__c = c.ContactId;
                    }

                    mapIntakeToCase.put(c.Id, intake);
                    intakeCases.add(c);
                }
            }
        }

        // add counter for calculating the number of related cases.

        if(!mapIntakeToCase.isEmpty()){
            insert mapIntakeToCase.values();
        }

        for(Case c: intakeCases) {
            if(mapIntakeToCase.containsKey(c.Id)){
                c.Intake__c = mapIntakeToCase.get(c.Id).Id;
            }
        }
        /*for(Id i : mapIntakeToCase.keySet()){
            c.Intake__c = mapIntakeToCase.get(i).Id;
        }*/
    }

    public static void LRERollUpForCases(Case[] rollUpCases){

        LREngineForLookUp_IP.Context ctx = new LREngineForLookUp_IP.Context(Case.SobjectType, // parent object
                                    Case.SobjectType,  // child object
                                    Schema.SObjectType.Case.fields.ParentId, // relationship field name,
                                    'ParentId != null' // filter out any cases without parent Id
                                    );

        ctx.add(new LREngineForLookUp_IP.RollupSummaryField(
                    Schema.SObjectType.Case.fields.NumberofClaims__c,
                    Schema.SObjectType.Case.fields.Claim_Counter__c,
                    LREngineForLookUp_IP.RollupOperation.Sum 
                )
        );

        Sobject[] masters = LREngineForLookUp_IP.rollUp(ctx, rollUpCases);    

        // Persiste the changes in master
        update masters; 
    }

    public void OnAfterInsert(Case[] newCases, Map<Id,Case> newCaseMap){
        List<Case> rollUpList = new List<Case>();
        for(Case c: newCases) {
            if(c.ParentId != null) {
                rollUpList.add(c);
            }
        }

        if(!rollUpList.isEmpty()) {
            LRERollUpForCases(rollUpList);
        }
    }

    /*@future public static void OnAfterInsertAsync(Set<ID> newActionIDs){
        
    }
    
    public void OnBeforeUpdate(Case[] oldCases, Case[] updatedCases, Map<ID, Case> oldCaseMap){
        
    }
    
    public void OnAfterUpdate(Case[] oldCases, Case[] updatedCases, Map<ID, Case> oldCaseMap){
        
    }
    
    @future public static void OnAfterUpdateAsync(Set<ID> updatedActionIDs){
        
    }
    
    public void OnBeforeDelete(Case[] CasesToDelete, Map<ID, Case> oldCaseMap){
        
    }*/
    
    public void OnAfterDelete(Case[] deletedCases, Map<ID, Case> oldCaseMap){
        List<Case> rollUpList = new List<Case>();
        for(Case c: deletedCases) {
            if(c.ParentId != null) {
                rollUpList.add(c);
            }
        }

        if(!rollUpList.isEmpty()) {
            LRERollUpForCases(rollUpList);
        }
    }
    
   /* @future public static void OnAfterDeleteAsync(Set<ID> deletedActionIDs){
        
    }
    
    public void OnUndelete(Case[] restoredCases){
        
    }
    
    public boolean IsTriggerContext{
        get{ return m_isExecuting;}
    }
    
    public boolean IsVisualforcePageContext{
        get{ return !IsTriggerContext;}
    }
    
    public boolean IsWebServiceContext{
        get{ return !IsTriggerContext;}
    }
    
    public boolean IsExecuteAnonymousContext{
        get{ return !IsTriggerContext;}
    }*/
    /********************
        Author: Cloud Developer
        Deprecated On: 06/22/2017
        Deprecated By: Venkat Adhmanaapa
    *********************/

    /*public static Integer runCount = 0;
    
	public static void beforeInsert(List<Case> nList) {
		
        if(runCount > 0) {
            system.debug(LoggingLevel.WARN,'=== Run count barrier ===');
            return;
        }
        
        CaseAssignment.assignCaseOwners(nList);
	}//****************************************Modified by Venkat 0n 08/18/2015**************************************

    public static void beforeUpdate(List<Case> nList, Map<Id,Case> oldMap) {
        
        if(runCount > 0) {
            system.debug(LoggingLevel.WARN,'=== Run count barrier ===');
            return;
        }
        
        List<Case> toConvert = new List<Case>();
        
        for(Case newCase : nList) {
            
            Case oldCase = oldMap.containsKey(newCase.Id) ? oldMap.get(newCase.Id) : null;
            
            if( !isConvertCase(oldCase,newCase) ){ continue; }
            
            toConvert.add(newCase);
        }
        
        ConvertCaseToClaimsEmail.convertCasesToClaims(toConvert);
    }
    
	public static void afterInsert(List<Case> nList) {}	
	public static void afterUpdate(List<Case> nList) {}
    
    //--------------------------------------------------------------
    
    public static Boolean isConvertCase(Case oldCase, Case newCase) {
        
        //Check for Email
        if (String.isBlank(newCase.Copy_Email_Address__c)){
            system.debug(LoggingLevel.WARN,'Case ' + newCase.CaseNumber + ' has no Email in Copy_Email_Address__c!');
            return false;
        }
        
        //Conditions for conversion
        if (newCase.Case_Created__c != true &&
            oldCase.Convert_to_Claims_Email__c == false &&
            newCase.Convert_to_Claims_Email__c == true) 
        {
        	return true;
        }
        
        return false;
    }*/
}