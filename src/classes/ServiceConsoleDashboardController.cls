global class ServiceConsoleDashboardController {

	global ServiceConsoleDashboardController() {}

	@RemoteAction
    global static Call_Log__c createCallLog(){
    	Call_Log__c newCallLog = new Call_Log__c();
    	
    	//Preliminary Call Log Information
		newCallLog.Call_Received__c = DateTime.now();
		newCallLog.Status__c = 'Open';
		
		try{
			insert newCallLog;

			//Retrieve autonumber name
			newCallLog = [
				SELECT Id,Name
				FROM Call_Log__c
				WHERE Id = :newCallLog.Id
			];

			return newCallLog;			
		}
		catch(Exception e){
			System.debug(LoggingLevel.ERROR, 'Error inserting newCallLog: ' + e.getMessage());
			return null;
		}
    }   
}