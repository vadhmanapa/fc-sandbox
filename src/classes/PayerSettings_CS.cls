public without sharing class PayerSettings_CS extends PayerPortalServices_CS {

	private final String pageAccountType = 'Payer';

	public Contact thisUser {get;set;}

	public PayerSettings_CS(){}
	
	public PageReference init(){
		
		///////////////////////////////////
		// Authentication and Permission
		///////////////////////////////////
		
		if(!isSecure && !Test.isRunningTest()){
			return DoLogout();
		}
				
		if(!authenticated){
			return DoLogout();
		}

		if(portalUser.AccountType != pageAccountType){
			return homepage;
		}
		
		///////////////////////////////////
		// Additional init
		///////////////////////////////////
		
		if(portalUser.instance != null){
			thisUser = portalUser.instance;
		}
		
		return null;
	}
	
	/*
	public PageReference toggleOrderDeliveredNotification(){
		
		if(thisUser.Setting_Notify_Delivery__c == false){
			thisUser.Setting_Notify_Delivery__c = true;
		}
		else{
			thisUser.Setting_Notify_Delivery__c = false;
		}
						
		return null;
	}
	
	public PageReference toggleOrderCanceledNotification(){
		
		if(thisUser.Setting_Notify_Order_Cancelled__c == false){
			thisUser.Setting_Notify_Order_Cancelled__c = true;
		}
		else{
			thisUser.Setting_Notify_Order_Cancelled__c = false;
		}
						
		return null;
	}
	*/
}