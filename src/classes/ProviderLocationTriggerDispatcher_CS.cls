/**
 *  @Description 
 *  @Author Cloud Software LLC
 *  Revision History: 
 *		01/11/2016 - karnold - Added header. Formatted code.
 *			Added System.isFuture() check before geocoding to prevent 
 *		the future method from calling back into geocoding and causing an error.
 *
 */
public without sharing class ProviderLocationTriggerDispatcher_CS {
	public ProviderLocationTriggerDispatcher_CS(Map<Id, Provider_Location__c> oldMap, Map<Id, Provider_Location__c> newMap, List<Provider_Location__c> triggerOld, List<Provider_Location__c> triggerNew, Boolean isInsert, Boolean isUpdate, Boolean isDelete, Boolean isUndelete, Boolean isBefore, Boolean isAfter) {
		if (isBefore) {
			if (isInsert) { doBeforeInsert(triggerNew); }
			else if (isUpdate) { doBeforeUpdate(oldMap, newMap, triggerOld, triggerNew); }
			else if (isDelete) { doBeforeDelete(oldMap, triggerOld); }
		} else if (isAfter) {
			if (isInsert) { doAfterInsert(newMap, triggerNew); }
			else if (isUpdate) { doAfterUpdate(oldMap, newMap, triggerOld, triggerNew); }
			else if (isDelete) { doAfterDelete(oldMap, triggerOld); }
			else if (isUndelete) { doAfterUnDelete(newMap, triggerNew); }
		}
	}

	public void doBeforeInsert(List<Provider_Location__c> triggerNew) {
	}
	public void doBeforeUpdate(Map<Id, Provider_Location__c> oldMap, Map<Id, Provider_Location__c> newMap, List<SObject> triggerOld, List<SObject> triggerNew) {
	}
	public void doBeforeDelete(Map<Id, Provider_Location__c> oldMap, List<Provider_Location__c> triggerOld) {
	}
	public void doAfterInsert(Map<Id, Provider_Location__c> newMap, List<Provider_Location__c> triggerNew) {
		Set<Id> providerLocationSet = ProviderLocationServices_CS.addressesChanged(newMap, newMap);
		if (providerLocationSet.size() > 0 && !CustomSettingServices.getCreateProvLocTriggersOff()) {
			ProviderLocationServices_CS.geocodeProviderLocationIds(providerLocationSet);
		}
	}
	public void doAfterUpdate(Map<Id, Provider_Location__c> oldMap, Map<Id, Provider_Location__c> newMap, List<Provider_Location__c> triggerOld, List<Provider_Location__c> triggerNew) {
		Set<Id> providerLocationSet = ProviderLocationServices_CS.addressesChanged(oldMap, newMap);
		system.debug(CustomSettingServices.getCreateProvLocTriggersOff());
		if (providerLocationSet.size() > 0 && !CustomSettingServices.getCreateProvLocTriggersOff() && !System.isFuture()) {
			system.debug('Update geo-location');
			ProviderLocationServices_CS.geocodeProviderLocationIds(providerLocationSet);
		}
	}
	public void doAfterDelete(Map<Id, Provider_Location__c> oldMap, List<Provider_Location__c> triggerOld) {
	}
	public void doAfterUndelete(Map<Id, Provider_Location__c> newMap, List<Provider_Location__c> triggerNew) {
	}
}