/**
 *  @Description	Dispatcher for New_Orders__c
 *  @Author
 *  Revision History:
 *		12/08/2015 - karnold - Changed check for existing plan patient to use ID_Number__c AND First_Name__c, Last_Name__c, and Patient_Date_of_Birth__c
 *		12/08/2015 - Kelly Sharp - add check if misc DME and if is set it's quanity to null
 *		01/12/2016 - karnold - Added filter of Active__c = true to queries for DMEs.
 *
 */
global without sharing class CreateOrderTriggerDispatcher_CS {
	
	private static Id standardOrderRTId = [SELECT Id FROM RecordType WHERE sObjectType = 'Order__c' AND DeveloperName = 'Standard'].Id;
	
	public CreateOrderTriggerDispatcher_CS(Map<Id,SObject> oldMap, Map<Id,SObject> newMap, List<SObject> triggerOld, List<SObject> triggerNew, Boolean isInsert, Boolean isUpdate, Boolean isDelete, Boolean isUndelete, Boolean isBefore, Boolean isAfter){
        if(isBefore) {
            if(isInsert)        { doBeforeInsert(oldMap, newMap, triggerOld, triggerNew);	}
            else if(isUpdate)   { doBeforeUpdate(oldMap, newMap, triggerOld, triggerNew);   }
            else if(isDelete)   { doBeforeDelete(oldMap, triggerOld);                       }           
        }else if(isAfter) {
            if(isInsert)        { doAfterInsert(newMap, triggerNew);                        }
            else if(isUpdate)   { doAfterUpdate(oldMap, newMap, triggerOld, triggerNew);    }
            else if(isDelete)   { doAfterDelete(oldMap, triggerold);                        }
            else if(isUndelete) { doAfterUnDelete(newMap, triggerNew);                      }        
        }
    }

    /****************************************** START TRIGGER FUNCTIONS ******************************************/
    
    public void doBeforeInsert(Map<Id,SObject> oldMap, Map<Id,SObject> newMap, List<SObject> triggerOld, List<SObject> triggerNew) {
        system.debug('CreateOrderTriggerDispatcher: doBeforeInsert()');
    }    
    
    public void doBeforeUpdate(Map<Id,SObject> oldMap, Map<Id,SObject> newMap, List<SObject> triggerOld, List<SObject> triggerNew) {
         system.debug('CreateOrderTriggerDispatcher: doBeforeUpdate()');
    }
    
    public void doBeforeDelete(Map<Id,SObject> oldMap, List<SObject> triggerOld) {
    	system.debug('CreateOrderTriggerDispatcher: doBeforeDelete()');
    }   
    
    public void doAfterInsert(Map<Id,SObject> newMap, List<SObject> triggerNew) {
        system.debug('CreateOrderTriggerDispatcher: doAfterInsert()');
        if (!CustomSettingServices.getCreateOrderTriggersOff()) {
			doCreateOrders(newMap, triggerNew);
		}
    }    
    
    public void doAfterUpdate(Map<Id,SObject> oldMap, Map<Id,SObject> newMap, List<SObject> triggerOld, List<SObject> triggerNew) {
        system.debug('CreateOrderTriggerDispatcher: doAfterUpdate()');
        
        //doUpdateOrders(newMap, triggerNew);
    }  
      
    public void doAfterDelete(Map<Id,SObject> oldMap, List<SObject> triggerOld) {
    	system.debug('CreateOrderTriggerDispatcher: doAfterDelete()');
    }
     
    public void doAfterUndelete(Map<Id,SObject> newMap, List<SObject> triggerNew) {
    	system.debug('CreateOrderTriggerDispatcher: doAfterUndelete()');
    }
    
    /****************************************** END TRIGGER FUNCTIONS ******************************************/
	@TestVisible
	static Map<String,Id> picklistValToAccountId = new Map<String,Id>{
		'CenterLight' => '001300000195ljg',
		'Amerigroup MLTC' => '0013000000zrScn',
		'VillageCare' => '00130000019eAUk',
		'Montefiore' => '0013000000zrSFF',
		'Emblem MLTC' => '0013000001D9Gdz',
		'Senior Whole Health' => '0013000001HCGPL',
		'HHH Choices MLTC' => '0013000001CeE1D'
	};

	public static void doCreateOrders(Map<Id,SObject> newMap, List<SObject> triggerNew){
		
		List<Order__c> newOrders = new List<Order__c>();
		List<Order__c> ordersToNotify = new List<Order__c>();
		
		//*********************************************
		//Records to update/insert
		List<DME__c> dmesToUpdate = new List<DME__c>();
		List<DME__c> dmesToInsert = new List<DME__c>();
		
		List<DME_Line_Item__c> lineItemsToInsert = new List<DME_Line_Item__c>();

		List<Order__c> ordersSuccessfulParse = new List<Order__c>();
		List<Order__c> ordersWithValidDMEs = new List<Order__c>();
		
		Map<Id, New_Orders__c> legacyOrdersToUpdate = new Map<Id, New_Orders__c>();		
	
		//*********************************************
		//Prereq Info
		
		Map<String, Id> patientIdNumbersToHealthPlanMap = new Map<String, Id>();
		Map<String, Id> patientNameDobToHealthPlanMap = new Map<String, Id>();
		Set<String> nameSet = new Set<String>();
		Set<Date> dateOfBirthSet = new Set<Date>();
		
		Map<Id,Map<String,Decimal>> legacyOrderCodes = new Map<Id,Map<String,Decimal>>();
		Set<String> codes = new Set<String>();
		
		//Collect prereq info
		for(New_Orders__c no : (New_Orders__c[])triggerNew){
			System.debug('New Order: ' + no);

			System.debug('picklistValToAccountId: ' + picklistValToAccountId);
			//Patient ID Number
			patientIdNumbersToHealthPlanMap.put(no.Member_Patient_ID__c, picklistValToAccountId.get(no.Health_Plans__c));

			// Patient Name and DoB
			if(String.isNotBlank(no.Patient_Name__c)) {
				System.debug('Patient Name is not blank.');
				List<String> pName = parseName(no.Patient_Name__c);
				System.debug('Order patient name: ' + pName);
				System.debug('Order health plan: ' + no.Health_Plans__c);
				patientNameDobToHealthPlanMap.put(getPatientInfo(no.Patient_Name__c, no.D_O_B__c), picklistValToAccountId.get(no.Health_Plans__c));
				nameSet.add(pName[2]);
				dateOfBirthSet.add(no.D_O_B__c);
			}
			
			//CPT Code
			Map<String,Decimal> CPTCodes = getCPTCodes(no);
			
			legacyOrderCodes.put(no.Id,CPTCodes);
			codes.addAll(CPTCodes.keySet());		
		}
		
		System.debug('patientIdNumbersToHealthPlanMap: ' + patientIdNumbersToHealthPlanMap);
		System.debug('patientNameDobToHealthPlanMap: ' + patientNameDobToHealthPlanMap);
		
		//Patients
		Map<String,Plan_Patient__c> patientIdToPatient = new Map<String,Plan_Patient__c>();
		System.debug(patientIdToPatient);
		for(Plan_Patient__c p : [
			SELECT
				Id,
				Name,
				Apartment_Number__c,
				Cell_Phone_Number__c,
				City__c,
				Date_of_Birth__c,
				Email_Address__c,
				First_Name__c,
				Geolocation__c,
				Health_Plan__c,
				ID_Number__c,
				Last_Name__c,
				Medicaid_ID__c,
				Medicare_ID__c,
				Medicare_Primary_Patient__c,
				Native_Language__c,
				Patient__c,
				Phone_Number__c,
				State__c,
				Street__c,
				Zip_Code__c
			FROM Plan_Patient__c
			WHERE ID_Number__c IN :patientIdNumbersToHealthPlanMap.keySet()
			OR (Name IN :nameSet AND Date_of_Birth__c IN :dateOfBirthSet)	// Added check for Name and DOB
		]) {
			String patientInfo = getPatientInfo(p.Name, p.Date_of_Birth__c);
			System.debug('Plan patient info: ' + patientInfo);
			System.debug('Plan patient health plan: ' + p.Health_Plan__c);
			System.debug('Patient health plan: ' + patientNameDobToHealthPlanMap.get(patientInfo));
			if(patientIdNumbersToHealthPlanMap.containsKey(p.ID_Number__c) && 
					patientIdNumbersToHealthPlanMap.get(p.ID_Number__c) == p.Health_Plan__c) {

				patientIdToPatient.put(p.ID_Number__c, p);	// Was default function

			} else if (patientNameDobToHealthPlanMap.containsKey(patientInfo) && 
					patientNameDobToHealthPlanMap.get(patientInfo) == p.Health_Plan__c) {

				patientIdToPatient.put(patientInfo, p);	// Alternate mappings for name DOB matches
			} 
			// Anything not matching based on id number + healthplan or name and dob + healthplan does not have a match and should not be populated.
		}

		//DME info from CPT codes
				Map<String,DME__c> codeToDME = new Map<String,DME__c>();
		
		for(DME__c dme : [
	        SELECT 
	            Id,
	            Name 
	        FROM DME__c
	        WHERE Name IN :codes
	    ]){
	        codeToDME.put(dme.Name, dme);
	    }
		//KS added Miscellaneous__c to query 12/08/2015
		//Map<String,DME__c> codeToDME = new Map<String,DME__c>();
		
		//for(DME__c dme : [
			//SELECT 
				//Id,
				//Name,
				//Miscellaneous__c
			//FROM DME__c
			//WHERE Active__c = true
			//AND Name IN :codes
		//]){
			//codeToDME.put(dme.Name, dme);
		//}
		
		//Account info
		Map<Id,Id> newOrderToProviderAccountId = new Map<Id,Id>();
		
		for(New_Orders__c no : [
			SELECT 
				Account_c__c,
				Provider_Location__c, 
				Provider_Location__r.Account__c 
			FROM New_Orders__c 
			WHERE Id IN :newMap.keyset()
		]){
	    	newOrderToProviderAccountId.put(no.id,no.Provider_Location__r.Account__c);
	    }

		system.debug(LoggingLevel.INFO,newOrderToProviderAccountId);

		//*********************************************
		//Main loop for conversion
		for(New_Orders__c no : (New_Orders__c[])triggerNew){

			String pNameDob = getPatientInfo(no.Patient_Name__c, no.D_O_B__c);	// parsing to re-create the Name/DoB fields
			System.debug('Patient name and dob' + pNameDob);
			
			// Stop order if plan patient does not exist
			// Also check order against Name/DoB fields
			if(!patientIdToPatient.containsKey(no.Member_Patient_ID__c) && !patientIdToPatient.containsKey(pNameDob)){	
				System.debug('patientIdToPatient: ' + patientIdToPatient);
				System.debug('no.Member_Patient_ID__c: ' + no.Member_Patient_ID__c);
				New_Orders__c clonedLegacyOrder = no.clone(true, true, true, true);
				clonedLegacyOrder.OCR_Parse_Failed__c = true;
				clonedLegacyOrder.Reason_for_OCR_Failure__c = 'Plan Patient Not Found';
				legacyOrdersToUpdate.put(no.Id, clonedLegacyOrder);
			
			// Stop order if there are no legacy order codes
			} else if (legacyOrderCodes.get(no.Id).isEmpty()) {
				New_Orders__c clonedLegacyOrder = no.clone(true, true, true, true);
				clonedLegacyOrder.OCR_Parse_Failed__c = true;
				clonedLegacyOrder.Reason_for_OCR_Failure__c = 'Parsing failure/No DMEs entered';
				legacyOrdersToUpdate.put(no.Id, clonedLegacyOrder);
			
			} else {
				Order__c newOrder = new Order__c();
				
				//*********************************************
				//Copy order data
				newOrder.Accepted_By__c = no.Accepted_By__c;
				newOrder.Accepted_Date__c = no.Accepted_Date__c;
				newOrder.Accept_Order__c = no.Accept_Order__c;
				newOrder.Comments__c = no.Notes__c;
				newOrder.Provider_Contact__c = no.Assigned_Contact__c;
				newOrder.Canceled_Order__c = no.Canceled_Order__c;
				System.debug('Dispatcher datetime: ' + no.Delivery_Date__c);
				newOrder.Delivery_Date_Time__c = (no.Delivery_Date__c == null ? null : DateTime.newInstanceGmt(no.Delivery_Date__c,Time.newInstance(0,0,0,0)));
				System.debug('New order datetime: ' + newOrder.Delivery_Date_Time__c);
				newOrder.Delivery_Method__c = no.Delivery_Method__c;
				newOrder.Entered_By__c = no.Entered_By__c;
				newOrder.Estimated_Delivery_Time__c = no.Estimated_Delivery_Time__c;
				newOrder.Legacy_Orders__c = no.Id;
				newOrder.Manually_Assigned__c = no.Manually_Assigned__c;
				newOrder.Order_Image__c = no.Order_Image__c;
				newOrder.Other_Canceled_Orders__c = no.Other_Canceled_Orders__c;
				newOrder.Authorization_Number__c  = no.Plan_Order__c;
				newOrder.Payer__c = picklistValToAccountId.get(no.Health_Plans__c);
				newOrder.Provider__c = newOrderToProviderAccountId.get(no.Id);
				newOrder.Status_of_Order__c = no.Status_of_Order__c;
				newOrder.Provider_Locations__c = no.Provider_Location__c;
				newOrder.Re_Assign_Order__c = no.Re_Assign_Order__c;
				newOrder.Tracking_Number__c = no.Tracking_Number__c;
				newOrder.Case_Manager__c = no.Test_Care_Navigator__c;
			
				//Delivery info
				newOrder.Street__c = no.Street__c;
				newOrder.City__c = no.City__c;
				newOrder.State__c = no.State__c;
				newOrder.Zip__c = no.Zip__c;
			
				//Status info
				if(no.Status_of_Order__c == 'Additional Information Needed'){
					newOrder.Status__c = 'Needs Attention - Incomplete Delivery Information';
				}else if(no.Status_of_Order__c == 'Closed/Not Delivered'){
					newOrder.Status__c = 'Needs Attention - Patient Refused - Other';
				}else if(no.Status_of_Order__c == 'Delivered'){
					newOrder.Status__c = 'Delivered';
				}else if(no.Status_of_Order__c == 'In Progress'){
					newOrder.Status__c = 'Provider Accepted';
				}else if(no.Status_of_Order__c == 'Open'){
					newOrder.Status__c = 'New';
				}
			
				//Referral info
				if(no.Person_placing_the_order__c != null){
					List<String> parse = parseName(no.Person_placing_the_order__c);
					newOrder.Referred_By_First_Name__c = parse[0];
					newOrder.Referred_By_Last_Name__c = parse[1];
					newOrder.Referred__c = true;	
				}			
				newOrder.Referred_By_Phone__c = no.Physician_Phone__c;
			
				//*********************************************
				//Copy patient data
				//NOTE: We do not insert new patients as needed.
				//NOTE: We cannot currently dedupe to provide a lookup, merely attach the order info

				//Parse the entered name into fields
				if(no.Patient_Name__c != null){
					List<String> parse = parseName(no.Patient_Name__c);

					//Patient
					newOrder.Patient_First_Name__c = parse[0];
					newOrder.Patient_Last_Name__c = parse[1];
					newOrder.Patient_Name__c = parse[2];

					//Recipient
					newOrder.Recipient_First_Name__c = parse[0];
					newOrder.Recipient_Last_Name__c = parse[1];
					newOrder.Recipient_Name__c = parse[2];
				}

				newOrder.Patient_Phone_Number__c = no.Patient_Phone__c;
				newOrder.Patient_Mobile_Phone_Number__c = null;
				newOrder.Patient_Email_Address__c = null;
				newOrder.Patient_Date_of_Birth__c = no.D_O_B__c;
				newOrder.Patient_ID_Number__c = no.Member_Patient_ID__c;
				newOrder.Patient_Medicaid_ID__c = no.Medicaid_ID__c;
				newOrder.Patient_Medicare_ID__c = no.Medicare_ID__c;
				newOrder.Patient_Native_Language__c = null;

				//Address Info
				newOrder.Patient_Street__c = no.Street__c;
				newOrder.Patient_City__c = no.City__c;
				newOrder.Patient_State__c = no.State__c;
				newOrder.Patient_Zip_Code__c = no.Zip__c;
				newOrder.Patient_Country__c = 'United States';

				//*********************************************
				//Copy recipient data from the recently copied patient data
				//NOTE: We are assuming that the patient and recipient are the same person
				newOrder.Recipient_Country__c = 'United States';
				newOrder.Recipient_Date_of_Birth__c = no.D_O_B__c;
				newOrder.Recipient_Email__c = null;
				newOrder.Recipient_Email_Address__c = null;
				newOrder.Recipient_Native_Language__c = null;
				newOrder.Recipient_Phone__c = newOrder.Patient_Phone_Number__c;
				newOrder.Recipient_Phone_Number__c = newOrder.Patient_Phone_Number__c;
				newOrder.Recipient_Cell_Phone__c = null;
				newOrder.Recipient_Relationship__c = 'Patient';
				//newOrder.RecordTypeId = standardOrderRTId;

				
				//Connect orders to their plan patients
				// Moved this inside loop to avoid double loop and to remove ambiguity about successful orders. 
				// Only orders with matches are added to the list to be inserted.
				if(patientIdToPatient.containsKey(newOrder.Patient_ID_Number__c)){
					system.debug(LoggingLevel.INFO,'Found patient with Id: ' + newOrder.Patient_ID_Number__c);

					newOrder.Plan_Patient__c = patientIdToPatient.get(newOrder.Patient_ID_Number__c).Id;
					ordersSuccessfulParse.add(newOrder);
				} else if (patientIdToPatient.containsKey(pNameDob)) {
					system.debug(LoggingLevel.INFO,'Found patient with Name/DoB: ' + pNameDob);

					newOrder.Plan_Patient__c = patientIdToPatient.get(pNameDob).Id;
					ordersSuccessfulParse.add(newOrder);
				} else {
					system.debug(LoggingLevel.INFO,'No patient found for: ' + pNameDob);

					New_Orders__c clonedLegacyOrder = no.clone(true, true, true, true);
					clonedLegacyOrder.OCR_Parse_Failed__c = true;
					clonedLegacyOrder.Reason_for_OCR_Failure__c = 'No Plan Patient Found';
					legacyOrdersToUpdate.put(no.Id, clonedLegacyOrder);
				}
			}
		}

		System.debug('Number of orders parsed: ' + ordersSuccessfulParse.size());
		
		//*********************************************
		//Make new line items
		//Map<Id, Order__c> ordersWithParseFailure = new Map<Id, Order__c>();
		Map<Integer, List<DME_Line_Item__c>> orderIndexIdToDMELineItemsMap = new Map<Integer, List<DME_Line_Item__c>>();
		Set<Integer> indexesOfErrorOrders = new Set<Integer>();
		for(Integer i = 0; i < ordersSuccessfulParse.size(); i++){

			//CPT codes included in the legacy order
			Map<String,Decimal> orderCPTCodes = legacyOrderCodes.get(ordersSuccessfulParse[i].Legacy_Orders__c);
			System.debug('Order CPT codes: ' + orderCPTCodes);
			
			//Clone the legacy order in order to update it with parsing information
			New_Orders__c clonedLegacyOrder = ((New_Orders__c)newMap.get(ordersSuccessfulParse[i].Legacy_Orders__c)).clone(true, true, true, true);
			Boolean parseFailed = false; //Flag for this run - the flag on the record does not get reset
			String failureMessage = '';

			//For each code in this order, find the matching dme
			for(String code : orderCPTCodes.keySet()){
				
				String failureReason = '';

				//Find the matching dme
				if(codeToDME.containsKey(code)){
					System.debug('Success: Found matching dme code for "' + code + '"');
					
					//Create the line item for this DME
					DME_Line_Item__c newLineItem = new DME_Line_Item__c(
						DME__c = codeToDME.get(code).Id,
						Quantity__c = orderCPTCodes.get(code)
					);
					// if a Miscellaneous code set quantity to null ... this is neede to not cause errors in other systems
					// added 12/08/2015 KS
					//DME_Line_Item__c newLineItem = new DME_Line_Item__c();
					//if(codeToDME.get(code).Miscellaneous__c){
						//newLineItem.DME__c = codeToDME.get(code).Id;
						//newLineItem.Quantity__c = null;
					//}else{			
						//newLineItem.DME__c = codeToDME.get(code).Id;
						//newLineItem.Quantity__c = orderCPTCodes.get(code);
					//}


					if (!orderIndexIdToDMELineItemsMap.containsKey(i)) {
						orderIndexIdToDMELineItemsMap.put(i, new List<DME_Line_Item__c>());
					}
					orderIndexIdToDMELineItemsMap.get(i).add(newLineItem);
					
					System.debug('New line item: ' + newLineItem);
				}
				else{
					parseFailed = true;
					failureMessage += 'No DME w/ code: ' + code + '. ';
					System.debug('Error: No matching dme code for "' + code + '"');
				}
			}
			if(parseFailed) {
				clonedLegacyOrder.OCR_Parse_Failed__c = true;
				clonedLegacyOrder.Reason_for_OCR_Failure__c = failureMessage;
				indexesOfErrorOrders.add(i);
			}else {
				clonedLegacyOrder.Order_Created__c = true;
			}
			legacyOrdersToUpdate.put(ordersSuccessfulParse[i].Legacy_Orders__c, clonedLegacyOrder);
			//If all DMEs exist, add the order to a list to insert
			if(!indexesOfErrorOrders.contains(i)) {
				ordersWithValidDMEs.add(ordersSuccessfulParse[i]);
			}
			//o.Reason_for_OCR_Failure__c.abbreviate(255);
		}
		
		update legacyOrdersToUpdate.values();

		insert ordersWithValidDMEs;
		
		System.debug('ordersWithValidDMEs: ' + ordersWithValidDMEs);

		System.debug(indexesOfErrorOrders);
		System.debug(orderIndexIdToDMELineItemsMap);

		//Insert the line items for all orders which were created
		for (Integer i : orderIndexIdToDMELineItemsMap.keySet()) {
			if(!indexesOfErrorOrders.contains(i)) {
				System.debug(indexesOfErrorOrders + ' contains ' + i);
				for (DME_Line_Item__c li : orderIndexIdToDMELineItemsMap.get(i)) {
					li.Order__c = ordersSuccessfulParse[i].Id;
					lineItemsToInsert.add(li);
				}
			}
		}
		insert lineItemsToInsert;

		////Assign the order based on Algorithm on/off switch
		//System.debug('Orders will be assigned with algorithm? ' + CustomSettingServices.getUseNewAlgorithm());
		//if (CustomSettingServices.getUseNewAlgorithm()) {
			//for (Order__c o : ordersWithValidDMEs) {
				//o.Simulation_Status__c = 'Ready';
			//}
		//} else {
			//System.debug('Assigning orders as ' + OrderModel_CS.STATUS_USE_OLD_ROUTING);
			//for (Order__c o : ordersWithValidDMEs) {
				//o.Status__c = OrderModel_CS.STATUS_USE_OLD_ROUTING;
			//}
		//}
		update ordersWithValidDMEs;
	
		//***********************************************
		//Notifications
		
		//Get new order information for inserted orders
		Set<Id> orderIds = new Set<Id>();
		for(Order__c o : ordersToNotify){
			orderIds.add(o.Id);
		}
		
		List<Order__c> updatedOrdersToInsert = [
			SELECT
				Name
			FROM Order__c
			WHERE Id IN :orderIds
		];
		
		Messaging.SingleEmailMessage notification = new Messaging.SingleEmailMessage();
		
		OCR_Patient_Notification__c[] notificationEmails = OCR_Patient_Notification__c.getAll().values();
		
		//Only send emails if the custom setting is populated
		if(notificationEmails.size() > 0) {
			
			List<String> emailAddresses = new List<String>();
			for(OCR_Patient_Notification__c emailInfo : notificationEmails){
				emailAddresses.add(emailInfo.Email__c);
			}
			
			notification.setToAddresses(emailAddresses);
			
	       	String emailBody = '';
			emailBody += 'Hello,<br/><br/>';
			emailBody += 'The following orders failed to create Plan Patients:<br/><br/>';
	       	
	       	for(Order__c o : updatedOrdersToInsert){
	   	       	emailBody += o.Name + '<br/>';
	       	}
			
			notification.setHtmlBody(emailBody);
			
			EmailServices_CS.sendEmail(new Messaging.SingleEmailMessage[]{notification});
		}
	}
    
    //*********************************************
    //Parse the CPT values from the CPT field into a map
	public static Map<String,Decimal> getCPTCodes(New_Orders__c no){
		
		//Try to parse the CPT codes. If any failures occur, pass an empty list
		Map<String,Decimal> codeToUnits;
		try {
		
			//Get all CPT codes and their quantities
			List<String> codeList = no.CPT_code_for_OCR__c.split('[,| ]+');
			List<String> quantityStringList = no.Units_for_OCR__c.split('[,| ]+');
			
			//If there are a different number of codes and quantities, leave the map empty
			codeToUnits = new Map<String,Decimal>();
			if (codeList.size() == quantityStringList.size()) {
				
				//Convert the quantity string list into a list of decimal values. If any value is not a decimal, throw an error
				List<Decimal> quantityList = new List<Decimal>();
				for (String quantity : quantityStringList) {
					quantityList.add(Decimal.valueOf(quantity));
				}
				
				//Map the CPT code to its quantity
				for (Integer i = 0; i < codeList.size(); i++) {
					codeToUnits.put(codeList[i], quantityList[i]);
				}
			}
		}catch(Exception e) {
			codeToUnits = new Map<String,Decimal>();
		}
		
	    return codeToUnits;
	}	
	
	public static Boolean hasMultiFieldCodes(New_Orders__c no){
		
		if(no.CPT_Code1__c != null || no.Units1__c != null){ return true; }
	    if(no.CPT_Code_2__c != null || no.Units_2__c != null){ return true; }
	    if(no.CPT_Code_3__c != null || no.Units_3__c != null){ return true; }
	    if(no.CPT_Code_4__c != null || no.Units_4__c != null){ return true; }
	    if(no.CPT_Code_5__c != null || no.Units_5__c != null){ return true; }
	    if(no.CPT_Code_6__c != null || no.Units_6__c != null){ return true; }
	    if(no.CPT_Code_7__c != null || no.Units_7__c != null){ return true; }
	    if(no.CPT_Code_8__c != null || no.Units_8__c != null){ return true; }
	    if(no.CPT_Code_9__c != null || no.Units_9__c != null){ return true; }
	    if(no.CPT_Code_10__c != null || no.Units_10__c != null){ return true; }
	    
	    return false;
	}
	
	//*********************************************
	//Parses a name and returns three fields
	//	0: First Name
	//	1: Last Name
	//	2: First Name + ' ' + Last Name
	public static List<String> parseName(String input){
		List<String> names = new List<String>(3);
		
		//If bad input, return a list of blanks
		if(String.isBlank(input)){
			names[0] = '';
			names[1] = '';
			names[2] = '';
			return names;
		}
		
		//Trim the input
		input = input.trim();
		
		//Case 1: "{Last Name}, {First Name}"
		if(input.contains(',')){
	    	List<String> parse = input.split(',',2);
	        names[0] = parse[1].trim();
	        names[1] = parse[0].trim();	        
	        names[2] = parse[1].trim() + ' ' + parse[0].trim();
	    }
	    //Case 2: "{First Name} {Last Name}"
	    else if(input.contains(' ')){
	    	//Only let "split" create two substrings
	        List<String> parse = input.split(' ',2);	        
	        names[0] = parse[0].trim();
	        names[1] = parse[1].trim();
	        names[2] = parse[0].trim() + ' ' + parse[1].trim();
	    }
	    //Case 3: "{Name}"
	    else{
	        names[0] = '';
	        names[1] = input;
	        names[2] = input;
	    }
	    
	    return names;
	}

	/**
	* @description concatonates and returns name and date of birth info as a string for consistency
	*/
	private static String getPatientInfo(String fullName, Date dob) {	
		return parseName(fullName)[2] + String.valueOf(dob);
	}
	
	webservice static Boolean doCreateOrder(Id legacyOrderId) {
		New_Orders__c no = [
			SELECT Id, Accepted_By__c, Accepted_Date__c, Accept_Order__c, Notes__c, Assigned_Contact__c, Canceled_Order__c, Delivery_Date__c, Delivery_Method__c, Entered_By__c,
				Estimated_Delivery_Time__c, Manually_Assigned__c, Order_Image__c, Other_Canceled_Orders__c, Plan_Order__c, Health_Plans__c, Re_Assign_Order__c, 
				Status_of_Order__c, Tracking_Number__c, Test_Care_Navigator__c, Street__c, City__c, State__c, Zip__c, Physician_Phone__c, Patient_Phone__c, D_O_B__c, Member_Patient_ID__c, Medicaid_ID__c,
				Medicare_ID__c, OCR_Parse_Failed__c, Reason_for_OCR_Failure__c, Person_placing_the_order__c, Patient_Name__c, CPT_code_for_OCR__c, Units_for_OCR__c,
				(SELECT Id FROM Legacy_Orders__r)
			FROM New_Orders__c
			WHERE Id = :legacyOrderId
		];
		if (no.Legacy_Orders__r.size() == 0) {
			doCreateOrders(
				new Map<Id, sObject>{no.Id => no},
				new List<sObject>{no}
			);
			return true;
		}
		else {
			return false;
		}
	}
}