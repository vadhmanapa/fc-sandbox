public with sharing class CopyProductsToParent_IP {
	public CopyProductsToParent_IP() {
		
	}
	public static void getTriggerData(List<Provider_Product_Item__c> newTrigger){
	Set<Id> oppIds = new Set<Id>();
	Set<Id> leadIds = new Set<Id>();
	Set<Id> prodIds = new Set<Id>();
	String oppProduct;
	String leadProduct;
		for(Provider_Product_Item__c ppi : newTrigger){

			prodIds.add(ppi.Product_Identifier__c);
			if(ppi.Lead__c != null){
				// add to lead list
				System.debug('Lead id is'+ppi.Lead__r.Id);
				leadIds.add(ppi.Lead__c);
			}
			System.debug('size of lead id'+leadIds.size());
			if(ppi.Opportunity__c != null){
				// add to opp
				oppIds.add(ppi.Opportunity__c);
			}
		}
	// query the values
	Map<Id,Opportunity> queryOpp = new Map<Id, Opportunity>();
		if(oppIds.size() > 0){

			for(Opportunity o: [Select Id,Products_Serviced__c from Opportunity where Id =:oppIds]) {
			   queryOpp.put(o.Id, o);
			}
			
		}
		
		Map<Id, Lead> queryLeads = new Map<Id, Lead>();
		if(leadIds.size() > 0){

			for(Lead l: [Select Id,Products_Serviced__c, isConverted from Lead where Id =:leadIds]) {
				if(l != null && !l.isConverted){
					queryLeads.put(l.Id, l);
				}
			}
		}

		Map<Id, String> prodMap = new Map<Id, String>();
		if(prodIds.size() > 0){

			for(Provider_Product_Identifier__c pi : [Select Id, Product_Name__c from Provider_Product_Identifier__c where Id=: prodIds]){

				prodMap.put(pi.Id, pi.Product_Name__c);
			}
		}

		for(Provider_Product_Item__c pp : newTrigger){

			if(queryLeads.containsKey(pp.Lead__c)){

				//leadProduct = queryOpp.get(pp.Lead__r.Id);

				System.debug('Product is'+prodMap.get(pp.Product_Identifier__c));

				if(queryLeads.get(pp.Lead__c).Products_Serviced__c != null){

					if((queryLeads.get(pp.Lead__c).Products_Serviced__c).contains(prodMap.get(pp.Product_Identifier__c))){

						// don't update anything
					} else{

						queryLeads.get(pp.Lead__c).Products_Serviced__c = queryLeads.get(pp.Lead__c).Products_Serviced__c + ',' + ' ' + prodMap.get(pp.Product_Identifier__c);
					}
					
				}else{

					queryLeads.get(pp.Lead__c).Products_Serviced__c = prodMap.get(pp.Product_Identifier__c);
				}
			}

			if(queryOpp.containsKey(pp.Opportunity__c)){

				//leadProduct = queryOpp.get(pp.Lead__r.Id);

				if(queryOpp.get(pp.Opportunity__c).Products_Serviced__c != null){

					if((queryOpp.get(pp.Opportunity__c).Products_Serviced__c).contains(prodMap.get(pp.Product_Identifier__c))){

						// dont do anything
					} else{

					queryOpp.get(pp.Opportunity__c).Products_Serviced__c = queryOpp.get(pp.Opportunity__c).Products_Serviced__c + ','+' '+prodMap.get(pp.Product_Identifier__c);

					}

				}else{

					queryOpp.get(pp.Opportunity__c).Products_Serviced__c = prodMap.get(pp.Product_Identifier__c);
				}
			}
		}

		Database.SaveResult[] srList = Database.update(queryLeads.values(),false);
		for(Database.SaveResult sr : srList){

			if(sr.isSuccess()){

				}else {
					System.debug('The following error occured'+sr.getErrors());
				}
		}
		//update queryLeads.values();

		Database.SaveResult[] srListOpp = Database.update(queryOpp.values(),false);
		for(Database.SaveResult sr : srListOpp){

			if(sr.isSuccess()){

				}else {
					System.debug('The following error occured'+sr.getErrors());
				}
		}
		//update queryOpp.values();
	}
}