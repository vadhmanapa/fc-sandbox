@isTest
private class WhoIsThisCallAboutControllerTest_IP
{
	public static TestCaseDataFactory_IP generateNewData;
	private static WhoIsThisCallAboutController_IP whoIsThisAbout;
	private static User testUser;

	@isTest static void testWhoThisCallAboutPicklist()
	{
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		testUser = TestCaseDataFactory_IP.csUser;
		List<SelectOption> whoIsThisAboutType = new List<SelectOption>();

		System.runAs(testUser)
		{
			Test.startTest();
			whoIsThisAbout = new WhoIsThisCallAboutController_IP();
			whoIsThisAboutType = whoIsThisAbout.getCallAboutType();
			Test.stopTest();
		}

		System.assertNotEquals(0, whoIsThisAboutType.size(), 'The picklist is ready when the controller is loaded');
	}

	@isTest static void testAutoFillPatientInfo()
	{
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		testUser = TestCaseDataFactory_IP.csUser;

		Contact c = [SELECT Id, Name, Birthdate,FirstName, LastName, Account.Name, AccountId, RecordTypeId, Account.Status__c, Phone, Email, Health_Plan_ID__c 
					FROM Contact WHERE Health_Plan_ID__c= '123450' LIMIT 1];

		System.runAs(testUser)
		{
			whoIsThisAbout = new WhoIsThisCallAboutController_IP();
			whoIsThisAbout.autoFillPatientContact = c;
			whoIsThisAbout.selectedCallerType = 'Patient';
			whoIsThisAbout.selectedCallAboutType = 'Patient';
			Test.startTest();
			whoIsThisAbout.initAboutCall();
			Test.stopTest();
		}

		System.assertNotEquals(null, whoIsThisAbout.conThisCallAbout, 'The patient information was copied from auto fill contact');
	}

	@isTest static void testNoAutoFillPatientInfo()
	{
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		testUser = TestCaseDataFactory_IP.csUser;

		Contact c = [SELECT Id, Name, Birthdate,FirstName, LastName, Account.Name, AccountId, RecordTypeId, Account.Status__c, Phone, Email, Health_Plan_ID__c 
					FROM Contact WHERE Health_Plan_ID__c= '123450' LIMIT 1];

		System.runAs(testUser)
		{
			whoIsThisAbout = new WhoIsThisCallAboutController_IP();
			whoIsThisAbout.autoFillPatientContact = c;
			whoIsThisAbout.selectedCallerType = 'Patient';
			whoIsThisAbout.selectedCallAboutType = 'Provider';
			Test.startTest();
			whoIsThisAbout.initAboutCall();
			Test.stopTest();
		}

		System.assertEquals(null, whoIsThisAbout.conThisCallAbout, 'The patient information was copied from auto fill contact');
	}

	@isTest static void testvalidateNewPatientNoFirstName()
	{
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		testUser = TestCaseDataFactory_IP.csUser;

		Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Payor0' LIMIT 1];

		System.runAs(testUser)
		{
			whoIsThisAbout = new WhoIsThisCallAboutController_IP();
			whoIsThisAbout.selectedCallerType = 'Patient';
			whoIsThisAbout.selectedCallAboutType = 'Patient';
			whoIsThisAbout.initAboutCall();
			whoIsThisAbout.conThisCallAbout.AccountId = acc.Id;
			whoIsThisAbout.initNewPatientContact();
			Test.startTest();
			whoIsThisAbout.insertNewPatient.FirstName = '';
			whoIsThisAbout.addNewPatient();
			Test.stopTest();
		}

		System.assertEquals(true, whoIsThisAbout.validateError, 'First Name cannot be empty on new contact');
	}

	@isTest static void testvalidateNewPatientNoLastName()
	{
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		testUser = TestCaseDataFactory_IP.csUser;

		Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Payor0' LIMIT 1];

		System.runAs(testUser)
		{
			whoIsThisAbout = new WhoIsThisCallAboutController_IP();
			whoIsThisAbout.selectedCallerType = 'Patient';
			whoIsThisAbout.selectedCallAboutType = 'Patient';
			whoIsThisAbout.initAboutCall();
			whoIsThisAbout.conThisCallAbout.AccountId = acc.Id;
			whoIsThisAbout.initNewPatientContact();
			Test.startTest();
			whoIsThisAbout.insertNewPatient.FirstName = 'Test';
			whoIsThisAbout.insertNewPatient.LastName = '';
			whoIsThisAbout.addNewPatient();
			Test.stopTest();
		}

		System.assertEquals(true, whoIsThisAbout.validateError, 'Last Name cannot be empty on new contact');
	}

	@isTest static void testvalidateNewPatientNoBirthDate()
	{
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		testUser = TestCaseDataFactory_IP.csUser;

		Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Payor0' LIMIT 1];

		System.runAs(testUser)
		{
			whoIsThisAbout = new WhoIsThisCallAboutController_IP();
			whoIsThisAbout.selectedCallerType = 'Patient';
			whoIsThisAbout.selectedCallAboutType = 'Patient';
			whoIsThisAbout.initAboutCall();
			whoIsThisAbout.conThisCallAbout.AccountId = acc.Id;
			whoIsThisAbout.initNewPatientContact();
			Test.startTest();
			whoIsThisAbout.insertNewPatient.FirstName = 'Test';
			whoIsThisAbout.insertNewPatient.LastName = 'Patient';
			whoIsThisAbout.insertNewPatient.Birthdate = null;
			whoIsThisAbout.addNewPatient();
			Test.stopTest();
		}

		System.assertEquals(true, whoIsThisAbout.validateError, 'Birth Date cannot be empty on new contact');
	}

	@isTest static void testvalidateNewPatientNoPhone()
	{
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		testUser = TestCaseDataFactory_IP.csUser;

		Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Payor0' LIMIT 1];

		System.runAs(testUser)
		{
			whoIsThisAbout = new WhoIsThisCallAboutController_IP();
			whoIsThisAbout.selectedCallerType = 'Patient';
			whoIsThisAbout.selectedCallAboutType = 'Patient';
			whoIsThisAbout.initAboutCall();
			whoIsThisAbout.conThisCallAbout.AccountId = acc.Id;
			whoIsThisAbout.initNewPatientContact();
			Test.startTest();
			whoIsThisAbout.insertNewPatient.FirstName = 'Test';
			whoIsThisAbout.insertNewPatient.LastName = 'Patient';
			whoIsThisAbout.insertNewPatient.Birthdate = System.today().addYears(-30);
			whoIsThisAbout.insertNewPatient.Phone = '';
			whoIsThisAbout.addNewPatient();
			Test.stopTest();
		}

		System.assertEquals(true, whoIsThisAbout.validateError, 'Phone cannot be empty on new contact');
	}

	@isTest static void testvalidateNewPatientHealthPlanId()
	{
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		testUser = TestCaseDataFactory_IP.csUser;

		Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Payor0' LIMIT 1];

		System.runAs(testUser)
		{
			whoIsThisAbout = new WhoIsThisCallAboutController_IP();
			whoIsThisAbout.selectedCallerType = 'Patient';
			whoIsThisAbout.selectedCallAboutType = 'Patient';
			whoIsThisAbout.initAboutCall();
			whoIsThisAbout.conThisCallAbout.AccountId = acc.Id;
			whoIsThisAbout.initNewPatientContact();
			Test.startTest();
			whoIsThisAbout.insertNewPatient.FirstName = 'Test';
			whoIsThisAbout.insertNewPatient.LastName = 'Patient';
			whoIsThisAbout.insertNewPatient.Birthdate = System.today().addYears(-30);
			whoIsThisAbout.insertNewPatient.Phone = '6308812598';
			whoIsThisAbout.insertNewPatient.Health_Plan_ID__c = '';
			whoIsThisAbout.addNewPatient();
			Test.stopTest();
		}

		System.assertEquals(true, whoIsThisAbout.validateError, 'Health Plan cannot be empty on new contact');
	}

	@isTest static void testCreateNewPatient()
	{
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		testUser = TestCaseDataFactory_IP.csUser;

		Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Payor0' LIMIT 1];

		System.runAs(testUser)
		{
			whoIsThisAbout = new WhoIsThisCallAboutController_IP();
			whoIsThisAbout.selectedCallerType = 'Patient';
			whoIsThisAbout.selectedCallAboutType = 'Patient';
			whoIsThisAbout.initAboutCall();
			whoIsThisAbout.conThisCallAbout.AccountId = acc.Id;
			whoIsThisAbout.initNewPatientContact();
			Test.startTest();
			whoIsThisAbout.insertNewPatient.FirstName = 'Test';
			whoIsThisAbout.insertNewPatient.LastName = 'Patient';
			whoIsThisAbout.insertNewPatient.Birthdate = System.today().addYears(-30);
			whoIsThisAbout.insertNewPatient.Phone = '6308812598';
			whoIsThisAbout.insertNewPatient.Health_Plan_ID__c = '652165621';
			whoIsThisAbout.addNewPatient();
			Test.stopTest();
		}

		System.assertEquals('652165621', whoIsThisAbout.conThisCallAbout.Health_Plan_ID__c, 'New patient created and value passed to conThisPatient');
	}

	@isTest static void testCopyPatientDetails()
	{
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		testUser = TestCaseDataFactory_IP.csUser;

		Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Payor0' LIMIT 1];
		Contact c = [SELECT Id, Name, Birthdate,FirstName, LastName, Account.Name, AccountId, RecordTypeId, Account.Status__c, Phone, Email, Health_Plan_ID__c 
					FROM Contact WHERE Health_Plan_ID__c= '123450' AND AccountId =: acc.Id LIMIT 1];

		System.runAs(testUser)
		{
			whoIsThisAbout = new WhoIsThisCallAboutController_IP();
			whoIsThisAbout.selectedCallerType = 'Patient';
			whoIsThisAbout.selectedCallAboutType = 'Patient';
			whoIsThisAbout.initAboutCall();
			whoIsThisAbout.conThisCallAbout.AccountId = acc.Id;
			whoIsThisAbout.conThisCallAbout.Id = c.Id;
			whoIsThisAbout.initNewPatientContact();
			Test.startTest();
			whoIsThisAbout.copyPatientDetails();
			Test.stopTest();
		}

		System.assertEquals('123450', whoIsThisAbout.conThisCallAbout.Health_Plan_ID__c, 'Patient detail was copied');
	}

	@isTest static void testCopyFromCasePatient()
	{
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		testUser = TestCaseDataFactory_IP.csUser;

		Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Payor0' LIMIT 1];
		Contact c = [SELECT Id, Name, Birthdate,FirstName, LastName, Account.Name, AccountId, RecordTypeId, Account.Status__c, Phone, Email, Health_Plan_ID__c 
					FROM Contact WHERE Health_Plan_ID__c= '123450' AND AccountId =: acc.Id LIMIT 1];

		Case cas = new Case();
		cas.AccountId = acc.Id;
		cas.ContactId = c.Id;
		cas.Patient__c = c.Id;
		cas.Patient_Payor__c = c.AccountId;
		cas.PolicyNumber__c = c.Health_Plan_ID__c;
		cas.DOB__c = c.Birthdate;
		insert cas;

		System.runAs(testUser)
		{
			whoIsThisAbout = new WhoIsThisCallAboutController_IP();
			Test.startTest();
			whoIsThisAbout.startCase = cas;
			Test.stopTest();
		}

		System.assertEquals('Patient', whoIsThisAbout.selectedCallAboutType, 'Call about type was set from case values');
		System.assertNotEquals(null, whoIsThisAbout.conThisCallAbout, 'Contact info was filled in from case');
	}

	@isTest static void testCopyFromCasePayor()
	{
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		testUser = TestCaseDataFactory_IP.csUser;

		Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Payor0' LIMIT 1];
		Contact c = [SELECT Id, Name, Birthdate,FirstName, LastName, Account.Name, AccountId, RecordTypeId, Account.Status__c, Phone, Email, Health_Plan_ID__c 
					FROM Contact WHERE Health_Plan_ID__c= '123450' AND AccountId =: acc.Id LIMIT 1];

		Case cas = new Case();
		cas.AccountId = acc.Id;
		cas.ContactId = c.Id;
		cas.PayerAccount__c = acc.Id;
		insert cas;

		System.runAs(testUser)
		{
			whoIsThisAbout = new WhoIsThisCallAboutController_IP();
			Test.startTest();
			whoIsThisAbout.startCase = cas;
			Test.stopTest();
		}

		System.assertEquals('Payor', whoIsThisAbout.selectedCallAboutType, 'Call about type was set from case values');
		System.assertNotEquals(null, whoIsThisAbout.accThisCallAbout, 'Contact info was filled in from case');
	}

	@isTest static void testCopyFromCaseProvider()
	{
		generateNewData = new TestCaseDataFactory_IP();
		generateNewData.initTestData();
		testUser = TestCaseDataFactory_IP.csUser;

		Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Payor0' LIMIT 1];
		Account prAcc = [SELECT Id, Name FROM Account WHERE Name = 'Provider0' LIMIT 1];
		Contact c = [SELECT Id, Name, Birthdate,FirstName, LastName, Account.Name, AccountId, RecordTypeId, Account.Status__c, Phone, Email, Health_Plan_ID__c 
					FROM Contact WHERE Health_Plan_ID__c= '123450' AND AccountId =: acc.Id LIMIT 1];

		Case cas = new Case();
		cas.AccountId = acc.Id;
		cas.ContactId = c.Id;
		cas.Provider_Account__c = prAcc.Id;
		insert cas;

		System.runAs(testUser)
		{
			whoIsThisAbout = new WhoIsThisCallAboutController_IP();
			Test.startTest();
			whoIsThisAbout.startCase = cas;
			Test.stopTest();
		}

		System.assertEquals('Provider', whoIsThisAbout.selectedCallAboutType, 'Call about type was set from case values');
		System.assertNotEquals(null, whoIsThisAbout.accThisCallAbout, 'Contact info was filled in from case');
	}
}