@isTest
private class ShowRelatedPayToTest_IP
{
	private static ShowRelatedPayTo_IP scc;
	static Pay_To__c newPay;
	static Account acc;
	static Provider_Location__c pLoc;
	static User testUser;

	static{
		Profile p = [Select id from Profile where Name = 'Integra Standard User'];
		Decimal d = Math.random();
		testUser = new User(LastName = 'User1'+d,
		             Alias = 'use',
		             Email = 'test'+d+'@test.com',
		             Username = 'testingtrigger'+d+'@testingcenter.com',
		             CommunityNickname = 'nick',
		             EmailEncodingKey = 'UTF-8',
		             LanguageLocaleKey = 'en_US',
		             LocaleSidKey = 'en_US',
		             ProfileId = p.Id,
		             TimeZoneSidKey = 'America/New_York'
		);
		insert testUser;
	}

	static void initTest()
	{
		System.runAs(testUser){

			acc = new Account(Name = 'Test Account', Type_Of_Provider__c = 'DME', Legal_Name__c = 'Test Legal Name', Tax_ID__c = '10254');
			insert acc;

			newPay = new Pay_To__c(Taxpayer_Name__c= 'Test 123 Legal Name', Tax_ID__c = '102545456', Mailing_Address__c = '5166 Test Address 1', Mailing_Address_2__c = 'Address 2', City__c = 'Test City',
											State__c = 'Hawaii', Zip__c = '10005-014');
			insert newPay;

			pLoc = new Provider_Location__c(Account__c = acc.Id,Name_DBA__c ='Test Store 1', Pay_To__c = newPay.Id);
			insert pLoc;

			//load the page

			PageReference pageRef = Page.ShowRelatedPayTo_IP;
			pageRef.getParameters().put('Id', pLoc.Id);
			Test.setCurrentPageReference(pageRef);

			ApexPages.StandardController sc = new ApexPages.StandardController(pLoc);
			scc = new ShowRelatedPayTo_IP(sc);
		}
	}

	private static testMethod void testDisplayPayTo(){
		// Given
		initTest();

		//When
		Test.startTest();
		pLoc = scc.proLoc;
		newPay = scc.relatedPayTo;
		Test.stopTest();

		System.assert(pLoc != null);
		System.assert(newPay != null);
	}

	static void initSecondTest()
	{
		System.runAs(testUser){

			initTest();
			delete newPay;

			PageReference pageRef = Page.ShowRelatedPayTo_IP;
			pageRef.getParameters().put('Id', pLoc.Id);
			Test.setCurrentPageReference(pageRef);

			ApexPages.StandardController sc = new ApexPages.StandardController(pLoc);
			scc = new ShowRelatedPayTo_IP(sc);
		}
	}

	private static testMethod void testNullDisplayPayTo(){
		// Given
		initSecondTest();

		//When
		Test.startTest();
		pLoc = scc.proLoc;
		newPay = scc.relatedPayTo;
		Test.stopTest();

		System.assert(pLoc != null);
		System.assert(newPay == null);
	}
}