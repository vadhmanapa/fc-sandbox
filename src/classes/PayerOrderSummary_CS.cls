/**
 *  @Description
 *  @Author Cloud Software LLC
 *  Revision History:
 *		12/23/2015 - karnold - Added header, removed references to DME_Category__c
 *		01/14/2016 - karnold - Removed productCategoryOptions, was an overlooked reference to DME_Category__c
 *		03/30/2016 - karnold - Added code to copy the comments from this order to all recurring orders when updating recurring orders in updateLineItemsAllOrders.
 *		05/19/2016 - karnold - IMOR-12 - Added a list of line items to show line items with duplicate IDs.
 *			Modified addProducts, removeProducts, editLineItems, and updateLineItemsAllOrders to use the new list.
 *		06/15/2016 - jbrown - Added cancelAllOrders() method and modified cancelOrder() to only cancel one order (the current order) when called.
 *		10/07/2016 - vadhmanapa - Added edit permission to render authorization date edit link.
 */
public without sharing class PayerOrderSummary_CS extends PayerPortalServices_CS {

	public static String guestUserId {	get{ return UserInfo.getUserId(); } }
	private final String pageAccountType = 'Payer';

	private PayerOrderServices_CS payerOrderServices;
	private PatientServices_CS patientServices;

	/******* Page Notifications *******/
    public String errorMessage {get;set;}
    public Boolean errorFlag {get;set;}

	public OrderModel_CS orderModel {get;set;}
	public Plan_Patient__c thisPatient {get;set;}

	public ApexPages.Standardsetcontroller recurringOrdersSetCon {get;set;}
	public List<Order__c> recurringOrders {
		get{
			System.debug('Recurring Orders: ' + recurringOrdersSetCon.getRecords());
			return (recurringOrdersSetcon == null ? new List<Order__c>() : recurringOrdersSetCon.getRecords());
		}
		set;
	}

	//Modifiers picklist
	public List<SelectOption> modifiers {
		get {
			if (modifiers != null) {
				return modifiers;
			}
			List<SelectOption> mods = new List<SelectOption>();
			mods.add(new SelectOption('', ''));
			for (Schema.PicklistEntry ple : DME_Line_Item__c.RR_NU__c.getDescribe().getPicklistValues()) {
				mods.add(new SelectOption(ple.getValue(), ple.getLabel()));
			}
			if (portalUser.accountName == 'CenterLight Healthcare') {
				for (Integer i = 1; i <= 8; i++) {
					mods.add(new SelectOption('Z' + i, 'Z' + i));
				}
			}
			return mods;
		}
	}

	//Cancellation of orders
	public String cancelReason {get; set;}
	public String cancelReasonDesc {get; set;}
    public List<SelectOption> getCancelReasons(){
        List<SelectOption> options = new List<SelectOption>{
            new SelectOption(OrderModel_CS.CANCELLED_SUPPLIES_NOT_NEEDED,'Supplies not needed for this authorization period'),
            new SelectOption(OrderModel_CS.CANCELLED_PATIENT_EXPIRED,'Patient expired'),
            new SelectOption(OrderModel_CS.CANCELLED_DUPLICATE_ORDER,'Duplicate order'),
            new SelectOption(OrderModel_CS.CANCELLED_COVERAGE_NOT_ACTIVE,'Coverage no longer active'),
            new SelectOption(OrderModel_CS.CANCELLED_OTHER,'Other (please describe)')
        };

        options.sort();
        options.add(0,new SelectOption('','Please select a reason...'));

        return options;
    }

	public List<Order__c> newRecurringOrders {get;set;}


	/******* File Attachments *******/
	public String attachmentUploadMessage {get;set;}
	public final String attachmentSuccess = 'Attachment uploaded successfully';
	public final String attachmentFailure = 'Error uploading attachment';

	public String selectedAttachmentId {get;set;}

	public Attachment attachment {
		get{
			if(attachment == null){
				attachment = new Attachment();
			}
			return attachment;
		}
		set;
	}
	public List<Attachment> currentAttachments {
		get{
			if(String.isBlank(orderModel.instance.Original_Order__c)){
				return [
					select
						Name,
						BodyLength,
						ParentId,
						CreatedDate,
						OwnerId
					from Attachment
					where ParentId = :orderModel.instance.Id
					order by CreatedDate desc
				];
			}
			else{
				return [
					select
						Name,
						BodyLength,
						ParentId,
						CreatedDate,
						OwnerId
					from Attachment
					where ParentId = :orderModel.instance.Original_Order__c or ParentId = :orderModel.instance.Id
					order by CreatedDate desc
				];
			}
		}
		set;
	}

	/******* Page Permissions *******/
	public Boolean cancelOrderPermission {get{ return portalUser.role.Cancel_Orders__c; }}
	public Boolean editOrderPermission {get{return portalUser.role.Edit_Orders__c;}} // Added by VK on 10/07/2016

	/******* Parameters Needed for Modal *******/

	/******* Order Notes *******/
	//Error message
	public String noteErrorMsg {get;set;}

	//Order notes
	public String noteBody {get;set;}

	//Order note needs attention
	public Boolean flagAttention {get;set;}
	public Boolean unflagAttention {get;set;}
	public String attentionReason {get;set;}
	public List<SelectOption> getAttentionReasons(){
		/*List<SelectOption> options = new List<SelectOption>{
			new SelectOption(OrderModel_CS.STATUS_NEEDS_ATTENTION_AUTH_CORRECTION,'Authorization correction needed'),
			new SelectOption(OrderModel_CS.STATUS_NEEDS_ATTENTION_AUTH_EXPIRED,'Authorization expired'),
			new SelectOption(OrderModel_CS.STATUS_NEEDS_ATTENTION_AUTH_EXTENSION,'Authorization extension needed'),
			new SelectOption(OrderModel_CS.STATUS_NEEDS_ATTENTION_SERVICE_LIMIT_EXCEEDED,'Units approved exceed quantity limit for time period'),
			new SelectOption(OrderModel_CS.STATUS_NEEDS_ATTENTION_UNABLE_TO_CONTACT_PATIENT,'Patient unable to be contacted'),
			// new SelectOption(OrderModel_CS.STATUS_NEEDS_ATTENTION_OTHER,'Other (please describe in detail)'), --> moved to end of the list by VK
			new SelectOption(OrderModel_CS.STATUS_NEEDS_ATTENTION_PATIENT_READMITTED,'Patient was readmitted to facility'),
			// new SelectOption(OrderModel_CS.STATUS_NEEDS_ATTENTION_FUTURE_DOS,'Future DOS') // TODO: Deprecate - Sprint 2/13 - Added by Dima
		};*/

		/*
		****************** NOTE: SPRINT 12/12 - BY VK************************/

		List<SelectOption> options = new List<SelectOption>();
        Map<String, String> reasonVals = new Map<String, String>();
        reasonVals.putAll(OrderStatusMetadataWrapper_IP.getPayorNAQ());

		if(reasonVals.size() > 0) {
			for(String s: reasonVals.keySet()) {
				if(s != OrderStatusMetadataWrapper_IP.payerOtherReason){
					options.add(new SelectOption(reasonVals.get(s), s));
                }
			}	
		}
            
        options.sort();
        options.add(new SelectOption(reasonVals.get(OrderStatusMetadataWrapper_IP.payerOtherReason),OrderStatusMetadataWrapper_IP.payerOtherReason));
		options.add(0,new SelectOption('','Please Select...'));

		return options;
	}

	/******* Product List and Searching *******/
	public List<DMELineItemModel_CS> lineItemEdit {get; set;}
	public List<DMELineItemModel_CS> lineItems {
        get{ return DMELineItemModel_CS.getModels(ProductServices_CS.retrieveDLIsForOrder(orderModel.instance)); }
        set;
    }
	public Map<Id,DMELineItemModel_CS> dmeIdToLineItemEdit {get;set;}
	public Map<Id,DMELineItemModel_CS> dmeIdToLineItem {
		get{
			//Create the map of products
			dmeIdToLineItem = new Map<Id,DMELineItemModel_CS>();

			for(DMELineItemModel_CS dlim : DMELineItemModel_CS.getModels(ProductServices_CS.retrieveDLIsForOrder(orderModel.instance))){
				dmeIdToLineItem.put(dlim.instance.DME__c,dlim);
			}

			return dmeIdToLineItem;
		}
		set;
	}

	public String selectedProductCategory {get;set;}
	public String productSearchString {get;set;}

	public List<DMEModel_CS> productSearchResults {get;set;}

	/******* Order Messages and History *******/

	public List<OrderMessage_CS> orderMessages{
		get
		{
			if(orderModel.instance.Message_History_JSON__c != null){
				return OrderJSONFactory_CS.returnOrderMessages(orderModel.instance.Message_History_JSON__c);
			}
			return new List<OrderMessage_CS>();//Q: Will returning null bust the front end?
		}
		private set;
	}

	public List<OrderHistory_CS> orderHistory{
		get{
			if(orderModel.instance.Order_History_JSON__c != null){
				return orderModel.returnHistory();
			}
			return new List<OrderHistory_CS>();
		}
	}

	/******* Error Handling *******/
	public Boolean lineItemsError {get;set;}
	public String lineItemsErrorMessage {get;set;}

	public Boolean authCodeError {get;set;}
	public Boolean authStartDateError {get;set;}
	public Boolean authEndDateError {get;set;}

	/********* Constructor/Init *********/
	public PayerOrderSummary_CS(){}

	public PageReference init()
	{

		///////////////////////////////////
		// Authentication and Permission
		///////////////////////////////////

		if(!isSecure && !Test.isRunningTest()){
			return DoLogout();
		}

		if(!authenticated)
		{
			return DoLogout();
		}

		if(portalUser.AccountType != pageAccountType)
		{
			return homepage;
		}
        

		///////////////////////////////////
		// Additional init
		///////////////////////////////////

		payerOrderServices = new PayerOrderServices_CS(portalUser.instance.Id);
		patientServices = new PatientServices_CS(portalUser.instance.Id);

		// Get order Id from page
		if(ApexPages.currentPage().getParameters().containsKey('oid'))
		{
			String orderId = ApexPages.currentPage().getParameters().get('oid');

			if(String.isNotBlank(orderId)){
				System.debug('OrderSummary: OrderId: ' + orderId);

				// Find the order based on order id
				Order__c o = payerOrderServices.getOrderById(orderId);

				// If no order is found bail out
				if(o == null || String.isBlank(o.Id)){
					// TODO: Display error for order not found, return to OrdersSummary for now.
					PageReference pr = Page.PayerOrdersSummary_CS;
					pr.setRedirect(true);
					return pr;
				}

				orderModel = new OrderModel_CS(o);

				//Check for recurring information
				if(String.isBlank(o.Original_Order__c)){
					recurringOrdersSetCon = payerOrderServices.getRecurringOrders(orderId);
				}
				else{
					recurringOrdersSetCon = payerOrderServices.getRecurringOrders(o.Original_Order__c);
				}
				recurringOrdersSetCon.setPageSize(10);

				// Find patient based on order
				thisPatient = patientServices.getPlanPatientById(orderModel.instance.Plan_Patient__c);

				// Set up flags for "Needs Attention"
				flagAttention = false;
				unflagAttention = false;
			}
			else{
				// TODO: Display error for order not found, return to OrdersSummary for now.
				PageReference pr = Page.PayerOrdersSummary_CS;
				pr.setRedirect(true);
				return pr;
			}
		}
		else
		{
			// TODO: Display error for order not found, return to OrdersSummary for now.
			PageReference pr = Page.PayerOrdersSummary_CS;
			pr.setRedirect(true);
			return pr;
		}

		return null;
	}

	/********* Page Buttons/Actions *********/
	public PageReference cancelOrder(){
		//TODO: Discuss cancellation state transition

		//Reset errors
        errorFlag = false;
        errorMessage = '';

		//Check for cancellation details
        if(String.isBlank(cancelReason)){
			errorFlag = true;
            errorMessage = 'Please select a reason for cancellation.';

            System.debug('cancelOrder Error: Please select a reason for cancellation.');
            return null;
        }

		//Error if expecting cancellation description but it is not provided
        if(cancelReason == OrderModel_CS.CANCELLED_OTHER && String.isBlank(cancelReasonDesc)){
            errorFlag = true;
            errorMessage = 'Please enter a reason for cancellation.';

            System.debug('cancelOrder Error: Please enter a reason for cancellation.');
            return null;
        }

		System.debug('rejectOrder: Cancelled the order: ' + cancelReason + ' / ' + cancelReasonDesc);


		List<Order__c> toUpdate = new List<Order__c>();

		//Cancel this order
		orderModel.changeStatus(portalUser.instance, OrderModel_CS.STATUS_CANCELLED);
		orderModel.instance.Canceled_Order__c = cancelReason;
		orderModel.instance.Canceled_Order_Description__c = cancelReasonDesc;
		toUpdate.add(orderModel.instance);

		update toUpdate;
		return null;
	}

	public PageReference cancelAllOrders(){
		//Reset errors
        errorFlag = false;
        errorMessage = '';

		//Check for cancellation details
        if(String.isBlank(cancelReason)){
			errorFlag = true;
            errorMessage = 'Please select a reason for cancellation.';

            System.debug('cancelOrder Error: Please select a reason for cancellation.');
            return null;
        }

		//Error if expecting cancellation description but it is not provided
        if(cancelReason == OrderModel_CS.CANCELLED_OTHER && String.isBlank(cancelReasonDesc)){
            errorFlag = true;
            errorMessage = 'Please enter a reason for cancellation.';

            System.debug('cancelOrder Error: Please enter a reason for cancellation.');
            return null;
        }

		System.debug('rejectOrder: Cancelled the order: ' + cancelReason + ' / ' + cancelReasonDesc);


		List<Order__c> toUpdate = new List<Order__c>();

		//Cancel this order
		orderModel.changeStatus(portalUser.instance, OrderModel_CS.STATUS_CANCELLED);
		orderModel.instance.Canceled_Order__c = cancelReason;
		orderModel.instance.Canceled_Order_Description__c = cancelReasonDesc;
		toUpdate.add(orderModel.instance);

		//Cancel all future orders
		for(Order__c o : recurringOrders){
			if((o.Stage__c == OrderModel_CS.STAGE_PENDING_ACCEPTANCE || o.Stage__c == OrderModel_CS.STAGE_RECURRING)
				&& o.Delivery_Window_Begin__c > orderModel.instance.Delivery_Window_Begin__c)
			{
				o.Canceled_Order__c = cancelReason;
				o.Canceled_Order_Description__c = cancelReasonDesc;
				new OrderModel_CS(o).changeStatus(portalUser.instance, OrderModel_CS.STATUS_CANCELLED);
				toUpdate.add(o);
			}
		}

		update toUpdate;
		return null;
	}

	public void createRecurringOrders(){

		//Reset errors
		authCodeError = false;
		authStartDateError = false;
		authEndDateError = false;

		//Check if authorization code is entered
		if(String.isBlank(orderModel.instance.Authorization_Number__c)){
			System.debug('No auth code.');
			authCodeError = true;
			return;
		}

		//Check if authorization from date is entered
		if(orderModel.instance.Recurring__c && orderModel.instance.Authorization_Start_Date__c == null){
			System.debug('No auth start date.');
			authStartDateError = true;
			return;
		}

		//Check if authorization to date is entered
		if(orderModel.instance.Recurring__c && orderModel.instance.Authorization_End_Date__c == null){
			System.debug('No auth end date.');
			authEndDateError = true;
			return;
		}

		//Clear the list of recurring orders
		newRecurringOrders = new List<Order__c>();

		List<Order__c> existingChildOrders = [
			select
				Id,
				Name,
				CreatedDate,
				Stage__c,
				Status__c,
				Delivery_Window_Begin__c,
				Delivery_Window_End__c
			from Order__c
			where Original_Order__c = :orderModel.instance.Id
			order by Delivery_Window_Begin__c asc
		];

		//Start the counter on the original delivery date
		Date counter = orderModel.instance.Delivery_Window_Begin__c.date();

		Integer deliveryWindowSeconds = ((orderModel.instance.Delivery_Window_End__c.getTime() -
			orderModel.instance.Delivery_Window_Begin__c.getTime())/1000).intValue();

		System.debug('Window seconds: ' + deliveryWindowSeconds);
		System.debug('Initializing counter: ' + counter);
		System.debug('Recurring frequency: ' + orderModel.instance.Recurring_Frequency__c);

		if(String.isNotBlank(orderModel.instance.Recurring_Frequency__c)){

			if(orderModel.instance.Recurring_Frequency__c == 'Monthly'){
				counter = counter.addMonths(1);
			}
			else if(orderModel.instance.Recurring_Frequency__c == 'Weekly'){
				counter = counter.addDays(7);
			}
			else if(orderModel.instance.Recurring_Frequency__c == 'Daily'){
				counter = counter.addDays(1);
			}

			System.debug('Counter: ' + counter + ' authEnd: ' + orderModel.instance.Authorization_End_Date__c);

			while(counter <= orderModel.instance.Authorization_End_Date__c){

				Boolean insideAuthPeriod = (counter > orderModel.instance.Authorization_Start_Date__c && counter <= orderModel.instance.Authorization_End_Date__c);
				System.debug('Date ' + counter + ' inside auth period? ' + insideAuthPeriod);

				Boolean orderExists = false;

				//Check if the order exists
				for(Order__c o : existingChildOrders){
					if(o.Delivery_Window_Begin__c.date() == counter){
						orderExists = true;

						//If we are inside the auth period, keep this order else cancel
						if(insideAuthPeriod){
							System.debug('Order ' + o.Name + ' on ' + o.Delivery_Window_Begin__c + ' is being kept.');
							newRecurringOrders.add(o);
						}
						else{
							System.debug('Order ' + o.Name + ' on ' + o.Delivery_Window_Begin__c + ' is being cancelled.');
							new OrderModel_CS(o).changeStatus(portalUser.instance,OrderModel_CS.STATUS_CANCELLED);
							newRecurringOrders.add(o);
						}

						System.debug('Child order ' + o.name + ' exists on ' + counter);
						break;
					}
				}

				//If it doesn't exist...
				if(!orderExists){

					//If we are inside the authPeriod
					if(insideAuthPeriod){
						System.debug('Creating new order on ' + counter);

						//Pointer to the current order
						Order__c currentOrder = orderModel.instance;

						//Create new order
						Order__c newChildOrder = new Order__c();

						//Lookup to parent
						newChildOrder.Original_Order__c = currentOrder.Id;

						//Delivery Information
						newChildOrder.Original_Order__c = orderModel.instance.Id;
						newChildOrder.Delivery_Window_Begin__c = DateTime.newInstance(counter,currentOrder.Delivery_Window_Begin__c.time());
						newChildOrder.Delivery_Window_End__c = newChildOrder.Delivery_Window_Begin__c.addSeconds(deliveryWindowSeconds);

						//Authorization Information
						newChildOrder.Authorization_Number__c = currentOrder.Authorization_Number__c;
						newChildOrder.Authorization_Start_Date__c = currentOrder.Authorization_Start_Date__c;
						newChildOrder.Authorization_End_Date__c = currentOrder.Authorization_End_Date__c;

						//Recurring Information
						newChildOrder.Recurring__c = currentOrder.Recurring__c;
						newChildOrder.Recurring_Frequency__c = currentOrder.Recurring_Frequency__c;

						//Comments and Delivery Instructions
						newChildOrder.Comments__c = currentOrder.Comments__c;
						newChildOrder.Delivery_Instructions__c = currentOrder.Delivery_Instructions__c;

						//Account Information
						newChildOrder.Payer__c = currentOrder.Payer__c;
						newChildOrder.Entered_By__c = currentOrder.Entered_By__c;

			            //Patient Information Record
			            newChildOrder.Plan_Patient__c = currentOrder.Plan_Patient__c;
			            newChildOrder.Patient_ID_Number__c = currentOrder.Patient_ID_Number__c;
			            newChildOrder.Patient_First_Name__c = currentOrder.Patient_First_Name__c;
			            newChildOrder.Patient_Last_Name__c = currentOrder.Patient_Last_Name__c;
			            newChildOrder.Patient_Phone_Number__c = currentOrder.Patient_Phone_Number__c;
			            newChildOrder.Patient_Mobile_Phone_Number__c = currentOrder.Patient_Mobile_Phone_Number__c;
			            newChildOrder.Patient_Email_Address__c = currentOrder.Patient_Email_Address__c;
			            newChildOrder.Patient_Date_of_Birth__c = currentOrder.Patient_Date_of_Birth__c;
			            newChildOrder.Patient_Medicare_ID__c = currentOrder.Patient_Medicare_ID__c;
			            newChildOrder.Patient_Medicaid_ID__c = currentOrder.Patient_Medicaid_ID__c;

			            //Referral Information
			            newChildOrder.Referred__c = currentOrder.Referred__c;
			            newChildOrder.Referred_By_First_Name__c = currentOrder.Referred_By_First_Name__c;
			            newChildOrder.Referred_By_Last_Name__c = currentOrder.Referred_By_Last_Name__c;
			            newChildOrder.Referred_By_Phone__c = currentOrder.Referred_By_Phone__c;

			            //Patient Address Record
			            newChildOrder.Patient_Street__c = currentOrder.Patient_Street__c;
			            newChildOrder.Patient_Apartment_Number__c = currentOrder.Patient_Apartment_Number__c;
			            newChildOrder.Patient_City__c = currentOrder.Patient_City__c;
			            newChildOrder.Patient_State__c = currentOrder.Patient_State__c;
			            newChildOrder.Patient_Zip_Code__c = currentOrder.Patient_Zip_Code__c;

			            //Recipient Information Record
			            newChildOrder.Recipient_First_Name__c = currentOrder.Recipient_First_Name__c;
			            newChildOrder.Recipient_Last_Name__c = currentOrder.Recipient_Last_Name__c;
			            newChildOrder.Recipient_Email_Address__c = currentOrder.Recipient_Email_Address__c;
			            newChildOrder.Recipient_Phone_Number__c = currentOrder.Recipient_Phone_Number__c;
			            newChildOrder.Recipient_Cell_Phone__c = currentOrder.Recipient_Cell_Phone__c;
			            newChildOrder.Recipient_Date_of_Birth__c = currentOrder.Recipient_Date_of_Birth__c;
			            newChildOrder.Recipient_Relationship__c = currentOrder.Recipient_Relationship__c;

			            //Recipient Address Record
			            newChildOrder.Street__c = currentOrder.Street__c;
			            newChildOrder.Apartment_Number__c = currentOrder.Apartment_Number__c;
			            newChildOrder.City__c = currentOrder.City__c;
			            newChildOrder.State__c = currentOrder.State__c;
			            newChildOrder.Zip__c = currentOrder.Zip__c;

						//Finally change status
						new OrderModel_CS(newChildOrder).changeStatus(portalUser.instance,OrderModel_CS.STATUS_RECURRING);

						newRecurringOrders.add(newChildOrder);
					}
				}

				if(orderModel.instance.Recurring_Frequency__c == 'Monthly'){
					counter = counter.addMonths(1);
				}
				else if(orderModel.instance.Recurring_Frequency__c == 'Weekly'){
					counter = counter.addDays(7);
				}
				else if(orderModel.instance.Recurring_Frequency__c == 'Daily'){
					counter = counter.addDays(1);
				}
			}
		}
	}

	public void saveRecurringOrders(){

		//Update authorization information for each existing child order
		for(Order__c o : recurringOrders){
			o.Authorization_Number__c = orderModel.instance.Authorization_Number__c;
			o.Authorization_Start_Date__c = orderModel.instance.Authorization_Start_Date__c;
			o.Authorization_End_Date__c = orderModel.instance.Authorization_End_Date__c;
		}

		update recurringOrders;

		//Find orders that need to be cancelled
		List<Order__c> toCancel = new List<Order__c>();

		for(Order__c existingOrder : recurringOrders){
			Boolean saveOrder = false;

			for(Order__c newOrder : newRecurringOrders){
				if(existingOrder.Id == newOrder.Id || existingOrder.Stage__c == OrderModel_CS.STAGE_COMPLETED){
					saveOrder = true;
				}
			}

			if(!saveOrder){
				System.debug('Cancelling order Id/Name: ' + existingOrder.Id + '/' + existingOrder.Name);
				new OrderModel_CS(existingOrder).changeStatus(portalUser.instance,OrderModel_CS.STATUS_CANCELLED);
				toCancel.add(existingOrder);
			}
		}

		Database.Saveresult[] cancelResults = Database.update(toCancel);

		for(Database.Saveresult r : cancelResults){
			if(!r.isSuccess()){
				System.debug('saveRecurringOrders: Failed to delete one or more orders.');
			}
		}

		List<Order__c> toInsert = new List<Order__c>();

		for(Order__c newOrder : newRecurringOrders){
			if(String.isBlank(newOrder.Id)){
				toInsert.add(newOrder);
			}
		}

		//Insert the new orders
		Database.Saveresult[] newOrderResults = Database.insert(toInsert);

		for(Order__c newOrder : toInsert){
			List<DME_Line_Item__c> newLineItems = [
				Select
					d.RR_NU__c,
					d.Quantity__c,
					d.Product_Name__c,
					d.Product_Code__c,
					d.Order__c,
					d.Name,
					d.IsDeleted,
					d.Id,
					d.Delivery_Window_End__c,
					d.Delivery_Window_Begin__c,
					d.Deliver_By__c,
					d.DME__c
				From DME_Line_Item__c d
				Where d.Order__c = :orderModel.instance.Id
			].deepClone(false,false,false);

			for(DME_Line_Item__c dli : newLineItems){
				dli.Order__c = newOrder.Id;
			}

			Database.Saveresult[] lineItemresults= Database.insert(newLineItems);

			for(Database.Saveresult r : lineItemResults){
				if(!r.isSuccess()){
					System.debug('saveRecurringOrders: Failed to upsert one or more orders.');
				}
			}
		}

		for(Database.Saveresult r : newOrderResults){
			if(!r.isSuccess()){
				System.debug('saveRecurringOrders: Failed to upsert one or more orders.');
			}
		}

		//Update the current order
		Database.Saveresult updateResult = Database.update(orderModel.instance);

		if(!updateResult.isSuccess()){
			System.debug('saveRecurringOrders: Failed to update this order.');
		}

		//Update the list of recurring orders on the page
		recurringOrdersSetCon = payerOrderServices.getRecurringOrders(orderModel.instance.Id);
	}

	/********* Notes Section *********/
	public void createNewMessage(){

		System.debug('Creating new message.\nMessage: ' + noteBody + '\nFlagAttention : ' + flagAttention +
			'\nAttention reason: ' + attentionReason + '\nUnFlagAttention: ' + unflagAttention);

		//Reset error messages
		noteErrorMsg = '';

		//Do not allow blank notes unless flagging or unflagging.
		if(String.isBlank(noteBody) && !flagAttention && !unflagAttention){
			System.debug('ERROR: No note body.');

			//Set page error
			noteErrorMsg = 'Please enter contents for the note.';

			return;
		}

		//If flagging/unflagging attention
		if(flagAttention){

			System.debug('Attention reason: ' + attentionReason);

			//Verify status is selected
			if(String.isBlank(attentionReason)){
				System.debug(LoggingLevel.ERROR,'ERROR: No reason selected for flagging attention.');

				//Set page error message
				noteErrorMsg = 'Please select a reason for attention.';

				return;
			}

			orderModel.changeStatus(portalUser.instance,attentionReason);

			/*
			if(attentionReason == OrderModel_CS.STATUS_NEEDS_ATTENTION_INCOMPLETE_DELIVERY_INFO){
				orderModel.changeStatus(portalUser.instance, OrderModel_CS.STATUS_NEEDS_ATTENTION_INCOMPLETE_DELIVERY_INFO);
			}
			else if(attentionReason == OrderModel_CS.STATUS_NEEDS_ATTENTION_OTHER){
				orderModel.changeStatus(portalUser.instance, OrderModel_CS.STATUS_NEEDS_ATTENTION_OTHER);
			}
			*/

		} else if(unflagAttention){

			String currentStatus = orderModel.instance.Status__c;

			if(orderHistory.size() < 2){
				System.debug(LoggingLevel.ERROR,'ERROR: Order has no status before current.');
				return;
			}


			String lastStatus = orderHistory[1].orderStatus;

			Boolean changeResult = orderModel.changeStatus(portalUser.instance, lastStatus);

			if(!changeResult){
				System.debug(LoggingLevel.ERROR,'ERROR: Order status change failed from ' + currentStatus + ' to ' + lastStatus);
				return;
			}
		}

		//Valid Note, be sure to quite before getting here if not
		orderModel.instance.Message_Body__c = noteBody;
		OrderJSONFactory_CS.processOrderMessage(portalUser.instance, orderModel.instance);
		orderModel.instance.Message_Body__c = '';

		orderModel.instance.LastModifiedDate_Notes__c = DateTime.now();

		update orderModel.instance;

		//Reset the note on the page
		noteBody = '';
		attentionReason = '';
		flagAttention = false;
		unflagAttention = false;

		//Update the order model
		Id orderId = orderModel.instance.Id;
		orderModel = new OrderModel_CS(payerOrderServices.getOrderById(orderId));

	}//End createNewMessage

	/******* Line Item Modification *******/

	public void searchProducts(){

		System.debug('Search Category: ' + selectedProductCategory + ' Term: ' + productSearchString);

		//Only perform a search if at least one field is populated
		if(String.isNotBlank(selectedProductCategory) || String.isNotBlank(productSearchString)){
			productSearchResults = DMEModel_CS.getDMEModelList(ProductServices_CS.retrieveDMEProducts(productSearchString));
		}
		else{
			System.debug('searchProducts: No search performed.');
			productSearchResults = new List<DMEModel_CS>();
		}

		System.debug('searchProducts: Found ' + productSearchResults.size() + ' products');
	}

	public void addProducts(){
		//List of new line items to update/insert
		List<DME_Line_Item__c> toUpsert = new List<DME_Line_Item__c>();

		for(DMEModel_CS dme : productSearchResults){

			if(dme.selected){

				//Check if this particular product already exists in the line items
				if (!dmeIdToLineItemEdit.containsKey(dme.instance.Id) || dme.instance.Multiple_Line_Items__c == true) {
					//Create new product pointing to this order
					DME_Line_Item__c dli = new DME_Line_Item__c(
						Order__c = orderModel.instance.Id,
						DME__c = dme.instance.Id,
						DME__r = dme.instance,
						Quantity__c = 1
					);
					DMELineItemModel_CS dlim = new DMELineItemModel_CS(dli);
					dlim.itemCode = dme.instance.Name;
					dlim.productName = dme.instance.Description__c;

					lineItemEdit.add(dlim);
					dmeIdToLineItemEdit.put(dli.DME__c, dlim);
				} else {
					//Pointer to line item instance
					//Add one to the quantity
					dmeIdToLineItemEdit.get(dme.instance.Id).instance.quantity__c ++;
				}
			}
		}
	}

	public void removeProducts(){

		List<DMELineItemModel_CS> newLineItems = new List<DMELineItemModel_CS>();
		Map<Id,DMELineItemModel_CS> newLineItemMap = new Map<Id,DMELineItemModel_CS>();

		for(DMELineItemModel_CS dlim : lineItemEdit){
			System.debug('Product: ' + dlim.instance.DME__r.Name + '/' + dlim.instance.DME__r.Description__c + ' selected: ' + dlim.selected);

			if (dlim.selected == false) {
				newLineItems.add(dlim);
				newLineItemMap.put(dlim.instance.DME__c, dlim);
			} else {
				System.debug('Deleting: ' + dlim.instance);
				dmeIdToLineItemEdit.remove(dlim.instance.DME__c);
			}
		}

		lineItemEdit = newLineItems;
		dmeIdToLineItemEdit = newLineItemMap;
	}

	public void editLineItems(){

		//Reset the search
		selectedProductCategory = '';
	 	productSearchString = '';
		searchProducts();

		//Set up the temporary list of line items
		dmeIdToLineItemEdit = dmeIdToLineItem.clone();
		lineItemEdit = lineItems.clone();
	}

	public void updateLineItemsThisOrder(){

		//Reset errors
		lineItemsError = false;
		lineItemsErrorMessage = '';

		//Check if line items exist
		if(lineItemEdit.size() == 0){
			lineItemsError = true;
			lineItemsErrorMessage = 'Please select products to continue.';
			System.debug(lineItemsErrorMessage);
			return;
		}

		orderModel.updateLineItems(DMELineItemModel_CS.getRecords(lineItemEdit));
		System.debug('Returned from OrderModel_CS');
	}

	public void updateLineItemsAllOrders(){

		//Reset error message
		lineItemsError = false;
		lineItemsErrorMessage = '';

		//Check if line items exist
		if(lineItemEdit.size() == 0){
			lineItemsError = true;
			lineItemsErrorMessage = 'Please select products to continue.';
			System.debug(lineItemsErrorMessage);
			return;
		}

		//Update this order first
		orderModel.updateLineItems(DMELineItemModel_CS.getRecords(lineItemEdit));

		//Update all child orders
		for(Order__c o : recurringOrders){
			if((o.Stage__c == OrderModel_CS.STAGE_PENDING_ACCEPTANCE || o.Stage__c == OrderModel_CS.STAGE_RECURRING)
				&& o.Delivery_Window_Begin__c > orderModel.instance.Delivery_Window_Begin__c)
			{
				System.debug('Updating order: ' + o.Name);

				// Code will update the comments on all recurring orders to match this order's comments.
				// This was implemented in order to allow adding Miscellaneous line items
				o.Comments__c = orderModel.instance.Comments__c;
				new OrderModel_CS(o).updateLineItems(DMELineItemModel_CS.getRecords(lineItemEdit));
			}
		}
	}

	public void cancelLineItemEdit(){
		//Reset errors
		lineItemsError = false;
		lineItemsErrorMessage = '';
	}

	/******* File Attachments *******/

	public void uploadAttachment(){

		//Reset upload message
		attachmentUploadMessage = '';

		attachment.OwnerId = UserInfo.getUserId();
		attachment.ParentId = orderModel.instance.Id;
		attachment.IsPrivate = false;

		try{
			insert attachment;
		}
		catch(DMLException e){
			attachmentUploadMessage = attachmentFailure;
			return;
		}

		attachmentUploadMessage = attachmentSuccess;

		attachment = new Attachment();
	}

	public void deleteAttachment(){

		System.debug('selectedAttachment: ' + selectedAttachmentId);

		if(String.isNotBlank(selectedAttachmentId)){
			for(Attachment a : currentAttachments){
				System.debug('file id: ' + a.Id);

				if(String.valueOf(a.Id) == selectedAttachmentId){
					delete a;
				}
			}
		}
	}
}