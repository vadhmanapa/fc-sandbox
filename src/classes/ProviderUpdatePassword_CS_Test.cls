@isTest
private class ProviderUpdatePassword_CS_Test {
	public static Account provider;
	public static Contact user;
	
    static testMethod void ProviderUpdatePassword_CS_Test() {
        init();
        Test.setCurrentPage(Page.ProviderUpdatePassword_CS);
        TestServices_CS.login(user);
        
        ProviderUpdatePassword_CS cont = new ProviderUpdatePassword_CS();
        cont.init();
        system.assertEquals(true, cont.authenticated);
        system.assertEquals('', cont.newPasswordMessage);
        
    	cont.newPassword = PasswordServices_CS.randomPassword(10);
    	cont.newPasswordConfirm = cont.newPassword;
    	
    	test.startTest();
    	cont.changePassword();
    	test.stopTest();
		
		system.assertEquals('Password successfully changed', cont.newPasswordMessage);    	
    }
    
    static testMethod void ProviderUpdatePassword_CS_initNotAuthenticated(){
    	init();
    	Test.setCurrentPage(Page.ProviderUpdatePassword_CS);
    	
    	ProviderUpdatePassword_CS cont = new ProviderUpdatePassword_CS();
    	cont.init();
    }
    
    static testMethod void ProviderUpdatePassword_CS_initWrongAccountType(){
    	init();
    	Account plan = TestDataFactory_CS.generatePlans('TestPlan', 1)[0];
    	insert plan;
    	user.Entity__c = plan.Id;
    	update user;
    	
    	Test.setCurrentPage(Page.ProviderUpdatePassword_CS);
    	TestServices_CS.login(user);
    	
    	ProviderUpdatePassword_CS cont = new ProviderUpdatePassword_CS();
    	cont.init();
    }
    
    static testMethod void ProviderUpdatePassword_CS_changePasswordNullUser(){
    	Test.setCurrentPage(Page.ProviderUpdatePassword_CS);
    	
    	ProviderUpdatePassword_CS cont = new ProviderUpdatePassword_CS();
    	cont.changePassword();
    }
    
    static testMethod void ProviderUpdatePassword_CS_changePasswordNull(){
    	init();
    	Test.setCurrentPage(Page.ProviderUpdatePassword_CS);
    	TestServices_CS.login(user);
    	
    	ProviderUpdatePassword_CS cont = new ProviderUpdatePassword_CS();
    	cont.changePassword();
    	system.assertEquals('Error: No password data found', cont.newPasswordMessage);
    }
    
    static testMethod void ProviderUpdatePassword_CS_changePasswordMissmatch(){
    	init();
    	Test.setCurrentPage(Page.ProviderUpdatePassword_CS);
    	TestServices_CS.login(user);
    	
    	ProviderUpdatePassword_CS cont = new ProviderUpdatePassword_CS();
    	cont.newPassword = 'testNew1';
    	cont.newPasswordConfirm = 'testNew2';
    	cont.changePassword();
    	system.assertEquals('Error: New passwords don\'t match.', cont.newPasswordMessage);
    }
    
    static testMethod void ProviderUpdatePassword_CS_changePasswordFail(){
    	init();
    	Test.setCurrentPage(Page.ProviderUpdatePassword_CS);
    	TestServices_CS.login(user);
    	
    	ProviderUpdatePassword_CS cont = new ProviderUpdatePassword_CS();
    	cont.newPassword = 'tn';
    	cont.newPasswordConfirm = 'tn';
    	cont.changePassword();
    	system.assertEquals('Error: Password change failed.', cont.newPasswordMessage);
    }
    
    public static void init(){
    	List<Account> initAccts = TestDataFactory_CS.generateProviders('TestProv', 1);
    	provider = initAccts[0];
    	insert provider;
    	
    	user = TestDataFactory_CS.createProviderContacts('Admin', initAccts, 1)[0];
    	
    }
}