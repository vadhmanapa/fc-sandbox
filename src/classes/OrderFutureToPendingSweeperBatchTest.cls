@isTest
private class OrderFutureToPendingSweeperBatchTest {

    private static List<Order__c> initOrders(Integer numberOfOrders) {
        List<Order__c> orders = new List<Order__c>();
        for (Integer i = 0; i < numberOfOrders; i++) {
            Order__c order = new Order__c(
                Authorization_Number__c = String.valueOf(i)
                , Status__c = OrderModel_CS.STATUS_FUTURE_DOS
                , Estimated_Delivery_Time__c = Datetime.now().addDays(-2)
            );
            orders.add(order);
        }
        insert orders;
        return orders;
    }

    @isTest
    static void testScheduleWithSomeFutureDosOrders() {
        initOrders(5);
        Test.startTest();
        String cronExp = '0 0 4 ? * SUN';
        System.schedule('Test Sweeper Job', cronExp, new OrderFutureToPendingSweeperBatch());
        Test.stopTest();
    }

    @isTest
    static void testScheduleWithNoOrders() {
        Test.startTest();
        String cronExp = '0 0 4 ? * SUN';
        System.schedule('Test Sweeper Job', cronExp, new OrderFutureToPendingSweeperBatch());
        Test.stopTest();
    }

}