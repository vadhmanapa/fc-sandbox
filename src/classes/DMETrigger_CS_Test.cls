/**
 *  @Description Test class for the trigger DME_Trigger_CS
 *  @Author Cloud Software LLC
 *  Revision History: 
 *		12/23/2015 - karnold - Added header, removed refernces to DME_Catgeory__c. Cleaned code.
 *
 */
@isTest
private class DMETrigger_CS_Test {
	private static DME_Settings__c dmeSettings;
	private static List<DME__c> testDMEs;
	private static final String fieldValidationError = 'FIELD_CUSTOM_VALIDATION_EXCEPTION, ';

	private static void initTestData() {
		dmeSettings = TestDataFactory_CS.generateDMESettings();
		insert dmeSettings;

		testDMEs = TestDataFactory_CS.generateDMEs(3);
	}

	private static testMethod void DMETrigger_CS_TestDuplicateDMECode1() {
		//Add DMEs to database, insert new dme record with same Code, cause error
		initTestData();
		insert testDMEs;
		//testDMEs[1].Name = 'A0000';
		try {
			DME__c newDME = new DME__c();
			newDME.Name = 'A0000';
			insert newDME;
			system.assert(false);
		} catch(Exception e) {
			system.assert(true);
			system.debug('### exception message: ' + e.getMessage());
		}

	} 

	static testMethod void DMETrigger_CS_TestDuplicateDMECode2() {
		//Ensure new entries are checked against themselves for duplication
		initTestData();

		List<DME__c> dmeList = new List<DME__c> ();
		for (Integer i = 0; i < 2; i++) {
			DME__c d = new DME__c();
			d.Name = 'A0000';
			dmeList.add(d);
		}

		try {
			insert dmeList;
			system.assert(false);
		} catch(Exception e) {
			system.debug('### exception message: ' + e.getMessage());
			system.assert(true);
		}

	}

	/*static testMethod void DMETrigger_CS_TestDuplicateDMECode3() {
		//Ensure updated entries are checked against themselves for duplication
		initTestData();
		insert testDMEs;
		DME__c dmeToUpdate = [Select Name From DME__c Where Name = 'A0001'];
		system.debug('### database DMES before: ' + [Select Name From DME__c]);
		try {
			dmeToUpdate.Name = 'A0000';
			update dmeToUpdate;
			system.debug('### database DMES after: ' + [Select Name From DME__c]);
			system.assert(false);
		} catch(Exception e) {
			system.debug('### exception message: ' + e.getMessage());
			system.assert(true);
		}

	}*/

	static testMethod void DMETrigger_CS_TestMaxDeliverySet() {

		initTestData();
		insert testDMEs;

		System.assertEquals(48, [SELECT Max_Delivery_Time__c FROM DME__c Limit 1].Max_Delivery_Time__c);
	}
}