public without sharing class DateTimeModel_CS {

	public static final String START_OF_WEEK = 'Monday';

	public String userLocale;	
	public DateTime dt;

	public static Map<String,Integer> localeToGMTOffset = new Map<String,Integer>{
		'US/Alaska' => -9,
		'US/Aleutian' => -10,
		'US/Arizona' => -7,
		'US/Central' => -6,
		'US/East-Indiana' => -5,
		'US/Eastern' => -5,
		'US/Hawaii' => -10,
		'US/Indiana-Starke' => -6,
		'US/Michigan' => -5,
		'US/Mountain' => -7,
		'US/Pacific' => -8,
		'US/Pacific-New' => -8,
		'US/Samoa' => -11		
	};
	
	public static Map<String,Boolean> localeToDST = new Map<String,Boolean>{
		'US/Alaska' => true,
		'US/Aleutian' => true,
		'US/Arizona' => false,
		'US/Central' => true,
		'US/East-Indiana' => true,
		'US/Eastern' => true,
		'US/Hawaii' => false,
		'US/Indiana-Starke' => true,
		'US/Michigan' => true,
		'US/Mountain' => true,
		'US/Pacific' => true,
		'US/Pacific-New' => true,
		'US/Samoa' => false
	};
	
	public static Boolean inDaylightSavings(String locale){
		
		if(DateTime.now() > dstStart(locale) && DateTime.now() < dstEnd(locale)){
			return true;
		}
		
		return false;
	}
	
	public static DateTime dstStart(String locale){
		//Find the local year from GMT time plus local offset
		Integer localYear = DateTime.now().addHours(localeToGMTOffset.get(locale)).yearGmt();
			
		//Daylight savings starts at 2:00AM on the second Sunday in March
		DateTime d = DateTime.newInstance(Date.newInstance(localYear, 3,1),Time.newInstance(2,0,0,0));

		//Find the first Sunday in March and add a week
		while(d.formatGmt('EEE') != 'Sun'){
			d = d.addDays(1);
		}
		d.addDays(7);
			
		//Convert from the "local" time back to gmt time
		d.addHours(-1*localeToGMTOffset.get(locale));
			
		return d;
	}
	
	public static DateTime dstEnd(String locale){
		//Find the local year from GMT time plus local offset
		Integer localYear = DateTime.now().addHours(localeToGMTOffset.get(locale)).yearGmt();
		
		//Pretend for the moment that this GMT time is in local time
		DateTime d = DateTime.newInstanceGmt(Date.newInstance(localYear,11,1),Time.newInstance(2,0,0,0));
		
		//Daylight savings ends at 2:00AM the first Sunday in November
		while(d.formatGmt('EEE') != 'Sun'){
			d = d.addDays(1);
		}
		
		//Convert from the "local" time back to gmt time
		d.addHours(-1*localeToGMTOffset.get(locale));
		
		return d;
	}
	
	public static Integer gmtOffset(String locale){
		if( localeToDST.get(locale) &&
			DateTime.now() > dstStart(locale) && DateTime.now() < dstEnd(locale))
		{
			return localeToGMTOffset.get(locale) + 1;
		}
		else{
			return localeToGMTOffset.get(locale);
		}
	}

	public static Date today(String locale){
		return DateTime.now().addHours(gmtOffset(locale)).dateGmt();
	}

	public static Time now(String locale){
		return DateTime.now().addHours(gmtOffset(locale)).timeGmt();
	}

	public static Time localTime(DateTime dt, String locale){
		return dt.addHours(gmtOffset(locale)).timeGmt();
	}
	
	public static Date localDate(DateTime dt, String locale){
		return dt.addHours(gmtOffset(locale)).dateGmt();
	}
	
	public Time localTime(){
		return this.dt.addHours(gmtOffset(userLocale)).timeGmt();
	}
	
	public Date localDate(){
		return this.dt.addHours(gmtOffset(userLocale)).dateGmt();
	}
	
	public String format(){
		return dt.addHours(gmtOffset(userLocale)).formatGmt('M/dd/yyyy HH:mm ') + userLocale;
	}
	
	public String format(String dateFormat){
		return dt.format(dateFormat,userLocale);
	}

	public String formatGmt(String dateFormat){
		return dt.formatGmt(dateFormat);
	}

	public DateTimeModel_CS addDays(Integer days){
		return new DateTimeModel_CS(this.dt.addDays(days),this.userLocale);
	}

	public DateTimeModel_CS addHours(Integer hours){
		return new DateTimeModel_CS(this.dt.addHours(hours),this.userLocale);
	}
	
	public DateTimeModel_CS addMinutes(Integer minutes){
		return new DateTimeModel_CS(this.dt.addMinutes(minutes),this.userLocale);
	}

	public void toStartOfWeek(){
		while(this.format('EEEEEEEE') != START_OF_WEEK){
			system.debug(LoggingLevel.INFO,this.format('EEEEEEEE'));
			this.addDays(-1);
		}
	}

	public static DateTimeModel_CS newInstanceFromGMT(Date d, Time t, String locale){
		return new DateTimeModel_CS(DateTime.newInstanceGmt(d,t),locale);
	}
	
	public static DateTimeModel_CS newInstanceFromLocal(Date d, Time t, String locale){
		return new DateTimeModel_CS(DateTime.newInstanceGmt(d,t).addHours(-1*gmtOffset(locale)),locale);
	}

	public DateTimeModel_CS(DateTime dt, String locale){
		if(localeToGMTOffset.containsKey(locale)){
			this.dt = dt;
			this.userLocale = locale;
		}
	}	
}