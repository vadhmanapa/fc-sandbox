/**
 *  @Description  Test class 
 *  @Author Cloud Software LLC
 *  Revision History: 
 *		01/11/2016 - karnold - Formatted code. Added header. 
 *			Trigger no longer exists, test code is deprecated.
 *		03/03/2016 - vadhmanapa - Enabling code back and editing Provider_Locations__c to Provider_Location__c
 */
@isTest
public class StateIdentifierTest_IP {
	static testMethod void testMedicaidAddDelete() {
		//user profile

		Profile p = [Select id from Profile where Name = 'Integra Standard User'];
		User u = new User(LastName = 'User1',
			Alias = 'use',
			Email = 'test@test.com',
			Username = 'testingtrigger@testingcenter.com',
			CommunityNickname = 'nick',
			EmailEncodingKey = 'UTF-8',
			LanguageLocaleKey = 'en_US',
			LocaleSidKey = 'en_US',
			ProfileId = p.Id,
			TimeZoneSidKey = 'America/New_York'
		);
		insert u;

		System.runAs(u) {

			// create account, provider location

			Account a = new Account(Name = 'Test Account', Type_Of_Provider__c = 'DME');
			insert a;

			Provider_Location__c pl = new Provider_Location__c(Account__c = a.Id, Name_DBA__c = 'Test Account Store');
			insert pl;

			// start creating new county items

			List<State_Identifier__c> addSid = new List<State_Identifier__c> ();

			for (String s : new String[] 
				{ 'Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut',
				'Delaware', 'Florida1', 'Florida2', 'Georgia', 'Hawaii', 'Idaho', 'Illinois',
				'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts',
				'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada',
				'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
				'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota',
				'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia',
				'Wisconsin', 'Wyoming' }
			) {
				String m = s + '123';
				addSid.add(new State_Identifier__c(Provider_Location_New__c = pl.id, State__c = s, Medicaid__c = m));
			}

			insert addSid;

			List<State_Identifier__c> getSid = [Select Id, State__c, Medicaid__c from State_Identifier__c where Provider_Location_New__c = :pl.Id];

			System.debug(LoggingLevel.INFO, 'getSid: ' + getSid);
			System.debug(LoggingLevel.INFO, 'pl.Id: ' + pl.Id);

			Provider_Location__c ploc = [Select Id, Alabama__c,Florida_1__c,Florida_2__c from Provider_Location__c where Id = :pl.Id LIMIT 1];

			// Check the case for each state if necessary

			System.assertEquals('Alabama123', ploc.Alabama__c, 'Alabama from SID was copied to PL');
			System.assertEquals('Florida1123', ploc.Florida_1__c, 'Florida1 from SID was copied to PL');
			System.assertEquals('Florida2123', ploc.Florida_2__c, 'Florida2 from SID was copied to PL');
			System.assertNotEquals(null, ploc.Alabama__c, 'Alabama from SID was not copied to PL');

			// check deletion


			delete getSid;

			Provider_Location__c ploc2 = [Select Id, Alabama__c from Provider_Location__c where Id = :pl.Id LIMIT 1];

			System.assertEquals(null, ploc2.Alabama__c, 'The deletion trigger deleted Alabama field in PL');
		}
	}
}