/**
 *  @Description Class is responsible for Generating Test Data for Sandbox testing and unit tests
 *  @Author Cloud Software LLC
 *  Revision History: 
 *      12/23/2015 - karnold - Added header, removed references to DME_Category__c. Added @isTest.
 *      01/07/2016 - karnold - Removed references to deprecated class ClearAlgorithmRoutingCustomSetting_Test
 *      01/08/2016 - karnold - Added generateProviderCategory.
 *		01/12/2016 - karnold - Added Active__c = true to generateDMEs method
 */
 @isTest
public class TestDataFactory_CS {
    
    public static final Integer TEMPID_LEN = 10;
    
    public List<Patient__c> patientsToReturn {get;set;}
    public List<Account> plansToReturn {get;set;}
    public List<Contact> planContactsToReturn {get;set;}
    public List<Plan_Patient__c> planPatientsToReturn {get;set;}
    public List<Account> providersToReturn {get;set;}
    public List<Contact> providerContactsToReturn {get;set;}
    public List<DME__c> dmesToReturn {get;set;}
    public List<Order__c> ordersToReturn {get;set;}
    public List<DME_Line_Item__c> lineItemsToReturn {get;set;}

    public static String getTempId() { 
        return RandomStringUtils.randomNumeric(TEMPID_LEN);
    }
    
    public static String getTempId(Integer len) {
        return RandomStringUtils.randomNumeric(len);
    }

    public static Datetime getStartOfTheWeek() {
        DateTime startOfWeek;
        if(Date.today().toStartOfWeek() == Date.today()){
            startOfWeek = DateTime.newInstance(Date.today().addDays(-6),Time.newInstance(0,0,0,0));
        } else {
            startOfWeek = DateTime.newInstance(Date.today().toStartOfWeek().addDays(1),Time.newInstance(0,0,0,0));
        }
        return startOfWeek;
    }

	public static Clear_Algorithm_Settings__c generateClearAlgorithmSettings(Boolean isInsertable) {
		return generateClearAlgorithmSettings(isInsertable, 'Draft');
	}

	// Override to allow non 'Draft' Settings
    public static Clear_Algorithm_Settings__c generateClearAlgorithmSettings(Boolean isInsertable, String status) {
        Clear_Algorithm_Settings__c settings = new Clear_Algorithm_Settings__c(
            GA_New_Patients__c = 5.000,
            BA_Accessibility_Rate__c = 5.000,
            Active__c = false,
            BA_Average_Delivery_Completion_Time__c = 5.000,
            BA_Average_Delivery_Time__c = 5.000,
            BA_Delivery_Time__c = 5.000,
            GA_Direct_Delivery_Provider__c = 5.000,
            GA_Direct_Relationship__c = 5.000,
            GA_Drive_Time__c = 5.000,
            Existing_Provider_Max__c = 5,
            GA_Languages_Spoken__c = 5.000,
            GA_Look_Back_Period__c = '30 Days',
            GA_Max_Number_Providers__c = 5.000,
            New_Provider_Time_Period_Max_Orders__c = 5,
            New_Provider_Time_Period_Num_Days__c = 5,
            BA_Percent_Orders_Accepted__c = 5.000,
            BA_Percent_Orders_Reassigned__c = 5.000,
            BA_Percent_Orders_Rejected__c = 5.000,
            BA_Percent_Orders_Updated_as_Completed__c = 5.000,
            BA_Speed_of_Acceptance__c = 5.000,
            Status__c = status,
            Tier_1_From__c = 5,
            Tier_1_Max_Orders__c = 5,
            Tier_2_From__c = 5,
            Tier_2_Max_Orders__c = 5,
            Tier_3_From__c = 5,
            Tier_3_Max_Orders__c = 5,
            Use_Attribute_Accepting_New_Patients__c = true,
            Use_Attribute_Accessibility_Rates__c = true,
            Use_Attribute_Average_Delivery_CompTime__c = true,
            Use_Attribute_Average_Delivery_Time__c = true,
            Use_Attribute_Delivery_Time__c = true,
            Use_Attribute_Direct_Delivery_Provider__c = true,
            Use_Attribute_Direct_Relationship__c = true,
            Use_Attribute_Drive_Time__c = true,
            Use_Attribute_Existing_Provider_Daily__c = true,
            Use_Attribute_Languages_Spoken__c = true,
            Use_Attribute_Look_Back_Period__c = true,
            Use_Attribute_Max_Number_of_Providers__c = true,
            Use_Attribute_New_Provider_Time_Period__c = true,
            Use_Attribute_Percent_Orders_Accepted__c = true,
            Use_Attribute_Percent_Orders_Reassigned__c = true,
            Use_Attribute_Percent_Orders_Rejected__c = true,
            Use_Attribute_Per_Orders_Updated_as_Comp__c = true,
            Use_Attribute_Speed_of_Acceptance__c = true,
            Use_Attribute_Tier_1_Range__c = true,
            Use_Attribute_Tier_2_Range__c = true,
            Use_Attribute_Tier_3_Range__c = true
        );
        if(isInsertable == true) {
            insert settings;
        }
        return settings;
    }
    public static List<Clear_Algorithm_Settings__c> generateMultipleClearAlgorithmSettings(Integer num) {
        
        Boolean isInsertable = false;
        List<Clear_Algorithm_Settings__c> settingsList = new List<Clear_Algorithm_Settings__c>();
        for(Integer i = 1; i <= num; i++) {
            Clear_Algorithm_Settings__c testSettings = generateClearAlgorithmSettings(isInsertable);
            settingsList.add(testSettings);
        }
        insert settingsList;
        return settingsList;
    }
    
    public TestDataFactory_CS(){
        initializePlatform();
    }
    
    public static Role__c generateAdminRole() {
        return new Role__c(
            Name = 'Admin',
            Accept_Orders__c = true,
            Approve_Recurring_Orders__c = true,
            Cancel_Orders__c = true,
            Change_Provider__c = true,
            Create_Orders__c = true,
            Create_Plan_Patients__c = true,
            Create_Users__c = true,
            Edit_Orders__c = true,
            Edit_Plan_Patients__c = true,
            Edit_Users__c = true,
            Filter_by_User__c = true,
            Mark_Orders_Delivered__c = true,
            Mark_Orders_Shipped__c = true,
            Mark_Orders_Future_DOS__c = true, // Sprint 2/13 - Added by Dima
            Reject_Orders__c = true,
            Reset_Password__c = true,
            Reset_User_Name__c = true,
            Super_User__c = true,
            View_Other_Users_Orders__c = true,
            View_Patients__c = true
        );
    }
    
    //begin fields needed for Order__c
    public static List<Patient__c> generatePatients(String lastName, Integer recordCount){
        
        List<Patient__c> patientList = new List<Patient__c>();
        
        for(Integer i=0; i<recordCount; i++) {
            Patient__c p = new Patient__c();
            if(recordCount > 1){
                p.Name = lastName;
                p.First_Name__c = 'testFirst' + i;
                p.Date_of_Birth__c = Date.today();
            }else{
                p.Name = lastName;
                p.First_Name__c = 'testFirst';
                p.Date_of_Birth__c = Date.today();
            }
            patientList.add(p);
        }
        
        return patientList;
    }//End generatePatients
    
    public static List<Account> generatePlans(String planName, Integer recordCount){
        
        List<Account> planList = new List<Account>();
        
        for(Integer i=0; i<recordCount; i++){
            Account plan = new Account();
            if(recordCount > 1){
                plan.Name = planName + i;
            }else{
                plan.Name = planName;
            }
            plan.Type__c = 'Payer';
            planList.add(plan);
        }
        return planList;
    }//End generatePlans
    
    public static List<Plan_Patient__c> generatePlanPatients(Map<Id, List<Patient__c>> planToPatients){

        List<Plan_Patient__c> planPatientList = new List<Plan_Patient__c>();
        integer i=0;
        for(Id planId : planToPatients.keySet()){
            //system.debug('patients: ' + planToPatients.get(planId));
            for(Patient__c p : planToPatients.get(planId)){
                Plan_Patient__c planPatient = new Plan_Patient__c(
                    Health_Plan__c = planId,
                    Patient__c = p.Id,
                    Name = p.First_Name__c + ' ' + p.Name,
                    First_Name__c = p.First_Name__c,
                    Last_Name__c = p.Name,
                    ID_Number__c = 'id-' + i,
                    Date_of_Birth__c = Date.today()
                );
                planPatientList.add(planPatient);
                i++;
            }
        }
        return planPatientList;
    }//End generatePlanPatients

    public static List<Plan_Patient__c> generatePlanPatientsPlanOnly(Id planId, integer recordCount){
        List<Plan_Patient__c> planPatientList = new List<Plan_Patient__c>();
        for(integer i=0; i<recordCount; i++){
            Plan_Patient__c planPatient = new Plan_Patient__c(
                Health_Plan__c = planId,
                Name = 'Test Patient' + i,
                First_Name__c = 'Test',
                Last_Name__c = 'Patient' + i,
                ID_Number__c = 'id-' + i
            );
            planPatientList.add(planPatient);
        }
        return planPatientList;
    }//End generatePlanPatients

    public static List<Contact> generatePlanContacts(String profile, List<Account> planList, integer recordCount){
        List<Contact> contactList = new List<Contact>();
        for(Account plan : planList){
            for(integer i = 0; i < recordCount; i++){
                
                String salt = PasswordServices_CS.randomSalt(12);
                
                Contact c = new Contact(
                    Entity__c = plan.Id,
                    FirstName = 'testFirst' + i, 
                    LastName = 'testLast' + i, 
                    Email = 'test' + i + '@email.com', 
                    Birthdate = system.today().addYears(-30), 
                    Profile__c = profile,
                    Health_Plan_User_Id__c = String.valueOf(Math.abs(Crypto.getRandomInteger())),
                    Username__c = 'testPlanId' + profile + i,
                    Active__c = true,
                    Salt__c = salt,
                    Password__c = PasswordServices_CS.hash('abcABC123!', salt)
                );
                contactList.add(c);
            }
        }
        return contactList;
    }
    
    public static List<Contact> createPlanContacts(String profile, List<Account> planList, integer recordCount) {
        List<Contact> contacts = generatePlanContacts(profile, planList, recordCount);
        insert contacts;
        
        for (Contact c : contacts) {
            c.Id_Key__c = PasswordServices_CS.hash(c.Id, c.Salt__c);
        }
        
        update contacts;
        
        return contacts;
    }

    public static List<Account> generateProviders(String name, Integer recordCount){
        Id providerRTId = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' AND DeveloperName = 'Provider'].Id;
        List<Account> provList = new List<Account>();
        for(Integer i = 0; i < recordCount; i++) {
            Account prov = new Account();
            if(recordCount > 1){
                prov.Name = name + i;
            }else{
                prov.Name = name;
            }
            prov.Type__c = 'Provider';
            prov.Type_Of_Provider__c = 'DME';
            prov.Status__c = 'Active';
            prov.Clear_Enabled__c = true;
            prov.RecordTypeId = providerRTId;
            
            provList.add(prov);
        }
        return provList;
    }//End generateProviders
    
    public static List<Contact> generateProviderContacts(String profile, List<Account> providerList, integer recordCount){
        List<Contact> contactList = new List<Contact>();

        for(Account provider : providerList){
            for(Integer i = 0; i < recordCount; i++){
                
                String salt = PasswordServices_CS.randomSalt(12);
                
                Contact c = new Contact(
                    Entity__c = provider.Id,
                    AccountId = provider.Id,
                    FirstName = 'testFirst' + i, 
                    LastName = 'testLast' + i, 
                    Email = 'test' + i + '@email.com', 
                    Birthdate = system.today().addYears(-30), 
                    Profile__c = profile,
                    Health_Plan_User_Id__c = String.valueOf(Math.abs(Crypto.getRandomInteger())),
                    Username__c = 'testProviderId' + profile + i,
                    Active__c = true,
                    Salt__c = salt,
                    Password__c = PasswordServices_CS.hash('abcABC123!', salt)
                );
                contactList.add(c);
            }
        }
        return contactList;
    }
    
    public static List<Contact> createProviderContacts(String profile, List<Account> providerList, integer recordCount) {
        List<Contact> contacts = generateProviderContacts(profile, providerList, recordCount);
        insert contacts;
        
        for (Contact c : contacts) {
            c.Id_Key__c = PasswordServices_CS.hash(c.Id, c.Salt__c);
        }
        
        update contacts;
        
        return contacts;
    }
    
    public static DME_Settings__c generateDMESettings(){
        DME_Settings__c dmeSettings = new DME_Settings__c(
            Name = 'Default',
            Max_Delivery_Time__c = 48
        );
        
        return dmeSettings;
    }//End generateDMESettings
    
    public static List<DME__c> generateDMEs(Integer recordCount){
        List<DME__c> dmeList = new List<DME__c>();
        
        for(Integer i = 0; i < recordCount; i++){
            
            DME__c d = new DME__c();

            if(i<10){
                d.Name = 'A000' + i;
            }else{
                d.Name = 'A00' + i;
            }

			d.Active__c = true;
            
            dmeList.add(d);
        }
        
        return dmeList;
    }

    public static HCPC_Database_Category__c generateHcpcDbCategory(){
        HCPC_Database_Category__c hcpcDbCategory = new HCPC_Database_Category__c(
            Name = 'Test HCPCDbCategory',
            Database__c = 'Test'
        );
        
        return HCPCDbCategory;
    }

    public static HCPC_Category__c generateHcpcCategory(Id hcpcId, Id hcpcDbCategoryId){
        HCPC_Category__c hcpcCategory = new HCPC_Category__c(
            Name = 'Test Category',
            HCPC__c =  hcpcId,  
            HCPC_Database_Category__c = hcpcDbCategoryId
        );
        
        return HCPCCategory;
    }

    /** @description Creates and returns Provider_Category__c objects */
    public static Provider_Category__c generateProviderCategory(Id providerAcct, Id hcpcDbCat) {
        Provider_Category__c providerCat = new Provider_Category__c(
            Name = 'Test Provider Category',
            Provider__c = providerAcct,
            HCPC_Database_Category__c = hcpcDbCat
        );

        return providerCat;
    }
    
    //End fields needed for Order__c
    
    public static List<Order__c> generateOrders(Id planPatientId, Id providerId, Integer recordCount) {
        List<Order__c> orderList = new List<Order__c>();
        
        for(Integer i=0; i<recordCount; i++){
            Order__c order = new Order__c(
                Plan_Patient__c = planPatientId,
                Provider__c = providerId,
                Comments__c = 'test comment'
            );
            orderList.add(order);
        }
        
        return orderList;
    }//End generateOrders

    public static List<Order__c> generateOrdersWithLegacyOrders(Id planPatientId, Id providerId, List<New_Orders__c> legacyOrders) {
        List<Order__c> orderList = new List<Order__c>();
        
        for(New_Orders__c no : legacyOrders) {
            Order__c order = new Order__c(
                Plan_Patient__c = planPatientId,
                Provider__c = providerId,
                Legacy_Orders__c = no.Id
            );
            orderList.add(order);
        }
        
        return orderList;
    }

    public static Order_Assignment__c generateOrderAssignments(Id orderId, Id providerId, Datetime assignedAt, Datetime unassignedAt) {
        return new Order_Assignment__c(
            Order__c = orderId,
            Provider__c = providerId,
            Assigned_At__c = assignedAt,
            Unassigned_At__c = unassignedAt
        );
    }
    
    public void initializePlatform(){
        //initializes Order__c and all variables related to Order__c
        
        patientsToReturn = generatePatients('testLast', 1);
        insert patientsToReturn;
        
        plansToReturn = generatePlans('testPlan', 1);
        insert plansToReturn;
        
        planContactsToReturn = generatePlanContacts('Admin', plansToReturn, 1);
        insert planContactsToReturn;
        
        Map<Id, List<Patient__c>> planToPatient = new Map<Id, List<Patient__c>>();
        planToPatient.put(plansToReturn[0].Id, patientsToReturn);
        
        planPatientsToReturn = generatePlanPatients(planToPatient);
        insert planPatientsToReturn;
        
        providersToReturn = generateProviders('testProvider', 1);
        insert providersToReturn;
        
        providerContactsToReturn = generateProviderContacts('Admin', providersToReturn, 1);
        insert providerContactsToReturn;
        
        DME_Settings__c dmeSettings = generateDMESettings();
        insert dmeSettings;
        
        dmesToReturn = generateDMEs(3);
        insert dmesToReturn;
        
        Map<Id, Id> providerToDME = new Map<Id, Id>();
        providerToDME.put(providersToReturn[0].Id, dmesToReturn[0].Id);
        
        ordersToReturn = generateOrders(planPatientsToReturn[0].Id, providersToReturn[0].Id,  1);
        insert ordersToReturn;
        
        Map<Id, Decimal> DMEToQuantity = new Map<Id, Decimal>();
        Integer quantity = 5;
        for(DME__c dme : dmesToReturn){
            DMEToQuantity.put(dme.Id, quantity);
            quantity += 5;
        }
        
        Map<Id, Map<Id, Decimal>> orderToDMEToQuantity = new Map<Id, Map<Id, Decimal>>();
        orderToDMEToQuantity.put(ordersToReturn[0].Id, DMEToQuantity);
        
        lineItemsToReturn =  generateDMELineItems(orderToDMEToQuantity);
        insert lineItemsToReturn;
    }//End initializePlatform
    
    public static List<DME_Line_Item__c> generateDMELineItems(Map<Id, Map<Id, Decimal>> orderToDMEToQuantity){
        List<DME_Line_Item__c> lineItemList = new List<DME_Line_Item__c>();
        for(Id oId : orderToDMEToQuantity.keySet()) {
        for(Id dmeId : orderToDMEToQuantity.get(oId).keySet()) {
                DME_Line_Item__c dmeLI = new DME_Line_Item__c(
                    DME__c = dmeId,
                    Order__c = oId,
                    Quantity__c = orderToDMEToQuantity.get(oId).get(dmeId)
                );
                lineItemList.add(dmeLI);
            }
        }
        return lineItemList;
    }//End generateDMELineItems
    
    public static List<Provider_Location__c> generateProviderLocations(Id acctId, Integer recordCount){
        
        List<Provider_Location__c> providerLocationsList = new List<Provider_Location__c>();
        
        for(Integer i=0; i<recordCount; i++){
        
            Provider_Location__c pl = new Provider_Location__c(
                Account__c = acctId
            );
            providerLocationsList.add(pl);
        }
        
        return providerLocationsList;
    }

    public static List<Provider_Locations__c> generateOldProviderLocations(Id acctId, Integer recordCount){
        
        List<Provider_Locations__c> providerLocationsList = new List<Provider_Locations__c>();
        
        for(Integer i=0; i<recordCount; i++){
        
            Provider_Locations__c pl = new Provider_Locations__c(
                Account__c = acctId
            );
            providerLocationsList.add(pl);
        }
        
        return providerLocationsList;
    }
    
    public static List<New_Orders__c> generateNewOrders(Id plId, Id planId, Id providerId, List<DME__c> dmeList, integer recordCount){
        List<New_Orders__c> newOrdersList = new List<New_Orders__c>();
        for(integer i=0; i<recordCount; i++){
            New_Orders__c no = new New_Orders__c(
                Member_Patient_ID__c = 'id-' + i,
                Account_c__c = planId,
                Plan_Order__c = 'testAuthorization' + i,
                Person_placing_the_order__c = 'testPerson' + i,
                Health_Plans__c = 'testPlan' + i,
                Patient_Name__c = 'testPatient' + i,
                Patient_Phone__c = '890789678' + i,
                //Provider_Location__c = plId,
				Provider_Location_New__c = plId,
                Provider__c = providerId
            );
            for(integer index=0; index<dmeList.size(); index++){
                if(index==0){
                    no.CPT_Code1__c = dmeList[index].Name;
                    no.Units1__c = 5 + index;
                }else if(index==1){
                    no.CPT_Code_2__c = dmeList[index].Name;
                    no.Units_2__c = 5 + index;
                }else if(index==2){
                    no.CPT_Code_3__c = dmeList[index].Name;
                    no.Units_3__c = 5 + index;
                }else if(index==3){
                    no.CPT_Code_4__c = dmeList[index].Name;
                    no.Units_4__c = 5 + index;
                }else if(index==4){
                    no.CPT_Code_5__c = dmeList[index].Name;
                    no.Units_5__c = 5 + index;
                }else if(index==5){
                    no.CPT_Code_6__c = dmeList[index].Name;
                    no.Units_6__c = 5 + index;
                }else if(index==6){
                    no.CPT_Code_7__c = dmeList[index].Name;
                    no.Units_7__c = 5 + index;
                }else if(index==7){
                    no.CPT_Code_8__c = dmeList[index].Name;
                    no.Units_8__c = 5 + index;
                }else if(index==8){
                    no.CPT_Code_9__c = dmeList[index].Name;
                    no.Units_9__c = 5 + index;
                }else if(index==9){
                    no.CPT_Code_10__c = dmeList[index].Name;
                    no.Units_10__c = 5 + index;
                }
            }
            newOrdersList.add(no);
        }
        return newOrdersList;
    }

    public static List<New_Orders__c> generateDuplicateNewOrders(Id plId, Id planId, Id providerId, String HCPCs, String units, String patientName, String authCode, Boolean isFromArchive, integer recordCount){
        
        List<New_Orders__c> newOrdersList = new List<New_Orders__c>();
        for(integer i=0; i<recordCount; i++){
            New_Orders__c no = new New_Orders__c(
                Member_Patient_ID__c = 'id-' + i,
                Account_c__c = planId,
                CPT_code_for_OCR__c = HCPCs,
                Order_Image__c = 'https://shreddr.captricity.com/test',
                Plan_Order__c = authCode,
                Person_placing_the_order__c = 'testPerson' + i,
                Health_Plans__c = 'testPlan' + i,
                Patient_Name__c = patientName,
                Patient_Phone__c = '890789678' + i,
                //Provider_Location__c = plId,
				Provider_Location_New__c = plId,
                Provider__c = providerId,
                Units_for_OCR__c = units
            );
            if (isFromArchive) {
                no.Original_Created_Date__c = Datetime.now();
                no.Loaded_From_Archive__c = true;
            }
            newOrdersList.add(no);
        }
        return newOrdersList;
    }

    public static List<User> generateUsers(String role,integer recordCount){

        UserRole roleObject = new UserRole();
        roleObject = [SELECT Id, Name, DeveloperName FROM UserRole WHERE DeveloperName = :role];

        List<User> userList = new List<User>();

        for(integer i = 0; i < recordCount; i++){
            Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
            String email = RandomStringUtils.randomAlphanumeric(8) + '@test.com';
            User u = new User(
                    FirstName = 'joe' + i,
                    LastName = 'tester',
                    Email = email,
                    Username = email,
                    ProfileId = p.Id,
                    Alias = RandomStringUtils.randomAlphanumeric(8),
                    CommunityNickname = RandomStringUtils.randomAlphanumeric(8),
                    TimeZoneSidKey='America/Phoenix',
                    LocaleSidKey='en_US',
                    EmailEncodingKey='UTF-8',
                    LanguageLocaleKey='en_US',
                    UserRole = roleObject
                );
            userList.add(u);
        }

        return userList;
    }

    public static List<Clear_Algorithm_Settings__c> generateClearAlgorithmSettings(User owner, Integer recordCount){
        List<Clear_Algorithm_Settings__c> clearAlgorithmSettingsList = new List<Clear_Algorithm_Settings__c>();
        for(Integer i = 0; i< recordCount; i++){
            Clear_Algorithm_Settings__c cas = new Clear_Algorithm_Settings__c();
            cas.OwnerId = owner.Id;
            clearAlgorithmSettingsList.add(cas);
            
        }
        return clearAlgorithmSettingsList;
    }

    public static void runSubmitAlgorithmSettingsForApprovalProcess_Test(Clear_Algorithm_Settings__c cas) {
        
        // Query System Admin user who isn't current user.
        Id profId = [Select Id FROM Profile WHERE Name = 'System Administrator'].Id;
        User user1 = [SELECT Id FROM User WHERE ProfileId =: profId AND isActive = true AND Id !=: UserInfo.getUserId()][0];

        // Create an approval request for the Draft Clear Algorithm Setting
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting CAS for Approval, set status to Pending Approval');
        req1.setObjectId(cas.Id);

        // Submit on behalf of a specific user
        req1.setSubmitterId(user1.Id);

        // Submit the record to a specific process, sets approver to current user
        req1.setProcessDefinitionNameOrId('Sumbit_Algorithm_Settings_for_Approval');
        req1.setSkipEntryCriteria(true);
        req1.setNextApproverIds(new Id[] {UserInfo.getUserId()});

        // Submit the approval request for the Clear Algorithm Setting
        Approval.ProcessResult result = Approval.process(req1);

        // Approve the submitted request
        // Get the Id of the newly created item
        List<Id> newWorkItemIds = result.getNewWorkitemIds();

        // Instantiate the new ProcessWorkItemRequest object and populate it, set action to Approve
        Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
        req2.setComments('Approving CAS');
        req2.setAction('Approve');
        //req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});

        // Use the Id from the newly created itme to specify the item to be worked
        req2.setWorkitemId(newWorkItemIds.get(0));

        // Submit the item for approval
        Approval.ProcessResult result2 = Approval.process(req2);

    }
    
    //Get methods: for use after initializePlatform
    public List<Patient__c> getPatientList(){
        return patientsToReturn;
    }
    public List<Account> getPlanList(){
        return plansToReturn;
    }
    public List<Plan_Patient__c> getPlanPatientList(){
        return planPatientsToReturn;
    }
    public List<Account> getProviderList(){
        return providersToReturn;
    }
    public List<DME__c> getDMEList(){
        return dmesToReturn;
    }
    public List<Order__c> getOrderList(){
        return ordersToReturn;
    }
    public List<DME_Line_Item__c> getLineItemList(){
        return lineItemsToReturn;
    }
    
}//End TestDataFactory_CS