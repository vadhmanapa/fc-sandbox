@isTest
private class OrdercStatsTT {

   static testMethod void testTrigger() {
      try {
          Order__c o = new Order__c();
          insert o;

          System.assertNotEquals(null, o);
      }
      catch(Exception e) {
          List<Order__c> l = [SELECT Id from Order__c LIMIT 1];
          update l;
          System.assertNotEquals(null, l);
      }
   }
}