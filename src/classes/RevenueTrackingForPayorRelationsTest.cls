@isTest
public class RevenueTrackingForPayorRelationsTest {

    	static testMethod void testRevenueTrackingForPayorRelations(){
        PageReference pageRef = Page.PayorRevenueTracking;
        Test.setCurrentPageReference(pageRef);
        
            // create test account
            Account testAcc = new Account(Name = 'Test Account', Type__c = 'Payer', RecordTypeId = Schema.Sobjecttype.Account.getRecordTypeInfosByName().get('Payer Relations').getRecordTypeId());
            insert testAcc;
            
            // test revenue data
            Integra_Revenue_Tracking__c testRevenueData = new Integra_Revenue_Tracking__c(January_Revenue__c = 1000, February_Revenue__c = 2000, March_Revenue__c = 3000, April_Revenue__c = 4000,
                                                                                         May_Revenue__c = 5000, June_Revenue__c = 6000, January_Plan_Tracking__c = 10000,February_Plan_Tracking__c = 20000,
                                                                                         March_Plan_Tracking__c = 30000, April_Plan_Tracking__c = 40000, May_Plan_Tracking__c = 50000,June_Plan_Tracking__c = 60000,
                                                                                         Account__c = testAcc.Id);
            insert testRevenueData;
            
            //test opportunity data
            Opportunity o = new Opportunity(AccountId = testAcc.Id, RecordTypeId = Schema.Sobjecttype.Opportunity.getRecordTypeInfosByName().get('Payer Relations').getRecordTypeId(), Name = 'Test Opp',
                                           CloseDate = System.today().addDays(200), StageName = 'Open', Amount = 200000, Probability_Stage__c = '10%', Estimated_Market_Share__c = '10%', 
                                           Potential_Implementation__c = '2015 Q2');
            insert o;
            
            Opportunity o1 = new Opportunity(AccountId = testAcc.Id, RecordTypeId = Schema.Sobjecttype.Opportunity.getRecordTypeInfosByName().get('Payer Relations').getRecordTypeId(), Name = 'Test Opp 1',
                                           CloseDate = System.today().addDays(200), StageName = 'Open', Amount = 300000, Probability_Stage__c = '10%', Estimated_Market_Share__c = '10%', 
                                           Potential_Implementation__c = '2014 Q3');
            insert o1;
            
             Opportunity o2 = new Opportunity(AccountId = testAcc.Id, RecordTypeId = Schema.Sobjecttype.Opportunity.getRecordTypeInfosByName().get('Payer Relations').getRecordTypeId(), Name = 'Test Opp 2',
                                           CloseDate = System.today().addDays(200), StageName = 'Open', Amount = 500000, Probability_Stage__c = '10%', Estimated_Market_Share__c = '10%', 
                                           Potential_Implementation__c = '2014 Q4');
            insert o2;
            
            Opportunity o3 = new Opportunity(AccountId = testAcc.Id, RecordTypeId = Schema.Sobjecttype.Opportunity.getRecordTypeInfosByName().get('Payer Relations').getRecordTypeId(), Name = 'Test Opp 3',
                                           CloseDate = System.today().addDays(200), StageName = 'Open', Amount = 700000, Probability_Stage__c = '10%', Estimated_Market_Share__c = '10%', 
                                           Potential_Implementation__c = '2015 Q1');
            insert o3;
        
        RevenueTrackingForPayerRelations testData = new RevenueTrackingForPayerRelations();
        String testName = '2015 Q2';
        Double PWPWMS = 20000;
        Double ActualRevenue = 100000;
        Double PlanRevenue = 200000;
        Double pQ1 = 50000;
        Double pQ2 = 40000;
        Double pQ3 = 30000;
        Double pQ4 = 10000;
        //Decimal axisValue = PWPWMS+ActualRevenue+PlanRevenue;
        RevenueTrackingForPayerRelations.revenueData cases = new RevenueTrackingForPayerRelations.revenueData(testName, PWPWMS, ActualRevenue, PlanRevenue);
        List<RevenueTrackingForPayerRelations.revenueData> testRevenueList = testData.getRevenueData();

        RevenueTrackingForPayerRelations.cumulativePWP cumul = new RevenueTrackingForPayerRelations.cumulativePWP(testName, pQ1, pQ2, pQ3, pQ4, ActualRevenue, PlanRevenue);
        List<RevenueTrackingForPayerRelations.cumulativePWP>testCumulativeList = testData.getCumulativePWP();
        
       
    }
}