/**
 *  @Description AlgorithmSettingsWrapper class to construct and hold Algorithm Settings
 *  @Author Cloud Software LLC
 *  Revision History: 
 *		09-29-2015 - Greg Harvey - Created
 *		09-29-2015 - Andrew Nash - Added the AlgorithmSettingsListWrapper method to handle list of settings record.
 *		12-21-2015 - karnold - Added check in constructor so that new object (no owner ID) are assigned to the user no matter what.
 */
public class AlgorithmSettingsWrapper { 

	public Clear_Algorithm_Settings__c clearAlg;
	public Boolean isWriteable;
	public Boolean isActive;
	public Boolean isOwnedByCurrentUser;

	//===========================================================================================
	//================================ MAIN CLASS WRAPPER METHOD ================================
	//===========================================================================================
	public AlgorithmSettingsWrapper(Clear_Algorithm_Settings__c clearAlg) {
		this.clearAlg = clearAlg;

		if(clearAlg.Status__c == 'Draft') {
			this.isWriteable = true;
		} else {
			this.isWriteable = false;
		}

		if(clearAlg.Active__c) {
			this.isActive = true;
		} else {
			this.isActive = false;
		}

		if(clearAlg.ownerId == null || clearAlg.ownerId == UserInfo.getUserId()) {	// added null check for new settings objects
			this.isOwnedByCurrentUser = true;
		} else {
			this.isOwnedByCurrentUser = false;
		}
	}
	//===========================================================================================
	//=================================== LIST WRAPPER METHOD ===================================
	//===========================================================================================
	public static List<AlgorithmSettingsWrapper> wrapCASList(List<Clear_Algorithm_Settings__c> clearAlgList) {
		
		// Instantiate a new clearAlgReturnList to store and return the wrapped records list.
		List<AlgorithmSettingsWrapper> clearAlgReturnList = new List<AlgorithmSettingsWrapper>();
		// For each setting record in the clearAlgList.
		for(Clear_Algorithm_Settings__c cas : clearAlgList) {
			// Pass the record into the main wrapper method and add it to the return list.
			clearAlgReturnList.add(new AlgorithmSettingsWrapper(cas));
		}
		// Return the new list of wrapped records.
		return clearAlgReturnList;

	}
}