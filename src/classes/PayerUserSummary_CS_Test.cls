@isTest
private class PayerUserSummary_CS_Test {

    /******* Test Parameters *******/
	static private final Integer N_USERS = 15;
	
	/******* Payer *******/
	static private List<Account> payers;
	static private List<Contact> payerUsers;

	/******* Test Objects *******/
	static private PayerUserSummary_CS cont;
	public static Contact loginUser;
	public static Contact paramUser;
	public static Account plan;
	public static Account provider;
	public static List<Plan_Patient__c> patientList;
	public static List<Contact> userList;

	/******* Temp Info *******/
	public static String validPhone;
	public static String invalidPhone;
	public static String validEmail;
	public static String invalidEmail;

	//Profile properly saves (or does not)
	static testMethod void PayerUserSummary_CS_SaveProfile(){
		init();
		
		createTempInfo();

		cont.firstName = paramUser.FirstName;
		cont.lastName = paramUser.LastName;
		cont.phone = paramUser.Phone;
		cont.email = paramUser.Email;
		cont.username = paramUser.Username__c;
		
		system.debug('paramUser: ' + paramUser);
		
		cont.saveUser();
		
		System.assert(cont.usernameErrorMsg == '', 'saveUser failed.');
	}

	//Checks if username is sent to user
	static testMethod void PayerUserSummary_CS_SendUsername(){
		init();
		createTempInfo();
		
		cont.email = validEmail;
		cont.sendUsername();
		System.assert(cont.thisUser.sendEmailTo('Subject','Body'),'Email not sent');
	}
	
	static testMethod void PayerUserSummary_CS_ResetPassword(){
		init();
		createTempInfo();
		
		cont.email = validEmail;
		cont.resetPassword();
		System.assert(cont.thisUser.sendEmailTo('Subject','Body'),'Email not sent');
	}
	
	static testMethod void PayerUserSummary_CS_DeactivateUser(){
		init();
		cont.deactivateUser();
		System.assert(!cont.thisUser.instance.Active__c,'User not deactivated');
	}

	//Test phone Numbers and emails
	public static void createTempInfo(){	
		validPhone = '8005555555';
		invalidPhone = '800555555';
		validEmail = 'test@email.com';
		invalidEmail = 'test@emailcom';
	}
	
	public static void init() {
    	List<Account> providerList = TestDataFactory_CS.generateProviders('Test Provider', 1);
    	provider = providerList[0];
    	insert providerList;
    	
    	List<Account> payerList = TestDataFactory_CS.generatePlans('Test Payer', 1);
    	plan = payerList[0];
    	insert payerList;
    	
    	userList = TestDataFactory_CS.createPlanContacts('Admin', payerList, N_USERS);
    	
    	loginUser = userList[0];
    	paramUser = userList[1];
    	
    	//Page Landing
		PageReference pr = Page.PayerUserSummary_CS;
		pr.getParameters().put('uId',paramUser.Id);
		Test.setCurrentPage(pr);
		TestServices_CS.login(loginUser);
		
		//Page init
		cont = new PayerUserSummary_CS();
		cont.init();
	}
    
}