/**
 *  @Description Test code for ProviderCategorySelectController_CS
 *  @Author Cloud Software LLC, Keith Arnold
 *  Revision History: 
 *		01/07/2016 - karnold - Created
 *
 */
 @isTest
private class ProviderCategorySelectController_CS_Test {
	private static ProviderCategorySelectController_CS controller;
	private static List<Account> provList;
	private static List<HCPC_Database_Category__c> hcpcCatList;

	private static testMethod void init_Test() {
		// GIVEN
		initProviderTests();

		List<Provider_Category__c> provCatList = new List<Provider_Category__c>();
		provCatList.add(TestDataFactory_CS.generateProviderCategory(provList[0].Id, hcpcCatList[0].Id));
		insert provCatList;

		// WHEN
		Test.startTest();
			controller.init();
		Test.stopTest();

		// THEN
		System.assertEquals(1, controller.availableDMECats.size());
		System.assertEquals(1, controller.currentDMECats.size());
		System.assertEquals(0, controller.availableAdultCats.size());
		System.assertEquals(0, controller.currentAdultCats.size());
		System.assertEquals(0, controller.availablePediatricCats.size());
		System.assertEquals(0, controller.currentPediatricCats.size());
	}

	private static testMethod void saveChanges_Test() {
		// GIVEN
		initProviderTests();
		controller.init();

		// Move all categories to the available side to save.
		for (SelectOption cat : controller.availableDMECats) {
			controller.currentDMECats.add(cat);
		}
		// WHEN
		Test.startTest();
			controller.saveChanges();
		Test.stopTest();

		// THEN
		// Re-init the controller to re-query and populate lists
		controller.init();
		System.assertEquals(0, controller.availableDMECats.size());
		System.assertEquals(2, controller.currentDMECats.size());
	}
	
	private static void initProviderTests() {
		provList = TestDataFactory_CS.generateProviders('Test Provider', 1);
		insert provList;
		controller = new ProviderCategorySelectController_CS(new ApexPages.StandardController(provList[0]));
		ApexPages.currentPage().getParameters().put('id', String.valueOf(provList[0].Id));

		hcpcCatList = new List<HCPC_Database_Category__c>();
		hcpcCatList.add(TestDataFactory_CS.generateHcpcDbCategory());
		hcpcCatList.add(TestDataFactory_CS.generateHcpcDbCategory());
		hcpcCatList[1].Database__c = 'Test2';
		hcpcCatList[1].Name = 'Test Database Cat 2';
		hcpcCatList[1].Equipment_Type__c = 'DME';
		hcpcCatList[1].Database__c = 'Integra';

		hcpcCatList[0].Equipment_Type__c = 'DME';
		hcpcCatList[0].Database__c = 'Integra';
		insert hcpcCatList;

	}
}