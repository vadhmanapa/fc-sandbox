public without sharing class ProviderInbox_CS extends ProviderPortalServices_CS {

	private final String pageAccountType = 'Provider';

	private ProviderOrderServices_CS pOrderServices {get;set;}
	
	/********* Page Interface *********/
	public Apexpages.Standardsetcontroller orderSetCon {get;set;}
    public List<Order__c> orderList 
    {
        get{
            if(orderSetCon.getRecords() != null){
                return (List<Order__c>) orderSetCon.getRecords();
            }
            return new List<Order__c>();
        }
        set;
    }	
	
	public Integer pageSize {
		get{
			if(pageSize == null) pageSize = 10;
			return pageSize;
		}
		set{
			pageSize = value;
			if(orderSetCon != null){ orderSetCon.setPageSize(pageSize); }
		}
	}

    public Integer totalPages {
    	get{
    		return (Integer)Math.ceil((Decimal)orderSetCon.getResultSize() / orderSetCon.getPageSize());
    	}
    	set;
	}

	public String selectedOrderId {get;set;}    

	/********* Constructor/Init *********/
	public ProviderInbox_CS(){}
	
	public PageReference init(){

		///////////////////////////////////
		// Authentication and Permission
		///////////////////////////////////
		
		if(!isSecure && !Test.isRunningTest()){
			return DoLogout();
		}
				
		if(!authenticated){
			return DoLogout();
		}

		if(portalUser.AccountType != pageAccountType){
			return homepage;
		}
		
		///////////////////////////////////
		// Additional init
		///////////////////////////////////
		
		pOrderServices = new ProviderOrderServices_CS(portalUser.instance.Id);
		
		orderSetCon = pOrderServices.getOrdersByStage(OrderModel_CS.STAGE_PENDING_ACCEPTANCE);
		orderSetCon.setPageSize(10);
		
		system.debug('# of results: ' + orderSetCon.getResultSize());

		return null;		
	}
}