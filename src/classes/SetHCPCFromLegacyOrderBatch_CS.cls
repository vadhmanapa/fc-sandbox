public class SetHCPCFromLegacyOrderBatch_CS implements database.Batchable<sObject>{ 

	public Database.QueryLocator start(Database.BatchableContext bc) {
		String query = 'SELECT Legacy_Orders__r.CPT_code_for_OCR__c, Legacy_Orders__r.Units_for_OCR__c, (SELECT Id FROM DMEs__r LIMIT 1)';
        query += 'FROM Order__c WHERE CreatedDate = LAST_90_DAYS';
        
        return Database.GetQueryLocator(query);
	}

	public void execute(Database.BatchableContext bc, List<sObject> scope) {
        List<Order__c> ordersWithoutDMEs = new List<Order__c>();
        System.debug(ordersWithoutDMEs.size());
        for (Order__c o : (List<Order__c>)scope) {
            if (o.DMEs__r.size() == 0) {
                ordersWithoutDMEs.add(o);
            }
        }
        SetHCPCFromLegacyOrderBatchServices_CS.createDMELineItemsForOrders(ordersWithoutDMEs);
	}

	public void finish(Database.BatchableContext bc) {

	}
}