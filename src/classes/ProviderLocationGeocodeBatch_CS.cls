public with sharing class ProviderLocationGeocodeBatch_CS implements Database.Batchable<sObject>, Database.AllowsCallouts {

	public Database.QueryLocator start(Database.BatchableContext bc) {
			SoqlBuilder query = new SoqlBuilder()
			.selectx('Id')
			.selectx('Name')
			.selectx('Account__c')
			.selectx('MALatitude__c')
			.selectx('MALongitude__c')
			.selectx('Location_City__c')
			.selectx('Location_State__c')
			.selectx('Location_Street__c')
			.selectx('Location_Zip_Code__c')
			.fromx('Provider_Location__c');
		
		String queryString = query.toSoql();
		System.debug('SB Query: ' + queryString);
		return Database.getQueryLocator(queryString);
	}
	
	public void execute(Database.BatchableContext bc, List<sObject> batch) {
		List<Provider_Location__c> geocodedProviderLocationList = new List<Provider_Location__c>();
		for( sObject so: batch ){
			if(so != null){
				Provider_Location__c pl = (Provider_Location__c) so;
				if(pl.Location_Street__c != null) {
					List<Provider_Location__c> results;
					results = ProviderLocationServices_CS.geocodeProviderLocations(new List<Provider_Location__c>{pl});
					if(results != null) {
						geocodedProviderLocationList.addall(results);
					}
				}
			}
		}

		try{
			update geocodedProviderLocationList;
		} catch (Exception e) {
			// Do nothing
		}
	}
	
	public void finish(Database.BatchableContext bc) {}
}