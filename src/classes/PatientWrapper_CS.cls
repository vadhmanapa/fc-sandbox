public without sharing class PatientWrapper_CS {
	public Boolean editPatient {get;set;}
	
	public Plan_Patient__c patientRef {get;set;}
	
	public PatientWrapper_CS(Plan_Patient__c pRef){
		patientRef = pRef;
		editPatient = false;
	}
}