public with sharing class ShowRelatedPayTo_IP {

	public final Provider_Location__c proLoc{get;set;}
    public Pay_To__c relatedPayTo {get;set;}

    public ShowRelatedPayTo_IP(ApexPages.StandardController stdController) {
        this.proLoc = [SELECT Id, Name_DBA__c, Account__r.Name, Account__r.Legal_Name__c, Account__r.Tax_ID__c, Pay_To__c FROM Provider_Location__c 
                       WHERE Id=: ApexPages.currentPage().getParameters().get('Id') LIMIT 1];
        if (proLoc != null && proLoc.Pay_To__c != null) {
            relatedPayTo = [SELECT Id, Name, Taxpayer_Name__c, Tax_ID__c, Mailing_Address__c, Mailing_Address_2__c, City__c, State__c, Zip__c,Masys_Legacy__c
                            FROM Pay_To__c WHERE Id=: proLoc.Pay_To__c];
        }else {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'No Pay To found'));
        }        
    }
}