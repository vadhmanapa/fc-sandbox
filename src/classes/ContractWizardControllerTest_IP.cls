@isTest
private class ContractWizardControllerTest_IP {
	private final Contract contract;
    
    public ContractWizardControllerTest_IP(ApexPages.StandardController ContractWizardController){
        this.contract = (Contract) ContractWizardController.getRecord();
    }
    
    static testMethod void testAccValues(){
        Account testAcc = new Account(Name='Test Account', 
						        	  Type_Of_Provider__c = 'DME');
        insert testAcc;
        
        Contact testCon = new Contact(LastName='TestCon',
                                      FirstName='Contract',
                                      AccountId = testAcc.Id);
        insert testCon;
        PageReference pageRef = Page.ContractIntroPage_IP;
        Test.setCurrentPage(pageRef);
        test.startTest();
        ApexPages.currentPage().getParameters().put('accId', testAcc.Id); ///set accId equal to Id of test account
        ContractWizardController controller = new ContractWizardController();
        if(testAcc.Id != null){
            controller.accountInfo();
            System.assertEquals(controller.accId, testAcc.Id);
        }
        test.stopTest();
    }
    static testMethod void testMainPage(){
        PageReference pageRef = Page.ContractStep1_IP;
        Test.setCurrentPage(pageRef);
        Account acc = new Account(name='test',Type_Of_Provider__c = 'DME');
        insert acc;
        Contact testCont = new Contact(LastName='TestCon',
                                      FirstName='Contract',
                                      AccountId = acc.Id);
        insert testCont;
        date d = Date.today();        
        Contract contTest = new Contract();
        contTest.Status = 'Draft';
        contTest.StartDate = d + 7;
        contTest.AccountId = acc.Id;
        contTest.Contact_Name__c = testCont.Id;      
        insert contTest;        
        ContractWizardController controller = new ContractWizardController();
        //The .getURL will return the page url the Save() method returns.
        String nextPage = controller.mainPage().getUrl();
        //Check that the save() method returns the proper URL.
        System.assertEquals('/apex/contractintropage_ip', nextPage); 
    }
    static testMethod void testAmendments(){
        PageReference pageRef = Page.ContractIntroPage_IP;
        Test.setCurrentPage(pageRef);
        Account acc = new Account(name='test',Type_Of_Provider__c = 'DME');
        insert acc;
        Contact testCont = new Contact(LastName='TestCon',
                                      FirstName='Contract',
                                      AccountId = acc.Id);
        insert testCont;
        date d = Date.today();        
        Contract contTest = new Contract();
        contTest.Status = 'Draft';
        contTest.StartDate = d + 7;
        contTest.AccountId = acc.Id;
        contTest.Contact_Name__c = testCont.Id;      
        insert contTest;        
        ContractWizardController controller = new ContractWizardController();
        //The .getURL will return the page url the Save() method returns.
        String nextPage = controller.amendments().getUrl();
        //Check that the save() method returns the proper URL.
        System.assertEquals('/apex/contractstep1_ip', nextPage); 
    }
    static testMethod void Requirements(){
        PageReference pageRef = Page.ContractStep1_IP;
        Test.setCurrentPage(pageRef);
        Account acc = new Account(name='test',Type_Of_Provider__c = 'DME');
        insert acc;
        Contact testCont = new Contact(LastName='TestCon',
                                      FirstName='Contract',
                                      AccountId = acc.Id);
        insert testCont;
        date d = Date.today();        
        Contract contTest = new Contract();
        contTest.Status = 'Draft';
        contTest.StartDate = d + 7;
        contTest.AccountId = acc.Id;
        contTest.Contact_Name__c = testCont.Id;      
        insert contTest;        
        ContractWizardController controller = new ContractWizardController();
        //The .getURL will return the page url the Save() method returns.
        String nextPage = controller.requirements().getUrl();
        //Check that the save() method returns the proper URL.
        System.assertEquals('/apex/contractstep2requirements_ip', nextPage); 
    }
    static testMethod void testClaims(){
        PageReference pageRef = Page.ContractStep2Requirements_IP;
        Test.setCurrentPage(pageRef);
        Account acc = new Account(name='test',Type_Of_Provider__c = 'DME');
        insert acc;
        Contact testCont = new Contact(LastName='TestCon',
                                      FirstName='Contract',
                                      AccountId = acc.Id);
        insert testCont;
        date d = Date.today();        
        Contract contTest = new Contract();
        contTest.Status = 'Draft';
        contTest.StartDate = d + 7;
        contTest.AccountId = acc.Id;
        contTest.Contact_Name__c = testCont.Id;      
        insert contTest;        
        ContractWizardController controller = new ContractWizardController();
        //The .getURL will return the page url the Save() method returns.
        String nextPage = controller.claims().getUrl();
        //Check that the save() method returns the proper URL.
        System.assertEquals('/apex/contractstep3claims_ip', nextPage); 
    }
    static testMethod void testTermination(){
        PageReference pageRef = Page.ContractStep3Claims_IP;
        Test.setCurrentPage(pageRef);
        Account acc = new Account(name='test',Type_Of_Provider__c = 'DME');
        insert acc;
        Contact testCont = new Contact(LastName='TestCon',
                                      FirstName='Contract',
                                      AccountId = acc.Id);
        insert testCont;
        date d = Date.today();        
        Contract contTest = new Contract();
        contTest.Status = 'Draft';
        contTest.StartDate = d + 7;
        contTest.AccountId = acc.Id;
        contTest.Contact_Name__c = testCont.Id;      
        insert contTest;        
        ContractWizardController controller = new ContractWizardController();
        //The .getURL will return the page url the Save() method returns.
        String nextPage = controller.termination().getUrl();
        //Check that the save() method returns the proper URL.
        System.assertEquals('/apex/contractstep4termination_ip', nextPage); 
    }
    static testMethod void testMiscellaneous(){
        PageReference pageRef = Page.ContractStep4Termination_IP;
        Test.setCurrentPage(pageRef);
        Account acc = new Account(name='test',Type_Of_Provider__c = 'DME');
        insert acc;
        Contact testCont = new Contact(LastName='TestCon',
                                      FirstName='Contract',
                                      AccountId = acc.Id);
        insert testCont;
        date d = Date.today();        
        Contract contTest = new Contract();
        contTest.Status = 'Draft';
        contTest.StartDate = d + 7;
        contTest.AccountId = acc.Id;
        contTest.Contact_Name__c = testCont.Id;      
        insert contTest;        
        ContractWizardController controller = new ContractWizardController();
        //The .getURL will return the page url the Save() method returns.
        String nextPage = controller.miscellaneous().getUrl();
        //Check that the save() method returns the proper URL.
        System.assertEquals('/apex/contractstep5miscellaneous_ip', nextPage); 
    }
    static testMethod void testSave(){
        PageReference pageRef = Page.ContractStep5Miscellaneous_IP;
        Test.setCurrentPage(pageRef);
        Account acc = new Account(name='testSave',Type_Of_Provider__c = 'DME');
        insert acc;
        System.debug('Account Id is'+acc.Id);
        Contact testCon = new Contact(LastName='TestCon',
                                      FirstName='Contract1',
                                      AccountId = acc.Id);
        insert testCon;
        System.debug('Contact id'+testCon.Id);
        System.debug('Contact Account id'+testCon.AccountId);
        date d = Date.today();        
        Contract contTest = new Contract();
        contTest.Status = 'Draft';
        contTest.StartDate = d + 7;
        contTest.AccountId = acc.Id;
        contTest.Contact_Name__c = testCon.Id;      
        insert contTest; 
        System.debug('Contract Accountid'+contTest.AccountId);
        System.debug('Contract contact'+contTest.Contact_Name__c);
        if(contTest!= null){
            ContractWizardController controller = new ContractWizardController(contTest);
            //The .getURL will return the page url the Save() method returns.
            String nextPage = controller.save().getUrl();
            //Check that the save() method returns the proper URL.
            String expected = '/'+contTest.Id;
            System.assertEquals(expected,nextPage);
        } 
    }
    static testmethod void testExitContractWizard(){
        PageReference pageRef = Page.ContractIntroPage_IP;
        Test.setCurrentPage(pageRef);
        Account acc = new Account(name='test',Type_Of_Provider__c = 'DME');
        insert acc;
        Contact testCont = new Contact(LastName='TestCon',
                                      FirstName='Contract',
                                      AccountId = acc.Id);
        ApexPages.currentPage().getParameters().put('accId', acc.Id);     
        ContractWizardController controller = new ContractWizardController();        
        //The .getURL will return the page url the cancel() method returns.
        
        String nextPage = controller.cancel().getUrl();
        //Check that the save() method returns the proper URL.
        String expected = '/'+acc.Id;
        System.assertEquals(expected, nextPage);     
    }
    static testMethod void testInitialize(){
        PageReference pageRef = Page.ContractIntroPage_IP;
        Test.setCurrentPage(pageRef);
        
        Account acc = new Account(name='test',Type_Of_Provider__c = 'DME');
        insert acc;
        Contact testCont = new Contact(LastName='TestCon',
                                      FirstName='Contract',
                                      AccountId = acc.Id);
        insert testCont;
        ApexPages.currentPage().getParameters().put('accId',acc.Id);
        
        date d = Date.today();        
        Contract contTest = new Contract();
        contTest.Status = 'Draft';
        contTest.StartDate = d + 7;
        contTest.AccountId = acc.Id;
        contTest.Contact_Name__c = testCont.Id;      
        insert contTest;
        
        ContractWizardController controller = new ContractWizardController(contTest);
        System.assertNotEquals(null, controller);
    }
    static testmethod void testGetContract(){
        PageReference pageRef = Page.COntractStep1_IP;
        Test.setCurrentPage(pageRef);
        Account s = new Account(name='test',Type_Of_Provider__c = 'DME');
        insert s;         
        Contact testCont = new Contact(LastName='TestCon',
                                      FirstName='Contract',
                                      AccountId = s.Id);
        insert testCont;
        date d = Date.today();
        Contract contTest = new Contract();
        contTest.Status = 'Draft';
        contTest.StartDate = d + 7;
        contTest.AccountId = s.Id;
        contTest.Contact_Name__c = testCont.Id;
        insert contTest;        
        ContractWizardController controller = new ContractWizardController(contTest);
        System.assertNotEquals(null, controller.getContract());     
    }
}