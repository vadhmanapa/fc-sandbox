public class OrderHistoryBatch_CS implements Database.Batchable<sObject> { 

	public Database.QueryLocator start(Database.BatchableContext bc) {
		return Database.GetQueryLocator('SELECT Accepted_Date__c, Order_History_JSON__c FROM Order__c'); 
	}

	public void execute(Database.BatchableContext bc, List<sObject> scope) {
		Map<Id, Order__c> orderMap = new Map<Id, Order__c>((List<Order__c>)scope);
		Map<Id, List<String>> orderIdToErrorList = OrderHistoryBatchServices_CS.setAssignedAtOnOrderHistory(orderMap);

		//Add errors to orders
		List<Order__c> ordersToUpdate = new List<Order__c>();
		for (Id orderId : orderIdToErrorList.keySet()) {
			ordersToUpdate.add(orderMap.get(orderId));
			orderMap.get(orderId).Order_History_Parse_Failed__c = true;
			orderMap.get(orderId).Order_History_Parse_Errors__c = String.join(orderIdToErrorList.get(orderId), '\n').abbreviate(255);
		}
		update ordersToUpdate;
	}

	public void finish(Database.BatchableContext bc) {

	}
}