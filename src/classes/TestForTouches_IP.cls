/**
 This is test class for Touches for Claims */
@isTest
private class TestForTouches_IP {

    static testMethod void TestTouch() {
        // create an ARAG with Status
        ARAG__c testBill = new ARAG__c(Bill__c = '12345', Provider__c = 'Michael Jackson', Payor_Family_text__c = 'Charles Babbage',
        								Total_Due__c = 10, Status__c = 'Unassigned');
        insert testBill;
        String testId = testBill.Id;
     
        // update ARAG
        
        testBill.Status__c = 'Scrubbed';
        update testBill;
        
        //check
        
        List<Claim_Work_Touch__c> newTouch = [Select Id,Name, Claims_Item__r.Id, To_Status__c from Claim_Work_Touch__c
        										where Claims_Item__r.Id =: testId and To_Status__c = 'Scrubbed' LIMIT 1];
        										
        for (Claim_Work_Touch__c cw : newTouch){
        	System.assertEquals(1,newTouch.size(), 'New Claim Touch was created');
        	System.assertEquals('Scrubbed', cw.To_Status__c, 'The updated status match');
        }
    }
}