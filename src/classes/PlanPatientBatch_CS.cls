/**
 *  @Description Batch to de-duplicate Plan Patient objects.
 *  @Author Cloud Software LLC, Jasmine Brown
 *  Revision History: 
 *      11/11/2015 - karnold - ICA-134 - Added batch setup methods and change archicture of batch to implement 
 *          comparable interface on Plan Patients
 *		12/07/2015 - steven - ICA-351 - Ignore null ID number fields
 *		12/08/2015 - steven - ICA-351 - Reassign orders from deleted duplicates to non-duplicates.
 *		12/10/2015 - karnold - ICA-351 - Integrated order reassignment logic into finding duplicates to prevent CPU timeouts.
 */
public class PlanPatientBatch_CS implements Database.Batchable < SObject > {
	/* Static Batch Variables */

	private Boolean doDml = true;
	private String queryString;

	/* Batch Setup Methods */

	public void setDoDml(Boolean doDml) {
		this.doDml = doDml;
	}

	public void setQueryString(String queryString) {
		System.debug('SB Query: ' + queryString);
		this.queryString = queryString;
	}

	public PlanPatientBatch_CS() { 
		SoqlBuilder query = new SoqlBuilder()
		.selectx('Id')
		.fromx('Account');

		setQueryString(query.toSoql());
	}

	public PlanPatientBatch_CS(String queryString) { 
		setQueryString(queryString);
	}

	/* Database.Batchable Methods */

	public Database.QueryLocator start(Database.BatchableContext context) {
		return Database.getQueryLocator(queryString);
	}

	public void execute(Database.BatchableContext context, List<Account> scope) {
		// Iterate through accounts to gather all Plan Patients to query for
		Set<Id> healthPlanIds = new Set<Id> ();
		for (Account acc : scope) {
			healthPlanIds.add(acc.Id);
		}

		// Query for Plan Patients.
		List<Plan_Patient__c> planPatientList = [
			SELECT
				Name,
				Health_Plan__c,
				ID_Number__c,
				First_Name__c,
				Last_Name__c,
				Date_of_Birth__c,
				CreatedDate
			FROM Plan_Patient__c
			WHERE Health_Plan__c IN :healthPlanIds
			ORDER BY Health_Plan__c
		];

		// Create Map of Account Id to List<Plan_Patients__c> to group patients, in order, based on health plan account
		Map<Id, List<Plan_Patient__c>> healthPlanToPlanPatientListMap = new Map<Id, List<Plan_Patient__c>> ();
		for (Plan_Patient__c patient : planPatientList) {
			if (!healthPlanToPlanPatientListMap.containsKey(patient.Health_Plan__c)) {
				healthPlanToPlanPatientListMap.put(patient.Health_Plan__c, new List<Plan_Patient__c> ());
			}
			healthPlanToPlanPatientListMap.get(patient.Health_Plan__c).add(patient);
		}

		// Iterate through each list and compare Plan Patients. Gather list to delete.
		List<Plan_Patient__c> planPatientsToDeleteList = new List<Plan_Patient__c> ();
		Map<Id, Id> oldPatientToNewPatientMap = new Map<Id, Id>();

		for (Id healthPlanId : healthPlanToPlanPatientListMap.keySet()) {
			List<Plan_Patient__c> ppList = healthPlanToPlanPatientListMap.get(healthPlanId);

			planPatientsToDeleteList.addAll(findDuplicates(ppList, oldPatientToNewPatientMap));
		}

		List<Order__c> ordersToUpdate = populateOrdersToUpdate(oldPatientToNewPatientMap);

		// Populate a list from the set of Plan Patients to delete
		System.debug(System.LoggingLevel.INFO, 'Deleting? ' + doDml + ' There are ' + planPatientsToDeleteList.size()
		             + ' Plan_Patient__c record(s) to delete.');
		System.debug(System.LoggingLevel.INFO, 'List to Delete: ' + planPatientsToDeleteList);
		System.debug(System.LoggingLevel.INFO, 'Updating these orders to non-duplicate patient records: ' + ordersToUpdate);

		// Delete duplicates
		if (doDml) {
			update ordersToUpdate;
			delete planPatientsToDeleteList;
		}
	}

	public void finish(Database.BatchableContext context) {

	}

	/* Helper Methods */

	/**  Given a list of Plan Patients from a single health plan, identify and return all Plan Patient records that should be deleted.
	 *  The first (ID Number) map will contain the one most recently created Plan Patient for each Id Number.
	 *  The next map (Name + DOB) wil only be generated from the values of the previous map.
	 *  And last, the delete list is populated by checking the original list against the Plan Patient records that should be kept
	 *  and adding the ones that are not in the list to keep.
	 */
	private List<Plan_Patient__c> findDuplicates(List<Plan_Patient__c> ppList, Map<Id, Id> oldPatientToNewPatientMap) {
		
		Map<String, Plan_Patient__c> idNumberToPatientMap = new Map<String, Plan_Patient__c> ();
		for (Plan_Patient__c pp : ppList) {
			if (idNumberToPatientMap.containsKey(pp.Id_Number__c)) {	// If there is a duplicate ID
				if (pp.CreatedDate > idNumberToPatientMap.get(pp.Id_Number__c).CreatedDate) {	// And the created date for this record is sooner than the existing record
					System.debug(System.LoggingLevel.INFO, 'Adding to map: ' + idNumberToPatientMap.get(pp.Id_Number__c).Id + ' to ' + pp.Id);
					oldPatientToNewPatientMap.put(idNumberToPatientMap.get(pp.Id_Number__c).Id, pp.Id);	// There should be a mapping from that to this
					idNumberToPatientMap.put(pp.Id_Number__c, pp);	// The most recent record should be kept
				} else {	// Or if the created date for this record is further back
					System.debug(System.LoggingLevel.INFO, 'Adding to map: ' + pp.Id + ' to ' + idNumberToPatientMap.get(pp.Id_Number__c).Id);
					oldPatientToNewPatientMap.put(pp.Id, idNumberToPatientMap.get(pp.Id_Number__c).Id);	// Then there should be a mapping from this to that
				}
			} else {
				idNumberToPatientMap.put(pp.Id_Number__c, pp);
			}
		}

		Map<String, Plan_Patient__c> nameDobToPatientMap = new Map<String, Plan_Patient__c> ();
		for (Plan_Patient__c pp : idNumberToPatientMap.values()) {
			if (nameDobToPatientMap.containsKey(getNameAndDob(pp))) {
				if (pp.CreatedDate > nameDobToPatientMap.get(getNameAndDob(pp)).CreatedDate) {
					System.debug(System.LoggingLevel.INFO, 'Adding to map: ' + nameDobToPatientMap.get(getNameAndDob(pp)).Id + ' to ' + pp.Id);
					oldPatientToNewPatientMap.put(nameDobToPatientMap.get(getNameAndDob(pp)).Id, pp.Id);
					nameDobToPatientMap.put(getNameAndDob(pp), pp);
				} else {
					System.debug(System.LoggingLevel.INFO, 'Adding to map: ' + pp.Id + ' to ' + nameDobToPatientMap.get(getNameAndDob(pp)).Id);
					oldPatientToNewPatientMap.put(pp.Id, nameDobToPatientMap.get(getNameAndDob(pp)).Id);
				}
			} else {
				nameDobToPatientMap.put(getNameAndDob(pp), pp);
			}
		}

		Map<Id, Plan_Patient__c> idToValidPatientsMap = new Map<Id, Plan_Patient__c> (nameDobToPatientMap.values());
		List<Plan_Patient__c> planPatientsToDelete = new List<Plan_Patient__c>();

		for (Plan_Patient__c pp : ppList) {
			if (!idToValidPatientsMap.containsKey(pp.Id) && String.isNotBlank(pp.ID_Number__c)) {
				planPatientsToDelete.add(pp);
			}
		}

		return planPatientsToDelete;
	}

	/**
	*	Iterates through a list of queried orders and reassignes the plan patient lookup. The while loop is necessary to find the ultimate plan patient left. 
	*/
	private List<Order__c> populateOrdersToUpdate(Map<Id, Id> oldPatientToNewPatientMap) {
		List<Order__c> ordersToUpdate = [
			SELECT
				Plan_Patient__c
			FROM Order__c
			WHERE Plan_Patient__c IN :oldPatientToNewPatientMap.keySet()
		];
		System.debug(LoggingLevel.INFO, 'ordersToUpdate ' + ordersToUpdate);
		System.debug(LoggingLevel.INFO, 'oldPatientToNewPatientMap ' + oldPatientToNewPatientMap);

		for (Order__c ord : ordersToUpdate) { 
			while (oldPatientToNewPatientMap.get(ord.Plan_Patient__c) != null) {
				System.debug('System.debug(ord.Plan_Patient__c) ' + ord.Plan_Patient__c);
				System.debug('oldPatientToNewPatientMap.get(ord.Plan_Patient__c) ' + oldPatientToNewPatientMap.get(ord.Plan_Patient__c));
				ord.Plan_Patient__c = oldPatientToNewPatientMap.get(ord.Plan_Patient__c);
			}
		}
		return ordersToUpdate;
	}

	private String getNameAndDob(Plan_Patient__c pp) {
		if (String.isBlank(pp.First_Name__c) || String.isBlank(pp.Last_Name__c)) {
			return pp.Name + String.valueOf(pp.Date_of_Birth__c);
		}
		return pp.First_Name__c + pp.Last_Name__c + String.valueOf(pp.Date_of_Birth__c);
	}

}