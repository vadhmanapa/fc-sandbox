@isTest
private class EmailServices_CS_Test {
	
	//For test coverage.There is no way to send emails in a test class.
	static testMethod void sendEmail() {
		
		List<Account> initAccts = TestDataFactory_CS.generateProviders('Test Provider', 1);
    	insert initAccts;
		List <Contact > userList = TestDataFactory_CS.generateProviderContacts('Admin', initAccts, 5);
    	insert userList;
		UserModel_CS u = new UserModel_CS(userList[0]);
					
		String emailSubject = 'Forgot Username';
		String emailBody = 'Username: ' + userList[0].Username__c;
					
		u.sendEmailTo(emailSubject, emailBody);
		
		System.assert(true);
	}
	
}