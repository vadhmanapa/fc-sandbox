public with sharing class CaseNotificationBatch implements Database.Batchable<sObject> {

	private string queryScript;
	
	public Database.QueryLocator start(Database.BatchableContext bc) {
		queryScript = '';
		queryScript += 'SELECT Id';
		queryScript += ',CaseNumber';
		queryScript += ',Due_Date__c';
		queryScript += ',OwnerId';
		queryScript += ' FROM Case';
		
		return Database.getQueryLocator(queryScript);
	}
	
	public void execute(Database.BatchableContext bc, List<sObject> batch) {
		CaseNotifications.checkNotifications((Case[])batch);
	}
	
	public void finish(Database.BatchableContext bc) {}
}