/**
 *  @Description Test class for ProviderCategoryTriggerDispatcher
 *  @Author Cloud Software LLC, sbatchelor
 *  Revision History: 
 *		2016-02-19 - sbatchelor - Created
 *
 */
@isTest
public class ProviderCategoryDispatcher_CS_Test {

	@testSetup
	private static void init() {

		HCPC_Database_Category__c databaseCategory = TestDataFactory_CS.generateHcpcDbCategory();
		insert databaseCategory;

		Account provider = TestDataFactory_CS.generateProviders('Test', 1)[0];

		insert provider;

		insert TestDataFactory_CS.generateProviderCategory(provider.Id, databaseCategory.Id);

		provider.Keywords__c ='';
		update provider;
	}

	private static testMethod void updateKeywords_Test() {
		Provider_Category__c providerCategory = [SELECT Id FROM Provider_Category__c][0];

		Test.startTest();
		update providerCategory;
		Test.stopTest();

		Account provider = [SELECT Keywords__c FROM Account];

		System.assertEquals('Test Provider Category', provider.Keywords__c);
	}
}