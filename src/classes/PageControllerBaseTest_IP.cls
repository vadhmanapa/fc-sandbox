@isTest
private class PageControllerBaseTest_IP {
	private static PageControllerBase_IP pgController;
	private static ComponentControllerBase_IP comController;
	
	@isTest static void testBaseController() {
		pgController = new PageControllerBase_IP();
		comController = new ComponentControllerBase_IP();

		Test.startTest();
		pgController.setComponentController(comController);
		pgController.setComponentControllerMap('test',comController);
		Test.stopTest();

		System.assertNotEquals(null, pgController.getMyComponentController());
		System.assertNotEquals(null, pgController.getComponentControllerMap().size());
		System.assertNotEquals(null, pgController.getThis());
	}
	
	@isTest static void test_method_two() {
		// Implement test code
	}
	
}