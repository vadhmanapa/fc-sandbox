/*===============================================================================================
 * Cloud Software LLC
 * Name: DeliveryCompletionBatchServices
 * Description: Services class for DeliveryCompletionBatch
 * Created Date: 9/30/2015
 * Created By: Andrew Nash (Cloud Software)
 * 
 * 	Date Modified			Modified By			Description of the update
 *	SEP 30, 2015			Andrew Nash			Created
 ==============================================================================================*/
public class DeliveryCompletionBatchServices {
	
	//===========================================================================================
	//=========================== PROCESS DELIVERY COMPLETION METHOD ============================
	//===========================================================================================
	public static void processDeliveryCompletion(List<Order_Assignment__c> orderAssignmentList) {
		
		Set<Id> orderIdSet = new Set<Id>();

		for(Order_Assignment__c oa : orderAssignmentList) {
			orderIdSet.add(oa.Order__c);
		}

		// Create a new map that contains all orders that do not have null accepted dates or null delivery date times
		Map<Id, Order__c> ordersMap = new Map<Id, Order__c>([
			SELECT 
				Id,
				Accepted_Date__c,
				Delivery_Date_Time__c
			FROM Order__c
			WHERE Accepted_Date__c != null
			AND Delivery_Date_Time__c != null
			AND Id IN :orderIdSet
		]);

		// For each order assignment in the legacy order assignment history list
		for(Order_Assignment__c oa : orderAssignmentList) {
			// Calculate that orders delivery completion time as Delivery Date Time minus Accepted Date
			if(ordersMap.containsKey(oa.Order__c)) {
				if (ordersMap.get(oa.Order__c).Delivery_Date_Time__c != null && ordersMap.get(oa.Order__c).Accepted_Date__c != null) {
					oa.Delivery_Completion_Time__c = (ordersMap.get(oa.Order__c).Delivery_Date_Time__c.getTime() - ordersMap.get(oa.Order__c).Accepted_Date__c.getTime())/1000;
				}
			}
		}

		update orderAssignmentList;
		
	}

}