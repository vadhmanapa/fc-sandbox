// This is a test class for TasktoIssueLog trigger which creates Issue Log on creation of task with Subject Call
@ isTest
private class TestCreateIssueLog {
	  /*static testMethod void test_TasktoIssueLog() {
        //create account record
        Account testAccount = new Account(Name = 'test', Type_Of_Provider__c = 'DME');
        insert testAccount;
        String accountId = testAccount.Id; 
      
     //create contact record
      Contact testContact = new Contact(FirstName = 'Test', LastName = 'Contact', AccountId = accountId);
      insert testContact;
      String contactId = testContact.Id;
    
      // create user record
      User testUser = new User(FirstName = 'test', LastName = 'check', Username = 'testcheck@test.com', Email = 'test@test.com', Alias = 'test',
                                                       CommunityNickname = 'testforlog', TimeZoneSidKey = 'America/Los_Angeles', EmailEncodingKey = 'ISO-8859-1',
                                                       ProfileId = '00e300000028QgNAAU', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
      insert testUser;
      String testUserId = testUser.Id;
      
      //create task record
      Task task = new Task();
      task.WhatId= testContact.AccountId;
      task.Description = 'test';
      task.WhoId = contactId;
      task.OwnerId = testUserId;
      task.skyplatform__Activity_Type__c = 'Call';
      task.skyplatform__Unique_Call_Id__c = 'test';
      task.skyplatform__From_Phone__c = '1112223333';
      task.skyplatform__To_Phone__c = '2223334444';
      task.Status = 'Completed';
      insert task;
      System.debug('Task with Subject as Call has been created');
      String uniqueId = task.skyplatform__Unique_Call_Id__c;
     
    //validate case was created
    List<Issue_Log__c> validateLog = [
        SELECT Id, Unique_Id__c
            FROM Issue_Log__c
            WHERE Unique_Id__c = :uniqueId
            LIMIT 1
        ];
    for( Issue_Log__c log : validateLog ){
        System.assertEquals(1, validateLog.size(), 'The Issue log was created for correct task');
    }
        //update task
        task.Description = 'update test';
        task.skyplatform__From_Phone__c = '4455667788';
        //task.Status = 'Completed';
        update task;
    
     System.debug('task was updated');
     String uniqueIdNull = task.skyplatform__Unique_Call_Id__c;
     
     //validate case was created
    List<Issue_Log__c> validateLogNull = [
        SELECT Id, Unique_Id__c, Description__c
            FROM Issue_Log__c
            WHERE Unique_Id__c = :uniqueIdNull
            LIMIT 1
        ];
        String testString = validateLogNull[0].Description__c;
    for( Issue_Log__c log : validateLogNull ){
        System.assertEquals(1, validateLogNull.size(), 'The Issue log was created for correct task');
        //System.assertEquals('test update test',testString , 'The task was updated');
    } 
    
   }*/
}