@isTest
public class TestServiceCloudInduvidualView {
    static testMethod void testGauge(){
        PageReference pageRef = Page.ServiceCloudInduvidualView;
        Test.setCurrentPageReference(pageRef);
        Integer i;
    List<Call_Log__c> testCalls = new List<Call_Log__c>();
    for (i=0; i<10; i++){
        Call_Log__c cTest = new Call_Log__c();
        cTest.Call_Reason__c = 'Clear';
        cTest.Caller_Type__c = 'Provider';
        cTest.Call_Reason_Detail__c = 'Password change';
        testCalls.add(cTest);
    }
    insert testCalls;
        
        ServiceCloudInduvidualView testController = new ServiceCloudInduvidualView();
        String testName = 'test';
        Integer testSize = 1000;
        ServiceCloudInduvidualView.callsGaugeData calls = new ServiceCloudInduvidualView.callsGaugeData(testSize);
        List<ServiceCloudInduvidualView.callsGaugeData> testList = testController.getnCalls();
        
        // for cases
        ServiceCloudInduvidualView testCaseController = new ServiceCloudInduvidualView();
        String testCaseName = 'test';
        Integer testCaseSize = 1000;
        ServiceCloudInduvidualView.caseGaugeData cases = new ServiceCloudInduvidualView.caseGaugeData(testCaseSize);
        List<ServiceCloudInduvidualView.caseGaugeData> testCaseList = testCaseController.getnCase();
        
        // for emails
        ServiceCloudInduvidualView testEmailController = new ServiceCloudInduvidualView();
        String testEmailName = 'test';
        Integer testEmailSize = 1000;
        ServiceCloudInduvidualView.emailGaugeData emails = new ServiceCloudInduvidualView.emailGaugeData(testCaseSize);
        List<ServiceCloudInduvidualView.emailGaugeData> testEmailList = testCaseController.getnEmail();
    }
	
}