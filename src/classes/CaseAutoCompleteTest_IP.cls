@isTest
private class CaseAutoCompleteTest_IP
{
	private static final Id payorAccRecordId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payer Relations').getRecordTypeId();
	private static final Id integraProviderAccRecordId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Integra Provider').getRecordTypeId();
	private static final Id dmensionProviderAccRecordId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('DMEnsion Provider').getRecordTypeId();
	private static final Id integraAndDmeProviderAccRecordId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('DMEnsion and Integra Provider').getRecordTypeId();

	private static final Id patientConRecordId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
	private static final Id payorConRecordId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Payor').getRecordTypeId();
	private static final Id providerConRecordId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Provider').getRecordTypeId();
	private static final Id familyConRecordId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Family').getRecordTypeId();
	private static Account payAcc;
	private static Account proAcc;
	private static Contact patCon;

	@isTest static void testAccountsSearch ()
	{
		generateTestData();
		Test.startTest();
		System.assertNotEquals(0, CaseAutoComplete_IP.getData('Account','AccountName__c','Id','Test', 'Payor', '', '', '').size());
		System.assertNotEquals(0, CaseAutoComplete_IP.getData('Account','AccountName__c','Id','Test', 'Provider', '', '', 'TaxID').size());
		System.assertNotEquals(0, CaseAutoComplete_IP.getData('Account','AccountName__c','Id','6846', 'Provider', '', '', 'TaxID').size());
		System.assertNotEquals(0, CaseAutoComplete_IP.getData('Account','AccountName__c','Id','Test', 'Provider', '', '', 'Account_Name').size());
		System.assertNotEquals(0, CaseAutoComplete_IP.getData('Account','AccountName__c','Id','6846', 'Provider', '', '', 'Account_Name').size());

		CaseAutoComplete_IP controller = new CaseAutoComplete_IP ();
        controller.setTargetFieldVar(proAcc.Id);
        System.assertEquals(proAcc.Id,controller.getTargetFieldVar());
        controller.setCacheField(null);
		Test.stopTest();

	}

	@isTest static void testContactSearch(){
		generateTestData();
		Test.startTest();
		System.assertNotEquals(0, CaseAutoComplete_IP.getData('Contact','Name','Id','Test', 'Payor', payAcc.Id, '', '').size());
		System.assertNotEquals(0, CaseAutoComplete_IP.getData('Contact','Name','Id','Test', 'Patient', payAcc.Id, '', '').size());
		System.assertNotEquals(0, CaseAutoComplete_IP.getData('Contact','Name','Id','Test', 'Family', payAcc.Id, patCon.Id, '').size());
		System.assertNotEquals(0, CaseAutoComplete_IP.getData('Contact','Name','Id','Test', 'Provider', proAcc.Id, '', '').size());
		Test.stopTest();
	}

	@isTest static void generateTestData()
	{
		List<Account> testPayorAccounts = new List<Account>();
		for(Integer i=0; i<10; i++){
			Account acc = new Account();
			acc.RecordTypeId = payorAccRecordId;
			acc.Type__c = 'Payer';
			acc.Name = 'Test Payer'+i;
			acc.Status__c = 'Active';

			testPayorAccounts.add(acc);
		}

		insert testPayorAccounts;

		payAcc = new Account();
		payAcc = [SELECT Id FROM Account WHERE Id=: testPayorAccounts[0].Id];

		List<Contact> payorContacts = new List<Contact>();
		for(Integer i=0; i<10; i++){
			Contact c = new Contact();
			c.RecordTypeId = payorConRecordId;
			c.AccountId = testPayorAccounts[0].Id;
			c.Phone = '1234567890';
			c.Email = 'test@test.com';
			c.FirstName = 'Test';
			c.LastName = 'Payor'+i;

			payorContacts.add(c);
		}

		insert payorContacts;

		List<Contact> patientContacts = new List<Contact>();
		for(Integer i=0; i<10; i++){
			Contact c = new Contact();
			c.RecordTypeId = patientConRecordId;
			c.AccountId = testPayorAccounts[0].Id;
			c.Phone = '6549871230';
			c.Health_Plan_ID__c = '12123'+i;
			c.Birthdate = System.today().addYears(-i);
			c.FirstName = 'Test';
			c.LastName = 'Patient'+i;

			patientContacts.add(c);
		}

		insert patientContacts;

		patCon = new Contact();
		patCon = [SELECT Id FROM Contact WHERE Id=: patientContacts[0].Id];

		List<Contact> familyContacts = new List<Contact>();
		for(Integer i=0; i<10; i++){
			Contact c = new Contact();
			c.RecordTypeId = familyConRecordId;
			c.AccountId = testPayorAccounts[0].Id;
			c.Phone = '6549871230';
			c.FirstName = 'Test';
			c.LastName = 'Family'+i;
			c.RelatedPatient__c = patientContacts[0].Id;
			c.RelationshiptoPatient__c = 'Relative';

			familyContacts.add(c);
		}

		insert familyContacts;

		List<Account> providerIntAccounts = new List<Account>();
		for(Integer i=0; i<10; i++){
			Account a = new Account();
			a.RecordTypeId = integraProviderAccRecordId;
			a.Name = 'Test Integra'+i;
			a.Tax_ID__c = '68464684'+i;
			a.Type__c = 'Provider';
			a.Status__c = 'Active';

			providerIntAccounts.add(a);
		}

		insert providerIntAccounts;

		proAcc = new Account();
		proAcc = [SELECT Id FROM Account WHERE Id=: providerIntAccounts[0].Id];

		List<Contact> intProviderContacts = new List<Contact>();
		for(Integer i=0; i<10; i++){
			Contact c = new Contact();
			c.RecordTypeId = providerConRecordId;
			c.Active__c = true;
			c.AccountId = providerIntAccounts[0].Id;
			c.Phone = '9876543210';
			c.Email = 'test1@test.com';
			c.Status__c = 'Active';
			c.FirstName = 'Test';
			c.LastName = 'Provider'+i;

			intProviderContacts.add(c);
		}

		insert intProviderContacts;

		List<Account> providerIntDmeAccounts = new List<Account>();
		for(Integer i=0; i<10; i++){
			Account a = new Account();
			a.RecordTypeId = integraAndDmeProviderAccRecordId;
			a.Name = 'Test Integra'+i;
			a.Tax_ID__c = '68464684'+i;
			a.Type__c = 'Provider';
			a.Status__c = 'Active';

			providerIntDmeAccounts.add(a);
		}

		insert providerIntDmeAccounts;

		List<Contact> intdmeProviderContacts = new List<Contact>();
		for(Integer i=0; i<10; i++){
			Contact c = new Contact();
			c.RecordTypeId = providerConRecordId;
			c.Active__c = true;
			c.AccountId = providerIntDmeAccounts[0].Id;
			c.Phone = '9876543210';
			c.Email = 'test1@test.com';
			c.Status__c = 'Active';
			c.FirstName = 'Test';
			c.LastName = 'Provider'+(i+11);

			intdmeProviderContacts.add(c);
		}

		insert intdmeProviderContacts;
	}
}