/**
 *  @Description 
 *  @Author Cloud Software LLC
 *  Revision History: 
 *		01/12/2016 - karnold - Formatted code. Added WHERE Active__c = true to DME__C query.
 *
 */
public class SetHCPCFromLegacyOrderBatchServices_CS {

	public static void createDMELineItemsForOrders(List<Order__c> orders) {

		//Map of legacy order id -> map of HCPC code -> quantity
		Map<Id, Map<String, Decimal>> legacyOrderCodes = new Map<Id, Map<String, Decimal>> ();
		for (Order__c o : orders) {
			legacyOrderCodes.put(o.Legacy_Orders__c, getCPTCodes(o.Legacy_Orders__r));
		}

		List<String> DMECodes = new List<String> ();
		for (Order__c o : orders) {
			DMEcodes.addAll(legacyOrderCodes.get(o.Legacy_Orders__c).keySet());
		}

		//DME info from CPT codes
		Map<String, DME__c> codeToDME = new Map<String, DME__c> ();

		for (DME__c dme :[
			SELECT
				Id,
				Name
			FROM DME__c
			WHERE Active__c = true
			AND Name IN :DMEcodes
		]) {
			codeToDME.put(dme.Name, dme);
		}

		//Make new line items
		Map<Id, Order__c> ordersWithParseFailure = new Map<Id, Order__c> ();
		List<DME_Line_Item__c> lineItemsToInsert = new List<DME_Line_Item__c> ();
		for (Order__c o : orders) {

			//CPT codes included in the legacy order
			Map<String, Decimal> orderCPTCodes = getCPTCodes(o.Legacy_Orders__r);
			System.debug('Order CPT codes: ' + orderCPTCodes);

			if (orderCPTCodes.isEmpty()) {
				o.OCR_Parse_Failed__c = true;
				o.Reason_for_OCR_Failure__c = 'Parsing failure/No DMEs entered';
				ordersWithParseFailure.put(o.Id, o);
				continue;
			}

			o.Reason_for_OCR_Failure__c = '';

			//For each code in this order, find the matching dme
			for (String code : orderCPTCodes.keySet()) {

				//Find the matching dme
				if (codeToDME.containsKey(code)) {
					System.debug('Success: Found matching dme code for "' + code + '"');

					//Create the line item for this DME
					DME_Line_Item__c newLineItem = new DME_Line_Item__c(
						DME__c = codeToDME.get(code).Id,
						Order__c = o.Id,
						Quantity__c = orderCPTCodes.get(code)
					);

					lineItemsToInsert.add(newLineItem);

					System.debug('New line item: ' + newLineItem);
				}
				else {
					o.OCR_Parse_Failed__c = true;
					o.Reason_for_OCR_Failure__c += 'No DME w/ code: ' + code + '. ';
					ordersWithParseFailure.put(o.Id, o);
					System.debug('Error: No matching dme code for "' + code + '"');
				}
			}
			o.Reason_for_OCR_Failure__c.abbreviate(255);
		}

		if (!ordersWithParseFailure.isEmpty()) {
			update ordersWithParseFailure.values();
		}

		insert lineItemsToInsert;
	}

	@testVisible
	//Get a map of HCPC code to quantity for a New Order
	private static Map<String, Decimal> getCPTCodes(New_Orders__c no) {

		//Try to parse the CPT codes. If any failures occur, pass an empty list
		Map<String, Decimal> codeToUnits;
		try {

			//Get all CPT codes and their quantities
			List<String> codeList = no.CPT_code_for_OCR__c.split('[,| |\n]+');
			System.debug(codeList);
			List<String> quantityStringList = no.Units_for_OCR__c.split('[,| |\n]+');
			System.debug(quantityStringList);

			//If there are a different number of codes and quantities, leave the map empty
			codeToUnits = new Map<String, Decimal> ();
			if (codeList.size() == quantityStringList.size()) {

				//Convert the quantity string list into a list of decimal values. If any value is not a decimal, throw an error
				List<Decimal> quantityList = new List<Decimal> ();
				for (String quantity : quantityStringList) {
					quantityList.add(Decimal.valueOf(quantity));
				}

				//Map the CPT code to its quantity
				for (Integer i = 0; i < codeList.size(); i++) {
					codeToUnits.put(codeList[i], quantityList[i]);
				}
			}
		} catch(Exception e) {
			codeToUnits = new Map<String, Decimal> ();
		}

		return codeToUnits;
	}
}