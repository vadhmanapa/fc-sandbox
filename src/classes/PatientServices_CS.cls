public without sharing class PatientServices_CS extends AccountServices_CS{

	//TODO: User soql builder instead of standard queries
	private final Set<String> objectFields = new Set<String>{
		'Apartment_Number__c',
		'Cell_Phone_Number__c',
		'City__c',
		'CreatedDate', 
		'CreatedById',
		'Date_of_Birth__c',
		'Email_Address__c',
		'First_Name__c',
		'Geolocation__Latitude__s',
		'Geolocation__Longitude__s',
		'Health_Plan__c',
		'ID_Number__c',
		'LastModifiedDate', 
		'LastModifiedById',
		'Last_Name__c',
		'Medicaid_ID__c',
		'Medicare_ID__c',
		'Medicare_Primary_Patient__c',
		'Name',
		'Native_Language__c',
		'Patient__c',
		'Phone_Number__c',
		'State__c',
		'Street__c',
		'Zip_Code__c'
	};

    private final Map<String, String> orderTableColumns = new Map<String, String> {
        'Insurance ID#' => 'ID_Number__c',
        'First Name' => 'First_Name__c',
        'Last Name' => 'Last_Name__c',
        'Phone Number' => 'Phone_Number__c',
        'Date of Birth' => 'Date_of_Birth__c'
    };

	public PatientServices_CS(String cId){ 
		super(cId);
	}//End Constructor 

    public NestableCondition getBaseConditions(String searchTerm) {
        NestableCondition conditions = new AndCondition();

        //Belongs to the same organization
        if(cAccount.Type__c == 'Provider'){

        }
        else if(cAccount.Type__c == 'Payer'){
            conditions.add(new FieldCondition('Health_Plan__c').equals(cAccount.Id));
            if (String.isNotEmpty(searchTerm)) {
                NestableCondition orCondition = new OrCondition()
                    .add(new FieldCondition('ID_Number__c').likex('%' + searchTerm + '%'))
                    .add(new FieldCondition('Last_Name__c').likex('%' + searchTerm + '%'))
                    .add(new FieldCondition('Medicare_Id__c').likex('%' + searchTerm + '%'))
                    .add(new FieldCondition('Medicaid_Id__c').likex('%' + searchTerm + '%'))
                    .add(new FieldCondition('First_Name__c').likex('%' + searchTerm + '%'))
                    .add(new FieldCondition('Phone_Number__c').likex('%' + searchTerm + '%'))
                    .add(new FieldCondition('Cell_Phone_Number__c').likex('%' + searchTerm + '%'));
                try{
                    Date dateValue = Date.parse(searchTerm);
                    orCondition.add(new FieldCondition('Date_of_Birth__c').equals(dateValue));
                }
                catch (Exception e) {
                }
                List<String> fullName = searchTerm.split(' ');
                if (fullName.size() > 1) {
                    orCondition.add(
                        new AndCondition()
                            .add(new FieldCondition('First_Name__c').likex('%' + fullName[0] + '%'))
                            .add(new FieldCondition('Last_Name__c').likex('%' + fullName[1] + '%'))
                    );
                }
                conditions.add(orCondition);
            }
        }
        else{
            throw new ApplicationException('Invalid Account Type: \"' + cAccount.Type__c + '\"');
        }

        return conditions;
    }

	public NestableCondition getBaseConditions(){
		return getBaseConditions(null);
	}

    public SoqlBuilder getBaseQueryWithCondition(String searchTerm) {
        return new SoqlBuilder()
            .selectx(objectFields)
            .fromx('Plan_Patient__c')
            .limitx(10000)
            .wherex(getBaseConditions(searchTerm));
    }
	
	public SoqlBuilder getBaseQuery(){
        return getBaseQueryWithCondition(null);
	}
	
	public SoqlBuilder attachToBaseQuery(NestableCondition moreConditions){

		NestableCondition conditions = getBaseConditions()
			.add(moreConditions);

		return new SoqlBuilder()
			.selectx(objectFields)				
			.fromx('Plan_Patient__c')
			.limitx(10000)
			.wherex(conditions);
	}

    public String getQueryForPartientSearchWithSort(String searchTerm, Map<String, String> sortColumn) {
        String queryString;
        try {
            SoqlBuilder sb = getBaseQueryWithCondition(searchTerm);

            String sortColumnValue = 'Last Name';
            String sortColumnOrder = 'DESC';
            if (sortColumn != null) {
                sortColumn.remove(null);
                for (String column : sortColumn.keySet()) {
                    sortColumnValue = column;
                    sortColumnOrder = (sortColumn.get(sortColumnValue) != null && String.isNotEmpty(sortColumn.get(sortColumnValue)))
                        ? sortColumn.get(sortColumnValue) : sortColumnOrder;
                }

                System.debug('Ordering by ' + sortColumnValue);
                if (orderTableColumns.containsKey(sortColumnValue)) {
                    OrderBy sortCondition = new OrderBy(orderTableColumns.get(sortColumnValue));
                    if ('DESC'.equalsIgnoreCase(sortColumnOrder)) {
                        sortCondition.descending();
                    } else {
                        sortCondition.ascending();
                    }
                    sortCondition.nullsLast();
                    sb.orderByx(sortCondition);
                }
            }

            queryString = sb.toSoql();
            System.debug('Query: ' + queryString);
        }
        catch (Exception e) {
            System.debug(e.getMessage());
        }
        return queryString;
    }

	public ApexPages.StandardSetController getPatientsWithSort(String searchTerm, Map<String, String> sortColumn) {
        Apexpages.Standardsetcontroller stdSetController;
        try{
            String queryString = getQueryForPartientSearchWithSort(searchTerm, sortColumn);
            System.debug('Query: ' + queryString);
            stdSetController = (String.isNotEmpty(queryString)) ? new Apexpages.Standardsetcontroller(Database.getQueryLocator(queryString)) : null;
        }
        catch(Exception e){
            System.debug(e.getMessage());
        }
        return stdSetController;
    }
	
	public Apexpages.Standardsetcontroller getPatients(){
		return getPatientsWithSort(null, null);
	}
	
	public Plan_Patient__c getPlanPatientById (String patientId){
		Plan_Patient__c pp;

		try{
			
			NestableCondition conditions = new AndCondition()
				.add(new FieldCondition('Id').equals(patientId));
			
			SoqlBuilder query = attachToBaseQuery(conditions);
			
			String queryString = query.toSoql();
			
			System.debug('Query: ' + queryString);
			
			pp = Database.query(queryString);
		}
		catch(Exception e){
			//throw((e instanceOf ApplicationException) ? e : new ApplicationException('Failed to find patient with ID:' + patientId,e));
			system.debug('exception:' + e);
		}
		
		return pp;
		
	}//End getPlanPatientById
	
	/*
	public Apexpages.Standardsetcontroller getPatientsByFilters(Map<String,String> filterMap){
		AndCondition ac = new AndCondition();
		
		if(filterMap.keySet().size() > 0){
			for(String field : filterMap.keySet()){
				if(filterMap.get(field) != '' && filterMap.get(field) != null){
					ac.add(new FieldCondition(field).equals(filterMap.get(field)));
				}
			}
			
			try{
				SoqlBuilder sb = attachToBaseQuery(ac);
				return new Apexpages.Standardsetcontroller(Database.getQueryLocator(sb.toSoql()));				
			}
			catch(Exception e){
				System.debug(e.getMessage());	
			}
			return null;
		}else{
			try{
				SoqlBuilder sb = getBaseQuery();
				return new Apexpages.Standardsetcontroller(Database.getQueryLocator(sb.toSoql()));				
			}
			catch(Exception e){
				System.debug('Error Message: ' + e.getMessage());	
			}
			return null;
		}			
	}
	*/

}//End PatientServices_CS