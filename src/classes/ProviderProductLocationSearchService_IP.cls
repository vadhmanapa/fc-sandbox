/**
 * Created By : Venkat Adhmanapa
 * Description : Controller
 **/
public class ProviderProductLocationSearchService_IP implements ObjectPaginatorListener
{
	/**** User Inputs*****/
	public String stateSelected {get;set;}
	public String userInputForCounty {get;set;}
	public String productSelected {get;set;}
	//public String userInputForProduct {get;set;}
	public Boolean showMedicaidOnly {get;set;}

	/**** System Values****/
	public static String displayTableHeader {get; 
		public set{
			if (String.isBlank(displayTableHeader)) {
				displayTableHeader = 'Select a state to start search';
			}	
		}
	}

	private List<Provider_Location__c> providerLocationDisplayed {get;set;}
	public List<LocationWrapperClass> locationsTable {get;set;}
	public List<Provider_Location__c> showResults {get;set;}
	public ObjectPaginator paginator {get;private set;}

	public ProviderProductLocationSearchService_IP()
	{
		showResults = new List<Provider_Location__c>();
		locationsTable = new List<LocationWrapperClass>();
		showMedicaidOnly = false;

		if (String.isBlank(stateSelected) || String.isBlank(productSelected)) {
			//atleast one selection is required to enter the process
			displayTableHeader = 'Select a state/product to start search';
		}	
		//processDispatcher();
	}
	public List<SelectOption> getStateNames()
	{
		//query states from State Custom Meta data

		List<SelectOption> states = new List<SelectOption>();
		states.add(new SelectOption('','---Select a state---'));
		states.add(new SelectOption('All States and Counties','All States and Counties of US'));

		for(State__mdt state : [SELECT Label FROM State__mdt ORDER BY Label] )
		{
			states.add(new SelectOption(state.Label, state.Label));
		}

		return states;
	}
	public List<SelectOption> getProductDatabaseCategories()
	{
		List<SelectOption> productCat = new List<SelectOption>();
		productCat.add(new SelectOption('','---Select a Product---'));

		List<HCPC_Database_Category__c> categoryList = [SELECT Id, Name FROM HCPC_Database_Category__c WHERE Database__c = 'Integra' ORDER BY Name];
		for(HCPC_Database_Category__c hcpc : categoryList)
		{
			productCat.add(new SelectOption(hcpc.Id, hcpc.Name));
		}

		return productCat;
	}
	//for states and counties
	//for products and product description
	public void processDispatcher(){

		Set<Id> parentLocationServiceMap = new Set<Id>();
		providerLocationDisplayed = new List<Provider_Location__c>();
		System.debug('Checkbox value:'+showMedicaidOnly);
		// preliminary searches and exceptions
		if (String.isBlank(stateSelected) && String.isNotBlank(userInputForCounty)) {
			//throw error to select State first
			ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.FATAL,'State is required and should be selected'));
		}
		
		// go to search state and counties
		if (String.isNotBlank(stateSelected)) {
			
			System.debug('Entering process. Searching for providers in state:'+stateSelected);
			parentLocationServiceMap.addAll(locationsServicedByProviders());

			if (String.isNotBlank(productSelected)) {
				Set<Id> prodResults = new Set<Id>();
				System.debug('Searching for providers for product:'+productSelected);
				prodResults.addAll(productsServicedByProviders());
				System.debug('State Result:'+parentLocationServiceMap);
				System.debug('Product Result:'+prodResults);
				
				if (prodResults.size() > 0) {
					for(Id i: parentLocationServiceMap) {
						if (!prodResults.contains(i)) {
							parentLocationServiceMap.remove(i);
						}else {
							continue;
						}		
					}
				}else {
					parentLocationServiceMap.clear();
				}
				System.debug('Final Result:'+parentLocationServiceMap);		
			}

			if (showMedicaidOnly) {
				Set<Id> medicaidResults = new Set<Id>();
				medicaidResults.addAll(medicaidOnlyProviders());

				if (medicaidResults.size() > 0) {
					for(Id i: parentLocationServiceMap) {
						if (!medicaidResults.contains(i)) {
							parentLocationServiceMap.remove(i);
						}else {
							continue;
						}
					}
				}else {
					parentLocationServiceMap.clear();
				}	
			}
			providerLocationDisplayed = [SELECT Id,
										 Name_DBA__c,
										 All_States_and_Counties__c,
										 Account__r.RecordTypeId,
										 Account__r.Key_Notes__c,
										 Store_Status__c,
										 Location_Phone__c,
										 Location_City__c,
										 Location_State__c,
										 Location_Street__c,
										 Location_Zip_Code__c,
										 Store_County__c,
										 Monday__c,
										 Tuesday__c,
										 Wednesday__c,
										 Thursday__c,
										 Friday__c,
										 Saturday__c,
										 Sunday__c FROM Provider_Location__c
										 WHERE Id IN: parentLocationServiceMap
										 AND Store_Status__c = 'Active'
										 AND Account__r.Status__c = 'Active'
										 ORDER BY All_States_and_Counties__c ASC, Name_DBA__c ASC ];


			if (providerLocationDisplayed.size() > 0) {
				paginator = new ObjectPaginator(20,this);
                paginator.setRecords(providerLocationDisplayed);
			}else {
				ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'NO RECORDS TO DISPLAY'));
                paginator = new ObjectPaginator(0,this);
                paginator.setRecords(providerLocationDisplayed);
			}		
		}else {
			// throw state required error
			ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.FATAL,'State is required and should be selected'));
		}	
			
		//consolidatedResults();				
	}

	public void handlePageChange(List<Object> newPage){

        //showResults.clear();
        locationsTable.clear();

        if(newPage != null){
            for(Object con : newPage){
                //showResults.add((Provider_Location__c)con);
                locationsTable.add(new LocationWrapperClass((Provider_Location__c) con));
            }
        }
    }

	public Set<Id> locationsServicedByProviders(){
		Set<Id> providerServicingArea = new Set<Id>();
		if(stateSelected != 'All States and Counties') {

			String queryString = 'Select Id, Provider_Location_New__c, State__c, County_Name__c FROM County_Item__c';
			queryString+= ' '+ 'WHERE State__c =:stateSelected';
			queryString+= ' '+ 'AND Provider_Location_New__r.Store_Status__c = \'Active\'';
			if (String.isNotBlank(userInputForCounty)) {
				queryString+= ' '+'AND';
				queryString+= ' '+'(County_Name__c LIKE \'%' + String.escapeSingleQuotes(userInputForCounty) + '%\')';
			}

			List<County_Item__c> servicedLocations = Database.query(queryString);
			if (servicedLocations.size() > 0) {
				for(County_Item__c c: servicedLocations) {
					providerServicingArea.add(c.Provider_Location_New__c);
				}
			}
		}

		String nationalProviders = 'Select Id from Provider_Location__c';
		nationalProviders+= ' '+ 'WHERE All_States_and_Counties__c = true';
		nationalProviders+= ' '+ 'AND Store_Status__c = \'Active\'';

		List<Provider_Location__c> nationalLocations = Database.query(nationalProviders);
		if(nationalLocations.size() > 0) {
			for(Provider_Location__c c: nationalLocations) {
				providerServicingArea.add(c.Id);
			}
		}

		return providerServicingArea;	
	}

	public Set<Id> productsServicedByProviders(){
		Set<Id> providerServicingProducts = new Set<Id>();
		Set<Id> providerLocServicingProducts = new Set<Id>();
		Set<Id> hcpcDatabaseCategory = new Set<Id>();
		String queryString = 'SELECT Id, (Select Id, Provider__c from Provider_Categories__r) FROM HCPC_Database_Category__c';
		queryString+= ' '+'WHERE Id=: productSelected';
		
		// ****** to be included in next version roll out *******

		/*if (String.isNotBlank(userInputForProduct)) {
			// commence SOSL search in order to search code or description
			String sosl = 'FIND '+userInputForProduct+'IN ALL FIELDS RETURNING HCPC_Category__c(Id,HCPC_Database_Category__c)';
			List<List<sObject>> soslResults = search.query(sosl);
			if (soslResults.size() > 0) {
				List<HCPC_Category__c> hcpcList = ((List<HCPC_Category__c>)soslResults[0]);
				for(HCPC_Category__c hcpc: hcpcList) {
					hcpcDatabaseCategory.add(hcpc.HCPC_Database_Category__c);
				}

				queryString+= ' '+'WHERE Id IN:'+ hcpcDatabaseCategory;
			}
		}else {
			queryString+= ' '+'WHERE Id IN:'+ (Id)productSelected;
		}*/ 

		List<HCPC_Database_Category__c> productCatgs = Database.query(queryString);

		if (productCatgs.size() > 0) {
			for(HCPC_Database_Category__c cat: productCatgs) {
				if (!cat.Provider_Categories__r.isEmpty()) {
					for(Provider_Category__c pCat: cat.Provider_Categories__r) {
						providerServicingProducts.add(pCat.Provider__c); // get the accounts from here
					}
				}		
			}
		}
		if (providerServicingProducts.size() > 0) {
			List<Provider_Location__c> providerLocations = [SELECT Id, Name from Provider_Location__c WHERE Account__c IN: providerServicingProducts AND (Store_Status__c = 'Active' AND Account__r.Status__c = 'Active')];
			for(Provider_Location__c pl: providerLocations) {
				providerLocServicingProducts.add(pl.Id);
			}
		}
			
		return providerLocServicingProducts;		
	}

	public Set<Id> medicaidOnlyProviders(){

		Set<Id> medicaidProviderIds = new Set<Id>();
		String queryString = 'SELECT Id, Provider_Location_New__c, Medicaid__c, State__c FROM State_Identifier__c';
		if(stateSelected == 'Florida') {
			queryString+= ' ' + 'WHERE State__c IN' + '(\'Florida1\'';
			queryString+= ',' + '\'Florida2\')';
		}else {
			queryString+= ' '+ 'WHERE State__c=:stateSelected';
		}
		queryString+= ' '+ 'AND Provider_Location_New__r.Store_Status__c = \'Active\'';

		List<State_Identifier__c> stateMedicaids = Database.query(queryString);
		if (stateMedicaids.size() > 0) {
			for(State_Identifier__c si: stateMedicaids) {
				medicaidProviderIds.add(si.Provider_Location_New__c);
			}
		}

		return medicaidProviderIds;	
	}

	public class LocationWrapperClass {
		public Provider_Location__c pLoc {get;set;}
		public String recordTypeName {get;set;}
		public String denoteNationalProvider {get;set;}
		public transient Map<Id, Schema.RecordTypeInfo> rt_map = Schema.getGlobalDescribe().get('Account').getDescribe().getRecordTypeInfosById();
		public LocationWrapperClass(Provider_Location__c pl){
			this.pLoc = pl;
			this.recordTypeName = rt_map.get(pl.Account__r.RecordTypeId).getName();
			if(pl.All_States_and_Counties__c == true) {
				this.denoteNationalProvider = 'Yes';
			}else {
				this.denoteNationalProvider = 'No';
			}
		}
	}

	public PageReference clearAllValues (){
		PageReference newPg = Page.ProviderSearchPage;
		newPg.setRedirect(true);
		return newPg;
	}
}