/*===============================================================================================
 * Cloud Software LLC
 * Name: ClearAlgorithmRoutingCustomSetting
 * Description: Utility class for the Clear Algorithm Routing Custom Setting
 * Created Date: 9/22/2015
 * Created By: Andrew Nash (Cloud Software)
 * 
 * 	Date Modified			Modified By			Description of the update
 *	SEP 21, 2015			Andrew Nash			Created
 *
 *		01/07/2015 - karnold - Class is deprecated. Will be deleted on next release. Functionality moved to CustomSettingServices
 ==============================================================================================*/
public without sharing class ClearAlgorithmRoutingCustomSetting {
	/*
	@testVisible private static Boolean useNewAlgorithm;
	@testVisible private static Boolean test_useNewAlgorithm;

	//===========================================================================================
	//============================== USE CLEAR ALGORITHM ROUTING ================================
	//===========================================================================================
	public static Boolean UseClearAlgorithmRouting() {
		
		// Check to see if the test boolean variable is set.
		if(test_useNewAlgorithm == null) {
			// If the test boolean variable is null set it to false.
			test_useNewAlgorithm = false;
		}

		// Get the value of the custom setting.
		Clear_Algorithm_Routing__c customSettingValue = Clear_Algorithm_Routing__c.getValues('Default');
		// Check to see if the custom setting is null.
		if(customSettingValue == null) {
			// If the custom setting is null set it to false.
			useNewAlgorithm = false;
		} else {
			// Store the value of the custom setting to the boolean variable
			useNewAlgorithm = customSettingValue.Use_New_Algorithm__c;
		}

		// Check to see if a test is being ran.
		if(Test.isRunningTest()) {
			// Return the value of the test boolean variable.
			return test_useNewAlgorithm;
		} else {
			// Return the boolean value of the custom setting.
			return useNewAlgorithm;
		}
	}
	*/
}