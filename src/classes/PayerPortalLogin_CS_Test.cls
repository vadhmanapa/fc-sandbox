//Login with expired password not possible as the created date for password cannot be
//updated. Too many login attempts tested on the page, but cannot be tested in a test
//class due to the lack of a sleep() function.
	
@isTest
private class PayerPortalLogin_CS_Test {
	
	/******* Test Parameters *******/
	static final Integer N_ORDERS = 5;

	/******* Provider *******/
	static private List<Account> providers;
	static private List<Contact> providerUsers;
	
	/******* Payer *******/
	static private List<Account> payers;
	static private List<Contact> payerUsers;
	
	/******* Patients *******/
	static private List<Patient__c> patients;
	static private List<Plan_Patient__c> planPatients;
	
	/******* Orders *******/
	static private List<Order__c> orders;
	
	/******* Test Objects *******/
	static private PayerPortalLogin_CS login;

	/******* Test Methods *******/    
   
   	static testMethod void PayerPortalLogin_Test_PageLanding(){
   		init();
   	}
   
   	static testMethod void PayerPortalLogin_Test_Login(){
   		init();
   		
   		login.username = payerUsers[0].Username__c;
   		login.password = 'abcABC123!';
   		PageReference pr = login.login();
   		
   		System.assert(login.portalUser != NULL);
   	}

   	static testMethod void PayerPortalLogin_Test_LoginAttemptTooSoon(){
   		init();
   		
   		login.username = payerUsers[0].Username__c;
   		login.password = 'wrongPassword';   		
   		PageReference pr = login.login(); 
   		
   		login.password = 'abcABC123!';
   		pr = login.login();
   		
   		System.assertEquals('You must wait at least 1 second between login attempts', login.loginErrorMessage);
   	}
   	
	static testMethod void PayerPortalLogin_Test_UsernameDoesntExist(){
		init();
		
		login.username = 'FakeUsername';
		login.password = 'PasswordDoesntMatter';
		PageReference pr = login.login();
		
		System.assertEquals(NULL, login.portalUser);
	}
	
	static testMethod void PayerPortalLogin_Test_ProviderUsername(){
		init();
		
		login.username = providerUsers[0].Username__c;
		login.password = 'PasswordDoesntMatter';
		PageReference pr = login.login();
		
		System.assertEquals('/apex/payerportallogin_cs', ApexPages.currentPage().getUrl());
	}

   	static testMethod void PayerPortalLogin_Test_InvalidPassword(){
   		init();
   		
   		login.username = payerUsers[0].Username__c;
   		login.password = ' ';
   		PageReference pr = login.login();
   		
   		System.assertEquals(PayerPortalLogin_CS.invalidLogin, login.loginErrorMessage);   		
   	}
   	
   	static testMethod void PayerPortalLogin_Test_InactiveUserLogin(){
   		init();
   		
   		payerUsers[0].active__c = false;
   		update payerUsers;
   		login.username = payerUsers[0].Username__c;
   		login.password = 'abcABC123!';
   		PageReference pr = login.login();
   		
   		System.assertEquals(PayerPortalLogin_CS.deactivatedUser, login.loginErrorMessage);
   	}
	
	//Cannot check whether an email is sent successfully or not - test class will always
	//assume success.
   	static testMethod void PayerPortalLogin_Test_ForgotPassword(){
   		init();
   		
   		login.username = payerUsers[0].Username__c;
   		PageReference pr = login.forgotPassword();
   		
   		System.assertEquals('An email has been sent to ' + login.username, login.forgotPasswordMsg);
   	}
   	
   	static testMethod void PayerPortalLogin_Test_ForgotPasswordNoUsername() {
   		init();
   		PageReference pr = login.forgotPassword();
   		
   		System.assertEquals(PayerPortalLogin_CS.missingUsername,login.errLogin);
   	}
   	
   	static testMethod void PayerPortalLogin_Test_ForgotPasswordNoEmail() {
   		init();
   		PageReference pr = login.forgotPassword();
   		
   		System.assertEquals(PayerPortalLogin_CS.missingUsername,login.errLogin);
   	}
   	
   	//For Test Coverage only: forgotUsername() just returns the PayerForgotUsername page.
   	static testMethod void PayerPortalLogin_Test_forgotUserName(){
   		init();
   		PageReference pr = login.forgotUsername();
   		
   		System.assert(true);
   	}
   
   	static testMethod void PayerPortalLogin_Test_RetrieveUsername(){
   		init();

   		login.userEmailAddress = payerUsers[0].Email;
   		PageReference pr = login.retrieveUsername();
   		
   		System.assertEquals(null, login.errForgotPass);
   	}
   	
   	static testMethod void PayerPortalLogin_Test_RetrieveUsernameNoEmail(){
   		init();
   		
   		login.userEmailAddress = '';
   		PageReference pr = login.retrieveUsername();
   		
   		System.assertEquals(PayerPortalLogin_CS.missingEmail, login.errForgotPass);
   	}
   
	static void init(){
    	
    	///////////////////////////////////
		// Data
		///////////////////////////////////
    	
    	//Providers
		providers = TestDataFactory_CS.generateProviders('TestProvider', 1);
		insert providers;

		providerUsers = TestDataFactory_CS.generateProviderContacts('ProviderUser', providers, 1);
		insert providerUsers;	
		
		//Payers
		payers = TestDataFactory_CS.generatePlans('TestPayer', 1);
		insert payers;
		
		payerUsers = TestDataFactory_CS.createPlanContacts('TestPlan', payers, 1);
		
		//Patients
		patients = TestDataFactory_CS.generatePatients('TestPatient', 1);
		insert patients;
		
		//Plan patients
		planPatients = TestDataFactory_CS.generatePlanPatients(
			new Map<Id,List<Patient__c>>{
				payers[0].Id => new List<Patient__c>{patients[0]}
			}
		);
		insert planPatients;
		
		//Orders, set status to 'New' yielding stage 'Pending Acceptance'
		orders = TestDataFactory_CS.generateOrders(planPatients[0].Id, providers[0].Id, N_ORDERS);
		for(Order__c o : orders){
			o.Status__c = OrderModel_CS.STATUS_NEW;
		}
		insert orders;

		///////////////////////////////////
		// Page Landing
		///////////////////////////////////

		//Set the page and authentication cookie
		Test.setCurrentPage(Page.PayerPortalLogin_CS.setRedirect(true));
		
		//Log the desired user in
		TestServices_CS.login(payerUsers[0]);
    	
    	//Make provider portal services
    	login = new PayerPortalLogin_CS();
    	login.init();
    }  
}