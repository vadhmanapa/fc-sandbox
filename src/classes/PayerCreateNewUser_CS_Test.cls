/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PayerCreateNewUser_CS_Test {
	public static Account payer;
	public static Contact user;
	
    static testMethod void PayerCreateNewUser_CS_Test() {
    	init();
    	Test.setCurrentPage(Page.PayerCreateNewUser_CS);
    	TestServices_CS.login(user);
    	
    	PayerCreateNewUser_CS cont = new PayerCreateNewUser_CS();
    	cont.init();
    	system.assertNotEquals(null, cont.newUser);
    	
    	cont.username = 'testUserName';
    	
    	cont.newUser.Health_Plan_User_Id__c = '008497';
    	cont.newUser.LastName = 'testLast';
    	
    	PageReference pr = cont.createNewUser();
    	
    	System.debug('returned url: ' + pr.getURL());
    	System.debug('expected url: ' + Page.PayerUsersSummary_CS.getURL());
    	
    	system.assert(pr.getURL() == Page.PayerUsersSummary_CS.getURL(), 'User not created.');
    	
    	Contact newUser = [
    		SELECT
    			Entity__c,
    			Password__c,
    			Salt__c,
    			Active__c
    		FROM Contact
    		WHERE Username__c = 'testUserName'
    	];
    	
    	system.assertEquals(payer.Id, newUser.Entity__c);
    	system.assertNotEquals(null, newUser.Password__c);
    	system.assertNotEquals(null, newUser.Salt__c);
    	system.assertEquals(true, newUser.Active__c);
    }

	static testMethod void PayerCreateNewUser_CS_Test_Change_Content_Role() {
    	init();

		Role__c userRole2 = new Role__c();
    	userRole2.Create_Users__c = true;
		userRole2.Health_Plan_Visible__c = true;
    	insert userRole2;

    	Test.setCurrentPage(Page.PayerCreateNewUser_CS);
    	TestServices_CS.login(user);
    	
    	PayerCreateNewUser_CS cont = new PayerCreateNewUser_CS();
    	cont.init();
    	system.assertNotEquals(null, cont.newUser);
    	
    	cont.username = 'testUserName';
    	cont.newUser.Health_Plan_User_Id__c = '008497';
    	cont.newUser.LastName = 'testLast';
		cont.selectedContentRoleId = userRole2.Id;
    	
    	PageReference pr = cont.createNewUser();

		Contact newUser = [
    		SELECT
    			Content_Role__c
    		FROM Contact
    		WHERE Username__c = 'testUserName'
    	];

		system.assertEquals(2, cont.contentRoleSelectOptions.size());
		system.assertEquals(userRole2.Id, newUser.Content_Role__c);
    }
    
    static testMethod void PayerCreateNewUser_CS_initNotAuth(){
    	init();
    	Test.setCurrentPage(Page.PayerCreateNewUser_CS);
    	
    	PayerCreateNewUser_CS cont = new PayerCreateNewUser_CS();
    	cont.init();
    }

    public static void init(){
    	List<Account> initAccts = TestDataFactory_CS.generatePlans('TestPlan', 2);
    	payer = initAccts[0];
    	insert initAccts;
    	
    	Role__c userRole = new Role__c();
    	userRole.Create_Users__c = true;
    	insert userRole;
    	
    	user = TestDataFactory_CS.createPlanContacts('Admin', initAccts, 1)[0];
    	user.Content_Role__c = userRole.Id;    	
    	update user;
    }
}