global class OrdersUpdateToReacceptanceBatch implements Database.Batchable<sObject> {
    
	private String query;
	
	public OrdersUpdateToReacceptanceBatch() {
		Date jobRunDate = System.today();
        Time jobRunTime = Time.newInstance(0, 0, 0, 0); // to match the system local time
		Datetime newRunTime = Datetime.newInstanceGmt(jobRunDate, jobRunTime);
        String dayOfWeek = newRunTime.format('EEEE'); //This returns - Monday, Tuesday, Wednesday, etc..
		Integer orderLookupMonToThurStart = 3;
		Integer orderLookupMonToThurEnd = 4;
		Integer orderLookupFriStart = 3;
		Integer orderLookupFriEnd = 6;
        //Map to check if the method can run or not
        Map<String, Boolean> jobRunDaysCheck = new Map<String, Boolean>{
			'Monday' => true,
			'Tuesday' => true,
			'Wednesday' => true,
			'Thursday' => true,
			'Friday' => true,
			'Saturday' => false,
			'Sunday' => false
		};
        
        // parent conditions
        NestableCondition orderConditions = new AndCondition();
		orderConditions.add(new FieldCondition('Status__c').equals(OrderModel_CS.STATUS_FUTURE_DOS));
        
        //Get run permission from jobRunDaysCheck Map
        
        if(jobRunDaysCheck.get(dayOfWeek)){
            //if true
            System.debug('Running day is a weekday'+dayOfWeek);
            if(!dayOfWeek.equalsIgnoreCase('Friday')) {
			// today 12AM to before next day 12AM
			orderConditions.add(new FieldCondition('Estimated_Delivery_Time__c').greaterThanOrEqualTo(newRunTime.addDays(orderLookupMonToThurStart)));
			orderConditions.add(new FieldCondition('Estimated_Delivery_Time__c').lessThan(newRunTime.addDays(orderLookupMonToThurEnd)));
            }else {
                // Monday 12 AM to before Thursday 12 AM
                orderConditions.add(new FieldCondition('Estimated_Delivery_Time__c').greaterThanOrEqualTo(newRunTime.addDays(orderLookupFriStart)));
                orderConditions.add(new FieldCondition('Estimated_Delivery_Time__c').lessThan(newRunTime.addDays(orderLookupFriEnd)));
            }
        }else{ 
            //if false
            System.debug('Running day is a weekend. No processing of batch'+dayOfWeek);
            orderConditions.add(new FieldCondition('Name').Equals('')); // in case if this batch runs, return zero records
        }
        
        query = new SoqlBuilder()
						 .selectx(new Set<String>{'Id','Stage__c','Estimated_Delivery_Time__c','Status__c'})
						 .fromx('Order__c')
						 .wherex(orderConditions)
						 .toSoql();
        
        System.debug('Final query to be run:'+query);
	}
    
    public void setQuery(String query) {
		this.query = query;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
			System.debug('\n\n this.queryString => ' + this.query + '\n');
            return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<Order__c> scope) {

            for(Order__c ord: scope) {
   				ord.Status__c = OrderModel_CS.STATUS_PENDING_REACCEPTANCE;
   			}

   			update scope;     
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
}