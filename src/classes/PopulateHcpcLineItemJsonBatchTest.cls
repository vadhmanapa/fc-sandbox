/**
 *  @Description Test class for PopulateHcpcLineItemJsonBatch
 *  @Author Cloud Software LLC, Keith Arnold
 *  Revision History: 
 *		01/12/2016 - karnold - Created
 *
 */
 @isTest
public class PopulateHcpcLineItemJsonBatchTest {
	
	private static testMethod void batch_Test() {
		// GIVEN
		TestDataFactory_CS testData = new TestDataFactory_CS();
		
		// Do pre-asserts to verify test data.
		List<Order__c> orderList = [
			SELECT HCPC_Line_Item_JSON__c
			FROM Order__c
		];

		System.assert(!orderList.isEmpty());
		for (Order__c order : orderList) {
			System.assertEquals(null, order.HCPC_Line_Item_JSON__c);
		}

		PopulateHcpcLineItemJsonBatch batch = new PopulateHcpcLineItemJsonBatch();

		// WHEN
		Test.startTest();
		Database.executeBatch(batch);
		Test.stopTest();

		// THEN
		List<Order__c> orderPostList = [
			SELECT HCPC_Line_Item_JSON__c
			FROM Order__c
		];

		//Assert that the list has items and then assert that each order was populated correctly
		System.assert(!orderPostList.isEmpty());
		for (Order__c order : orderPostList) {
			System.assertNotEquals(null, order.HCPC_Line_Item_JSON__c);
			
			List<DmeLineItemWrapper_CS> wrapperList = (List<DmeLineItemWrapper_CS>) 
				JSON.deserialize(order.HCPC_Line_Item_JSON__c, List<DmeLineItemWrapper_CS>.class);
			System.assertEquals(testData.getLineItemList().size(), wrapperList.size());
		}
	}

	
	private static testMethod void passInQueryBatch_Test() {
		// GIVEN
		TestDataFactory_CS testData = new TestDataFactory_CS();

		// Do pre-asserts to verify test data.
		List<Order__c> orderList = [
			SELECT HCPC_Line_Item_JSON__c
			FROM Order__c
		];

		System.assert(!orderList.isEmpty());
		for (Order__c order : orderList) {
			System.assertEquals(null, order.HCPC_Line_Item_JSON__c);
		}

		String query = 
			'SELECT (SELECT Order__c, DME__c, Quantity__c, Product_Code__c FROM DMEs__r) ' +
			'FROM Order__c';
		PopulateHcpcLineItemJsonBatch batch = new PopulateHcpcLineItemJsonBatch(query);

		// WHEN
		Test.startTest();
		Database.executeBatch(batch);
		Test.stopTest();

		// THEN
		List<Order__c> orderPostList = [
			SELECT HCPC_Line_Item_JSON__c
			FROM Order__c
		];

		//Assert that the list has items and then assert that each order was populated correctly
		System.assert(!orderPostList.isEmpty());
		for (Order__c order : orderPostList) {
			System.assertNotEquals(null, order.HCPC_Line_Item_JSON__c);
			
			List<DmeLineItemWrapper_CS> wrapperList = (List<DmeLineItemWrapper_CS>) 
				JSON.deserialize(order.HCPC_Line_Item_JSON__c, List<DmeLineItemWrapper_CS>.class);
			System.assertEquals(testData.getLineItemList().size(), wrapperList.size());
		}
	}

		private static testMethod void noLineItemBatch_Test() {
		// GIVEN
		new TestDataFactory_CS();

		List<DME_Line_Item__c> lineItemList = [
			SELECT Id
			FROM DME_Line_Item__c
		];

		delete lineItemList;

		// Do pre-asserts to verify test data.
		List<Order__c> orderList = [
			SELECT HCPC_Line_Item_JSON__c
			FROM Order__c
		];

		System.assert(!orderList.isEmpty());
		for (Order__c order : orderList) {
			System.assertEquals(null, order.HCPC_Line_Item_JSON__c);
		}

		PopulateHcpcLineItemJsonBatch batch = new PopulateHcpcLineItemJsonBatch();

		// WHEN
		Test.startTest();
		Database.executeBatch(batch);
		Test.stopTest();

		// THEN
		List<Order__c> orderPostList = [
			SELECT HCPC_Line_Item_JSON__c
			FROM Order__c
		];

		// Assert that each order has an empty array (because there were no line items)
		System.assert(!orderPostList.isEmpty());
		for (Order__c order : orderPostList) {
			System.assertEquals('[]', order.HCPC_Line_Item_JSON__c);
		}
	}
}