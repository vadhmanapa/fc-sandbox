@isTest
private class ProviderPortalLogin_CS_Test {
	
	public static List<Account> providerList;
	public static List<Contact> provAdminList;
	public static List<Account> payers;
	public static List<Contact> payerUsers;
	public static Contact c;
	
    static testMethod void ProviderPortalLogin_CS_Test() {
   		init();
   		c.Salt__c = 'testSalt';
		c.Password__c = PasswordServices_CS.hash('testPassword3', c.Salt__c);
   		update c;
   		
   		Test.setCurrentPage(Page.ProviderPortalLogin_CS);
   		ProviderPortalLogin_CS cont = new ProviderPortalLogin_CS();
   		
   		cont.username = c.Username__c;
   		cont.userEmailAddress = c.Email;
   		cont.password = 'testPassword3';
		
		cont.login();
		system.assert(ApexPages.currentPage().getCookies().containsKey('userId'));
		
		cont.forgotPassword();
		system.assertNotEquals(PasswordServices_CS.hash('testPassword3', 'testSalt'), [SELECT Password__c FROM Contact WHERE Id = :c.Id].Password__c);
		
		cont.forgotUsername();
		
		cont.retrieveUsername();
    }
    
    static testMethod void ProviderPortalLogin_CS_loginMinimumDelay() {
   		init();
   		
   		ProviderPortalLogin_CS cont = new ProviderPortalLogin_CS();
   		cont.username = c.Username__c;
   		cont.lastAttempt = system.now();
   		
		test.startTest();
		cont.login();
		test.stopTest();
    }
    
    static testMethod void ProviderPortalLogin_CS_loginMaxAttempt() {
   		init();
   		
   		c.Password__c = PasswordServices_CS.hash('testPassword1', c.Salt__c);
   		update c;
   		
   		ProviderPortalLogin_CS cont = new ProviderPortalLogin_CS();
   		cont.username = c.Username__c;
		cont.password = 'testPassword2';
		
		test.startTest();
		for(Integer i = 0; i < 6; i++){
			cont.login();
		}
		test.stopTest();
		
		system.assertEquals(false, [SELECT Active__c FROM Contact WHERE Id = :c.Id].Active__c);
    }
    
    static testMethod void ProviderPortalLogin_CS_loginNoUser() {
   		init();
   		c.Entity__c = null;
   		update c;
   		
   		ProviderPortalLogin_CS cont = new ProviderPortalLogin_CS();
   		cont.username = c.Username__c;
		
		test.startTest();
		cont.login();
		test.stopTest();
    }
    
    static testMethod void ProviderPortalLogin_CS_loginInactive() {
   		init();
   		c.Active__c = false;
   		update c;
   		
   		ProviderPortalLogin_CS cont = new ProviderPortalLogin_CS();
   		cont.username = c.Username__c;
		
		test.startTest();
		cont.login();
		test.stopTest();
    }
    
    static testMethod void ProviderPortalLogin_CS_loginWrongPassword() {
   		init();
   		c.Password__c = 'testPassword8';
   		c.Salt__c = 'sugar';
   		update c;
   		
   		ProviderPortalLogin_CS cont = new ProviderPortalLogin_CS();
   		cont.username = c.Username__c;
   		cont.password = 'testPass7';
		
		test.startTest();
		cont.login();
		test.stopTest();
		
		system.assertEquals(1, cont.portalUser.instance.Login_Attempts__c);
		system.assertEquals(ProviderPortalLogin_CS.invalidLogin + '<br/>Remaining login attempts: ' + 
						(ProviderPortalLogin_CS.MAX_ATTEMPT_COUNT - Integer.valueOf(cont.portalUser.instance.Login_Attempts__c)), cont.loginErrorMessage);
    }
    
    static testMethod void ProviderPortalLogin_CS_PayerUsername() {
		init();
		
		Test.setCurrentPage(Page.ProviderPortalLogin_CS.setRedirect(true));
		ProviderPortalLogin_CS cont = new ProviderPortalLogin_CS();
		cont.init();
		
		Test.startTest();
		
		cont.username = payerUsers[0].Username__c;
		cont.password = 'PasswordDoesntMatter';
		PageReference pr = cont.login();
		
		Test.stopTest();
		
		System.assertEquals('/apex/providerportallogin_cs', ApexPages.currentPage().getURL());
    }
    
    static testMethod void ProviderPortalLogin_CS_loginNoPassword() {
   		init();
   		c.Password__c = null;
   		c.Salt__c = null;
   		update c;
   		
   		ProviderPortalLogin_CS cont = new ProviderPortalLogin_CS();
   		cont.username = c.Username__c;
   		cont.password = 'testPass7';
		
		test.startTest();
		cont.login();
		test.stopTest();
    }
    
    static testMethod void ProviderPortalLogin_CS_PasswordExpired(){
    	init();
    	c.Password_Set_Date__c = system.today().addDays(-200);
    	c.Salt__c = 'testSalt';
		c.Password__c = PasswordServices_CS.hash('testPassword3', c.Salt__c);
		update c;
		
		ProviderPortalLogin_CS cont = new ProviderPortalLogin_CS();
		cont.username = c.Username__c;
		cont.password = 'testPassword3';
		
		test.startTest();
		cont.login();
		test.stopTest();
		
		system.assertEquals(true, [SELECT Password_Reset__c FROM Contact WHERE Id = :c.Id].Password_Reset__c);
    }
    
    static testMethod void ProviderPortalLogin_CS_nullForgotPasswordTest() {
   		
   		ProviderPortalLogin_CS cont = new ProviderPortalLogin_CS();
   		
		test.startTest();
		
		cont.forgotPassword();
		
		
		test.stopTest();
    }
    
    static testMethod void ProviderPortalLogin_CS_retrieveUserNameFailTest() {
   		
   		init();
   		
   		ProviderPortalLogin_CS cont = new ProviderPortalLogin_CS();
   		
   		cont.userEmailAddress = 'testEmailToFail@mail.com';
		
		test.startTest();
		
		try {
		
			cont.retrieveUsername();
		}
		catch(Exception e) {
			
		}
		
		
		test.stopTest();
    }
    
    static testMethod void ProviderPortalLogin_CS_retrieveUserNameNoEmail(){
    	init();
    	ProviderPortalLogin_CS cont = new ProviderPortalLogin_CS();
    	
    	test.startTest();
    	cont.retrieveUsername();
    	test.stopTest();
    }
   	
   	static testMethod void ProviderPortalLogin_CS_wrongUserNameForgotPasswordTest() {
   		
   		//init();
   		
   		ProviderPortalLogin_CS cont = new ProviderPortalLogin_CS();
   		
   		cont.username = 'TestUN';
   		
   		
		test.startTest();
		
		try {
			cont.forgotPassword();
		}
		catch(Exception e) {
			
		}
		
		test.stopTest();
    }
    
    static testMethod void ProviderPortalLogin_CS_TestFailedLoginTest() {
   		
   		init();
   		
   		ProviderPortalLogin_CS cont = new ProviderPortalLogin_CS();
   		
   		cont.username = 'TestUN';
   		
   		
		test.startTest();
		
		try {
			cont.login();
		}
		catch(Exception e) {
			
		}
		
		test.stopTest();
    }
    
   	static testMethod void notClearEnabled() {
    	
    	providerList = TestDataFactory_CS.generateProviders('testProvider', 1);
    	providerList[0].Clear_Enabled__c = false;
    	insert providerList;
    	
    	provAdminList = TestDataFactory_CS.generateProviderContacts('Admin', providerList, 1);
    	insert provAdminList;
    	
    	c = [select Id, Active__c, Password__c, Salt__c, Entity__r.Type__c, Username__c, Email from Contact where Id = :provAdminList[0].Id];
   		
   		c.Salt__c = 'testSalt';
		c.Password__c = PasswordServices_CS.hash('testPassword3', c.Salt__c);
   		update c;
   		
   		Test.setCurrentPage(Page.ProviderPortalLogin_CS);
   		ProviderPortalLogin_CS cont = new ProviderPortalLogin_CS();
   		
   		cont.username = c.Username__c;
   		cont.userEmailAddress = c.Email;
   		cont.password = 'testPassword3';
		
		cont.login();
		
		test.startTest();
		cont.login();
		test.stopTest();
		
		System.assertEquals(ProviderPortalLogin_CS.notClearEnabled, cont.loginErrorMessage);
    }
	
    //INIT
    static void init(){
    	
    	providerList = TestDataFactory_CS.generateProviders('testProvider', 1);
    	insert providerList;
    	
    	provAdminList = TestDataFactory_CS.createProviderContacts('Admin', providerList, 1);
    	
    	c = [select Id, Active__c, Password__c, Salt__c, Entity__r.Type__c, Username__c, Email from Contact where Id = :provAdminList[0].Id];
    	
    	
    	//For testing redirect
		payers = TestDataFactory_CS.generatePlans('TestPayer', 1);
		insert payers;
		payerUsers = TestDataFactory_CS.createPlanContacts('TestPlan', payers, 1);
    	
    	
    }//End Init
    

}