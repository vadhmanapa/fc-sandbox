public class CaseActionControllerExtensionv2_IP extends PageControllerBase_IP {
    
    public Id actionDetailId;
    public CaseReasonComponentController_IP reasonCompController {set;
        get{
            if(getComponentControllerMap() != null) {
                CaseReasonComponentController_IP crc;
                crc = (CaseReasonComponentController_IP)getComponentControllerMap().get('reasonComponent');
                if(crc != null) {
                    return crc;
                }
            }
            return new CaseReasonComponentController_IP();
        }
    }

    public CaseActionandDetailsController_IP actionandDetails{set;
        get{
            if(getComponentControllerMap() != null) {
                CaseActionandDetailsController_IP actions;
                actions = (CaseActionandDetailsController_IP) getComponentControllerMap().get('actionComponent');
                if(actions != null) {
                    return actions;
                }
            }

            return new CaseActionandDetailsController_IP();
        }
    }

    public CaseActionControllerExtensionv2_IP(ApexPages.StandardController stdController) {
        caseLog.userAction = (Case_Action__c)stdController.getRecord();
        String cId = ApexPages.currentPage().getParameters().get('cId') != null ? ApexPages.currentPage().getParameters().get('cId') : ApexPages.currentPage().getParameters().get('retURL').replace('%2F','').replace('/','');
        String splitId = cId.substringBefore('?');
        System.debug('Id'+splitId);
        caseLog.caseDetails = [SELECT Id, Account.Name, Contact.Name, RecordTypeId, CaseNumber, Status, Subject, Description, CaseReason__c, CaseReasonDetail__c, Last_Case_Comment__c, CallerType__c,
                      CaseReasonId__c, CaseReasonDetailId__c, CaseActionId__c, CreatedDate,SuppliedCompany, SuppliedName, SuppliedPhone, SuppliedEmail
                      FROM Case 
                      WHERE Id=: (Id)splitId LIMIT 1];

        caseLog.userAction.Case__c = caseLog.caseDetails.Id;
    }

    public PageReference save(){

        if(String.isNotBlank(this.actionandDetails.actionDetailSelected)){
            
            actionDetailId = (Id)this.actionandDetails.actionDetailSelected;
        }

        if(String.isNotBlank((String)actionDetailId)) {
            
            ActionDetail__c actToUpdate = CaseReasonRelationshipHelper_IP.getActionValues((Id)this.actionDetailId);
            
            if(actToUpdate != null){
                
                caseLog.userAction.CaseAction__c = actToUpdate.Action__c;
                caseLog.userAction.CaseActionDetail__c = actToUpdate.ActionDetail__c;
                caseLog.userAction.Case_Comment__c = this.actionandDetails.userComments;
                caseLog.caseDetails.CaseActionId__c = actToUpdate.Id;
            }
        }
        
        reasonCompController.setParentControllerValues();

        if(validated()){

            update caseLog.caseDetails;
            insert caseLog.userAction;
            return new PageReference('/'+caseLog.caseDetails.Id);
        }

        return null;
        
    }

    public PageReference cancel(){
        return new PageReference('/'+caseLog.caseDetails.Id);
    }

    public Boolean validated(){

        if(String.isBlank(caseLog.caseDetails.CaseReason__c)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Case Reason is needed to proceed'));
            return false;
        }

        if(String.isBlank(caseLog.caseDetails.CaseReasonDetail__c)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Case Reason Detail is needed to proceed'));
            return false;
        }

        if(String.isBlank(caseLog.userAction.CaseAction__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Action is needed to proceed'));
            return false;
        }

        if(String.isBlank(caseLog.userAction.CaseActionDetail__c)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Action Detail needed to proceed'));
            return false;
        }

        return true;
    }

}