public without sharing abstract class AbstractSObjectModel_CS extends AbstractModel_CS {

	public sObject record {get;set;}
	public Boolean updateError {get;set;}
	
	public AbstractSObjectModel_CS(sObject s){
		super();
		if(s == null){
			throw new ApplicationException('AbstractModel: The constructor sObject was null.');
		}
		
		this.record = s;
	}
	
	public static List<sObject> getSObjects(List<AbstractSObjectModel_CS> models){
		List<sObject> objects = new List<sObject>();
		
		for(AbstractSObjectModel_CS m : models){
			objects.add(m.record);
		}
		
		return objects;
	}
}