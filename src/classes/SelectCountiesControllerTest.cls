/**
 *  @Description Test class for SelectCountiesController
 *  @Author 
 *  Revision History: 
 *		01/08/2016 - karnold - Added header. Formatted code.
 *		01/11/2016 - karnold - Refactored single test method into smaller methods with proper start/stop conditions. 
 *			Fixed assert failures.
 */
@isTest
private class SelectCountiesControllerTest {
	private static User u;
	private static SelectCountiesController scc;
	private static County_Item__c ci;
	private static Integer startCount;

	private static void initTest() {
		// user profile

		Profile p = [Select id from Profile where Name = 'Integra Standard User'];
		u = new User(LastName = 'User1',
		             Alias = 'use',
		             Email = 'test@test.com',
		             Username = 'testingtrigger@testingcenter.com',
		             CommunityNickname = 'nick',
		             EmailEncodingKey = 'UTF-8',
		             LanguageLocaleKey = 'en_US',
		             LocaleSidKey = 'en_US',
		             ProfileId = p.Id,
		             TimeZoneSidKey = 'America/New_York'
		);
		insert u;

		System.runAs(u) {

			// create account, provider location

			Account a = new Account(Name = 'Test Account', Type_Of_Provider__c = 'DME');
			insert a;

			Provider_Location__c pl = new Provider_Location__c(Account__c = a.Id,
			                                                   Name_DBA__c = 'Test Account Store');
			insert pl;

			//create sample states

			Set<String> states = new Set<String> { 'Alabama', 'Alaska', 'New York', 'New Jersey' };
			List<US_State_County__c> allIns = new List<US_State_County__c> ();
			for (String s : states) {
				for (Integer i = 0; i < 3; i++) {

					String na = 'Test' + ' ' + i;
					US_State_County__c newCounty = new US_State_County__c(Name = na, State__c = s);
					allIns.add(newCounty);
				}
			}

			insert allIns;

			// add atleast one item to the PL

			US_State_County__c us = [Select Id, Name from US_State_County__c where State__c = 'Alabama' AND Name = 'Test 0'];

			ci = new County_Item__c(Provider_Location_New__c = pl.Id, US_State_County__c = us.Id);
			insert ci;

			Provider_Location__c testPL = [Select id from Provider_Location__c where id = :pl.id];

			//load the page

			PageReference pageRef = Page.AddCountiesToLocation;
			pageRef.getParameters().put('Id', pl.Id);
			Test.setCurrentPageReference(pageRef);

			//load the extension

			ApexPages.StandardController sc = new ApexPages.StandardController(testPL);
			scc = new SelectCountiesController(sc);
		}
	}

	private static testMethod void confirmBasket_Test() {
		// GIVEN
		initTest();

		// WHEN
		Test.startTest();
		startCount = scc.shoppingCart.size();
		Test.stopTest();

		// THEN
		System.assert(startCount > 0);
	}


	private static testMethod void searchFunction_Test() {
		// GIVEN
		initTest();

		//test for search functionality
		scc.searchString = 'user check';

		// WHEN
		Test.startTest();
		scc.updateAvailableCounties();
		Test.stopTest();

		// THEN
		System.assertEquals(0, scc.availableCounties.size());
	}

	private static testMethod void addAllFunction_Test() {
		// GIVEN
		initTest();

		//test for search functionality
		scc.selectedState = 'Alaska';
		// WHEN
		Test.startTest();
		scc.selectAllCountiesStates();
		Test.stopTest();

		// THEN
		System.assertEquals(0, scc.availableCounties.size());
	}


	private static testMethod void removeFromCart_Test() {
		// GIVEN
		initTest();
		startCount = scc.shoppingCart.size();

		scc.searchString = 'user check';
		scc.updateAvailableCounties();
		scc.toUnselect = ci.US_State_County__c;

		// WHEN
		Test.startTest();
		scc.removeFromCart();
		Test.stopTest();

		// THEN
		System.assertEquals(startCount - 1, scc.shoppingCart.size());
	}


	private static testMethod void saveAndReload_Test() {
		// GIVEN
		initTest();
		startCount = scc.shoppingCart.size();

		scc.searchString = 'user check';
		scc.updateAvailableCounties();
		scc.toUnselect = ci.US_State_County__c;
		scc.removeFromCart();

		scc.onSave();
		Provider_Location__c testPL1 = [SELECT Id FROM Provider_Location__c WHERE Id = :ci.Provider_Location_New__c LIMIT 1];
		ApexPages.StandardController sc1 = new ApexPages.StandardController(testPL1);

		// WHEN
		Test.startTest();
		scc = new SelectCountiesController(sc1);
		Test.stopTest();

		// THEN
		System.assertEquals(startCount - 1, scc.shoppingCart.size());
	}


	private static testMethod void searchAfterReload_Test() {
		// GIVEN
		initTest();
		startCount = scc.shoppingCart.size();

		scc.searchString = 'user check';
		scc.updateAvailableCounties();
		scc.toUnselect = ci.US_State_County__c;
		scc.removeFromCart();

		scc.onSave();

		Provider_Location__c testPL1 = [SELECT Id FROM Provider_Location__c WHERE Id = :ci.Provider_Location_New__c LIMIT 1];
		ApexPages.StandardController sc1 = new ApexPages.StandardController(testPL1);
		scc = new SelectCountiesController(sc1);
		scc.searchString = ci.County_Name__c;

		// WHEN
		Test.startTest();
		scc.updateAvailableCounties();
		Test.stopTest();

		// THEN
		System.assert(scc.availableCounties.size() > 0);
	}

	private static testMethod void addToCartAfterReload_Test() {
		// GIVEN
		initTest();
		startCount = scc.shoppingCart.size();

		scc.searchString = 'user check';
		scc.updateAvailableCounties();
		scc.toUnselect = ci.US_State_County__c;
		scc.removeFromCart();

		scc.onSave();

		Provider_Location__c testPL1 = [SELECT Id FROM Provider_Location__c WHERE Id = :ci.Provider_Location_New__c LIMIT 1];
		ApexPages.StandardController sc1 = new ApexPages.StandardController(testPL1);
		scc = new SelectCountiesController(sc1);
		scc.searchString = ci.County_Name__c;
		scc.updateAvailableCounties();
		scc.toSelect = scc.availableCounties[0].Id;

		// WHEN
		Test.startTest();
		scc.addToCart();
		Test.stopTest();

		// THEN
		System.assertEquals(1, scc.shoppingCart.size());
	}

	private static testMethod void populateSelectOptions_Test() {
		// GIVEN
		initTest();
		startCount = scc.shoppingCart.size();

		scc.searchString = 'user check';
		scc.updateAvailableCounties();
		scc.toUnselect = ci.US_State_County__c;
		scc.removeFromCart();

		scc.onSave();

		Provider_Location__c testPL1 = [SELECT Id FROM Provider_Location__c WHERE Id = :ci.Provider_Location_New__c LIMIT 1];
		ApexPages.StandardController sc1 = new ApexPages.StandardController(testPL1);
		scc = new SelectCountiesController(sc1);

		scc.searchString = ci.County_Name__c;
		scc.updateAvailableCounties();
		scc.toSelect = scc.availableCounties[0].Id;
		scc.addToCart();

		scc.onSave();

		County_Item__c[] ci2 = [Select Id from County_Item__c where Provider_Location_New__c = :ci.Provider_Location_New__c];
		System.assert(ci2.size() == startCount);
		scc.selectedState = 'Alaska';

		// WHEN
		Test.startTest();
		scc.updateAvailableCounties();
		Test.stopTest();

		// THEN
		System.assertEquals(3, scc.availableCounties.size());
	}
}