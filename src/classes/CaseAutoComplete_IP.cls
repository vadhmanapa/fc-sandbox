/*
*    Source Code : AutoCompleteV2_Con
**/

public with sharing class CaseAutoComplete_IP {
	public String labelFieldVar{ get; set; }
    public String valueFieldVar{ get; set; }
    public String sObjVal{get;set;}
    public Integer randomJsIden{get;set;}
    public Object cacheField{get;private set;}
    public String recordType {get;set;}
    public String accId {get;set;}
    public String patId {get;set;}
	public String searchBy {get;set;}

    private Object targetFieldVar;

    public CaseAutoComplete_IP(){
        randomJsIden = getRandomNumber(1000000);
        sObjVal='Account';
        labelFieldVar='Name';
        valueFieldVar='Id';
    }

    /*Dummy setter Method*/
    public void setCacheField(Object cacheField){}

    public void setTargetFieldVar(Object targetFieldVar){

        if(targetFieldVar != this.targetFieldVar){
            cacheField = getCacheFieldValue(targetFieldVar);
            this.targetFieldVar = targetFieldVar;
        }

    }

    public Object getTargetFieldVar(){
        return targetFieldVar;
    }

    private Object getCacheFieldValue(Object targetFieldVar){
        Object retVal = targetFieldVar;
        if(targetFieldVar!=null){
            for(sObject sObj : Database.query('SELECT '+valueFieldVar+','+labelFieldVar+' FROM '+sObjVal+' WHERE '+valueFieldVar+' =:targetFieldVar')){
                retVal = sObj.get(labelFieldVar);
                break;
            }
        }

        return retVal;
    }


    /*
    *Random number generator to change the js function name if multiple components us
    ***/
    private Integer getRandomNumber(Integer size){
        Double d = Math.random() * size;
        return d.intValue();
    }

    /*
    *This method queries data according to the passed parameters
    ***/
    @RemoteAction
    public static List<AutoCompleteData> getData(String sObjVal,String labelFieldVar,String valueFieldVar,String param, String recordType, String accId, String patId, String searchBy){

        List<AutoCompleteData> AutoCompleteDatas = new List<AutoCompleteData>();
        param = String.escapeSingleQuotes(param);
        for( Sobject sObj : Database.query(formatQuery(sObjVal,labelFieldVar,valueFieldVar,param, recordType, accId, patId, searchBy))){
            // 'SELECT '+valueFieldVar+','+labelFieldVar+' FROM '+sObjVal+' WHERE '+labelFieldVar+' LIKE \'%'+param+'%\''
            if(sObjVal == 'Account') {
                AutoCompleteDatas.add(new AutoCompleteData(sObj.get(valueFieldVar),sObj.get(labelFieldVar),sObj.get('Corporate_Address_Text__c'), sObj.get('Phone')));
                // AutoCompleteDatas.add(new AutoCompleteData(sObj.get(valueFieldVar),sObj.get(labelFieldVar),sObj.get('Type__c'), sObj.get('Status__c')));
            }

            if(sObjVal == 'Contact') {
                AutoCompleteDatas.add(new AutoCompleteData(sObj.get(valueFieldVar),sObj.get(labelFieldVar),sObj.get('Status__c'), sObj.get('Birthdate'), sObj.get('Health_Plan_ID__c'), sObj.get('Phone'), recordType));
            }
        }

        return AutoCompleteDatas;

    }

    String s = 'WHERE Status__c IN (\'Active\', \'Active/Pending Training\')';

    public static String formatQuery(String sObjVal,String labelFieldVar,String valueFieldVar,String param, String recordType, String accId, String patId, String searchBy){

        Set<String> queryValues;
        AndCondition qCoditions = new AndCondition();

        if(sObjVal == 'Account') {

            queryValues = new Set<String>{labelFieldVar,valueFieldVar,'Type__c', 'Status__c', 'Corporate_Address_Text__c', 'Phone', 'Legal_Name__c'};

            qCoditions.add(new OrCondition()
                            .add(new FieldCondition('Status__c').equals('Active'))
                            .add(new FieldCondition('Status__c').equals('Active/Pending Training'))
                        );

            if(String.isNotBlank(recordType) && (recordType == 'Payor' || recordType == 'Family' || recordType == 'Patient')) {

                qCoditions.add( new AndCondition()
                                .add(new FieldCondition('Type__c').equals('Payer'))
                                .add(new FieldCondition(labelFieldVar).likex(param))
                            );
            }

            if(String.isNotBlank(recordType) && (recordType == 'Provider')) {

                /***
                ** Modified to search by name and Tax ID on all instances
                ***/
				qCoditions.add(
                    new AndCondition()
                       .add(new FieldCondition('Type__c').equals('Provider'))
                       .add(new OrCondition()
                           .add(new FieldCondition('RecordType.Name').equals('Integra Provider'))
                           .add(new FieldCondition('RecordType.Name').equals('DMEnsion and Integra Provider'))
                        )
                        .add(new OrCondition()
                            .add(new FieldCondition('Tax_ID__c').likex(param))
                            .add(new FieldCondition('AccountName__c').likex(param))
                            .add(new FieldCondition('Legal_Name__c').likex(param))
                            .add(new FieldCondition('Fax_Text__c').likex(param))
                            .add(new FieldCondition('Phone_Text__c').likex(param))
                        )
                );

                /*if(searchBy == 'TaxID'){
					qCoditions.add(
                        new AndCondition()
	                       .add(new FieldCondition('Type__c').equals('Provider'))
	                       .add(new FieldCondition('Tax_ID__c').likex(param))
	                       .add(new OrCondition()
	                           .add(new FieldCondition('RecordType.Name').equals('Integra Provider'))
	                           .add(new FieldCondition('RecordType.Name').equals('DMEnsion and Integra Provider'))
	                        )
                            .add(new OrCondition()
                                .add(new FieldCondition('Tax_ID__c').likex(param))
                                .add(new FieldCondition('AccountName__c').likex(param))
                            )
	                );
				}

				if(searchBy == 'Account_Name'){
					qCoditions.add(new AndCondition()
	                                .add(new FieldCondition('Type__c').equals('Provider'))
	                                .add(new FieldCondition('AccountName__c').likex(param))
	                                .add(new OrCondition()
	                                    .add(new FieldCondition('RecordType.Name').equals('Integra Provider'))
	                                    .add(new FieldCondition('RecordType.Name').equals('DMEnsion and Integra Provider'))
	                                )
	                            );
				}*/
            }
        }

        if(sObjVal == 'Contact') {

            queryValues = new Set<String>{labelFieldVar,valueFieldVar, 'Status__c', 'Birthdate', 'Health_Plan_ID__c', 'Phone', 'Email'};

            qCoditions.add(new FieldCondition('RecordType.Name').equals(recordType));

            if(accId != null && String.isNotBlank(accId)) {
                qCoditions.add(new FieldCondition('AccountId').equals((Id)accId));
            }

            if(recordType == 'Family') {
                qCoditions.add(new FieldCondition('RelatedPatient__c').equals((Id)patId));
            }

            if(recordType == 'Patient') {
                // qCoditions.add(new FieldCondition('Health_Plan_ID__c').likex(param));
                qCoditions.add( new OrCondition()
                    .add(new FieldCondition('Health_Plan_ID__c').likex(param))
                    .add(new FieldCondition(labelFieldVar).likex(param))
                );
            }else {
                qCoditions.add(new FieldCondition(labelFieldVar).likex(param));
            }
        }

        String query = new SoqlBuilder()
                           .selectx(queryValues)
                           .fromx(sObjVal)
                           .orderByx(new List<Orderby>{new OrderBy(labelFieldVar).ascending().nullsLast()})
                           .limitx(10)
                           .wherex(qCoditions)
                           .toSoql(new SoqlOptions().wildcardStringsInLikeOperators());

        return query;
    }

    public class AutoCompleteData{
        public String id;
        public String text;
        public String additionalText;

        public AutoCompleteData(Object id, Object text, Object accAddress, Object accPhone){
            this.id = String.valueOf(id);
            this.additionalText = 'Address: '+ String.valueOf(accAddress)+ ' || '+ 'Phone : '+ String.valueOf(accPhone);
            this.text = ((String.valueOf(text))+ ';' +additionalText);
        }

        public AutoCompleteData(Object id, Object text, Object conStatus, Object bDate, Object planId, Object phone, String callType){

            this.id = String.valueOf(id);

            if(String.isNotBlank(callType) && callType != 'Provider' && callType != 'Patient'){
                this.additionalText = 'Phone: ' + String.valueOf(phone);
            }

            if(String.isNotBlank(callType) && callType == 'Patient') {
                if(String.isNotBlank(String.valueOf(planId))){
                    this.additionalText = 'Patient ID: '+ String.valueOf(String.valueOf(planId));
                }

                if(bDate != null){
                    Date dt = (Date)bDate;
                    this.additionalText = additionalText + ' || '+ ' DOB: '+ dt.format();
                }
            }

            if(String.isNotBlank(callType) && callType == 'Provider') {
                this.additionalText = 'Status: ' + String.valueOf(conStatus) + ' || ' + ' Phone: ' + String.valueOf(phone);
            }

            this.text = ((String.valueOf(text))+ ';' +additionalText); // use ; as de-limiter
        }
    }
}