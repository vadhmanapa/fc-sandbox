@isTest
private class PublicKnowledgeVideoRedirectTest {

	static testMethod void testVideoRedirect(){
		
		Test.setMock(HttpCalloutMock.class, new PublicKnowledgeCalloutMock());
		
		PageReference result;
		PageReference pr = Page.PublicKnowledgeVideoRedirect;
		Test.setCurrentPage(pr);
		
		//Incomplete parameters
		PublicKnowledgeVideoRedirectController ctrl = new PublicKnowledgeVideoRedirectController();
		result = ctrl.redirectToVideo();
		TestUtils.assertErrorMessagesExist();
		System.assertEquals(null, result);
		
		//No custom setting
		pr.getParameters().put('course', '123456');
		Test.setCurrentPage(pr);
		result = ctrl.redirectToVideo();
		System.assertEquals(null, result);
		
		//Failed callout
		Litmos_Settings__c setting = new Litmos_Settings__c();
		setting.Name = 'Defaults';
		insert setting;
		Test.startTest();
		result = ctrl.redirectToVideo();
		System.assertEquals(null, result);
		
		//Successfull callout
		PublicKnowledgeCalloutMock.success = true;
		result = ctrl.redirectToVideo();
		System.assertEquals('www.test.com&titlebar=false&c=123456', result.getUrl());
		Test.stopTest();
	}

}