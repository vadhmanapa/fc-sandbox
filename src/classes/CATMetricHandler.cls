public with sharing class CATMetricHandler {

	public class MyException extends Exception{}	

	public Map<Id, String> rTypeMap = new Map<Id, String>();

	public CATMetricHandler() {
		
		for(RecordType rt: [Select Id, Name from RecordType where sObjectType = 'CAT_Metric__c']) {
			rTypeMap.put(rt.Id, rt.Name);
		}
		System.debug('Number of record type queried'+rTypeMap.size());
	}
	
	public Time getElapsedTime(Time startTime, Time endTime)
	{
	    if(startTime == null || endTime == null)
	        return Time.newInstance(0, 0, 0, 0);

	    Integer elapsedHours = endTime.hour() - startTime.hour();
	    Integer elapsedMinutes = endTime.minute() - startTime.minute();
	    Integer elapsedSeconds = endTime.second() - startTime.second();
	    Integer elapsedMiliseconds = endTime.millisecond() - startTime.millisecond();

	    return Time.newInstance(elapsedHours, elapsedMinutes, elapsedSeconds, elapsedMiliseconds);
	}

	public Decimal calculateTAT(BusinessHours bh, Datetime startTime, Datetime endTime, String units){
		Decimal h = 1.0;
		if (units == 'BD'){
			h = (bh.MondayEndTime.hour()+bh.MondayEndTime.minute()/60) - (bh.MondayStartTime.hour()+bh.MondayStartTime.minute()/60);	
		}

		Decimal d = 0.0;

		if (startTime.date().isSameDay(endTime.date()) == True){
			Time t = getElapsedTime(startTime.Time(), endTime.Time()); 
			d = (t.hour() + t.minute()/60.0)/h;
		}
		else{
			d = BusinessHours.diff(bh.id, startTime, endTime)/1000/60/60.0/h;
		}
		return d;
	}

	public void runTrigger(List<CAT_Metric__c> triggerValues, Map<Id, CAT_Metric__c> triggerOldMap){
		BusinessHours bh = [SELECT Id, MondayStartTime, MondayEndTime FROM BusinessHours WHERE Name='Credentialing'];
		List<CAT_Metric__c> catmToInsert = new List<CAT_Metric__c>();
		for(CAT_Metric__c catm: triggerValues){
			
			CAT_Metric__c oldCATM = triggerOldMap.get(catm.Id);
			if (catm.Hold_TS__c == null && catm.On_Hold__c == True){
				// First time this CAT goes on hold
				catm.Hold_TS__c = catm.Start_Time__c;
			}

			if (catm.On_Hold__c == True && oldCATM.On_Hold__c == False){
				//Went on hold
				if (catm.Active_Time__c == null) {catm.Active_Time__c = 0;}
				catm.Active_Time__c += calculateTAT(bh, catm.Hold_TS__c, Datetime.now(), 'BH');
				catm.Hold_TS__c = Datetime.now();
			}
			else if (catm.On_Hold__c == False && oldCATM.On_Hold__c == True){
				//Went off hold
				if (catm.Hold_Time__c == null) {catm.Hold_Time__c = 0;}
				catm.Hold_Time__c += calculateTAT(bh, catm.Hold_TS__c, Datetime.now(), 'BH');
				catm.Hold_TS__c = Datetime.now();
			}
			else if (catm.End_Time__c != null && oldCATM.End_Time__c == null && catm.Hold_TS__c != null){
				catm.Active_Time__c += calculateTAT(bh, catm.Hold_TS__c, catm.End_Time__c, 'BH');
				catm.Hold_TS__c = catm.End_Time__c;
			}

			if (catm.End_Time__c != null && catm.Hold_TS__c == null && catm.TAT__c == null){
				catm.TAT__c = calculateTAT(bh, catm.Start_Time__c, catm.End_Time__c, 'BH');
			}
			else if (catm.End_Time__c != null && catm.Hold_TS__c != null && catm.TAT__c == null){
				catm.TAT__c = catm.Active_Time__c;
				catmToInsert.add(new CAT_Metric__c(CAT__c = catm.CAT__c, 
													TAT__c = catm.Hold_Time__c,
													Start_Time__c = catm.Start_Time__c,
													End_Time__c = catm.End_Time__c,
													Completed_By__c = catm.Completed_By__c,
													Metric_Type__c = 'Hold',
													TAT_Type__c = 'Hold'));
			}

		}
		insert catmToInsert;
	}
}

//if (catm.Start_Time__c != null && catm.End_Time__c != null){
//					catm.TAT__c = calculateTAT(bh, catm.Start_Time__c, catm.End_Time__c);
//				}