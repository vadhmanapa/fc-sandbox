public without sharing class ProviderAccountProfile_CS extends ProviderPortalServices_CS {

	private final String pageAccountType = 'Provider';

	private AccountServices_CS accServices;
	
	/********* Page Interface *********/
	public Account thisAccount {get;set;}
	public Contact superUser {get;set;}
	
	/********* Constructor/Init *********/
	public ProviderAccountProfile_CS(){
		
	}

	public PageReference init(){

		///////////////////////////////////
		// Authentication and Permission
		///////////////////////////////////
		
		if(!isSecure && !Test.isRunningTest()){
			return DoLogout();
		}
				
		if(!authenticated){
			return DoLogout();
		}

		if(portalUser.AccountType != pageAccountType){
			return homepage;
		}
		
		///////////////////////////////////
		// Additional init
		///////////////////////////////////
		
		accServices = new AccountServices_CS(portalUser.instance.Id);		
		thisAccount = accServices.cAccount;
		
		//TODO: Move into a service class
		if(thisAccount.Super_User__c != null){
			superUser = [
				select
					Id,
					FirstName,
					LastName,
					Email
				from Contact
				where Id = :thisAccount.Super_User__c
			];
		}

		return null;
	}
}