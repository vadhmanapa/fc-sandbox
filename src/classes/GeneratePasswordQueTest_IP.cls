@isTest
private class GeneratePasswordQueTest_IP {
	
	@isTest static void testPasswordGen() {
		// Implement test code
		Profile p = [Select id from Profile where Name ='Integra Standard Platform User'];
		User u = new User(LastName='UserQue', 
						  Alias='use',
						  Email='test@test.com',
						  Username='testingtrigger@testingcenter.com',
						  CommunityNickname='nick',
						  EmailEncodingKey='UTF-8',
						  LanguageLocaleKey='en_US',
						  LocaleSidKey='en_US',
						  ProfileId = p.Id,
						  TimeZoneSidKey='America/New_York');
		insert u;

		System.runAs(u){

			Account testAcc = new Account(Name='Test Account', 
						        Type_Of_Provider__c = 'DME');
			insert testAcc;

			List<Contact> newInserts = new List<Contact>();

			for(Integer i = 0; i < 10; i++) {
				
				newInserts.add(new Contact(LastName='User'+'i', FirstName='Test', AccountId = testAcc.Id, Que_Admin__c = false, Que_Password__c = ''));
			}

			insert newInserts;

			List<Contact> updateContacts = [Select Id, FirstName, LastName, Que_Admin__c, Que_Password__c, Que_Unique_Login_Id__c from Contact where FirstName= 'Test'];
			if(updateContacts.size() > 0){

				for(Contact c: updateContacts) {
					c.Que_Admin__c = true;
				}
			}

			update updateContacts;

			List<Contact> testUpdateCon = [Select Id, FirstName, LastName, Que_Admin__c, Que_Password__c, Que_Unique_Login_Id__c from Contact where FirstName= 'Test'];

			for(Contact con: testUpdateCon) {
				
				System.assertEquals(true, con.Que_Admin__c, 'The Que Admin checkbox was checked.Safe');
				System.assertNotEquals(null, con.Que_Password__c, 'The password was generated and populated. Safe');
				System.assertNotEquals(null, con.Que_Unique_Login_Id__c, 'The username was generated. Safe');
			}

			Contact testCon = new Contact(LastName='UserQue', FirstName='Test', AccountId = testAcc.Id, Que_Password__c = 'test200');
			insert testCon;

			Contact testCon2 = [Select Id, Name, Que_Password__c, Que_Admin__c from Contact where Id=:testCon.Id];
			testCon2.Que_Admin__c = true;

			update testCon2;

			System.assertEquals('test200', testCon2.Que_Password__c, 'The trigger did not update the field. Safe');

			// test for last name only

			Contact testUsername = new Contact(LastName='LastNameOnly', FirstName='', AccountId = testAcc.Id);
			insert testUsername;

			Contact testCheckUsername = [Select Id, Name, Que_Password__c, Que_Admin__c, Que_Unique_Login_Id__c from Contact where Id=:testUsername.Id];
			testCheckUsername.Que_Admin__c = true;

			update testCheckUsername;

			Contact testCheckUsername2 = [Select Id, Name, Que_Password__c, Que_Admin__c, Que_Unique_Login_Id__c from Contact where Id=:testUsername.Id];
			System.assertEquals('LastNameOnly', testCheckUsername2.Que_Unique_Login_Id__c, 'The username is LastNameOnly. Safe');

		} // end of System.runAs()
	}
	
}