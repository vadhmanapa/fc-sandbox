/**
 *  @Description Geocoding Utilities for Provider Location object.
 *  @Author Cloud Software LLC
 *  Revision History: 
 *		01/11/2016- karnold - Formatted code. Removed header. Removed commented code.
 *
 */
public without sharing class ProviderLocationServices_CS {

	public static Set<Id> addressesChanged(Map<Id,Provider_Location__c> oldMap, Map<Id,Provider_Location__c> newMap){
		if(oldMap != newMap){
			Set<Id> providerLocationIdsSet = new Set<Id>();
			for(Id key :newMap.KeySet()) {
				Provider_Location__c newProviderLocation = newMap.get(key);
				Provider_Location__c oldProviderLocation = oldMap.get(key);

				if(oldProviderLocation.Location_Street__c != newProviderLocation.Location_Street__c ||
						oldProviderLocation.Location_City__c != newProviderLocation.Location_City__c ||
						oldProviderLocation.Location_State__c != newProviderLocation.Location_State__c ||
						oldProviderLocation.Location_Zip_Code__c != newProviderLocation.Location_Zip_Code__c) {

					providerLocationIdsSet.add(key);
				}
			}
			return providerLocationIdsSet;
		}
		else{
			return newMap.keySet();
		}
	}

	@future(callout=true)
	public static void geocodeProviderLocationIds(Set<Id> providerLocationIdSet) {
		List<Provider_Location__c> providerLocationList = [select Id, Location_Street__c, Location_City__c, Location_State__c, Location_Zip_Code__c FROM Provider_Location__c WHERE Id in:providerLocationIdSet];
		geocodeProviderLocations(providerLocationList);
		
		update providerLocationList;
	}

	public static List<Provider_Location__c> geocodeProviderLocations(List<Provider_Location__c> providerLocationList) {
		List<GeocodingModel_CS> geocodingModelList = new List<GeocodingModel_CS>();
		for(Provider_Location__c pl : providerLocationList){
			GeocodingModel_CS geocodingModel = new GeocodingModel_CS();
			geocodingModel.street = pl.Location_Street__c;
			geocodingModel.city = pl.Location_City__c;
			geocodingModel.state = pl.Location_State__c;
			geocodingModel.postalCode = pl.Location_Zip_Code__c;
			geocodingModelList.add(geocodingModel);
		}
		GeocodingUtils_CS.geocodeAddresses(geocodingModelList);
		for(Integer i = 0; i < providerLocationList.size(); i++){
			providerLocationList.get(i).MALatitude__c = geocodingModelList.get(i).lat;
			providerLocationList.get(i).MALongitude__c = geocodingModelList.get(i).lng;
		}
		System.debug('update provider location list: ' + providerLocationList);

		return providerLocationList;
	}
}