public with sharing class SelectCountiesController {

    // initialize the primary variables

    public Provider_Location__c theLoc {get;set;} // get the provider location from which it is launched
    public String searchString {get; set;} // get the search value from user
    public County_Item__c[] shoppingCart {get; set;} // to hold already selected products
    public US_State_County__c[] availableCounties {get; set;}
    public US_State_County__c[] addedCounties {get;set;}
    public Map<Id, County_Item__c> mapCounties = new Map<Id, County_Item__c>();
    public Boolean selectAllCounties {get; set;} // when selected, include all counties for that state
    public Boolean selectAllStates {get; set;}

    public String toSelect{get; set;} // to pass value from command button to controller
    public String toUnselect {get; set;}
    public Boolean pageLimit {get; set;}// to control the page size
    public String selectedState {get;set;} // get the value from state picklist
    public Integer countiesReturned {get; set;} // to show on stop text

    public County_Item__c[] forDeletion = new County_Item__c[]{}; // to hold deletion values
    public String[] allStates = new String[]{'Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut',
            'Delaware','District Of Columbia','Florida','Georgia','Hawaii','Idaho','Illinois',
            'Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts',
            'Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada',
            'New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota',
            'Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota',
            'Tennessee','Texas','Utah','Vermont','Virginia','Washington','West Virginia',
            'Wisconsin','Wyoming'}; // container for 50 states

    public SelectCountiesController(ApexPages.StandardController controller) {

        // find the provider location

        //Add 
        theLoc = [Select Id, Name_DBA__c, All_States_and_Counties__c, Name from Provider_Location__c where Id =:controller.getRecord().Id LIMIT 1];

        System.debug('The provider location is'+theLoc.Name);

        // get the products already selected

        shoppingCart = [Select Id,US_State_County__c,County_Name__c, State__c from County_Item__c where Provider_Location_New__c =: theLoc.Id];
        for(County_Item__c c : shoppingCart){
            mapCounties.put(c.US_State_County__c, c);
        }

        // initialize a state value

        selectedState = 'New York';
        //selectAllCounties = false;
    }

    public List<SelectOption> getStates(){

        List<SelectOption> options = new List<SelectOption>();
        for(String s : allStates){
            options.add(new SelectOption(s, s));
        }
        return options;
    }

    public void selectAllCountiesStates(){

        selectAllCounties = true; // making this true, include all county values
        addToCart();
    }

    /*public void selectAllStates(){

        theLoc.All_States_and_Counties__c = selectAllStates;
    }*/

    public void updateAvailableCounties(){

        if(theLoc.All_States_and_Counties__c != true) {

            countiesReturned = 0; // everytime called should set to zero
            // build a dynamic query and exclude items already on shopping cart
            String qString = 'Select Id, Name, State__c from US_State_County__c WHERE State__c ='+ '\'' + selectedState + '\''+' '; // to show all the available counties in all 50 states

            // when a search statement is included, add that to the query
            if(searchString != null){

                qString+= 'AND (Name LIKE \'%' + searchString + '%\')'+' ';
            }

            //remove the id's in shopping cart

            Set<Id> selectedEntries = new Set<Id>();

            for(County_Item__c c : shoppingCart){

                selectedEntries.add(c.US_State_County__c);
            }

            String tempFilter;

            if(selectedEntries.size()>0){

                tempFilter = 'AND Id NOT IN (';


                for(Id i : selectedEntries){

                    tempFilter+= '\'' + (String)i + '\',';
                }

                String extraFilter = tempFilter.substring(0, tempFilter.length()-1);
                extraFilter+= ')';

                qString+= extraFilter;
            }

            //final ordering of the query

            qString+= 'ORDER BY Name'+ ' ';
            qString+= 'ASC'+ ' '+'LIMIT 101';

            System.debug('Final Query String on Runtime'+qString);
            availableCounties = Database.query(qString);
            countiesReturned = availableCounties.size();
            System.debug('Number of available counties'+availableCounties.size());

            // limit the number of counties to 100.. display message if more than 100

            if(availableCounties.size() == 101){
                availableCounties.remove(100);
                pageLimit = true;
            } else {
                pageLimit = false;
            }

        } else{
            availableCounties = null;
        }

    }

    public void addToCart(){

        if(selectAllCounties == true){
            String allCounties = 'Select Id, Name, State__c from US_State_County__c WHERE State__c ='+ '\'' + selectedState + '\''+' ';

            Set<Id> selectedEntries = new Set<Id>();

            for(County_Item__c c : shoppingCart){

                selectedEntries.add(c.US_State_County__c);
            }

            String tempFilter;

            if(selectedEntries.size()>0){

                tempFilter = 'AND Id NOT IN (';


                for(Id i : selectedEntries){

                    tempFilter+= '\'' + (String)i + '\',';
                }

                String extraFilter = tempFilter.substring(0, tempFilter.length()-1);
                extraFilter+= ')';

                allCounties+= extraFilter;
            }

            //final ordering of the query

            allCounties+= 'ORDER BY Name';

            availableCounties = Database.query(allCounties);

            if(availableCounties != null){

                for(US_State_County__c u : availableCounties){
                    System.debug('The County selected is'+u.Name);
                    shoppingCart.add(new County_Item__c(Provider_Location_New__c = theLoc.Id, US_State_County__c = u.Id, County_Name__c = u.Name, State__c = u.State__c));
                }
            }
        } else {

            System.debug('Entering Add to Cart function');
            System.debug('The toSelect parameter is:'+toSelect);

            for(US_State_County__c u : availableCounties){

                if((String)u.Id==toSelect){
                    System.debug('The County selected is'+u.Name);
                    System.debug('The toSelect parameter is:'+toSelect);
                    shoppingCart.add(new County_Item__c(Provider_Location_New__c = theLoc.Id, US_State_County__c = u.Id, County_Name__c = u.Name, State__c = u.State__c));
                    break;
                }
            }
        }
        // This function runs when select button is pressed

        selectAllCounties = false;
        updateAvailableCounties();

    }

    public PageReference removeFromCart(){

        // This function is executed which Remove button is selected

        Integer count = 0;

        for(County_Item__c c : shoppingCart){

            if((String)c.US_State_County__c==toUnselect){

                if(c.Id != null){

                    forDeletion.add(c);
                    shoppingCart.remove(count);
                    break;
                } else {

                    //forDeletion.add(c);
                    shoppingCart.remove(count);
                    break;
                }

            }

            count++;
            updateAvailableCounties();

        }
        return null;
    }

    public PageReference onSave(){

        // Delete the prviously selected products

        if(forDeletion.size() > 0){
            delete(forDeletion);
        }

        // Use upsert command

        try {

            if(shoppingCart.size() > 0){
                upsert(shoppingCart);
            }
        } catch(Exception e) {
            ApexPages.addMessages(e);
            return null;
        }

        update theLoc;


        return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
    }

    public PageReference onCancel(){

        // If user hits cancel we commit no changes and return them to the Opportunity
        return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
    }
}