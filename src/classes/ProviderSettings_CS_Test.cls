/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ProviderSettings_CS_Test {

	/******* Provider *******/
	static private List<Account> providers;
	static private List<Contact> providerUsers;
	
	/******* Test Objects *******/
	static private ProviderSettings_CS settings;

	/******* Test Methods *******/
	
    static testMethod void ProviderSettings_Test_PageLanding(){
    	init();
    }
  	
  	static testMethod void ProviderSettings_Test_ToggleNotifications(){
  		init();
  		
  		Contact c;
  		
  		Boolean oldNewOrderNotification = settings.portalUser.instance.Notify_New_Order__c;  		
  		Boolean oldOrderCanceledNotification = settings.portalUser.instance.Notify_Cancelled__c;  		
  		Boolean oldNeedsAttentionNotification = settings.portalUser.instance.Notify_Needs_Attention__c;  		
  		Boolean oldPasswordChangeNotification = settings.portalUser.instance.Notify_Password_Change__c;
  	
  		settings.toggleNewOrderNotification();
  		settings.toggleCanceledNotification();
  		settings.toggleNeedsAttentionNotification();
  		settings.togglePasswordChangeNotification();
  		
  		//TODO: Finish these tests like the others
  		settings.togglePayerCanceledNotification();
  		settings.toggleRejectedNotification();
  		settings.toggleShippedNotification();
  		settings.toggleDeliveredNotification();
  		settings.toggleAcceptedNotification();
  		
  		c = [select
  				Id,
  				Notify_New_Order__c,
  				Notify_Cancelled__c,
  				Notify_Needs_Attention__c,
  				Notify_Password_Change__c
  			from Contact
  			where Id = :settings.portalUser.instance.Id];
  		
  		System.assert(c.Notify_New_Order__c != oldNewOrderNotification,'');
  		System.assert(c.Notify_Cancelled__c != oldOrderCanceledNotification,'');
  		System.assert(c.Notify_Needs_Attention__c != oldNeedsAttentionNotification,'');
  		System.assert(c.Notify_Password_Change__c != oldPasswordChangeNotification,'');
  		
  		settings.toggleNewOrderNotification();
  		settings.toggleCanceledNotification();
  		settings.toggleNeedsAttentionNotification();
  		settings.togglePasswordChangeNotification();
  		
  		c = [select
  				Id,
  				Notify_New_Order__c,
  				Notify_Cancelled__c,
  				Notify_Needs_Attention__c,
  				Notify_Password_Change__c
  			from Contact
  			where Id = :settings.portalUser.instance.Id];
  		
  		System.assert(c.Notify_New_Order__c == oldNewOrderNotification,'');
  		System.assert(c.Notify_Cancelled__c == oldOrderCanceledNotification,'');
  		System.assert(c.Notify_Needs_Attention__c == oldNeedsAttentionNotification,'');
  		System.assert(c.Notify_Password_Change__c == oldPasswordChangeNotification,'');
  	}
  	
	static void init(){
    	
    	///////////////////////////////////
		// Data
		///////////////////////////////////
    	
    	//Providers
		providers = TestDataFactory_CS.generateProviders('TestProvider', 1);
		insert providers;
		
		//Provider users
		providerUsers = TestDataFactory_CS.createProviderContacts('ProviderUser', providers, 1);

		///////////////////////////////////
		// Page Landing
		///////////////////////////////////

		//Set the page and authentication cookie
		Test.setCurrentPage(Page.ProviderSettings_CS.setRedirect(true));
		
		//Log the desired user in
		TestServices_CS.login(providerUsers[0]);
    	
    	//Make provider portal services
    	settings = new ProviderSettings_CS();
    	settings.init();
    }
}