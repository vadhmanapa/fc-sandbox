public with sharing class CaseActionandDetailsController_IP extends ComponentControllerBase_IP {
    public String reasonDetailSelected {get;set;}
    public String actionSelected {get;set;}
    public String actionDetailSelected{get;set;}
    public String userComments {get;set;}
    private String userTierAccess {get;set{
       userTierAccess = String.isNotBlank(pageController.currentUser.Actions_Access__c) ? pageController.currentUser.Actions_Access__c : 'Tier 1';
    }}
    public ActionDetail__c actToUpdate;

    public CaseActionandDetailsController_IP() {
        // resetController(); 
    }

    public Case preCase {get;set{
        if(value != null){
            preCase = new Case();
            preCase = value;
            if(!isInit){
                actionValues();
                isInit = true;
            }
        }
    }}
    public Boolean isInit {get{if (isInit == null) isInit = false; return isInit;}set;}

    /*public void resetController(){
        // reasonDetailSelected = '';
        actionSelected = '';
        actionDetailSelected = '';
        userComments = '';
    }*/

    public void actionValues(){
        if(String.isNotBlank(preCase.CaseActionId__c)){
            reasonDetailSelected = preCase.CaseReasonDetailId__c;
            ActionDetail__c actOn = [SELECT Id, Action__c, ActionDetail__c FROM ActionDetail__c WHERE Id=: (Id)preCase.CaseActionId__c LIMIT 1];
            actionSelected = actOn.Action__c;
            actionDetailSelected = actOn.Id;
        }
    }

    public List<SelectOption> getActions(){
        List<SelectOption> options = new List<SelectOption>();
        options.clear();
        options.addAll(CaseReasonRelationshipHelper_IP.actionDetailsPicklist(reasonDetailSelected, userTierAccess));
        return options;
    }

    public List<SelectOption> getActionDetails(){
        List<SelectOption> options = new List<SelectOption>();
        options.clear();
        options.addAll(CaseReasonRelationshipHelper_IP.actionDetailsPicklist(reasonDetailSelected, actionSelected, userTierAccess));
        return options;
    }

    public CRR__c getActionRelatedValues(){
        if(String.isNotBlank(actionDetailSelected)) {
            return CaseReasonRelationshipHelper_IP.actionDetails.containsKey(actionDetailSelected) ? CaseReasonRelationshipHelper_IP.actionDetails.get(actionDetailSelected) : new CRR__c();
        }

        return new CRR__c();
    }

    public void resetActionDetails(){
        actionDetailSelected = '';
    }

    public void setParentControllerValues(){

        actToUpdate = new ActionDetail__c();

        if(String.isNotBlank(this.actionDetailSelected)) {
            actToUpdate = CaseReasonRelationshipHelper_IP.getActionValues((Id)this.actionDetailSelected);
        }
        
        if(actToUpdate != null){

            pageController.caseLog.userAction.Case__c = pageController.caseLog.caseDetails.Id;
            pageController.caseLog.userAction.CaseAction__c = actToUpdate.Action__c;
            pageController.caseLog.userAction.CaseActionDetail__c = actToUpdate.ActionDetail__c;
            pageController.caseLog.userAction.Case_Comment__c = userComments;
            insert pageController.caseLog.userAction;

            AssignmentRule newRule = new AssignmentRule();
            newRule = [select id from AssignmentRule where SobjectType = 'Case' and Active = true limit 1];

            //Creating the DMLOptions for "Assign using active assignment rules" checkbox
            Database.DMLOptions dmlOpts = new Database.DMLOptions();
            dmlOpts.assignmentRuleHeader.assignmentRuleId= newRule.id;

            pageController.caseLog.caseDetails.Status = actToUpdate.CaseStatustoUpdate__c;
            if(pageController.caseLog.caseDetails.Status != 'Closed') {
                pageController.caseLog.caseDetails.setOptions(dmlOpts);
            }
            
            update pageController.caseLog.caseDetails;
        }
    }
}