public class PayToForProviderLocation {
	/******************************

	*Created By: Venkat Adhmanapa
	*Purpose: To get all pay to listed under Account tied to current provider location and have a easy way to map existing pay to, if existing. Else create new.
	*Dependencies: VF page
	*Related Objects: Provider Location(New), Pay To, Account

	******************************/

	public final Provider_Location__c pLoc {get;set;}
	public final Pay_To__c newPayTo {get;set;}
	public List<PayToWrapper> payList {get;set;}
	public Boolean showNewPanel {get;set;}
	public Boolean isNewEntry {get;set;}
	public User approvalUser {get;set;}
	
	public PayToForProviderLocation(ApexPages.StandardController stdCon){
			
		this.newPayTo = (Pay_To__c)stdCon.getRecord();
		this.showNewPanel = false;
		this.isNewEntry = false;

		Id plId = (Id) ApexPages.currentPage().getParameters().get('pId');
		
		pLoc = [SELECT Id, Name_DBA__c, Account__r.Name, Account__r.Legal_Name__c, Account__r.Tax_ID__c, Pay_To__c FROM Provider_Location__c WHERE Id=: plId LIMIT 1]; // get current provider location
		System.debug('Provider Location Id'+plId);
		Account parent_Acc = new Account();
		Map<Id, Provider_Location__c> getAllPl = new Map<Id,Provider_Location__c>();

		if (pLoc != null){
			// get account from current PL and query all related PL to this account
			parent_Acc = [SELECT Id,Name,Status__c,Tax_ID__c,(SELECT Id,Name,Store_Status__c, Pay_To__c FROM Provider_Locations__r) FROM Account WHERE Id=: pLoc.Account__c];
			System.debug('Account Name'+parent_Acc.Name);
			for(Provider_Location__c pl: parent_Acc.Provider_Locations__r) {
				getAllPl.put(pl.Pay_To__c, pl);
			}
		}

		List<Pay_To__c> allPayTo = new List<Pay_To__c>();
		if (!getAllPl.isEmpty()) {
			// get all pay to's related to the list of PL's queried from Account
			allPayTo = [SELECT Id, Name, Taxpayer_Name__c, Tax_ID__c, Mailing_Address__c, Mailing_Address_2__c, City__c, State__c, Zip__c,Masys_Legacy__c
						FROM Pay_To__c WHERE ID IN:getAllPl.KeySet()];
			//(SELECT Id, Name FROM Provider_Locations_New__r WHERE ID IN:getAllPl.KeySet())
		}

		if (!allPayTo.isEmpty()) {
			this.payList = new List<PayToWrapper>();
			for(Pay_To__c p: allPayTo) {
				payList.add(new PayToWrapper(p));
			}				
		}else {
			ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'No Pay To found. Try adding new Pay To'));
		}

		approvalUser = [SELECT Id, Name from User WHERE Name = 'Taryn Black'];										
	}

	public class PayToWrapper {
		public Pay_To__c payDetails {get;set;}
		public Boolean isChecked {get;set;}

		public PayToWrapper(Pay_To__c payDetails){
			this.payDetails = payDetails;
			this.isChecked = false;
		}
	}

	public void AddNewPay(){
		showNewPanel = true;
		isNewEntry = true;

		// to prevent double entry
		if (payList != null) {
			for(PayToWrapper p: payList) {
				if (p.isChecked) {
					p.isChecked = false;
				}		
			}
		}
	}

	public PageReference save(){
		try {
			if (isNewEntry) {
				if (newPayTo.Use_Store_Name__c == true) {
					newPayTo.Taxpayer_Name__c = pLoc.Name_DBA__c;
					newPayTo.Taxpayer_Name_Holder__c = pLoc.Account__r.Legal_Name__c;	
				}else {
					newPayTo.Taxpayer_Name__c = pLoc.Account__r.Legal_Name__c;
				}

				newPayTo.Tax_ID__c = pLoc.Account__r.Tax_ID__c;
					
				Database.SaveResult sr = Database.insert(newPayTo,false);

				if (sr.isSuccess()) {
					pLoc.Pay_To__c = sr.getId();

					if (newPayTo.Use_Store_Name__c == true) {
						//start approval process
       
				        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
				        req1.setComments('Submitting request for approval.');
				        req1.setObjectId(sr.getId());
				        
				        // Submit on behalf of a specific submitter
				        req1.setSubmitterId(approvalUser.Id); 
				        
				        // Submit the record to specific process and skip the criteria evaluation
				        req1.setProcessDefinitionNameOrId('Use_Store_Name');
				        req1.setSkipEntryCriteria(true);
				        
				        // Submit the approval request for the account
				        Approval.ProcessResult result = Approval.process(req1);
				        
				        // Verify the result
				        System.assert(result.isSuccess());
					}			
				}
			}else {
				for(PayToWrapper p: payList) {
					if (p.isChecked) {
						pLoc.Pay_To__c = p.payDetails.Id;
					}		
				}
			}
			update pLoc;

			return new PageReference('/'+pLoc.Id);		
		} catch(Exception e) {
			System.debug(e.getMessage());
			ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, e.getMessage()));
			return null;
		}
	}

	public PageReference cancel(){
		return new PageReference('/'+pLoc.Id);
	}
}