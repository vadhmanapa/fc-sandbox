@isTest
private class CATMetricHandlerTest {
	@isTest static void testCATToAccount() {
		
		Profile p = [Select id from Profile where Name ='Credentialing Team'];
		User u = new User(LastName='User', 
						  Alias='use',
						  Email='testuser@test.com',
						  Username='testingtriggeruser@testingcenter.com',
						  CommunityNickname='testing',
						  EmailEncodingKey='UTF-8',
						  LanguageLocaleKey='en_US',
						  LocaleSidKey='en_US',
						  ProfileId = p.Id,
						  TimeZoneSidKey='America/New_York');
		insert u;

		Round_Robin__c rr = new Round_Robin__c(User__c = u.Id,
									Function__c = 'Credentialing CATs',
									Active__c = True);
		insert rr;

		Cred_Trigger_CS__c CTCS = new Cred_Trigger_CS__c();
		CTCS.Active__c = True;
		insert CTCS;

		System.runAs(u){
			// create a test account
			Map<String, Id> rTypeMap = new Map<String, Id>();

			Account testAcc = new Account(Name='Test Account', 
						        Type_Of_Provider__c = 'DME');
			insert testAcc;
            
            Contact testCon = new Contact(AccountId = testAcc.Id, FirstName = 'Test', LastName = 'Audit', Email = 'test@test.com');
            insert testCon;

			for(RecordType rt: [Select Id, Name from RecordType where sObjectType = 'CAT__c']) {
				rTypeMap.put(rt.Name, rt.Id);
			}
            
            System.debug('Number od Record types'+rTypeMap.size());

			// Case 1: Initial Credentialing

			CAT__c iniCred = new CAT__c(Account__c = testAcc.Id, Application_Completed_Date__c = System.today(), New_Application_Received__c = System.today(),
										Credentialing_Status__c = 'In Triage', RecordTypeId = rTypeMap.get('Credentialing Application'), Application_Type__c = 'Initial Credentialing',
										Missing_Documentation__c = 'Accreditation');
			insert iniCred;

			iniCred.On_Hold__c = True;
			update iniCred;

			iniCred.On_Hold__c = False;
			update iniCred;

			iniCred.Credentialing_Status__c = 'In CVO prep';
			update iniCred;

			CATMetricHandler CMH = new CATMetricHandler();
			CMH.getElapsedTime(Time.newInstance(12,0,0,0), Time.newInstance(13,0,0,0));

		}
	}
}