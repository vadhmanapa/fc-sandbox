@isTest
public class TestCaseDataFactory_IP {
	
	public static User csUser {get;set;}
	public static User claimUser {get;set;}
	public static List<Account> payorAccounts {get;set;}
	public static List<Account> providerAccounts {get;set;}
	public static List<Contact> payorContacts {get;set;}
	public static List<Contact> providerContacts {get;set;}
	public static List<Contact> patientContact {get;set;}
	public static List<Contact> familyContact {get;set;}

	// Case Reasons

	public List<CallerType__c> callCallerTypes {get;set;}
	public static List<CaseReason__c> reasons {get;set;}
	public static List<CaseReasonDetail__c> reasonDetail {get;set;}
	public static List<ActionDetail__c> actionAndDetails {get;set;}
	public static List<CRR__c> reasonAndActionDetails {get;set;}

	public static User generateCSUser (){
		User testCSUser = new User();
        testCSUser.FirstName = 'CustomerCallCase';
        testCSUser.LastName = 'Tester'+ System.currentTimeMillis(); // to maintain the randomness of username
        testCSUser.Belongs_To__c = 'Customer Service';
        testCSUser.Username = 'test'+System.currentTimeMillis()+'@test.com';
        testCSUser.CommunityNickname = 'cccase';
        testCSUser.ProfileId = [SELECT Id FROM Profile WHERE Name='Customer Service Supervisor w Service Cloud' LIMIT 1].Id;
        testCSUser.Email = 'test'+System.currentTimeMillis()+'@test.com';
        testCSUser.CompanyName = 'Integra Partners';
        testCSUser.Alias = 'alias';
        testCSUser.TimeZoneSidKey = 'America/New_York';
        testCSUser.EmailEncodingKey = 'UTF-8';
        testCSUser.LanguageLocaleKey = 'en_US';
        testCSUser.LocaleSidKey = 'en_US';
        //testCSUser.UserRoleId = ur1.Id;
        return testCSUser;
	}

	public static User generateClaimUser (){
		User testClaimUser = new User();
        testClaimUser.FirstName = 'ClaimsCallCase';
        testClaimUser.LastName = 'Tester'+ System.currentTimeMillis(); // to maintain the randomness of username
        testClaimUser.Username = 'test'+System.currentTimeMillis()+'@test.com';
        testClaimUser.CommunityNickname = 'cclaims';
        testClaimUser.Belongs_To__c = 'Claims';
        testClaimUser.ProfileId = [SELECT Id FROM Profile WHERE Name='Claims w Service Cloud' LIMIT 1].Id;
        testClaimUser.Email = 'test'+System.currentTimeMillis()+'@test.com';
        testClaimUser.CompanyName = 'Integra Partners';
        testClaimUser.Alias = 'alias2';
        testClaimUser.TimeZoneSidKey = 'America/New_York';
        testClaimUser.EmailEncodingKey = 'UTF-8';
        testClaimUser.LanguageLocaleKey = 'en_US';
        testClaimUser.LocaleSidKey = 'en_US';
        //testClaimUser.UserRoleId = ur2.Id;
        return testClaimUser;
	}

	public static List<Account> generatePayorAccs (String accName, Integer numOfAccsNeeded){
		List<Account> payorAccs = new List<Account>();
		Id payerRecId =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Payer Relations').getRecordTypeId();
		for(Integer i =0; i<numOfAccsNeeded; i++){
			Account a = new Account();
			a.Name = accName + i;
			a.Status__c = 'Active';
			a.Type__c = 'Payer';
			a.RecordTypeId = payerRecId;
			payorAccs.add(a);
		}

		return payorAccs;
	}

	public static List<Account> generateProviderAccs (String accName, Integer numOfAccsNeeded){
		List<Account> providerAccs = new List<Account>();
		Id providerRecId =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Integra Provider').getRecordTypeId();
		for(Integer i =0; i<numOfAccsNeeded; i++){
			Account a = new Account();
			a.Name = accName + i;
			a.Status__c = 'Active';
			a.Type__c = 'Provider';
			a.Tax_ID__c = '12345'+i;
			a.RecordTypeId = providerRecId;
			providerAccs.add(a);
		}

		return providerAccs;
	}

	public static List<Contact> generateContacts (String contactType, String lastName, Integer numOfAccsNeeded, Id accountId, Id patientId){
		List<Contact> contactList = new List<Contact>();
		Id conRecType = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(contactType).getRecordTypeId();
		for(Integer i=0; i<numOfAccsNeeded; i++){
			Contact c = new Contact();
			c.FirstName = 'Test';
			c.LastName = lastName+i;
			c.Status__c = 'Active';
			c.AccountId = accountId;
			if(String.isNotBlank(contactType) && contactType == 'Patient'){
				c.Health_Plan_ID__c = '12345'+i;
				c.Birthdate = Date.today().addYears(-i);
			}
			c.Phone = '213546987'+i;

			if(String.isNotBlank(contactType) && contactType == 'Family'){
				c.Patient__c = patientId;
				// c.RelatedPatient__c = patientId;
				c.RelationshiptoPatient__c = 'Relative';
			}

			c.RecordTypeId = conRecType;

			contactList.add(c);
		}
		return contactList;
	}

	public static Account queryMyAccount (Id accId){
		return [SELECT Id, Name, Type__c FROM Account WHERE Id =: accId LIMIT 1];
	}

	public void initTestData(){
		csUser = generateCSUser();
		insert csUser;

		claimUser = generateClaimUser();
		insert claimUser;

		payorAccounts = new List<Account>();
		payorAccounts = generatePayorAccs('Payor', 3);
		insert payorAccounts;

		providerAccounts = new List<Account>();
		providerAccounts = generateProviderAccs('Provider', 3);
		insert providerAccounts;

		patientContact = new List<Contact>();
		patientContact = generateContacts('Patient', 'Stark', 3, payorAccounts[0].Id, null);
		insert patientContact;

		familyContact = new List<Contact>();
		familyContact = generateContacts('Family', 'Stark', 2, payorAccounts[0].Id, patientContact[0].Id);
		insert familyContact;

		payorContacts = new List<Contact>();
		payorContacts = generateContacts('Payor', 'Superman', 3, payorAccounts[0].Id, null);
		insert payorContacts;

		providerContacts = new List<Contact>();
		providerContacts = generateContacts('Provider', 'Potter', 3, providerAccounts[0].Id, null);
		insert providerContacts;
	}

	public void generateReasonsAndActions(){
		CallerType__c testCallerTypeA = new CallerType__c();
		testCallerTypeA.CallerType__c = 'Patient';
		testCallerTypeA.RecordType__c = 'Customer Service Call';
		insert testCallerTypeA;

		//insert reasons
		reasons = new List<CaseReason__c>();
		CaseReason__c reasonA = new CaseReason__c();
		reasonA.CallerType__c = testCallerTypeA.Id;
		reasonA.Reason__c = 'New Referral Inquiry';
		reasons.add(reasonA);
		insert reasons;

		actionAndDetails = new List<ActionDetail__c>();

		ActionDetail__c actionA = new ActionDetail__c();
		actionA.Action__c = 'Provided Provider Information';
		actionA.ActionDetail__c = 'Provided 2-3 Provider options';
		actionA.Tier__c = 'Tier 1';
		actionAndDetails.add(actionA);

		insert actionAndDetails;

		//insert reason detail
		reasonDetail = new List<CaseReasonDetail__c>();

		CaseReasonDetail__c reasonDetailA = new CaseReasonDetail__c();
		reasonDetailA.CaseReason__c = reasonA.Id;
		reasonDetailA.CaseReasonDetail__c = 'DME Item Request';
		reasonDetail.add(reasonDetailA);

		CaseReasonDetail__c reasonDetailB = new CaseReasonDetail__c();
		reasonDetailB.CaseReason__c = reasonA.Id;
		reasonDetailB.CaseReasonDetail__c = 'O&P Item Request';
		reasonDetail.add(reasonDetailB);

		insert reasonDetail;

		CRR__c reasonActionA = new CRR__c();
		reasonActionA.CaseReasonDetail__c = reasonDetailA.Id;
		reasonActionA.ActionDetail__c = actionA.Id;
		insert reasonActionA;

		CRR__c reasonActionB = new CRR__c();
		reasonActionB.ActionDetail__c = actionA.Id;
		reasonActionB.CaseReasonDetail__c = reasonDetailB.Id;
		insert reasonActionB;

		CallerType__c testCallerTypeB = new CallerType__c();
		testCallerTypeB.CallerType__c = 'Provider';
		testCallerTypeB.RecordType__c = 'Claims Inquiry';
		insert testCallerTypeB;

	}

		
}