public class ProviderLocationServicesBatch_CS implements Database.Batchable<SObject> {
	
	public ProviderLocationServicesBatch_CS() {
		
	}
	
	public Database.QueryLocator start(Database.BatchableContext context) {

		Set<String> allObjectFields = Schema.SObjectType.Provider_Locations__c.fields.getMap().keySet();

		SoqlBuilder query = new SoqlBuilder()
			.selectx(allObjectFields)
			.fromx('Provider_Locations__c');
		
		String queryString = query.toSoql();
		System.debug('SB Query: ' + queryString);
		return Database.getQueryLocator(queryString);
	}

   	public void execute(Database.BatchableContext context, List<Provider_Locations__c> scope) {
		List<Provider_Location__c> newProviderLocationRecords = ProviderLocationTransferServices.transferDataToNewProviderLocationObject(scope);
	}
	
	public void finish(Database.BatchableContext context) {
		
	}
}