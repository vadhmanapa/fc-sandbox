@isTest
private class DMEProductConfigurationExt_CS_Test {
	
	static DME_Settings__c dmeSettings;
	static DMEProductConfigurationExtension_CS ext;
	
    static testMethod void initDMEList() {
        init();
        
        List<DME__c> testDMEs = TestDataFactory_CS.generateDMEs(3);
		insert testDMEs;
		System.debug('Test DMEs: ' + testDMEs);
		ext = new DMEProductConfigurationExtension_CS();
        
        Test.startTest();
        
        ext.categoryFilter = 'TestCat';
        ext.productCodeFilter = 'A00';
        ext.initDMEList(); 
        
        System.assertEquals(3,ext.setCon.getResultSize());
        
        Test.stopTest();
    }
    
    //For test coverage only. Utility tested on page
    static testMethod void removeDME() {
    	ext = new DMEProductConfigurationExtension_CS();
    	ext.removeDME();
    	System.assert(true);
    }
    
    static testMethod void saveNewDMEs() {
		init();
    	
    	List<DME__c> newDMEs = TestDataFactory_CS.generateDMEs(4);
    	ext = new DMEProductConfigurationExtension_CS();
    	for(DME__c d : newDMEs)	{
    		DMEProductConfigurationExtension_CS.dmeWrapper dw = new DMEProductConfigurationExtension_CS.dmeWrapper();
    		dw.dme = d;
    		ext.newDMEList.add(dw);
    	}
    	System.debug('New DME List: ' + ext.newDMEList);
    	ext.saveNewDMEs();
    	
    	SoqlBuilder sb = new SoqlBuilder().selectx('Id').fromx('DME__c');
    	ApexPages.Standardsetcontroller setCon = new ApexPages.Standardsetcontroller(Database.getQueryLocator(sb.toSoql()));
    	
    	//Includes the 'blank' DME created by addNewDME() when the constructor is called
    	System.assertEquals(5, setCon.getResultSize());
    }
    
    static testMethod void updateExistingDMEs() {
    	init();
    	
    	List<DME__c> testDMEs = TestDataFactory_CS.generateDMEs(3);
		insert testDMEs;
		System.debug('Test DMEs: ' + testDMEs);
		ext = new DMEProductConfigurationExtension_CS();
    	
    	ext.dmeList[0].Name = 'UniqueProductCode12bjk';
    	ext.updateExistingDMEs();
    	
    	FieldCondition condition = new FieldCondition('Name', Operator.EQUALS,'UniqueProductCode12bjk');
    	SoqlBuilder sb = new SoqlBuilder().selectx('Id').fromx('DME__c').wherex(condition);
    	ApexPages.Standardsetcontroller setCon = new ApexPages.Standardsetcontroller(Database.getQueryLocator(sb.toSoql()));
    	System.assertEquals(1, setCon.getResultSize());
    }
    
    //For test coverage only
    static testMethod void showHideNewBlock() {
    	ext = new DMEProductConfigurationExtension_CS();
    	ext.displayNewBlock = true;
    	ext.showHideNewBlock();
    	ext.showHideNewBlock();
    	System.assert(true);
    }
    
    static testMethod void getHasNext() {
    	init();
    	
    	List<DME__c> testDMEs = TestDataFactory_CS.generateDMEs(11);
		insert testDMEs;
		ext = new DMEProductConfigurationExtension_CS();
		
		System.assert(ext.getHasNext());
    }
    
	static testMethod void getHasNext_NoNext() {
		init();
		
		List<DME__c> testDMEs = TestDataFactory_CS.generateDMEs(3);
		insert testDMEs;
		ext = new DMEProductConfigurationExtension_CS();
		
		System.assert(!ext.getHasNext());
	}
	
	static testMethod void getHasPrevious() {
		init();
    	
		List<DME__c> testDMEs = TestDataFactory_CS.generateDMEs(11);
		insert testDMEs;
		ext = new DMEProductConfigurationExtension_CS();
		
		System.assert(!ext.getHasPrevious());
		
		ext.getNext();
		System.assert(ext.getHasPrevious());
	}
	
	static testMethod void getNext() {
		init();
    	
		List<DME__c> testDMEs = TestDataFactory_CS.generateDMEs(18);
		insert testDMEs;
		ext = new DMEProductConfigurationExtension_CS();
		
		//Checks there are 3 pages and navigation only works when a page exists
		ext.pageSize = 6;
		ext.getNext(); ext.getNext();
		System.assertEquals(3,ext.pageNumber);
		ext.getNext();
		System.assertEquals(3,ext.pageNumber);

	}
	
	static testMethod void getPrevious() {
		init();
    	
		List<DME__c> testDMEs = TestDataFactory_CS.generateDMEs(18);
		insert testDMEs;
		ext = new DMEProductConfigurationExtension_CS();
		
		//Checks that getPrevious() navigates backwards until page 1, then stays on page 1
		ext.getNext();
		System.assertEquals(2,ext.pageNumber);
		ext.getPrevious();
		System.assertEquals(1,ext.pageNumber);
		ext.getPrevious();
		System.assertEquals(1,ext.pageNumber);
	}
	
	static testMethod void getFirst() {
		init();
    	
		List<DME__c> testDMEs = TestDataFactory_CS.generateDMEs(18);
		insert testDMEs;
		ext = new DMEProductConfigurationExtension_CS();
		
		//Checks that getFirst() navigates to page 1
		ext.pageSize = 6;
		ext.getNext(); ext.getNext();
		System.assertEquals(3,ext.pageNumber);
		ext.getFirst();
		System.assertEquals(1,ext.pageNumber);
	}
	
	static testMethod void getLast_RecordsSizeDividesPageSize() {
		init();
    	
		List<DME__c> testDMEs = TestDataFactory_CS.generateDMEs(20);
		insert testDMEs;
		ext = new DMEProductConfigurationExtension_CS();
		
		ext.getLast();
		System.assertEquals(2,ext.pageNumber);
	}
	
	static testMethod void getLast_RecordsSizeDoesNotDividePageSize() {
		init();
    	
		List<DME__c> testDMEs = TestDataFactory_CS.generateDMEs(21);
		insert testDMEs;
		ext = new DMEProductConfigurationExtension_CS();
		
		ext.getLast();
		System.assertEquals(3,ext.pageNumber);
	}
    
    static void init(){
		dmeSettings = TestDataFactory_CS.generateDMESettings();
		insert dmeSettings;
	}//End initTestData
}