/**
 *  @Description Test Class for PlanPatientBatch_CS
 *  @Author Cloud Software LLC, Jasmin Brown
 *  Revision History: 
 *      
 *
 */
@isTest
public class PlanPatientBatchTest_CS { 
	
	private static List<Plan_Patient__c> testPlanPatients;
	private static List<Plan_Patient__c> testPlanPatients2;
	private static List<Account> testAccounts;
	private static List<Order__c> testOrders;

	static testmethod void PlanPatientBatch_Test() {
		init();

		// Clear criteria fields
		for(Integer i = 0; i < 15; i++) {
			testPlanPatients[i].ID_Number__c = null;
			testPlanPatients[i].First_Name__c = null;
			testPlanPatients[i].Last_Name__c = null;
			testPlanPatients[i].Date_of_Birth__c = null;
		}

		for(Integer i = 0; i < 15; i++) {
			testPlanPatients2[i].ID_Number__c = null;
			testPlanPatients2[i].First_Name__c = null;
			testPlanPatients2[i].Last_Name__c = null;
			testPlanPatients2[i].Date_of_Birth__c = null;
		}

		// Make the first 10 Plan Patients meet criteria 1, 5 will be missing fields
		for(Integer i = 0; i < 5; i++) {
			if(testPlanPatients[i].Health_Plan__c == testAccounts[0].Id) {
				testPlanPatients[i].ID_Number__c = 'Test ID Number';
				testPlanPatients[i].Apartment_Number__c = 'Test Apartment Number';
				testPlanPatients[i].City__c = 'Test City';
				testPlanPatients[i].Email_Address__c = 'test@test.com';
				testPlanPatients[i].Last_Name__c = 'Test Last Name 1';
				testPlanPatients[i].Medicaid_ID__c = 'Test Medicaid ID';
				testPlanPatients[i].Medicare_ID__c = 'Test Medicare ID';
			}
		}


		for(Integer i = 5; i < 10; i++) {
			if(testPlanPatients[i].Health_Plan__c == testAccounts[0].Id) {
				testPlanPatients[i].ID_Number__c = 'Unique ID Number ' + i;
				testPlanPatients[i].ID_Number__c = 'Test ID Number';
				testPlanPatients[i].Last_Name__c = 'Test Last Name 1';
			}
		}
		
		// Make the next 10 Plan Patients meet criteria 2, 5 will be missing fields
		for(Integer i = 10; i < 15; i++) {
			if(testPlanPatients[i].Health_Plan__c == testAccounts[1].Id) {
				testPlanPatients[i].ID_Number__c = 'Unique ID Number ' + i;
				testPlanPatients[i].First_Name__c = 'TestFirstName 2';
				testPlanPatients[i].Last_Name__c = 'TestLastName 2';
				testPlanPatients[i].Date_of_Birth__c = System.today();
				testPlanPatients[i].Apartment_Number__c = 'Test Apartment Number';
				testPlanPatients[i].City__c = 'Test City';
				testPlanPatients[i].Email_Address__c = 'test@test.com';
				testPlanPatients[i].Medicaid_ID__c = 'Test Medicaid ID';
				testPlanPatients[i].Medicare_ID__c = 'Test Medicare ID';
			}
		}

		insert testPlanPatients;

		for(Integer i = 0; i < 5; i++) {
			if(testPlanPatients2[i].Health_Plan__c == testAccounts[1].Id) {
				testPlanPatients2[i].ID_Number__c = 'Unique ID Number ' + i;
				testPlanPatients2[i].First_Name__c = 'TestPatientFirstName ' + i;
				testPlanPatients2[i].Last_Name__c = 'TestPatientLastName ' + i;
				testPlanPatients2[i].Date_of_Birth__c = System.today();
			}
		}

		// Make the last 10 Plan Patients NEVER meet criteria
		for(Integer i = 5; i < 15; i++) {
			if(testPlanPatients2[i].Health_Plan__c == testAccounts[2].Id) {
				testPlanPatients2[i].ID_Number__c = 'Unique ID Number ' + i;
				testPlanPatients2[i].First_Name__c = 'TestFirst ' + i;
				testPlanPatients2[i].Last_Name__c = 'TestLast ' + i;
				testPlanPatients2[i].Date_of_Birth__c = System.today().addDays(1);
			}
		}

		// Make the last 5 Plan Patients have null ID numbers
		for(Integer i = 15; i < 20; i++) {
			if(testPlanPatients2[i].Health_Plan__c == testAccounts[3].Id) {
				testPlanPatients2[i].ID_Number__c = null;
				testPlanPatients2[i].First_Name__c = 'TestFirst ' + i;
				testPlanPatients2[i].Last_Name__c = 'TestLast ' + i;
				testPlanPatients2[i].Date_of_Birth__c = System.today().addDays(2);
			}
		}
		System.debug(testPlanPatients2);
		insert testPlanPatients2;

		testOrders = TestDataFactory_CS.generateOrders(testPlanPatients2[0].Id, testAccounts[1].Id, 5);
		insert testOrders;
        
		List<Plan_Patient__c> testPlanPatientsPreBatch = [
			SELECT 
				Health_Plan__c
			FROM Plan_Patient__c 
		];

        Test.startTest();
        String jobId = Database.executeBatch(new PlanPatientBatch_CS());
        Test.stopTest();

		List<Plan_Patient__c> testPlanPatientsPostBatch = [
			SELECT 
				Health_Plan__c
			FROM Plan_Patient__c 
		];

		List<Plan_Patient__c> testPlanPatientsMeetingCriteria1 = [
			SELECT 
				Apartment_Number__c,
				City__c,
				Email_Address__c,
				Last_Name__c,
				First_Name__c,
				Medicaid_ID__c,
				Medicare_ID__c,
				Health_Plan__c,
				ID_Number__c,
				Date_of_Birth__c
			FROM Plan_Patient__c 
			WHERE Health_Plan__c = :testAccounts[0].Id
		];

		List<Plan_Patient__c> testPlanPatientsMeetingCriteria2 = [
			SELECT 
				Apartment_Number__c,
				City__c,
				Email_Address__c,
				Last_Name__c,
				First_Name__c,
				Medicaid_ID__c,
				Medicare_ID__c,
				Health_Plan__c,
				ID_Number__c,
				Date_of_Birth__c
			FROM Plan_Patient__c 
			WHERE Health_Plan__c = :testAccounts[1].Id
		];

		List<Plan_Patient__c> testPlanPatientsNOTMeetingCriteria = [
			SELECT 
				Apartment_Number__c,
				City__c,
				Email_Address__c,
				Last_Name__c,
				First_Name__c,
				Medicaid_ID__c,
				Medicare_ID__c,
				Health_Plan__c,
				ID_Number__c,
				Date_of_Birth__c
			FROM Plan_Patient__c 
			WHERE Health_Plan__c = :testAccounts[2].Id
		];

		List<Plan_Patient__c> testPlanPatientsNullIDsExist = [
			SELECT 
				Apartment_Number__c,
				City__c,
				Email_Address__c,
				Last_Name__c,
				First_Name__c,
				Medicaid_ID__c,
				Medicare_ID__c,
				Health_Plan__c,
				ID_Number__c,
				Date_of_Birth__c
			FROM Plan_Patient__c 
			WHERE Health_Plan__c = :testAccounts[3].Id
		];

		List<Order__c> testOrdersReassigned = [
			SELECT
				Plan_Patient__c
			FROM Order__c
		];


		System.assertEquals(35,testPlanPatientsPreBatch.size());
		System.assertEquals(22,testPlanPatientsPostBatch.size());

		System.assertEquals(1,testPlanPatientsMeetingCriteria1.size());
		System.assertEquals(6,testPlanPatientsMeetingCriteria2.size());
		System.assertEquals(10,testPlanPatientsNOTMeetingCriteria.size());
		System.assertEquals(5,testPlanPatientsNullIDsExist.size());

		System.assertEquals('Test ID Number',testPlanPatientsMeetingCriteria1[0].ID_Number__c);
		System.assertEquals('Test Apartment Number',testPlanPatientsMeetingCriteria1[0].Apartment_Number__c);
		System.assertEquals('Test City',testPlanPatientsMeetingCriteria1[0].City__c);
		System.assertEquals('test@test.com',testPlanPatientsMeetingCriteria1[0].Email_Address__c);
		System.assertEquals('Test Last Name 1',testPlanPatientsMeetingCriteria1[0].Last_Name__c);
		System.assertEquals('Test Medicaid ID',testPlanPatientsMeetingCriteria1[0].Medicaid_ID__c);
		System.assertEquals('Test Medicare ID',testPlanPatientsMeetingCriteria1[0].Medicare_ID__c);

		System.assertEquals('TestFirstName 2',testPlanPatientsMeetingCriteria2[0].First_Name__c);
		System.assertEquals('TestLastName 2',testPlanPatientsMeetingCriteria2[0].Last_Name__c);
		System.assertEquals(System.today(),testPlanPatientsMeetingCriteria2[0].Date_of_Birth__c);
		System.assertEquals('Test Apartment Number',testPlanPatientsMeetingCriteria2[0].Apartment_Number__c);
		System.assertEquals('Test City',testPlanPatientsMeetingCriteria2[0].City__c);
		System.assertEquals('test@test.com',testPlanPatientsMeetingCriteria2[0].Email_Address__c);
		System.assertEquals('Test Medicaid ID',testPlanPatientsMeetingCriteria2[0].Medicaid_ID__c);
		System.assertEquals('Test Medicare ID',testPlanPatientsMeetingCriteria2[0].Medicare_ID__c);

		System.assertEquals(null , testPlanPatientsNullIDsExist[0].ID_Number__c);

		//System.assertEquals(testPlanPatientsMeetingCriteria2[0].Id, testOrdersReassigned[0].Plan_Patient__c); TODO: FIX ASSERT - karnold - 03/05/2016 - commented out for deploy 

	}

    private static void init(){
		// Create Accounts
		testAccounts = TestDataFactory_CS.generatePlans('Test', 4);
		insert testAccounts;

		// Create Patients - necessary in order to create Plan Patients
		List<Patient__c> testPatients = TestDataFactory_CS.generatePatients('Test', 35);
		insert testPatients;

		Map<Id, List<Patient__c>> testPatientMap = new Map<Id, List<Patient__c>>();

		for(Integer i = 0; i < 10; i++) {
			if(testPatientMap.keySet().contains(testAccounts[0].Id)) {
				testPatientMap.get(testAccounts[0].Id).add(testPatients[i]);
			} else {
				testPatientMap.put(testAccounts[0].Id,new List<Patient__c>{testPatients[i]});
			}
		}
		for(Integer i = 10; i < 20; i++) {
			if(testPatientMap.keySet().contains(testAccounts[1].Id)) {
				testPatientMap.get(testAccounts[1].Id).add(testPatients[i]);
			} else {
				testPatientMap.put(testAccounts[1].Id,new List<Patient__c>{testPatients[i]});
			}
		}

		for(Integer i = 20; i < 30; i++) {
			if(testPatientMap.keySet().contains(testAccounts[2].Id)) {
				testPatientMap.get(testAccounts[2].Id).add(testPatients[i]);
			} else {
				testPatientMap.put(testAccounts[2].Id,new List<Patient__c>{testPatients[i]});
			}
		}

		for(Integer i = 30; i < 35; i++) {
			if(testPatientMap.keySet().contains(testAccounts[3].Id)) {
				testPatientMap.get(testAccounts[3].Id).add(testPatients[i]);
			} else {
				testPatientMap.put(testAccounts[3].Id,new List<Patient__c>{testPatients[i]});
			}
		}

		//Create Plan Patients that look up to the above Accounts
		List<Plan_Patient__c> tempList = TestDataFactory_CS.generatePlanPatients(testPatientMap); //Creates 35 records

		testPlanPatients = new List<Plan_Patient__c>();
		testPlanPatients2 = new List<Plan_Patient__c>();
		for(Integer i = 0; i < 15; i++) {
			testPlanPatients.add( tempList.get(i) );
		}

		for(Integer i = 15; i < 35; i++) {
			testPlanPatients2.add( tempList.get(i) );
		}
	}
}