@isTest
private class CallLogRelationModelTest {

	static Call_Log_Relation__c relation;

    static testMethod void test() {
        
        init();
        
		CallLogRelationModel model = new CallLogRelationModel(relation);
		
		system.assertEquals(true,model.isPopulated);
		system.assertEquals(true,CallLogRelationModel.isPopulated(relation));
		
		system.assertEquals(Contact.getSObjectType(),model.relationType);
		system.assertEquals('Contact',model.relationTypeName);
    }
    
    static void init() {
    	
    	Call_Log__c testLog = new Call_Log__c();
		insert testLog;
		
		Contact testContact = new Contact(
			FirstName = 'Test',
			LastName = 'Contact'
		);
		insert testContact;
		
		relation = new Call_Log_Relation__c(
			Call_Log__c = testLog.Id,
			Contact__c = testContact.Id
		);
		insert relation;
    }
}