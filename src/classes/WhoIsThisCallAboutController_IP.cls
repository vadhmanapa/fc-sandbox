public with sharing class WhoIsThisCallAboutController_IP extends ComponentControllerBase_IP {

    public String selectedCallAboutType {get;set;}
    
    public  List<SelectOption> getCallAboutType(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--None--'));
        options.add(new SelectOption('Patient', 'Patient'));
        options.add(new SelectOption('Payor', 'Payor'));
        options.add(new SelectOption('Provider', 'Provider'));

        return options;
    }
    
    public Case startCase {get;set{
        if(value != null){
            startCase = new Case();
            startCase = value;
            if(!init){
                valuesFromCase();
                init = true;
            }
        }
    }}

    public Account accThisCallAbout {get;set;}
    public Contact conThisCallAbout {get;set;}
    public Contact autoFillPatientContact {get; set;}
    public String selectedCallerType {get;set;}
    public Boolean isNewPatient {get;set;}
    public Contact insertNewPatient {get;set;}
    public String[] errorList {get;set;}
    public Boolean validateError {get;set;}
    
    public Boolean init {get{if (init == null) init = false; return init;}set;}
    
    public void valuesFromCase(){
        if(startCase != null){
            if(startCase.PayerAccount__c != null) {
                selectedCallAboutType = 'Payor';
                retreiveAccountDetailsFromId(startCase.PayerAccount__c);
            }
            
            if(startCase.Provider_Account__c != null){
                selectedCallAboutType = 'Provider';
                retreiveAccountDetailsFromId(startCase.Provider_Account__c);
            }
            
            if(startCase.Patient__c != null){
                selectedCallAboutType = 'Patient';
                retreiveContactDetailsFromId(startCase.Patient__c);
            }
        }
    }

    public void initAboutCall(){
    	if(String.isNotBlank(selectedCallAboutType) && selectedCallAboutType == 'Patient') {
    		// Instantiate contact
    		conThisCallAbout = new Contact();
    		if((selectedCallerType == 'Patient' || selectedCallerType == 'Family') && autoFillPatientContact != null) {
    			// Copy patients info
    			conThisCallAbout = autoFillPatientContact;
    		}
    	}else if (String.isNotBlank(selectedCallAboutType) && (selectedCallAboutType == 'Provider' || selectedCallAboutType == 'Payor')) {
    		accThisCallAbout = new Account();
    	}else {
    		
    	}
    }

    public void copyPatientDetails(){
    	if(conThisCallAbout != null && String.isNotBlank(conThisCallAbout.Id)) {
    		// Search for contact Id and auto fill patient info
            Contact c = [SELECT Id, Name, Birthdate,FirstName, LastName, Account.Name, AccountId, RecordTypeId, Account.Status__c, Phone, Email, Health_Plan_ID__c FROM Contact WHERE Id=: conThisCallAbout.Id LIMIT 1];
            if(c != null) {
                // then copy patient info
                conThisCallAbout = c;
            }
    	}
    }
    
    public void retreiveContactDetailsFromId(Id i){
        Contact c = [SELECT Id, Name, Birthdate,FirstName, LastName, Account.Name, AccountId, RecordTypeId, Account.Status__c, Phone, Email, Health_Plan_ID__c FROM Contact WHERE Id=:i LIMIT 1];
            if(c != null) {
                // then copy patient info
                conThisCallAbout = c;
            }
    }
    
    public void retreiveAccountDetailsFromId(Id i){
        Account acc = [SELECT Id, Name FROM Account WHERE Id=:i LIMIT 1];
        if(acc != null){
            accThisCallAbout = new Account();
            accThisCallAbout = acc;
        }
    }

    public void addNewPatient(){
        //if(isNewPatient) {
            // Validate new patient's input
            if(contactValidatedBeforeSave(insertNewPatient)) {
                Database.SaveResult insertCon = Database.insert(insertNewPatient, true);
                if(insertCon.isSuccess()) {
                    // then pass the contact back to component
                    System.debug('New Contact insert successful'+insertCon.getId());
                    isNewPatient = false;
                    conThisCallAbout = new Contact();
                    conThisCallAbout = [SELECT Id, Name, Birthdate, FirstName, LastName, Account.Name, AccountId, RecordTypeId, Account.Status__c, Phone, Email, Health_Plan_ID__c FROM Contact WHERE Id=: insertCon.getId() LIMIT 1];
                }
            }
        //}
    }

    public void initNewPatientContact(){
        insertNewPatient = new Contact();
        validateError = false;
        if(conThisCallAbout.AccountId != null) {
            insertNewPatient.AccountId = conThisCallAbout.AccountId;
            insertNewPatient.RecordTypeId = RecordTypes.availableContactTypes.get('Patient').getRecordTypeId();
        }
    }

    public Boolean contactValidatedBeforeSave(Contact con){
        errorList = new String[]{};

        if(String.isBlank(con.FirstName)) {
            System.debug('Missing First Name');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Missing First Name'));
            //errorList.add('Missing First Name');
            validateError = true;
            return false;
        }
        if(String.isBlank(con.LastName)) {
            System.debug('Missing Last Name');
            // errorList.add('Missing Last Name');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Missing Last Name'));
            validateError = true;
            return false;
        }

        if(con.Birthdate == null) {
            System.debug('Missing Birthdate');
            // errorList.add('Missing Date of Birth');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Missing Patient Date of Birth'));
            validateError = true;
            return false;
        }

        if(con.Phone == null || String.isBlank(con.Phone)) {
            System.debug('Missing Phone Number');
            //errorList.add('Missing Phone Number');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Missing Patient Phone Number'));
            validateError = true;
            return false;
        }

        if(String.isBlank(con.Health_Plan_ID__c)) {
            System.debug('Missing Health Plan Id');
            // errorList.add('Missing Health Plan ID');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Missing Patient Health Plan ID'));
            validateError = true;
            return false;
        }

        System.debug('Contact check completed and validated. Proceed with insert');
        return true;
    }

	public WhoIsThisCallAboutController_IP() {
		isNewPatient = false;
	}

    public void setParentControllerValues(){

        if(selectedCallAboutType == 'Patient') {
            pageController.caseLog.caseDetails.Patient__c = conThisCallAbout.Id;
            pageController.caseLog.caseDetails.Patient_Payor__c = conThisCallAbout.AccountId;
            pageController.caseLog.caseDetails.PolicyNumber__c = conThisCallAbout.Health_Plan_ID__c;
            pageController.caseLog.caseDetails.DOB__c = conThisCallAbout.Birthdate;
            pageController.caseLog.caseDetails.Patient_First_Name__c = conThisCallAbout.FirstName;
            pageController.caseLog.caseDetails.Patient_Last_Name__c = conThisCallAbout.LastName;
            pageController.caseLog.caseDetails.PatientHealthPlan__c = conThisCallAbout.Account.Name;
        }else {
            if(selectedCallAboutType == 'Payor') {
                pageController.caseLog.caseDetails.PayerAccount__c = accThisCallAbout.Id;
            }

            if(selectedCallAboutType == 'Provider') {
                pageController.caseLog.caseDetails.Provider_Account__c = accThisCallAbout.Id;
            }
        }
    }
}