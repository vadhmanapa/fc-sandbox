public class ProviderAttributes {
    
	public Integer id;
	public String sfid;
	public String name;
	public String hcErr;
	public String hcLastop;
	public String lastmodifiedbyid;
	public String providerC;
	public String simulationResultsC;
	public Date lastactivitydate;
	public Date lastmodifieddate;
	public Date createddate;
	public Date systemmodstamp;
    public Double overallPercentileC;
    public Double acceptanceDurationC;
    public Double ordersReassignedC;
    public Double ordersUpdatedAsCompletedC;
    public Double ordersAcceptedC;
    public Double ordersRejectedC;
    public Double averageDeliveryCompletionTimeC;
    public Double accessibilityRateC;
    public Double driveTimeC;
    public Double shippingTimeC;
    public Boolean hasHistoryC;
	public Boolean isdeleted;
}