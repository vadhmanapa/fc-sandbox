public with sharing class CaseUtils {

	public static final String CASE_TYPE_QUE_SUPPORT = 'Que Support';
	public static final String CASE_REASON_MULTI = 'Multiple Items Selected';

	public static void debugOnCase(Case c, String msg) {
        
        if(c.Debug_Log__c == null) {
            c.Debug_Log__c = '';
        }
        
        c.Debug_Log__c += msg + '\n';
    }

	public static void CopyFromCallLog(Call_Log__c log, Case c) {
		CopyFromCallLog(new List<Call_Log__c>{log},new List<Case>{c});		
	}

	public static void CopyFromCallLog(List<Call_Log__c> logs, List<Case> cases) {
		
		if(logs.size() != cases.size()) {
			System.debug(LoggingLevel.ERROR,'Mismatched list sizes. Aborting.');
			return;
		}
		
		//Collect Contacts
		Set<Id> contactIds = new Set<Id>();
		
		for(Call_Log__c log : logs) {
			if(log.Caller_Contact__c == null){ 
				System.debug(LoggingLevel.WARN,'Log ' + log.Name + ' has no Contact.');
				continue; 
			}
			contactIds.add(log.Caller_Contact__c);
		}
		
		Map<Id,Contact> contactMap = new Map<Id,Contact>([
			SELECT Id,AccountId,Email,Phone,MobilePhone
			FROM Contact
			WHERE Id IN :contactIds
		]);
		
		for(Integer i = 0; i < logs.size(); i++) {
			
			Call_Log__c log = logs[i];
			Case c = cases[i];
			
			//Log info - modified 4/16/2015 ISSCI-33
			//Modified again by INeufer 9/1/2105 to add Account 
			c.Type = log.Call_Reason__c;
			c.Reason = log.Call_Reason_Detail__c;
			c.Description = log.Call_Result_Description__c;
			c.Que_Help_Page__c = log.Que_Help_Page__c;
			c.Que_Help_Details__c = log.Que_Help_Details__c;
			c.Other_Products__c = log.Other_Products__c;
			c.Location__c = log.Location__c;
			c.AccountId = log.Caller_Account__c;
			//System.debug(LoggingLevel.INFO,log.Call_Reason_Detail_Multi__c);
			//System.debug(LoggingLevel.INFO,c.Que_Support_Detail__c);
			System.debug('Case type: ' + c.Type);
			if (c.Type == CASE_TYPE_QUE_SUPPORT || c.Type == 'Test'){
				//c.Que_Support_Detail__c = saveMultiselect(log.Call_Reason_Detail_Multi__c);
				List<String> selected = parseMultiselect(log.Call_Reason_Detail_Multi__c);
				System.debug('Options: ' + selected);
				c.RecordTypeId = RecordTypes.queId;
				if (selected.size() == 1){
					c.Reason = selected[0];
				} else if (selected.size() > 1){
					c.Reason = CASE_REASON_MULTI;
				}
			}
			
			
			//Patient info
			if(log.Patient__c != null) {
				c.Patient__c = log.Patient__c;
				c.Patient_Payor__c = log.Patient_Payor__c;
			}
			
			//Caller info
			if(log.Caller_Contact__c != null) {
				
				//Check for Contact
				if(!contactMap.containsKey(log.Caller_Contact__c)) {
					System.debug(LoggingLevel.WARN,'Could not find Contact. Aborting Case ' + c.CaseNumber);
					continue;
				}
				
				//Get Contact
				Contact caseContact = contactMap.get(log.Caller_Contact__c);
				
				//Copy Contact info
				CopyFromContact(caseContact,c);
			}
		}
	}
	
	public static void CopyFromContact(Contact contact, Case c) {
		CopyFromContact(new List<Contact>{contact},new List<Case>{c});
	}
	
	public static void CopyFromContact(List<Contact> contacts, List<Case> cases){
    	
    	if(contacts.size() != cases.size()) {
			System.debug(LoggingLevel.ERROR,'Mismatched list sizes. Aborting.');
			return;
		}
		
		for(Integer i = 0; i < contacts.size(); i++) {
		
			Contact contact = contacts[i];
			Case c = cases[i];
		
			c.ContactId = contact.Id;
			c.AccountId = contact.AccountId;
			c.SuppliedEmail = contact.Email;
			c.SuppliedPhone = (contact.Phone != null ? 
				contact.Phone : contact.MobilePhone);
		}
    }
    
    //Parsing selectlists into multiselect fields - added 4/16/2015 ISSCI-33
    
    public static List<String> parseMultiselect(String input){
    	if (String.isBlank(input) || input == '[]'){
    		return new List<String>();
    	}
    	input = input.removeStart('[').removeEnd(']');
    	List<String> values = input.split(',', 0);
    	for (Integer i=0; i<values.size(); i++){
    		values[i] = values[i].trim();
    	}
    	return values;
    }
    
    public static String formatMultiselect(List<String> input){
    	String output = '';
    	for (String s : input){
    		output += s + ';';
    	}
    	output = output.removeEnd(';');
    	return output;
    }
    
    public static String saveMultiselect(String input){
    	return formatMultiselect(parseMultiselect(input));
    }
}