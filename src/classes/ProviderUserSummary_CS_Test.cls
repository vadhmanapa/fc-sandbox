/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ProviderUserSummary_CS_Test {

	/******* Test Parameters *******/
	static private final Integer N_USERS = 5;
	
	/******* Provider *******/
	static private List<Account> providers;
	static private List<Contact> providerUsers;

	/******* Test Objects *******/
	static private ProviderUserSummary_CS userSummary;

    static testMethod void ProviderUserSummary_Test_PageLanding(){
        init();
        
        System.assert(userSummary.thisUser != null, 'No user context for summary.');
    }
    
    static testMethod void ProviderUserSummary_Test_EditUser(){
    	init();
        
		userSummary.saveUser();
		
		System.assert(userSummary.usernameErrorMsg == '','Error messages present.');
    }
    
    static testMethod void ProviderUserSummary_Test_EditUserBlankUsername(){
    	init();
        
        userSummary.username = '';
        
		userSummary.saveUser();
		
		System.assert(userSummary.usernameErrorMsg != '','Error messages not present.');
    }

    static testMethod void ProviderUserSummary_Test_EditUserDuplicateUsername(){
    	init();
        
        userSummary.username = providerUsers[2].Username__c;        
        
        userSummary.saveUser();
        
        System.assert(userSummary.usernameErrorMsg != '','Error messages not present.');		
    }

    static testMethod void ProviderUserSummary_Test_SendUsername(){
    	init();
        
        System.assert(userSummary.thisUser != null, 'No user context for summary.');
        
        //TODO: How to validate this?
        userSummary.sendUsername();
    }
    
    static testMethod void ProviderUserSummary_Test_ResetPassword(){
    	init();
        
        System.assert(userSummary.thisUser != null, 'No user context for summary.');
        
        String oldPassword = userSummary.thisUser.instance.Password__c;
        
        userSummary.resetPassword();
        
        //Re-query the user
        Contact c = [select Id,Password__c from Contact where Id = :userSummary.thisUser.instance.Id];
        
        System.assert(c.Password__c != oldPassword, 'Password not reset.');
    }
    
    static testMethod void ProviderUserSummary_Test_DeactivateUser(){
    	init();
        
        System.assert(userSummary.thisUser != null, 'No user context for summary.');
        
        userSummary.deactivateUser();
        
        //Re-query the user
        Contact c = [select Id,Active__c from Contact where Id = :userSummary.thisUser.instance.Id];
        
        System.assert(c.Active__c == false, 'User still active.');
    }

    static void init(){
    	
    	///////////////////////////////////
		// Data
		///////////////////////////////////
    	
    	//Providers
		providers = TestDataFactory_CS.generateProviders('TestProvider', 1);
		insert providers;
		
		//Provider users, set user roles/profiles
		providerUsers = TestDataFactory_CS.createProviderContacts('ProviderUser', providers, N_USERS);
		//providerUsers[0].Role__c = 'Admin';
		providerUsers[0].Profile__c = 'Admin';
		//providerUsers[1].Role__c = 'Admin';
		providerUsers[1].Profile__c = 'Admin';
		update providerUsers;

		///////////////////////////////////
		// Page Landing
		///////////////////////////////////

		//Set the page and any parameters
		PageReference pr = Page.ProviderUserSummary_CS;
		pr.getParameters().put('uid',providerUsers[1].Id);
		pr.setRedirect(true);
		Test.setCurrentPage(pr);
		
		//Log the desired user in
		TestServices_CS.login(providerUsers[0]);
    	
    	//Land on the page
    	userSummary = new ProviderUserSummary_CS();
    	userSummary.init();
    }
}