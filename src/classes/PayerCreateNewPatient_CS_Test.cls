/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PayerCreateNewPatient_CS_Test extends PayerPortalServices_CS{

	/******* Provider *******/
	static private List<Account> providers;
	static private List<Contact> providerUsers;
	
	/******* Payer *******/
	static private List<Account> payers;
	static private List<Contact> payerUsers;
	
	/******* Patients *******/
	static private List<Patient__c> patients;
	static private List<Plan_Patient__c> planPatients;
	
	/******* Test Objects *******/
	static private PayerCreateNewPatient_CS newPatientClass;

    static testMethod void testPatients(){
        init();
        
        createTestPatients();
        
        System.assert(checkRequiredFields(planPatients[0]), 'Not all required fields have info');
        System.assert(checkRequiredFields(planPatients[1]), 'Not all required fields have info');
        System.assert(!checkRequiredFields(planPatients[2]), 'All required fields have info');
        System.assert(!checkRequiredFields(planPatients[3]), 'All required fields have info');
        
        
        for(Plan_Patient__c p : planPatients)
        {
        	if (checkRequiredFields(p) == true)
        	{
	        	newPatientClass.newPatient = p;
	        	System.assert(newPatientClass.CreateRecord() != null, 'Record not created');
        	}
        }
        
    }
    
    static testMethod void insertNullPatient(){
    	
    	init();
    	
    	createTestPatients();
    	newPatientClass.newPatient = planPatients[3];
    	System.assert(newPatientClass.CreateRecord() != null, 'Record not created');
    	
    }
    
    static void createTestPatients(){
    	
    	planPatients = new List<Plan_Patient__c>();
    	
    	Plan_Patient__c patient1 = new Plan_Patient__c(), patient2 = new Plan_Patient__c(), 
    	 patient3 = new Plan_Patient__c(), patient4 = new Plan_Patient__c();
    	
    	patient1.First_Name__c = 'A';
    	patient1.Last_Name__c = 'B';
    	patient1.Email_Address__c = 'C@email.com';
    	patient1.Date_of_Birth__c = Date.newInstance(1990, 1, 1);
    	patient1.Street__c = '123 E Street';
    	patient1.Apartment_Number__c = '1026';
    	patient1.City__c = 'New York';
    	patient1.State__c = 'NY';
    	patient1.Zip_Code__c = '00000';
    	patient1.Phone_Number__c ='6029547148';
    	patient1.Cell_Phone_Number__c = '4804154399';
    	patient1.Native_Language__c = 'English';
    	patient1.ID_Number__c = '1';
    	patient1.Medicare_ID__c = '2';
    	patient1.Medicaid_ID__c = '3';
    	patient1.Medicare_Primary_Patient__c = true;
    	
    	patient2.First_Name__c = 'D';
    	patient2.Last_Name__c = 'E';
    	patient2.Email_Address__c = 'F@email.com';
    	patient2.Date_of_Birth__c = Date.newInstance(1990, 1, 1);
    	patient2.Street__c = '123 E Street';
    	patient2.City__c = 'New York';
    	patient2.State__c = 'NY';
    	patient2.Zip_Code__c = '00000';
    	
    	patient3.First_Name__c = 'G';
    	patient3.Email_Address__c = 'H@email.com';
    	patient3.Date_of_Birth__c = Date.newInstance(1990, 1, 1);
    	patient3.Street__c = '123 E Street';
    	patient3.City__c = 'New York';
    	patient3.State__c = 'NY';
    	patient3.Zip_Code__c = '00000';
    	
    	planPatients.add(patient1);
    	planPatients.add(patient2);
    	planPatients.add(patient3);
    	planPatients.add(patient4);

    }
    
    static Boolean checkRequiredFields(Plan_Patient__c p){
    	if(p.First_Name__c == null || p.Last_Name__c == null || p.Email_Address__c == null || p.Date_of_Birth__c == null ||
    	 p.Street__c == null || p.City__c == null || p.State__c == null || p.Zip_Code__c == null){
    		return false;
    	}
    	
    	return true;
    }
    
    static void init(){
    	
    	///////////////////////////////////
		// Data
		///////////////////////////////////
		
		//Payers
		payers = TestDataFactory_CS.generatePlans('TestPayer', 1);
		insert payers;
		
		payerUsers = TestDataFactory_CS.createPlanContacts('TestPlan', payers, 1);

		//Patients
		patients = TestDataFactory_CS.generatePatients('TestPatient', 1);
		insert patients;

		///////////////////////////////////
		// Page Landing
		///////////////////////////////////

		//Set the page and authentication cookie
		Test.setCurrentPage(Page.PayerCreateNewPatient_CS.setRedirect(true));
		
		//Log the desired user in
		TestServices_CS.login(payerUsers[0]);
    	
    	//Make Create New Patient Page
    	newPatientClass = new PayerCreateNewPatient_CS();
    	newPatientClass.init();
    } 
}