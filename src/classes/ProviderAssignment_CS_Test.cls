/**
 *  @Description Test for 
 *  @Author Cloud Software LLC
 *  Revision History: 
 *		01/07/2016 - karnold - Added Header.
 *		01/08/2016 - karnold - Formatted code.
 *				Added private access modifier to all methods.
 *				Fixed ProviderAssignment_AssignToManual, ProviderAssignment_AssignToNearest, ProviderAssignment_AssignToPrevious_Patient, 
 *			and ProviderAssignment_AssignToPrevious_PlanPatient by setting the status to Use olde routing and calling an update on the order.
 */
@isTest
private class ProviderAssignment_CS_Test {

	static testMethod void ProviderAssignment_AssignToPrevious_PlanPatient() {
		CustomSettingServices.clearAlgorithmRouting = new Clear_Algorithm_Routing__c(Use_New_Algorithm__c = false);

		//Provider Account
		List<Account> testProviders = TestDataFactory_CS.generateProviders('testAcct', 2);
		for (Account p : testProviders) {
			if (p.Name.contains('1')) {
				p.Corporate_Geolocation__Latitude__s = 35;
				p.Corporate_Geolocation__Longitude__s = - 110;
			}
			p.Provider_Rank__c = 'Preferred';
			p.Locale__c = 'US/Eastern';
		}
		insert testProviders;

		Account assignedProvider;
		Account nearestProvider;
		for (Account p : testProviders) {
			if (p.Name.contains('0')) {
				assignedProvider = p;
			} else if (p.Name.contains('1')) {
				nearestProvider = p;
			}
		}

		//Plan patient
		List<Account> testPlans = TestDataFactory_CS.generatePlans('testPlan', 1);
		insert testPlans;

		List<Plan_Patient__c> testPlanPatients = TestDataFactory_CS.generatePlanPatientsPlanOnly(testPlans[0].Id, 1);
		insert testPlanPatients;

		//old Order where status = 'Manually Assigned' and Provider = provider account and plan patient = plan patient
		//new order where status = new and plan patient = old order plan patient   
		List<Order__c> testOrders = TestDataFactory_CS.generateOrders(testPlanPatients[0].Id, null, 2);
		Order__c firstOrder = testOrders[0];
		firstOrder.Provider__c = assignedProvider.Id;
		firstOrder.Status__c = OrderModel_CS.STATUS_PROVIDER_ASSIGNED_MANUAL;
		insert firstOrder;

		Order__c secondOrder = testOrders[1];
		secondOrder.Status__c = OrderModel_CS.STATUS_USE_OLD_ROUTING;
		secondOrder.Geolocation__Latitude__s = 34;
		secondOrder.Geolocation__Longitude__s = - 110;
		insert secondOrder;

		test.startTest();
		update secondOrder;
		test.stopTest();

		system.assertEquals(assignedProvider.Id, [SELECT Provider__c FROM Order__c WHERE Id = :secondOrder.Id].Provider__c);
	}

	static testMethod void ProviderAssignment_AssignToPrevious_Patient() {
		//Provider Account
		List<Account> testProviders = TestDataFactory_CS.generateProviders('testAcct', 2);
		for (Account p : testProviders) {
			if (p.Name.contains('1')) {
				p.Corporate_Geolocation__Latitude__s = 35;
				p.Corporate_Geolocation__Longitude__s = - 110;
			}
			p.Provider_Rank__c = 'Preferred';
			p.Locale__c = 'US/Eastern';
		}
		insert testProviders;

		Account assignedProvider;
		Account nearestProvider;
		for (Account p : testProviders) {
			if (p.Name.contains('0')) {
				assignedProvider = p;
			} else if (p.Name.contains('1')) {
				nearestProvider = p;
			}
		}

		//Plan patient
		List<Patient__c> testPatients = TestDataFactory_CS.generatePatients('testPatient', 1);
		insert testPatients;

		List<Account> testPlans = TestDataFactory_CS.generatePlans('testPlan', 2);
		insert testPlans;

		Map<Id, List<Patient__c>> planIdToPatients = new Map<Id, List<Patient__c>> ();
		planIdToPatients.put(testPlans[0].Id, new List<Patient__c> { testPatients[0] });
		planIdToPatients.put(testPlans[1].Id, new List<Patient__c> { testPatients[0] });

		List<Plan_Patient__c> testPlanPatients = TestDataFactory_CS.generatePlanPatients(planIdToPatients);
		insert testPlanPatients;

		//old Order where status = 'Manually Assigned' and Provider = provider account and plan patient = plan patient
		//new order where status = new and plan patient = old order plan patient        
		List<Order__c> testOrders = TestDataFactory_CS.generateOrders(null, null, 2);
		Order__c firstOrder = testOrders[0];
		firstOrder.Provider__c = assignedProvider.Id;
		firstOrder.Status__c = OrderModel_CS.STATUS_PROVIDER_ASSIGNED_MANUAL;
		firstOrder.Plan_Patient__c = testPlanPatients[0].Id;
		insert firstOrder;

		Order__c secondOrder = testOrders[1];
		secondOrder.Status__c = OrderModel_CS.STATUS_USE_OLD_ROUTING;
		secondOrder.Geolocation__Latitude__s = 34;
		secondOrder.Geolocation__Longitude__s = - 110;
		secondOrder.Plan_Patient__c = testPlanPatients[1].Id;
		insert secondOrder;

		test.startTest();
		update secondOrder;
		test.stopTest();

		system.assertEquals(assignedProvider.Id, [SELECT Provider__c FROM Order__c WHERE Id = :secondOrder.Id].Provider__c);
	}

	static testMethod void ProviderAssignment_AssignToNearest() {
		//Provider Account
		List<Account> testProviders = TestDataFactory_CS.generateProviders('testAcct', 2);
		for (Account p : testProviders) {
			if (p.Name.contains('1')) {
				p.Corporate_Geolocation__Latitude__s = 35;
				p.Corporate_Geolocation__Longitude__s = - 110;
			}
			p.Provider_Rank__c = 'Preferred';
			p.Locale__c = 'US/Eastern';
		}
		insert testProviders;

		Account assignedProvider;
		Account nearestProvider;
		for (Account p : testProviders) {
			if (p.Name.contains('0')) {
				assignedProvider = p;
			} else if (p.Name.contains('1')) {
				nearestProvider = p;
				system.debug('Lat : ' + p.Corporate_Geolocation__Latitude__s);
				system.debug('Lng : ' + p.Corporate_Geolocation__Longitude__s);
				system.debug('Type__c : ' + p.Type__c);
				system.debug('Provider Rank : ' + p.Provider_Rank__c);
			}
		}
		system.debug([SELECT Id, Corporate_Geolocation__Latitude__s, Corporate_Geolocation__Longitude__s FROM Account WHERE Type__c = 'Provider' AND Provider_Rank__c = 'Preferred' AND Corporate_Geolocation__Latitude__s != null AND Corporate_Geolocation__Longitude__s != null ORDER BY DISTANCE(Corporate_Geolocation__c, GEOLOCATION(34.000000000000000, - 110.000000000000000), 'mi')].size());

		List<Order__c> testOrders = TestDataFactory_CS.generateOrders(null, null, 1);
		Order__c testOrder = testOrders[0];
		testOrder.Status__c = OrderModel_CS.STATUS_USE_OLD_ROUTING;
		testOrder.Geolocation__Latitude__s = 34;
		testOrder.Geolocation__Longitude__s = - 110;
		insert testOrder;
		
		test.startTest();
		update testOrder;
		test.stopTest();

		system.assertEquals(nearestProvider.Id, [SELECT Provider__c FROM Order__c WHERE Id = :testOrder.Id].Provider__c);
	}

	private static testMethod void ProviderAssignment_AssignToManual() {
		CustomSettingServices.clearAlgorithmRouting = new Clear_Algorithm_Routing__c(Use_New_Algorithm__c = false);

		List<Order__c> testOrders = TestDataFactory_CS.generateOrders(null, null, 1);
		Order__c testOrder = testOrders[0];
		testOrder.Status__c = OrderModel_CS.STATUS_USE_OLD_ROUTING;
		testOrder.Geolocation__Latitude__s = 34;
		testOrder.Geolocation__Longitude__s = - 110;
		insert testOrder;

		test.startTest();
		update testOrder;
		test.stopTest();

		system.assertEquals(OrderModel_CS.STATUS_MANUALLY_ASSIGN_PROVIDER, [SELECT Status__c FROM Order__c WHERE Id = :testOrder.Id].Status__c);
	}

	private static testMethod void ProviderNotClearEnabled() {
		//Provider Account
		List<Account> testProviders = TestDataFactory_CS.generateProviders('testAcct', 2);
		for (Account p : testProviders) {
			if (p.Name.contains('1')) {
				p.Corporate_Geolocation__Latitude__s = 35;
				p.Corporate_Geolocation__Longitude__s = - 110;
			}
			p.Provider_Rank__c = 'Preferred';
			p.Locale__c = 'US/Eastern';
		}
		insert testProviders;

		Account assignedProvider;
		Account nearestProvider;
		for (Account p : testProviders) {
			if (p.Name.contains('0')) {
				assignedProvider = p;
			} else if (p.Name.contains('1')) {
				nearestProvider = p;
			}
		}

		//Plan patient
		List<Patient__c> testPatients = TestDataFactory_CS.generatePatients('testPatient', 1);
		insert testPatients;

		List<Account> testPlans = TestDataFactory_CS.generatePlans('testPlan', 2);
		insert testPlans;

		Map<Id, List<Patient__c>> planIdToPatients = new Map<Id, List<Patient__c>> ();
		planIdToPatients.put(testPlans[0].Id, new List<Patient__c> { testPatients[0] });
		planIdToPatients.put(testPlans[1].Id, new List<Patient__c> { testPatients[0] });

		List<Plan_Patient__c> testPlanPatients = TestDataFactory_CS.generatePlanPatients(planIdToPatients);
		insert testPlanPatients;

		//old Order where status = 'Manually Assigned' and Provider = provider account and plan patient = plan patient
		//new order where status = new and plan patient = old order plan patient
		test.startTest();
		List<Order__c> testOrders = TestDataFactory_CS.generateOrders(null, null, 2);
		Order__c firstOrder = testOrders[0];
		firstOrder.Provider__c = assignedProvider.Id;
		firstOrder.Status__c = OrderModel_CS.STATUS_PROVIDER_ASSIGNED_MANUAL;
		firstOrder.Plan_Patient__c = testPlanPatients[0].Id;
		insert firstOrder;

		//Set the assigned provider as not Clear enabled
		assignedProvider.Clear_Enabled__c = false;
		update assignedProvider;

		Order__c secondOrder = testOrders[1];
		secondOrder.Status__c = OrderModel_CS.STATUS_NEW;
		secondOrder.Geolocation__Latitude__s = 34;
		secondOrder.Geolocation__Longitude__s = - 110;
		secondOrder.Plan_Patient__c = testPlanPatients[1].Id;
		insert secondOrder;
		test.stopTest();

		//Assert that the order was not assigned the provider who is not Clear enabled
		system.assertNotEquals(AssignedProvider.Id, [SELECT Provider__c FROM Order__c WHERE Id = :secondOrder.Id].Provider__c);
	}

}