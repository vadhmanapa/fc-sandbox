public with sharing class PublicKnowledgeVideoRedirectController {

	public static final String ENDPOINT_DOMAIN = 'https://api.litmos.com/';
	public static final String ENDPOINT_PATH = 'v1.svc/users/';
	public static final String USERNAME = 'ABogarin@AccessIntegra.com';
	public static final String API_KEY = 'F0ECD3DC-0449-4485-B5F4-D25CBC48793E';
	public static final String SOURCE = 'QueHelpCenter';

	public String link {get; set;}

	public PublicKnowledgeVideoRedirectController(){}
	
	public PageReference redirectToVideo(){
		//First get the course from the page params
		Map<String, String> params = ApexPages.currentPage().getParameters();
		String course; 
		if (params.containsKey('course')){
			course = params.get('course');
		}
		if (String.isBlank(course)){
			PageUtils.addError('The Course Id is incomplete.');
			return null;
		}
		
		if (!Litmos_Settings__c.getAll().containsKey('Defaults')){
			PageUtils.addError('There is no custom setting containing the Litmos information.  There must be a Litmos Setting' +
							   ' record named "Defaults" in order for redirect to be successful.');
			return null;
		}
		
		Litmos_Settings__c litmosSet = Litmos_Settings__c.getInstance('Defaults');
		
		//Send the request to litmos for authentication
		HttpRequest req = new HttpRequest();
		req.setMethod('GET');
		//req.setEndpoint(ENDPOINT_DOMAIN + ENDPOINT_PATH + USERNAME + '?apikey=' + API_KEY + '&source=' + SOURCE);
		req.setEndpoint(ENDPOINT_DOMAIN + ENDPOINT_PATH + litmosSet.Username__c + '?apikey=' + litmosSet.API_Key__c + '&source=' + SOURCE);
		Http http = new Http();
		HttpResponse resp = http.send(req);
		String body = resp.getBody();
		Integer statusCode = resp.getStatusCode();
		if(statusCode != 200 && statusCode != 201) {
			PageUtils.addError('Response Status: ' + statusCode + '\n' + resp.getStatus());
			return null;
		}
		link = body.substringAfter('<LoginKey>').subStringBefore('</LoginKey>');
		link += '&titlebar=false&c=';
		link += course;
		return new PageReference(link);
	}
	
}