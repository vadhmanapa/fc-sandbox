public with sharing class TaskTriggerDispatcher {
	
	public static void afterInsert(List<Task> nList) {
		ShoreTelUtils.updateCallLogs(nList);
	}
	
	public static void afterUpdate(List<Task> nList) {}
}