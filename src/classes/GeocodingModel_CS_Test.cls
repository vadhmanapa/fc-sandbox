@isTest
private class GeocodingModel_CS_Test {

    static testMethod void GeocodingModelTest_CompressedAddress() {
        GeocodingModel_CS geoModel = new GeocodingModel_CS();
        geoModel.RecordId = 'testId';
        geoModel.street = '111 testStreet';
        geoModel.city = 'test';
        geoModel.state = 'TE';
        geoModel.postalCode = '22222';
        geoModel.country = 'US';
        geoModel.lat = 1.12345;
        geoModel.lng = -1.12345;        
        geoModel.travelTime = 1;
        String compressedAddress = '111 testStreet, test, TE 22222, US';
        system.assertEquals(compressedAddress.remove(' '), geoModel.getCompressedAddress());
    }
}