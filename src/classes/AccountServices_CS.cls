public virtual without sharing class AccountServices_CS {
	
	protected Contact c;
	public Account cAccount {get;set;}
	
	//TODO: Refactor pass in accountID
	public AccountServices_CS(String cId){
		
		if(cId != null && cId != ''){	
			
			try{
				c = [Select Id, Entity__c, Name, FirstName, LastName, Email, Birthdate, Profile__c From Contact Where Id = :cId];				
			}catch(Exception e){
				//Todo: improve error message
				throw ((e instanceOf ApplicationException) ? e : new ApplicationException('Invalid User Id: ' + cId, e));
			}//End TryCatch
			
			try{
				cAccount = [
					select 
						Id, 
						Name, 
						Type__c,
						AccountNumber,
						BillingStreet, 
						BillingCity, 
						BillingState, 
						BillingPostalCode, 
						BillingCountry,
                    	ShippingStreet, 
						ShippingCity, 
						ShippingState, 
						ShippingPostalCode, 
						ShippingCountry, 
						Phone, 
						Fax,
						Super_User__c,
                    	Go_Live_Date__c
					from Account 
					where Id = :c.Entity__c];
			}catch(Exception e){
				throw ((e instanceOf ApplicationException) ? e : new ApplicationException('Invalid Account Id: ' + c.Entity__c, e));
			}//End TryCatch
			
			
			
		}//End getContact and Account		
	}//End Constructor

	public static ApexPages.Standardsetcontroller getProviders(){
		
		SoqlBuilder query = new SoqlBuilder()
				.selectx('Name')
				.selectx('Phone')
				.selectx('ShippingCity')
				.selectx('ShippingStreet')
				.selectx('ShippingState')
				.selectx('ShippingPostalCode')
				.selectx('ShippingCountry')		
				.fromx('Account')
				.wherex(new FieldCondition('Type__c').equals('Provider'))
				.limitx(10000);
		
		return new ApexPages.Standardsetcontroller(Database.getQueryLocator(query.toSoql()));		
	}
	
	public static ApexPages.Standardsetcontroller getPreferredProviders(){
		
		SoqlBuilder query = new SoqlBuilder()
				.selectx('Name')
				.selectx('Phone')
				.selectx('ShippingCity')
				.selectx('ShippingStreet')
				.selectx('ShippingState')
				.selectx('ShippingPostalCode')
				.selectx('ShippingCountry')	
				.selectx('Clear_Enabled__c')	
				.fromx('Account')
				.wherex(new AndCondition()
					.add(new FieldCondition('Status__c').equals('Active'))
					.add(new FieldCondition('Type__c').equals('Provider'))
					.add(new FieldCondition('Provider_Rank__c').equals('Preferred'))
					.add(new FieldCondition('Clear_Enabled__c').equals(true)))
				.limitx(10000);
		
		System.debug(query.toSoql());
		
		return new ApexPages.Standardsetcontroller(Database.getQueryLocator(query.toSoql()));		
	}

	public static ApexPages.Standardsetcontroller searchProviders(String searchTerm){
	
		SoqlBuilder query = new SoqlBuilder()
				.selectx('Name')
				.selectx('Phone')
				.selectx('ShippingCity')
				.selectx('ShippingStreet')
				.selectx('ShippingState')
				.selectx('ShippingPostalCode')
				.selectx('ShippingCountry')		
				.fromx('Account')
				.wherex(new AndCondition()
					.add(new FieldCondition('Type__c').equals('Provider'))
					.add(new OrCondition()
						.add(new FieldCondition('Name').likex('%' + searchTerm + '%'))
						.add(new FieldCondition('ShippingStreet').likex('%' + searchTerm + '%'))
						.add(new FieldCondition('ShippingCity').likex('%' + searchTerm + '%'))
						.add(new FieldCondition('ShippingState').likex('%' + searchTerm + '%'))
						.add(new FieldCondition('ShippingPostalCode').likex('%' + searchTerm + '%'))))
				.limitx(10000);
		
		return new ApexPages.Standardsetcontroller(Database.getQueryLocator(query.toSoql()));	
		
	}
	
	public static ApexPages.Standardsetcontroller searchActiveProviders(String searchTerm){
	
		SoqlBuilder query = new SoqlBuilder()
				.selectx('Name')
				.selectx('Phone')
				.selectx('ShippingCity')
				.selectx('ShippingStreet')
				.selectx('ShippingState')
				.selectx('ShippingPostalCode')
				.selectx('ShippingCountry')	
				.selectx('Status__c')
				.selectx('Clear_Enabled__c')	
				.fromx('Account')
				.wherex(new AndCondition()
					.add(new FieldCondition('Status__c').equals('Active'))
					.add(new FieldCondition('Type__c').equals('Provider'))
					.add(new FieldCondition('Clear_Enabled__c').equals(true))
					.add(new OrCondition()
						.add(new FieldCondition('Name').likex('%' + searchTerm + '%'))
						.add(new FieldCondition('ShippingStreet').likex('%' + searchTerm + '%'))
						.add(new FieldCondition('ShippingCity').likex('%' + searchTerm + '%'))
						.add(new FieldCondition('ShippingState').likex('%' + searchTerm + '%'))
						.add(new FieldCondition('ShippingPostalCode').likex('%' + searchTerm + '%'))))
				.limitx(10000);
		
		System.debug('query: ' + query.toSoql());
		
		return new ApexPages.Standardsetcontroller(Database.getQueryLocator(query.toSoql()));	
		
	}
	
}//End AccountServices_CS