public with sharing class CreateCATFromDocuSign_IP {
	
	public CreateCATFromDocuSign_IP() {
		
	}

	public static void createNewCAT(List<dsfs__DocuSign_Status__c> newContracts){

		// get all the record types available in CAT
		Map<String, Id> rTypeMap = new Map<String, Id>();
		List<CAT__c> credHolder = new List<CAT__c>();
		String recordTypeId;
		for(RecordType rt: [Select Id, Name from RecordType where sObjectType = 'CAT__c']) {
				rTypeMap.put(rt.Name, rt.Id);
		}
		System.debug('Number of record type queried'+rTypeMap.size());

		for(dsfs__DocuSign_Status__c doc: newContracts) {
			
			recordTypeId = '';
			if(doc.dsfs__Envelope_Status__c == 'Completed') {
				List<dsfs__DocuSign_Recipient_Status__c> drs = [select id, dsfs__Contact__c from dsfs__DocuSign_Recipient_Status__c where dsfs__Parent_Status_Record__c =: doc.id];
				Id c = null;
				if (!drs.isEmpty()){
					c = drs[0].dsfs__Contact__c;
				}
				dsfs__DocuSign_Status__c ds = [Select Id, (Select Id from Crdentialing_Account_Trails__r) from dsfs__DocuSign_Status__c where Id=: doc.Id];
				// only if the document is received completed, then proceed
				if (ds.Crdentialing_Account_Trails__r.size() == 0){
					if((doc.dsfs__Subject__c).contains('Recredential') || (doc.dsfs__Subject__c).contains('Re-credential') || (doc.dsfs__Subject__c).contains('Re-Credential')) {
						
						// then assign the record type as recredentialing
						credHolder.add(new CAT__c(RecordTypeId = rTypeMap.get('Credentialing Application'), 
												  Account__c = doc.dsfs__Company__c,
												  Application_Completed_Date__c = doc.dsfs__Completed_Date_Time__c,
	                                              Application_Sent_Time__c = doc.dsfs__Sent_Date_Time__c,
												  Credentialing_Status__c = 'Ready for triage',
												  DocuSign_Link__c = doc.Id,
												  Application_Type__c = 'Recredentialing',
												  Provider_Contact__c = c));
					}
					else if ((doc.dsfs__Subject__c).contains('Network Application') || (doc.dsfs__Subject__c).contains('Application Package')) {
						// then assign record type as Initial Credentialing
						credHolder.add(new CAT__c(RecordTypeId = rTypeMap.get('Credentialing Application'), 
												  Account__c = doc.dsfs__Company__c,
												  Application_Completed_Date__c = doc.dsfs__Completed_Date_Time__c,
												  New_Application_Received__c = doc.dsfs__Completed_Date_Time__c,
	                                              Application_Sent_Time__c = doc.dsfs__Sent_Date_Time__c,
												  Credentialing_Status__c = 'Ready for triage',
												  DocuSign_Link__c = doc.Id,
												  Application_Type__c = 'Initial Credentialing',
												  Provider_Contact__c = c));
					}
				}
			}
		}

		// create a new CAT record
		if(credHolder.size() > 0) {
			try {
				insert credHolder;
			} catch(Exception e) {
				System.debug(e.getMessage());
			}
		}
	}
}