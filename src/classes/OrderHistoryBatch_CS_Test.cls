@isTest
private class OrderHistoryBatch_CS_Test { 

	//Fixes:
	static testMethod void testBatch() {
		
		//Create test data

		//1. Provider Accounts (Id)
		List<Account> providers = TestDataFactory_CS.generateProviders('testAccount', 3);
		insert providers;
		System.debug(providers[0].recordTypeId);

		//2. Provider Contacts (Id, AccountId)
		List<Contact> contacts = TestDataFactory_CS.generateProviderContacts('Admin', providers, 2);
		insert contacts;
		
		//3. Plan Patient for Orders
		List<Patient__c> patients = TestDataFactory_CS.generatePatients('testLast', 1);
		insert patients;
		List<Account> plans = TestDataFactory_CS.generatePlans('testPlan', 1);
		insert plans;
		Map<Id, List<Patient__c>> planToPatient = new Map<Id, List<Patient__c>>();
    	planToPatient.put(plans[0].Id, patients);
    	List<Plan_Patient__c> planPatients = TestDataFactory_CS.generatePlanPatients(planToPatient);
    	insert planPatients; 

		//Datetime constants
		Datetime dateTime1 = Datetime.valueOfGmt('2000-01-01 00:00:00');
		Datetime dateTime2 = Datetime.valueOfGmt('2000-01-01 12:00:00');
		Datetime dateTime2a = Datetime.valueOfGmt('2000-01-01 15:00:00');
		Datetime dateTime3 = Datetime.valueOfGmt('2000-01-02 00:00:00');
		Datetime dateTime4 = Datetime.valueOfGmt('2000-01-03 00:00:00');
		Datetime dateTime5 = Datetime.valueOfGmt('2000-01-04 00:00:00');
		Datetime dateTime6 = Datetime.valueOfGmt('2000-01-04 12:00:00');

		//4. Orders (Id, Order_History_JSON)
		List<Order__c> orders = TestDataFactory_CS.generateOrders(planPatients[0].Id, providers[0].Id, 2);
		for (Order__c o : orders) {
			o.Accepted_Date__c = dateTime6;
		}
		insert orders;

		//5. Order Assignments (Assigned_At__c, Order__c, Provider__c, Unassigned_At__c)
		
		List<Order_Assignment__c> orderAssignments = new List<Order_Assignment__c>();
		orderAssignments.add(TestDataFactory_CS.generateOrderAssignments(orders[0].Id, providers[0].Id, dateTime1, dateTime2a));
		orderAssignments.add(TestDataFactory_CS.generateOrderAssignments(orders[0].Id, providers[1].Id, dateTime2a, dateTime3));
		orderAssignments.add(TestDataFactory_CS.generateOrderAssignments(orders[0].Id, providers[2].Id, dateTime3, dateTime5));
		orderAssignments.add(TestDataFactory_CS.generateOrderAssignments(orders[1].Id, providers[0].Id, dateTime1, dateTime3));
		orderAssignments.add(TestDataFactory_CS.generateOrderAssignments(orders[1].Id, providers[1].Id, dateTime2, dateTime4));
		orderAssignments.add(TestDataFactory_CS.generateOrderAssignments(orders[1].Id, providers[1].Id, dateTime2, dateTime4));
		orderAssignments.add(TestDataFactory_CS.generateOrderAssignments(orders[1].Id, providers[2].Id, dateTime4, null));

		insert orderAssignments;

		List<OrderHistoryJSONModel_CS> order0HistoryList = new List<OrderHistoryJSONModel_CS>();
		List<OrderHistoryJSONModel_CS> order1HistoryList = new List<OrderHistoryJSONModel_CS>();

		//Order history cases to test:
		//Success
		order0HistoryList.add(generateHistoryJSON(dateTime2, 'Provider Accepted', contacts[0], 'testAccount0'));
		//Status != Provider Accepted
		order0HistoryList.add(generateHistoryJSON(dateTime3, 'Cancelled', contacts[2], 'testAccount1'));
		//Account mismatch - should be testAccount2
		order0HistoryList.add(generateHistoryJSON(dateTime4, 'Provider Accepted', contacts[1], 'testAccount0'));
		//None in window - failure border condition
		order1HistoryList.add(generateHistoryJSON(dateTime1, 'Provider Accepted', contacts[0], 'testAccount0'));
		//Multiple in window - success border condition
		order1HistoryList.add(generateHistoryJSON(dateTime4, 'Provider Accepted', contacts[3], 'testAccount1'));
		//Success - no unassigned time
		order1HistoryList.add(generateHistoryJSON(dateTime6, 'Provider Accepted', contacts[5], 'testAccount2'));

		//Add histories to orders
		orders[0].Order_History_JSON__c = JSON.serialize(order0HistoryList);
		orders[1].Order_History_JSON__c = JSON.serialize(order1HistoryList);
		update orders;


		//Run batch
		Test.startTest();
		OrderHistoryBatch_CS batch = new OrderHistoryBatch_CS();
		Database.executeBatch(batch);
		Test.stopTest();

		//Test error messages
		Map<Id, Order__c> queriedOrders = new Map<Id, Order__c>([
			SELECT Order_History_Parse_Failed__c, Order_History_Parse_Errors__c
			FROM Order__c
		]);
		
		System.assert(true, queriedOrders.get(orders[0].Id).Order_History_Parse_Failed__c);
		System.assertEquals('Account conflict for model with timestamp: ' + order0HistoryList[2].timeStamp, queriedOrders.get(orders[0].Id).Order_History_Parse_Errors__c);

		System.assertEquals(false, queriedOrders.get(orders[1].Id).Order_History_Parse_Failed__c);

		//Query for updated order assignments
		Map<Id, Order_Assignment__c> queriedOrderAssignments = new Map<Id, Order_Assignment__c>([
			SELECT Accepted_At__c
			FROM Order_Assignment__c
		]);

		//Make assertions
		//Success - with unassigned time
		System.assertEquals(dateTime2, queriedOrderAssignments.get(orderAssignments[0].Id).Accepted_At__c);
		//Status != Provider Accepted
		System.assertEquals(null, queriedOrderAssignments.get(orderAssignments[1].Id).Accepted_At__c);
		//None in window - failure border condition
		System.assertEquals(null, queriedOrderAssignments.get(orderAssignments[3].Id).Accepted_At__c);
		//Multiple in window - success border condition
		System.assertEquals(dateTime4, queriedOrderAssignments.get(orderAssignments[4].Id).Accepted_At__c);
		System.assertEquals(dateTime4, queriedOrderAssignments.get(orderAssignments[5].Id).Accepted_At__c);
		//Success - no unassigned time
		System.assertEquals(dateTime6, queriedOrderAssignments.get(orderAssignments[6].Id).Accepted_At__c);
	}

	private static OrderHistoryJSONModel_CS generateHistoryJSON(Datetime timestamp, String orderStatus, Contact c, String accountName) { 
		OrderHistoryJSONModel_CS historyJSON = new OrderHistoryJSONModel_CS();
		historyJSON.timestamp = timestamp;
		historyJSON.orderStatus = orderStatus;
		historyJSON.contactName = c.Name;
		historyJSON.contactId = c.Id;
		historyJSON.accountName = accountName;
		return historyJSON;
	}
}