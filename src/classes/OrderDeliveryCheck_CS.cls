/**
 * @description Class will assign recurring orders within 3 days of delivery.
 * @author Cloud Software, LLC 
 *	Revision History
 *		04/06/2016 - karnold - ICA-582 - Changed conditions of if to make it so an order that 
 *			was assigned a provider before the three daays is not reassigned by this running
 *		07/28/2016 - saguirre - IMOR-58 - Changed class to run as Batchable
 **/
public class OrderDeliveryCheck_CS implements Database.Batchable<sObject> {
	private String query;
	public OrderDeliveryCheck_CS() {
		//Retrieve any recurring orders with a delivery window beginning within the next three days
		query =
			'SELECT ' +
				'Status__c, ' +
				'Original_Order__r.Provider__c, ' +
				'Provider__c ' +
			'FROM Order__c ' +
			'WHERE Status__c = \''+ OrderModel_CS.STATUS_RECURRING + '\'' +
			'AND Delivery_Window_Begin__c = NEXT_N_DAYS : 3 ';
		
	}

	public void setQuery(String query) {
		this.query = query;
	}

	/**
	* @description starts the batch, returns the Orders specified in query variable
	*/
	public Database.QueryLocator start(Database.BatchableContext context) {
		return Database.getQueryLocator(query);
	}
	
	/**
	* @description updates all the Order statuses in "scope" based on if their Provider was assinged.
	*/
	public void execute(Database.BatchableContext context, List<Order__c> scope) {
		for (Order__c o : scope) {
			if (o.Provider__c != null) { 
				o.Status__c = OrderModel_CS.STATUS_PROVIDER_ASSIGNED_MANUAL;
			} else if (o.Original_Order__r.Provider__c != null) {
				o.Provider__c = o.Original_Order__r.Provider__c;
				o.Status__c = OrderModel_CS.STATUS_PROVIDER_ASSIGNED_AUTO;
			} else {
				o.Status__c = OrderModel_CS.STATUS_MANUALLY_ASSIGN_PROVIDER;
			}
		}
		update scope;
	}
	
	/**
	* @description No finish actions at this time.
	*/
	public void finish(Database.BatchableContext context) {}

}