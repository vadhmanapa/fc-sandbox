/**
 *  @Description Batch to transfer Provider Location objects from the old object to the new Provider_Location__c object.
 *		To be run after Provider_Location__c deploy.
 *  @Author Cloud Software LLC
 *  Revision History: 
 *		12-22-2015 - karnold - Added comments, changed call to generic
 *
 */
public class ProviderStateIdentifierBatch_CS implements Database.Batchable<SObject>{ 

	String queryString;

	public ProviderStateIdentifierBatch_CS(String query) {
		this.queryString = query;
	}

	public ProviderStateIdentifierBatch_CS() {
		
		SoqlBuilder query = new SoqlBuilder()
			.selectx('Id')
			.selectx('Provider_Location__c')
			.fromx('State_Identifier__c')
			.wherex(new FieldCondition('Provider_Location_New__c').equals(null));
			
			queryString = query.toSoql();
	}

	public Database.QueryLocator start(Database.BatchableContext context) {
		
		return Database.getQueryLocator(queryString);

	}

	public void execute(Database.BatchableContext context, List<State_Identifier__c> scope) {
		List<State_Identifier__c> stateIdentList = (List<State_Identifier__c>) ProviderLocationTransferServices.updateObjectProviderLocation(scope);
		update stateIdentList;
	}

	public void finish(Database.BatchableContext context) {

	}

}