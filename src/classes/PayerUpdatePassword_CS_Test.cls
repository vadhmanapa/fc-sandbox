@isTest
private class PayerUpdatePassword_CS_Test {
	public static Account payer;
	public static Contact user;
	
    static testMethod void PayerUpdatePassword_CS_Test() {
        init();
        Test.setCurrentPage(Page.PayerUpdatePassword_CS);
        TestServices_CS.login(user);
        
        PayerUpdatePassword_CS cont = new PayerUpdatePassword_CS();
        cont.init();
        system.assertEquals(true, cont.authenticated);
        system.assertEquals('', cont.newPasswordMessage);
        
    	cont.newPassword = PasswordServices_CS.randomPassword(10);
    	cont.newPasswordConfirm = cont.newPassword;
    	
    	test.startTest();
    	cont.changePassword();
    	test.stopTest();
		
		system.assertEquals('Password successfully changed', cont.newPasswordMessage);    	
    }
    
    static testMethod void PayerUpdatePassword_CS_initNotAuthenticated(){
    	init();
    	Test.setCurrentPage(Page.PayerUpdatePassword_CS);
    	
    	PayerUpdatePassword_CS cont = new PayerUpdatePassword_CS();
    	cont.init();
    }
    
    static testMethod void PayerUpdatePassword_CS_initWrongAccountType(){
    	init();
    	Account provider = TestDataFactory_CS.generateProviders('TestProvider', 1)[0];
    	insert provider;
    	user.Entity__c = provider.Id;
    	update user;
    	
    	Test.setCurrentPage(Page.PayerUpdatePassword_CS);
    	TestServices_CS.login(user);
    	
    	PayerUpdatePassword_CS cont = new PayerUpdatePassword_CS();
    	cont.init();
    }
    
    static testMethod void PayerUpdatePassword_CS_changePasswordNullUser(){
    	Test.setCurrentPage(Page.PayerUpdatePassword_CS);
    	
    	PayerUpdatePassword_CS cont = new PayerUpdatePassword_CS();
    	cont.changePassword();
    }
    
    static testMethod void PayerUpdatePassword_CS_changePasswordNull(){
    	init();
    	Test.setCurrentPage(Page.PayerUpdatePassword_CS);
    	TestServices_CS.login(user);
    	
    	PayerUpdatePassword_CS cont = new PayerUpdatePassword_CS();
    	cont.changePassword();
    	system.assertEquals('Error: No password data found', cont.newPasswordMessage);
    }
    
    static testMethod void PayerUpdatePassword_CS_changePasswordMissmatch(){
    	init();
    	Test.setCurrentPage(Page.PayerUpdatePassword_CS);
    	TestServices_CS.login(user);
    	
    	PayerUpdatePassword_CS cont = new PayerUpdatePassword_CS();
    	cont.newPassword = 'testNew1';
    	cont.newPasswordConfirm = 'testNew2';
    	cont.changePassword();
    	system.assertEquals('Error: New passwords don\'t match.', cont.newPasswordMessage);
    }
    
    static testMethod void PayerUpdatePassword_CS_changePasswordFail(){
    	init();
    	Test.setCurrentPage(Page.PayerUpdatePassword_CS);
    	TestServices_CS.login(user);
    	
    	PayerUpdatePassword_CS cont = new PayerUpdatePassword_CS();
    	cont.newPassword = 'tn';
    	cont.newPasswordConfirm = 'tn';
    	cont.changePassword();
    	system.assertEquals('Error: Password change failed.', cont.newPasswordMessage);
    }
    
    public static void init(){
    	List<Account> initAccts = TestDataFactory_CS.generatePlans('TestPayer', 1);
    	Payer = initAccts[0];
    	insert Payer;
    	
    	user = TestDataFactory_CS.createPlanContacts('Admin', initAccts, 1)[0];    	
    }
}