@isTest
private class TestClaimsToARAG_IP {
	
	@isTest static void test_newClaims() {
		// 
		User testUser = new User(FirstName = 'test', LastName = 'check', Username = 'testcheck@IntegraTest.com', Email = 'test@test.com', Alias = 'test',
                                                       CommunityNickname = 'testforlog', TimeZoneSidKey = 'America/Los_Angeles', EmailEncodingKey = 'ISO-8859-1',
                                                       ProfileId = '00e300000028QgNAAU', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
		insert testUser;
		String userId = testUser.Id;

		Account testProvider = new Account(Name = 'Test Claim Provider', Type_Of_Provider__c = 'DME');
    	insert testProvider;
    	String providerId = testProvider.Id;

    	Account testPayor = new Account(Name = 'Test Claim Payor', Type_Of_Provider__c = 'DME');
    	insert testPayor;
    	String payorId = testPayor.Id;

    	Claims_Inquiry__c testClaim = new Claims_Inquiry__c (Provider__c = providerId, Health_Plan__c = payorId, Bill_Number__c = '666985',
    														Link_to_Bill__c = 'test@test.com', Create_ARAG__c = false, Assigned_To__c = userId);
    	insert testClaim;
    	String claimId = testClaim.Id;
    	String billNum = testClaim.Bill_Number__c;

        //System.assertEquals(true, testClaim.Create_ARAG__c, 'The insert trigger ran and updated Create ARAG to true');

    	/*Claims_Inquiry__c searchClaim = [Select Id, Create_ARAG__c from Claims_Inquiry__c where Id =: claimId];
    	searchClaim.Create_ARAG__c = true;
    	update searchClaim;*/

    	Claims_Inquiry__c searchClaim1 = [Select Id, Bill_Related__r.Id from Claims_Inquiry__c where Id =:claimId];

    	 String aragId = searchClaim1.Bill_Related__r.Id;

    	List<ARAG__c> claimLog = [ SELECT Id, Bill__c FROM ARAG__c WHERE Id =: aragId LIMIT 1];
    	for( ARAG__c log : claimLog ){
        	 System.assertEquals(1, claimLog.size(), 'The ARAG was created');
        	// System.assertEquals(billNum, log.Bill__c, 'The record was mapped to correct ARAG');
    	}
        
        ARAG__c testARAG = new ARAG__c (Payor_Family_text__c = 'Test Payor', Provider__c = 'Test Provider', Bill__c = '09876');
        insert testARAG;
        String newARAGId = testARAG.Id;

        Claims_Inquiry__c testClaim1 = new Claims_Inquiry__c (Provider__c = providerId, Health_Plan__c = payorId,
    														Link_to_Bill__c = 'test@test.com', Create_ARAG__c = false, Assigned_To__c = userId);
        insert testClaim1;
        String claimId1 = testClaim1.Id;

        testClaim1.Bill_Number__c = '09876';
        update testClaim1;

        String billNum1 = testClaim1.Bill_Number__c;

        
        Claims_Inquiry__c searchClaim2 = [Select Id, Bill_Related__r.Id from Claims_Inquiry__c where Id =:claimId1];
		String aragId1 = searchClaim2.Bill_Related__r.Id;

        List<ARAG__c> claimLog1 = [ SELECT Id, Bill__c FROM ARAG__c WHERE Id =: aragId1 LIMIT 1];
    	for( ARAG__c log : claimLog1 ){
        	 System.assertEquals(1, claimLog1.size(), 'The ARAG was created');
        	 System.assertEquals(billNum1, log.Bill__c, 'The ARAG was mapped to correct existing bill');
        	}


	}		
}