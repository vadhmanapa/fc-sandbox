@isTest
private class CaseNotificationSchedulerTest {

    static testMethod void CaseNotificationScheduler() {
	 	
	 	Test.startTest();
        
        String jobId = CaseNotificationScheduler.schedule();
        
        // Get the information from the CronTrigger API object
      	CronTrigger ct = [
      		SELECT 
      			Id,
      			CronExpression,
      			TimesTriggered,
      			NextFireTime
	         FROM CronTrigger 
	         WHERE Id = :jobId
         ];

		// Verify the expressions are the same
		System.assertEquals(CaseNotificationScheduler.jobSchedule, ct.CronExpression);

		// Verify the job has not run
		System.assertEquals(0, ct.TimesTriggered);

		Test.stopTest();
    }
}