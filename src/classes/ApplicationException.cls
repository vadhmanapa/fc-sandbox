public virtual class ApplicationException extends Exception{
	
	public static boolean USE_APPLICATION_EXCEPTION = true;
	
	public static final List<String> emailList = new List<String>{
		'test@cloudsoftwarellc.com',
		'emunson@cloudsoftwarellc.com',
		'mrockford@cloudsoftwarellc.com'
	};
	
	public ApplicationException(String error, Exception ae, boolean sendEmail) {
		if (sendEmail) {
			emailExceptionToCS(error);
		}
		else {
			this(error,ae);
		}
	}
	
	public ApplicationException(String error, boolean sendEmail){
		if(sendEmail){
			emailExceptionToCS(error);
		}
		else{
			this(error);
		}
	}
	
	public void emailExceptionToCS(String errorMessage) {
		
		if (USE_APPLICATION_EXCEPTION == true) {
			
			Messaging.SingleEmailMessage excEmail = new Messaging.SingleEmailMessage();
			
			//String[] sendTo = new String[1];
			//sendTo[0] = 'test@cloudsoftwarellc.com';
			excEmail.setToAddresses(emailList);
			
			excEmail.setSubject('Exception');
			excEmail.setPlainTextBody(errorMessage + '\n\n' + getStackTraceString());
		}
	}
}