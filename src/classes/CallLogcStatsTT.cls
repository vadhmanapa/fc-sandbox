@isTest
private class CallLogcStatsTT {

   static testMethod void testTrigger() {
      try {
          Call_Log__c o = new Call_Log__c();
          insert o;

          System.assertNotEquals(null, o);
      }
      catch(Exception e) {
          List<Call_Log__c> l = [SELECT Id from Call_Log__c LIMIT 1];
          update l;
          System.assertNotEquals(null, l);
      }
   }
}