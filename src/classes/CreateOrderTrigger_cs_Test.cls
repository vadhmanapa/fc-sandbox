/**
 *  @Description
 *  @Author Cloud Software LLC
 *  Revision History: 
 *		12/23/2015 - karnold - Added header, class was commented out, appears to be deprecated.
 *
 */
@isTest
private class CreateOrderTrigger_cs_Test {
	/*
	public static List<Account> providerList;
	public static List<Account> planList;
	public static List<DME__c> dmeList;
	public static List<Order__c> testOrderList;
	public static Plan_Patient__c planPatient;
	
    static testMethod void CreateOrderTrigger_cs_Test_Insert_Other(){
    	New_Orders__c no = initNewOrders(1)[0];
    	no.CPT_Code_4__c = 'A0003';
    	no.Units_4__c = 2;
    	no.CPT_Code_5__c = 'A0004';
    	no.Units_5__c = 3;
    	insert no;
    	
    	List<Order__c> orderList = [
    		SELECT 
				Id,
				Parent_Provider__c,
				Plan_Patient__c
    		FROM Order__c 
    		WHERE Legacy_Orders__c = :no.Id
		];
		List<DME_Line_Item__c> dmeLIList = [
			SELECT 
				Id,
				Order__c,
				DME__c
			FROM DME_Line_Item__c 
			WHERE Order__c = :orderList[0].Id
		];
    	system.assertEquals(1, orderList.size());
    	system.assertEquals(providerList[0].Id, orderList[0].Parent_Provider__c);
    	system.assertEquals(5, dmeLIList.size());
    	system.assertEquals(5, [SELECT Id FROM DME__c].size());
    	system.assertEquals(1, [SELECT Id FROM DME__c WHERE Name = 'A0003'].size());
    	system.assertEquals(1, [SELECT Id FROM Plan_Patient__c WHERE Id = :orderList[0].Plan_Patient__c].size());
    	system.assertEquals('testPatient0', [SELECT Last_Name__c FROM Plan_Patient__c WHERE Id = :orderList[0].Plan_Patient__c].Last_Name__c);
    }
    
    
    static testMethod void CreateOrderTrigger_cs_Test_Insert_FirstLast(){
    	New_Orders__c no = initNewOrders(1)[0];
    	no.Patient_Name__c = 'First Last';
    	insert no;
    	
    	List<Order__c> orderList = [
    		SELECT 
				Id,
				Parent_Provider__c,
				Plan_Patient__c
    		FROM Order__c 
    		WHERE Legacy_Orders__c = :no.Id
		];
    	
    	system.assertEquals('First', [SELECT First_Name__c FROM Plan_Patient__c WHERE Id = :orderList[0].Plan_Patient__c].First_Name__c);
    	system.assertEquals('Last', [SELECT Last_Name__c FROM Plan_Patient__c WHERE Id = :orderList[0].Plan_Patient__c].Last_Name__c);
    }
    /*
    static testMethod void CreateOrderTrigger_cs_Test_Insert_StatusOne(){
    	New_Orders__c no = initNewOrders(1)[0];
    	no.Status_of_Order__c = 'Additional Information Needed';
    	insert no;
    	
    	List<Order__c> orderList = [
    		SELECT 
				Id,
				Status__c
    		FROM Order__c 
    		WHERE Legacy_Orders__c = :no.Id
		];
    	
    	system.assertEquals('Needs Attention - Incomplete Delivery Information', orderList[0].Status__c);
	}
	static testMethod void CreateOrderTrigger_cs_Test_Insert_StatusTwo(){
    	New_Orders__c no = initNewOrders(1)[0];
    	no.Status_of_Order__c = 'Closed/Not Delivered';
    	insert no;
    	
    	List<Order__c> orderList = [
    		SELECT 
				Id,
				Status__c
    		FROM Order__c 
    		WHERE Legacy_Orders__c = :no.Id
		];
    	
    	system.assertEquals('Needs Attention - Patient Refused - Other', orderList[0].Status__c);
	}
	static testMethod void CreateOrderTrigger_cs_Test_Insert_StatusThree(){
    	New_Orders__c no = initNewOrders(1)[0];
    	no.Status_of_Order__c = 'Delivered';
    	insert no;
    	
    	List<Order__c> orderList = [
    		SELECT 
				Id,
				Status__c
    		FROM Order__c 
    		WHERE Legacy_Orders__c = :no.Id
		];
    	
    	system.assertEquals('Delivered', orderList[0].Status__c);
	}
	static testMethod void CreateOrderTrigger_cs_Test_Insert_StatusFour(){
    	New_Orders__c no = initNewOrders(1)[0];
    	no.Status_of_Order__c = 'In Progress';
    	insert no;
    	
    	List<Order__c> orderList = [
    		SELECT 
				Id,
				Status__c
    		FROM Order__c 
    		WHERE Legacy_Orders__c = :no.Id
		];
    	
    	system.assertEquals('Provider Accepted', orderList[0].Status__c);
	}
	
	static testMethod void CreateOrderTrigger_cs_Test_Insert_StatusFive(){
    	New_Orders__c no = initNewOrders(1)[0];
    	no.Status_of_Order__c = 'Open';
    	insert no;
    	
    	List<Order__c> orderList = [
    		SELECT 
				Id,
				Status__c
    		FROM Order__c 
    		WHERE Legacy_Orders__c = :no.Id
		];
    	
    	system.assertEquals('New', orderList[0].Status__c);
	}
    static testMethod void CreateOrderTrigger_cs_Test_Insert_LastFirst(){
    	New_Orders__c no = initNewOrders(1)[0];
    	no.Patient_Name__c = 'Last, First';
    	insert no;
    	
    	List<Order__c> orderList = [
    		SELECT 
				Id,
				Parent_Provider__c,
				Plan_Patient__c
    		FROM Order__c 
    		WHERE Legacy_Orders__c = :no.Id
		];
    	
    	system.assertEquals('First', [SELECT First_Name__c FROM Plan_Patient__c WHERE Id = :orderList[0].Plan_Patient__c].First_Name__c);
    	system.assertEquals('Last', [SELECT Last_Name__c FROM Plan_Patient__c WHERE Id = :orderList[0].Plan_Patient__c].Last_Name__c);
    }
    
    static testMethod void CreateOrderTrigger_cs_Test_InsertWithUpdatedPlanPatient_FirstLast(){
    	New_Orders__c no = initNewOrders(1)[0];
    	
    	planPatient = TestDataFactory_CS.generatePlanPatientsPlanOnly(planList[0].Id, 1)[0];
    	insert planPatient;
    	
    	no.Patient_Name__c = 'Alicia Summers';
    	no.City__c = 'testCity';
		no.D_O_B__c = Date.valueOf('1990-10-09');
		no.Medicaid_ID__c = 'medicaid';
		no.Medicare_ID__c = 'medicare';
		no.Patient_Phone__c = '9876543212';
		no.State__c = 'UT';
		no.Street__c = '123 W 4th St';
		no.Zip__c = '76543';
    	insert no;
    	
    	List<Order__c> orderList = [
    		SELECT 
				Id,
				Plan_Patient__c
    		FROM Order__c 
    		WHERE Legacy_Orders__c = :no.Id
		];
		List<Plan_Patient__c> planPatientList = [
			SELECT
				Id,
				First_Name__c,
				Last_Name__c
			FROM Plan_Patient__c
			WHERE Id = :planPatient.Id
		];
    	system.assertEquals(planPatient.Id, orderList[0].Plan_Patient__c);
    	//system.assertEquals('Alicia', planPatientList[0].First_Name__c);
    	//system.assertEquals('Summers', planPatientList[0].Last_Name__c);
    }
    
    static testMethod void CreateOrderTrigger_cs_Test_InsertWithUpdatedPlanPatient_LastFirst(){
    	New_Orders__c no = initNewOrders(1)[0];
    	
    	planPatient = TestDataFactory_CS.generatePlanPatientsPlanOnly(planList[0].Id, 1)[0];
    	insert planPatient;
    	
    	no.Patient_Name__c = 'Summer, Alice';
    	insert no;
    	
    	List<Order__c> orderList = [
    		SELECT 
				Id,
				Plan_Patient__c
    		FROM Order__c 
    		WHERE Legacy_Orders__c = :no.Id
		];
		List<Plan_Patient__c> planPatientList = [
			SELECT
				Id,
				First_Name__c,
				Last_Name__c
			FROM Plan_Patient__c
			WHERE Id = :planPatient.Id
		];
    	system.assertEquals(planPatient.Id, orderList[0].Plan_Patient__c);
    	//system.assertEquals('Alice', planPatientList[0].First_Name__c);
    	//system.assertEquals('Summer', planPatientList[0].Last_Name__c);
    }
    
    static testMethod void CreateOrderTrigger_cs_Test_Update_Other(){
    	New_Orders__c no = initUpdateNewOrders(1)[0];
    	Order__c testOrder = testOrderList[0];
		insert testOrder;
		
		DME_Line_Item__c dmeLI = new DME_Line_Item__c(
			DME__c = dmeList[0].Id,
			Order__c = testOrder.Id,
			Quantity__c = 1
		);
		insert dmeLI;
		
		no.CPT_Code_4__c = 'A0003';
		no.Units_4__c = 2;
		no.CPT_Code_5__c = 'A0004';
		no.Units_5__c = 3;
		no.Member_Patient_ID__c = 'id-1';
		update no;
		
		List<Order__c> orderList = [
    		SELECT 
				Id,
				Plan_Patient__c
    		FROM Order__c 
    		WHERE Legacy_Orders__c = :no.Id
		];
		
		List<DME_Line_Item__c> dmeLIList = [
			SELECT 
				Id,
				Order__c,
				DME__c
			FROM DME_Line_Item__c 
			WHERE Order__c = :orderList[0].Id
		];
    	system.assertEquals(1, orderList.size());
    	system.assertEquals(1, [SELECT Id FROM Order__c].size());
    	system.assertEquals(5, dmeLIList.size());
    	system.assertEquals(5, [SELECT Id FROM DME__c].size());
    	system.assertEquals(1, [SELECT Id FROM DME__c WHERE Name = 'A0003'].size());
    	system.assertEquals(2, [SELECT Id FROM Plan_Patient__c].size());
    	system.assertEquals('id-1', [SELECT ID_Number__c FROM Plan_Patient__c WHERE Id = :orderList[0].Plan_Patient__c].ID_Number__c);
    }
    static testMethod void CreateOrderTrigger_cs_Test_Update_FirstLast(){
    	New_Orders__c no = initUpdateNewOrders(1)[0];
    	Order__c testOrder = testOrderList[0];
		insert testOrder;
		
		no.Patient_Name__c = 'First Last';
		no.Member_Patient_ID__c = 'id-1';
		update no;
		
		List<Order__c> orderList = [
    		SELECT 
				Id,
				Plan_Patient__c
    		FROM Order__c 
    		WHERE Legacy_Orders__c = :no.Id
		];
    	system.assertEquals('First', [SELECT First_Name__c FROM Plan_Patient__c WHERE Id = :orderList[0].Plan_Patient__c].First_Name__c);
    	system.assertEquals('Last', [SELECT Last_Name__c FROM Plan_Patient__c WHERE Id = :orderList[0].Plan_Patient__c].Last_Name__c);
	}
	static testMethod void CreateOrderTrigger_cs_Test_Update_LastFirst(){
    	New_Orders__c no = initUpdateNewOrders(1)[0];
    	Order__c testOrder = testOrderList[0];
		insert testOrder;
		
		no.Patient_Name__c = 'Last, First';
		no.Member_Patient_ID__c = 'id-1';
		update no;
		
		List<Order__c> orderList = [
    		SELECT 
				Id,
				Plan_Patient__c
    		FROM Order__c 
    		WHERE Legacy_Orders__c = :no.Id
		];
    	system.assertEquals('First', [SELECT First_Name__c FROM Plan_Patient__c WHERE Id = :orderList[0].Plan_Patient__c].First_Name__c);
    	system.assertEquals('Last', [SELECT Last_Name__c FROM Plan_Patient__c WHERE Id = :orderList[0].Plan_Patient__c].Last_Name__c);
	}
	/*
	static testMethod void CreateOrderTrigger_cs_Test_Update_StatusOne(){
    	New_Orders__c no = initUpdateNewOrders(1)[0];
    	Order__c testOrder = testOrderList[0];
		insert testOrder;
		
		no.Status_of_Order__c = 'Additional Information Needed';
		update no;
		
		List<Order__c> orderList = [
    		SELECT 
				Id,
				Status__c
    		FROM Order__c 
    		WHERE Legacy_Orders__c = :no.Id
		];
    	system.assertEquals('Needs Attention - Incomplete Delivery Information', orderList[0].Status__c);
	}
	static testMethod void CreateOrderTrigger_cs_Test_Update_StatusTwo(){
    	New_Orders__c no = initUpdateNewOrders(1)[0];
    	Order__c testOrder = testOrderList[0];
		insert testOrder;
		
		System.debug(testOrder.Id);
		
		no.Status_of_Order__c = 'Closed/Not Delivered';
		update no;
		
		List<Order__c> orderList = [
    		SELECT 
				Id,
				Status__c
    		FROM Order__c 
    		WHERE Legacy_Orders__c = :no.Id
		];
		
		System.debug(orderList[0].Id);
		
    	system.assertEquals('Needs Attention - Patient Refused - Other', orderList[0].Status__c);
	}
	static testMethod void CreateOrderTrigger_cs_Test_Update_StatusThree(){
    	New_Orders__c no = initUpdateNewOrders(1)[0];
    	Order__c testOrder = testOrderList[0];
		insert testOrder;
		
		no.Status_of_Order__c = 'Delivered';
		update no;
		
		List<Order__c> orderList = [
    		SELECT 
				Id,
				Status__c
    		FROM Order__c 
    		WHERE Legacy_Orders__c = :no.Id
		];
    	system.assertEquals('Delivered', orderList[0].Status__c);
	}
	static testMethod void CreateOrderTrigger_cs_Test_Update_StatusFour(){
    	New_Orders__c no = initUpdateNewOrders(1)[0];
    	Order__c testOrder = testOrderList[0];
		insert testOrder;
		
		no.Status_of_Order__c = 'In Progress';
		update no;
		
		List<Order__c> orderList = [
    		SELECT 
				Id,
				Status__c
    		FROM Order__c 
    		WHERE Legacy_Orders__c = :no.Id
		];
    	system.assertEquals('Provider Accepted', orderList[0].Status__c);
	}
	
	static testMethod void CreateOrderTrigger_cs_Test_Update_StatusFive(){
    	New_Orders__c no = initUpdateNewOrders(1)[0];
    	Order__c testOrder = testOrderList[0];
		insert testOrder;
		
		no.Status_of_Order__c = 'Open';
		update no;
		
		List<Order__c> orderList = [
    		SELECT 
				Id,
				Status__c
    		FROM Order__c 
    		WHERE Legacy_Orders__c = :no.Id
		];
    	system.assertEquals('New', orderList[0].Status__c);
	}
	static testMethod void CreateOrderTrigger_cs_Test_UpdateWithUpdatedPlanPatient_Other(){
    	New_Orders__c no = initUpdateNewOrders(1)[0];
    	Order__c testOrder = testOrderList[0];
		insert testOrder;
		
		no.Patient_Name__c = 'testPatient';
		update no;
		
		List<Order__c> orderList = [
    		SELECT 
				Id,
				Plan_Patient__c
    		FROM Order__c 
    		WHERE Legacy_Orders__c = :no.Id
		];
		List<Plan_Patient__c> planPatientList = [
			SELECT
				Id,
				First_Name__c,
				Last_Name__c
			FROM Plan_Patient__c
			WHERE Id = :planPatient.Id
		];
    	system.assertEquals(planPatient.Id, orderList[0].Plan_Patient__c);
    	//system.assertEquals('Test', planPatientList[0].First_Name__c);
    	//system.assertEquals('testPatient', planPatientList[0].Last_Name__c);
    }
    
    static testMethod void CreateOrderTrigger_cs_Test_UpdateWithUpdatedPlanPatient_FirstLast(){
    	New_Orders__c no = initUpdateNewOrders(1)[0];
    	Order__c testOrder = testOrderList[0];
		insert testOrder;
		
		no.Patient_Name__c = 'Alicia Summers';
    	no.City__c = 'testCity';
		no.D_O_B__c = Date.valueOf('1990-10-09');
		no.Medicaid_ID__c = 'medicaid';
		no.Medicare_ID__c = 'medicare';
		no.Patient_Phone__c = '9876543212';
		no.State__c = 'UT';
		no.Street__c = '123 W 4th St';
		no.Zip__c = '76543';
		update no;
		
		List<Order__c> orderList = [
    		SELECT 
				Id,
				Plan_Patient__c
    		FROM Order__c 
    		WHERE Legacy_Orders__c = :no.Id
		];
		List<Plan_Patient__c> planPatientList = [
			SELECT
				Id,
				First_Name__c,
				Last_Name__c
			FROM Plan_Patient__c
			WHERE Id = :planPatient.Id
		];
    	system.assertEquals(planPatient.Id, orderList[0].Plan_Patient__c);
    	//system.assertEquals('Alicia', planPatientList[0].First_Name__c);
    	//system.assertEquals('Summers', planPatientList[0].Last_Name__c);
    }
    static testMethod void CreateOrderTrigger_cs_Test_UpdateWithUpdatedPlanPatient_LastFirst(){
    	New_Orders__c no = initUpdateNewOrders(1)[0];
    	Order__c testOrder = testOrderList[0];
		insert testOrder;
		
		no.Patient_Name__c = 'Summer, Alice';
		update no;
		
		List<Order__c> orderList = [
    		SELECT 
				Id,
				Plan_Patient__c
    		FROM Order__c 
    		WHERE Legacy_Orders__c = :no.Id
		];
		List<Plan_Patient__c> planPatientList = [
			SELECT
				Id,
				First_Name__c,
				Last_Name__c
			FROM Plan_Patient__c
			WHERE Id = :planPatient.Id
		];
    	system.assertEquals(planPatient.Id, orderList[0].Plan_Patient__c);
    	//system.assertEquals('Alice', planPatientList[0].First_Name__c);
    	//system.assertEquals('Summer', planPatientList[0].Last_Name__c);
    }
    static testMethod void CreateOrderTrigger_cs_Test_Bulk(){
    	List<New_Orders__c> upsertList = initUpdateNewOrders(20);
    	for(New_Orders__c no : upsertList){
    		no.CPT_Code_4__c = 'A0004';
    	}
    	delete dmeList;
    	
    	List<New_Orders__c> insertList = initNewOrders(20);
    	for(New_Orders__c no : insertList){
    		no.CPT_Code_4__c = 'A0003';
    	}
    	upsertList.addAll(insertList);
    	
    	upsert upsertList;
    }
    static List<New_Orders__c> initUpdateNewOrders(Integer recordCount){
    	List<New_Orders__c> noList = initNewOrders(recordCount);
    	Set<Id> noIds = new Set<Id>();
    	
    	planPatient = TestDataFactory_CS.generatePlanPatientsPlanOnly(planList[0].Id, 1)[0];
    	planPatient.Patient__c = null;
    	insert planPatient;
    	
    	insert noList;
    	for(New_Orders__c no : noList){
    		noIds.add(no.Id);
    	}
    	
    	List<Order__c> orderList = [
    		SELECT 
				Id
    		FROM Order__c 
    		WHERE Legacy_Orders__c IN :noIds
		];
		delete orderList;
		
		testOrderList = TestDataFactory_CS.generateOrders(planPatient.Id, providerList[0].Id, recordCount);
		
		for(integer i=0; i<recordCount; i++){
			testOrderList[i].Legacy_Orders__c = noList[i].Id;
			testOrderList[i].Plan_Patient__c = null;
		}
		
		return noList;
    }
	static List<New_Orders__c> initNewOrders(Integer recordCount) {
		DME_Settings__c dmeSettings = TestDataFactory_CS.generateDMESettings();
		insert dmeSettings;
		system.debug('initNewOrders DMESetting Id: ' + dmeSettings.Id);
    	
		dmeList = TestDataFactory_CS.generateDMEs(3);
		insert dmeList;
		system.debug('initNewOrders DME Id: ' + dmeList[0].Id);
    	
		planList = TestDataFactory_CS.generatePlans('testPlan', 1);
		insert planList;
		system.debug('initNewOrders Plan Id: ' + planList[0].Id);
    	
		providerList = TestDataFactory_CS.generateProviders('testProvider', 1);
		insert providerList;
		system.debug('initNewOrders Account Id: ' + providerList[0].Id);
    	
		Provider_Location__c pl = TestDataFactory_CS.generateProviderLocations(providerList[0].Id, 1)[0];
		insert pl;
		system.debug('initNewOrders Provider_Location__c Id: ' + pl.Id);
    	
		return TestDataFactory_CS.generateNewOrders(pl.Id, planList[0].Id, providerList[0].Id, dmeList, recordCount);
	}
    */
}