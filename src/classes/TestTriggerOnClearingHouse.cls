@isTest
private class TestTriggerOnClearingHouse {
	
	@isTest static void TestTriggerOnClearingHouse() {
		// create bank deposits

		Check_Deposit__c check1 = new Check_Deposit__c(Paid_Amount__c = 150.06, Remitter_Name__c = 'Empire');
		insert check1;

		Check_Deposit__c check2 = new Check_Deposit__c(Paid_Amount__c = 150.06, Remitter_Name__c = 'Empire');
		insert check2;

		Check_Deposit__c check3 = new Check_Deposit__c(Paid_Amount__c = 200, Remitter_Name__c = 'Aetna');
		insert check3;

		// test for multiple matches
		Clearinghouse__c claim1 = new Clearinghouse__c(Amount__c = 150.06, Payer__c = 'Empire');
		Insert claim1;

		List<Clearinghouse__c> testClaim1 = [Select Id, Amount__c, Payer__c, Status__c from Clearinghouse__c where
											 Amount__c = 150.06];
		for(Clearinghouse__c c : testClaim1){

			System.assertEquals(c.Status__c, 'More than one possible match','Clearinghouse status checked');
		}

		//test for unique match
		Clearinghouse__c claim2 = new Clearinghouse__c(Amount__c = 200, Payer__c = 'Aetna');
		Insert claim2;

		List<Clearinghouse__c> testClaim2 = [Select Id, Amount__c, Payer__c, Status__c from Clearinghouse__c where
											 Amount__c = 200];
		for(Clearinghouse__c c : testClaim2){

			System.assertEquals(c.Status__c, 'Deposit Matched,Claim Matched','Clearinghouse status checked');
		}

		//no match

		Clearinghouse__c claim3 = new Clearinghouse__c(Amount__c = 100, Payer__c = 'Test');
		Insert claim3;

		List<Clearinghouse__c> testClaim3 = [Select Id, Amount__c, Payer__c, Status__c from Clearinghouse__c where
											 Amount__c = 100];
		for(Clearinghouse__c c : testClaim3){

			System.assertEquals(c.Status__c, 'Deposit Present, no Claim Matched','Clearinghouse status checked');
		}
	}
	
	
}