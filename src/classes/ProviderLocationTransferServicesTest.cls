/**
 *  @Description Test ProviderLocationTransferServices
 *  @Author Cloud Software LLC
 *  Revision History: 
 *      12/02/2015 - karnold - ICA-168 - added transferDataToNewProviderLocationObject_Fields_Test to test new method.
 *		03/02/2016 - ksharp - ICA- 531 - added test to if the "_Is_Open__c" is set correctly to true or false
 *
 */
@isTest
public class ProviderLocationTransferServicesTest {

	public static Account testAccount1;
	public static Account testAccount2;
	public static Account testAccount3;
	public static Account testAccount4;
	public static Account testAccount5;
	public static List<Provider_Locations__c> testOldProvLocList;
	public static List<Provider_Location__c> testNewProvLocList;
	public static String dataIssuesPL2;
	public static String dataIssuesPL3;
	public static String dataIssuesPL4;
	public static String dataIssuesPL5;

	private static testMethod void transferDataToNewProviderLocationObject_Fields_Test() {
		// GIVEN
		init();
		Contact cont = new Contact(LastName = 'Test');
		insert cont;

		Provider_Locations__c oldProviderLocation = testOldProvLocList[0];
		oldProviderLocation.ADA_Form_Received__c = Date.today();
		oldProviderLocation.All_States_and_Counties__c = true;
		oldProviderLocation.MACleanCity__c = 'Clean City';
		oldProviderLocation.MACleanCountry__c = 'Clean Country';
		oldProviderLocation.MACleanCounty__c = 'Clean County';
		oldProviderLocation.MACleanDistrict__c = 'Clean District';
		oldProviderLocation.MACleanPostalCode__c = 'Clean Postal';
		oldProviderLocation.MACleanState__c = 'Clean State';
		oldProviderLocation.MACleanStreet__c = 'Clean Street';
		oldProviderLocation.Does_NOT_Deliver__c = false;
		oldProviderLocation.Handicap_Accessible__c = true;
		oldProviderLocation.MALatitude__c = 112.1;
		oldProviderLocation.Local_Counties__c = false;
		oldProviderLocation.Location_City__c = 'Loc City';
		oldProviderLocation.Location_Contact__c = cont.Id;
		oldProviderLocation.Location_Contact_2__c = cont.Id;
		oldProviderLocation.Location_Fax__c = '123-159-1471';
		oldProviderLocation.Location_Medicaid__c = 'Loc Medi';
		oldProviderLocation.Location_Medicaid_State__c = 'Med St';
		oldProviderLocation.Location_NPI__c = 'Loc NPI';
		oldProviderLocation.Location_Phone__c = '3219517417';
		oldProviderLocation.Location_State__c = 'PA';
		oldProviderLocation.Location_Street__c = 'Loc Street';
		oldProviderLocation.Location_Wellcare_ID__c = 'Loc Well Id';
		oldProviderLocation.Location_Zip_Code__c = 'Loc Zip';
		oldProviderLocation.MALongitude__c = 211.2;
		oldProviderLocation.Send_Wellcare_Docs__c = true;
		oldProviderLocation.Send_Wellcare_Docs_To__c = cont.Id;
		oldProviderLocation.Store_Closure_Date__c = Date.today().addDays(1);
		oldProviderLocation.County__c = 'Store county';
		oldProviderLocation.Name_DBA__c = 'Name DBA';
		oldProviderLocation.Store_Status__c = 'Active';
		oldProviderLocation.Wellcare_Sent_on__c = DateTime.now();
		

		List<Provider_Locations__c> singleFullProvider = new List<Provider_Locations__c> ();
		singleFullProvider.add(oldProviderLocation);

		// WHEN
		Test.startTest();
		testNewProvLocList = ProviderLocationTransferServices.transferDataToNewProviderLocationObject(singleFullProvider);
		Test.stopTest();

		// THEN
		Provider_Location__c newProviderLocation = testNewProvLocList[0];

		System.assertEquals(newProviderLocation.Account__c, oldProviderLocation.Account__c);
		System.assertEquals(newProviderLocation.ADA_Form_Received__c, oldProviderLocation.ADA_Form_Received__c);
		System.assertEquals(newProviderLocation.All_States_and_Counties__c, oldProviderLocation.All_States_and_Counties__c);
		System.assertEquals(newProviderLocation.MACleanCity__c, oldProviderLocation.MACleanCity__c);
		System.assertEquals(newProviderLocation.MACleanCountry__c, oldProviderLocation.MACleanCountry__c);
		System.assertEquals(newProviderLocation.MACleanCounty__c, oldProviderLocation.MACleanCounty__c);
		System.assertEquals(newProviderLocation.MACleanDistrict__c, oldProviderLocation.MACleanDistrict__c);
		System.assertEquals(newProviderLocation.MACleanPostalCode__c, oldProviderLocation.MACleanPostalCode__c);
		System.assertEquals(newProviderLocation.MACleanState__c, oldProviderLocation.MACleanState__c);
		System.assertEquals(newProviderLocation.MACleanStreet__c, oldProviderLocation.MACleanStreet__c);
		System.assertEquals(newProviderLocation.Does_NOT_Deliver__c, oldProviderLocation.Does_NOT_Deliver__c);
		System.assertEquals(newProviderLocation.Handicap_Accessible__c, oldProviderLocation.Handicap_Accessible__c);
		System.assertEquals(newProviderLocation.MALatitude__c, oldProviderLocation.MALatitude__c);
		System.assertEquals(newProviderLocation.Local_Counties__c, oldProviderLocation.Local_Counties__c);
		System.assertEquals(newProviderLocation.Location_City__c, oldProviderLocation.Location_City__c);
		System.assertEquals(newProviderLocation.Location_Contact__c, oldProviderLocation.Location_Contact__c);
		System.assertEquals(newProviderLocation.Location_Contact_2__c, oldProviderLocation.Location_Contact_2__c);
		System.assertEquals(newProviderLocation.Location_Fax__c, oldProviderLocation.Location_Fax__c);
		System.assertEquals(newProviderLocation.Location_Medicaid__c, oldProviderLocation.Location_Medicaid__c);
		System.assertEquals(newProviderLocation.Location_Medicaid_State__c, oldProviderLocation.Location_Medicaid_State__c);
		System.assertEquals(newProviderLocation.Location_NPI__c, oldProviderLocation.Location_NPI__c);
		System.assertEquals(newProviderLocation.Location_Phone__c, oldProviderLocation.Location_Phone__c);
		System.assertEquals(newProviderLocation.Location_State__c, oldProviderLocation.Location_State__c);
		System.assertEquals(newProviderLocation.Location_Street__c, oldProviderLocation.Location_Street__c);
		System.assertEquals(newProviderLocation.Location_Wellcare_ID__c, oldProviderLocation.Location_Wellcare_ID__c);
		System.assertEquals(newProviderLocation.Location_Zip_Code__c, oldProviderLocation.Location_Zip_Code__c);
		System.assertEquals(newProviderLocation.MALongitude__c, oldProviderLocation.MALongitude__c);
		System.assertEquals(newProviderLocation.Provider_Location_Old__c, oldProviderLocation.Id);
		System.assertEquals(newProviderLocation.Send_Wellcare_Docs__c, oldProviderLocation.Send_Wellcare_Docs__c);
		System.assertEquals(newProviderLocation.Send_Wellcare_Docs_To__c, oldProviderLocation.Send_Wellcare_Docs_To__c);
		System.assertEquals(newProviderLocation.Store_Closure_Date__c, oldProviderLocation.Store_Closure_Date__c);
		System.assertEquals(newProviderLocation.Store_County__c, oldProviderLocation.County__c);
		System.assertEquals(newProviderLocation.Name_DBA__c, oldProviderLocation.Name_DBA__c);
		System.assertEquals(newProviderLocation.Store_Status__c, oldProviderLocation.Store_Status__c);
		System.assertEquals(newProviderLocation.Wellcare_Sent_on__c, oldProviderLocation.Wellcare_Sent_on__c);
	}

	static testMethod void transferDataToNewProviderLocationObject_TimesAndDates_Test() {
		init();
		System.Test.startTest();

		testNewProvLocList = ProviderLocationTransferServices.transferDataToNewProviderLocationObject(testOldProvLocList);

		System.Test.stopTest();

		System.assertEquals(5, testNewProvLocList.size());

		testNewProvLocList = [
			SELECT
				Accessibility_Rate__c,
				Account__c,
				Data_Issue_Found__c,
				Description_of_Data_Issue__c,
				Friday_Closing_Time__c,
				Friday_Opening_Time__c,
				Friday_Is_Open__c,
				Monday_Closing_Time__c,
				Monday_Opening_Time__c,
				Monday_Is_Open__c,
				Saturday_Closing_Time__c,
				Saturday_Opening_Time__c,
				Saturday_Is_Open__c,
				Sunday_Closing_Time__c,
				Sunday_Opening_Time__c,
				Sunday_Is_Open__c,
				Thursday_Closing_Time__c,
				Thursday_Opening_Time__c,
				Thursday_Is_Open__c,
				Tuesday_Closing_Time__c,
				Tuesday_Opening_Time__c,
				Tuesday_Is_Open__c,
				Wednesday_Closing_Time__c,
				Wednesday_Opening_Time__c,
				Wednesday_Is_Open__c
			FROM Provider_Location__c
		];

		System.assertEquals(5, testNewProvLocList.size());

		for (Provider_Location__c pl : testNewProvLocList) {
			if (pl.Account__c == testAccount1.Id) {
				System.assertEquals(22.61905, pl.Accessibility_Rate__c);
				System.assert(!pl.Data_Issue_Found__c);
				System.assertEquals(null, pl.Description_of_Data_Issue__c);
				System.assertEquals(null, pl.Monday_Opening_Time__c);
				System.assertEquals(null, pl.Monday_Closing_Time__c);
				System.assertEquals(false, pl.Monday_Is_Open__c);
				System.assertEquals(9, pl.Tuesday_Opening_Time__c);
				System.assertEquals(17, pl.Tuesday_Closing_Time__c);
				System.assertEquals(true, pl.Tuesday_Is_Open__c);
				System.assertEquals(9, pl.Wednesday_Opening_Time__c);
				System.assertEquals(17, pl.Wednesday_Closing_Time__c);
				System.assertEquals(true, pl.Wednesday_Is_Open__c);
				System.assertEquals(9, pl.Thursday_Opening_Time__c);
				System.assertEquals(12, pl.Thursday_Closing_Time__c);
				System.assertEquals(true, pl.Thursday_Is_Open__c);
				System.assertEquals(9, pl.Friday_Opening_Time__c);
				System.assertEquals(12, pl.Friday_Closing_Time__c);
				System.assertEquals(true, pl.Friday_Is_Open__c);
				System.assertEquals(9.5, pl.Saturday_Opening_Time__c);
				System.assertEquals(17.5, pl.Saturday_Closing_Time__c);
				System.assertEquals(true, pl.Saturday_Is_Open__c);
				System.assertEquals(9, pl.Sunday_Opening_Time__c);
				System.assertEquals(17, pl.Sunday_Closing_Time__c);
				System.assertEquals(true, pl.Sunday_Is_Open__c);
			} else if (pl.Account__c == testAccount2.Id) {
				System.assertEquals(0.00000, pl.Accessibility_Rate__c);
				System.assert(pl.Data_Issue_Found__c);
				System.assertEquals(dataIssuesPL2, pl.Description_of_Data_Issue__c);
				System.assertEquals(null, pl.Monday_Opening_Time__c);
				System.assertEquals(null, pl.Monday_Closing_Time__c);
				System.assertEquals(null, pl.Tuesday_Opening_Time__c);
				System.assertEquals(null, pl.Tuesday_Closing_Time__c);
				System.assertEquals(null, pl.Wednesday_Opening_Time__c);
				System.assertEquals(null, pl.Wednesday_Closing_Time__c);
				System.assertEquals(null, pl.Thursday_Opening_Time__c);
				System.assertEquals(null, pl.Thursday_Closing_Time__c);
				System.assertEquals(null, pl.Friday_Opening_Time__c);
				System.assertEquals(null, pl.Friday_Closing_Time__c);
				System.assertEquals(null, pl.Saturday_Opening_Time__c);
				System.assertEquals(null, pl.Saturday_Closing_Time__c);
				System.assertEquals(null, pl.Sunday_Opening_Time__c);
				System.assertEquals(null, pl.Sunday_Closing_Time__c);
			} else if (pl.Account__c == testAccount3.Id) {
				System.assertEquals(0.00000, pl.Accessibility_Rate__c);
				System.assert(pl.Data_Issue_Found__c);
				System.assertEquals(dataIssuesPL3, pl.Description_of_Data_Issue__c);
				System.assertEquals(null, pl.Monday_Opening_Time__c);
				System.assertEquals(null, pl.Monday_Closing_Time__c);
				System.assertEquals(null, pl.Tuesday_Opening_Time__c);
				System.assertEquals(null, pl.Tuesday_Closing_Time__c);
				System.assertEquals(null, pl.Wednesday_Opening_Time__c);
				System.assertEquals(null, pl.Wednesday_Closing_Time__c);
				System.assertEquals(null, pl.Thursday_Opening_Time__c);
				System.assertEquals(null, pl.Thursday_Closing_Time__c);
				System.assertEquals(null, pl.Friday_Opening_Time__c);
				System.assertEquals(null, pl.Friday_Closing_Time__c);
				System.assertEquals(null, pl.Saturday_Opening_Time__c);
				System.assertEquals(null, pl.Saturday_Closing_Time__c);
				System.assertEquals(null, pl.Sunday_Opening_Time__c);
				System.assertEquals(null, pl.Sunday_Closing_Time__c);
			} else if (pl.Account__c == testAccount4.Id) {
				System.assertEquals(19.04762, pl.Accessibility_Rate__c);
				System.assert(pl.Data_Issue_Found__c);
				System.assertEquals(dataIssuesPL4, pl.Description_of_Data_Issue__c);
				System.assertEquals(null, pl.Monday_Opening_Time__c);
				System.assertEquals(null, pl.Monday_Closing_Time__c);
				System.assertEquals(null, pl.Tuesday_Opening_Time__c);
				System.assertEquals(null, pl.Tuesday_Closing_Time__c);
				System.assertEquals(9, pl.Wednesday_Opening_Time__c);
				System.assertEquals(17, pl.Wednesday_Closing_Time__c);
				System.assertEquals(9, pl.Thursday_Opening_Time__c);
				System.assertEquals(17, pl.Thursday_Closing_Time__c);
				System.assertEquals(9, pl.Friday_Opening_Time__c);
				System.assertEquals(17, pl.Friday_Closing_Time__c);
				System.assertEquals(9, pl.Saturday_Opening_Time__c);
				System.assertEquals(17, pl.Saturday_Closing_Time__c);
				System.assertEquals(null, pl.Sunday_Opening_Time__c);
				System.assertEquals(null, pl.Sunday_Closing_Time__c);
			} else if (pl.Account__c == testAccount5.Id) {
				System.assertEquals(21.42857, pl.Accessibility_Rate__c);
				System.assert(pl.Data_Issue_Found__c);
				System.assertEquals(dataIssuesPL5, pl.Description_of_Data_Issue__c);
				System.assertEquals(null, pl.Monday_Opening_Time__c);
				System.assertEquals(null, pl.Monday_Closing_Time__c);
				System.assertEquals(5, pl.Tuesday_Opening_Time__c);
				System.assertEquals(17, pl.Tuesday_Closing_Time__c);
				System.assertEquals(9.5, pl.Wednesday_Opening_Time__c);
				System.assertEquals(17, pl.Wednesday_Closing_Time__c);
				System.assertEquals(9, pl.Thursday_Opening_Time__c);
				System.assertEquals(17.5, pl.Thursday_Closing_Time__c);
				System.assertEquals(9, pl.Friday_Opening_Time__c);
				System.assertEquals(17, pl.Friday_Closing_Time__c);
				System.assertEquals(null, pl.Saturday_Opening_Time__c);
				System.assertEquals(null, pl.Saturday_Closing_Time__c);
				System.assertEquals(null, pl.Sunday_Opening_Time__c);
				System.assertEquals(null, pl.Sunday_Closing_Time__c);
			}
		}
	}

	static testMethod void updateRecoupmentProviderLocation_Test() {
		init();

		testNewProvLocList = ProviderLocationTransferServices.transferDataToNewProviderLocationObject(testOldProvLocList);

		ARAG__c workItem = New ARAG__c();
		insert workItem;

		Recoupment__c refRec = new Recoupment__c();
		refRec.Related_Claims_Work_Item__c = workItem.Id;
		refRec.Provider_Location__c = testOldProvLocList[0].Id;
		refRec.Provider_Location_New__c = testNewProvLocList[1].Id;
		insert refRec;

		Test.startTest();
		List<Recoupment__c> recList = (List<Recoupment__c>) ProviderLocationTransferServices.updateObjectProviderLocation(new List<Recoupment__c> { refRec });
		Test.stopTest();

		System.assertNotEquals(testNewProvLocList[1].Id, recList[0].Provider_Location_New__c);
		System.assertEquals(testNewProvLocList[0].Id, recList[0].Provider_Location_New__c);
	}

	static testMethod void updateBusinessIdentifierProviderLocation_Test() {
		init();

		testNewProvLocList = ProviderLocationTransferServices.transferDataToNewProviderLocationObject(testOldProvLocList);

		Contracts_Licenses__c conLic = new Contracts_Licenses__c();
		conLic.Provider_Location__c = testOldProvLocList[0].Id;
		conLic.Provider_Location_New__c = testNewProvLocList[1].Id;
		conLic.Account_Name__c = testAccount1.Id;
		insert conLic;

		Test.startTest();
		List<Contracts_Licenses__c> conLicList = (List<Contracts_Licenses__c>) ProviderLocationTransferServices.updateObjectProviderLocation(new List<Contracts_Licenses__c> { conLic });
		Test.stopTest();

		System.assertNotEquals(testNewProvLocList[1].Id, conLicList[0].Provider_Location_New__c);
		System.assertEquals(testNewProvLocList[0].Id, conLicList[0].Provider_Location_New__c);
	}

	static testMethod void updateCountyItemProviderLocation_Test() {
		init();

		testNewProvLocList = ProviderLocationTransferServices.transferDataToNewProviderLocationObject(testOldProvLocList);

		US_State_County__c usc = new US_State_County__c();
		usc.State__c = 'New York';
		usc.State_Code__c = 'NY';
		insert usc;

		County_Item__c countItem = new County_Item__c();
		countItem.Provider_Location__c = testOldProvLocList[0].Id;
		countItem.Provider_Location_New__c = testNewProvLocList[1].Id;
		countItem.US_State_County__c = usc.Id;
		insert countItem;

		Test.startTest();
		List<County_Item__c> countItemList = (List<County_Item__c>) ProviderLocationTransferServices.updateObjectProviderLocation(new List<County_Item__c> { countItem });
		Test.stopTest();

		System.assertNotEquals(testNewProvLocList[1].Id, countItemList[0].Provider_Location_New__c);
		System.assertEquals(testNewProvLocList[0].Id, countItemList[0].Provider_Location_New__c);
	}

	static testMethod void updateOrderProviderLocation_Test() {
		init();

		testNewProvLocList = ProviderLocationTransferServices.transferDataToNewProviderLocationObject(testOldProvLocList);

		Account healthPlan = TestDataFactory_CS.generatePlans('Test Health Plan', 1) [0];
		insert healthPlan;

		Plan_Patient__c pp = TestDataFactory_CS.generatePlanPatientsPlanOnly(healthPlan.Id, 1) [0];
		insert pp;

		Order__c ord = TestDataFactory_CS.generateOrders(pp.Id, testAccount1.Id, 1) [0];
		ord.Provider_Locations__c = testOldProvLocList[0].Id;
		ord.Provider_Location__c = testNewProvLocList[1].Id;
		insert ord;

		Test.startTest();
		List<Order__c> orderList = ProviderLocationTransferServices.updateOrderProviderLocation(new List<Order__c> { ord });
		Test.stopTest();

		System.assertNotEquals(testNewProvLocList[1].Id, orderList[0].Provider_Location__c);
		System.assertEquals(testNewProvLocList[0].Id, orderList[0].Provider_Location__c);
	}

	static testMethod void updateProvStateIdentifierProviderLocation_Test() {
		init();

		testNewProvLocList = ProviderLocationTransferServices.transferDataToNewProviderLocationObject(testOldProvLocList);

		State_Identifier__c stateIdent = new State_Identifier__c();
		stateIdent.Provider_Location__c = testOldProvLocList[0].Id;
		stateIdent.Provider_Location_New__c = testNewProvLocList[1].Id;
		stateIdent.Medicaid__c = 'Test Medicaid';
		insert stateIdent;

		Test.startTest();
		List<State_Identifier__c> stateIdentList = (List<State_Identifier__c>) ProviderLocationTransferServices.updateObjectProviderLocation(new List<State_Identifier__c> { stateIdent });
		Test.stopTest();

		System.assertNotEquals(testNewProvLocList[1].Id, stateIdentList[0].Provider_Location_New__c);
		System.assertEquals(testNewProvLocList[0].Id, stateIdentList[0].Provider_Location_New__c);
	}

	static void init() {

		Global_Switches__c gs = new Global_Switches__c(
		                                               Name = 'Defaults',
		                                               Create_ProvLoc_Trigger_Off__c = true
		);
		insert gs;

		List<Account> testAccountList = TestDataFactory_CS.generateProviders('Test Account', 5);
		insert testAccountList;
		testAccount1 = testAccountList[0];
		testAccount2 = testAccountList[1];
		testAccount3 = testAccountList[2];
		testAccount4 = testAccountList[3];
		testAccount5 = testAccountList[4];

		testOldProvLocList = TestDataFactory_CS.generateOldProviderLocations(testAccount1.Id, 5);

		// Will not be flagged
		testOldProvLocList[0].Account__c = testAccount1.Id;
		testOldProvLocList[0].Monday__c = 'Closed';
		testOldProvLocList[0].Tuesday__c = '9:00am-5:00pm';
		testOldProvLocList[0].Wednesday__c = '9:00AM-5:00PM';
		testOldProvLocList[0].Thursday__c = ' 9am - 12:00pm';
		testOldProvLocList[0].Friday__c = ' 9:00am - 12pm';
		testOldProvLocList[0].Saturday__c = '9:30am-5:30pm';
		testOldProvLocList[0].Sunday__c = '09am-05:00pm';

		// Will be flagged - all values on the new record will be null except the Account lookup and the error flags
		testOldProvLocList[1].Account__c = testAccount2.Id;
		testOldProvLocList[1].Monday__c = null;
		testOldProvLocList[1].Tuesday__c = '';
		testOldProvLocList[1].Wednesday__c = '      ';
		testOldProvLocList[1].Thursday__c = 'By Appt';
		testOldProvLocList[1].Friday__c = '12:00 - 1:00';
		testOldProvLocList[1].Saturday__c = '1:00 - 12:00';
		testOldProvLocList[1].Sunday__c = '9 to 5';

		// Will be flagged - all values on the new record will be null except the Account lookup and the error flags
		testOldProvLocList[2].Account__c = testAccount3.Id;
		testOldProvLocList[2].Monday__c = '3 - 5';
		testOldProvLocList[2].Tuesday__c = '3am - 5';
		testOldProvLocList[2].Wednesday__c = '3 - 5pm';
		testOldProvLocList[2].Thursday__c = '3pm - 5';
		testOldProvLocList[2].Friday__c = '3 -';
		testOldProvLocList[2].Saturday__c = '- 5';
		testOldProvLocList[2].Sunday__c = '3:00pm-5:00pm';

		// Will be flagged - not all values will be null on the new record
		testOldProvLocList[3].Account__c = testAccount4.Id;
		testOldProvLocList[3].Monday__c = 'On call'; //will cause error
		testOldProvLocList[3].Tuesday__c = '5 - 9'; //will cause error
		testOldProvLocList[3].Wednesday__c = '9:00-5:00';
		testOldProvLocList[3].Thursday__c = '9-5  ';
		testOldProvLocList[3].Friday__c = '9:00-5';
		testOldProvLocList[3].Saturday__c = '9-5:00';
		testOldProvLocList[3].Sunday__c = '12:30-5'; //will cause error

		// Will be flagged - not all values will be null on the new record
		testOldProvLocList[4].Account__c = testAccount5.Id;
		testOldProvLocList[4].Monday__c = '9am - 5'; //will cause error
		testOldProvLocList[4].Tuesday__c = '5-5';
		testOldProvLocList[4].Wednesday__c = '9:30-05';
		testOldProvLocList[4].Thursday__c = '09-5:30';
		testOldProvLocList[4].Friday__c = '9am-5pm';
		testOldProvLocList[4].Saturday__c = '9;00-5:00'; //actual error in the data
		testOldProvLocList[4].Sunday__c = '24 Hr - Emerg'; //actual error in the data

		insert testOldProvLocList;

		String generalErrorPart1 = ' Issue found on the fields ';
		String error1Part1 = '. A null value was found on the ';
		String error1Part2 = ' field on the original record.';
		String error3 = '. Could not determine whether the opening and closing times are AM or PM.';
		String error4 = '. Unexpected error occurred.';

		dataIssuesPL2 = '(Original record: ' + testOldProvLocList[1].Id + ').' + generalErrorPart1;
		dataIssuesPL2 = dataIssuesPL2 + 'Monday_Opening_Time__c and Monday_Closing_Time__c' + error1Part1 + 'Monday__c' + error1Part2;
		dataIssuesPL2 = dataIssuesPL2 + generalErrorPart1 + 'Tuesday_Opening_Time__c and Tuesday_Closing_Time__c' + error1Part1 + 'Tuesday__c' + error1Part2;
		dataIssuesPL2 = dataIssuesPL2 + generalErrorPart1 + 'Wednesday_Opening_Time__c and Wednesday_Closing_Time__c' + error1Part1 + 'Wednesday__c' + error1Part2;
		dataIssuesPL2 = dataIssuesPL2 + generalErrorPart1 + 'Thursday_Opening_Time__c and Thursday_Closing_Time__c.';
		dataIssuesPL2 = dataIssuesPL2 + generalErrorPart1 + 'Friday_Opening_Time__c and Friday_Closing_Time__c' + error3;
		dataIssuesPL2 = dataIssuesPL2 + generalErrorPart1 + 'Saturday_Opening_Time__c and Saturday_Closing_Time__c' + error3;
		dataIssuesPL2 = dataIssuesPL2 + generalErrorPart1 + 'Sunday_Opening_Time__c and Sunday_Closing_Time__c.';

		dataIssuesPL3 = '(Original record: ' + testOldProvLocList[2].Id + ').' + generalErrorPart1;
		dataIssuesPL3 = dataIssuesPL3 + 'Monday_Opening_Time__c and Monday_Closing_Time__c' + error3;
		dataIssuesPL3 = dataIssuesPL3 + generalErrorPart1 + 'Tuesday_Opening_Time__c and Tuesday_Closing_Time__c' + error4;
		dataIssuesPL3 = dataIssuesPL3 + generalErrorPart1 + 'Wednesday_Opening_Time__c and Wednesday_Closing_Time__c' + error4;
		dataIssuesPL3 = dataIssuesPL3 + generalErrorPart1 + 'Thursday_Opening_Time__c and Thursday_Closing_Time__c' + error4;
		dataIssuesPL3 = dataIssuesPL3 + generalErrorPart1 + 'Friday_Opening_Time__c and Friday_Closing_Time__c' + error3;
		dataIssuesPL3 = dataIssuesPL3 + generalErrorPart1 + 'Saturday_Opening_Time__c and Saturday_Closing_Time__c' + error3;
		dataIssuesPL3 = dataIssuesPL3 + generalErrorPart1 + 'Sunday_Opening_Time__c and Sunday_Closing_Time__c' + error4;

		dataIssuesPL4 = '(Original record: ' + testOldProvLocList[3].Id + ').' + generalErrorPart1;
		dataIssuesPL4 = dataIssuesPL4 + 'Monday_Opening_Time__c and Monday_Closing_Time__c.';
		dataIssuesPL4 = dataIssuesPL4 + generalErrorPart1 + 'Tuesday_Opening_Time__c and Tuesday_Closing_Time__c' + error3;
		dataIssuesPL4 = dataIssuesPL4 + generalErrorPart1 + 'Sunday_Opening_Time__c and Sunday_Closing_Time__c' + error3;

		dataIssuesPL5 = '(Original record: ' + testOldProvLocList[4].Id + ').' + generalErrorPart1;
		dataIssuesPL5 = dataIssuesPL5 + 'Monday_Opening_Time__c and Monday_Closing_Time__c' + error4;
		dataIssuesPL5 = dataIssuesPL5 + generalErrorPart1 + 'Saturday_Opening_Time__c and Saturday_Closing_Time__c' + error3;
		dataIssuesPL5 = dataIssuesPL5 + generalErrorPart1 + 'Sunday_Opening_Time__c and Sunday_Closing_Time__c' + error3;
	}
}