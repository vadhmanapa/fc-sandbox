public without sharing class ProviderForgotUsername_CS extends ProviderPortalServices_CS {

	private final String pageAccountType = 'Provider';

	/******* Page Interface *******/
	public String userEmail {get;set;}
	public String userEmailError {get;set;}

	public ProviderForgotUsername_CS(){}

	//TODO: How to handle more than one user with the same email address?

	public PageReference requestUsername(){
		
		Contact[] users;
		
		try{
			users = [select Id,Name,Email,Username__c from Contact where Email = :userEmail and Entity__r.Type__c = :pageAccountType limit 1];
		}
		catch(Exception e){
			//TODO: Better error handling.
			System.debug('Error: Error finding users with email ' + userEmail + '. ' + e.getMessage());
		}
		
		if(users.size() > 0){
			for(Contact c : users){

				System.debug('Found user ' + c.Name + ' with email ' + userEmail);

				if(c.Email != null && c.Email != ''){
				
					UserModel_CS u = new UserModel_CS(c);
					
					String emailSubject = 'Forgot Username';
					String emailBody = 'Username: ' + c.Username__c;
					
					if(!u.sendEmailTo(emailSubject, emailBody)){
						System.debug('Error: Failed to send username to ' + c.Email);
					}
				}
				else{
					System.debug('Error: Contact ' + c.Id + ' has no email.');
				}				
			}
		}
		else{
			System.debug('Error: No contact with email ' + userEmail + ' found.');
		}
		
		return null;
	}
}