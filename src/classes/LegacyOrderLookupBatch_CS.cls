public class LegacyOrderLookupBatch_CS implements Database.Batchable<sObject>, Database.Stateful { 

	public LegacyOrderLookupBatch_CS() {
	}
	
	public Database.QueryLocator start(Database.BatchableContext bc) {
		String queryString = 'SELECT CPT_code_for_OCR__c, Original_Created_Date__c, Patient_Name__c, Plan_Order__c, Units_for_OCR__c ';
		queryString += 'FROM New_Orders__c WHERE Loaded_From_Archive__c = true';
		return Database.GetQueryLocator(queryString); 
	}

	public void execute(Database.BatchableContext bc, List<sObject> scope) {
		LegacyOrderLookupBatchServices_CS.processLegacyOrders((List<New_Orders__c>)scope);
	}

	public void finish(Database.BatchableContext bc) {
	}
}