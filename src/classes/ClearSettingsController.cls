/**
 *  @Description Controller class for ClearSettingsPage
 *  @Author Cloud Software LLC, Greg Harvey
 *  Revision History: 
 *		10/05/2015 - Greg Harvey - Created
 *		10/07/2015 - Greg Harvey - Added Provider and Health Plan list creation methods
 *		11/04/2015 - Kelly Sharp - Added Save Provider Setting Methods
 *		12/05/2015 - Kelly Sharp(CS) - Added search to getPayers
 *		12/21/2015 - karnold - Changed approve record method to submit record for approval.
 *		12/23/2015 - karnold - removed references to DME_Category__c
 *		01/07/2016 - gharvey - Added queryOrdersAndDmeLineItems and processOrders for multi order simulations
 *		01/12/2016 - karnold - Added Active filter to getHCPCs
 *			Added Drive_Time_Callout_Radius__c and Estimated_Drive_Time__c and default values to createNewAlgSettWrap() method.
 *		01/13/2016 - karnold - Removed references to 'Syncing'. Renamed queryOrdersAndDmeLineItems to submitOrdersForBulkSimulation.
 *		01/15/2016 - karnold - Moved references of simulation record type to OrderModel_CS.simulationRecordType.
 *		02/03/2016 - jbrown - Added check to getPayers() so that only active payers are returned from the query. 
 *			Ordered the lists by Account name.
 *		02/18/2016 - karnold - Change query to soqlBuilder in processOrders and modified submitOrdersForBulkSimulation to
 *			check if the list for the future method is empty or not. Future method will not run if the list is empty.
 *		02/24/2016 - sbatchelor - Added the field Simulation_Original_Order__c to orders to hold a lookup to the orignial order after it's been cloned.
 *
 */
public class ClearSettingsController {

	//===========================================================================================
	//======================== Save Clear Algorithm Setting =====================================
	//===========================================================================================
	@RemoteAction
	public static String save(Clear_Algorithm_Settings__c settings) {
		upsert settings;
		return settings.Id;
	}
	
	//===========================================================================================
	//======================== Approve Clear Algorithm Setting ==================================
	//===========================================================================================
	@RemoteAction
	public static String approveRecord(Clear_Algorithm_Settings__c settings) {

		// Create the approval process
		Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
		
		approvalRequest.setSubmitterId(UserInfo.getUserId());	// Set the submitter
		approvalRequest.setComments('Settings submitted by ' + UserInfo.getUserName() + 
			' from Clear Algorithm Settings page.');	// Set the comments
		approvalRequest.setObjectId(settings.Id);	// Set the object to submit
		approvalRequest.setProcessDefinitionNameOrId('Sumbit_Algorithm_Settings_for_Approval');	// Set the process name
		approvalRequest.setSkipEntryCriteria(true);
		approvalRequest.setNextApproverIds(null);	// Set approver to null b/c next step is not another approval process

		// Submit settings object for approval.
		Approval.ProcessResult result = Approval.process(approvalRequest);

		return settings.Id;
	}

	//===========================================================================================
	//====================== Save Clear Algorithm Setting as Draft ==============================
	//===========================================================================================
	@RemoteAction
	public static String saveAsDraft(Clear_Algorithm_Settings__c settings) {
		settings.Status__c = 'Draft';
		upsert settings;
		return settings.Id;
	}

	//===========================================================================================
	//======================== Draft Clear Algorithm Setting ====================================
	//===========================================================================================
	@RemoteAction
	public static void deleteDraftCas(Clear_Algorithm_Settings__c settings) {
		List<Clear_Algorithm_Settings__c> draftCasListToDelete = new List<Clear_Algorithm_Settings__c>();
		draftCasListToDelete.add(settings);
		deleteDraftCasList(draftCasListToDelete);
	}

	//===========================================================================================
	//====================== Delete List of Draft Clear Algorithm Setting =======================
	//===========================================================================================
	@RemoteAction
	public static void deleteDraftCasList(List<Clear_Algorithm_Settings__c> settingsList) {
		delete settingsList;
	}

	//===========================================================================================
	//===================== Create New Clear Algorithm Setting Wrapper ==========================
	//===========================================================================================
	@RemoteAction
	public static AlgorithmSettingsWrapper createNewAlgSettWrap() {
		
		Clear_Algorithm_Settings__c cas =  new Clear_Algorithm_Settings__c(
			Status__c = 'Draft',
			GA_New_Patients__c = 0,
			BA_Accessibility_Rate__c = 0,
			BA_Average_Delivery_Completion_Time__c = 0,
			BA_Average_Delivery_Time__c = 0,
			BA_Delivery_Time__c = 0,
			GA_Direct_Delivery_Provider__c = 0,
			GA_Direct_Relationship__c = 0,
			GA_Drive_Time__c = 0,
			Drive_Time_Callout_Radius__c = 0,
			Estimated_Drive_Speed__c = 0,
			Existing_Provider_Max__c = 0,
			GA_Languages_Spoken__c = 0,
			GA_Look_Back_Period__c = '30 Days',
			GA_Max_Number_Providers__c = 0,
			GA_Max_Providers_Number__c = 0,
			GA_Max_Providers_Score_Over__c = 0,
			New_Provider_Time_Period_Max_Orders__c = 0,
			New_Provider_Time_Period_Num_Days__c = 0,
			BA_Percent_Orders_Accepted__c = 0,
			BA_Percent_Orders_Reassigned__c = 0,
			BA_Percent_Orders_Rejected__c = 0,
			BA_Percent_Orders_Updated_as_Completed__c = 0,
			BA_Speed_of_Acceptance__c = 0,
			Tier_1_From__c = 0,
			Tier_1_Max_Orders__c = 0,
			Tier_2_From__c = 0,
			Tier_2_Max_Orders__c = 0,
			Tier_3_From__c = 0,
			Tier_3_Max_Orders__c = 0,
			Use_Attribute_Accepting_New_Patients__c = true,
			Use_Attribute_Accessibility_Rates__c = true,
			Use_Attribute_Average_Delivery_CompTime__c = true,
			Use_Attribute_Average_Delivery_Time__c = true,
			Use_Attribute_Delivery_Time__c = true,
			Use_Attribute_Direct_Delivery_Provider__c = true,
			Use_Attribute_Direct_Relationship__c = true,
			Use_Attribute_Drive_Time__c = true,
			Use_Attribute_Existing_Provider_Daily__c = true,
			Use_Attribute_Languages_Spoken__c = true,
			Use_Attribute_Look_Back_Period__c = true,
			Use_Attribute_Max_Number_of_Providers__c = true,
			Use_Attribute_New_Provider_Time_Period__c = true,
			Use_Attribute_Percent_Orders_Accepted__c = true,
			Use_Attribute_Percent_Orders_Reassigned__c = true,
			Use_Attribute_Percent_Orders_Rejected__c = true,
			Use_Attribute_Per_Orders_Updated_as_Comp__c = true,
			Use_Attribute_Speed_of_Acceptance__c = true,
			Use_Attribute_Tier_1_Range__c = true,
			Use_Attribute_Tier_2_Range__c = true,
			Use_Attribute_Tier_3_Range__c = true
		);
		return new AlgorithmSettingsWrapper(cas);
	}

	//===========================================================================================
	//===================== Create Cloned Clear Algorithm Setting Wrapper =======================
	//===========================================================================================
	@RemoteAction
	public static AlgorithmSettingsWrapper createClonedAlgSettWrap(String clonedCas) {
		system.debug(clonedCas);
		//String prependClonedCas = clonedCas.replaceFirst('[{]', '{"attributes":{"type":"Clear_Algorithm_Settings__c"},');
		Clear_Algorithm_Settings__c tempCAS = (Clear_Algorithm_Settings__c)JSON.deserialize(clonedCas,sObject.class);
		system.debug(tempCas);
		return new AlgorithmSettingsWrapper(tempCAS);
	}

	//===========================================================================================
	//================== Create Clear Algorithm Setting Wrapper from record =====================
	//===========================================================================================
	@RemoteAction
	public static AlgorithmSettingsWrapper createAlgorithmSettingsWrapper(Id settingsId) {
		String SobjectApiName = 'Clear_Algorithm_Settings__c';
		Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
		Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
		String commaSeperatedFields = '';
		for(String fieldName : fieldMap.keyset()){
			if(commaSeperatedFields == null || commaSeperatedFields == ''){
				commaSeperatedFields = fieldName;
			}else{
				commaSeperatedFields = commaSeperatedFields + ', ' + fieldName;
			}
		}
 
		String query = 'SELECT ' + commaSeperatedFields + ' FROM ' + SobjectApiName + ' WHERE Id = \'' + settingsId + '\'';
 
		List<Clear_Algorithm_Settings__c> clsList = Database.query(query);

		return new AlgorithmSettingsWrapper(clsList[0]);
	}

	//===========================================================================================
	//====================== Query Draft Clear Algorithm Settings ===============================
	//===========================================================================================
	@RemoteAction
	public static List<AlgorithmSettingsWrapper> algorithmSettingsDraftList() {
		String SobjectApiName = 'Clear_Algorithm_Settings__c';
		Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
		Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
		String commaSeperatedFields = '';
		for(String fieldName : fieldMap.keyset()){
			if(commaSeperatedFields == null || commaSeperatedFields == ''){
				commaSeperatedFields = fieldName;
			}else{
				commaSeperatedFields = commaSeperatedFields + ', ' + fieldName;
			}
		}
		commaSeperatedFields = commaSeperatedFields + ', Owner.Name';
 
		String query = 'SELECT ' + commaSeperatedFields + ' FROM ' + SobjectApiName + ' WHERE Status__c = \'Draft\'';
 
		List<Clear_Algorithm_Settings__c> clsList = Database.query(query);

		return AlgorithmSettingsWrapper.wrapCASList(clsList);
	}

	//===========================================================================================
	//================== Query Approved  Clear Algorithm Settings ===============================
	//===========================================================================================
	@RemoteAction
	public static List<AlgorithmSettingsWrapper> algorithmSettingsApprovedList() {
		String SobjectApiName = 'Clear_Algorithm_Settings__c';
		Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
		Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
		String commaSeperatedFields = '';
		for(String fieldName : fieldMap.keyset()){
			if(commaSeperatedFields == null || commaSeperatedFields == ''){
				commaSeperatedFields = fieldName;
			}else{
				commaSeperatedFields = commaSeperatedFields + ', ' + fieldName;
			}
		}
		commaSeperatedFields = commaSeperatedFields + ', Owner.Name';
 
		String query = 'SELECT ' + commaSeperatedFields + ' FROM ' + SobjectApiName + ' WHERE Status__c = \'' + ClearAlgorithmSettingsServices.STATUS_APPROVED + '\'';
 
		List<Clear_Algorithm_Settings__c> clsList = Database.query(query);

		System.debug(clsList);

		return AlgorithmSettingsWrapper.wrapCASList(clsList);
	}

	/**
	* @description  Queries all orders that fall between the fromDate and toDate.
	*		Passes these Ids to the processOrders method 1,600 at a time to 
	*		query for DME Line Items and updatethe object to be simulated on.
	*/
	@RemoteAction
	public static String submitOrdersForBulkSimulation(String cas, String fromDateString, String toDateString) {

		Date fromDate = Date.parse(fromDateString);
		Date toDate = Date.parse(toDateString);

		Datetime fromDateTime = Datetime.newInstance(fromDate.year(), fromDate.month(), fromDate.day());
		Datetime toDateTime = Datetime.newInstance(toDate.year(), toDate.month(), toDate.day());

		List<Id> orderIds = new List<Id>();
		Integer count = 0;
		String simulationId = RandomStringUtils.randomUUID();

		for(Order__c o : [
			SELECT Id 
			FROM Order__c 
			WHERE CreatedDate >=: fromDateTime
			AND CreatedDate <=: toDateTime
			AND RecordType.Id != :OrderModel_CS.simulationRecordTypeId
			AND Recurring__c != true
			AND Stage__c = 'Completed'
		]) {
			orderIds.add(o.Id);
			count++;
			if(count == 1600) {
				System.debug('Ids: ' + orderIds);
				processOrders(cas, orderIds, simulationId);
				count = 0;
				orderIds = new List<Id>();
			}
		}
		System.debug('Ids: ' + orderIds);
		if (orderIds.isEmpty() != true) {
			processOrders(cas, orderIds, simulationId);
		}

		return simulationId;

	}

	/**
	* @description  The processOrders method queries the orders and related DME Line Items.
	*				The orders are cloned and the clones have the simulation status set to ready.
	*/
	@future
	private static void processOrders(String casId, List<Id> orderIds, String simulationId) {
		System.debug('Clear Algorithm Setting: ' + casId);
		System.debug(orderIds);

		// Create dynamic query to grab all fields from the order object.
		SoqlBuilder query = new SoqlBuilder();
		query.selectAll()
			.selectx(new SoqlBuilder()
					.selectx('Id')
					.selectx('Order__c')
					.selectx('Dme__c')
					.selectx('Quantity__c')
					.selectx('Product_Code__c')
				.fromx('DMEs__r')
			)
			.fromx('Order__c')
			.wherex(new SetCondition('Id').inx(orderIds));
		List<Order__c> orders = Database.query(query.toSoql());

		// Clone each order and add to a map from the original Id to the cloned order
		Map<Id, Order__c> orderIdToOrderClone = new Map<Id, Order__c>();

		for(Order__c o : orders) {
			Order__c clonedOrder = o.Clone(false, true, false, false);

			// Update fields and record types for simulation orders
			clonedOrder.Simulation_Group_Id__c = simulationId;
			clonedOrder.Status__c = 'New';
			clonedOrder.RecordTypeId = OrderModel_CS.simulationRecordTypeId;
			clonedOrder.Simulation_Settings__c = casId;

			orderIdToOrderClone.put(o.Id, clonedOrder);
		}
		
		// Insert cloned orders.
		insert orderIdToOrderClone.values();

		// Go through orders and serialize the DME Line Item list from the original order on to the cloned order
		for (Order__c o : orders) {
			if (orderIdToOrderClone.get(o.Id) == null ) {
				continue;
			}
			Order__c clonedOrder = orderIdToOrderClone.get(o.Id);

			List<DmeLineItemWrapper_CS> dmeList = new List<DmeLineItemWrapper_CS>();
			for (DME_Line_Item__c dme : o.DMEs__r) {
				dmeList.add(new DmeLineItemWrapper_CS(clonedOrder.Id, dme));
			}

			clonedOrder.HCPC_Line_Item_JSON__c = Json.serialize(dmeList);
			clonedOrder.Simulation_Original_Order__c = o.Id;
			clonedOrder.Simulation_Status__c = 'Ready';

		}

		System.debug('update size: ' + orderIdToOrderClone.size());
		update orderIdToOrderClone.values();
	}

	//===========================================================================================
	//================== Collect Look Back Period picklist values ===============================
	//===========================================================================================
	@RemoteAction
	public static List<String> collectLookBackPeriodValues() {
		List<String> lookBackPeriodValues = new List<String>();
		for(Schema.PicklistEntry pv : Clear_Algorithm_Settings__c.GA_Look_Back_Period__c.getDescribe().getPicklistValues()) {
			lookBackPeriodValues.add(pv.getValue());
		}
		return lookBackPeriodValues;
	}

	//===========================================================================================
	//================== Collect Modifiers picklist values ======================================
	//===========================================================================================
	@RemoteAction
	public static List<String> collectModifierValues() {
		List<String> modifiers = new List<String>();
		for(Schema.PicklistEntry ple : DME_Line_Item__c.RR_NU__c.getDescribe().getPicklistValues()) {
			modifiers.add(ple.getValue());
		}
		return modifiers;
	}

	//===========================================================================================
	//====================== Clone Clear Algorithm Setting record ===============================
	//===========================================================================================
	@RemoteAction
	public static AlgorithmSettingsWrapper cloneAlgorithmSetting(Clear_Algorithm_Settings__c cas, Boolean isWriteable, Boolean isChanged) {
		if(isWriteable && isChanged) {
			update cas;
		}
		Clear_Algorithm_Settings__c tempClonedCas = cas.clone(false, true, false, false);
		return new AlgorithmSettingsWrapper(tempClonedCas);
	}

	//============================= QUERY / CREATE PROVIDERS LIST ===============================
	@remoteAction
	public static List<Account> getProviderList() {

		List<Account> providerList = [
			SELECT
				Name,
				OwnerId,
				AccountNumber,
				Type_Of_Provider__c,
				Status__c,
				Type__c,
				Daily_Order_Limit__c,
				Owner.Name
			FROM Account
			WHERE Type__c = 'Provider'
			AND Status__c = 'Active'
		];
		return providerList;
	}

	/**
	* @description Queries for the provider for edit 
	*
	* @param providerId 
	*/
	@remoteAction
	public static Account getProviderForEdit(Id providerId) {
		Account provider = [
			SELECT
				Id,
				Name,
				OwnerId,
				AccountNumber,
				Type_Of_Provider__c,
				Status__c,
				Type__c,
				Daily_Order_Limit__c,
				Owner.Name
			FROM Account
			Where Id = :providerId 
			LIMIT 1
		];

		return provider;
	}

	//============================= QUERY Provider Location Hours ===============================
	@remoteAction
	public static List<Provider_Location__c> getProviderLocationHours(Id providerId) {

		List<Provider_Location__c> providerLocationHours = [
			SELECT
				Name,
				Name_DBA__c,
				Sunday_Opening_Time__c,
				Sunday_Closing_Time__c,
				Sunday_Is_Open__c,
				Sunday_Is_24_Hours__c,
				Monday_Opening_Time__c,
				Monday_Closing_Time__c,
				Monday_Is_Open__c,
				Monday_Is_24_Hours__c,
				Tuesday_Opening_Time__c,
				Tuesday_Closing_Time__c,
				Tuesday_Is_Open__c,
				Tuesday_Is_24_Hours__c,
				Wednesday_Opening_Time__c,
				Wednesday_Closing_Time__c,
				Wednesday_Is_Open__c,
				Wednesday_Is_24_Hours__c,
				Thursday_Opening_Time__c,
				Thursday_Closing_Time__c,
				Thursday_Is_Open__c,
				Thursday_Is_24_Hours__c,
				Friday_Opening_Time__c,
				Friday_Closing_Time__c,
				Friday_Is_Open__c,
				Friday_Is_24_Hours__c,
				Saturday_Opening_Time__c,
				Saturday_Closing_Time__c,
				Saturday_Is_Open__c,
				Saturday_Is_24_Hours__c
			FROM Provider_Location__c
			WHERE Account__c = :providerId
		];
		return providerLocationHours;

	}

	//===========================================================================================
	//============================= SAVE Provider Location Hours ===============================
	//===========================================================================================
	@remoteAction
	public static void saveProviderLocationHours(Provider_Location__c providerLocation) {
		update providerLocation;
	}

	//===========================================================================================
	//============================= SAVE Provider Daily Order Limit ===============================
	//===========================================================================================
	@remoteAction
	public static void saveProviderDOL(Id providerId, Integer DOL) {

		Account provider = 
			[
			SELECT
				Daily_Order_Limit__c
			FROM Account
			WHERE Id = :providerId
			];

		provider.Daily_Order_Limit__c = DOL;
		update provider;
	}

	//===========================================================================================
	//============================ QUERY / CREATE PAYER LIST ====================================
	//===========================================================================================
	@remoteAction
	public static List<Account> getPayerList() {
		
		List<Account> payerList = [
			SELECT
				Id,
				Name,
				OwnerId,
				AccountNumber,
				Type_Of_Provider__c,
				Status__c,
				Type__c,
				Owner.Name	
			FROM Account
			WHERE Type__c = 'Payer'
		];
		return payerList;
	}

	/**
	* @description  Validates that the search string is populated and calls out to a query method.
	*/
	@remoteAction
	public static List<DME__c> searchProducts(String searchTerm){
		
		List<DME__c> productSearchResults;

		//Only perform a search if at least one field is populated
		if(String.isNotBlank(searchTerm)){			
			productSearchResults = ProductServices_CS.retrieveDMEProducts(searchTerm);
		}
		else{
			System.debug('searchProducts: No search performed.');
			productSearchResults = new List<DME__c>();
		}
		
		System.debug('searchProducts: Found ' + productSearchResults.size() + ' products');
		System.debug(productSearchResults);
		return productSearchResults;
	}

	//=======================================================================================
	//================================== Search Orders ======================================
	//=======================================================================================
	@remoteAction
	public static List<Order__c> searchOrders(String searchTerm){
		
		List<Order__c> orderSearchResults;

		//Only perform a search if at least one field is populated
		if(String.isNotBlank(searchTerm)){	
			String tempInput =	'%'+searchTerm+'%';	
			orderSearchResults = [
				SELECT
					Id,
					Name,
					CreatedDate,
					City__c,
					Zip__c,
					Street__c,
					State__c,
					Comments__c,
					Provider__r.name,
					Payer__c,
					Payer__r.Name,
					( Select
						Id,
						Name,
						DME__r.Name,	// removed DME__r.DME_Category__c,
						DME__r.Description__c,
						Product_Code__c,
						Product_Name__c
					From DMEs__r)
				FROM Order__c
				WHERE Name LIKE :tempInput
			];
			return orderSearchResults;
		}
		else{
			System.debug('searchOrder: No search performed.');
			orderSearchResults = new List<Order__c>();
		}
		
		System.debug('searchOrders: Found ' + orderSearchResults.size() + ' orders');
		System.debug(orderSearchResults);
		return orderSearchResults;
	}

	


	//===========================================================================================
	//================================== Process Simulation Order ===============================
	//===========================================================================================
	@remoteAction
	public static Id processSimulationOrder(Id settingsId, List<DME__c> selectedProducts, String street, String city, String state, String zip, String comments, Id payer, Id original){
		System.debug(selectedProducts);
		
		Order__c simOrder = New Order__c();
		simOrder.RecordTypeId = Schema.SObjectType.Order__c.getRecordTypeInfosByName().get('Simulation Order').getRecordTypeId();
		simOrder.Authorization_Number__c = RandomStringUtils.randomAlphanumeric(8);
		simOrder.Simulation_Settings__c = settingsId;
		simOrder.Street__c = street;
		simOrder.City__c = city;
		simOrder.State__c = state;
		simOrder.Zip__c = zip;
		simOrder.Payer__c = payer;
		simOrder.Comments__c = comments;
		simOrder.Simulation_Original_Order__c = original;
		System.debug('Comments: ' + comments);
		insert simOrder;
		
		List<DME_Line_Item__c> DMELineItemList = New List<DME_Line_Item__c>();
		List<DmeLineItemWrapper_CS> DMELineItemWrapperList = New List<DmeLineItemWrapper_CS>();
		for (DME__c product : selectedProducts){
			DME_Line_Item__c DMELineItem = New DME_Line_Item__c();
			DMELineItem.Order__c = simOrder.Id;
			DMELineItem.DME__c = product.Id;
			DMELineItem.Quantity__c = 1;
			DMELineItemList.add(DMELineitem);
			DMELineItemWrapperList.add(new DmeLineItemWrapper_CS(DMELineitem));
		}
		insert DMELineItemList;

		System.debug('DMELineitems: ' + DMELineitemList);

		simOrder.Simulation_Status__c = 'Ready';
		simOrder.Status__c = 'New';
		simOrder.HCPC_Line_Item_JSON__c = JSON.serialize(DMELineitemWrapperList);
		update simOrder;
 
		return simOrder.Id;
	}

	//===========================================================================================
	//================================== Simulation Order Results ===============================
	//===========================================================================================
	@remoteAction
	public static String simulationOrderResults(Id simOrderId){
		Order__c simOrder = [SELECT Simulation_Status__c, Simulation_Results__c FROM Order__c WHERE Id =: simOrderId];
		return simOrder.Simulation_Results__c;	
	}

	//===========================================================================================
	//================================== Simulation Order Status ================================
	//===========================================================================================
	@remoteAction
	public static String simulationOrderStatus(Id simOrderId){
		Order__c simOrder = [SELECT Simulation_Status__c FROM Order__c WHERE Id =: simOrderId];
		return simOrder.Simulation_Status__c;
	}

	
	//=======================================================================================
	//================================== Search Payers ======================================
	//=======================================================================================
	@remoteAction
	public static List<Account> getPayers(String searchTerm){
		
		List<Account> Payers;

		//Only perform a search if at least one field is populated
		if(String.isNotBlank(searchTerm)){	
			String tempInput =	'%'+searchTerm+'%';	
			Payers = [
				SELECT
					Id,
					Name
				FROM Account
				WHERE Name LIKE :tempInput AND RecordType.Name = 'Payer Relations'
				AND Status__c = 'Active'
				ORDER BY Name
			];
			return Payers;
		} else {
		//Return all Providers if no search term
			Payers = [
				SELECT
					Id, 
					Name 
				FROM Account 
				WHERE RecordType.Name = 'Payer Relations'
				AND Status__c = 'Active'
				ORDER BY Name
			];
		}
		
		System.debug('searchPayer: Found ' + Payers.size() + ' Payer');
		System.debug(Payers);
		return Payers;
	}

	//=======================================================================================**
	//================================== Search HCPCs =======================================
	//=======================================================================================
	@remoteAction
	public static List<DME__c> getHCPCs(){
		List<DME__c> HCPCs = [
			SELECT 
				Id,
				Name
			FROM DME__c
			WHERE Active__c = true
		];

		return HCPCs;
	}

	//=======================================================================================**
	//================================== Search HCPC Categories =============================
	//=======================================================================================
	@remoteAction
	public static List<HCPC_Database_Category__c> getHCPCCategories(){
		//get Integra HCPC Database Categories

		List<String> providerCategoryNames = new List<String>();
		List<Provider_Category__c> providerCategoriesList = [
			SELECT 
				Name
			FROM Provider_Category__c
		];
		for (Provider_Category__c cat : providerCategoriesList) {
			providerCategoryNames.add(cat.Name);
		}
		List<HCPC_Database_Category__c> HCPCCategories = [
			SELECT 
				Id,
				Name
			FROM HCPC_Database_Category__c
			WHERE Name IN :providerCategoryNames
		];
		return HCPCCategories;
	}

	//===========================================================================================
	//=========================== Add Payer Provider Direct Relationship  =======================
	//===========================================================================================
	@remoteAction
	public static List<Account> addDirectRelationship(Id payerId, Id providerId){
		DirectRelationship__c newDR = new DirectRelationship__c (
			Payer__c = payerId,
			Provider__c = providerId
			);

		insert newDR;

		return getPayersForProviderDirectRelationship(providerId);
	}

	//===========================================================================================**
	//=========================== Add Payor Provider HCPC Direct Relationship  ==================
	//===========================================================================================
	@remoteAction
	public static List<Provider_Payor_HCPC__c> addPayorProviderHCPCDirectRelationship(Id payorId, Id providerId, Id HCPCId){
		boolean isHCPC;

		isHCPC = HCPCId.getSObjectType().getDescribe().getLabel() == 'HCPC';
		
		Provider_Payor_HCPC__c newDR = new Provider_Payor_HCPC__c (
			Payor__c = payorId,
			Provider__c = providerId,
			HCPC__c = isHCPC ? HCPCId : null,
			HCPC_Database_Category__c = !isHCPC ? HCPCId : null
		);

		insert newDR;

		return getHCPCPayersForProviderDirectRelationship(providerId);
	}

	//===========================================================================================
	//=========================== Get Payers for Provider Direct Relationship  ==================
	//===========================================================================================
	@remoteAction
	public static List<Account> getPayersForProviderDirectRelationship(Id providerId){
		
		List<Account> payerList = [
			SELECT Name, Account_Number__c
			FROM Account 
			WHERE id in (SELECT Payer__c
							FROM DirectRelationship__c
							WHERE Provider__c = :providerId)
			];
		
		return payerList;
	}

	//===========================================================================================
	//=========================== Get HCPC Payor Direct Relationship for Provider ===============
	//===========================================================================================
	@remoteAction
	public static List<Provider_Payor_HCPC__c> getHCPCPayersForProviderDirectRelationship(Id providerId){

		List<Provider_Payor_HCPC__c> pphList = [
			SELECT 
				Payor__c,
				Payor__r.Name,
				Payor__r.Account_Number__c,
				Provider__c,
				Provider__r.Name,
				HCPC__c,
				HCPC__r.Name,
				HCPC_Database_Category__c,
				HCPC_Database_Category__r.Name
			FROM Provider_Payor_HCPC__c
			WHERE Provider__c =: providerId
		];
		
		return pphList;
	}

	//===========================================================================================
	//=========================== Delete Provider Direct Relationship  ==========================
	//===========================================================================================
	@remoteAction
	public static List<Account> deleteDirectRelationship(Id payerId, Id providerId){
		
		List<DirectRelationship__c> DirectRelationship = [
			SELECT Id
			FROM DirectRelationship__c 
			WHERE Payer__c = :payerId AND Provider__c = :providerId
		];

		delete DirectRelationship;
		
		return getPayersForProviderDirectRelationship(providerId);
	}

	//===========================================================================================
	//=========================== Delete HCPC Provider Direct Relationship  =====================
	//===========================================================================================
	@remoteAction
	public static List<Provider_Payor_HCPC__c> deleteHCPCDirectRelationship(Id payerId, Id providerId, Id HCPCId){
		
		List<Provider_Payor_HCPC__c> payorHCPCDirectRelationship = [
			SELECT Id
			FROM Provider_Payor_HCPC__c
			WHERE Payor__c = :payerId AND Provider__c = :providerId AND (HCPC__c =: HCPCId OR HCPC_Database_Category__c =: HCPCId)
		];

		System.debug(payorHCPCDirectRelationship);

		delete payorHCPCDirectRelationship;
		
		return getHCPCPayersForProviderDirectRelationship(providerId);
	}

	/* Provider Language methods */

	/**
	* @Description Returns a list of all the language records in Salesforce
	*/
	@remoteAction
	public static List<Language__c> getAvailableLanguages() {
		return [
			SELECT
				Id,
				Name
			FROM Language__c
			ORDER BY Name
		];
	}

	/**
	* @Description Returns the list of langauges that exist for a particular location, and if any duplicates are found, they will be removed.
	*/
	@remoteAction
	public static List<Language__c> getCurrentProviderLangauges(Id providerId) {
		List<Provider_Language__c> providerLangList = [
			SELECT
				Language__r.Id,
				Language__r.Name
			FROM Provider_Language__c
			WHERE Provider_Location__c = :providerId
			ORDER BY Language__r.Name
		];

		Set<Language__c> langSet = new Set<Language__c>();
		List<Language__c> langListToDelete = new List<Language__c>();
		for (Provider_Language__c providerLang : providerLangList) {
			if(!langSet.add(providerLang.Language__r)) {
				langListToDelete.add(providerLang.Language__r);
			}
		}

		List<Language__c> langList = new List<Language__c>();
		langList.addAll(langSet);
		if (!langListToDelete.isEmpty()) {
			delete langListToDelete;
		}
		return langList;
	}

	/**
	* @Description Merges the junction objects between languages and a provider location based on ids passed in.
	*/
	@remoteAction
	public static void saveLanguagesToProviderLocation(List<Id> languageIdList, Id providerId) {
		// Get list of languages to populate name fields
		Map<Id, Language__c> langMap = new Map<Id, Language__c>(getAvailableLanguages());

		// Get existing languages
		List<Provider_Language__c> existingProviderLangauges = [
			SELECT 
				Id,
				Language__c
			FROM Provider_Language__c
			WHERE Provider_Location__c = :providerId
		];
		Map<Id, Provider_Language__c> langIdToProviderLangMap = new Map<Id, Provider_Language__c>();	// Create a map to populate the delete
		for (Provider_Language__c pl : existingProviderLangauges) {
			langIdToProviderLangMap.put(pl.Language__c, pl);
		}

		// Create new provider languages
		List<Provider_Language__c> newProviderLanguages = new List<Provider_Language__c>();
		Set<Id> languageIdSet = new Set<Id>();
		for (Id langId : languageIdList) {
			languageIdSet.add(langId);
			if (!langIdToProviderLangMap.keySet().contains(langId)) {	// If the language does NOT exist, make one to insert
				Provider_Language__c langToAdd = new Provider_Language__c(
					Language__c = langId,
					Name = langMap.get(langId).Name,
					Provider_Location__c = providerId
				);
				newProviderLanguages.add(langToAdd);
			}
		}

		// Delete unused languages
		List<Provider_Language__c> providerLangsToDelete = new List<Provider_Language__c>();
		for (Id langId : langIdToProviderLangMap.keySet()) {
			if (!languageIdSet.contains(langId)) {	// If the set of all current languages does NOT have this language, add it to the delete
				providerLangsToDelete.add(langIdToProviderLangMap.get(langId));
			}
		}

		delete providerLangsToDelete;
		insert newProviderLanguages;
	}


}