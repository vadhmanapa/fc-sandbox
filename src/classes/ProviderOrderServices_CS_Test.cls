@isTest
private class ProviderOrderServices_CS_Test {
	
	public static final Integer N_ORDERS = 3;
	
	public static Account plan;
	public static Account provider;
	public static Contact user;
	public static List<Order__c> orderList;
	public static List<Account> planList;
	public static List<PLan_Patient__c> patientList;
	public static List<Account> providerList;

    static testMethod void ProviderOrderServices_Test_returnAggregateOrdersByStage() {
        init();

        for(Order__c o : orderList){
            o.Status__c = 'Needs Attention - Delivery Information Incomplete';
        }
        update orderList;

        ProviderOrderServices_CS cont = new ProviderOrderServices_CS(user.Id);
        System.assertEquals(N_ORDERS, cont.returnAggregateOrdersByStage('Needs Attention'));
    }
    
    static testMethod void ProviderOrderServices_Test_getOrdersForSummaryNeedsAttention_lastWeek(){
    	init();

		for(Integer i = 0, j = orderList.size(); i < j; i++){
            Order__c o = orderList[i];
			o.Status__c = 'Needs Attention - Delivery Information Incomplete';
            o.Estimated_Delivery_Time__c = System.now().addDays(i);
            Test.setCreatedDate(o.Id, TestDataFactory_CS.getStartOfTheWeek().addDays(-2));
		}
	    update orderList;

        List<Order__c> sortedOrderListByExpectedDeliveryDate = [
            SELECT
                Id
            FROM Order__c
            ORDER BY Estimated_Delivery_Time__c DESC
        ];

    	ProviderOrderServices_CS cont = new ProviderOrderServices_CS(user.Id);
    	
    	ApexPages.Standardsetcontroller setCon = cont.getOrdersForSummary('Needs Attention','Other Attention', user.Id, plan.Id, patientList[0].Name, 'lastWeek');
    	System.AssertEquals(N_ORDERS,setCon.getResultSize());

    	/*System.AssertEquals(setCon.getRecords()[0].Id,sortedOrderListByExpectedDeliveryDate[0].Id);
    	System.AssertEquals(setCon.getRecords()[1].Id,sortedOrderListByExpectedDeliveryDate[1].Id);
    	System.AssertEquals(setCon.getRecords()[2].Id,sortedOrderListByExpectedDeliveryDate[2].Id);*/
    }
    
     static testMethod void ProviderOrderServices_Test_getOrdersForSummaryMyNeedsAttention_lastWeek(){
    	init();

		for(Integer i = 0, j = orderList.size(); i < j; i++){
            Order__c o = orderList[i];
			o.Status__c = 'Needs Attention - Change in Delivery Instructions	';
            o.Estimated_Delivery_Time__c = System.now().addDays(i);
            Test.setCreatedDate(o.Id, TestDataFactory_CS.getStartOfTheWeek().addDays(-2));
		}
	    update orderList;

        List<Order__c> sortedOrderListByExpectedDeliveryDate = [
            SELECT
                Id
            FROM Order__c
            ORDER BY Estimated_Delivery_Time__c DESC
        ];

    	ProviderOrderServices_CS cont = new ProviderOrderServices_CS(user.Id);
    	
    	ApexPages.Standardsetcontroller setCon = cont.getOrdersForSummary('Needs Attention','My Attention', user.Id, plan.Id, patientList[0].Name, 'lastWeek');
    	System.AssertEquals(N_ORDERS,setCon.getResultSize());

    	/*System.AssertEquals(setCon.getRecords()[0].Id,sortedOrderListByExpectedDeliveryDate[0].Id);
    	System.AssertEquals(setCon.getRecords()[1].Id,sortedOrderListByExpectedDeliveryDate[1].Id);
    	System.AssertEquals(setCon.getRecords()[2].Id,sortedOrderListByExpectedDeliveryDate[2].Id);*/
    }

    static testMethod void ProviderOrderServices_Test_getOrdersForSummaryWithSort_thisWeek() {
        init();

        List<Order__c> sortedOrderListByName = [
            SELECT
                Id
            FROM Order__c
            ORDER BY Name ASC
        ];

        ProviderOrderServices_CS cont = new ProviderOrderServices_CS(user.Id);

        ApexPages.Standardsetcontroller setCon = cont.getOrdersForSummaryWithSort('Pending Acceptance','', user.Id, plan.Id, patientList[0].Name, 'thisWeek', new Map<String, String>{'Order #' => 'ASC'});
        System.assertEquals(N_ORDERS, setCon.getResultSize());

        System.AssertEquals(setCon.getRecords()[0].Id,sortedOrderListByName[0].Id);
        System.AssertEquals(setCon.getRecords()[1].Id,sortedOrderListByName[1].Id);
        System.AssertEquals(setCon.getRecords()[2].Id,sortedOrderListByName[2].Id);
    }

    static testMethod void ProviderOrderServices_Test_getOrdersForSummary_thisMonth() {
        init();

        ProviderOrderServices_CS cont = new ProviderOrderServices_CS(user.Id);

        ApexPages.Standardsetcontroller setCon = cont.getOrdersForSummary('Pending Acceptance','', user.Id, plan.Id, patientList[0].Name, 'thisMonth');
        System.assertEquals(N_ORDERS, setCon.getResultSize());
    }

    static testMethod void ProviderOrderServices_Test_getOrdersForSummary_lastMonth() {
        init();

        for(Order__c o : orderList){
            o.Status__c = 'New';
            Test.setCreatedDate(o.Id, DateTime.newInstance(Date.today().toStartOfMonth().addMonths(-1).addDays(3),Time.newInstance(0,0,0,0)));
        }

        update orderList;

        ProviderOrderServices_CS cont = new ProviderOrderServices_CS(user.Id);

        ApexPages.Standardsetcontroller setCon = cont.getOrdersForSummary('Pending Acceptance','', user.Id, plan.Id, patientList[0].Name, 'lastMonth');
        System.assertEquals(N_ORDERS, setCon.getResultSize());
    }

    static testMethod void ProviderOrderServices_Test_getOrdersForSummary_someTimeFrame() {
        init();

        ProviderOrderServices_CS cont = new ProviderOrderServices_CS(user.Id);

        ApexPages.Standardsetcontroller setCon = cont.getOrdersForSummary('Pending Acceptance','', user.Id, plan.Id, patientList[0].Name, 'someTimeFrame');
        System.assertEquals(N_ORDERS, setCon.getResultSize());
    }
    
    static testMethod void ProviderOrderServices_Test_getOrderListForSummary_thisWeek(){
    	init();
    	ProviderOrderServices_CS cont = new ProviderOrderServices_CS(user.Id);
    	
    	List<Order__c> orderSummaryList = cont.getOrderListForSummary('Pending Acceptance','', user.Id, plan.Id, patientList[0].Name, 'thisWeek');
    	System.assertEquals(N_ORDERS, orderSummaryList.size());
    }

    static testMethod void ProviderOrderServices_Test_getOrderListForSummary_lastWeek(){
        init();

        for(Order__c o : orderList){
            Test.setCreatedDate(o.Id, TestDataFactory_CS.getStartOfTheWeek().addDays(-4));
        }

        ProviderOrderServices_CS cont = new ProviderOrderServices_CS(user.Id);

        List<Order__c> orderSummaryList = cont.getOrderListForSummary('Pending Acceptance','', user.Id, plan.Id, patientList[0].Name, 'lastWeek');
    }

    static testMethod void ProviderOrderServices_Test_getOrderListForSummary_thisMonth(){
        init();

        ProviderOrderServices_CS cont = new ProviderOrderServices_CS(user.Id);

        List<Order__c> orderSummaryList = cont.getOrderListForSummary('Pending Acceptance','', user.Id, plan.Id, patientList[0].Name, 'thisMonth');
        System.assertEquals(N_ORDERS, orderSummaryList.size());
    }

    static testMethod void ProviderOrderServices_Test_getOrderListForSummary_lastMonth(){
        init();

        for(Order__c o : orderList){
            o.Status__c = 'New';
            Test.setCreatedDate(o.Id, DateTime.newInstance(Date.today().toStartOfMonth().addMonths(-1).addDays(3),Time.newInstance(0,0,0,0)));
        }

        update orderList;

        ProviderOrderServices_CS cont = new ProviderOrderServices_CS(user.Id);

        List<Order__c> orderSummaryList = cont.getOrderListForSummary('Pending Acceptance','', user.Id, plan.Id, patientList[0].Name, 'lastMonth');
        System.assertEquals(N_ORDERS, orderSummaryList.size());
    }

    /*static testMethod void ProviderOrderServices_Test_getOrderListForSummary_someTimeFrame(){
        init();

        ProviderOrderServices_CS cont = new ProviderOrderServices_CS(user.Id);

        List<Order__c> orderSummaryList = cont.getOrderListForSummary('Pending Acceptance','', user.Id, plan.Id, patientList[0].Name, 'someTimeFrame');
        System.assertEquals(N_ORDERS, orderSummaryList.size());
    }*/
    
    public static void init(){
    	planList = TestDataFactory_CS.generatePlans('Test Plan', 1);
    	plan = planList[0];
    	insert planList;
    	
    	patientList = TestDataFactory_CS.generatePlanPatientsPlanOnly(plan.Id, 1);
    	insert patientList;
    	
    	providerList = TestDataFactory_CS.generateProviders('Test Provider', 1);
    	provider = providerList[0];
    	insert providerList;
    	
    	user = TestDataFactory_CS.generateProviderContacts('Admin', providerList, 1)[0];
    	insert user;
    	
    	//Give user credentials
    	UserModel_CS portalUser = new UserModel_CS(user);
    	portalUser.resetPassword();
    	
    	orderList = TestDataFactory_CS.generateOrders(patientList[0].Id, provider.Id, N_ORDERS);
    	for(Order__c o : orderList){
    		o.Accepted_By__c = user.Id;
    		o.Payer__c = plan.Id;
    	}

    	orderList[0].LastModifiedDate_Notes__c = DateTime.newInstance(2000, 1, 1, 0, 0, 0);
    	orderList[1].LastModifiedDate_Notes__c = DateTime.newInstance(2001, 1, 1, 0, 0, 0);
    	orderList[2].LastModifiedDate_Notes__c = DateTime.newInstance(2002, 1, 1, 0, 0, 0);
    	insert orderList;
    }
}