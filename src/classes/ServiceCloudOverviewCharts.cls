public class ServiceCloudOverviewCharts {
        Datetime myDateTime = Datetime.now();
        Datetime myDate = Date.today();
        Datetime newdate = myDateTime.addDays(-2);
    List<User> csUsers = [Select Id from User where Profile.Name = 'Customer Service w Service Cloud'];
    public List<PieWedgeData> getPieData(){
        List<PieWedgeData> data = new List<PieWedgeData>();
        List<AggregateResult> calls = [Select count(Id)i, Owner.Name cn from Call_Log__c where RecordType.Name = 'Customer Support'   AND OwnerId =:csUsers AND CreatedDate = TODAY GROUP BY Owner.Name ORDER BY count(Id) ASC];
        for (AggregateResult c : calls){
            data.add(new PieWedgeData((String)c.get('cn'), (Integer)c.get('i')));
        }
        return data;
    }
    public class PieWedgeData{
        public String name {get; set;}
        public Integer data {get; set;}
        public PieWedgeData(String name, Integer data){
            this.name = name;
            this.data = data;
        }
    }
    
    public List<BarSeriesData> getBarData(){
        List<BarSeriesData> barValues = new List<BarSeriesData>();
        List<AggregateResult> openCases = [Select count(id)openCases, Owner.Name openBy from Case where OwnerId =: csUsers AND Status != 'Closed' AND RecordType.Name != 'Complaint'  GROUP BY Owner.Name ];
        for (AggregateResult ca : openCases){
            barValues.add(new BarSeriesData((String)ca.get('openBy'), (Integer)ca.get('openCases')));
        }
        
        return barValues;
    }
    public class BarSeriesData{
        public String name {get; set;}
        public Integer bar {get; set;}
        public BarSeriesData(String name, Integer bar){
            this.name = name;
            this.bar = bar;
        }
    }
    
    public List<OpenEmailBarChart> getCaseData(){
        List<OpenEmailBarChart> emailValues = new List<OpenEmailBarChart>();
        List<AggregateResult> openEmail = [Select count(id)openEmail, Owner.FirstName openBy from Case where RecordType.Name = 'Email'  AND OwnerId =:csUsers AND Status != 'Closed' GROUP BY Owner.FirstName ORDER BY count(Id) ASC ];
        for (AggregateResult ca : openEmail){
            emailValues.add(new OpenEmailBarChart((String)ca.get('openBy'), (Integer)ca.get('openEmail')));
        }
        return emailValues;
    }
    public class OpenEmailBarChart{
        public String name {get; set;}
        public Integer emailCount {get; set;}
        public OpenEmailBarChart(String name, Integer emailCount){
            this.name = name;
            this.emailCount = emailCount;
        }
    }
}