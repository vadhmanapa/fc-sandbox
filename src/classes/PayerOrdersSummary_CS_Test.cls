@isTest
private class PayerOrdersSummary_CS_Test {
	public static Account plan;
	public static Account provider;
	public static Contact user;
	public static List<Order__c> orderList;
	
	
    static testMethod void PayerOrdersSummary_CS_PageLanding() {
        init();
		
		//Page Landing
		PageReference pr = Page.PayerOrdersSummary_CS;
		Test.setCurrentPage(pr);
		TestServices_CS.login(user);
		
		//Page Init
        PayerOrdersSummary_CS cont = new PayerOrdersSummary_CS();		
		cont.init();
    }
    
    static testMethod void PayerOrdersSummary_CS_PageLandingNoAuth(){
    	init();
    	
    	//Page Landing, no login
    	PageReference pr = Page.PayerOrdersSummary_CS;
		Test.setCurrentPage(pr);
    	
    	//Page Init
    	PayerOrdersSummary_CS cont = new PayerOrdersSummary_CS();    	
    	cont.init();
    }
    
    static testMethod void PayerOrdersSummary_CS_initWithFilter(){
    	init();
    	
    	//Page Landing with filter param
    	PageReference pr = Page.PayerOrdersSummary_CS;
    	pr.getParameters().put('filter', 'Pending Acceptance');
		Test.setCurrentPage(pr);
		TestServices_CS.login(user);
		
		//Page Init		
    	PayerOrdersSummary_CS cont = new PayerOrdersSummary_CS();		
		cont.init();
    }
    
    static testMethod void PayerOrdersSummary_CS_FilterByUser(){
    	init();
    	
    	//Page Landing with filter param
    	PageReference pr = Page.PayerOrdersSummary_CS;
		Test.setCurrentPage(pr);
		TestServices_CS.login(user);
		
		//Page Init		
    	PayerOrdersSummary_CS cont = new PayerOrdersSummary_CS();		
		cont.init();
		
		//Filter will be set up for User by default, select a user
		cont.filterByUser = cont.filterByUserOptions[0].getValue();
		System.debug('Selected User: ' + cont.filterByUserOptions[0].getValue());
		
		cont.populateOrderList();
        List<Order__c> newOrdersList = cont.orderList;
        System.assertEquals(orderList.size(), newOrdersList.size());
    }
    
    static testMethod void PayerOrdersSummary_CS_FilterByAccount(){
    	init();
    	
    	//Page Landing with filter param
    	PageReference pr = Page.PayerOrdersSummary_CS;
		Test.setCurrentPage(pr);
		TestServices_CS.login(user);
		
		//Page Init		
    	PayerOrdersSummary_CS cont = new PayerOrdersSummary_CS();		
		cont.init();

		List<SelectOption> filterByEntityType = cont.filterByEntityTypeOptions;
		//Filter will be set up for User by default, change to account
		cont.filterByEntityType = 'Provider';
		
		//Select an account
		cont.filterByProvider = cont.filterByProviderOptions[0].getValue();
		System.debug('Selected Provider: ' + cont.filterByUserOptions[0].getValue());
		
		//Repopulate orderList
		cont.populateOrderList();
        cont.pageSize = 10;
        String userFilter = cont.userFilter;
        String selectedOrderId = cont.selectedOrderId;
    }
    
    public static void init() {
    	List<Account> planList = TestDataFactory_CS.generatePlans('Test Plan', 1);
    	plan = planList[0];
    	insert planList;
    	
    	List<Plan_Patient__c> patientList = TestDataFactory_CS.generatePlanPatientsPlanOnly(plan.Id, 1);
    	insert patientList;
    	
    	List<Account> provList = TestDataFactory_CS.generateProviders('Test Provider', 1);
    	provider = provList[0];
    	insert provList;
    	
    	user = TestDataFactory_CS.createPlanContacts('Admin', planList, 1)[0];
    	
    	orderList = TestDataFactory_CS.generateOrders(patientList[0].Id, provider.Id, 3);
    	for(Order__c o : orderList){
    		o.Entered_By__c = user.Id;
    		o.Case_Manager__c = user.Id;
    		o.Status__c = 'New';
    	}
    	insert orderList;
	}	
}