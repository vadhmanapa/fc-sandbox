/*===============================================================================================
 * Cloud Software LLC
 * Name: ProviderLocationHoursController_Test
 * Description: Test Class for the Controller class for ProviderLocationHours page
 * Created Date: 12/04/2015
 * Created By: Kelly Sharp (Cloud Software)
 * 
 * 	Date Modified			Modified By			Description of the update
 *	12/04/2015				Kelly Sharp 		Created
 ==============================================================================================*/


@isTest
public class ProviderLocationHoursController_Test { 


	static testMethod void test1() {

		 //ASSUME

		 //create location

		Global_Switches__c gs = new Global_Switches__c(
			Name = 'Defaults',
			Create_ProvLoc_Trigger_Off__c = true
		);
		insert gs;

		Account prov = TestDataFactory_CS.generateProviders('testProvider', 1)[0];
		insert prov;

		Provider_Location__c provLoc = TestDataFactory_CS.generateProviderLocations(prov.Id, 1)[0];
		provLoc.Sunday_Opening_Time__c = 17;
		provLoc.Sunday_Closing_Time__c = 17.5;
		provLoc.Sunday_Is_24_Hours__c = false;
		provLoc.Sunday_Is_Open__c = true;
		provLoc.Monday_Opening_Time__c  =  17; 
		provLoc.Monday_Closing_Time__c  =  17.5; 
		provLoc.Monday_Is_Open__c  =  true; 
		provLoc.Monday_Is_24_Hours__c = false;
		provLoc.Tuesday_Opening_Time__c  =  17; 
		provLoc.Tuesday_Closing_Time__c  =  17.5; 
		provLoc.Tuesday_Is_Open__c = true;
		provLoc.Tuesday_Is_24_Hours__c = false;
		provLoc.Wednesday_Opening_Time__c  =  17; 
		provLoc.Wednesday_Closing_Time__c  =  17.5; 				
		provLoc.Wednesday_Is_Open__c = true;
		provLoc.Wednesday_Is_24_Hours__c = false;
		provLoc.Thursday_Opening_Time__c  =  17; 
		provLoc.Thursday_Closing_Time__c  =  17.5 ; 
		provLoc.Thursday_Is_Open__c = true;
		provLoc.Thursday_Is_24_Hours__c = false;
		provLoc.Friday_Opening_Time__c  =  17; 
		provLoc.Friday_Closing_Time__c  =  17.5 ; 
		provLoc.Friday_Is_Open__c = true;
		provLoc.Friday_Is_24_Hours__c = false;
		provLoc.Saturday_Opening_Time__c  =  17; 
		provLoc.Saturday_Closing_Time__c  =  17.5 ; 
		provLoc.Saturday_Is_Open__c = true;
		provLoc.Saturday_Is_24_Hours__c = false;
		insert provLoc;



		//Get location Id
		
		Id locationId = [SELECT Id FROM Provider_Location__c LIMIT 1].id;
		// WHEN
		Test.setCurrentPageReference(new PageReference('Page.ProviderLocationHours'));
		System.currentPageReference().getParameters().put('id', locationId);

		Test.startTest();
			ApexPages.StandardController stCon = new ApexPages.StandardController(provLoc);
			ProviderLocationHoursController testCon = new ProviderLocationHoursController(stCon);
			testCon.timeToDec();
			testCon.store.Saturday_Opening_Time__c  =  5; 
			testCon.store.Saturday_Closing_Time__c  =  5.5 ; 
			testCon.updateLocation();
		Test.stopTest();

		// THEN
		system.assertEquals(17 ,testCon.store.Sunday_Opening_Time__c);
		system.assertEquals(17.5 ,testCon.store.Sunday_Closing_Time__c);
		system.assertEquals(false ,testCon.store.Sunday_Is_24_Hours__c);
		system.assertEquals(true ,testCon.store.Sunday_Is_Open__c);
		system.assertEquals(5 ,testCon.open_sunday_hrs);
		system.assertEquals(0 ,testCon.open_sunday_min);
		system.assertEquals('pm' ,testCon.open_sunday_ampm);
		system.assertEquals(5 ,testCon.closing_sunday_hrs);
		system.assertEquals(.5 ,testCon.closing_sunday_min);
		system.assertEquals('pm' ,testCon.closing_sunday_ampm);
		system.assertEquals(5 ,testCon.store.Saturday_Opening_Time__c);
		system.assertEquals(5.5 ,testCon.store.Saturday_Closing_Time__c);


	}
	 
		static testMethod void test2() {

		 //ASSUME

		 //create location

		Global_Switches__c gs = new Global_Switches__c(
			Name = 'Defaults',
			Create_ProvLoc_Trigger_Off__c = true
		);
		insert gs;

		Account prov = TestDataFactory_CS.generateProviders('testProvider', 1)[0];
		insert prov;

		Provider_Location__c provLoc = TestDataFactory_CS.generateProviderLocations(prov.Id, 1)[0];
		provLoc.Sunday_Opening_Time__c = 7;
		provLoc.Sunday_Closing_Time__c = 7.5;
		provLoc.Sunday_Is_24_Hours__c = false;
		provLoc.Sunday_Is_Open__c = true;
		provLoc.Monday_Opening_Time__c  =  7; 
		provLoc.Monday_Closing_Time__c  =  7.5; 
		provLoc.Monday_Is_Open__c  =  true; 
		provLoc.Monday_Is_24_Hours__c = false;
		provLoc.Tuesday_Opening_Time__c  =  7; 
		provLoc.Tuesday_Closing_Time__c  =  7.5; 
		provLoc.Tuesday_Is_Open__c = true;
		provLoc.Tuesday_Is_24_Hours__c = false;
		provLoc.Wednesday_Opening_Time__c  =  7; 
		provLoc.Wednesday_Closing_Time__c  =  7.5; 				
		provLoc.Wednesday_Is_Open__c = true;
		provLoc.Wednesday_Is_24_Hours__c = false;
		provLoc.Thursday_Opening_Time__c  =  7; 
		provLoc.Thursday_Closing_Time__c  =  7.5 ; 
		provLoc.Thursday_Is_Open__c = true;
		provLoc.Thursday_Is_24_Hours__c = false;
		provLoc.Friday_Opening_Time__c  =  7; 
		provLoc.Friday_Closing_Time__c  =  7.5 ; 
		provLoc.Friday_Is_Open__c = true;
		provLoc.Friday_Is_24_Hours__c = false;
		provLoc.Saturday_Opening_Time__c  =  7; 
		provLoc.Saturday_Closing_Time__c  =  7.5 ; 
		provLoc.Saturday_Is_Open__c = true;
		provLoc.Saturday_Is_24_Hours__c = false;
		insert provLoc;

		//Get location Id
		
		Id locationId = [SELECT Id FROM Provider_Location__c LIMIT 1].id;
		// WHEN
		Test.setCurrentPageReference(new PageReference('Page.ProviderLocationHours'));
		System.currentPageReference().getParameters().put('id', locationId);

		ApexPages.StandardController stCon = new ApexPages.StandardController(provLoc);
		ProviderLocationHoursController testCon = new ProviderLocationHoursController(stCon);

		testCon.store.Sunday_Opening_Time__c = 8;
		testCon.store.Sunday_Closing_Time__c = 7.5;
		testCon.store.Sunday_Is_24_Hours__c = false;
		testCon.store.Sunday_Is_Open__c = true;
		testCon.store.Monday_Opening_Time__c  =  8; 
		testCon.store.Monday_Closing_Time__c  =  7.5; 
		testCon.store.Monday_Is_Open__c  =  true; 
		testCon.store.Monday_Is_24_Hours__c = false;
		testCon.store.Tuesday_Opening_Time__c  =  8; 
		testCon.store.Tuesday_Closing_Time__c  =  7.5; 
		testCon.store.Tuesday_Is_Open__c = true;
		testCon.store.Tuesday_Is_24_Hours__c = false;
		testCon.store.Wednesday_Opening_Time__c  = 8; 
		testCon.store.Wednesday_Closing_Time__c  =  7.5; 				
		testCon.store.Wednesday_Is_Open__c = true;
		testCon.store.Wednesday_Is_24_Hours__c = false;
		testCon.store.Thursday_Opening_Time__c  =  8; 
		testCon.store.Thursday_Closing_Time__c  =  7.5 ; 
		testCon.store.Thursday_Is_Open__c = true;
		testCon.store.Thursday_Is_24_Hours__c = false;
		testCon.store.Friday_Opening_Time__c  =  8; 
		testCon.store.Friday_Closing_Time__c  =  7.5 ; 
		testCon.store.Friday_Is_Open__c = true;
		testCon.store.Friday_Is_24_Hours__c = false;
		testCon.store.Saturday_Opening_Time__c  =  8; 
		testCon.store.Saturday_Closing_Time__c  =  7.5 ; 
		testCon.store.Saturday_Is_Open__c = true;
		testCon.store.Saturday_Is_24_Hours__c = false;

		Test.startTest();
			
			testCon.timeToDec();
			
		Test.stopTest();

		// THEN
		/*system.assertEquals(9 ,testCon.store.Sunday_Opening_Time__c);
		system.assertEquals(17.5 ,testCon.store.Sunday_Closing_Time__c);
		system.assertEquals(false ,testCon.store.Sunday_Is_24_Hours__c);
		system.assertEquals(true ,testCon.store.Sunday_Is_Open__c);
		system.assertEquals(9 ,testCon.open_sunday_hrs);
		system.assertEquals(0 ,testCon.open_sunday_min);
		system.assertEquals('am' ,testCon.open_sunday_ampm);
		system.assertEquals(5 ,testCon.closing_sunday_hrs);
		system.assertEquals(.5 ,testCon.closing_sunday_min);
		system.assertEquals('pm' ,testCon.closing_sunday_ampm);*/



		

	}

	static testMethod void test3() {

		 //ASSUME

		 //create location

		Global_Switches__c gs = new Global_Switches__c(
			Name = 'Defaults',
			Create_ProvLoc_Trigger_Off__c = true
		);
		insert gs;

		Account prov = TestDataFactory_CS.generateProviders('testProvider', 1)[0];
		insert prov;

		Provider_Location__c provLoc = TestDataFactory_CS.generateProviderLocations(prov.Id, 1)[0];
	
		insert provLoc;

		//Get location Id
		
		Id locationId = [SELECT Id FROM Provider_Location__c LIMIT 1].id;
		// WHEN
		Test.setCurrentPageReference(new PageReference('Page.ProviderLocationHours'));
		System.currentPageReference().getParameters().put('id', locationId);

		Test.startTest();
			ApexPages.StandardController stCon = new ApexPages.StandardController(provLoc);
			ProviderLocationHoursController testCon = new ProviderLocationHoursController(stCon);
			testCon.timeToDec();
			
		Test.stopTest();

		// THEN
		/*system.assertEquals(9 ,testCon.store.Sunday_Opening_Time__c);
		system.assertEquals(17.5 ,testCon.store.Sunday_Closing_Time__c);
		system.assertEquals(false ,testCon.store.Sunday_Is_24_Hours__c);
		system.assertEquals(true ,testCon.store.Sunday_Is_Open__c);
		system.assertEquals(9 ,testCon.open_sunday_hrs);
		system.assertEquals(0 ,testCon.open_sunday_min);
		system.assertEquals('am' ,testCon.open_sunday_ampm);
		system.assertEquals(5 ,testCon.closing_sunday_hrs);
		system.assertEquals(.5 ,testCon.closing_sunday_min);
		system.assertEquals('pm' ,testCon.closing_sunday_ampm);*/



		

	}



}