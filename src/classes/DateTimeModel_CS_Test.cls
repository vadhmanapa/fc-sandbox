/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class DateTimeModel_CS_Test {

    static testMethod void DateTimeModel_CS_Test_Now() {

		for(String locale : DateTimeModel_CS.localeToGMTOffset.keySet()){
			
			Integer offset = DateTimeModel_CS.gmtOffset(locale);
			Time t = DateTimeModel_CS.now(locale);

			System.debug(t);
			System.debug(DateTime.now().addHours(offset).timeGmt());

			System.assertEquals(t.hour(),DateTime.now().addHours(offset).timeGmt().hour());
			System.assertEquals(t.minute(),DateTime.now().addHours(offset).timeGmt().minute());		
		}
    }

    static testMethod void DateTimeModel_CS_Test_Today() {

		for(String locale : DateTimeModel_CS.localeToGMTOffset.keySet()){
			
			Integer offset = DateTimeModel_CS.gmtOffset(locale);
			Date d = DateTimeModel_CS.today(locale);

			System.assertEquals(d,DateTime.now().addHours(offset).dateGmt());			
		}
    }
    
    static testMethod void DateTimeModel_CS_Test_LocalTime() {

		DateTime dt = DateTime.now();

		for(String locale : DateTimeModel_CS.localeToGMTOffset.keySet()){
			
			Integer offset = DateTimeModel_CS.gmtOffset(locale);
			DateTimeModel_CS dtm = new DateTimeModel_CS(dt,locale);

			System.assertEquals(dtm.localTime().hour(),DateTime.now().addHours(offset).timeGmt().hour());
			System.assertEquals(dtm.localTime().minute(),DateTime.now().addHours(offset).timeGmt().minute());		
		}
    }
    
    static testMethod void DateTimeModel_CS_Test_LocalDate() {

		DateTime dt = DateTime.now();

		for(String locale : DateTimeModel_CS.localeToGMTOffset.keySet()){
			
			Integer offset = DateTimeModel_CS.gmtOffset(locale);
			DateTimeModel_CS dtm = new DateTimeModel_CS(dt,locale);

			System.assertEquals(dtm.localDate(),DateTime.now().addHours(offset).dateGmt());			
		}
    }
    
    static testMethod void DateTimeModel_CS_Test_Format() {

		DateTime dt = DateTime.now();

		for(String locale : DateTimeModel_CS.localeToGMTOffset.keySet()){
			
			Integer offset = DateTimeModel_CS.gmtOffset(locale);
			DateTimeModel_CS dtm = new DateTimeModel_CS(dt,locale);

			System.assertEquals(dtm.format(),dtm.dt.addHours(offset).formatGmt('M/dd/yyyy HH:mm ') + locale);
			System.assertEquals(dtm.format('MM/dd/yyyy HH:mm'),dtm.dt.format('MM/dd/yyyy HH:mm',locale));			
		}
    }
}
/*
DateTimeModel_CS startDate = DateTimeModel_CS.newInstanceFromLocal(Date.today(), Time.newInstance(0,0,0,0), 'US/Arizona');
DateTimeModel_CS endDate = startDate.addDays(7);

System.debug('StartDate: ' + startDate.format());
System.debug('EndDate: ' + endDate.format());

DateTimeModel_CS testDate = startDate;
System.debug(LoggingLevel.ERROR, testDate.format('EEEEEE MM/dd/yyyy'));

while(testDate.dt < endDate.dt){
    
    DateTime expiresAt = ProviderAssignmentServices_CS.getExpirationTime(testDate.dt,'US/Eastern');

    System.debug(LoggingLevel.ERROR, testDate.format('EEE MM/dd/yyyy hh:mm aa zzzz') + ' => ' + expiresAt.format('EEE MM/dd/yyyy hh:mm aa zzzz'));
    
    Integer minutesToAdd = Integer.valueOf(Math.roundToLong(Math.random() * 60));
    
    DateTimeModel_CS newTestDate = testDate.addMinutes(minutesToAdd);
    if(testDate.localDate() != newTestDate.localDate()){
        System.debug(LoggingLevel.ERROR, newTestDate.format('EEEEEE MM/dd/yyyy'));
    }
   	testDate = newTestDate;
}
*/