@isTest
private class ProviderAccountProfile_CS_Test {
	public static Account provider;
	public static Contact user;
	
    static testMethod void ProviderAccountProfile_CS_Test() {
        init();
        Test.setCurrentPage(Page.ProviderAccountProfile_CS);
        TestServices_CS.login(user);
        
        ProviderAccountProfile_CS cont = new ProviderAccountProfile_CS();
        cont.init();
    }
    
    static testMethod void ProviderAccountProfile_CS_initNotAuthenticated(){
    	init();
    	Test.setCurrentPage(Page.ProviderAccountProfile_CS);
    	
    	ProviderAccountProfile_CS cont = new ProviderAccountProfile_CS();
    	cont.init();
    }
    
    static testMethod void ProviderAccountProfile_CS_initMismatch(){
    	init();
    	Account plan = TestDataFactory_CS.generatePlans('TestPlan', 1)[0];
    	insert plan;
    	user.Entity__c = plan.Id;
    	update user;
    	Test.setCurrentPage(Page.ProviderAccountProfile_CS);
    	TestServices_CS.login(user);
    	
    	ProviderAccountProfile_CS cont = new ProviderAccountProfile_CS();
    	cont.init();
    }
    
    public static void init(){
    	List<Account> initAccts = TestDataFactory_CS.generateProviders('TestProv', 1);
    	provider = initAccts[0];
    	insert provider;
    	
    	user = TestDataFactory_CS.createProviderContacts('Admin', initAccts, 1)[0];
    }
}