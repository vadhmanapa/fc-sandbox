/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestCreateARAG_IP {

    static testMethod void createARAG() {
        // TO DO: implement unit test
        User testUser = new User(FirstName = 'test', LastName = 'check', Username = 'testcheck@IntegraTest.com', Email = 'test@test.com', Alias = 'test',
                                                       CommunityNickname = 'testforlog', TimeZoneSidKey = 'America/Los_Angeles', EmailEncodingKey = 'ISO-8859-1',
                                                       ProfileId = '00e300000028QgNAAU', LanguageLocaleKey = 'en_US', LocaleSidKey = 'en_US');
		insert testUser;
		String userId = testUser.Id;

		Account testProvider = new Account(Name = 'Test Claim Provider', Type_Of_Provider__c = 'DME');
    	insert testProvider;
    	String providerId = testProvider.Id;

    	Account testPayor = new Account(Name = 'Test Claim Payor', Type_Of_Provider__c = 'DME');
    	insert testPayor;
    	String payorId = testPayor.Id;

    	PADR__c testClaim = new PADR__c (Provider__c = providerId, Health_Plan__c = payorId, Bill_Number__c = '12345',
    									 Claim_Closed__c = false, OwnerId = userId);
    	insert testClaim;
    	String claimId = testClaim.Id;
    	String billNum = testClaim.Bill_Number__c;

    	testClaim.Claim_Closed__c = true;
    	update testClaim;
    	
    	PADR__c searchClaim1 = [Select Id, Related_ARAG__r.Id from PADR__c where Id =:claimId];

    	 String aragId = searchClaim1.Related_ARAG__r.Id;

    	List<ARAG__c> claimLog = [ SELECT Id, Bill__c FROM ARAG__c WHERE Id =: aragId LIMIT 1];
    	for( ARAG__c log : claimLog ){
        	 System.assertEquals(1, claimLog.size(), 'The ARAG was created');
        	// System.assertEquals(billNum, log.Bill__c, 'The record was mapped to correct ARAG');
    	}
        	
         PADR__c testClaim1 = new PADR__c (Provider__c = providerId, Health_Plan__c = payorId, Bill_Number__c = '09876',
    									 Claim_Closed__c = false, OwnerId = userId);
        insert testClaim1;
        String claimId1 = testClaim1.Id;
        String billNum1 = testClaim1.Bill_Number__c;

        testClaim1.Claim_Closed__c = true;
        update testClaim1;

        PADR__c searchClaim2 = [Select Id, Related_ARAG__r.Id from PADR__c where Id =:claimId1];
		String aragId1 = searchClaim2.Related_ARAG__r.Id;

        List<ARAG__c> claimLog1 = [ SELECT Id, Bill__c FROM ARAG__c WHERE Id =: aragId1 LIMIT 1];
    	for( ARAG__c log1 : claimLog1 ){
        	 System.assertEquals(1, claimLog1.size(), 'The ARAG was created');
        	 System.assertEquals(billNum1, log1.Bill__c, 'The ARAG was mapped to correct existing bill');
    	}
    }
}