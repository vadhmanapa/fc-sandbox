public with sharing class NewCaseEntryPageControllerExtension_IP extends PageControllerBase_IP{

    public Enum callerType {Patient, Provider, Payor, Family, Facility}
    public String selectedCallerType {get
        {
            if(String.isBlank(selectedCallerType)) {
                selectedCallerType = 'None';
            }

            return selectedCallerType;
        } set;
    }
	
    // CONTACT VARIABLES
    public Contact callerContactInfo {get;set;}
    public Contact familyContactInfo {get;set;}
    public Contact newCallerContact {get;set;}
    public Contact newFamilyContact {get;set;}
    public Boolean isConInsertSucess {get;set;}

    public Datetime startTime{get;set;} // TO CAPTURE TRUE TIME OF AGENT'S LOG START
    private static Id intakeId;

    //VARIABLES TO AID RENDERING
    public Boolean isNewContact {get;set;}
    public Boolean isNewFamilyContact {get;set;}
    public Boolean nonCredContact {get;set;}
    public Boolean nonCredAccount {get;set;}

    private String parentId {get;set;}
    public Boolean isCloneCase {get;set;}
    public Boolean isNonClaimInquiry {get;set;}
    private String parentCaseRedirect;
    private Boolean reDirectToParent;
    // public Boolean isAdditionalDetailsRequired {get;set;}

    public PageReference newPg {get;set;} // Page redirect
    public String actionDetailId;

    // CUSTOM COMPONENT CONTROLLER COMMUNICATION --> https://developer.salesforce.com/page/Controller_Component_Communication
    public CaseReasonComponentController_IP reasonCompController {set;
        get{
            if(getComponentControllerMap() != null) {
                CaseReasonComponentController_IP crc;
                crc = (CaseReasonComponentController_IP)getComponentControllerMap().get('reasonComponent');
                if(crc != null) {
                    return crc;
                }
            }
            return new CaseReasonComponentController_IP();
        }
    }

    public WhoIsThisCallAboutController_IP whoIsThisCallAbout {set;
        get{
            if(getComponentControllerMap() != null) {
                WhoIsThisCallAboutController_IP callAbout;
                callAbout = (WhoIsThisCallAboutController_IP)getComponentControllerMap().get('whoIsThisCallAboutComponent');
                if(callAbout != null) {
                    return callAbout;
                }
            }

            return new WhoIsThisCallAboutController_IP();
        }
    }

    public AdditionalDetailsController_IP additionalDetailsList{set;
        get{
            if(getComponentControllerMap() != null) {
                AdditionalDetailsController_IP aDetails;
                aDetails = (AdditionalDetailsController_IP)getComponentControllerMap().get('additionalDetailsComponent');
                if(aDetails != null) {
                    return aDetails;
                }
            }

            return new AdditionalDetailsController_IP();
        }
    }

    public CaseActionandDetailsController_IP actionandDetails{set;
        get{
            if(getComponentControllerMap() != null) {
                CaseActionandDetailsController_IP actions;
                actions = (CaseActionandDetailsController_IP) getComponentControllerMap().get('actionComponent');
                if(actions != null) {
                    return actions;
                }
            }

            return new CaseActionandDetailsController_IP();
        }
    }

    public NewCaseEntryPageControllerExtension_IP(ApexPages.StandardController stdController) {
       
        caseLog.caseDetails = (Case)stdController.getRecord();
        caseLog.caseDetails.StartTime__c = Datetime.now();

        if(currentUser.Belongs_To__c == 'Customer Service') {
            caseLog.caseDetails.RecordTypeId = RecordTypes.customerCallId;
        }else if(currentUser.Belongs_To__c == 'Claims') {
            caseLog.caseDetails.RecordTypeId = RecordTypes.claimsInquiryId;
        }else {
            caseLog.caseDetails.RecordTypeId = RecordTypes.complaintId;
        }

        initCasePage();

        if(!ApexPages.currentPage().getParameters().containsKey('casId')){
            // If casId does not exist, the case is generated from case detail page and we will trim the cId in setParams before querying the case details
            setParamsFromParent();
        }else {
            // If casId exists, means that it was generated case entry page for follow up cases or clone cases
            autoPopulateCaseDetails((Id)ApexPages.currentPage().getParameters().get('casId'));
        }
        startTime = Datetime.now();
        isNonClaimInquiry = false;
        reDirectToParent = false;
    }

    public void initCasePage(){
        callerContactInfo = new Contact();
        familyContactInfo = new Contact();
        isNewContact = false;
        resetValidations();
        isCloneCase = false;
    }

    public void setParamsFromParent(){

        if(ApexPages.currentPage().getParameters().containsKey('cId')) {
            // Then the trigger point is service cloud
            parentId = ApexPages.currentPage().getParameters().get('cId').substringBefore('?');
        }else if(ApexPages.currentPage().getParameters().containsKey('returnURL')) {
            parentId = ApexPages.currentPage().getParameters().get('retURL').replace('%2F','').replace('/','');
        }else{}

        if(String.isNotBlank(parentId)) {
            autoPopulateCaseDetails((Id)parentId);
        }
    }

    public void autoPopulateCaseDetails(Id caId){
        Case c = getCaseInfo(caId);
        if(c != null) {
            // preset the available values to variables
            selectedCallerType = c.CallerType__c;
            if(selectedCallerType == callerType.Family.name()) {
                familyContactInfo = getContactDetails(c.ContactId);
                if(familyContactInfo != null) {
                    callerContactInfo = getContactDetails(familyContactInfo.RelatedPatient__c);
                }
            }else {
                callerContactInfo = getContactDetails(c.ContactId);
            }

            // caseLog.caseDetails.RecordTypeId = c.RecordTypeId;
            caseLog.caseDetails.AccountId = c.AccountId;
            caseLog.caseDetails.ContactId = c.ContactId;
            caseLog.caseDetails.SuppliedCompany = c.SuppliedCompany;
            caseLog.caseDetails.SuppliedName = c.SuppliedName;
            caseLog.caseDetails.SuppliedPhone = c.SuppliedPhone;
            caseLog.caseDetails.SuppliedEmail = c.SuppliedEmail;
            caseLog.caseDetails.Origin = c.Origin;
            caseLog.caseDetails.Intake__c = c.Intake__c;
            caseLog.caseDetails.Subject = c.Subject;
            caseLog.caseDetails.Description = c.Description;
            
            /*if(c.ContactId == null){
                caseLog.caseDetails.NonCredContact__c = true;
                nonCredContact = true;
            }

            if(c.AccountId == null){
                caseLog.caseDetails.NonCredAccount__c = true;
                nonCredAccount = true;
            }*/
            caseLog.caseDetails.NonCredContact__c = c.NonCredContact__c;
            if(c.NonCredContact__c) nonCredContact = true;
            caseLog.caseDetails.NonCredAccount__c = c.NonCredAccount__c;
            if(c.NonCredAccount__c) nonCredAccount = true;

            if(c.Origin == 'Claims Email') {
                caseLog.caseDetails.ParentId = c.Id;
                parentCaseRedirect = (String)c.Id;
                reDirectToParent = true;
                caseLog.caseDetails.Debug_Log__c = 'Assigned Parent Case Id';
            }else{
                reDirectToParent = false;
            }
            
            if(c.CallerType__c == 'Facility'){
                nonCredContact = true;
                nonCredAccount = true;
            }

            if(ApexPages.currentPage().getParameters().containsKey('isClone') && ApexPages.currentPage().getParameters().get('isClone') == 'true') {
                isCloneCase = true;
                if(c.Patient__c != null) {
                caseLog.caseDetails.Patient__c = c.Patient__c;
                caseLog.caseDetails.Patient_Payor__c = c.Patient_Payor__c;
                caseLog.caseDetails.Patient_Payor_Line_of_Business__c = c.Patient_Payor_Line_of_Business__c;
                caseLog.caseDetails.PolicyNumber__c = c.PolicyNumber__c;
                caseLog.caseDetails.DOB__c = c.DOB__c;
                caseLog.caseDetails.Patient_First_Name__c = c.Patient_First_Name__c;
                caseLog.caseDetails.Patient_Last_Name__c = c.Patient_Last_Name__c;
                whoIsThisCallAbout.selectedCallAboutType = 'Patient';
                }

                if(c.Provider_Account__c != null) {
                    caseLog.caseDetails.Provider_Account__c = c.Provider_Account__c;
                    caseLog.caseDetails.Provider__c = c.Provider__c;
                    whoIsThisCallAbout.selectedCallAboutType = 'Provider';
                }

                if(c.PayerAccount__c != null) {
                    caseLog.caseDetails.PayerAccount__c = c.PayerAccount__c;
                    caseLog.caseDetails.Payor__c = c.Payor__c;
                    whoIsThisCallAbout.selectedCallAboutType = 'Payor';
                }

                caseLog.caseDetails.QueClaimID__c = c.QueClaimID__c; // for cloning the Claim ID
                caseLog.caseDetails.CaseReasonId__c = c.CaseReasonId__c;
                caseLog.caseDetails.CaseReasonDetailId__c = c.CaseReasonDetailId__c;
            }
        }
    }

    public static Case getCaseInfo(Id caseId){

        Case c = [SELECT Id, AccountId, ContactId, Intake__c, ParentId, RecordTypeId, SuppliedPhone, SuppliedEmail, SuppliedName, SuppliedCompany, Contact.RecordTypeId, Contact_Type__c, CaseNumber, Status, Subject,
                    Description, CallerType__c, Reason, Case_Subreason__c, DOB__c, PolicyNumber__c,Contact.Email, Origin, Patient_First_Name__c, Patient_Last_Name__c, PatientHealthPlan__c,
                    Payor__c, Provider_Account__c, NonCredContact__c, NonCredAccount__c, Patient__c, PayerAccount__c,Provider__c,  Intake__r.DraftNotes__c, Intake__r.Id, QueClaimID__c ,CaseReason__c ,CaseReasonDetail__c, Patient_Payor__c ,
                    Patient_Payor_Line_of_Business__c, CaseReasonId__c, CaseReasonDetailId__c, CaseActionId__c,
                    (SELECT Id, AuthorizationNumber__c, HCPC_Code__c, Order__c, QueClaimID__c, ReferredProvider__c, zPaperReferenceNumber__c FROM Additional_Case_Details__r),
                    (SELECT Id, CaseAction__c, CaseActionDetail__c, Case_Comment__c FROM Case_Actions__r)
                FROM Case
                WHERE Id=: (Id) caseId LIMIT 1
            ];

        return c != null ? c : new Case();
    }

    public Contact getContactDetails(Id conId){
        // get contact info by passing the ID
        if(conId != null) {
            Contact con = [SELECT Id, Name, FirstName, LastName, Account.Name, Birthdate, AccountId, RecordTypeId, RelatedPatient__c, RelationshiptoPatient__c, Account.Status__c, Phone, Email, Health_Plan_ID__c
                            FROM Contact WHERE Id=: conId LIMIT 1];
            return con;
        }
        return new Contact();
    }

    public void resetValidations(){
        nonCredContact = false;
        nonCredAccount = false;
    }

    /************************* AUTO FILL INFORMATIONS ******************************/

    public void autoFillContactInfo(){
        if(callerContactInfo.Id != null && String.isNotBlank(callerContactInfo.Id)) {
            callerContactInfo = getContactDetails(callerContactInfo.Id) != null ? getContactDetails(callerContactInfo.Id) : new Contact();
            if(selectedCallerType != 'Family') {
                // Fill the contact info for case
                caseLog.caseDetails.ContactId = callerContactInfo.Id;
                caseLog.caseDetails.AccountId = callerContactInfo.AccountId;
                caseLog.caseDetails.SuppliedPhone = callerContactInfo.Phone;
                caseLog.caseDetails.SuppliedEmail = callerContactInfo.Email;
                caseLog.caseDetails.CallerType__c = RecordTypes.recordTypesById.get(callerContactInfo.RecordTypeId).Name;
                // NOTE: For aiding the search of cases from Golbal search
                caseLog.caseDetails.Contact_Name_Search__c = callerContactInfo.FirstName+' '+callerContactInfo.LastName;
                caseLog.caseDetails.Account_Name_Search__c = callerContactInfo.Account.Name;
                // selectedCallerType = RecordTypes.recordTypesById.get(callerContactInfo.RecordTypeId).Name;
                if(selectedCallerType == 'Patient') {
                    caseLog.caseDetails.DOB__c = callerContactInfo.Birthdate;
                    caseLog.caseDetails.PolicyNumber__c = callerContactInfo.Health_Plan_ID__c;
                }
                if(String.isBlank(callerContactInfo.Health_Plan_ID__c) && (selectedCallerType == 'Patient' || selectedCallerType == 'Family'))
                {ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Patient Policy Number is required.'));}
            }else {
                if(familyContactInfo.Id != null) {
                    familyContactInfo = getContactDetails(familyContactInfo.Id);
                    caseLog.caseDetails.ContactId = familyContactInfo.Id;
                    caseLog.caseDetails.AccountId = familyContactInfo.AccountId;
                    caseLog.caseDetails.SuppliedPhone = familyContactInfo.Phone;
                    caseLog.caseDetails.SuppliedEmail = familyContactInfo.Email;
                    caseLog.caseDetails.CallerType__c = RecordTypes.recordTypesById.get(familyContactInfo.RecordTypeId).Name;
                    // NOTE: For aiding the search of cases from Golbal search
                    caseLog.caseDetails.Contact_Name_Search__c = familyContactInfo.FirstName+' '+familyContactInfo.LastName;
                	caseLog.caseDetails.Account_Name_Search__c = familyContactInfo.Account.Name;
                    // selectedCallerType = RecordTypes.recordTypesById.get(familyContactInfo.RecordTypeId).Name;
                }
            }
        }else {
            // no contact was found, so make the contact non credentialed
            // nonCredContact = true;
            // caseDetails.SuppliedName = conId;
        }
    }

    /*********************** SAVE CASE ACTIONS ********************************/

    public Boolean validateSelectedValuesBeforeSave(){
        if(String.isBlank(this.reasonCompController.reasonSelected)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Case Reason is required to proceed'));
            return false;
        }

        if(String.isBlank(this.reasonCompController.reasonDetailSelected)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Case Reason Details is required to proceed'));
            return false;
        }

        if(String.isBlank(this.actionandDetails.actionSelected)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Action Field is required to proceed'));
            return false;
        }

        if(String.isBlank(this.actionandDetails.actionDetailSelected)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Action Detail Field is required to proceed'));
            return false;
        }

        return true;
    }

    public Boolean saveThisCase(){
        // 1. Get case information and pass it over to caseDetails

        caseLog.caseDetails.CallerType__c = selectedCallerType;

        if(caseLog.caseDetails.Intake__c == null ) { 
            // when second case is created, intake Id is assigned automatically to new case
            // when creating from parent case, parent's Intake id is assigned
            // create new Intake Id
            Intake__c newIntake = new Intake__c(StartTime__c = startTime, EndTime__c = Datetime.now());

            if(callerContactInfo != null && callerContactInfo.Id != null){
                newIntake.Contact__c = callerContactInfo.Id;
            }else{
                newIntake.CallerName__c = caseLog.caseDetails.SuppliedName;
            	newIntake.CallerCompany__c = caseLog.caseDetails.SuppliedCompany;
            }
            
            /*if(nonCredContact) newIntake.CallerName__c = caseLog.caseDetails.SuppliedName;
            if(nonCredAccount) newIntake.CallerCompany__c = caseLog.caseDetails.SuppliedCompany;*/

            insert newIntake;

            caseLog.caseDetails.Intake__c = newIntake.Id;
            intakeId = newIntake.Id;
        }else{
            Intake__c updateIntake = [SELECT Id, EndTime__c FROM Intake__c WHERE Id=: caseLog.caseDetails.Intake__c LIMIT 1];
            updateIntake.EndTime__c = Datetime.now();
            update updateIntake;
            intakeId = updateIntake.Id;
        }

        if(nonCredContact){
            caseLog.caseDetails.NonCredContact__c = true;
        }

        if(nonCredAccount){
            caseLog.caseDetails.NonCredAccount__c = true;
        }

        // 2. Set case reason and case reason details
        
        if(!Test.isRunningTest()) {
            
            if(!validateSelectedValuesBeforeSave()) {
                return false;
            }
        }
        

        reasonCompController.setParentControllerValues();

        // 3. Set Who is this call about details
        whoIsThisCallAbout.setParentControllerValues();

        //Fetching the assignment rules on case
        AssignmentRule newRule = new AssignmentRule();
        newRule = [select id from AssignmentRule where SobjectType = 'Case' and Active = true limit 1];

        //Creating the DMLOptions for "Assign using active assignment rules" checkbox
        Database.DMLOptions dmlOpts = new Database.DMLOptions();
        dmlOpts.assignmentRuleHeader.assignmentRuleId= newRule.id;
        //caseLog.caseDetails.setOptions(dmlOpts);

        // 4. Insert the case
        Database.SaveResult insertCase = Database.insert(caseLog.caseDetails, true);
        if(insertCase.isSuccess()){
            System.debug(loggingLevel.INFO, 'Case Insert successful'+insertCase.getId());
			caseLog.userAction.Case__c = insertCase.getId();
                
            // 4a. Additional Details
            additionalDetailsList.setParentControllerValues();

            //4b. Case Action and Details
            // actionandDetails.setParentControllerValues();
            ActionDetail__c actToUpdate = new ActionDetail__c();
            if(!Test.isRunningTest()){
                actionDetailId = this.actionandDetails.actionDetailSelected;
            }
			
            if(String.isNotBlank(actionDetailId)) {
                actToUpdate = CaseReasonRelationshipHelper_IP.getActionValues((Id)actionDetailId);
            }
            
            if(actToUpdate != null){
                caseLog.userAction.CaseAction__c = actToUpdate.Action__c;
                caseLog.userAction.CaseActionDetail__c = actToUpdate.ActionDetail__c;
                caseLog.userAction.Case_Comment__c = this.actionandDetails.userComments;
                
                Case c = [SELECT Id, Status, CaseActionId__c FROM Case WHERE Id=: caseLog.caseDetails.Id LIMIT 1];
                
                c.Status = actToUpdate.CaseStatustoUpdate__c;
                c.CaseActionId__c = actToUpdate.Id;
                
                insert caseLog.userAction;
                update c;
            }

            newPg = Page.NewCaseEntryPage_IP;
            newPg.getParameters().put('casId',insertCase.getId()); // pass the case id to clone

            return true;
        }else{
            System.debug(loggingLevel.ERROR, 'Following errors while creating case:'+insertCase.getErrors());
        }

        return false;
    }

    public PageReference save(){

        Boolean isSaveSuccess = saveThisCase();

        if(isSaveSuccess){

            newPg.setRedirect(true);
            return newPg;

        }else{
            //SOMETHING WENT WRONG HERE
        }

        return null;
    }

    public PageReference saveAndClone(){

        Boolean isSaveSuccess = saveThisCase();

        if(isSaveSuccess){

            newPg.getParameters().put('isClone','true');
            newPg.setRedirect(true);
            return newPg;

        }else{
            //SOMETHING WENT WRONG HERE
        }

        return null;
    }

    public PageReference saveAndExit(){

        PageReference exitPage;
        System.debug('Parent Case Id'+parentId);

        if(saveThisCase()){

            if(String.isNotBlank(parentId)){
                // if true, then the users should be re-directed to parent case page to close the case.
                System.debug('PAGE IS REDIRECTING TO PARENT CASE PAGE'+parentId);
                exitPage = new PageReference('/'+ parentId);
            }else{
                System.debug('THE CASE IS REDIRECTED TO INTAKE PAGE'+intakeId);
                exitPage = new PageReference('/'+intakeId);
            }

            //exitPage.setRedirect(true);
            return exitPage;
        }

        return null;
    }

    public PageReference cancel(){
        PageReference pg = Page.NewCaseEntryPage_IP;
        pg.setRedirect(true);
        return pg;
    }

    /******************************** CONTACT ACTIONS ************************/

    public void initNewCallerContact(){ // This method should run when "Create New" button is clicked

        newCallerContact = new Contact();
        if(selectedCallerType == 'Family'){
            newCallerContact.RecordTypeId = RecordTypes.availableContactTypes.get('Patient').getRecordTypeId();
        }else{
            newCallerContact.RecordTypeId = RecordTypes.availableContactTypes.get(selectedCallerType).getRecordTypeId();
        }
        
        if(callerContactInfo.AccountId != null) {
            newCallerContact.AccountId = callerContactInfo.AccountId;
        }
    }

    public void insertNewContact(){ // This method should run when "Save" button is clicked
        if(! contactValidatedBeforeSave(newCallerContact)) {
            System.debug('Error');
        }else {
            Database.SaveResult insertCon = Database.insert(this.newCallerContact, true);
            if(insertCon.isSuccess()) {
                System.debug('Post contact is success message');
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'New '+ selectedCallerType+' Contact was created'));
                isConInsertSucess = true;
                isNewContact = false;
                callerContactInfo = getContactDetails(insertCon.getId());
                autoFillContactInfo();
            }
        }
    }

    public void initNewFamilyContact(){
        newFamilyContact = new Contact();
        newFamilyContact.RecordTypeId = RecordTypes.availableContactTypes.get('Family').getRecordTypeId();
        if(callerContactInfo.Id != null) {
            newFamilyContact.Patient__c = callerContactInfo.Id;
            newFamilyContact.AccountId = callerContactInfo.AccountId;
        }else {
            System.debug('Patient not present');
        }
    }

    public void insertNewFamContact(){
        if(! contactValidatedBeforeSave(newFamilyContact)) {
            System.debug('Error while creating new contact for Family');
        }else {
            Database.SaveResult insertCon = Database.insert(this.newFamilyContact, true);
            if(insertCon.isSuccess()) {
                System.debug('Post family contact is success message');
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'New Family Contact was created'));
                familyContactInfo = getContactDetails(insertCon.getId());
                isNewFamilyContact = false;
                autoFillContactInfo();
            }
        }
    }

    public void updateContactInfo(){
        if(callerContactInfo != null) {
            // pass through validation before updating contact
            if(contactValidatedBeforeSave(callerContactInfo)) {
                update callerContactInfo;
            }
        }
    }

    public Boolean contactValidatedBeforeSave(Contact con){
        if(con.Id == null){
            if(String.isBlank(con.FirstName)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'First Name is required'));
            return false;
            }
            if(String.isBlank(con.LastName)) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Last Name is required'));
                return false;
            }
        }

        if((con.RecordTypeId == RecordTypes.availableContactTypes.get('Patient').getRecordTypeId()) && con.Birthdate == null) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Birth Date is required for Patient'));
            return false;
        }

        if((con.RecordTypeId == RecordTypes.availableContactTypes.get('Patient').getRecordTypeId()) && String.isBlank(con.Health_Plan_ID__c)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Health Plan Id is required for Patient'));
            return false;
        }

        if((con.RecordTypeId == RecordTypes.availableContactTypes.get('Family').getRecordTypeId()) && String.isBlank(con.RelationshiptoPatient__c)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Relationship to Patient is required for Family Contact'));
            return false;
        }

        if(con.Phone == null || con.Phone.length() < 10) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Phone is required. Check if 10 digits are entered'));
            return false;
        }

        return true;
    }
}