<apex:page controller="PayerPatientsSummary_CS" action="{!init}" showHeader="false" sidebar="false" standardStyleSheets="false" cache="false">

	<!--header-->
	<c:PayerHeader_CS accountName="{!portalUser.instance.Entity__r.Name}"/>
	
	<apex:form >

        <apex:actionstatus id="status" onstart="getDocumentHeight();" onstop="sortTableState.markSortedColumn();">
            <apex:facet name="start">
                <div class="waitingSearchDiv text-center" id="loading" style="background-color: #fbfbfb; height: 100%; opacity:0.65; width:100%; filter: alpha(opacity=65); position: fixed; z-index: 999;">
                    <div class="waitingHolder" style="margin:0 auto;">
                        <div id="spinjs"></div>
                    </div>
                </div>
            </apex:facet>
        </apex:actionstatus>

		<apex:outputPanel rendered="{!showSearchPanel}">

			<!-- For pressing enter after typing in search criteria -->
			<apex:actionFunction name="doBackendSearch" action="{!searchPatient}" status="status" />
			
			<div class="page-wrap">
			
				<c:PayerNavigation_CS userName="{!portalUser.instance.Name}" accountName="{!portalUser.instance.Entity__r.Name}"/>
			
				<div class="page-inner-push">
			
			    <!--intro Title-->
			    <div class="row">
			    	<div class="large-12 columns text-center">
			      	  <h2>Search for Patient</h2>
			          <p class="text-secondary">Enter Patient ID, Medicaid ID, Medicare ID <br/> or search by Patient’s last name, first name</p>
			        </div>
			    </div>
			
				<div class="row">
					<div class="large-12 columns text-center">
					<apex:outputText id="searchMessage1" value="{!searchStringMessage}" style="color:red"/>
					</div>
				</div>
			
			      <!--search field-->
				<div class="row">
			       	<div class="large-offset-4 large-4 small-12 columns">       	
			        	<apex:inputText styleclass="medium" value="{!filterBySearch}" onkeypress="return pressEnterToSearch(event);" html-placeholder="Enter Patient's ID or First/Last Name"/>
			        </div><!--large-4-->
			      </div><!--row-->
			
			      <!--call to action buttons-->
			      <div class="row">
			        <div class="large-offset-4 large-4 columns">
			        	
			        	<apex:commandLink value="Search" action="{!searchPatient}" status="status" styleclass="button primary medium right"/>
						<apex:outputpanel rendered="{!showCreatePatients}">
							<a href="PayerCreateNewPatient_CS" class="link medium link-small">Add New Patient</a>
						</apex:outputpanel>
					</div><!--large-4-->
			      </div><!--row-->
			    </div><!--page-inner-push-->
			
			</div><!--.page-wrap-->
			
		</apex:outputPanel>
	
		<apex:outputPanel rendered="{!NOT(showSearchPanel)}">
		
			<div class="page-wrap">
	
				<c:PayerNavigation_CS userName="{!portalUser.instance.Name}" accountName="{!portalUser.instance.Entity__r.Name}"/>
				<!-- For pressing enter after typing in search criteria -->
				<apex:actionFunction name="doBackendSearch" action="{!searchPatient}" rerender="patientList, searchMessage2" status="status">
                    <apex:param name="sortCol" value="" />
                    <apex:param name="sortOrd" value="" />
                </apex:actionFunction>
				
				<div class="admin-options">
					<div class="row">
						<!--call to action buttons-->
						<div class="large-12 small-12 columns">
							<div>
								<!--- Output Panel added by VK on 10/12/2016 to wrap elements in rendering-->
								<apex:outputPanel rendered="{!editUserPermission}">
									<a href="PayerUsersSummary_CS" class="button secondary tiny">View All Users</a>
								</apex:outputPanel>
								 &nbsp;&nbsp;								
								 <apex:outputPanel rendered="{!viewPatientsPermission}">
									<a href="PayerPatientsSummary_CS" class="button secondary tiny">Search for Patients</a>
								</apex:outputPanel>
								<!--<a href="new-portalUser.php" class="button secondary tiny">Create New User</a>-->
								<!--<a href="new-patient.php" class="button secondary tiny">Add New Patient</a>-->
								<!--<a href="#" class="button secondary tiny">Manage Your Team</a>-->
								<!--<a href="create-order-1.php" class="button primary tiny right">Create New Order</a>-->
							</div>
						</div>
					</div>
				</div>
				<div class="small-8 columns right text-right">
			      <span class="text-secondary" style="font-size: 80%;opacity: 1!important;">
			        Logged in as <span style="font-weight: bold;">{!portalUser.instance.Name}</span> for <span style="font-weight: bold;">{!portalUser.instance.Entity__r.Name}</span>
			      </span>
			  	</div>
				
				<div class="page-inner-push">
					<div class="row">
	
						<div class="large-5 columns order-summary-title">
							<h2>Health Plan Patient List</h2>
						</div>
						
						<div class="large-4 columns text-center search-box">
							<div id="white" class="large-offset-2" style="margin-left: 0%; margin-top:8.25%;">
								<apex:inputText id="searchBox" value="{!filterBySearch}" size="50" onkeypress="return pressEnterToSearch(event);" html-placeholder="Enter Patient's ID or Last Name" >
									<!--<apex:actionSupport event="onblur" rerender="patientList, searchMessage2, searchMessage1" action="{!searchPatient}" status="status"/>-->
								</apex:inputText>
							</div>
						</div>
	
						<div class="large-3 columns text-right user-filter push-top" style="margin-top:3.25%;">
							<apex:outputpanel rendered="{!showCreatePatients}">
								<a href="PayerCreateNewPatient_CS" class="button primary tiny">Create New Patient</a>
							</apex:outputpanel>
						</div>
						<!--.user-filter-->
	
						<div class="large-12 small-12 columns left-cero push-top">
							<div class="large-4 columns order-view-filter">
								<p class="text-secondary">
									Show&nbsp;
									<apex:selectList style="width:50px;" value="{!pageSize}" size="1">
										<apex:selectOption itemValue="10" itemLabel="10" />
										<apex:selectOption itemValue="20" itemLabel="20" />
										<apex:selectOption itemValue="30" itemLabel="30" />
										<apex:selectOption itemValue="50" itemLabel="50" />
										<apex:selectOption itemValue="100" itemLabel="100" />
										<apex:selectOption itemValue="{!patientSetCon.ResultSize}" itemLabel="All" />
										<apex:actionSupport event="onchange" rerender="patientList" status="status" />
									</apex:selectList>
									Patients Per Page
								</p>
							</div>
							<!--order-view-filter-->
	
							<div class="large-4 columns text-center search-box">
								<div id="white" class="large-offset-2">
									<form method="get" action="/search" id="search">
										<!-- 			        <input name="q" type="text" size="40" placeholder="Search Patients..." /> -->
									</form>
								</div>
							</div>
							<!--search-box-->
						</div>
					</div>
					<!--.row-->
					
					<div class="row">
						<div class="large-12 columns text-left" style="padding-bottom: 1%;">
							<apex:outputText id="searchMessage2" value="{!searchStringMessage}" style="color:red"/>
						</div>
					</div>
	
	
					<!--user list-->
					<apex:outputPanel id="patientList">
	
						<div class="row">
							<div class="summary large-12 small-12 columns">
								
								<apex:outputpanel rendered="{!portalUser.role.Create_Orders__c}">
	
								<table class="all large-12">
									<thead>
										<tr>
                                            <th id="insuranceId" onclick="sortTableState.sortByColumn(this);" class="sortable-header">Insurance ID#<span></span></th>
                                            <th id="firstName" onclick="sortTableState.sortByColumn(this);" class="sortable-header">First Name<span></span></th>
                                            <th id="lastName" onclick="sortTableState.sortByColumn(this);" class="sortable-header">Last Name<span></span></th>
                                            <th id="dob" onclick="sortTableState.sortByColumn(this);" class="sortable-header">Date of Birth<span></span></th>
                                            <th id="phoneNumber" onclick="sortTableState.sortByColumn(this);" class="sortable-header">Phone Number<span></span></th>
											<!-- <th>Email Address</th> -->
											<!-- <th>Insurance ID#</th> -->
											<th>Create Order</th>
										</tr>
									</thead>
									<tbody>
	
										<apex:repeat value="{!patientList}" var="patient">
	
											<tr>
												<td>
													<apex:outputLink value="PayerPatientSummary_CS" styleclass="link darker">{!patient.ID_Number__c}
				                  						<apex:param name="pid" value="{!patient.Id}" />
													</apex:outputLink>
												</td>
                                                <td>
                                                    <apex:outputLink value="PayerPatientSummary_CS" styleclass="link darker">{!patient.First_Name__c}
                                                        <apex:param name="pid" value="{!patient.Id}" />
                                                    </apex:outputLink>
                                                </td>
                                                <td>
                                                    <apex:outputLink value="PayerPatientSummary_CS" styleclass="link darker">{!patient.Last_Name__c}
                                                        <apex:param name="pid" value="{!patient.Id}" />
                                                    </apex:outputLink>
                                                </td>
                                                <td>
                                                    <apex:outputField value="{!patient.Date_of_Birth__c}" />
                                                </td>
												<td><span data-tooltip="{!patient.Id}" data-options="disable-for-touch:true" class="has-tip" title="Home Phone: {!patient.Phone_Number__c}<br/> Mobile Phone: {!patient.Cell_Phone_Number__c}"> {!patient.Phone_Number__c}</span></td>
												<!-- <td><a href="mailto:{!patient.Email_Address__c}" class="link">{!patient.Email_Address__c}</a></td> -->
												<!-- <td>{!patient.ID_Number__c}</td> -->
												<td>
													<apex:outputLink value="PayerCreateNewOrder_3_CS" styleclass="button tiny secondary">Create Order
														<apex:param name="pid" value="{!patient.Id}"/>
													</apex:outputLink>
												</td>
											</tr>
											<!--patient-->
	
										</apex:repeat>
	
									</tbody>
								</table>
								</apex:outputpanel>
								
								<apex:outputpanel rendered="{!NOT(portalUser.role.Create_Orders__c)}">
	
								<table class="all large-12">
									<thead>
										<tr>
											<th id="insuranceId" onclick="sortTableState.sortByColumn(this);" class="sortable-header">Insurance ID#<span></span></th>
											<th id="firstName" onclick="sortTableState.sortByColumn(this);" class="sortable-header">First Name<span></span></th>
											<th id="lastName" onclick="sortTableState.sortByColumn(this);" class="sortable-header">Last Name<span></span></th>
											<th id="phoneNumber" onclick="sortTableState.sortByColumn(this);" class="sortable-header">Phone Number<span></span></th>
											<!-- <th>Email Address</th> -->
											<!-- <th>Insurance ID#</th> -->
										</tr>
									</thead>
									<tbody>
	
										<apex:repeat value="{!patientList}" var="patient">
	
											<tr>
												<td>
													<apex:outputLink value="PayerPatientSummary_CS" styleclass="link darker">{!patient.ID_Number__c}
				                  						<apex:param name="pid" value="{!patient.Id}" />
													</apex:outputLink>
                                                </td>
                                                <td>
                                                    <apex:outputLink value="PayerPatientSummary_CS" styleclass="link darker">{!patient.First_Name__c}
                                                        <apex:param name="pid" value="{!patient.Id}" />
                                                    </apex:outputLink>
                                                </td>
                                                <td>
                                                    <apex:outputLink value="PayerPatientSummary_CS" styleclass="link darker">{!patient.Last_Name__c}
                                                        <apex:param name="pid" value="{!patient.Id}" />
                                                    </apex:outputLink>
                                                </td>
                                                <td><span data-tooltip="{!patient.Id}" data-options="disable-for-touch:true" class="has-tip" title="Home Phone: {!patient.Phone_Number__c}<br/> Mobile Phone: {!patient.Cell_Phone_Number__c}"> {!patient.Phone_Number__c}</span></td>
												<!-- <td><a href="mailto:{!patient.Email_Address__c}" class="link">{!patient.Email_Address__c}</a></td> -->
												<!-- <td>{!patient.ID_Number__c}</td> -->
											</tr>
											<!--patient-->
	
										</apex:repeat>
	
									</tbody>
								</table>
								
								</apex:outputpanel>
	
	
								<!--patient-selection-->
	
								<!--pagination-->
								<div class="row" style="{!IF((totalPages > 1),'visibility: visible;','visibility:hidden;')}">
									<div class="large-12 small-12 columns text-center">
										<ul class="pagination">
											<li class="arrow {!IF((!patientSetCon.HasPrevious), 'unavailable','')}"><apex:commandLink value="<" action="{!patientSetCon.Previous}" rerender="patientList, pagination" oncomplete="$(document).foundation();" status="status" /></li> <!--status="statusPaging"-->
											<li>{!patientSetCon.PageNumber} of {!totalPages}</li>
											<li class="arrow {!IF((!patientSetCon.HasNext), 'unavailable','')}"><apex:commandLink value=">" action="{!patientSetCon.Next}" rerender="patientList" oncomplete="$(document).foundation();" status="status" /></li> <!--status="statusPaging"-->
										</ul>
									</div>
									<!--pagination-->
								</div>
								<!--.row .pagination-->
	
							</div>
							<!--summary-->
						</div>
	
					</apex:outputPanel>
					<!--.row table-->
	
				</div>
				<!--page-inner-push-->
			</div>
			<!--.page-wrap-->
		
		</apex:outputPanel>

	</apex:form>

	<c:PayerFooter_CS />

	<script>
		function pressEnterToSearch(ev){
		   	if (window.event && window.event.keyCode == 13 || ev.which == 13) {
		   		doSearch();
		       	return false;
		     } else {
		      	return true;
		     }
		}

        var sortTableState = new sortTable(doSearch);
        $(document).ready(
                function() {
                    sortTableState.init('lastName');
                    sortTableState.markSortedColumn();
                }
        );

        function doSearch() {
            if (sortTableState.currentColumnState.columnId != null) {
                doBackendSearch(document.getElementById(sortTableState.currentColumnState.columnId).innerText, sortTableState.currentColumnState.columnSortOrder);
            }
            else {
                doBackendSearch();
            }
        }

        function getDocumentHeight() {
            docHeight = $j(document).height();
            $("#loading").css({'height' : docHeight});
        }

        var target = document.getElementById('spinjs');
        var spinner = new Spinner(largeSpinner).spin(target);

	</script>
</apex:page>