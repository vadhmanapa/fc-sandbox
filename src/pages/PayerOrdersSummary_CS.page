<apex:page controller="PayerOrdersSummary_CS" action="{!init}" showHeader="false" sidebar="false" standardStyleSheets="false" cache="false">

	<c:PayerHeader_CS accountName="{!portalUser.instance.Entity__r.Name}"/>
    <style>
    	/******* TODO: STAGING FOR FUTURE DEPLOYMENT******/
        .status-list-filter ul {
            -webkit-font-smoothing:antialiased;
            text-shadow:0 1px 0 #FFF;
            list-style: none;
            margin: 0px;
            padding: 0px;
            width: 35%;
        }
        .status-list-filter ul a{
            display: block;
            padding: 0 25px;
            text-align: center;
            text-decoration: none;
            -webkit-transition: all .25s ease;
               -moz-transition: all .25s ease;
                -ms-transition: all .25s ease;
                 -o-transition: all .25s ease;
                    transition: all .25s ease;
        }
        .dropdown:after {
            content: ' ▶';
        }
        .dropdown:hover:after{
            content:'\25bc';
        }
        .status-list-filter li ul {
            left: 240px;
            opacity: 0;
            position: absolute;
            top: 35px;
            visibility: hidden;
            z-index: 1;
            -webkit-transition: all .25s ease;
               -moz-transition: all .25s ease;
                -ms-transition: all .25s ease;
                 -o-transition: all .25s ease;
                    transition: all .25s ease;
        }
        .page-wrap li:hover ul {
            opacity: 0.9;
            top: 35px;
            left: 290px;
            visibility: visible;
        }
        .page-wrap li ul li {
            float: none;
            width: 100%;
        	display:relative;
        }
        .status-list-filter li ul a:hover {
           color: #ffffff;
           background-color: #ef630b;
        }
        .cf:after, .cf:before {
        content:"";
        display:inline;
        }
        .cf:after {
            clear:both;
        }
        .cf {
            zoom:1;
        }
        .dropdown-menu-nav{
            background-color:#F5F9FA;
        }
    </style>

	<apex:form >
		<span id="statusPanel">
			<apex:actionstatus id="status" onstart="getDocumentHeight();" onstop="sortTableState.markSortedColumn();">
				<apex:facet name="start">
					<div class="waitingSearchDiv text-center" id="loading" style="background-color: #fbfbfb; height: 90%; opacity:0.65; width:100%; filter: alpha(opacity=65); position: absolute; z-index: 999;">
						<div class="waitingHolder" style="margin:0 auto;">
							<div id="spinjs"></div>
						</div>
					</div>
				</apex:facet>
			</apex:actionstatus>
			
			<apex:pageBlock >
	
				<!-- For pressing enter after typing in search criteria -->
				<apex:actionFunction name="doBackendSearch" action="{!populateOrderList}" rerender="orderResults, orderCount, filterByEntity, orderSizeNotification" status="status">
                    <apex:param name="sortCol" assignTo="{!sortColumn}" value="" />
                    <apex:param name="sortOrd" assignTo="{!sortOrder}" value="" />
                </apex:actionFunction>
	
				<!-- The page -->
				<div class="page-wrap">
	
					<c:PayerNavigation_CS userName="{!portalUser.instance.Name}" accountName="{!portalUser.instance.Entity__r.Name}"/>
	
					<div class="page-inner-push">
						<div class="row">
							<div class="large-12 columns order-summary-title">
								<h2>Order Summary</h2>
							</div>
							<div class="large-4 columns order-view-filter">
								<p class="text-secondary" style="margin-bottom: 0em">
									Viewing &nbsp;
									<!-- NOTE: CP-80 - Sprint 3/20 - Wrapping in output panel to render when stage changes-->
									<apex:outputPanel id="pageSizeFilter">
                                        <apex:selectList value="{!pageSize}" style="width:50px;" size="1">
                                            <apex:selectOption itemValue="10" itemLabel="10" />
                                            <apex:selectOption itemValue="20" itemLabel="20" />
                                            <apex:selectOption itemValue="30" itemLabel="30" />
                                            <apex:selectOption itemValue="50" itemLabel="50" />
                                            <apex:selectOption itemValue="100" itemLabel="100" />
                                            <apex:actionSupport event="onchange" rerender="orderResults" status="status"/>
                                        </apex:selectList>
                                    </apex:outputPanel>
									<apex:outputPanel id="orderCount">&nbsp;of {!orderSetCon.ResultSize} orders</apex:outputPanel>
								</p>
								<p class="text-secondary" style="margin-bottom: 0em">
									Show Orders for &nbsp;
									<apex:outputPanel id="filterByTymeFrame">
										<apex:selectList value="{!filterByTime}" style="width:110px;" size="1">
											<apex:selectOption itemValue="thisWeek" itemLabel="This Week" />
											<apex:selectOption itemValue="lastWeek" itemLabel="Last Week" />
											<apex:selectOption itemValue="thisMonth" itemLabel="This Month" />
											<apex:selectOption itemValue="lastMonth" itemLabel="Last Month" />
											<apex:selectOption itemValue="allTime" itemLabel="All Time" />
											<apex:actionSupport event="onchange" rerender="orderResults, orderCount, filterByEntity, orderSizeNotification" action="{!populateOrderList}" status="status"/>
										</apex:selectList>
									</apex:outputPanel>
								</p>
								<apex:outputPanel id="orderSizeNotification">
									<apex:outputText value="*Limited to 10,000 records.<br/>Continue filtering for more precise results." rendered="{!filterByTime == 'allTime'}" style="font-size:75%" escape="false" />
                                    <br />
                                    <apex:outputText value="**Showing All Needs Attention by default. Drill down to specific bucket by hovering over Needs Attention" rendered="{!filterByStage='Needs Attention'}" style="font-size:75%" escape="false" /><!-- TODO: FUTURE DEPLOYMENT-->
								</apex:outputPanel>
                                <br />
							</div>
							
							<!--order-view-filter-->
							<apex:outputpanel >
								<div class="large-4 columns text-center search-box">
									<div id="white" class="large-offset-2" style="margin-left: 0%">
										<apex:inputText id="searchBox" value="{!filterBySearch}" size="50" onkeypress="return pressEnterToSearch(event);" html-placeholder="Search Patient Name, Auth Code or ID# ..." >
											<apex:actionSupport event="onblur" rerender="orderResults, orderCount, filterByEntity" action="{!populateOrderList}" status="status"/>
										</apex:inputText>
									</div>
								</div>
							</apex:outputpanel>
	
							<div class="large-4 columns text-right user-filter">
								<p class="text-secondary">
									Filter by &nbsp;
									<apex:selectList value="{!filterByEntityType}" style="width:100px;" size="1">
										<apex:selectOptions value="{!filterByEntityTypeOptions}" />
										<apex:actionSupport event="onchange" rerender="orderResults, orderCount, filterByEntity" status="status" />
									</apex:selectList>
									<apex:outputPanel id="filterByEntity">
										<apex:selectList value="{!filterByUser}" style="width:100px;" size="1" rendered="{!filterByEntityType == 'User'}">
											<apex:selectOptions value="{!filterByUserOptions}" />
											<apex:actionSupport event="onchange" rerender="orderResults, orderCount" action="{!populateOrderList}" status="status" />
										</apex:selectList>
										<apex:selectList value="{!filterByProvider}" style="width:100px;" size="1" rendered="{!filterByEntityType == 'Provider'}">
											<apex:selectOptions value="{!filterByProviderOptions}" />
											<apex:actionSupport event="onchange" rerender="orderResults, orderCount" action="{!populateOrderList}" status="status" />
										</apex:selectList>
									</apex:outputPanel>
								</p>
								<p class="text-secondary">
								<!-- Moving Create order button from above table to below filter to make space for added Future DOS button - Sprint 2/13 - VK-->
									<!--<div class="large-2 columns right-cero text-right">-->
										<apex:outputpanel rendered="{!portalUser.role.Create_Orders__c}">
											<a href="PayerCreateNewOrder_1_CS" class="button primary tiny">Create New Order</a>
										</apex:outputpanel>
									<!--</div>-->
								</p>
							</div>
						</div>
						<!--.row-->
	
						<apex:outputPanel id="orderResults">
							<!--nav order summary-->
							<div class="row">
								<div class="large-12 columns order-summary-sub-header">
									<div class="large-11 columns">
										<ul class="status-list-filter">
											<li class="filter-all {!IF((filterByStage=''),'filter-active','')}">
                                                <apex:commandLink value="All" action="{!populateOrderList}" rerender="orderResults, orderCount, orderSizeNotification, searchBox, filterByEntity, filterByTymeFrame, pageSizeFilter" status="status" onComplete="resetSorting();">
													<apex:param name="filterByAll" value="" assignTo="{!filterByStage}" />
                                                	<apex:param name="filterBySubStage" value="" assignTo="{!filterBySubStage}" /><!-- to avoid subStage becoming a static varaible - VK-->
                                                    <!-- CP-80 - Sprint 3/20 - ADDED BY VK-->
<!--                                      			<apex:param name="filterByTimeFrame" value="thisWeek" assignTo="{!filterByTime}" />
                                                    <apex:param name="pageSizeFilter" value="10" assignTo="{!pageSize}" />
-->
												</apex:commandLink></li>
											<li class="filter-pending {!IF((filterByStage='Pending Acceptance'),'filter-active','')}">
                                                <apex:commandLink value="Pending Acceptance" action="{!populateOrderList}" rerender="orderResults, orderSizeNotification, orderCount, searchBox, filterByEntity, filterByTymeFrame, pageSizeFilter" status="status" onComplete="resetSorting();">
													<apex:param name="filterByPending" value="Pending Acceptance" assignTo="{!filterByStage}" />
                                                	<apex:param name="filterBySubStage" value="" assignTo="{!filterBySubStage}" /><!-- to avoid subStage becoming a static varaible - VK-->
                                                	<!-- CP-80 - Sprint 3/20 - ADDED BY DIMA-->
<!--                                                <apex:param name="filterByTimeFrame" value="allTime" assignTo="{!filterByTime}" />
                                                    <apex:param name="pageSizeFilter" value="100" assignTo="{!pageSize}" />
-->
												</apex:commandLink></li>
											<li class="filter-progress {!IF((filterByStage='In Progress'),'filter-active','')}">
                                                <apex:commandLink value="In Progress" action="{!populateOrderList}" rerender="orderResults, orderCount, orderSizeNotification, searchBox, filterByEntity, filterByTymeFrame, pageSizeFilter" status="status" onComplete="resetSorting();">
													<apex:param name="filterByInProgress" value="In Progress" assignTo="{!filterByStage}" />
                                                	<apex:param name="filterBySubStage" value="" assignTo="{!filterBySubStage}" /><!-- to avoid subStage becoming a static varaible - VK-->
                                                    <!-- CP-80 - Sprint 3/20 - ADDED BY VK-->
<!--                                                <apex:param name="filterByTimeFrame" value="thisWeek" assignTo="{!filterByTime}" />
                                                    <apex:param name="pageSizeFilter" value="10" assignTo="{!pageSize}" />
-->
												</apex:commandLink></li>
											<li>
                                                <!-- TODO: DEPRECATE IN FUTURE - VK-->
                                                <!--class="filter-attention {!IF((filterByStage='Needs Attention'),'filter-active','')}"-->
													<!--<apex:commandLink value="Needs Attention" action="{!populateOrderList}" rerender="orderResults, orderCount, searchBox, filterByEntity" status="status" onComplete="sortTableState.init('expectedDate');">
													<apex:param name="filterByNeedsAttention" value="Needs Attention" assignTo="{!filterByStage}" />
													<apex:param name="filterBySubStage" value="" assignTo="{!filterBySubStage}" />
												</apex:commandLink>-->
                                                
                                                <!-- Staging for Sprint 12/12 - DEPLOYING IN FUTURE-->
                                                
                                                <a class="dropdown" href="#">Needs Attention</a>
                                                    <ul class="dropdown-menu-nav">
                                                    <li>
                                                        <apex:commandLink value="Needs My Attention" action="{!populateOrderList}" rerender="orderResults, orderCount, searchBox, orderSizeNotification, filterByEntity, filterByTymeFrame, pageSizeFilter" status="status">
                                                            <apex:param name="filterByNeedsAttention" value="Needs Attention" assignTo="{!filterByStage}"/>
                                                            <apex:param name="filterBySubStage" value="My Attention" assignTo="{!filterBySubStage}" />
                                                            				<!-- CP-80 - Sprint 3/20 - ADDED BY DIMA-->
<!--                                                        <apex:param name="filterByTimeFrame" value="allTime" assignTo="{!filterByTime}" />
                                                            <apex:param name="pageSizeFilter" value="100" assignTo="{!pageSize}" />
-->
                                                        </apex:commandLink>
                                                    </li>
                                                    <li>
                                                        <apex:commandLink value="Needs Provider Attention" action="{!populateOrderList}" rerender="orderResults, orderCount, searchBox, orderSizeNotification, filterByEntity, filterByTymeFrame, pageSizeFilter" status="status">
                                                            <apex:param name="filterByNeedsAttention" value="Needs Attention" assignTo="{!filterByStage}"/>
                                                            <apex:param name="filterBySubStage" value="Other Attention" assignTo="{!filterBySubStage}" />
                                                            				<!-- CP-80 - Sprint 3/20 - ADDED BY DIMA-->
<!--                                                        <apex:param name="filterByTimeFrame" value="allTime" assignTo="{!filterByTime}" />
                                                            <apex:param name="pageSizeFilter" value="100" assignTo="{!pageSize}" />
-->
                                                        </apex:commandLink>
                                                    </li>
                                                    <li>
                                                        <apex:commandLink value="All Needs Attention" action="{!populateOrderList}" rerender="orderResults, orderCount, searchBox, orderSizeNotification, filterByEntity, filterByTymeFrame, pageSizeFilter" status="status">
                                                            <apex:param name="filterByNeedsAttention" value="Needs Attention" assignTo="{!filterByStage}"/>
                                                            <apex:param name="filterBySubStage" value="" assignTo="{!filterBySubStage}" />
                                                            				<!-- CP-80 - Sprint 3/20 - ADDED BY DIMA-->
<!--                                                        <apex:param name="filterByTimeFrame" value="allTime" assignTo="{!filterByTime}" />
                                                            <apex:param name="pageSizeFilter" value="100" assignTo="{!pageSize}" />
-->
                                                        </apex:commandLink>
                                                    </li>
                                                </ul>
                                                
                                                <!------------ End Staging------------>       
                                            </li>
											<li class="filter-recurring {!IF((filterByStage='Recurring'),'filter-active','')}">
                                                <apex:commandLink value="Recurring" action="{!populateOrderList}" rerender="orderResults, orderCount, searchBox, orderSizeNotification, filterByEntity, filterByTymeFrame, pageSizeFilter" status="status" onComplete="resetSorting();">
													<apex:param name="filterByRecurring" value="Recurring" assignTo="{!filterByStage}" />
                                                	<apex:param name="filterBySubStage" value="" assignTo="{!filterBySubStage}" /><!-- to avoid subStage becoming a static varaible - VK-->
                                                    <!-- CP-80 - Sprint 3/20 - ADDED BY VK-->
<!--                                                <apex:param name="filterByTimeFrame" value="thisWeek" assignTo="{!filterByTime}" />
                                                    <apex:param name="pageSizeFilter" value="10" assignTo="{!pageSize}" />
-->
												</apex:commandLink></li>
											<li class="filter-cancelled {!IF((filterByStage='Cancelled'),'filter-active','')}">
                                                <apex:commandLink value="Cancelled" action="{!populateOrderList}" rerender="orderResults, orderCount, orderSizeNotification, searchBox, filterByEntity, filterByTymeFrame, pageSizeFilter" status="status" onComplete="resetSorting();">
													<apex:param name="filterByCancelled" value="Cancelled" assignTo="{!filterByStage}" />
                                                	<apex:param name="filterBySubStage" value="" assignTo="{!filterBySubStage}" /><!-- to avoid subStage becoming a static varaible - VK-->
                                                    <!-- CP-80 - Sprint 3/20 - ADDED BY VK-->
<!--                                                <apex:param name="filterByTimeFrame" value="thisWeek" assignTo="{!filterByTime}" />
                                                    <apex:param name="pageSizeFilter" value="10" assignTo="{!pageSize}" />
-->
												</apex:commandLink></li>
											<li class="filter-completed {!IF((filterByStage='Completed'),'filter-active','')}">
												<apex:commandLink value="Completed" action="{!populateOrderList}" rerender="orderResults, orderCount, orderSizeNotification, searchBox, filterByEntity, filterByTymeFrame, pageSizeFilter" status="status" onComplete="resetSorting();">
													<apex:param name="filterByCompleted" value="Completed" assignTo="{!filterByStage}" />
                                                	<apex:param name="filterBySubStage" value="" assignTo="{!filterBySubStage}" /><!-- to avoid subStage becoming a static varaible - VK-->
                                                    <!-- CP-80 - Sprint 3/20 - ADDED BY VK-->
<!--                                                <apex:param name="filterByTimeFrame" value="thisWeek" assignTo="{!filterByTime}" />
                                                    <apex:param name="pageSizeFilter" value="10" assignTo="{!pageSize}" />
-->
												</apex:commandLink></li>
											<li class="filter-future {!IF((filterByStage='Future DOS'),'filter-active','')}">
					                            <!-- Added filterByEntity to rerender everytime a filter is changed and status so as to prevent users from clicking on screen while query is loading - VK on 11/28/2016-->
					                            <apex:commandLink value="Future DOS" action="{!populateOrderList}" rerender="orderResults, orderCount, orderSizeNotification, searchBox, filterByEntity, filterByTymeFrame, pageSizeFilter" status="status" onComplete="resetSorting();">
					                                <apex:param name="filterByFuture" value="Future DOS" assignTo="{!filterByStage}"/>
					                                <apex:param name="filterBySubStage" value="" assignTo="{!filterBySubStage}" />
					                                			<!-- CP-80 - Sprint 3/20 - ADDED BY DIMA-->
<!--					                            <apex:param name="filterByTimeFrame" value="allTime" assignTo="{!filterByTime}" />
                                                    <apex:param name="pageSizeFilter" value="100" assignTo="{!pageSize}" />
-->
					                            </apex:commandLink>
					                        </li>
										</ul>
									</div>
								</div>
								<!--large-12 columns-->
							</div>
							<div class="row">
								<div class="large-12 columns">
									<p class="text-secondary" style="font-size:.9em; margin-bottom:0em;">
									<apex:outputpanel rendered="{!(filterByEntityType == 'User' && filterByUser != '')}">Showing orders for: {!userMap[filterByUser]}</apex:outputpanel>
									<apex:outputpanel rendered="{!(filterByEntityType == 'User' && filterByUser == '')}">Showing orders for: All Users</apex:outputpanel>
									<apex:outputpanel rendered="{!(filterByEntityType == 'Provider' && filterByProvider != '')}">Showing orders for: {!providerMap[filterByProvider]}</apex:outputpanel>
									<apex:outputpanel rendered="{!(filterByEntityType == 'Provider' && filterByProvider == '')}">Showing orders for: All Providers</apex:outputpanel>
									<apex:outputpanel rendered="{!filterBySearch != ''}"> &gt; "{!filterBySearch}"</apex:outputpanel>
									</p>
								</div>
							</div>
							<div class="row">
								<div class="order-summary large-12 small-12 columns">
									<table class="all large-12" id="thetable">
										<thead>
											<tr>
												<th id="orderName" onclick="sortTableState.sortByColumn(this);" class="sortable-header">Order #<span></span></th>
												<th id="orderedDate" onclick="sortTableState.sortByColumn(this);" class="sortable-header">Ordered<span></span></th>
												<!-- <th>Note</th> -->
												<th id="patient" onclick="sortTableState.sortByColumn(this);" class="sortable-header">Patient<span></span></th>
												<th id="authCode" onclick="sortTableState.sortByColumn(this);" class="sortable-header">Auth Code<span></span></th>
												<th id="provider" onclick="sortTableState.sortByColumn(this);" class="sortable-header">Provider<span></span></th>
												<th id="acceptedDate" onclick="sortTableState.sortByColumn(this);" class="sortable-header">Accepted<span></span></th>
												<th id="expectedDate" onclick="sortTableState.sortByColumn(this);" class="sortable-header">Expected<span></span></th>
												<th id="deliveredDate" onclick="sortTableState.sortByColumn(this);" class="sortable-header">Delivered<span></span></th>
												<!--<th id="ocr">OCR<span></span></th>-->
												<!-- <th>Entered By</th> -->
												<th id="careNavigator" onclick="sortTableState.sortByColumn(this);" class="sortable-header">Care Navigator<span></span></th>
												<th id="status" style="display:{!IF(filterByStage == 'Needs Attention', '', 'none')}" onclick="sortTableState.sortByColumn(this);">Status<span></span></th>
                                                <!--<th id="futureDos" style="display:{!IF(filterByStage == 'Needs Attention', '', 'none')}">Future DOS<span></span></th>--><!-- Removing as of Sprint 2/13 -VK -->
                                            </tr>
										</thead>
										<tbody>
											<apex:repeat value="{!orderList}" var="order">
												<tr>
													<td width="10%" style="white-space: nowrap;"><span class="status-small {!IF((order.Stage__c='Pending Acceptance'),'status-pending','')} {!IF((order.Stage__c='In Progress'),'status-progress','')} {!IF((order.Stage__c='Needs Attention'),'status-attention','')} {!IF((order.Stage__c='Recurring'),'status-recurring','')} {!IF((order.Stage__c='Cancelled'),'status-cancelled','')} {!IF((order.Stage__c='Completed'),'status-completed','')} {!IF((order.Stage__c='Future DOS'),'status-future','')}"></span>
														<apex:outputLink value="PayerOrderSummary_CS" styleclass="link darker">{!order.Name}
	                                    					<apex:param name="oid" value="{!order.Id}" />
														</apex:outputLink></td>
													<td class="push-date">
														<apex:outputtext value="{0,date,MM/dd/yyyy}" escape="false">
															<apex:param value="{!IF( AND(order.CreatedDate != null,(order.CreatedDate >= $Setup.DST_Dates__c.CorrectionStartDate__c && 
                                                       		order.CreatedDate <= $Setup.DST_Dates__c.CorrectionEndDate__c)), order.CreatedDate - (5/24),  order.CreatedDate - (4/24))}" />
                                                            <!-- changing 4/24 to timeZoneOffset for daylight saving end - 11/21/2016 -->
														</apex:outputtext> 
														<span class="summary-time"> 
															<apex:outputtext value="{0,date,h:mm a}" escape="false">
																<apex:param value="{!IF( AND(order.CreatedDate != null,(order.CreatedDate >= $Setup.DST_Dates__c.CorrectionStartDate__c && 
                                                       			order.CreatedDate <= $Setup.DST_Dates__c.CorrectionEndDate__c)), order.CreatedDate - (5/24),  order.CreatedDate - (4/24))}" />
                                                                <!-- changing 4/24 to timeZoneOffset for daylight saving end - 11/21/2016 -->
															</apex:outputtext>
														</span></td>
													<!-- 
													<td>
														<apex:outputPanel rendered="{!IF(isBlank(order.Comments__c),False,True)}">
															<a onclick="openModal('{!order.Name}','{!URLENCODE(order.Comments__c)}');"><i class="foundicon-page"></i></a>
														</apex:outputPanel></td> 
													-->
													<td><span data-options="disable-for-touch:true" class=""> {!order.Plan_Patient__r.Name}</span></td>
													<td class="text-uppercase">{!order.Authorization_Number__c}</td>
													<td class="text-secondary">
														<apex:outputPanel rendered="{!IF(isBlank(order.Provider_Phone__c),True,False)}">
                                                            <a onclick="openProviderPhoneModal('{!order.Provider__r.Name}','{!URLENCODE(order.Provider__r.Phone)}');">
                                                                <span data-options="disable-for-touch:true" class=""> {!order.Provider__r.Name}</span>
                                                            </a> 
														</apex:outputPanel>
														<!--<apex:outputPanel rendered="{!IF(isBlank(order.Provider_Phone__c),True,False)}">
															<span data-options="disable-for-touch:true" class=""> {!order.Provider__r.Name}</span>
														</apex:outputPanel>--></td>
													<td class="push-date">
														<apex:outputtext value="{0,date,MM/dd/yyyy}" escape="false">
															<apex:param value="{!IF( AND(order.Accepted_Date__c != null,(order.Accepted_Date__c >= $Setup.DST_Dates__c.CorrectionStartDate__c && 
                                                       		order.Accepted_Date__c <= $Setup.DST_Dates__c.CorrectionEndDate__c)), order.Accepted_Date__c - (5/24), order.Accepted_Date__c - (4/24))}" />
                                                            <!-- changing 4/24 to timeZoneOffset for daylight saving end - 11/21/2016 -->
														</apex:outputtext> 
														<span class="summary-time"> 
															<apex:outputtext value="{0,date,h:mm a}" escape="false">
																<apex:param value="{!IF( AND(order.Accepted_Date__c != null,(order.Accepted_Date__c >= $Setup.DST_Dates__c.CorrectionStartDate__c && 
                                                       			order.Accepted_Date__c <= $Setup.DST_Dates__c.CorrectionEndDate__c)), order.Accepted_Date__c - (5/24), order.Accepted_Date__c - (4/24))}" />
                                                                <!-- changing 4/24 to timeZoneOffset for daylight saving end - 11/21/2016 -->
															</apex:outputtext>
														</span></td>
													<td class="push-date">
														<apex:outputtext value="{0,date,MM/dd/yyyy}" escape="false">
															<apex:param value="{!IF( AND(order.Estimated_Delivery_Time__c != null,(order.Estimated_Delivery_Time__c >= $Setup.DST_Dates__c.CorrectionStartDate__c && 
                                                       		order.Estimated_Delivery_Time__c <= $Setup.DST_Dates__c.CorrectionEndDate__c)), order.Estimated_Delivery_Time__c - (5/24), 
                                                      		order.Estimated_Delivery_Time__c - (4/24))}" /><!-- changing 4/24 to timeZoneOffset for daylight saving end - 11/21/2016 -->
														</apex:outputtext>
														<span class="summary-time">
															<apex:outputtext value="{0,date,h:mm a}" escape="false">
																<apex:param value="{!IF( AND(order.Estimated_Delivery_Time__c != null,(order.Estimated_Delivery_Time__c >= $Setup.DST_Dates__c.CorrectionStartDate__c && 
                                                       		order.Estimated_Delivery_Time__c <= $Setup.DST_Dates__c.CorrectionEndDate__c)), order.Estimated_Delivery_Time__c - (5/24), 
                                                      		order.Estimated_Delivery_Time__c - (4/24))}" /><!-- changing 4/24 to timeZoneOffset for daylight saving end - 11/21/2016 -->
															</apex:outputtext>
														</span></td>
                                                    <td class="push-date"><!--Staging for Sprint 12/12--><!--<td class="push-date" style="display:{!IF(filterByStage == '' || filterByStage == 'Completed', '', 'none')}"></td>-->
														<apex:outputtext value="{0,date,MM/dd/yyyy}" escape="false">
															<apex:param value="{!IF( AND(order.Delivery_Date_Time__c != null,(order.Delivery_Date_Time__c >= $Setup.DST_Dates__c.CorrectionStartDate__c && 
                                                       		order.Delivery_Date_Time__c <= $Setup.DST_Dates__c.CorrectionEndDate__c)), order.Delivery_Date_Time__c - (5/24), 
                                                       		order.Delivery_Date_Time__c - (4/24))}" /><!-- changing 4/24 to timeZoneOffset for daylight saving end - 11/21/2016 -->
														</apex:outputtext> 
														<span class="summary-time">
															<apex:outputtext value="{0,date,h:mm a}" escape="false">
																<apex:param value="{!IF( AND(order.Delivery_Date_Time__c != null,(order.Delivery_Date_Time__c >= $Setup.DST_Dates__c.CorrectionStartDate__c && 
                                                       			order.Delivery_Date_Time__c <= $Setup.DST_Dates__c.CorrectionEndDate__c)), order.Delivery_Date_Time__c - (5/24), 
                                                       			order.Delivery_Date_Time__c - (4/24))}" /><!-- changing 4/24 to timeZoneOffset for daylight saving end - 11/21/2016 -->
															</apex:outputtext>
														</span></td>
													<!--<td><apex:outputPanel rendered="{!IF(isBlank(order.Order_Image__c),False,True)}">
															<a href="{!order.Order_Image__c}" target="_new"><i class="foundicon-page"></i></a>
														</apex:outputPanel></td>-->
													<!-- <td class="text-secondary">{!order.Entered_By__r.Name}</td> -->
													<td class="text-secondary">{!order.Case_Manager__r.Name}</td>
													<td style="display:{!IF(filterByStage == 'Needs Attention', '', 'none')}" class="text-secondary">{!order.NeedsAttentionReason__c}</td>
                                                    <!--<td style="display:{!IF(filterByStage == 'Needs Attention', '', 'none')}">
                                                        <img style="display:{!IF(order.Status__c == 'Needs Attention - Future DOS', '', 'none')}" src="/img/checkbox_checked.gif" alt="Checked" width="21" height="16" class="checkImg" title="Checked" />
                                                        <img style="display:{!IF(order.Status__c == 'Needs Attention - Future DOS', 'none', '')}" src="/img/checkbox_unchecked.gif" alt="Not Checked" width="21" height="16" class="checkImg" title="Not Checked" />
                                                    </td>--><!-- Removing as of Sprint 2/13 - VK -->
</tr>
											</apex:repeat>
										</tbody>
									</table>
									<!--patient-selection-->
								</div>
								<!--order-summary-->
							</div>
							<!--row order-summary-->
	
							<!--pagination-->
							<div class="row" style="{!IF((totalPages > 1),'visibility:visible;','visibility:hidden;')}">
								<div class="large-12 small-12 columns text-center">
									<ul class="pagination">
										<li class="arrow {!IF((!orderSetCon.HasPrevious), 'unavailable','')}">
											<apex:commandLink value="<" action="{!orderSetCon.Previous}" rerender="orderResults, OrderCount, pagination" oncomplete="$(document).foundation();" status="status" /></li>
										<li>{!orderSetCon.PageNumber} of {!totalPages}</li>
										<li class="arrow {!IF((!orderSetCon.HasNext), 'unavailable','')}">
											<apex:commandLink value=">" action="{!orderSetCon.Next}" rerender="orderResults, OrderCount" oncomplete="$(document).foundation();" status="status" /></li>
									</ul>
								</div>
								<!--pagination-->
							</div>
							<!--.row .pagination-->
						</apex:outputPanel>
					</div>
					<!--page-inner-push-->
	
				</div>
				<!--.page-wrap-->
	
				<!--Modal Window for Notes-->
				<!--
			<div id="notesModal" class="reveal-modal small">
			    <h4>Notes for <span class="text-secondary"><span id="theOrderID"></span></span></h4>
			    <hr/>
			    <p class="text-secondary" id="theNoteContent"></p>
			
			    <a class="close-reveal-modal">&#215;</a>
			</div>-->
				<!--Reveal Modal-->
	
				<!--Modal Window for Notes-->
				 
			<div id="providerPhoneModal" class="reveal-modal small">
			    <h4><span class="text-secondary"><span id="theProviderName"></span></span></h4>
			    <hr/>
			    <p class="text-secondary" id="theProviderPhone"></p>
			
			    <a class="close-reveal-modal">&#215;</a>
			</div>
				<!--Reveal Modal-->
	
				<script>
					function urldecode(url) {
				 		return decodeURIComponent(url.replace(/\+/g, ' '));
					}
					
					function openModal(theid,thetext) {
						$('#theOrderID').text(theid);
						$('#theNoteContent').text(urldecode(thetext));
						$(document).foundation();
						$('#notesModal').foundation('reveal', 'open');
					
					}
					
					function openProviderPhoneModal(theprovider,thephone) {
						$('#theProviderName').text(theprovider);
						$('#theProviderPhone').text(urldecode('Phone: ' + thephone));
						$(document).foundation();
						$('#providerPhoneModal').foundation('reveal', 'open');
					
					}
					
					function pressEnterToSearch(ev){
					   	if (window.event && window.event.keyCode == 13 || ev.which == 13) {
					   		doSearch();
					       	return false;
					     } else {
					      	return true;
					     }
					}
					 
					function getDocumentHeight() {
			    		docHeight = $j(document).height();
			    		$("#loading").css({'height' : docHeight});
			  		}

                    var sortTableState = new sortTable(doSearch, '{!sortOrder}');
                    $(document).ready(
                            function() {
                                resetSorting();
                                sortTableState.markSortedColumn();
                            }
                    );

                    function resetSorting() {
                        sortTableState.init('{!sortColumn}');
                    }

                    function doSearch() {
                        if (sortTableState.currentColumnState.columnId != null) {
                            doBackendSearch(sortTableState.currentColumnState.columnId, sortTableState.currentColumnState.columnSortOrder);
                        }
                        else {
                            doBackendSearch();
                        }
                    }
			  		
			  		var target = document.getElementById('spinjs');
					var spinner = new Spinner(largeSpinner).spin(target);
				</script>
	
				<c:PayerFooter_CS />
	
			</apex:pageBlock>
		</span> <!-- End of Action Status  -->
	</apex:form>
</apex:page>