<apex:page controller="PayerPortalServices_CS" showHeader="false" sidebar="false" standardStyleSheets="false" cache="false" >

<c:PayerHeader_CS accountName="{!portalUser.instance.Entity__r.Name}" />

<div class="page-wrap">

	<c:PayerNavigation_CS userName="{!portalUser.instance.Name}" accountName="{!portalUser.instance.Entity__r.Name}" />

	<div class="row">
		<div class="large-12 small-12 colums push-top">
			<div class="large-12 columns">
				<h3 class="text-left push-top">Terms &amp; Conditions</h3>
			</div>
		</div>
		<div class="large-12 small-12 columns push-top">
			<p class="lead text-secondary text-left">Terms of Use</p>
			<p class="text-justify">This Terms and Conditions of Use Agreement (&#8220;Agreement&#8221;) is a legal agreement between you and <strong><em>Integra Partners Holdings, LLC and its affiliates</em></strong> (collectively &#8220;<strong><em>Integra</em></strong>&#8220;) governing your use of this website (the &#8220;site&#8221;).  The information and content on this website is for your general educational information only and should not be considered medical advice.  If you are experiencing a medical emergency, you should not rely on any information on this site and should seek appropriate emergency medical assistance, such as calling “911”.</p>
			<p class="text-justify">Your use of this site is subject to the terms and conditions of this Agreement. By accessing, using and/or viewing our site, you hereby agree to be bound by the terms of this Agreement. If you do not agree to the terms and conditions, you must exit this site.</p>
			<p class="text-justify">We reserve the right to change the terms of this Agreement at any time. If you access our site at any time after such a change in terms of the Agreement is posted, then you are deemed to have accepted and agreed to all of the revisions.</p>
			
			<p class="lead text-secondary text-left">Privacy Policy</p>
			<p class="text-justify">Please review our Privacy Policy to understand our practices regarding your privacy in connection with our online services.</p>
			
			<p class="lead text-secondary text-left">Disclaimer</p>
			<p class="text-justify">Our site contains various types of information originating from ourselves or from third parties, including outside vendors that provide health-related information or goods and services. As the user of this site, you assume full risk and responsibility for any and all use of this site, including the information presented on the site. You agree to hold <strong><em>Integra </em></strong>harmless from any and all claims relating to your use of, your reliance upon, or errors or omissions in, information found on this site.</p>
			<p class="text-justify">The services, information, and functions contained in this site are provided &#8220;as is&#8221; without warranty of any kind. We expressly disclaim any and all representations and warranties, expressed or implied, including those of accuracy, completeness, non-infringement, merchantability, and fitness for a particular use or purpose, concerning our services or the adequacy, accuracy, or completeness of the information or services included on our site, or that the services will be uninterrupted or error free.</p>
			
			<p class="lead text-secondary text-left">Limitation of Liability</p>
			<p class="text-justify">Use of our services is at your own risk. With regard to outside vendors and information providers, we do not endorse, or otherwise recommend or approve any product or information located on or available through our site. We do not own or control other networks, sites, or hyperlinked sites outside of our own site, including the outside vendors accessed through our site.</p>
			<p class="text-justify">We will not be liable to you or anyone else for any harm to you or others resulting from the use of our site and/or the products and services provided through this site.   This protection covers claims based on warranty, contract, tort, strict liability and any other legal theory and all losses including without limitation direct or indirect, special, incidental, consequential, exemplary and punitive damages, personal injury/wrongful death, lost profits or damages resulting from lost data or business interruption.  If you are dissatisfied with this website or the content, your sole and exclusive remedy is to discontinue using the website.</p>
			<p class="text-justify">We are not responsible or liable for acts or omissions of outside vendors or information providers, or for performance or non-performance within outside networks or interconnection points between our site and other networks and/or sites operated by third parties. We also make no representation regarding your ability to transmit and receive information from or through our site, and you agree and acknowledge that at times your ability to access our site may be impaired or disrupted. Although we will use commercially reasonable efforts to take any action we consider appropriate to remedy and avoid such events, we cannot guarantee that they will not occur, and accordingly we disclaim any and all liability resulting from or related to such events.</p>
			<p class="text-justify">We are not liable for any failure or delay in our performance due to any cause beyond our reasonable control, including acts of war, acts of God, earthquake, flood, embargo, riot, sabotage, labor shortage or dispute, governmental act, or failure of the internet.</p>
			
			<p class="lead text-secondary text-left">Indemnification</p>
			<p class="text-justify">You agree to defend, indemnify, and hold <strong><em>Integra</em></strong> and its directors, officers, employees, agents, affiliates, and successors harmless from and against any and all claims, demands, liabilities, judgments, losses, damages, costs, fees, and expenses (including but not limited to reasonable attorneys&#8217; fees) arising from or relating to your use of the site or your acts or omissions, including but not limited to (1) infringement or misappropriation of any intellectual property rights, (2) defamation, libel, slander, obscenity, pornography, or violation of the rights of privacy or publicity, (3) spamming, or any other offensive, harassing, or illegal conduct or violation of this Agreement, or (4) any damage or destruction to our site, to us, or to other persons or parties.</p>
			
			<p class="lead text-secondary text-left">Termination and Investigation</p>
			<p class="text-justify"><strong><em>Integra</em></strong> may terminate access granted to you at any time if you are in breach of this Agreement. Access will terminate immediately without notice from <strong><em>Integra</em></strong> if, in <strong><em>Integra</em></strong>&#8216;s sole discretion, you fail to comply with any term or provision of this Agreement. Upon such termination, you may not access, use, or view the site.</p>
			<p class="text-justify">We reserve the right to investigate suspected violations of this Agreement. If we become aware of possible violations, we may initiate an investigation that may include gathering information from you or any user involved and the complaining party, if any, and examination of other material. We may suspend the provision of our services temporarily, or we may permanently remove the material involved from our servers, cancel posts, provide warnings to you, or suspend or terminate your access to our services. We will determine what action will be taken in response to a violation on a case-by-case basis, and at our sole discretion. We will fully cooperate with law enforcement authorities in investigating suspected lawbreakers.</p>
			
			<p class="lead text-secondary text-left">Breach</p>
			<p class="text-justify">If you breach any of the terms of this Agreement, your right to access the content terminates immediately.  Upon such termination, you must stop using this website, including all content, and return or destroy all copies, including electronic copies, of the content in your possession.  In addition, you hereby acknowledge that your breach of this Agreement may result in immediate and irreparable harm to <strong><em>Integra</em></strong>, for which there will be no adequate remedy at law, and that <strong><em>Integra</em></strong> shall be entitled to equitable relief to compel you to cease and desist all unauthorized use, evaluation, and/or disclosure of information obtained through the site. Your breach of this Agreement may also entitle <strong><em>Integra</em></strong> to bring an action against you for any and all other remedies available at law or in equity.</p>
			
			<p class="lead text-secondary text-left">No Waiver, Severability</p>
			<p class="text-justify">A waiver of any breach of any provision of this Agreement shall not be deemed to be a waiver of any repetition of such breach or in any manner affect any other terms or conditions of this Agreement. In the event that any provision of this Agreement is held to be unenforceable, it will not affect the validity or enforceability of the remaining provisions and will be replaced by an enforceable provision that is the closest to the intention underlying the unenforceable provision.</p>
			
			<p class="lead text-secondary text-left">Jurisdiction</p>
			<p class="text-justify">We operate and control our site from our location in New York, United States of America. We make no representation that any information, materials, or functions included on or through our site are appropriate for use in any other jurisdiction. If you choose to access our site from locations other than New York, you do on your own initiative and are responsible for compliance with applicable laws and regulations</p>
			<p class="text-justify">This Agreement, and the rights and obligations of you and <strong><em>Integra </em></strong>hereunder, shall be governed by, and construed in accordance with, the laws of the State of New York. You further agree that any legal action or proceeding between you and <strong><em>Integra</em></strong> arising out of or relating to this Agreement or your use of the site shall be brought exclusively in a federal or state court of competent jurisdiction sitting in New York, New York, and you hereby expressly and irrevocably consent to the jurisdiction and venue of such courts.</p>
			
			<p class="lead text-secondary text-left">Assignability</p>
			<p class="text-justify">We may assign our rights and delegate our duties under this Agreement either in whole or in part at any time, at our sole discretion, and without your consent. You may not assign, sublicense, or otherwise transfer your rights or obligations, in whole or in part, under this Agreement to anyone else.</p>
			
			<p class="lead text-secondary text-left">Relationship</p>
			<p class="text-justify">This Agreement does not establish any relationship of partnership, joint venture, employment, franchise, or agency between you and us.</p>
		</div>
	</div><!--row-->
</div><!--page-wrap-->

<c:PayerFooter_CS />

</apex:page>