<apex:component controller="AdditionalDetailsController_IP" selfClosing="true" layout="block">
	<apex:attribute name="cssStyle" description="style for the input component" type="String"/>
	<apex:attribute name="value" description="group to which current user belongs to" type="String" assignTo="{!belongsTo}" required="true" />
	<apex:attribute name="vfPageController" type="PageControllerBase_IP" assignTo="{!pageController}" required="true" description="The controller for the page." />
    <apex:attribute name="componentKey" type="String" assignTo="{!key}" description="The key given to this component so the page can easily get access to it" required="true" />
    <apex:attribute name="sectionsToRender" type="String" description="Fill in sections to render or the action function" />
    <apex:attribute name="actFunToRun" type="String" description="Action Functions to run" />
    <style>
      .errors .message {
         background: none !important;
         border: none !important;
         width: 70% !important;
      }
   </style>
    <!-- START ADDITIONAL DETAILS PANEL -->
    <!--<script type="text/javascript">
        function openSubTab(url,label,name,openimmediately) {
            sforce.console.getEnclosingPrimaryTabId(function(primarytab){
                sforce.console.openSubtab(primarytab.id , url, openimmediately, label, null, function(result){
                  // do any processing here if passes or fails, use result.success (boolean)
                }, name);
            });
        }
    </script>-->

    <apex:outputPanel id="additionalDetailsContainerHeader" styleClass="slds-grid slds-container--fluid" style="{!cssStyle}">
    	<div class="slds-grid" style="background: #33A97F;border-radius: 25px; width: 50%">
            <div class="slds-text-heading--small" style="color: #ffffff; padding-left: 20px;">Additional Details related to this case</div>
        </div>
        <br />
	</apex:outputPanel>
	<apex:outputPanel styleClass="errors" id="errorPanel" >
    	<!--<apex:pageMessages />-->
    </apex:outputPanel>
	<apex:outputPanel id="additionalDetailsContainerScope" styleClass="slds-grid slds-container--fluid" style="{!cssStyle}">
		<div class="slds-form--compound slds-container--fluid">
			<apex:outputPanel id="additionalDetailsSelection" styleClass="slds-grid">
				<div class="slds-p-horizontal--small slds-size--1-of-2 slds-medium-size--3-of-6 slds-large-size--4-of-12">
	              <div class="slds-form-element">
	                <label class="slds-form-element__label">Select the detail you want to add
	                    &nbsp; &nbsp;<!--<h3 style="float:right;"><apex:outputText value="ADDITIONAL DETAIL TYPE IS REQUIRED TO PROCEED" rendered="{!IF(errorFlag == true && selectedDetailRecType != '',true,false)}" style="color: red;" /></h3> -->
	                </label>
	                <div class="slds-form-element__control">
	                  <apex:selectList styleClass="slds-input" value="{!selectedDetailRecType}" id="addDetailType" size="1">
	                    <apex:selectOptions value="{!DetailsRecType}" />
	                    <apex:actionSupport event="onchange" reRender="additionalDetailsSelection,{!sectionsToRender}" status="loadingStatus" />
	                  </apex:selectList>
	                </div>
	              </div>
	            </div>
	            <div class="slds-p-horizontal--small slds-size--1-of-2 slds-medium-size--3-of-6 slds-large-size--4-of-12">
                	<div class="slds-form-element">
	                    <label class="slds-form-element__label" for="addDetailInput">
	                        &nbsp;&nbsp; <apex:outputText value="{!addDetailsErrorMsg}" style="color: red;" rendered="{!addDetailsError}" id="detailErrorMsg" />
	                    </label>
	                    <div class="slds-form-element__control">
	                      	<apex:inputField styleClass="slds-input" value="{!newDetail.zPaperReferenceNumber__c}" rendered="{!selectedDetailRecType == 'zPaper Reference Number'}" html-placeholder="Enter {!selectedDetailRecType}" />
	                      	<apex:inputField styleClass="slds-input" value="{!newDetail.HCPC_Code__c}" rendered="{!selectedDetailRecType == 'HCPC Code'}" html-placeholder="Enter {!selectedDetailRecType}" />
	                      	<apex:inputField styleClass="slds-input" value="{!newDetail.Order__c}" rendered="{!selectedDetailRecType == 'Order Number'}" html-placeholder="Enter {!selectedDetailRecType}"/>
	                      	<apex:inputField styleClass="slds-input" value="{!newDetail.QueClaimID__c}" rendered="{!selectedDetailRecType == 'Que Claim ID'}" html-placeholder="Enter {!selectedDetailRecType}" />
	                      	<!--<apex:inputField styleClass="slds-input" value="{!newDetail.ReferredProvider__c}" rendered="{!selectedDetailRecType == 'Referred Provider'}" html-placeholder="Enter {!selectedDetailRecType}" />-->
							<c:AutoCompleteComponent_IP allowClear="false" labelField="AccountName__c" SObject="Account" valueField="Id" targetField="{!newDetail.ReferredProvider__c}" caseRecordType="Provider" plHolder="Enter Provider Name" rendered="{!selectedDetailRecType == 'Referred Provider' || selectedDetailRecType == 'Current Servicing Provider' || selectedDetailRecType == 'Referral Rerouted to New Provider'}" importJquery="true" searchFilter="Account_Name" />
	                      	<apex:inputField styleClass="slds-input" value="{!newDetail.AuthorizationNumber__c}" rendered="{!selectedDetailRecType == 'Authorization Number'}" html-placeholder="Enter {!selectedDetailRecType}" />
	                    </div>
                  	</div>
                </div>
                <div class="slds-p-horizontal--small slds-size--1-of-2 slds-medium-size--3-of-6 slds-large-size--4-of-12">
                    <div class="slds-form-element">
                        <label class="slds-form-element__label" for="addDetailInput"></label>
                        <div class="slds-form-element__control">
                          <apex:commandButton reRender="additionalDetailsContainerScope,{!sectionsToRender},errorPanel" styleClass="slds-button slds-button--brand" value="Add Detail" action="{!addDetailsToList}" status="loadingStatus" oncomplete="actFunToRun" rendered="{!selectedDetailRecType != ''}">
                          </apex:commandButton>
                        </div>
                      </div>
                    </div>
			</apex:outputPanel>
			<br />
			<apex:outputPanel id="additionalDetailsReviewList" styleClass="slds-grid" rendered="{!additionalDetailsList.size > 0}">
				<div class="slds-p-horizontal--small slds-size--1-of-1 slds-medium-size--6-of-6 slds-large-size--11-of-12">
					<apex:dataTable value="{!additionalDetailsList}" var="add" styleClass="slds-table slds-table__bordered slds-table__cell-buffer slds-table__striped" border="1">
						<apex:column styleClass="slds-text-title_caps">
							<apex:facet name="header">Detail Type</apex:facet>
                          	<apex:outputText value="{!add.detailAdded.AdditionalDetail__c}" />
						</apex:column>
						<apex:column styleClass="slds-text-title_caps">
							<apex:facet name="header">Detail Info</apex:facet>
                            <apex:outputField styleClass="slds-text-heading--small" value="{!add.detailAdded.zPaperReferenceNumber__c}" rendered="{!add.detailAdded.AdditionalDetail__c == 'zPaper Reference Number'}"  />
                            <apex:outputField styleClass="slds-text-heading--small" value="{!add.detailAdded.HCPC_Code__c}" rendered="{!add.detailAdded.AdditionalDetail__c == 'HCPC Code'}"  />
                            <apex:outputField styleClass="slds-text-heading--small" value="{!add.detailAdded.Order__c}" rendered="{!add.detailAdded.AdditionalDetail__c == 'Order Number'}" />
                            <apex:outputField styleClass="slds-text-heading--small" value="{!add.detailAdded.QueClaimID__c}" rendered="{!add.detailAdded.AdditionalDetail__c == 'Que Claim ID'}"  />
                            <apex:outputField styleClass="slds-text-heading--small" value="{!add.detailAdded.ReferredProvider__c}" rendered="{!add.detailAdded.AdditionalDetail__c == 'Referred Provider' || add.detailAdded.AdditionalDetail__c == 'Current Servicing Provider' || add.detailAdded.AdditionalDetail__c == 'Referral Rerouted to New Provider'}"  />
                            <apex:outputField styleClass="slds-text-heading--small" value="{!add.detailAdded.AuthorizationNumber__c}" rendered="{!add.detailAdded.AdditionalDetail__c == 'Authorization Number'}" />
						</apex:column>
						<apex:column styleClass="slds-text-title_caps"><!--rendered="{!userGroup == 'Claims'}"-->
							<apex:facet name="header">Links</apex:facet>
							<!--<apex:outputLink value="https://internal.accessintegra.com/Claim/SentClaimDetails/{!add.detailAdded.QueClaimID__c}" rendered="{!add.detailAdded.AdditionalDetail__c == 'Que Claim ID'}" target="_self" >Click here to access this claim in que</apex:outputLink>-->
							<apex:outputPanel rendered="{!add.detailAdded.AdditionalDetail__c == 'Que Claim ID'}">
								<a href="https://internal.accessintegra.com/Claim/SentClaimDetails/{!add.detailAdded.QueClaimID__c}" target="_blank">Click here to access this claim in que</a>
							</apex:outputPanel>
                        </apex:column>
						<apex:column styleClass="slds-text-title_caps">
							<apex:facet name="header">Actions</apex:facet>
							<apex:commandLink value="Remove" action="{!removeDetailsFromList}" reRender="additionalDetailsContainerScope" status="loadingStatus" >
								<apex:param name="index" value="{!add.selectedCounter}" />
							</apex:commandLink>
						</apex:column>
					</apex:dataTable>
				</div>
			</apex:outputPanel>
		</div>
	</apex:outputPanel>
</apex:component>