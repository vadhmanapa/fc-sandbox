trigger ARAGFollowUp on ARAG_Follow_Up__c (before insert, after insert, after delete){        
    
    // deprecated as this function is taken care of by list button in salesforce
    
    /*if (Trigger.IsBefore) {
        if (Trigger.isInsert) {
        
            for(ARAG_Follow_Up__c afu : Trigger.new){
        
                if(afu.Worked_By__c == null){                  
                    afu.Worked_By__c = UserInfo.getUserId();
                }
            }
        
        }
    }*/
    
    if (Trigger.isAfter) {
        if (Trigger.isInsert) {
            ARAGFollowUp_Dispatcher.doAfterInsert(Trigger.new,Trigger.oldMap);
        }
        if (Trigger.isDelete) {
            ARAGFollowUp_Dispatcher.doAfterDelete(Trigger.old,Trigger.oldMap);
        }   
    }
    
}