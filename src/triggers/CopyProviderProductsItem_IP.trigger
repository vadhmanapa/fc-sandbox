trigger CopyProviderProductsItem_IP on Provider_Product_Item__c (
	before insert, 
	before update) {

		if (Trigger.isBefore) {
	    	//call your handler.before method
	    	CopyProductsToParent_IP.getTriggerData(Trigger.new);
	    
		} 
}