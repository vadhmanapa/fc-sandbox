trigger MapWorkItemsToDenials on Denials__c (before update) {
    List<ARAG__c> matchedBill = new List<ARAG__c>();
    for(Denials__c deny : Trigger.new){
        if(deny.Work_Denial__c == true){
             // based on bill number in denial, search CWI for a match
            matchedBill = [Select Id, Name, Bill__c, Total_Due__c, Account_Number__c, Payor_Family_text__c, Patient_Name__c,
                       Billed_Date__c, Visit_Date__c, Status__c from ARAG__c where Bill__c = :deny.Bill__c ORDER BY CreatedDate DESC];
    // when a match is found, retrieve results from the recently updated ARAG
            if (matchedBill.size() > 0) {
                // update denial
                deny.Account_Number__c = matchedBill[0].Account_Number__c;
                deny.Patient_Name__c = matchedBill[0].Patient_Name__c;
                deny.Billed_Date__c = matchedBill[0].Billed_Date__c;
                deny.Amount_Outstanding__c = matchedBill[0].Total_Due__c;
                deny.Payor__c = matchedBill[0].Payor_Family_text__c;
                deny.Visit_Date__c = matchedBill[0].Visit_Date__c;
                deny.Related_CWI__c = matchedBill[0].Id;
                
                matchedBill[0].Status__c = 'Denied';
                update matchedBill;
            }          
        }
     }
 }