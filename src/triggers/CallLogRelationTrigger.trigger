trigger CallLogRelationTrigger on Call_Log_Relation__c (before insert, before update) {

	if(Trigger.isBefore) {
		if(Trigger.isInsert) {
			CallLogRelationTriggerDispatcher.doBeforeInsert(Trigger.new);
		} else if(Trigger.isUpdate) {
			CallLogRelationTriggerDispatcher.doBeforeUpdate(Trigger.new);
		}
	}
}