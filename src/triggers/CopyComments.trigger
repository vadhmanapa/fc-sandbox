trigger CopyComments on Notes__c (after insert, after update) {
  List<Initiative__c> linkedInitiative = new List<Initiative__c>();
  List<Initiative__c> updateInitiative = new List<Initiative__c>();
  //List<Notes__c> noteList = new List <Notes__c>();
  String actualComments;
  String newComments;
  String joinedComments;
  String commentedUser;
  String commentedDate;
  String mostRecent;
  for(Notes__c newNote : Trigger.new){
  	String noteId;
  	 	List<Notes__c> noteList = [Select Id, Initiative__r.Id, Note__c, LastModifiedBy.Name, LastModifiedDate from Notes__c where Id =: newNote.Id];
  	 	if (noteList.size() > 0){
  	 		noteId = noteList[0].Initiative__r.Id;
  			System.debug('The Id of note is' +noteId);
  			newComments = noteList[0].Note__c;
  			System.debug('The new note entered is' +newComments);
  			commentedUser = noteList[0].LastModifiedBy.Name;
  	 		commentedDate = string.valueof(noteList[0].LastModifiedDate);
  	 	}
  		linkedInitiative = [Select Id, Name, Comments_Update__c, Most_Recent_Comment__c from Initiative__c where Id =:noteId LIMIT 1];
  		if (linkedInitiative[0].Comments_Update__c != null){
  			actualComments = linkedInitiative[0].Comments_Update__c;
  			} else {
  				actualComments = '';
  			}
  		
  		System.debug('The size of queried initiave is :'+linkedInitiative.size());
  	 if (linkedInitiative.size() > 0){
  	 		joinedComments = commentedDate + ' '+'-' + ' ' + commentedUser + ' ' + ':' + ' ' + newComments+ '\r\n' + actualComments;
  	 		linkedInitiative[0].Comments_Update__c = joinedComments;
  	 		mostRecent = commentedDate + ' '+'-' + ' ' + commentedUser + ' ' + ':' + ' ' + newComments;
  	 		linkedInitiative[0].Most_Recent_Comment__c = mostRecent;
  	 	}
  	 	try{
  	 		update linkedInitiative;
  	 		} catch (System.Dmlexception e){
  	 			System.debug(e);
  	 		}
  	}
}