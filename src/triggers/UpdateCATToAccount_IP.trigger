trigger UpdateCATToAccount_IP on CAT__c (before insert, before update, after insert, after update) {

	Cred_Trigger_CS__c CTCS = Cred_Trigger_CS__c.getInstance();

	if (CTCS.Active__c == True){
		CATTriggerHandler_IP newRun = new CATTriggerHandler_IP();

		if(Trigger.isBefore) {
			

			if(Trigger.isInsert || Trigger.isUpdate) {
				
				newRun.runTrigger(Trigger.new, Trigger.oldMap, Trigger.newMap);
				
			}
		}

		if(Trigger.isAfter) {

			if(Trigger.isInsert){
				newRun.addCATM(Trigger.new);
			}		
			if(Trigger.isUpdate) {
				//newRun.createNewDataEntry(Trigger.new);
				newRun.fireEmails(Trigger.new, Trigger.oldMap);
			}
		}	
	}
}