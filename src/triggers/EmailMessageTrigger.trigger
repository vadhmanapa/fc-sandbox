trigger EmailMessageTrigger on EmailMessage (after insert) {
	if(Trigger.isInsert){
		EmailMessageServices.checkToReopenCases(Trigger.newMap);
		EmailMessageServices.updateCATStatus(Trigger.newMap);
	}
}