trigger DenialTrigger_CS on Denials__c (before insert, before update) {
    DenialDispatcher dispatch = new DenialDispatcher(
        trigger.oldMap, 
        trigger.newMap, 
        trigger.old, 
        trigger.new, 
        trigger.isInsert, 
        trigger.isUpdate, 
        trigger.isDelete, 
        trigger.isUndelete, 
        trigger.isBefore, 
        trigger.isAfter
    );
}