trigger TriggerOnPRTracker on Provider_Recoupment_Tracker__c (after insert, after update, after delete, after undelete) {

	LookUpRollUpUtils_IP newLRE = new LookUpRollUpUtils_IP();
	List<Provider_Recoupment_Tracker__c> updatedValues = new List<Provider_Recoupment_Tracker__c>();

	if(Trigger.isDelete){

		newLRE.rollUpAction(Trigger.old);

	}else{
		if(Trigger.isUpdate){

			for(Provider_Recoupment_Tracker__c pr : Trigger.new){

				if(pr.Amount_Recouped__c != Trigger.oldMap.get(pr.Id).Amount_Recouped__c){

					updatedValues.add(pr);
					
				} else{

					continue;
				}
			}

			if(updatedValues.size() > 0){

				newLRE.rollUpAction(updatedValues);
			}
		} else{

			newLRE.rollUpAction(Trigger.new);
		}
		
	}
}