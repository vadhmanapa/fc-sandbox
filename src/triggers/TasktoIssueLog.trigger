/* The following trigger is created by Integra Partners to create a Issue Log whenever a call is completed through Shoretel CTI.
    The Shoretel CTI tool creates a record of phone call as task related to the contact selected. That task is used and manipulated to create 
    a new Issue Log record*/

trigger TasktoIssueLog on Task (after insert, after update) {
  List<Issue_Log__c> callToIssueLog = new List<Issue_Log__c>();
  List<Issue_Log__c> currentLogs = new List<Issue_Log__c>();
  List<Issue_Log__c>updateLog = new List<Issue_Log__c>();
  List<Task> convertToLog = new List <Task>();
  // Map<Id, Issue_Log__c> issueId = new Map<Id, Issue_Log__c>();
  String accountId;
  String taskComment;
  Boolean isSuccess = false;
  String objectId;

  for (Task callTask : Trigger.new){
    if (callTask.skyplatform__Activity_Type__c == 'Call' && callTask.Status == 'Completed'){
      // this condition is satisfied, then go for next condition
      convertToLog.add (callTask);
      System.debug('Size of convert log'+ convertToLog.size());
      for (Task tsk : convertToLog){
            taskComment = tsk.Description;
                if (tsk.skyplatform__Unique_Call_Id__c != null){
                     System.debug('The Unique Id is' + tsk.skyplatform__Unique_Call_Id__c);
        System.debug('Run the main query to check whether this is insert or update');
            //DateTime dT = tsk.CreatedDate;
            //Date newDate = date.newinstance(dT.year(), dT.month(), dT.day());
         currentLogs = [Select Id, Name, Unique_Id__c, Call_Result__c, Call_Duration__c, Call_Type__c, Description__c, Task_Created_Date__c from Issue_Log__c where Task_Created_Date__c =: tsk.CreatedDate AND Unique_Id__c =: tsk.skyplatform__Unique_Call_Id__c  LIMIT 1];
         if (currentLogs.size() == 0){

          System.debug('The case is insert and no Issue_Log__c can be found');
          System.debug('Entering insert trigger');
          System.debug('Entering insert operation');
          System.debug('The task comment is' + taskComment); //before copying

          //setting accountId
          if (tsk.WhoId != null){
            accountId = tsk.AccountId;
          } else {
              List<Account> a = [Select Id, Name, Legal_Name__c from Account where Name LIKE 'Integra Partners%' AND Legal_Name__c LIKE 'Call Center%' LIMIT 1];
              if (a.size() > 0){
                accountId = a[0].Id;
              }
            }

           System.debug('The account Id is'+accountId);
           System.debug('Start copying fields');

        callToIssueLog.add(new Issue_Log__c (Account_Contact__c = tsk.WhoId, Account__c = accountId, Assigned_To__c = tsk.OwnerId,
                          Description__c = taskComment, Call_Start_Time__c = tsk.skyplatform__Call_Start_Date_Time__c, Call_Result__c = tsk.CallDisposition,
                          Call_Duration__c = tsk.CallDurationInSeconds, Unique_Id__c = tsk.skyplatform__Unique_Call_Id__c, Caller_Id__c = tsk.skyplatform__From_Phone__c,
                          To_Caller_Id__c = tsk.skyplatform__To_Phone__c, Call_Type__c = tsk.CallType, Phone_Call__c = true, Task_Created_Date__c = tsk.CreatedDate));
        
        System.debug('The number of records to be inserted' + callToIssueLog.size());

        // run dml operation

        try{
             Database.SaveResult[] taskList = Database.insert(callToIssueLog, false);
                      for (Database.SaveResult dsr : taskList) {
                        if (dsr.isSuccess()) {
                            // Operation was successful, so get the ID of the record that was processed
                            System.debug('Successfully inserted Issue Log. Issue Log ID: ' + dsr.getId());
                            isSuccess = true;
                            objectId = dsr.getId();
                          } else {
                              // Operation failed, so get all errors                
                              for(Database.Error err : dsr.getErrors()) {
                                    System.debug('The following error has occurred.');                    
                                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                                  System.debug('Issue Log fields that affected this error: ' + err.getFields());
                              }
                            }
                      }
              } catch (System.Dmlexception e){
                    System.debug('The task was not converted to Issue Log: ' + e.getMessage());
                  }

    } else if (currentLogs.size() > 0){

              System.debug('Entering update trigger');

                    // then log is already created.
                  if(taskComment != ''){
                      currentLogs[0].Description__c = taskComment;
                    }

                      currentLogs[0].Call_Result__c = tsk.CallDisposition;
                      currentLogs[0].Call_Duration__c = tsk.CallDurationInSeconds;
                      //updateLog.add (currentLogs[0]);
                      update currentLogs[0]; 
      } else {
        System.debug('There is some other issue');
        // do nothing
      }
     }else {
        System.debug('The Unique Id was not captured and hence record is not created');
        break;
     }
    }
    }
  }
}