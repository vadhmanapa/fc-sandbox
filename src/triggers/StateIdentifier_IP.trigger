trigger StateIdentifier_IP on State_Identifier__c (
	before insert, 
	before update, 
	before delete 
	) {

		if (Trigger.isBefore) {
	    	
	    	if(Trigger.isInsert || Trigger.isUpdate){

	    		StateIdentifierAgent_IP.stateMedicaidIU(Trigger.new);
	    	}

	    	if(Trigger.isDelete){

	    		StateIdentifierAgent_IP.stateMedicaidDel(Trigger.old);
	    	}
	    
		} 
	    
}