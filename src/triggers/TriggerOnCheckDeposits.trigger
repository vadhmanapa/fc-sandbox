trigger TriggerOnCheckDeposits on Check_Deposit__c (before insert) {
    if(Trigger.isBefore){
        if (Trigger.isInsert){
           // CheckDepositsTriggerDispatch_IP.CheckOnInsert(Trigger.New);
           Set<Double> paidAmount = new Set<Double>();
            Map<Double, Clearinghouse__c> keySet = new Map<Double, Clearinghouse__c>();
            Map<Id, Clearinghouse__c> newMap = new Map<Id, Clearinghouse__c>();
            Map<Id, Clearinghouse__c> dupeMap = new Map<Id, Clearinghouse__c>();
            List<String> planName = new List<String>();
            Integer numMatch = 0;
            Integer runNum = 0;
            for (Check_Deposit__c c : Trigger.New){
                // get all the amount values
                paidAmount.add(c.Paid_Amount__c);
            }
            
            // query based on the amount from Clearinghouse
            List <Clearinghouse__c> claimFile = [Select Id,
                                                 Name,
                                                 Amount__c,
                                                 Payer__c, Status__c, Payment_Date__c from Clearinghouse__c 
                                                 where Amount__c IN: paidAmount];
            
            // get all the short names from info
            List<Payor_Routing_Info__c> info = [Select Routing_Number__c, Short_Name__c from Payor_Routing_Info__c];
            
            //convert to a map
            Map <String,Payor_Routing_Info__c > routingInfo = new Map<String, Payor_Routing_Info__c>();
            for (Payor_Routing_Info__c pr : info){
                routingInfo.put(pr.Routing_Number__c, pr);
            }
            
            //  convert the clearinghouse query into a map
            Map<Double,Clearinghouse__c> matchFiles = new Map<Double, Clearinghouse__c>();
            for (Clearinghouse__c cl : claimFile){
                matchFiles.put(cl.Amount__c, cl);
                newMap.put(cl.Id, cl);
            }
            
       
            if (matchFiles.size() > 0){
                system.debug('size of map'+matchFiles.size());
                
                // start running on induvidual record
                for(Check_Deposit__c ch : Trigger.New){
                    
                runNum++;
                System.debug('Run Number'+runNum);
                System.debug('Amount'+ch.Paid_Amount__c);
                System.debug('Payer' +ch.Remitter_Name__c);
                Integer i = 0;
                String allIds = '';
                system.debug('Entering amount check');
                if(ch.Remitter_Name__c != null && ch.Paid_Amount__c != null){
                    dupeMap.clear();
                    
                // check if there is a unique match or multiple match                    
                    if (matchFiles.get(ch.Paid_Amount__c) != null){
                        
                        for (Clearinghouse__c cr : newmap.values()){
                            if (ch.Paid_Amount__c == newMap.get(cr.Id).Amount__c){
                                dupeMap.put(cr.Id, cr);
                            }
                        }
                        System.debug('Size of dupeMap'+ dupeMap.size());
                         keyset.clear();
                         keyset.put(matchFiles.get(ch.Paid_Amount__c).Amount__c,matchFiles.get(ch.Paid_Amount__c));
                         Integer repPayer = 0;
                         system.debug('Size of keyset'+keyset.size());
                        
                        // if value greater than one, then more claims file matched
                        if (dupeMap.size() > 1){
                            // date comparison on both
                            /*for(Id newId : dupeMap.keySet()){
                                if((newMap.get(newId).Payer__c).contains(ch.Remitter_Name__c)){
                                    repPayer++;
                                }
                                System.debug('Number of payer name matches'+repPayer);
                            }*/
                                    ch.Status__c = 'More than one possible match';
                                    for (Id newId : dupeMap.keySet()){
                                        if (allIds == ''){
                                            allIds = dupeMap.get(newId).Name;
                                        }else {
                                            allIds = allIds + dupeMap.get(newId).Name;
                                        }
                                    }
                                    ch.Possible_Matches__c = allIds;
                                                                   
                                    update newMap.values();
                                                   
                        } else {
                            // if the payment is unique
                            
                            if(ch.Paid_Amount__c == matchFiles.get(ch.Paid_Amount__c).Amount__c){
                                system.debug('Entering payor check');
                                String str = matchFiles.get(ch.Paid_Amount__c).Payer__c;
                                if (str != null){
                                    String[] splitString = str.split('\\s');
                                
                                for(String s : splitString){
                                    System.debug('String is' +s);
                                    if (s!= null && s.containsIgnoreCase(ch.Remitter_Name__c)){
                                        i++;
                                    }
                                    System.debug(logginglevel.INFO, 'Number of matched words'+i);
                                }
                                }
                                
                                if(i > 0){
                                    ch.Status__c = 'Claim File with matching deposit';
                                    ch.ClearingHouse_File__c = matchFiles.get(ch.Paid_Amount__c).Id;
                                    matchFiles.get(ch.Paid_Amount__c).Status__c = 'Deposit Matched,Claim Matched';
                                } else {
                                    ch.Status__c = 'Claim file no deposit';
                                    System.debug(logginglevel.ERROR, 'Match not found');
                                }
                                
                            }
                        }
                        
                            
                         }else {
                        
                                System.debug(logginglevel.INFO, '****No Clearinghouse Files found****');
                                    ch.Status__c = 'Claim file no deposit';
                                } 
                }
                     
                
           				 }
                update matchFiles.values();
            } else {
                for (Check_Deposit__c cd : Trigger.New){
                    System.debug(logginglevel.INFO, '****No Clearinghouse Files found****');
                    cd.Status__c = 'Claim file no deposit';
                }
            }
            
        }
    }
}