trigger CreateOrderTrigger_cs on New_Orders__c (after insert, after update) {
    
    /*
    Set<Id> newOrdersIds = new Set<Id>();
    Set<Id> orderIds = new Set<Id>();
    Set<String> codes = new Set<String>();
    Set<String> dmeCodes = new Set<String>();
    Map<Id, Map<String, Decimal>> newOrdersIdToCodeToUnit = new Map<Id, Map<String, Decimal>>();
    Map<String, List<DME_Line_Item__c>> codeToDMELineItem = new Map<String, List<DME_Line_Item__c>>();
    Map<String, DME__c> codeToDME = new Map<String, DME__c>();
    Map<Id, List<DME__c>> newOrdersIdToNewDME = new Map<Id, List<DME__c>>();
    Map<Id, List<DME_Line_Item__c>> newOrdersIdToLineItems = new Map<Id, List<DME_Line_Item__c>>();
    Map<String, List<Plan_Patient__c>> patientIdToPlanPatients = new Map<String, List<Plan_Patient__c>>();
    Map<Id, Plan_Patient__c> newOrdersIdToPlanPatient = new Map<Id, Plan_Patient__c>();
    //Map for getting Account Name off of Provider location. Legacy 
    Map<Id, Id> newOrdersIdToAccountIdMap = new Map<Id,Id>();
    
    for(New_Orders__c no: [Select Provider_Location__c, Provider_Location__r.Account__c From New_Orders__c Where Id IN :trigger.newMap.keyset()]){
        newOrdersIdToAccountIdMap.put(no.id,no.Provider_Location__r.Account__c);
    }//End newOrdersIdToAccountIdMap  
    
    for(New_Orders__c no : Trigger.new) {
        newOrdersIds.add(no.Id);
        Map<String, Decimal> codeToUnits = new Map<String, Decimal>();
        if(no.CPT_Code1__c != null && no.Units1__c != null) codeToUnits.put(no.CPT_Code1__c, no.Units1__c);
        if(no.CPT_Code_2__c != null && no.Units_2__c != null) codeToUnits.put(no.CPT_Code_2__c, no.Units_2__c);
        if(no.CPT_Code_3__c != null && no.Units_3__c != null) codeToUnits.put(no.CPT_Code_3__c, no.Units_3__c);
        if(no.CPT_Code_4__c != null && no.Units_4__c != null) codeToUnits.put(no.CPT_Code_4__c, no.Units_4__c);
        if(no.CPT_Code_5__c != null && no.Units_5__c != null) codeToUnits.put(no.CPT_Code_5__c, no.Units_5__c);
        if(no.CPT_Code_6__c != null && no.Units_6__c != null) codeToUnits.put(no.CPT_Code_6__c, no.Units_6__c);
        if(no.CPT_Code_7__c != null && no.Units_7__c != null) codeToUnits.put(no.CPT_Code_7__c, no.Units_7__c);
        if(no.CPT_Code_8__c != null && no.Units_8__c != null) codeToUnits.put(no.CPT_Code_8__c, no.Units_8__c);
        if(no.CPT_Code_9__c != null && no.Units_9__c != null) codeToUnits.put(no.CPT_Code_9__c, no.Units_9__c);
        if(no.CPT_Code_10__c != null && no.Units_10__c != null) codeToUnits.put(no.CPT_Code_10__c, no.Units_10__c);
        codes.addAll(codeToUnits.keySet());
        newOrdersIdToCodeToUnit.put(no.Id, codeToUnits);
    }
    
    
    for(DME__c dme : [
        SELECT 
            Id,
            Name 
        FROM DME__c
        WHERE Name IN :codes
    ]) {
        codeToDME.put(dme.Name, dme);
        dmeCodes.add(dme.Name);
    }
    
     for(Plan_Patient__c planPatient : [
        SELECT
            Id,
            Name,
            Apartment_Number__c,
            Cell_Phone_Number__c,
            City__c,
            Date_of_Birth__c,
            Email_Address__c,
            First_Name__c,
            Health_Plan__c,
            ID_Number__c,
            Last_Name__c,
            Medicaid_ID__c,
            Medicare_ID__c,
            Medicare_Primary_Patient__c,
            Native_Language__c,
            Patient__c,
            Phone_Number__c,
            State__c,
            Street__c,
            Zip_Code__c
        FROM Plan_Patient__c
    ]){
        List<Plan_Patient__c> tempPlanPatientList;
        if(patientIdToPlanPatients.containsKey(planPatient.ID_Number__c)){
            tempPlanPatientList = patientIdToPlanPatients.get(planPatient.ID_Number__c);
        }else{
            tempPlanPatientList = new List<Plan_Patient__c>();  
        }
        tempPlanPatientList.add(planPatient);
        patientIdToPlanPatients.put(planPatient.ID_Number__c, tempPlanPatientList);
    }
    
    Map<Id, Order__c> newOrdersIdToOrder = new Map<Id, Order__c>();
    for(Order__c o : [
        SELECT
            Id,
            Accepted_By__c,
            Accepted_Date__c,
            Accept_Order__c,
            Authorization_Number__c,
            Canceled_Order__c,
            City__c,
            Comments__c,
            Delivery_Date_Time__c,
            Delivery_Instructions__c,
            Delivery_Method__c,
            Delivery_Window_Begin__c,
            Delivery_Window_End__c,
            Entered_By__c,
            Estimated_Delivery_Time__c,
            Legacy_Orders__c,
            Manually_Assigned__c,
            Order_Image__c,
            Original_Order__c,
            Other_Canceled_Orders__c,
            Parent_Provider__c,
            Plan_Patient__c,
            Provider__c,
            Provider_Contact__c,
            Provider_Locations__c,
            Re_Assign_Order__c,
            Recipient_First_Name__c,
            Recipient_Phone_Number__c,
            Recurring_Period__c,
            //Referred_By__c,
            State__c,
            Status__c,
            Status_of_Order__c,
            Street__c,
            Tracking_Number__c,
            Zip__c,
            (SELECT
                DME__c,
                Quantity__c
             FROM DMEs__r)
        FROM Order__c
        WHERE Legacy_Orders__c IN :newOrdersIds
    ]) {
        newOrdersIdToOrder.put(o.Legacy_Orders__c, o);
        orderIds.add(o.Id);
    }
    
    List<DME_Line_Item__c> itemsToUpsert = new List<DME_Line_Item__c>();
    List<Plan_Patient__c> planPatientsToUpsert = new List<Plan_Patient__c>();
    List<Order__c> ordersToUpsert = new List<Order__c>();
    List<DME__c> dmesToInsert = new List<DME__c>();
    
    for(New_Orders__c no : Trigger.new) {
        if(newOrdersIdToOrder.containsKey(no.Id)) {
            Order__c orderToUpdate = newOrdersIdToOrder.get(no.Id);
            orderToUpdate.Accepted_By__c = no.Accepted_By__c;
            orderToUpdate.Accepted_Date__c = no.Accepted_Date__c;
            orderToUpdate.Accept_Order__c = no.Accept_Order__c;
            orderToUpdate.Comments__c = no.Notes__c;
            orderToUpdate.Provider_Contact__c = no.Assigned_Contact__c;
            orderToUpdate.Canceled_Order__c = no.Canceled_Order__c;
            orderToUpdate.City__c = no.City__c;
            orderToUpdate.Recipient_Date_of_Birth__c = no.D_O_B__c;
            orderToUpdate.Delivery_Date_Time__c = (no.Delivery_Date__c == null ? null : DateTime.newInstance(no.Delivery_Date__c,Time.newInstance(0,0,0,0)));
            orderToUpdate.Delivery_Method__c = no.Delivery_Method__c;
            //orderToUpdate.Delivery_Ticket__c = no.Delivery_Ticket__c;
            orderToUpdate.Entered_By__c = no.Entered_By__c;
            orderToUpdate.Estimated_Delivery_Time__c = no.Estimated_Delivery_Time__c;
            orderToUpdate.Legacy_Orders__c = no.Id;
            orderToUpdate.Manually_Assigned__c = no.Manually_Assigned__c;
            orderToUpdate.Order_Image__c = no.Order_Image__c;
            orderToUpdate.Other_Canceled_Orders__c = no.Other_Canceled_Orders__c;
            orderToUpdate.Referred_By_Last_Name__c = no.Person_placing_the_order__c;
            orderToUpdate.Authorization_Number__c  = no.Plan_Order__c;
            orderToUpdate.Parent_Provider__c = no.Provider__c;
            //orderToUpdate.Provider__c = no.Provider_Location__r.Account__c;
            orderToUpdate.Provider__c = newOrdersIdToAccountIdMap.get(no.Id);
            orderToUpdate.Provider_Locations__c = no.Provider_Location__c;
            orderToUpdate.Re_Assign_Order__c = no.Re_Assign_Order__c;
            orderToUpdate.State__c = no.State__c;
            orderToUpdate.Status_of_Order__c = no.Status_of_Order__c;
            orderToUpdate.Street__c = no.Street__c;
            orderToUpdate.Tracking_Number__c = no.Tracking_Number__c;
            orderToUpdate.Zip__c = no.Zip__c;
            
            if(no.Status_of_Order__c == 'Additional Information Needed'){
                orderToUpdate.Status__c = 'Needs Attention - Incomplete Delivery Information';
            }else if(no.Status_of_Order__c == 'Closed/Not Delivered'){
                orderToUpdate.Status__c = 'Needs Attention - Patient Refused - Other';
            }else if(no.Status_of_Order__c == 'Delivered'){
                orderToUpdate.Status__c = 'Delivered';
            }else if(no.Status_of_Order__c == 'In Progress'){
                orderToUpdate.Status__c = 'Provider Accepted';
            }else if(no.Status_of_Order__c == 'Open'){
                orderToUpdate.Status__c = 'New';
            }
            ordersToUpsert.add(orderToUpdate);
            
            if(newOrdersIdToCodeToUnit.containsKey(no.Id)) {
                Map<String, Decimal> tempCodeToUnit = newOrdersIdToCodeToUnit.get(no.Id);
                for(String c : tempCodeToUnit.keySet()) {
                    DME__c tempDME;
                    boolean dmeExists = false;
                    if(codeToDME.containsKey(c)) {
                        tempDME = codeToDME.get(c);
                        dmeExists = true;
                    }else{
                        if(!(dmeCodes.contains(c))){
                            tempDME = new DME__c(
                                Name = c
                            );
                            dmesToInsert.add(tempDME);
                            dmeCodes.add(c);
                        }else{
                            for(DME__c dme : dmesToInsert){
                                if(c == dme.Name){
                                    tempDME = dme;
                                }
                            }
                        }
                        List<DME__c> dmeList;
                        if(newOrdersIdToNewDME.containsKey(no.Id)){
                            dmeList = newOrdersIdToNewDME.get(no.Id);
                        }else{
                            dmeList = new List<DME__c>();
                        }
                        dmeList.add(tempDME);
                        newOrdersIdToNewDME.put(no.Id, dmeList);
                        
                    }
                    if(dmeExists){
                        boolean liExists = false;
                        for(DME_Line_Item__c liTemp : orderToUpdate.DMEs__r) {
                            if(tempDME.Id == liTemp.DME__c) {
                                liExists = true;
                                if(liTemp.Quantity__c != tempCodeToUnit.get(c)) {
                                    liTemp.Quantity__c = tempCodeToUnit.get(c);
                                    itemsToUpsert.add(liTemp);
                                }
                            }
                        }
                        if(!liExists) {
                            DME_Line_Item__c liToInsert = new DME_Line_Item__c(
                                DME__c = tempDME.Id,
                                Order__c = orderToUpdate.Id,
                                Quantity__c = tempCodeToUnit.get(c)
                            );
                            itemsToUpsert.add(liToInsert);
                        }
                    }
                }
            }
            if(no.Member_Patient_ID__c != null && no.Account_c__c != null){
                Boolean pptExists = false;
                if(patientIdToPlanPatients.containsKey(no.Member_Patient_ID__c)){
                    for(Plan_Patient__c ppt : patientIdToPlanPatients.get(no.Member_Patient_ID__c)){
                        if(no.Account_c__c == ppt.Health_Plan__c){
                            /* DO NOT UPDATE A PATIENT FROM AN ORDER
                            List<String> firstLastName = new List<String>();
                            List<String> lastFirstName = new List<String>();
                            String otherName;
                            
                            if(no.Patient_Name__c != null && no.Patient_Name__c.contains(',')){
                                lastFirstName = no.Patient_Name__c.split(',');
                                ppt.Last_Name__c = lastFirstName[0];
                                ppt.First_Name__c = lastFirstName[1];
                                ppt.Name = lastFirstName[1] + ' ' + lastFirstName[0];
                            }else if(no.Patient_Name__c != null && no.Patient_Name__c.contains(' ')){
                                firstLastName = no.Patient_Name__c.split(' ');
                                ppt.First_Name__c = firstLastName[0];
                                ppt.Last_Name__c = firstLastName[1];
                                ppt.Name = firstLastName[0] + ' ' + firstLastName[1];
                            }else if(no.Patient_Name__c != null){
                                ppt.Last_Name__c = no.Patient_Name__c;
                                ppt.Name = no.Patient_Name__c;
                            }
                            if(no.City__c != null) ppt.City__c = no.City__c;
                            if(no.D_O_B__c != null) ppt.Date_of_Birth__c = no.D_O_B__c;
                            if(no.Medicaid_ID__c != null) ppt.Medicaid_ID__c = no.Medicaid_ID__c;
                            if(no.Medicare_ID__c != null) ppt.Medicare_ID__c = no.Medicare_ID__c;
                            if(no.Patient_Phone__c != null) ppt.Phone_Number__c = no.Patient_Phone__c;
                            if(no.State__c != null) ppt.State__c = no.State__c;
                            if(no.Street__c != null) ppt.Street__c = no.Street__c;
                            if(no.Zip__c != null) ppt.Zip_Code__c = no.Zip__c;
                            planPatientsToUpsert.add(ppt);
                            *//*
                            if(orderToUpdate.Plan_Patient__c != ppt.Id){
                                newOrdersIdToPlanPatient.put(no.Id, ppt);
                            }
                            pptExists = true;
                            
                            //Order lookup to patient
                            orderToUpdate.Plan_Patient__c = ppt.Id;
                            
                            //Newer fields holding patient personal data                        
                            orderToUpdate.Patient_Name__c = ppt.Name;
                            orderToUpdate.Patient_First_Name__c = ppt.First_Name__c;
                            orderToUpdate.Patient_Last_Name__c = ppt.Last_Name__c;
                            orderToUpdate.Patient_Phone_Number__c = ppt.Phone_Number__c;
                            orderToUpdate.Patient_Mobile_Phone_Number__c = ppt.Cell_Phone_Number__c;
                            orderToUpdate.Patient_Email_Address__c = ppt.Email_Address__c;
                            orderToUpdate.Patient_Date_of_Birth__c = ppt.Date_of_Birth__c;                                                      
                            orderToUpdate.Patient_ID_Number__c = ppt.ID_Number__c;                          
                            orderToUpdate.Patient_Medicaid_ID__c = ppt.Medicaid_ID__c;
                            orderToUpdate.Patient_Medicare_ID__c = ppt.Medicare_ID__c;                                                      
                            orderToUpdate.Patient_Native_Language__c = ppt.Native_Language__c;                          
                            
                            //Newer fields hodling patient address data
                            //orderToUpdate.Patient_Apartment_Number__c = ;
                            orderToUpdate.Patient_Street__c = no.Street__c;
                            orderToUpdate.Patient_City__c = no.City__c;
                            orderToUpdate.Patient_State__c = no.State__c;
                            orderToUpdate.Patient_Zip_Code__c = no.Zip__c;
                            orderToUpdate.Patient_Country__c = 'United States';
                            
                            //Newer fields holding delivery data
                            orderToUpdate.Recipient_Cell_Phone__c = ppt.Cell_Phone_Number__c;
                            orderToUpdate.Recipient_Country__c = 'United States';
                            orderToUpdate.Recipient_Date_of_Birth__c = ppt.Date_of_Birth__c;
                            orderToUpdate.Recipient_Email__c = ppt.Email_Address__c;
                            orderToUpdate.Recipient_Email_Address__c = ppt.Email_Address__c;
                            orderToUpdate.Recipient_First_Name__c = ppt.First_Name__c;
                            orderToUpdate.Recipient_Last_Name__c = ppt.Last_Name__c;
                            orderToUpdate.Recipient_Native_Language__c = ppt.Native_Language__c;
                            orderToUpdate.Recipient_Phone__c = ppt.Phone_Number__c;
                            orderToUpdate.Recipient_Phone_Number__c = ppt.Phone_Number__c;
                            orderToUpdate.Recipient_Relationship__c = 'Patient';                                                 
                        }
                    }
                }
                if(!pptExists){
                    List<String> firstLastName = new List<String>();
                    List<String> lastFirstName = new List<String>();
                    String otherName;
                    
                    if(no.Patient_Name__c != null && no.Patient_Name__c.contains(',')){
                        lastFirstName = no.Patient_Name__c.split(',');
                    }else if(no.Patient_Name__c != null && no.Patient_Name__c.contains(' ')){
                        firstLastName = no.Patient_Name__c.split(' ');
                    }else if(no.Patient_Name__c != null){
                        otherName = no.Patient_Name__c;
                    }
                    Plan_Patient__c planPatientToInsert = new Plan_Patient__c(
                        City__c = no.City__c,
                        Date_of_Birth__c = no.D_O_B__c,
                        Health_Plan__c = no.Account_c__c,
                        ID_Number__c = no.Member_Patient_ID__c,
                        Medicaid_ID__c = no.Medicaid_ID__c,
                        Medicare_ID__c = no.Medicare_ID__c,
                        Phone_Number__c = no.Patient_Phone__c,
                        State__c = no.State__c,
                        Street__c = no.Street__c,
                        Zip_Code__c = no.Zip__c
                    );
                    if(lastFirstName.size() > 0){
                        planPatientToInsert.Last_Name__c = lastFirstName[0];
                        planPatientToInsert.First_Name__c = lastFirstName[1];
                        planPatientToInsert.Name = lastFirstName[1] + ' ' + lastFirstName[0];
                    }else if(firstLastName.size() > 0){
                        planPatientToInsert.First_Name__c = firstLastName[0];
                        planPatientToInsert.Last_Name__c = firstLastName[1];
                        planPatientToInsert.Name = firstLastName[0] + ' ' + firstLastName[1];
                    }else if(otherName != null && otherName != ''){
                        planPatientToInsert.Last_Name__c = otherName;
                        planPatientToInsert.Name = otherName;
                    }
                    
                    //Newer fields holding patient/delivery data
                    orderToUpdate.Patient_Apartment_Number__c = planPatientToInsert.Apartment_Number__c;
                    orderToUpdate.Patient_City__c = planPatientToInsert.City__c;
                    orderToUpdate.Patient_Country__c = 'United States';
                    orderToUpdate.Patient_Date_of_Birth__c = planPatientToInsert.Date_of_Birth__c;
                    orderToUpdate.Patient_Email_Address__c = planPatientToInsert.Email_Address__c;
                    orderToUpdate.Patient_First_Name__c = planPatientToInsert.First_Name__c;
                    orderToUpdate.Patient_ID_Number__c = planPatientToInsert.ID_Number__c;
                    orderToUpdate.Patient_Last_Name__c = planPatientToInsert.Last_Name__c;
                    orderToUpdate.Patient_Medicaid_ID__c = planPatientToInsert.Medicaid_ID__c;
                    orderToUpdate.Patient_Medicare_ID__c = planPatientToInsert.Medicare_ID__c;
                    orderToUpdate.Patient_Mobile_Phone_Number__c = planPatientToInsert.Cell_Phone_Number__c;
                    orderToUpdate.Patient_Name__c = planPatientToInsert.Name;
                    orderToUpdate.Patient_Native_Language__c = planPatientToInsert.Native_Language__c;
                    orderToUpdate.Patient_Phone_Number__c = planPatientToInsert.Phone_Number__c;
                    orderToUpdate.Patient_State__c = planPatientToInsert.State__c;
                    orderToUpdate.Patient_Street__c = planPatientToInsert.Street__c;
                    orderToUpdate.Patient_Zip_Code__c = planPatientToInsert.Zip_Code__c;
                    
                    //Newer fields holding delivery data
                    orderToUpdate.Recipient_Cell_Phone__c = planPatientToInsert.Cell_Phone_Number__c;
                    orderToUpdate.Recipient_Country__c = 'United States';
                    orderToUpdate.Recipient_Date_of_Birth__c = planPatientToInsert.Date_of_Birth__c;
                    orderToUpdate.Recipient_Email__c = planPatientToInsert.Email_Address__c;
                    orderToUpdate.Recipient_Email_Address__c = planPatientToInsert.Email_Address__c;
                    orderToUpdate.Recipient_First_Name__c = planPatientToInsert.First_Name__c;
                    orderToUpdate.Recipient_Last_Name__c = planPatientToInsert.Last_Name__c;
                    orderToUpdate.Recipient_Native_Language__c = planPatientToInsert.Native_Language__c;
                    orderToUpdate.Recipient_Phone__c = planPatientToInsert.Phone_Number__c;
                    orderToUpdate.Recipient_Phone_Number__c = planPatientToInsert.Phone_Number__c;
                    orderToUpdate.Recipient_Relationship__c = 'Patient';
                    
                    planPatientsToUpsert.add(planPatientToInsert);
                    newOrdersIdToPlanPatient.put(no.Id, planPatientToInsert);
                }
            }else{
                //throw new ApplicationException('Please enter a Member/Patient ID# and an Account');
            }
        } else {
            Order__c orderToInsert = new Order__c(
                Accepted_By__c = no.Accepted_By__c,
                Accepted_Date__c = no.Accepted_Date__c,
                Accept_Order__c = no.Accept_Order__c,
                Comments__c = no.Notes__c,
                Provider_Contact__c = no.Assigned_Contact__c,
                Canceled_Order__c = no.Canceled_Order__c,
                City__c = no.City__c,
                Recipient_Date_of_Birth__c = no.D_O_B__c,
                //Delivery_Date_Time__c = (no.Delivery_Date__c == null ? DateTime.now().addMinutes(-1) : no.Delivery_date__c),
                Delivery_Method__c = no.Delivery_Method__c,
                //Delivery_Ticket__c = no.Delivery_Ticket__c,
                Entered_By__c = no.Entered_By__c,
                Estimated_Delivery_Time__c = no.Estimated_Delivery_Time__c,
                Legacy_Orders__c = no.Id,
                Manually_Assigned__c = no.Manually_Assigned__c,
                Order_Image__c = no.Order_Image__c,
                Other_Canceled_Orders__c = no.Other_Canceled_Orders__c,
                Referred_By_Last_Name__c = no.Person_placing_the_order__c,
                Authorization_Number__c  = no.Plan_Order__c,
                Parent_Provider__c = no.Provider__c,
                Provider__c = newOrdersIdToAccountIdMap.get(no.Id),
                Provider_Locations__c = no.Provider_Location__c,
                Re_Assign_Order__c = no.Re_Assign_Order__c,
                State__c = no.State__c,
                Status_of_Order__c = no.Status_of_Order__c,
                Street__c = no.Street__c,
                Tracking_Number__c = no.Tracking_Number__c,
                Zip__c = no.Zip__c
            );
            
            if(no.Status_of_Order__c == 'Additional Information Needed'){
                orderToInsert.Status__c = 'Needs Attention - Incomplete Delivery Information';
            }else if(no.Status_of_Order__c == 'Closed/Not Delivered'){
                orderToInsert.Status__c = 'Needs Attention - Patient Refused - Other';
            }else if(no.Status_of_Order__c == 'Delivered'){
                orderToInsert.Status__c = 'Delivered';
            }else if(no.Status_of_Order__c == 'In Progress'){
                orderToInsert.Status__c = 'Provider Accepted';
            }else if(no.Status_of_Order__c == 'Open'){
                orderToInsert.Status__c = 'New';
            }
            ordersToUpsert.add(orderToInsert);
            
            if(newOrdersIdToCodeToUnit.containsKey(no.Id)) {
                Map<String, Decimal> tempCodeToUnit = newOrdersIdToCodeToUnit.get(no.Id);
                List<DME_Line_Item__c> tempLineItems = new List<DME_Line_Item__c>();
                for(String c : tempCodeToUnit.keySet()) {
                    DME__c tempDME;
                    boolean dmeExists = false;
                    if(codeToDME.containsKey(c)) {
                        tempDME = codeToDME.get(c);
                        dmeExists = true;
                    }else{
                        List<DME__c> dmeList;
                        if(!(dmeCodes.contains(c))){
                            tempDME = new DME__c(
                                Name = c
                            );
                            dmesToInsert.add(tempDME);
                            dmeCodes.add(c);
                        }else{
                            for(DME__c dme : dmesToInsert){
                                if(c == dme.Name){
                                    tempDME = dme;
                                }
                            }
                        }
                        if(newOrdersIdToNewDME.containsKey(no.Id)){
                            dmeList = newOrdersIdToNewDME.get(no.Id);
                        }else{
                            dmeList = new List<DME__c>();
                        }
                        dmeList.add(tempDME);
                        newOrdersIdToNewDME.put(no.Id, dmeList);
                    }
                    if(dmeExists){
                        DME_Line_Item__c liToInsert = new DME_Line_Item__c(
                            DME__c = tempDME.Id,
                            Quantity__c = tempCodeToUnit.get(c)
                        );
                        tempLineItems.add(liToInsert);
                    }
                }
                newOrdersIdToLineItems.put(no.Id, tempLineItems);
            }
            if(no.Member_Patient_ID__c != null && no.Account_c__c != null){
                Boolean pptExists = false;
                if(patientIdToPlanPatients.containsKey(no.Member_Patient_ID__c)){
                    //reference plan patient
                    for(Plan_Patient__c ppt : patientIdToPlanPatients.get(no.Member_Patient_ID__c)){
                        if(no.Account_c__c == ppt.Health_Plan__c){
                            
                            /* DO NOT UPDATE A PATIENT FROM AN ORDER
                            List<String> firstLastName = new List<String>();
                            List<String> lastFirstName = new List<String>();
                            String otherName;
                            
                            if(no.Patient_Name__c != null && no.Patient_Name__c.contains(',')){
                                lastFirstName = no.Patient_Name__c.split(',');
                                ppt.Last_Name__c = lastFirstName[0];
                                ppt.First_Name__c = lastFirstName[1];
                            }else if(no.Patient_Name__c != null && no.Patient_Name__c.contains(' ')){
                                firstLastName = no.Patient_Name__c.split(' ');
                                ppt.First_Name__c = firstLastName[0];
                                ppt.Last_Name__c = firstLastName[1];
                            }else if(no.Patient_Name__c != null){
                                ppt.Last_Name__c = no.Patient_Name__c;
                            }
                            if(no.City__c != null) ppt.City__c = no.City__c;
                            if(no.D_O_B__c != null) ppt.Date_of_Birth__c = no.D_O_B__c;
                            if(no.Medicaid_ID__c != null) ppt.Medicaid_ID__c = no.Medicaid_ID__c;
                            if(no.Medicare_ID__c != null) ppt.Medicare_ID__c = no.Medicare_ID__c;
                            if(no.Patient_Phone__c != null) ppt.Phone_Number__c = no.Patient_Phone__c;
                            if(no.State__c != null) ppt.State__c = no.State__c;
                            if(no.Street__c != null) ppt.Street__c = no.Street__c;
                            if(no.Zip__c != null) ppt.Zip_Code__c = no.Zip__c;
                            planPatientsToUpsert.add(ppt);
                            newOrdersIdToPlanPatient.put(no.Id, ppt);
                            *//*
                            pptExists = true;
                            
                            //Order lookup to patient
                            orderToInsert.Plan_Patient__c = ppt.Id;
                            
                            //Newer fields holding patient personal data                        
                            orderToInsert.Patient_Name__c = ppt.Name;
                            orderToInsert.Patient_First_Name__c = ppt.First_Name__c;
                            orderToInsert.Patient_Last_Name__c = ppt.Last_Name__c;
                            orderToInsert.Patient_Phone_Number__c = ppt.Phone_Number__c;
                            orderToInsert.Patient_Mobile_Phone_Number__c = ppt.Cell_Phone_Number__c;
                            orderToInsert.Patient_Email_Address__c = ppt.Email_Address__c;
                            orderToInsert.Patient_Date_of_Birth__c = ppt.Date_of_Birth__c;                                                      
                            orderToInsert.Patient_ID_Number__c = ppt.ID_Number__c;                          
                            orderToInsert.Patient_Medicaid_ID__c = ppt.Medicaid_ID__c;
                            orderToInsert.Patient_Medicare_ID__c = ppt.Medicare_ID__c;                                                      
                            orderToInsert.Patient_Native_Language__c = ppt.Native_Language__c;
                            
                            
                            //Newer fields hodling patient address data
                            //orderToInsert.Patient_Apartment_Number__c = ;
                            orderToInsert.Patient_Street__c = no.Street__c;
                            orderToInsert.Patient_City__c = no.City__c;
                            orderToInsert.Patient_State__c = no.State__c;
                            orderToInsert.Patient_Zip_Code__c = no.Zip__c;
                            orderToInsert.Patient_Country__c = 'United States';
                            
                            //Newer fields holding delivery data
                            orderToInsert.Recipient_Cell_Phone__c = ppt.Cell_Phone_Number__c;
                            orderToInsert.Recipient_Country__c = 'United States';
                            orderToInsert.Recipient_Date_of_Birth__c = ppt.Date_of_Birth__c;
                            orderToInsert.Recipient_Email__c = ppt.Email_Address__c;
                            orderToInsert.Recipient_Email_Address__c = ppt.Email_Address__c;
                            orderToInsert.Recipient_First_Name__c = ppt.First_Name__c;
                            orderToInsert.Recipient_Last_Name__c = ppt.Last_Name__c;
                            orderToInsert.Recipient_Native_Language__c = ppt.Native_Language__c;
                            orderToInsert.Recipient_Phone__c = ppt.Phone_Number__c;
                            orderToInsert.Recipient_Phone_Number__c = ppt.Phone_Number__c;
                            orderToInsert.Recipient_Relationship__c = 'Patient';                            
                        }
                    }
                }
                if(!pptExists){
                    List<String> firstLastName = new List<String>();
                    List<String> lastFirstName = new List<String>();
                    String otherName;
                    
                    if(no.Patient_Name__c != null && no.Patient_Name__c.contains(',')){
                        lastFirstName = no.Patient_Name__c.split(',');
                    }else if(no.Patient_Name__c != null && no.Patient_Name__c.contains(' ')){
                        firstLastName = no.Patient_Name__c.split(' ');
                    }else if(no.Patient_Name__c != null){
                        otherName = no.Patient_Name__c;
                    }
                    Plan_Patient__c planPatientToInsert = new Plan_Patient__c(
                        City__c = no.City__c,
                        Date_of_Birth__c = no.D_O_B__c,
                        Health_Plan__c = no.Account_c__c,
                        ID_Number__c = no.Member_Patient_ID__c,
                        Medicaid_ID__c = no.Medicaid_ID__c,
                        Medicare_ID__c = no.Medicare_ID__c,
                        Phone_Number__c = no.Patient_Phone__c,
                        State__c = no.State__c,
                        Street__c = no.Street__c,
                        Zip_Code__c = no.Zip__c
                    );
                    if(lastFirstName.size() > 0){
                        planPatientToInsert.Last_Name__c = lastFirstName[0];
                        planPatientToInsert.First_Name__c = lastFirstName[1];
                        planPatientToInsert.Name = lastFirstName[1] + ' ' + lastFirstName[0];
                    }else if(firstLastName.size() > 0){
                        planPatientToInsert.First_Name__c = firstLastName[0];
                        planPatientToInsert.Last_Name__c = firstLastName[1];
                        planPatientToInsert.Name = firstLastName[0] + ' ' + firstLastName[1];
                    }else if(otherName != null && otherName != ''){
                        planPatientToInsert.Last_Name__c = otherName;
                        planPatientToInsert.Name = otherName;
                    }
                    
                    //Newer fields holding patient/delivery data
                    orderToInsert.Patient_Apartment_Number__c = planPatientToInsert.Apartment_Number__c;
                    orderToInsert.Patient_City__c = planPatientToInsert.City__c;
                    orderToInsert.Patient_Country__c = 'United States';
                    orderToInsert.Patient_Date_of_Birth__c = planPatientToInsert.Date_of_Birth__c;
                    orderToInsert.Patient_Email_Address__c = planPatientToInsert.Email_Address__c;
                    orderToInsert.Patient_First_Name__c = planPatientToInsert.First_Name__c;
                    orderToInsert.Patient_ID_Number__c = planPatientToInsert.ID_Number__c;
                    orderToInsert.Patient_Last_Name__c = planPatientToInsert.Last_Name__c;
                    orderToInsert.Patient_Medicaid_ID__c = planPatientToInsert.Medicaid_ID__c;
                    orderToInsert.Patient_Medicare_ID__c = planPatientToInsert.Medicare_ID__c;
                    orderToInsert.Patient_Mobile_Phone_Number__c = planPatientToInsert.Cell_Phone_Number__c;
                    orderToInsert.Patient_Name__c = planPatientToInsert.Name;
                    orderToInsert.Patient_Native_Language__c = planPatientToInsert.Native_Language__c;
                    orderToInsert.Patient_Phone_Number__c = planPatientToInsert.Phone_Number__c;
                    orderToInsert.Patient_State__c = planPatientToInsert.State__c;
                    orderToInsert.Patient_Street__c = planPatientToInsert.Street__c;
                    orderToInsert.Patient_Zip_Code__c = planPatientToInsert.Zip_Code__c;
                    
                    //Newer fields holding delivery data
                    orderToInsert.Recipient_Cell_Phone__c = planPatientToInsert.Cell_Phone_Number__c;
                    orderToInsert.Recipient_Country__c = 'United States';
                    orderToInsert.Recipient_Date_of_Birth__c = planPatientToInsert.Date_of_Birth__c;
                    orderToInsert.Recipient_Email__c = planPatientToInsert.Email_Address__c;
                    orderToInsert.Recipient_Email_Address__c = planPatientToInsert.Email_Address__c;
                    orderToInsert.Recipient_First_Name__c = planPatientToInsert.First_Name__c;
                    orderToInsert.Recipient_Last_Name__c = planPatientToInsert.Last_Name__c;
                    orderToInsert.Recipient_Native_Language__c = planPatientToInsert.Native_Language__c;
                    orderToInsert.Recipient_Phone__c = planPatientToInsert.Phone_Number__c;
                    orderToInsert.Recipient_Phone_Number__c = planPatientToInsert.Phone_Number__c;
                    orderToInsert.Recipient_Relationship__c = 'Patient';
                    
                    planPatientsToUpsert.add(planPatientToInsert);
                    newOrdersIdToPlanPatient.put(no.Id, planPatientToInsert);
                }
            }else{
                //throw new ApplicationException('Please enter a Member/Patient ID# and an Account');
            }
        }
    }

    Set<Plan_Patient__c> planPatientSet = new Set<Plan_Patient__c>();

    planPatientSet.addAll(planPatientsToUpsert); 

    system.debug(planPatientSet);

    planPatientsToUpsert.clear();

    planPatientsToUpsert.addAll(planPatientSet);

    upsert planPatientsToUpsert;

    for(Order__c o : ordersToUpsert){
        if(newOrdersIdToPlanPatient.containsKey(o.Legacy_Orders__c)){
            o.Plan_Patient__c = newOrdersIdToPlanPatient.get(o.Legacy_Orders__c).Id;
        }
    }

    upsert ordersToUpsert;

    if(dmesToInsert.size()>0){
        insert dmesToInsert;
        for(Id noId : newOrdersIdToNewDME.keySet()){
            List<DME__c> dmeList = newOrdersIdToNewDME.get(noId);
            Map<String, Decimal> codeToUnit = newOrdersIdToCodeToUnit.get(noId);
            for(DME__c dme : dmeList){
                DME_Line_Item__c liToInsert = new DME_Line_Item__c(
                    DME__c = dme.Id,
                    Quantity__c = codeToUnit.get(dme.Name)
                );
                List<DME_Line_Item__c> dmeLiList;
                if(newOrdersIdToLineItems.containsKey(noId)){
                    dmeLiList = newOrdersIdToLineItems.get(noId);
                }else{
                    dmeLiList = new List<DME_Line_Item__c>();
                }
                dmeLiList.add(liToInsert);
                newOrdersIdToLineItems.put(noId, dmeLiList);
            }
        }
    }
    for(Order__c o : ordersToUpsert) {
        if(newOrdersIdToLineItems.containsKey(o.Legacy_Orders__c)) {
            List<DME_Line_Item__c> tempLineItems = newOrdersIdToLineItems.get(o.Legacy_Orders__c);
            for(DME_Line_Item__c dmeLI : tempLineItems) {
                dmeLI.Order__c = o.Id;
                itemsToUpsert.add(dmeLI);
            }
        }
    }
    upsert itemsToUpsert;
    */
}