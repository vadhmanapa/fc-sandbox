trigger CopyConvertedLeadDetails on Lead (before insert, before update) {


	if(Trigger.isBefore && Trigger.isInsert) {

		// update the state to state code

		State__mdt[] stateMetadata = [SELECT MasterLabel,  State_Code_Text__c FROM State__mdt];
		Map<String, String> stateMap = new Map<String, String>();

		for(State__mdt sm: stateMetadata) {
			
			stateMap.put(sm.MasterLabel , sm.State_Code_Text__c);
		}

		if(!stateMap.isEmpty()) {

			for(Lead le: Trigger.new) {
				
				if(le.State__c != null) {
					
					String stateCodeValue =   (String) stateMap.get(le.State__c);
					le.State = stateCodeValue;

				}
			}
				
		}
		
	}

	// after update action

	if(Trigger.isBefore && Trigger.isUpdate){

		CopyLeadDetails_IP.runBeforeUpdate(Trigger.new);
	
	}
}