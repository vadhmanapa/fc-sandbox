trigger DocuSignTrigger on dsfs__DocuSign_Status__c (
	before insert, 
	before update, 
	before delete, 
	after insert, 
	after update, 
	after delete, 
	after undelete) {

		if (Trigger.isAfter) {
	    	//call your handler.before method
	    	if(Trigger.isInsert || Trigger.isUpdate) {
	    		
	    		CreateCATFromDocuSign_IP.createNewCAT(Trigger.new);
	    	}
	    
		} 
}