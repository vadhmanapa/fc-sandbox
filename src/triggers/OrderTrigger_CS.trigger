trigger OrderTrigger_CS on Order__c (before insert, before update, after insert, after update) {
    
	OrderTriggerDispatcher_CS dispatch = new OrderTriggerDispatcher_CS(
		trigger.oldMap,
		trigger.newMap,
		trigger.old,
		trigger.new,
		trigger.isInsert,
		trigger.isUpdate,
		trigger.isDelete,
		trigger.isUndelete,
		trigger.isBefore,
		trigger.isAfter
	);
}