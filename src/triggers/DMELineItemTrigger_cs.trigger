/**
 *  @Description Trigger will callout to the dispatcher for processing.
 *  @Author Cloud Software LLC
 *  Revision History: 
 *		01/12/2016 - karnold - Formatted code. Removed commented code.
 *
 */
trigger DMELineItemTrigger_cs on DME_Line_Item__c(before insert) {

	DMELineItemDispatcher_CS dispatch = new DMELineItemDispatcher_CS(
		trigger.oldMap,
		trigger.newMap,
		trigger.old,
		trigger.new,
		trigger.isInsert,
		trigger.isUpdate,
		trigger.isDelete,
		trigger.isUndelete,
		trigger.isBefore,
		trigger.isAfter
	);
}