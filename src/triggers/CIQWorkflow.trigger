trigger CIQWorkflow on Claims_Inquiry__c (before insert, before update) {
    
    /********************** 
    NOTE: DEPRECATED BY VENKAT ADHMANAPA 
    MOVING THE CODE TO SALESFORCE NATIVE CASE FUNCTIONALITY
  **********************/
  
    /*List <Claims_Inquiry__c> newMailIssue = new List <Claims_Inquiry__c>();
    
    List <User> healthPlanUsers= new List <User>();
    
    //List <AccountTeamMember> ATM = [SELECT userid, accountid from AccountTeamMember where teammemberrole = 'Sales Rep'];
    
    // Add everything from the trigger bulk list to my current bucket
    for(Claims_Inquiry__c ci : Trigger.New){
        
        newMailIssue.add(ci);
    }
    
    // Get all users that are active
    // healthPlanUsers = [SELECT ID, Name, IsActive, CIQ_Last_Assigned__c FROM User WHERE IsActive = True];
    healthPlanUsers = [SELECT ID, Name, IsActive FROM User WHERE IsActive = True];
    
    // Instantiating the map or dictionary key-value pairs wooo
    Map<string,ID> userMap = new Map<string,ID>();
    
    // Populating the dictionary with key-value pairs
    for(User u : healthPlanUsers) {
        userMap.put(u.Name,U.ID);
    }   */
    /*
// For CRS last assigned
Map<Id,List<User>> userCRS = new Map<Id,List<User>>();

// Populating the dictionary with key-value pairs
for(User u : healthPlanUsers) {
userCRS.put(U.ID,u);
}

// For Account team member last assigned
Map<Account,User> HPUser = new Map<Account,User>();

// Populating the dictionary with key-value pairs
for(AccountTeamMember a : ATM) {
userCRS.put(a.accountid,a.userid);
}   */
    
    /*for(Claims_Inquiry__c ci : newMailIssue){
        
        // Attach the created datetime of the original email tothe date time of inquiry
        if(ci.Inquiry_via__c == 'Email' && ci.Email_Log__c != 'a1c300000029iiy'){
            List<Email_Log__c> emailLog = [SELECT ID, CreatedDate FROM Email_Log__c WHERE ID =: ci.Email_log__c];
            if(emailLog.size() >0){
                ci.Date_Time_of_Inquiry__c = emailLog[0].CreatedDate;
            }
        }
        
        if(ci.State__c == Null && (ci.Issue_Summary__c != Null || ci.Reason_for_Inquiry__c != Null)){
			ci.State__c = 'Legacy';
            ci.Internal_Inquiry_Details__c = ci.Issue_Summary__c + '.';
            ci.Escalation_Date_Time__c = ci.CreatedDate;
            if(ci.Inquiry_via__c == 'Email' && ci.Email_Log__c == Null){
                ci.Email_Log__c = 'a1c300000029iiy';
                ci.Date_Time_of_Inquiry__c = ci.CreatedDate;
            }
            
            if(ci.Inquiry_via__c != 'Email'){
                if(ci.Date_Time_of_Inquiry__c == Null){
                    ci.Date_Time_of_Inquiry__c = ci.CreatedDate;
                }
                if(ci.Escalation_Date_Time__c == Null){
                    ci.Escalation_Date_Time__c = ci.CreatedDate;
                }
            }
            
            if(ci.Action_Taken__c == Null){
            	ci.Inquiry_Reason__c = 'Other';
            	ci.Inquiry_Sub_Reason__c = 'Other';
            	ci.Action_Taken__c  = 'Other';
            }
            
            if(ci.Status_of_Inquiry__c == 'Closed'){
                if(ci.Resolution_Steps__c == Null){
                	ci.Resolution_Steps__c = ci.Additional_Escalation_Details__c + '.';
                }
                if(ci.Resolved_Date__c != Null){
                	ci.Resolved_Date_Time__c = datetime.newInstance(ci.Resolved_Date__c, time.newInstance(12,0,0,0));  
                } else {
                    ci.Resolved_Date_Time__c = ci.CreatedDate+1;
                }
                if(ci.Resolved_By__c == Null){
                    ci.Resolved_By__c = '0053000000BaOCU';
                }
                
            }

        }
        // For the create a case button
        if(Trigger.isupdate){
            Claims_Inquiry__c checkPoint = Trigger.oldMap.get(ci.Id);
            Boolean isCaseAlreadyTrue = checkPoint.Case_Created__c;
            if(ci.Case_Created__c == True && isCaseAlreadyTrue == False){
                List<Case> createCase = new List<Case>();
                createCase.add(new Case(Subject =''));
                insert createCase;
                system.debug('case case : ' + createCase[0].Id);
                ci.Related_Case__c = createCase[0].Id;
            } 
        }
    
        if(ci.Action_Taken__c != Null){
            if(ci.Action_Taken__c.equalsIgnoreCase('Adjusted claim to 0') || 
               ci.Action_Taken__c.equalsIgnoreCase('Advised the provider appropriately') || 
               ci.Action_Taken__c.equalsIgnoreCase('Allow for more time for primary to process') || 
               ci.Action_Taken__c.equalsIgnoreCase('Attached EOB to Que') || 
               ci.Action_Taken__c.equalsIgnoreCase('Case pending') || 
               ci.Action_Taken__c.equalsIgnoreCase('Created a case') || 
               ci.Action_Taken__c.equalsIgnoreCase('Created Que case') || 
               ci.Action_Taken__c.equalsIgnoreCase('Deleted claim - entered in error') || 
               ci.Action_Taken__c.equalsIgnoreCase('Explained claim details to provider') || 
               ci.Action_Taken__c.equalsIgnoreCase('Explained payment to the provider') || 
               ci.Action_Taken__c.equalsIgnoreCase('Explained recoupment to the provider') || 
               ci.Action_Taken__c.equalsIgnoreCase('Explained resolution') || 
               ci.Action_Taken__c.equalsIgnoreCase('Explained the denial to the provider') || 
               ci.Action_Taken__c.equalsIgnoreCase('Explained the notes to the provider') || 
               ci.Action_Taken__c.equalsIgnoreCase('Explained to the provider that billing 2nd insurance is not possible') || 
               ci.Action_Taken__c.equalsIgnoreCase('Informed the provider of the credentialing process') || 
               ci.Action_Taken__c.equalsIgnoreCase('Internal transfer') || 
               ci.Action_Taken__c.equalsIgnoreCase('Made the necessary corrections and resubmitted the claim by paper mail') || 
               ci.Action_Taken__c.equalsIgnoreCase('Made the necessary corrections and resubmitted the claim electronically') ||
               ci.Action_Taken__c.equalsIgnoreCase('Needs a 1st level appeal') ||
               ci.Action_Taken__c.equalsIgnoreCase('Needs more processing time') || 
               ci.Action_Taken__c.equalsIgnoreCase('Needs more time as recoupment is still pending') || 
               ci.Action_Taken__c.equalsIgnoreCase('No response necessary') ||  
               ci.Action_Taken__c.equalsIgnoreCase('Provided check # & date') || 
               ci.Action_Taken__c.equalsIgnoreCase('Provided necessary information') || 
               ci.Action_Taken__c.equalsIgnoreCase('Removed from NAQ') || 
               ci.Action_Taken__c.equalsIgnoreCase('Re-opened inquiry for further investigation') || 
               ci.Action_Taken__c.equalsIgnoreCase('Requested documentation from provider') || 
               ci.Action_Taken__c.equalsIgnoreCase('Requested the EOB from the provider') || 
               ci.Action_Taken__c.equalsIgnoreCase('Requested the provider to rebill the primary insurance with new code') || 
               ci.Action_Taken__c.equalsIgnoreCase('Requested the provider to update/verify information') || 
               ci.Action_Taken__c.equalsIgnoreCase('Submitted adjustment - no further action possible') || 
               ci.Action_Taken__c.equalsIgnoreCase('Submitted corrected claim by paper mail') || 
               ci.Action_Taken__c.equalsIgnoreCase('Submitted corrected claim electronically') || 
               ci.Action_Taken__c.equalsIgnoreCase('Submitted recoupment request') || 
               ci.Action_Taken__c.equalsIgnoreCase('Submitted to 2nd insurance') || 
               ci.Action_Taken__c.equalsIgnoreCase('Submitted to 3rd insurance') || 
               ci.Action_Taken__c.equalsIgnoreCase('Updated claim information and paper mailed') ||  
               ci.Action_Taken__c.equalsIgnoreCase('Updated claim information and submitted electronically')){
                   ci.State__c = 'Closed';
               } else if(ci.Action_Taken__c.equalsIgnoreCase('Needs an expert as claims on EOP posted with $0') || 
                         ci.Action_Taken__c.equalsIgnoreCase('Needs an expert as the payment from Integra does not match the provider\'s payment') || 
                         ci.Action_Taken__c.equalsIgnoreCase('Needs an expert to locate recoupment status')){
                             ci.State__c = 'Escalated to Accounting';
                         } else if(ci.Action_Taken__c.equalsIgnoreCase('Needs a 2nd level appeal') || 
                                   ci.Action_Taken__c.equalsIgnoreCase('Needs a copy of the appeal') || 
                                   ci.Action_Taken__c.equalsIgnoreCase('Needs an outbound call to the payor for claim status') || 
                                   ci.Action_Taken__c.equalsIgnoreCase('Needs an outbound call to the payor for recoupment reason') || 
                                   ci.Action_Taken__c.equalsIgnoreCase('Needs an outbound call to the payor to obtain EOB')){
                                       ci.State__c = 'Escalated to Claims Resolution Specialist';
                                   } else if(ci.Action_Taken__c.equalsIgnoreCase('Needs an expert opinion')){
                                       ci.State__c = 'Closed';
                                   } else if(ci.Action_Taken__c.equalsIgnoreCase('Explained the issue')){
                                       ci.State__c = 'Closed';
                                   } else if(ci.Action_Taken__c.equalsIgnoreCase('Needs an expert as payment posted in Dr.com but not in Que') || 
                                             ci.Action_Taken__c.equalsIgnoreCase('Needs an expert as the plan has paid but payment is not matching in Que') ||
                                             ci.Action_Taken__c.equalsIgnoreCase('Needs an outbound call to trace the check as the plan has paid but payment has not posted in Que')){
                                                 ci.State__c = 'Escalated to Payment & Denial Resolution';
                                             }
        }
        
        if(ci.State__c != NULL){
            if(ci.State__c == 'Closed' && ci.Status_of_Inquiry__c == 'Open'){
                ci.Status_of_Inquiry__c = 'Closed';
                if(ci.Resolved_By__c == Null){
                    ci.Resolved_By__c = UserInfo.getUserId();
                }
                if(ci.Resolved_Date_Time__c == NULL){
                    ci.Resolved_Date_Time__c = system.now();
                }
                ci.Assigned_To__c = UserInfo.getUserId();
            }         
            
            // If statement to help assign the CIQ to the right people for first time
            if(ci.State__c == 'Escalated to Claims Resolution Specialist' && ci.Escalation_Date_Time__C == NULL && ci.Health_Plan_Name__c != NULL){
                ci.Assigned_To__c = userMap.get('Evie Contaldo');
                //ci.Escalation_Date_Time__C = system.now();
                //double rand = system.math.random();
                
                //List <AccountTeamMember> ATM = [SELECT userid, accountid,teammemberrole from AccountTeamMember where accountid =: ci.Health_Plan__c AND teammemberrole = 'Sales Rep'];
                //system.debug('account team member : ' + ATM[0]);
                /*
				datetime tempDT = system.now();
                User tempUser = NUll;
                
                for(AccountTeamMember i: ATM ){
                if(userCRS.containsKey(i.userid)){
                if(userCRS.get(i.userid).CIQ_Last_Assigned__c < tempDT){
                tempDT = userCRS.get(i.userid).CIQ_Last_Assigned__c;
                tempUser = userCRS.get(i.userid);
                }
                }
                }*/
                
                // Assign the CIQ to the appropriate rep
                //if(ci.Health_Plan_Name__c == 'CenterLight Healthcare'){
                //    if (rand > 0.5){
                //        ci.Assigned_To__c = userMap.get('Jen Lewis');
                //    } else {
                //        ci.Assigned_To__c = userMap.get('Michelle Scott');
                //    } 
                //} else if (ci.Health_Plan_Name__c == 'Empire BlueCross BlueShield HealthPlus'){
                //    if (rand > 0.66){
                //        ci.Assigned_To__c = userMap.get('Tanya Lindsey');
                //    } else if(rand < 0.33) {
                //        ci.Assigned_To__c = userMap.get('Jose Torres');
                //    } else{
                //        ci.Assigned_To__c = userMap.get('Raul Blackwell');
                //    }
                //} else if (ci.Health_Plan_Name__c.containsIgnoreCase('Wellcare')){
                //    ci.Assigned_To__c = userMap.get('Natasha Walcott');
                //} else if (ci.Health_Plan_Name__c == 'Fidelis Care New York'){
                //    ci.Assigned_To__c = userMap.get('Evie Contaldo');
                //} else if (ci.Health_Plan_Name__c == 'Affinity'){
                //    ci.Assigned_To__c = userMap.get('Evie Contaldo');
                //}
                //else {
                //    ci.Assigned_To__c = userMap.get('Evie Contaldo');
                //}*/
                
           /*     
            } else if (ci.State__c == 'Escalated to Accounting' && ci.Escalation_Date_Time__C == NULL && ci.Action_Taken__c == 'Needs an expert to locate recoupment status'){
                ci.Escalation_Date_Time__C = system.now();
                ci.Assigned_To__c = userMap.get('Stacey Rolon');
            } else if (ci.State__c == 'Escalated to Accounting' && ci.Escalation_Date_Time__C == NULL && ci.Action_Taken__c != 'Needs an expert to locate recoupment status'){
                ci.Escalation_Date_Time__C = system.now();
                ci.Assigned_To__c = userMap.get('Stacey Rolon');
            } else if (ci.State__c == 'Escalated to Network Development' && ci.Escalation_Date_Time__C == NULL){
                ci.Escalation_Date_Time__C = system.now();
                List<Account> accountList = [SELECT ID, NAME, OwnerID FROM ACCOUNT WHERE ID =: ci.Provider__c];
                system.debug('ownerID = ' + accountList[0].Name);
                ci.Assigned_To__c = accountList[0].OwnerID;
            } else if (ci.State__c == 'Escalated to MCS' && ci.Escalation_Date_Time__C == NULL){
                ci.Escalation_Date_Time__C = system.now();
                ci.Assigned_To__c = userMap.get('Carole Sayegh');  
            } else if (ci.State__c == 'Escalated to Payment & Denial Resolution' && ci.Escalation_Date_Time__C == NULL){
                ci.Escalation_Date_Time__C = system.now();
                ci.Assigned_To__c = userMap.get('Jerone Kadnar');  
            }     
            
        }
        else {
            if (ci.Assigned_To__c == NUll){
            	ci.Assigned_To__c = UserInfo.getUserId();
            }
            if(ci.Inquiry_Reason__c ==  'Transfer to CS' || ci.Inquiry_Reason__c ==  'Hang up'){
                ci.Status_of_Inquiry__c = 'Closed';
                ci.state__c = 'Closed';
                ci.Resolved_By__c = UserInfo.getUserId();
                ci.Resolved_Date_Time__c = system.now();
                ci.Inquiry_via__c = 'Call';
            }
        }
        

        if(ci.Status_of_Inquiry__c == 'Closed' && ci.Resolved_By__c == Null && ci.Resolved_Date_Time__c == Null){
            ci.Resolved_Date_Time__c = system.now();
            ci.Resolved_By__c = UserInfo.getUserId();
        }
        
        if(ci.Action_Taken__c == 'Other' && ci.Assigned_To__c != UserInfo.getUserId() && ci.Escalation_Date_Time__c == NULL && ci.Resolved_By__c == NULL){
            ci.Escalation_Date_Time__C = system.now();
        }
    }*/
}