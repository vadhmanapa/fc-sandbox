trigger TouchesForClaims_IP on ARAG__c (after update) {
    
    List<Claim_Work_Touch__c> toInsert = new List<Claim_Work_Touch__c>();
    
    //Stores the gathered Claims Work Touches before processing
    Map<Id,List<Claim_Work_Touch__c>> claimIdToTouches = new Map<Id,List<Claim_Work_Touch__c>>();
    
    //NOTE TO VENKAT: SOQL should always be outside of 'for' loops
    //Gather old touches
    for(Claim_Work_Touch__c touch : [
        SELECT Id, Name, Claims_Item__c, From_Status__c, To_Status__c
        FROM Claim_Work_Touch__c
        WHERE Claims_Item__c IN :Trigger.newMap.keySet()
    ]){
        //Check if touch has already been added to map
        if(claimIdToTouches.containsKey(touch.Claims_Item__c)) {
            claimIdToTouches.get(touch.Claims_Item__c).add(touch);
        } else {
            claimIdToTouches.put(touch.Claims_Item__c,new List<Claim_Work_Touch__c>{touch});
        }
    }
        
    //**MAIN FOR LOOP: Processing claims**
    for(ARAG__c newClaim : Trigger.new) {

        system.debug(LoggingLevel.INFO,'Checking claim ' + newClaim.Name);
        
        ARAG__c oldClaim = Trigger.oldMap.get(newClaim.Id);
        
        List<Claim_Work_Touch__c> existingTouches = (claimIdToTouches.containsKey(newClaim.Id) ? 
            claimIdToTouches.get(newClaim.Id) : new List<Claim_Work_Touch__c>());       
        
        String fromStatus = oldClaim.Status__c;
        String toStatus = newClaim.Status__c;
        
        //Check for status change
        if(fromStatus == toStatus) {
            system.debug(LoggingLevel.INFO,'Found sames status ' + newClaim.Status__c);
            continue;
        }
        system.debug(LoggingLevel.INFO,'Found status change: ' + fromStatus + ' to ' + toStatus);
        
        //Check for existing touch
        Boolean err = false;

        for(Claim_Work_Touch__c touch :  existingTouches) {
            if(touch.From_Status__c == fromStatus && touch.To_Status__c == toStatus) {
                System.debug(LoggingLevel.INFO,'Found existing touch: ' + touch.Name);
                err = true;
            }
        }
        
        if(err){
            continue;
        }
        
        //Create new touch
        Claim_Work_Touch__c newTouch = new Claim_Work_Touch__c(
            Claims_Item__c = newClaim.Id, 
            From_Status__c = fromStatus, 
            To_Status__c = toStatus,
            Touched_By__c = newClaim.LastModifiedById, 
            Touched_On__c = System.now() 
        );
        
        toInsert.add(newTouch);
    }
    
    //NOTE TO VENKAT: Inserts should always be outside of 'for' loops
    //Insert new touches
    try {
        insert toInsert;
        System.debug(LoggingLevel.INFO,'Number of records to be attached:  ' + toInsert.size());
    }catch(System.Dmlexception e) {
        System.debug(LoggingLevel.ERROR,'Touch History not inserted. Job unsuccessful: ' + e.getMessage());
    }
}

/*
    //Venkat's Implementation - Prior to 1.13.15

    //This trigger tracks the the number of touches happened on a Claim Work Item after it was created. It tracks the status changes and stamps them on Claim Touch object
    for (ARAG__c claim : Trigger.new){
      List<Claim_Work_Touch__c> newRecord = new List <Claim_Work_Touch__c>();
      // ARAG__c toConvert = Trigger.oldMap.get(claim.Id);
      String fromStatus = Trigger.oldMap.get(claim.Id).Status__c;
      String toStatus = claim.Status__c;
        System.debug('Before Changing');
        System.debug('The from status is' +fromStatus);
        System.debug('The to Status is' +toStatus);
      
        // check whether the status was changed
        List<Claim_Work_Touch__c> oldTouch = [Select Id, Name,Claims_Item__c, From_Status__c, To_Status__c from Claim_Work_Touch__c where Claims_Item__c =:claim.Id and
                                             From_Status__c =:fromStatus and To_Status__c =:toStatus LIMIT 1];
        if (oldTouch.size() == 0){
            if (fromStatus != toStatus){
           //then the status has been changed. Record the change in Claim Work Touch
           Claim_Work_Touch__c newTouch = new Claim_Work_Touch__c(Claims_Item__c = claim.Id, From_Status__c = fromStatus, To_Status__c = toStatus,
                                                                  Touched_By__c = claim.LastModifiedById, Touched_On__c = System.now() );
              newRecord.add (newTouch);  
        }
        try {
            insert newRecord;
            System.debug('Number of records to be attached'+newRecord.size());
        }catch(System.Dmlexception e) {
                                    System.debug('Touch History not inserted. Job unsuccessful ' + e.getMessage());
                            }
        }
        
    }
*/