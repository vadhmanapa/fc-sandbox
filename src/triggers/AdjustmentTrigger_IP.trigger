trigger AdjustmentTrigger_IP on Adjustments__c (before insert, before update, after delete) {

	if(Trigger.isBefore){
		if(Trigger.isInsert || Trigger.isUpdate){
			AdjustmentRequestAgent_IP callAgent = new AdjustmentRequestAgent_IP();
			callAgent.runOnInsert(Trigger.new);
		}
	}

	if(Trigger.isAfter){
		if(Trigger.isDelete){
			AdjustmentRequestAgent_IP.fnDelete(Trigger.old);
		}
	}
}