trigger CreateOrderTrigger_CS_Dev on New_Orders__c (before insert, before update, after insert, after update) {

    //System.debug('Entering CreateOrderTriggerDispatcher');
    
    CreateOrderTriggerDispatcher_CS dispatch = new CreateOrderTriggerDispatcher_CS(
        trigger.oldMap,
        trigger.newMap,
        trigger.old,
        trigger.new,
        trigger.isInsert,
        trigger.isUpdate,
        trigger.isDelete,
        trigger.isUndelete,
        trigger.isBefore,
        trigger.isAfter
    );
    
    //System.debug('Exiting CreateOrderTriggerDispatcher');
}