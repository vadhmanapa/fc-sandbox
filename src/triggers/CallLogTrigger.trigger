trigger CallLogTrigger on Call_Log__c (before insert, before update) {
	
	if(Trigger.isBefore) {
		if(Trigger.isInsert) {
			CallLogTriggerDispatcher.beforeInsert(Trigger.new);
		} else if(Trigger.isUpdate) {
			CallLogTriggerDispatcher.beforeUpdate(Trigger.new);
		}
	}
	
}