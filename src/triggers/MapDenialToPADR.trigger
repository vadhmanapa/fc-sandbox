trigger MapDenialToPADR on Denials__c (before update) {
    /* This trigger is to map Denials to PADR. If the bill number is not found in PADR, then create a new record. If the bill number exists in PADR, then map it to that record. 
    List<PADR__c> toMap = new List<PADR__c>();
    List<PADR__c> query = new List <PADR__c>();
    List<PADR__c> toInsert = new List<PADR__c>();
    String recordId;
    for(Denials__c deny : Trigger.new){
       if (deny.Work_Denial__c == true){
           query = [Select Id, Bill_Number__c, Procedure_Code_s__c, CreatedDate from PADR__c where Bill_Number__c =: deny.Bill__c ORDER BY  CreatedDate DESC];
           if (query.size() > 0){
               System.debug('Entering mapping trigger');
               query[0].Procedure_Code_s__c = query[0].Procedure_Code_s__c + ','+deny.Line_Item__c;
               update query;
               deny.Related_PADR__c = query[0].Id;             
               System.debug ('The Denial is mapped to PADR');           
           } else {
                   System.debug ('Entering insert trigger');
                   String providerName = deny.Provider__c;
                       PADR__c newDeny = new PADR__c(PADR_Type__c = 'Denial', Bill_Number__c = deny.Bill__c, Provider_Name_Text__c = deny.Provider__c, Procedure_Code_s__c = deny.Line_Item__c,
                                                                                         Claim__c = deny.Claim_Number__c, Payor_Name__c = deny.Payor__c );
                        toInsert.add (newDeny);
                        try {
                          Database.SaveResult[] srMapList = Database.insert(toInsert, false);
                            for (Database.SaveResult dsr : srMapList) {
                                if (dsr.isSuccess()) {
                                // Operation was successful, so get the ID of the record that was processed
                                System.debug('Successfully inserted PADR. PADR Record ID: ' + dsr.getId());
                                // isSuccess = true;
                                recordId = dsr.getId();
                            }else {
                                // Operation failed, so get all errors                
                                for(Database.Error err : dsr.getErrors()) {
                                    System.debug('The following error has occurred.');                    
                                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                                    System.debug('PADR__c fields that affected this error: ' + err.getFields());
                                }
                            }
                            }
                        }catch (System.Dmlexception e) {
                                    System.debug('PADR not inserted. Job unsuccessful ' + e.getMessage());
                            }
                        if (recordId != null){
                                    deny.Related_PADR__c  = recordId;
                                    System.debug('The Denials is mapped to PADR');
                                }else {
                                    System.debug('Some issue happened');
                                }
           }
       }
    }*/
}