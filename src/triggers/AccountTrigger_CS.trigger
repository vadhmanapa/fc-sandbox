trigger AccountTrigger_CS on Account (after insert, after update) {
    AccountTriggerDispatcher_CS dispatch = new AccountTriggerDispatcher_CS(
        trigger.oldMap,
        trigger.newMap,
        trigger.old,
        trigger.new,
        trigger.isInsert,
        trigger.isUpdate,
        trigger.isDelete,
        trigger.isUndelete,
        trigger.isBefore,
        trigger.isAfter
    );
}