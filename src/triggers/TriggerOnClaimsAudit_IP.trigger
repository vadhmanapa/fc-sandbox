trigger TriggerOnClaimsAudit_IP on Claim_Work_Audit__c (before insert, before update) {
	
    if(Trigger.isBefore){
        ClaimsAuditController_IP.calculateAuditScore(Trigger.new);
    }
}