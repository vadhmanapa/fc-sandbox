trigger CaseActionTrigger_IP on Case_Action__c (
	before insert, 
	before update, 
	before delete, 
	after insert, 
	after update, 
	after delete, 
	after undelete) {

	CaseActionTriggerHandler_IP handler = new CaseActionTriggerHandler_IP(Trigger.isExecuting, Trigger.size);

		if(Trigger.isInsert && Trigger.isBefore){
		// handler.OnBeforeInsert(Trigger.new);
		}
		else if(Trigger.isInsert && Trigger.isAfter){
			handler.OnAfterInsert(Trigger.new);
			// CaseActionTriggerHandler_IP.OnAfterInsertAsync(Trigger.newMap.keySet());
		}else {
			
		}
		
		/*else if(Trigger.isUpdate && Trigger.isBefore){
			handler.OnBeforeUpdate(Trigger.old, Trigger.new, Trigger.newMap);
		}
		else if(Trigger.isUpdate && Trigger.isAfter){
			handler.OnAfterUpdate(Trigger.old, Trigger.new, Trigger.newMap);
			CaseActionTriggerHandler_IP.OnAfterUpdateAsync(Trigger.newMap.keySet());
		}
		
		else if(Trigger.isDelete && Trigger.isBefore){
			handler.OnBeforeDelete(Trigger.old, Trigger.oldMap);
		}
		else if(Trigger.isDelete && Trigger.isAfter){
			handler.OnAfterDelete(Trigger.old, Trigger.oldMap);
			CaseActionTriggerHandler_IP.OnAfterDeleteAsync(Trigger.oldMap.keySet());
		}
		
		else if(Trigger.isUnDelete){
			handler.OnUndelete(Trigger.new);
	}*/
}