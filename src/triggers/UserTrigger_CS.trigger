trigger UserTrigger_CS on Contact (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
	UserTriggerDispatcher_CS dispatch = new UserTriggerDispatcher_CS(
		trigger.oldMap,
		trigger.newMap,
		trigger.old,
		trigger.new,
		trigger.isInsert,
		trigger.isUpdate,
		trigger.isDelete,
		trigger.isUndelete,
		trigger.isBefore,
		trigger.isAfter
	);

	if(trigger.isBefore && trigger.isUpdate){

		GeneratePasswordQue_IP quePasswordGen = new GeneratePasswordQue_IP();
		quePasswordGen.runPasswordGenerator(Trigger.new);
	}
}