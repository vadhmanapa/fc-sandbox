trigger PlanPatientTrigger_CS on Plan_Patient__c (before insert, before update) {
    system.debug('Entering Plan Patient Dispatcher');
    PlanPatientTriggerDispatcher_CS dispatch = new PlanPatientTriggerDispatcher_CS(
        trigger.oldMap,
        trigger.newMap,
        trigger.old,
        trigger.new,
        trigger.isInsert,
        trigger.isUpdate,
        trigger.isDelete,
        trigger.isUnDelete,
        trigger.isBefore,
        trigger.isAfter
    );
    system.debug('Exiting Plan Patient Dispatcher');
}