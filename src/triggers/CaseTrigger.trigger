trigger CaseTrigger on Case (before insert, before update, after insert, after update) {

    CaseTriggerDispatcher handler = new CaseTriggerDispatcher(Trigger.isExecuting, Trigger.size);

    if(Trigger.isInsert && Trigger.isBefore){
        handler.OnBeforeInsert(Trigger.new);
    }else {
        
    }
    /*else if(Trigger.isInsert && Trigger.isAfter){
        handler.OnAfterInsert(Trigger.new, Trigger.newMap);
        //{{ trigger_handler_name }}.OnAfterInsertAsync(Trigger.newMap.keySet());
    }

    else if(Trigger.isUpdate && Trigger.isBefore){
        handler.OnBeforeUpdate(Trigger.old, Trigger.new, Trigger.newMap);
    }
    else if(Trigger.isUpdate && Trigger.isAfter){
        handler.OnAfterUpdate(Trigger.old, Trigger.new, Trigger.newMap);
        //{{ trigger_handler_name }}.OnAfterUpdateAsync(Trigger.newMap.keySet());
    }

    else if(Trigger.isDelete && Trigger.isBefore){
        handler.OnBeforeDelete(Trigger.old, Trigger.oldMap);
    }
    else if(Trigger.isDelete && Trigger.isAfter){
        handler.OnAfterDelete(Trigger.old, Trigger.oldMap);
        //{{ trigger_handler_name }}.OnAfterDeleteAsync(Trigger.oldMap.keySet());
    }

    else if(Trigger.isUnDelete){
        handler.OnUndelete(Trigger.new);
    }*/

    CaseTriggerDispatcher.runCount++;



     /********************
        Author: Cloud Developer
        Deprecated On: 06/22/2017
        Deprecated By: Venkat Adhmanaapa
    *********************/
	/*if(Trigger.isBefore) {
		if (Trigger.isInsert) {
			// CaseTriggerDispatcher.beforeInsert(Trigger.new); 
			// TrackCaseOwnership_IP.runOnInsert(Trigger.new);
			Map<Id, RecordType> mapRecordTypes = new Map<Id, RecordType>();
        	// Case[] ownerChange = new Case[0];
        	for(RecordType rt : [Select Id, Name from RecordType where sObjectType = 'Case']){
           	 mapRecordTypes.put(rt.Id, rt);
        	}
        
        
        	for(Case c : Trigger.New){
                System.debug('The record type is'+mapRecordTypes.get(c.RecordTypeId).Name);
            	if(mapRecordTypes.get(c.RecordTypeId).Name == 'Standard'){
                // ownerChange.add(c);
                //Database.DMLOptions opt = new Database.DMLOptions();
        		//opt.AssignmentRuleHeader.useDefaultRule = false;
                c.OwnerId = UserInfo.getUserId();
                //c.setOptions(opt);
            	}
        	}
        } 

        if (Trigger.isUpdate) {
             CaseTriggerDispatcher.beforeUpdate(Trigger.new,Trigger.oldMap);
            /*TrackCaseOwnership_IP.runOnUpdate(Trigger.new, Trigger.oldMap); // added by Venkat to track case ownership on 05/21/2015
            TrackCaseOwnership_IP.triggerRun++;*/
    /*    }
	} 

    CaseTriggerDispatcher.runCount++;


	/*else if(Trigger.isAfter) {
		if (Trigger.isInsert) {
			CaseTriggerDispatcher.afterInsert(Trigger.new);
		} else if (Trigger.isUpdate) {
			CaseTriggerDispatcher.afterUpdate(Trigger.new);
		}
	}*/

    // CaseTriggerDispatcher.runCount++;
     // added by Venkat on 05/21/2015
    
    //--***************** Modified by Venkat on 05/28/2015 ---> Turning on Assignment Rules back ****************************************
}