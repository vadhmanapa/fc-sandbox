trigger IssueLog on Issue_Log__c (before insert, before update) {
    
    List <Issue_Log__c> newIssueLog = new List <Issue_Log__c>();
    
    for (Issue_Log__c il : Trigger.new){
        newIssueLog.add(il);
    }
    
    for(Issue_Log__c il : newIssueLog){
        if(Trigger.isupdate){
            if(il.touch_counter__c != NULL){
            	il.touch_counter__c += 1;
            }
            else{
                il.touch_counter__c = 1;
            }
        }
    } 
}