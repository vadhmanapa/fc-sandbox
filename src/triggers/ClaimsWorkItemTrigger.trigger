trigger ClaimsWorkItemTrigger on ARAG__c (after update) {

	ClaimsWorkItemTriggerDispatcher.runCount++;
	system.debug(LoggingLevel.INFO,'Trigger runCount: ' + ClaimsWorkItemTriggerDispatcher.runCount);

	if(Trigger.isAfter) {
		if(Trigger.isUpdate) {
			ClaimsWorkItemTriggerDispatcher.afterUpdate(Trigger.new,Trigger.oldMap);
		}
	}
}