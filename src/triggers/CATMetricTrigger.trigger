trigger CATMetricTrigger on CAT_Metric__c (before insert, before update, after insert, after update) {

	CATMetricHandler newRun = new CATMetricHandler();

	if(Trigger.isBefore) {
		
		if(Trigger.isUpdate) {
			
			newRun.runTrigger(Trigger.new, Trigger.oldMap);
		}
	}
}