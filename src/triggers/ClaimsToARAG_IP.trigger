trigger ClaimsToARAG_IP on Claims_Inquiry__c (before insert, before update) {
	/********** This trigger is created in order to facilitate one click ARAG creation. The Claims Inquiry object has a checkbox Create ARAG, which when checked
	should create a new ARAG record*********/
	
    if (Trigger.isUpdate || Trigger.isInsert){
        
        ClaimsInquiryToCWI_IP runOnUpdate = new ClaimsInquiryToCWI_IP();
        runOnUpdate.runTrigger(Trigger.New);
    }
}