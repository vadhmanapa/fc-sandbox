trigger ClearingHouseToDeposits on Clearinghouse__c (before insert) {
	if(Trigger.isBefore){
        if (Trigger.isInsert){

            Set<Double> paidAmount = new Set<Double>();
            Map<Double, Check_Deposit__c> keySet = new Map<Double, Check_Deposit__c>();
            Map<Id, Check_Deposit__c> newMap = new Map<Id, Check_Deposit__c>();
            Map<Id, Check_Deposit__c> dupeMap = new Map<Id, Check_Deposit__c>();
            List<String> planName = new List<String>();
            Integer numMatch = 0;
            Integer runNum = 0;
            for (Clearinghouse__c c : Trigger.New){
                // get all the amount values
                paidAmount.add(c.Amount__c);
            }
            
            // query based on the amount from Clearinghouse
            List <Check_Deposit__c> claimFile = [Select Id,
                                                 Name,
                                                 Paid_Amount__c,
                                                 Remitter_Name__c, Status__c, Processing_Date__c from Check_Deposit__c 
                                                 where Paid_Amount__c IN: paidAmount and Status__c != 'Claim File with matching deposit'];
            
            // get all the short names from info
            List<Payor_Routing_Info__c> info = [Select Routing_Number__c, Short_Name__c from Payor_Routing_Info__c];
            
            //convert to a map
            Map <String,Payor_Routing_Info__c > routingInfo = new Map<String, Payor_Routing_Info__c>();
            for (Payor_Routing_Info__c pr : info){
                routingInfo.put(pr.Routing_Number__c, pr);
            }
            
            //  convert the clearinghouse query into a map
            Map<Double,Check_Deposit__c> matchFiles = new Map<Double, Check_Deposit__c>();
            for (Check_Deposit__c cl : claimFile){
                matchFiles.put(cl.Paid_Amount__c, cl);
                newMap.put(cl.Id, cl);
            }
            
       
            if (matchFiles.size() > 0){
                system.debug('size of map'+matchFiles.size());
                
                // start running on induvidual record
                for(Clearinghouse__c ch : Trigger.New){
                    
                runNum++;
                System.debug('Run Number'+runNum);
                System.debug('Amount'+ch.Amount__c);
                System.debug('Payer' +ch.Payer__c);
                Integer i = 0;
                String allIds = '';
                system.debug('Entering amount check');
                if(ch.Payer__c != null && ch.Amount__c != null){
                    dupeMap.clear();
                    
                // check if there is a unique match or multiple match                    
                    if (matchFiles.get(ch.Amount__c) != null){
                        
                        for (Check_Deposit__c cr : newmap.values()){
                            if (ch.Amount__c == newMap.get(cr.Id).Paid_Amount__c){
                                dupeMap.put(cr.Id, cr);
                            }
                        }
                        System.debug('Size of dupeMap'+ dupeMap.size());
                         keyset.clear();
                         keyset.put(matchFiles.get(ch.Amount__c).Paid_Amount__c,matchFiles.get(ch.Amount__c));
                         Integer repPayer = 0;
                         system.debug('Size of keyset'+keyset.size());
                        
                        // if value greater than one, then more claims file matched
                        if (dupeMap.size() > 1){
                            // date comparison on both
                            /*for(Id newId : dupeMap.keySet()){
                                if((newMap.get(newId).Payer__c).contains(ch.Remitter_Name__c)){
                                    repPayer++;
                                }
                                System.debug('Number of payer name matches'+repPayer);
                            }*/
                                    ch.Status__c = 'More than one possible match';
                                    for (Id newId : dupeMap.keySet()){
                                        if (allIds == ''){
                                            allIds = dupeMap.get(newId).Name;
                                        }else {
                                            allIds = allIds + dupeMap.get(newId).Name;
                                        }
                                    }
                                    ch.Possible_Matches__c = allIds;
                                                                   
                                    update newMap.values();
                                                   
                        } else {
                            // if the payment is unique
                            
                            if(ch.Amount__c == matchFiles.get(ch.Amount__c).Paid_Amount__c){
                                system.debug('Entering payor check');
                                String str = ch.Payer__c;
                                if (str != null){
                                    String[] splitString = str.split('\\s');
                                
                                for(String s : splitString){
                                    System.debug('String is' +s);
                                    if (s!= null && s.containsIgnoreCase(matchFiles.get(ch.Amount__c).Remitter_Name__c)){
                                        i++;
                                    }
                                    System.debug(logginglevel.INFO, 'Number of matched words'+i);
                                }
                                }
                                
                                if(i > 0){
                                    ch.Status__c = 'Deposit Matched,Claim Matched';
                                    ch.Check_Deposit__c = matchFiles.get(ch.Amount__c).Id;
                                    matchFiles.get(ch.Amount__c).Status__c = 'Claim File with matching deposit';
                                } else {
                                    ch.Status__c = 'Deposit Present, no Claim Matched';
                                    System.debug(logginglevel.ERROR, 'Match not found');
                                }
                                
                            }
                        }
                        
                            
                         }else {
                        
                                System.debug(logginglevel.INFO, '****No Clearinghouse Files found****');
                                    ch.Status__c = 'Deposit Present, no Claim Matched';
                                } 
                }
                     
                
                         }
                update matchFiles.values();
            } else {
                for (Clearinghouse__c cd : Trigger.New){
                    System.debug(logginglevel.INFO, '****No Clearinghouse Files found****');
                    cd.Status__c = 'Deposit Present, no Claim Matched';
                }
            }

        }
    }
}