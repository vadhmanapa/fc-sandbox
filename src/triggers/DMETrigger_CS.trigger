trigger DMETrigger_CS on DME__c (before insert, before update) {
    
    if(trigger.isBefore && trigger.isInsert && (DMEUtils_CS.isFirstRun() || System.Test.isRunningTest())){
        
        DMEUtils_CS.BeforeInsertProcessor(trigger.new);//Sets Max Delivery Time for DMEs and Finds appropriate DME Category
        DMEUtils_CS.validateDMECode(trigger.new); //NOTE: add an overload method to handle single DME and add call to BeforeInsertProcessor
        DMEUtils_CS.setFirstRunFalse();
    }//END Before and Insert
    
    if(trigger.isBefore && trigger.isUpdate && (DMEUtils_CS.isBeforeUpdateFirstRun() || System.Test.isRunningTest())){
      
        DMEUtils_CS.setBeforeUpdateFirstRunFalse();
    }//End Before and Update
    
/*  
    DME_Settings__c dmeSettings = DME_Settings__c.getInstance('Default');
    if(DMEUtils_CS.isFirstRun() || System.Test.isRunningTest()) {
        List<DME_Category__c> catList = [Select Name, Code_Start__c, Code_End__c From DME_Category__c];
        
        for(DME__c dme : Trigger.new) {
            Boolean categoryFound = false;
            if(dme.Max_Delivery_Time__c == null) {//Set default max delivery time
                dme.Max_Delivery_Time__c = dmeSettings.Max_Delivery_Time__c;
            }
            
            //Automatically assign DME Product to proper DME Category
            if(catList.size() > 0){
                Integer i = 0;
                while(dme.DME_Category__c == null && i < catList.size()){
                    if(catList[i].Code_Start__c <= dme.Name && catList[i].Code_End__c  >= dme.Name){
                        dme.DME_Category__c = catList[i].Id;
                        categoryFound = true;
                    }
                    i++;    
                }
                
            }
            if(!categoryFound){
                dme.Name.addError('This product was not found in the range of any of the DME Categories');
            }
        }
        DMEUtils_CS.setFirstRunFalse();
    }
*/  
}