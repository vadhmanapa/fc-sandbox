trigger EmaiAssignment_IP on Email_Log__c (before insert, before update) {
    
    // Currently running for Insert trigger
    if(Trigger.isBefore){
        if (Trigger.isInsert){
            // RunTriggerEmailAssignment.OnBeforeInsert(Trigger.New);
            List <User> clearUsers = [Select Id, Name, ClearHelp__c, Last_Email_Asigned__c from User where isActive = true AND ClearHelp__c = true ORDER BY Last_Email_Asigned__c ASC ];
            List <User> claimUsers = [Select Id, Name, Last_Email_Asigned__c, Assign_Email__c from User where isActive = true AND Assign_Email__c = true ORDER BY Last_Email_Asigned__c ASC ];
            List<Email_Log__c> toUpdate = new List<Email_Log__c>();
            
            // if Clearhelp is not empty, run this query
            Integer i = 0;
            System.debug('Number of available Clear Users are' + clearUsers.size());
            for (Email_Log__c email : Trigger.new){
                // for email logs not through inbound email handler
                if (email.All_To_Addresses__c == null){
                    // then address was not found
                    email.All_To_Addresses__c = email.To_Email_Address__c;
                }
                Integer ccd = 0;
                if (email.CC_Addresses__c != null){
                    if (email.CC_Addresses__c.contains ('clearhelp')){
                        ccd = 1;
                    } else if (email.CC_Addresses__c.contains ('claims')){
                        ccd = 2;
                    }
                }
                
                if ((email.All_To_Addresses__c != '' && email.All_To_Addresses__c.contains ('clearhelp')) ||  ccd == 1){
                    System.debug('User considered'+clearUsers[i].Name);
                    email.Assigned_To__c = clearUsers[i].Id;
                    email.OwnerId = clearUsers[i].Id;
                    email.Department_Responsible__c = 'Clear' ;
                    clearUsers[i].Last_Email_Asigned__c = System.now();
                    i++;
                    if (i >= clearUsers.size()){
                        System.debug ('Start the counter again');
                        i = 0;
                    }
                } else if ((email.All_To_Addresses__c != '' && email.All_To_Addresses__c.contains ('claims')) || ccd == 2){
                    email.Assigned_To__c = claimUsers[i].Id;
                    email.OwnerId = claimUsers[i].Id;
                    email.Department_Responsible__c = 'Claims' ;
                    claimUsers[i].Last_Email_Asigned__c = System.now(); 
                    
                    i++;
                    if (i >= clearUsers.size()){
                        System.debug ('Start the counter again');
                        i = 0;
                    }  
                }
                toUpdate.add(email);
                
            }
            
            
            try {
                // update toUpdate;
                update clearUsers;
                update claimUsers;
            } catch(Exception e) {
                System.debug(e.getMessage());
            }
        }
        if(Trigger.isUpdate){
            List<Case> createCase = new List<Case>();
            List <Claims_Inquiry__c> newMailIssue = new List <Claims_Inquiry__c>();
            List<QueueSobject> queId = [Select QueueId from QueueSobject where SobjectType = 'Case' AND Queue.Name = 'Que Support Inbox' LIMIT 1];
            List<RecordType> recId = [Select Id from RecordType where SobjectType = 'Case' AND Name = 'Que' LIMIT 1];
            for(Email_Log__c el : Trigger.New){
                
                // Added by Yahia
                // Auto close emails that need to transfer to CS
                if(el.Email_Reason__c == 'Transfer to CS'){
                    el.Number_of_claims__c = 0;
                    el.Status__c = 'Closed';
                }
                
                // If status is closed, sign the name and date
                if(el.Status__c == 'Closed' && el.Closed_By__c == Null & el.Closed_on__c == Null){
                    el.Closed_by__c = UserInfo.getUserId();
                    el.Closed_on__c = system.now();
                }
 
                Email_Log__c checkPoint = Trigger.oldMap.get(el.Id);
                //Boolean isInquiryAlreadyTrue = checkPoint.Pushed_to_Inquiry__c;
                Boolean isQueAlreadyTrue = checkPoint.Forwarded_to_Que__c;
                
                if (el.Forwarded_to_Que__c == true && isQueAlreadyTrue != true){
                    // create a new case
                    createCase.add(new Case(SuppliedEmail = el.From_Email_Address__c,Subject = el.Email_Subject__c, Description = el.Email_Content__c, RecordTypeId = recId[0].Id,
                                            AccountId = el.Email_Account__r.Id, ContactId = el.Email_Contact__r.Id, Email_Id__c = el.Name,
                                            Que_Email_Log__c = el.Id, Type = 'Que Support',  Origin = 'Claims Email', OwnerId = queId[0].QueueId));
                    System.debug('Case has been added');
                    
                } 
                if(el.Pushed_to_Inquiry__c == true){
                    List <Claims_Inquiry__c> mailIssue = [Select Id, Email_Log__c from Claims_Inquiry__c where Email_Log__c =: el.Id];
                    
                    // create a new issue log
                    DateTime dt = el.CreatedDate;
                    
                    Claims_Inquiry__c newClaim = new Claims_Inquiry__c (Provider__c = el.Email_Account__c, Contact__c = el.Email_Contact__c, 
                                                                        Inquiry_via__c = 'Email', Email_Log__c = el.Id,Status_of_Inquiry__c = 'Open', 
                                                                        Inquiry_Created_on__c = dt.date(), Date_Time_of_Inquiry__c = el.CreatedDate);
                    newMailIssue.add(newClaim);
                    
                    el.Pushed_to_Inquiry__c = false;
                }
            }
            // insert all logs
            if (createCase.size() > 0){
                insert createCase;
                System.debug('Number of cases inserted' + createCase.size());
            } 
            if(newMailIssue.size() > 0){
                insert newMailIssue;
                System.debug('Number of inquirues inserted' + newMailIssue.size());
            }            
            
        }
    }
}