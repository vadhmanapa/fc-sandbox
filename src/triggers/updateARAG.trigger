trigger updateARAG on ARAG_Follow_Up__c (after insert){        

    //List<ID> aragIds = New List<ID>();
    /*Set<Id> aragIds = new Set<Id>();

    for(ARAG_Follow_Up__c afu : Trigger.new){

        if(afu.Related_CWI__c != null){                   
            aragIds.add(afu.Related_CWI__c);
        }
    }
  
    system.debug('ARAG Id list size:' + aragIds.size());

    Map<Id, ARAG__c> aragFUMap = new Map<Id, ARAG__c> ([
        SELECT 
            Id, 
            Pending_Action__c, 
            Resolution_Action__c,
            CRS_Correction_Type__c, 
            Collector_s_Notes__c, 
            Completed__c,
            Completed_Date__c,
            Worked_By__c
        FROM ARAG__c 
        WHERE Id IN :aragIds
    ]);
    
    List<ARAG__c> updatedARAG = new List<ARAG__c>();                                //new empty list

    for (ARAG_Follow_Up__c afu : Trigger.new){
        ARAG__c a = aragFUMap.get(afu.Related_CWI__c);
        
        if (a != null) 
        {
            a.Pending_Action__c = afu.Pending_Action__c;                           
            a.Resolution_Action__c = afu.Resolution_Action__c;
            a.CRS_Correction_Type__c = afu.CRS_Correction_Type__c;
            a.Collector_s_Notes__c = afu.Collector_s_Notes__c;
            a.Completed__c = afu.Completed__c;
            a.Completed_Date__c = afu.Completed_Date__c;
            a.Worked_By__c = afu.Worked_By__c;
            
            system.debug('Fields are being updated for: ' + a.Id);
            updatedARAG.add(a);                                                         //add item to the new list
            system.debug('UpdatedARAG List first size:' + updatedARAG.size());
        }
    }
        
    update updatedARAG;
    system.debug('UpdatedARAG List second size:' + updatedARAG.size());*/
}