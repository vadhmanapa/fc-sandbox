trigger ProviderCategoryTrigger_CS on Provider_Category__c (after insert, after update, after delete)  {
    ProviderCategoryTriggerDispatcher_CS dispatch = new ProviderCategoryTriggerDispatcher_CS(
        trigger.oldMap,
        trigger.newMap,
        trigger.old,
        trigger.new,
        trigger.isInsert,
        trigger.isUpdate,
        trigger.isDelete,
        trigger.isUndelete,
        trigger.isBefore,
        trigger.isAfter
    );
}