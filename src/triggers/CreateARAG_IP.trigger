trigger CreateARAG_IP on PADR__c (before insert, before update) {
// This trigger is created to help Claims team to convert the Write-Off's into ARAG by single click and copy the data.
//Initialize
for (PADR__c ar : Trigger.new){
	List <ARAG__c> newRecord = new List <ARAG__c>();
	List <PADR__c> toConvert = new List <PADR__c>();
	String provider;
	String recordId = '';
	// entering if condition
	if (ar.Claim_Closed__c == true){
		System.debug('Entering ARAG creation loop');
		toConvert.add (ar);
		for (PADR__c pad : toConvert){
			List <ARAG__c> oldMap = [Select Id, Bill__c, CreatedDate from ARAG__c where Bill__c =: pad.Bill_Number__c ORDER BY CreatedDate DESC];
			if (oldMap.size() > 0){
				recordId = oldMap[0].Id;
			} else {
				provider = pad.Provider__r.Name;
				ARAG__c newAdd = new ARAG__c(Payor_Family__c = pad.Health_Plan__c, Provider__c = pad.Provider_Name__c, Bill__c = pad.Bill_Number__c,
										OwnerId = UserInfo.getUserId(), Worked_By__c = pad.OwnerId, Write_Off_Reason__c = pad.Write_Off_Reason__c,
										Collector_s_Notes__c = pad.Additional_Denial_Notes__c, Claim_Number__c = pad.Claim__c);
				newRecord.add (newAdd);
			}
			try {
						  Database.SaveResult[] srARList = Database.insert(newRecord, false);
                			for (Database.SaveResult dsr : srARList) {
                				if (dsr.isSuccess()) {
                        		// Operation was successful, so get the ID of the record that was processed
                        		System.debug('Successfully inserted ARAG. ARAG Record ID: ' + dsr.getId());
                        		// isSuccess = true;
                        		recordId = dsr.getId();
							}else {
                            	// Operation failed, so get all errors                
                            	for(Database.Error err : dsr.getErrors()) {
                                    System.debug('The following error has occurred.');                    
                                		System.debug(err.getStatusCode() + ': ' + err.getMessage());
                                	System.debug('ARAG__c fields that affected this error: ' + err.getFields());
                            	}
							}
							}
						}catch (System.Dmlexception e) {
            						System.debug('ARAG not inserted. Job unsuccessful ' + e.getMessage());
       						}
       					if (recordId != null){
									pad.Related_ARAG__c = recordId;
									System.debug('The Claims Inquiry is mapped to ARAG');
								}else {
									System.debug('Some issue happened');
								}

		}
		
	}
}
}