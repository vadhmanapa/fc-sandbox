trigger StatusDenied on Denials__c (before insert, before update){        
    
    List<ID> aragIds = New List<ID>();

    for(Denials__c d : Trigger.new){

        if(d.Work_Denial__c == true && d.Related_CWI__c != null){                   
            aragIds.add(d.Related_CWI__c);
            system.debug(d.Related_CWI__c);
        }
    }
    system.debug('ARAG Id list size:' + aragIds.size());

    List<ARAG__c> ARAGList = [SELECT Id, Status__c FROM ARAG__c WHERE Id in :aragIds];     
    system.debug('ARAG list size' + ARAGList.size() + 'ARAG List: ' + ARAGList);

    for(ARAG__c a : ARAGList){
        a.Status__c = 'Denied';
        system.debug('Status changed for: ' + a.Id);
    }
    
    update ARAGList;
}