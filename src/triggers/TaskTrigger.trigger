trigger TaskTrigger on Task (after insert, after update) {

	if(Trigger.isAfter) {
		if(Trigger.isInsert) {
			TaskTriggerDispatcher.afterInsert(Trigger.new);
		} else if(Trigger.isUpdate) {
			TaskTriggerDispatcher.afterUpdate(Trigger.new);
		}
	}
}