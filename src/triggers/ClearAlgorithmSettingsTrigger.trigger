trigger ClearAlgorithmSettingsTrigger on Clear_Algorithm_Settings__c (before insert, before update, before delete, after insert, after update, after delete) {
    ClearAlgorithmSettingsTriggerDispatcher dispatch = new ClearAlgorithmSettingsTriggerDispatcher(
        trigger.oldMap,
        trigger.newMap,
        trigger.old,
        trigger.new,
        trigger.isInsert,
        trigger.isUpdate,
        trigger.isDelete,
        trigger.isUndelete,
        trigger.isBefore,
        trigger.isAfter
    );
}