<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Unable to Obtain Documentation</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>DisplayLabel__c</field>
        <value xsi:type="xsd:string">Please Cancel - Unable to Obtain Documentation</value>
    </values>
    <values>
        <field>OrderStatusMapped__c</field>
        <value xsi:type="xsd:string">Needs Attention - Please Cancel - Unable to Obtain Documentation</value>
    </values>
    <values>
        <field>Stage__c</field>
        <value xsi:type="xsd:string">Needs Attention</value>
    </values>
    <values>
        <field>VisibletoPayor__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>VisibletoProvider__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
