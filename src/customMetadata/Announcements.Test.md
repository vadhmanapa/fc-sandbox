<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Test</label>
    <protected>false</protected>
    <values>
        <field>Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Created_Date__c</field>
        <value xsi:type="xsd:date">2016-04-01</value>
    </values>
    <values>
        <field>Heading__c</field>
        <value xsi:type="xsd:string">Test Announcement</value>
    </values>
    <values>
        <field>Message_Body__c</field>
        <value xsi:type="xsd:string">Testing the announcement body. Cheers!!!</value>
    </values>
</CustomMetadata>
